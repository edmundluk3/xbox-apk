.class public Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "StoreGoldItemsScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
        ">;"
    }
.end annotation


# instance fields
.field private listViewRowLayoutId:I

.field private originalCollection:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;"
        }
    .end annotation
.end field

.field private storeGoldListFilter:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

.field private willDisplaySubheaders:Z

.field private workingCollection:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83
    .local p3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;>;"
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->willDisplaySubheaders:Z

    .line 28
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;->Unspecified:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->storeGoldListFilter:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    .line 84
    iput p2, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->listViewRowLayoutId:I

    .line 85
    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->addAll(Ljava/util/Collection;)V

    .line 86
    return-void
.end method

.method private filterGoldLoungeItemsBySection(Ljava/util/ArrayList;Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;)Ljava/util/ArrayList;
    .locals 5
    .param p2, "section"    # Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;",
            "Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;>;"
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;->Unspecified:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    if-ne p2, v4, :cond_1

    .line 57
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 71
    :cond_0
    return-object v2

    .line 60
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 64
    .local v3, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 65
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;

    .line 66
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->getGoldResultItemSection()Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    move-result-object v4

    if-ne v4, p2, :cond_2

    .line 67
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private populateListRowItem(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;)V
    .locals 7
    .param p1, "viewHolder"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 122
    if-eqz p2, :cond_2

    .line 124
    const/16 v3, 0x2329

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 125
    .local v0, "defaultRid":I
    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->titleImageUrl:Ljava/lang/String;

    .line 126
    .local v1, "imageUri":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 127
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    move-result-object v3

    invoke-virtual {v3, v1, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 129
    :cond_0
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->TitleName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v3, p2, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->GoldWeekendText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 132
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$700(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->GoldWeekendText:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 138
    :goto_0
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$800(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 139
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->willDisplaySubheaders:Z

    if-eqz v3, :cond_6

    .line 140
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->showGamesWithGoldHeaderText()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 141
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$800(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 142
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$800(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07012a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    :cond_1
    :goto_1
    iget-object v3, p2, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->ReleaseDate:Ljava/util/Date;

    if-eqz v3, :cond_8

    .line 155
    iget-object v3, p2, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->ReleaseDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getYear()I

    move-result v3

    add-int/lit16 v2, v3, 0x76c

    .line 157
    .local v2, "year":I
    const/16 v3, 0xaef

    if-lt v2, v3, :cond_7

    .line 158
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$400(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    const v4, 0x7f0703e9

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 166
    .end local v0    # "defaultRid":I
    .end local v1    # "imageUri":Ljava/lang/String;
    .end local v2    # "year":I
    :cond_2
    :goto_2
    return-void

    .line 134
    .restart local v0    # "defaultRid":I
    .restart local v1    # "imageUri":Ljava/lang/String;
    :cond_3
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$600(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->DiscountedPriceText:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 135
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->ListPriceText:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 143
    :cond_4
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->showDealsWithGoldHeaderText()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 144
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$800(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 145
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$800(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070125

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 147
    :cond_5
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$800(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_1

    .line 150
    :cond_6
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$800(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_1

    .line 160
    .restart local v2    # "year":I
    :cond_7
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$400(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 163
    .end local v2    # "year":I
    :cond_8
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$400(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_2
.end method


# virtual methods
.method public add(Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;)V
    .locals 2
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->storeGoldListFilter:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->filterGoldLoungeItemsBySection(Ljava/util/ArrayList;Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    .line 215
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->notifyDataSetChanged()V

    .line 216
    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->add(Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;)V

    return-void
.end method

.method public addAll(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 199
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<+Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 200
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    .line 204
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->storeGoldListFilter:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->filterGoldLoungeItemsBySection(Ljava/util/ArrayList;Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    .line 205
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->notifyDataSetChanged()V

    .line 206
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 226
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->notifyDataSetChanged()V

    .line 227
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->getItem(I)Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 194
    int-to-long v0, p1

    return-wide v0
.end method

.method public getStoreGoldListFilter()Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->storeGoldListFilter:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 91
    move-object v1, p2

    .line 92
    .local v1, "v":Landroid/view/View;
    if-nez v1, :cond_1

    .line 93
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 94
    .local v2, "vi":Landroid/view/LayoutInflater;
    iget v4, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->listViewRowLayoutId:I

    const/4 v5, 0x0

    invoke-virtual {v2, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 95
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$1;)V

    .line 97
    .local v3, "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    const v4, 0x7f0e0a7b

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$102(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Landroid/view/View;)Landroid/view/View;

    .line 98
    const v4, 0x7f0e0a73

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$202(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 99
    const v4, 0x7f0e0a74

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$302(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 100
    const v4, 0x7f0e0a75

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$402(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 101
    const v4, 0x7f0e0a78

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$502(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 102
    const v4, 0x7f0e0a79

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$602(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 103
    const v4, 0x7f0e0a7d

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$702(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 104
    const v4, 0x7f0e0a7c

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$802(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 106
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 107
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v4

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getPaintFlags()I

    move-result v5

    or-int/lit8 v5, v5, 0x10

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setPaintFlags(I)V

    .line 110
    :cond_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 115
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->getItem(I)Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;

    move-result-object v0

    .line 116
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;
    invoke-direct {p0, v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->populateListRowItem(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;)V

    .line 117
    return-object v1

    .line 112
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;
    .end local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;

    .restart local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    goto :goto_0
.end method

.method public getWillDisplaySubheaders()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->willDisplaySubheaders:Z

    return v0
.end method

.method public remove(Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;)V
    .locals 1
    .param p1, "object"    # Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 237
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->notifyDataSetChanged()V

    .line 238
    return-void
.end method

.method public bridge synthetic remove(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->remove(Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;)V

    return-void
.end method

.method public setStoreGoldListFilter(Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;)V
    .locals 2
    .param p1, "value"    # Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->storeGoldListFilter:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    .line 40
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;->Unspecified:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    if-ne p1, v0, :cond_0

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    .line 46
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 52
    return-void

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->originalCollection:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->storeGoldListFilter:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->filterGoldLoungeItemsBySection(Ljava/util/ArrayList;Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->workingCollection:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public setWillDisplaySubheaders(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->willDisplaySubheaders:Z

    .line 80
    return-void
.end method
