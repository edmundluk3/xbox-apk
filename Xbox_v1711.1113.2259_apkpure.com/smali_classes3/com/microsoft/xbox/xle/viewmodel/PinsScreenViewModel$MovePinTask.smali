.class Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PinsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MovePinTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final request:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

.field public final requestId:J

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;JLcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;)V
    .locals 0
    .param p2, "requestId"    # J
    .param p4, "request"    # Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 208
    iput-wide p2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->requestId:J

    .line 209
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->request:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    .line 210
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 7

    .prologue
    .line 229
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v1

    .line 230
    .local v1, "pm":Lcom/microsoft/xbox/service/model/pins/PinsModel;
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->requestId:J

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->request:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->getSrcItem()Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->request:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->getDstItem()Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->request:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->isBeforeDstItem()Z

    move-result v6

    invoke-virtual/range {v1 .. v6}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->move(JLcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 231
    .local v0, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->request:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->setLastResult(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 232
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    return-object v2
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 224
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 6

    .prologue
    .line 219
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->requestId:J

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->request:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    sget-object v5, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-object v1, p0

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;JLcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 220
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->requestId:J

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->request:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    move-object v1, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;JLcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 242
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 203
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 237
    return-void
.end method
