.class Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "BundlesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private TitleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private titleTextView:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$1;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;->TitleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;)Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;->TitleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;->titleTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;->titleTextView:Landroid/widget/TextView;

    return-object p1
.end method
