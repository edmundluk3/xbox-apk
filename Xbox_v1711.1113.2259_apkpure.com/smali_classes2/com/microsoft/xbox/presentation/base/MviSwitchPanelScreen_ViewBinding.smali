.class public Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;
.super Ljava/lang/Object;
.source "MviSwitchPanelScreen_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;)V
    .locals 0
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p1}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;Landroid/view/View;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;->target:Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;

    .line 26
    const v0, 0x7f0e0a95

    const-string v1, "field \'switchPanel\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 27
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;->target:Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;

    .line 33
    .local v0, "target":Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 34
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;->target:Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;

    .line 36
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 37
    return-void
.end method
