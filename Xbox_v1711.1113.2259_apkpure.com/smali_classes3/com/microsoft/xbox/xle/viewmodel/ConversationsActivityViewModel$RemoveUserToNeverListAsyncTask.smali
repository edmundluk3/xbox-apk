.class Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ConversationsActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemoveUserToNeverListAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

.field private unblockUserXuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "blockUserXuid"    # Ljava/lang/String;

    .prologue
    .line 579
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 580
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->unblockUserXuid:Ljava/lang/String;

    .line 581
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 585
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 586
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 614
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->forceLoad:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->unblockUserXuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeUserFromNeverList(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 609
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 591
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 592
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->unblockUserXuid:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    .line 593
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 604
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->unblockUserXuid:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    .line 605
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 575
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 597
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 598
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$602(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Z)Z

    .line 599
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->updateAdapter()V

    .line 600
    return-void
.end method
