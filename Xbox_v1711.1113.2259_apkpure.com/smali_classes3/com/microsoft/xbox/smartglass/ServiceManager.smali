.class public Lcom/microsoft/xbox/smartglass/ServiceManager;
.super Ljava/lang/Object;
.source "ServiceManager.java"


# instance fields
.field private final _catalogServiceManager:Lcom/microsoft/xbox/smartglass/CatalogServiceManager;

.field private final _pServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

.field private final _xboxLiveServiceManager:Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;


# direct methods
.method constructor <init>(J)V
    .locals 5
    .param p1, "pPlatform"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/ServiceManager;->initialize(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/ServiceManager;->_pServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/smartglass/CatalogServiceManager;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/ServiceManager;->_pServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/smartglass/ServiceManager;->getCatalog(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/CatalogServiceManager;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/ServiceManager;->_catalogServiceManager:Lcom/microsoft/xbox/smartglass/CatalogServiceManager;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/ServiceManager;->_pServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/smartglass/ServiceManager;->getXboxLive(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/ServiceManager;->_xboxLiveServiceManager:Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;

    .line 17
    return-void
.end method

.method private native getCatalog(J)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native getXboxLive(J)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native initialize(J)J
.end method

.method private native setCulture(JLjava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method


# virtual methods
.method public getCatalog()Lcom/microsoft/xbox/smartglass/CatalogServiceManager;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/ServiceManager;->_catalogServiceManager:Lcom/microsoft/xbox/smartglass/CatalogServiceManager;

    return-object v0
.end method

.method public getXboxLive()Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/ServiceManager;->_xboxLiveServiceManager:Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;

    return-object v0
.end method

.method public setCulture(Ljava/lang/String;)V
    .locals 2
    .param p1, "culture"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/ServiceManager;->_pServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/ServiceManager;->setCulture(JLjava/lang/String;)J

    .line 26
    return-void
.end method
