.class final enum Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;
.super Ljava/lang/Enum;
.source "ClubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RosterChangeType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

.field public static final enum ADD_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

.field public static final enum ADD_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

.field public static final enum REMOVE_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

.field public static final enum REMOVE_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 105
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    const-string v1, "ADD_USER"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->ADD_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    const-string v1, "REMOVE_USER"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    const-string v1, "ADD_ROLE"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->ADD_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    const-string v1, "REMOVE_ROLE"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->ADD_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->ADD_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->$VALUES:[Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 105
    const-class v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->$VALUES:[Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    return-object v0
.end method
