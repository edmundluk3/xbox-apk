.class public Lcom/microsoft/playready/LicenseAcquisitionTask;
.super Ljava/util/concurrent/FutureTask;
.source "LicenseAcquisitionTask.java"

# interfaces
.implements Lcom/microsoft/playready/ILicenseAcquisitionTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Lcom/microsoft/playready/ILicenseAcquisitionTask;"
    }
.end annotation


# instance fields
.field private mLicenseAcquirerListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/playready/ILicenseAcquirerListener;",
            ">;"
        }
    .end annotation
.end field

.field private mListenersNotified:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;)V
    .locals 1
    .param p1, "worker"    # Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/LicenseAcquisitionTask;->mLicenseAcquirerListeners:Ljava/util/ArrayList;

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/playready/LicenseAcquisitionTask;->mListenersNotified:Z

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/LicenseAcquisitionTask;->mLicenseAcquirerListeners:Ljava/util/ArrayList;

    .line 32
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;)V
    .locals 1
    .param p1, "worker"    # Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/LicenseAcquisitionTask;->mLicenseAcquirerListeners:Ljava/util/ArrayList;

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/playready/LicenseAcquisitionTask;->mListenersNotified:Z

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/LicenseAcquisitionTask;->mLicenseAcquirerListeners:Ljava/util/ArrayList;

    .line 25
    return-void
.end method


# virtual methods
.method public addLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/ILicenseAcquirerListener;

    .prologue
    .line 57
    monitor-enter p0

    .line 59
    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/playready/LicenseAcquisitionTask;->mListenersNotified:Z

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/microsoft/playready/LicenseAcquisitionTask;->mLicenseAcquirerListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    :goto_0
    monitor-exit p0

    .line 68
    return-void

    .line 65
    :cond_0
    invoke-interface {p1, p0}, Lcom/microsoft/playready/ILicenseAcquirerListener;->onAcquireLicenseCompleted(Lcom/microsoft/playready/ILicenseAcquisitionTask;)V

    goto :goto_0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected done()V
    .locals 3

    .prologue
    .line 37
    monitor-enter p0

    .line 39
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/LicenseAcquisitionTask;->mLicenseAcquirerListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 44
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/playready/LicenseAcquisitionTask;->mListenersNotified:Z

    .line 37
    monitor-exit p0

    .line 46
    return-void

    .line 39
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/ILicenseAcquirerListener;

    .line 41
    .local v0, "listener":Lcom/microsoft/playready/ILicenseAcquirerListener;
    invoke-interface {v0, p0}, Lcom/microsoft/playready/ILicenseAcquirerListener;->onAcquireLicenseCompleted(Lcom/microsoft/playready/ILicenseAcquisitionTask;)V

    goto :goto_0

    .line 37
    .end local v0    # "listener":Lcom/microsoft/playready/ILicenseAcquirerListener;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getResponseCustomData()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/microsoft/playready/LicenseAcquisitionTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public removeLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/ILicenseAcquirerListener;

    .prologue
    .line 73
    monitor-enter p0

    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/LicenseAcquisitionTask;->mLicenseAcquirerListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 73
    monitor-exit p0

    .line 77
    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
