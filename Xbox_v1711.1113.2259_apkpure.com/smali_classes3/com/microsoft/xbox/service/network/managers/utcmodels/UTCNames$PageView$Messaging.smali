.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView$Messaging;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Messaging"
.end annotation


# static fields
.field public static final ConversationView:Ljava/lang/String; = "Messaging - Conversation View"

.field public static final ConversationsView:Ljava/lang/String; = "Messaging - Conversations View"

.field public static final RenameConversationView:Ljava/lang/String; = "Messaging - Rename Conversation View"

.field public static final ViewAllMembersView:Ljava/lang/String; = "Messaging - View All Members View"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
