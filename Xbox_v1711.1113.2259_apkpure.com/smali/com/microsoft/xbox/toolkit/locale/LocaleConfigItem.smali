.class public Lcom/microsoft/xbox/toolkit/locale/LocaleConfigItem;
.super Ljava/lang/Object;
.source "LocaleConfigItem.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    name = "LocaleConfigItem"
.end annotation


# instance fields
.field public CountryRegion:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public Language:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = false
    .end annotation
.end field

.field public Locale:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
