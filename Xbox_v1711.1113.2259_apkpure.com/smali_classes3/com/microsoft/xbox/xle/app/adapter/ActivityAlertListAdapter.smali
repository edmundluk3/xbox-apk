.class public Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ActivityAlertListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 27
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 33
    if-nez p2, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->inflateView(Landroid/content/Context;)Landroid/view/View;

    move-result-object p2

    .line 35
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;

    .line 36
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 41
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 42
    .local v1, "item":Ljava/lang/Object;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->updateUI(Ljava/lang/Object;)V

    .line 44
    return-object p2

    .line 38
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;
    .end local v1    # "item":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;

    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;
    goto :goto_0
.end method
