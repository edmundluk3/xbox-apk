.class public Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;
.super Landroid/view/View;
.source "CirclePageIndicator.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# static fields
.field private static final KEY_CURRENT_PAGE:Ljava/lang/String; = "currentPage"

.field private static final KEY_SUPER_STATE:Ljava/lang/String; = "superState"


# instance fields
.field private currentPage:I

.field private currentPageOffset:F

.field private leftWrapping:Z

.field private paintFillPrimary:Landroid/graphics/Paint;

.field private paintFillSecondary:Landroid/graphics/Paint;

.field private radius:F

.field private rightWrapping:Z

.field private scrollState:I

.field private viewPager:Landroid/support/v4/view/ViewPager;

.field private viewPagerId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->paintFillPrimary:Landroid/graphics/Paint;

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->paintFillPrimary:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 50
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->paintFillSecondary:Landroid/graphics/Paint;

    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->paintFillSecondary:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 53
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->CirclePageIndicator:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 54
    .local v0, "a":Landroid/content/res/TypedArray;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->paintFillPrimary:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 55
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->paintFillSecondary:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 56
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    .line 57
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPagerId:I

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    return-void
.end method

.method private getPageCount()I
    .locals 2

    .prologue
    .line 172
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    if-nez v1, :cond_0

    .line 173
    const/4 v1, 0x0

    .line 179
    :goto_0
    return v1

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    .line 177
    .local v0, "pagerAdapter":Landroid/support/v4/view/PagerAdapter;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 179
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->isCarousel()Z

    move-result v1

    if-eqz v1, :cond_1

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;

    .line 180
    .end local v0    # "pagerAdapter":Landroid/support/v4/view/PagerAdapter;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->getRealCount()I

    move-result v1

    goto :goto_0

    .line 181
    .restart local v0    # "pagerAdapter":Landroid/support/v4/view/PagerAdapter;
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v1

    goto :goto_0
.end method

.method private isCarousel()Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private measureHeight(I)I
    .locals 6
    .param p1, "heightMeasureSpec"    # I

    .prologue
    .line 118
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 119
    .local v2, "specMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 121
    .local v3, "specSize":I
    const/high16 v4, 0x40000000    # 2.0f

    if-ne v2, v4, :cond_1

    .line 122
    move v1, v3

    .line 132
    .local v1, "height":I
    :cond_0
    :goto_0
    return v1

    .line 124
    .end local v1    # "height":I
    :cond_1
    const/high16 v4, 0x40000000    # 2.0f

    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    mul-float v0, v4, v5

    .line 125
    .local v0, "diameter":F
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getPaddingTop()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getPaddingBottom()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v1, v4

    .line 127
    .restart local v1    # "height":I
    const/high16 v4, -0x80000000

    if-ne v2, v4, :cond_0

    .line 128
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0
.end method

.method private measureWidth(I)I
    .locals 7
    .param p1, "widthMeasureSpec"    # I

    .prologue
    .line 94
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 95
    .local v2, "specMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 97
    .local v3, "specSize":I
    const/high16 v5, 0x40000000    # 2.0f

    if-ne v2, v5, :cond_1

    .line 98
    move v4, v3

    .line 113
    .local v4, "width":I
    :cond_0
    :goto_0
    return v4

    .line 100
    .end local v4    # "width":I
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getPageCount()I

    move-result v1

    .line 101
    .local v1, "pageCount":I
    const/high16 v5, 0x40000000    # 2.0f

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    mul-float v0, v5, v6

    .line 103
    .local v0, "diameter":F
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getLeftPaddingOffset()I

    move-result v5

    .line 104
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getRightPaddingOffset()I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v6, v1

    mul-float/2addr v6, v0

    float-to-int v6, v6

    add-int/2addr v5, v6

    add-int/lit8 v6, v1, -0x1

    int-to-float v6, v6

    mul-float/2addr v6, v0

    float-to-int v6, v6

    add-int v4, v5, v6

    .line 108
    .restart local v4    # "width":I
    const/high16 v5, -0x80000000

    if-ne v2, v5, :cond_0

    .line 109
    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPagerId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getRootView()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPagerId:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 69
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 137
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 139
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getPageCount()I

    move-result v11

    if-lez v11, :cond_2

    .line 140
    const/4 v0, 0x0

    .line 141
    .local v0, "cx":F
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getPaddingTop()I

    move-result v12

    int-to-float v12, v12

    add-float v1, v11, v12

    .line 142
    .local v1, "cy":F
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getLeftPaddingOffset()I

    move-result v12

    int-to-float v12, v12

    add-float v5, v11, v12

    .line 143
    .local v5, "leftOffset":F
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    const/high16 v12, 0x40400000    # 3.0f

    mul-float v10, v11, v12

    .line 146
    .local v10, "threeRadii":F
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getPageCount()I

    move-result v11

    if-ge v4, v11, :cond_0

    .line 147
    int-to-float v11, v4

    mul-float/2addr v11, v10

    add-float v0, v11, v5

    .line 148
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->paintFillSecondary:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v11, v12}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 146
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 151
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->isCarousel()Z

    move-result v11

    if-eqz v11, :cond_7

    iget-boolean v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->leftWrapping:Z

    if-nez v11, :cond_1

    iget-boolean v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->rightWrapping:Z

    if-eqz v11, :cond_7

    .line 153
    :cond_1
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    iget v12, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPageOffset:F

    mul-float v6, v11, v12

    .line 154
    .local v6, "r1":F
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    iget v12, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    iget v13, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPageOffset:F

    mul-float/2addr v12, v13

    sub-float v7, v11, v12

    .line 156
    .local v7, "r2":F
    iget-boolean v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->leftWrapping:Z

    if-eqz v11, :cond_3

    move v9, v5

    .line 157
    .local v9, "shrinkingX":F
    :goto_1
    iget-boolean v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->leftWrapping:Z

    if-eqz v11, :cond_4

    move v8, v6

    .line 158
    .local v8, "shrinkingR":F
    :goto_2
    iget-boolean v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->leftWrapping:Z

    if-eqz v11, :cond_5

    move v3, v0

    .line 159
    .local v3, "growingX":F
    :goto_3
    iget-boolean v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->leftWrapping:Z

    if-eqz v11, :cond_6

    move v2, v7

    .line 161
    .local v2, "growingR":F
    :goto_4
    iget-object v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->paintFillPrimary:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v1, v8, v11}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 162
    iget-object v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->paintFillPrimary:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v1, v2, v11}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 169
    .end local v0    # "cx":F
    .end local v1    # "cy":F
    .end local v2    # "growingR":F
    .end local v3    # "growingX":F
    .end local v4    # "i":I
    .end local v5    # "leftOffset":F
    .end local v6    # "r1":F
    .end local v7    # "r2":F
    .end local v8    # "shrinkingR":F
    .end local v9    # "shrinkingX":F
    .end local v10    # "threeRadii":F
    :cond_2
    :goto_5
    return-void

    .restart local v0    # "cx":F
    .restart local v1    # "cy":F
    .restart local v4    # "i":I
    .restart local v5    # "leftOffset":F
    .restart local v6    # "r1":F
    .restart local v7    # "r2":F
    .restart local v10    # "threeRadii":F
    :cond_3
    move v9, v0

    .line 156
    goto :goto_1

    .restart local v9    # "shrinkingX":F
    :cond_4
    move v8, v7

    .line 157
    goto :goto_2

    .restart local v8    # "shrinkingR":F
    :cond_5
    move v3, v5

    .line 158
    goto :goto_3

    .restart local v3    # "growingX":F
    :cond_6
    move v2, v6

    .line 159
    goto :goto_4

    .line 165
    .end local v3    # "growingX":F
    .end local v6    # "r1":F
    .end local v7    # "r2":F
    .end local v8    # "shrinkingR":F
    .end local v9    # "shrinkingX":F
    :cond_7
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPage:I

    int-to-float v11, v11

    mul-float/2addr v11, v10

    iget v12, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPageOffset:F

    mul-float/2addr v12, v10

    add-float/2addr v11, v12

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getLeftPaddingOffset()I

    move-result v12

    int-to-float v12, v12

    add-float/2addr v11, v12

    iget v12, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    add-float v0, v11, v12

    .line 166
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->radius:F

    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->paintFillPrimary:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v11, v12}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_5
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->measureWidth(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->measureHeight(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->setMeasuredDimension(II)V

    .line 90
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 238
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->scrollState:I

    .line 239
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 209
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->isCarousel()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 210
    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->leftWrapping:Z

    .line 211
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getPageCount()I

    move-result v0

    if-ne p1, v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->rightWrapping:Z

    .line 213
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->leftWrapping:Z

    if-eqz v0, :cond_2

    .line 214
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getPageCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPage:I

    .line 224
    :goto_2
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPageOffset:F

    .line 225
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->invalidate()V

    .line 226
    return-void

    :cond_0
    move v0, v2

    .line 210
    goto :goto_0

    :cond_1
    move v1, v2

    .line 211
    goto :goto_1

    .line 215
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->getPageCount()I

    move-result v0

    if-le p1, v0, :cond_3

    .line 216
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPage:I

    goto :goto_2

    .line 218
    :cond_3
    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPage:I

    goto :goto_2

    .line 221
    :cond_4
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPage:I

    goto :goto_2
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 230
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->scrollState:I

    if-nez v0, :cond_0

    .line 231
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPage:I

    .line 232
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->invalidate()V

    .line 234
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 198
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 199
    check-cast v0, Landroid/os/Bundle;

    .line 200
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "currentPage"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPage:I

    .line 201
    const-string v1, "superState"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    .line 204
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 205
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 190
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 191
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "superState"

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 192
    const-string v1, "currentPage"

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->currentPage:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 193
    return-object v0
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 1
    .param p1, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eq v0, p1, :cond_2

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->removeOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 77
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CirclePageIndicator;->invalidate()V

    .line 85
    :cond_2
    return-void
.end method
