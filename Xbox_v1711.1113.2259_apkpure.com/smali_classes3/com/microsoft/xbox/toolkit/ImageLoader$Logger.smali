.class final Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;
.super Ljava/lang/Object;
.source "ImageLoader.java"

# interfaces
.implements Lcom/bumptech/glide/request/RequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Logger"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/bumptech/glide/request/RequestListener",
        "<TT;TR;>;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 169
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;, "Lcom/microsoft/xbox/toolkit/ImageLoader$Logger<TT;TR;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ImageLoader$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ImageLoader$1;

    .prologue
    .line 169
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;, "Lcom/microsoft/xbox/toolkit/ImageLoader$Logger<TT;TR;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;-><init>()V

    return-void
.end method


# virtual methods
.method public onException(Ljava/lang/Exception;Ljava/lang/Object;Lcom/bumptech/glide/request/target/Target;Z)Z
    .locals 7
    .param p1, "e"    # Ljava/lang/Exception;
    .param p4, "isFirstResource"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Exception;",
            "TT;",
            "Lcom/bumptech/glide/request/target/Target",
            "<TR;>;Z)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;, "Lcom/microsoft/xbox/toolkit/ImageLoader$Logger<TT;TR;>;"
    .local p2, "model":Ljava/lang/Object;, "TT;"
    .local p3, "target":Lcom/bumptech/glide/request/target/Target;, "Lcom/bumptech/glide/request/target/Target<TR;>;"
    const/4 v6, 0x0

    .line 172
    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v2, "onException({e}=%s, {model}=%s, {target}=%s, {isFirstResource}=%s)"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v6

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p3, v3, v4

    const/4 v4, 0x3

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "log":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ImageLoader;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    return v6
.end method

.method public onResourceReady(Ljava/lang/Object;Ljava/lang/Object;Lcom/bumptech/glide/request/target/Target;ZZ)Z
    .locals 1
    .param p4, "isFromMemoryCache"    # Z
    .param p5, "isFirstResource"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TT;",
            "Lcom/bumptech/glide/request/target/Target",
            "<TR;>;ZZ)Z"
        }
    .end annotation

    .prologue
    .line 179
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;, "Lcom/microsoft/xbox/toolkit/ImageLoader$Logger<TT;TR;>;"
    .local p1, "resource":Ljava/lang/Object;, "TR;"
    .local p2, "model":Ljava/lang/Object;, "TT;"
    .local p3, "target":Lcom/bumptech/glide/request/target/Target;, "Lcom/bumptech/glide/request/target/Target<TR;>;"
    const/4 v0, 0x0

    return v0
.end method
