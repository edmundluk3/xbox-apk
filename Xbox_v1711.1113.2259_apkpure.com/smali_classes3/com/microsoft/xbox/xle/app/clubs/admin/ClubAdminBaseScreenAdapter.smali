.class public abstract Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ClubAdminBaseScreenAdapter.java"


# instance fields
.field private final clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;

.field private final headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

.field protected final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;)V
    .locals 1
    .param p1, "clubViewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 23
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;

    .line 26
    const v0, 0x7f0e02b2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    .line 27
    const v0, 0x7f0e0a95

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 28
    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 33
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->updateLoadingIndicator(Z)V

    .line 34
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->getClubName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 35
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->getHeaderView()Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 36
    .local v0, "headerTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setMaxLines(I)V

    .line 37
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->getPreferredColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setBackgroundColor(I)V

    .line 39
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 40
    return-void
.end method
