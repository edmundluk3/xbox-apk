.class final enum Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;
.super Ljava/lang/Enum;
.source "ClubAdminMembersScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Filter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

.field public static final enum ALL:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

.field public static final enum MODERATORS:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 75
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->ALL:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    const-string v1, "MODERATORS"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->MODERATORS:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->ALL:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->MODERATORS:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 75
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    return-object v0
.end method
