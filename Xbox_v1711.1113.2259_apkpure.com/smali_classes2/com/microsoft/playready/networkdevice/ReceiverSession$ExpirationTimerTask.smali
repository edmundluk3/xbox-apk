.class Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
.super Ljava/util/TimerTask;
.source "ReceiverSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/networkdevice/ReceiverSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExpirationTimerTask"
.end annotation


# instance fields
.field private mExpirationTimer:Ljava/util/Timer;

.field private mNearExpirationListener:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/playready/networkdevice/ReceiverSession;


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/networkdevice/ReceiverSession;)V
    .locals 1

    .prologue
    .line 454
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->this$0:Lcom/microsoft/playready/networkdevice/ReceiverSession;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 455
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->mNearExpirationListener:Ljava/util/ArrayList;

    .line 456
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->mExpirationTimer:Ljava/util/Timer;

    .line 457
    return-void
.end method


# virtual methods
.method public declared-synchronized addReceiverNearExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;

    .prologue
    .line 467
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->mNearExpirationListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468
    monitor-exit p0

    return-void

    .line 467
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeReceiverNearExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;

    .prologue
    .line 473
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->mNearExpirationListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    monitor-exit p0

    return-void

    .line 473
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 479
    monitor-enter p0

    .line 481
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->mNearExpirationListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;

    .line 483
    .local v1, "listener":Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;
    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->this$0:Lcom/microsoft/playready/networkdevice/ReceiverSession;

    invoke-interface {v1, v2}, Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;->onExpirationThreshold(Lcom/microsoft/playready/networkdevice/IReceiverSession;)V

    goto :goto_0

    .line 485
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 486
    return-void
.end method

.method public declared-synchronized schedule(J)V
    .locals 1
    .param p1, "diff"    # J

    .prologue
    .line 461
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->mExpirationTimer:Ljava/util/Timer;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 462
    monitor-exit p0

    return-void

    .line 461
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
