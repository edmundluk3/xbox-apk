.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;
.super Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;
.source "ClubAdminRequestsScreenAdapter.java"


# instance fields
.field private final countAppearance:Landroid/text/style/TextAppearanceSpan;

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

.field private final title:Landroid/widget/TextView;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V
    .locals 6
    .param p1, "clubViewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;)V

    .line 36
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    .line 39
    const v3, 0x7f0e0297

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->title:Landroid/widget/TextView;

    .line 40
    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->title:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f08021e

    invoke-direct {v3, v4, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->countAppearance:Landroid/text/style/TextAppearanceSpan;

    .line 42
    const v3, 0x7f0e06bd

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 43
    .local v1, "noContent":Landroid/widget/TextView;
    const v3, 0x7f070164

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 45
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    new-instance v4, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;)V

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    .line 73
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    new-instance v4, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestHeaderItem;

    invoke-direct {v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestHeaderItem;-><init>()V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->add(Ljava/lang/Object;)V

    .line 75
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 77
    .local v0, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    const v3, 0x7f0e0298

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    .line 79
    .local v2, "requestList":Landroid/support/v7/widget/RecyclerView;
    new-instance v3, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    invoke-direct {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;-><init>(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;Landroid/support/v7/widget/LinearLayoutManager;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->addOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 84
    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 85
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    return-object v0
.end method

.method private setTitle()V
    .locals 7

    .prologue
    .line 111
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07020f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 112
    .local v1, "titleBase":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getNumOfRequests()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 114
    .local v2, "titleCount":Ljava/lang/String;
    new-instance v0, Landroid/text/SpannableString;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 115
    .local v0, "styledTitle":Landroid/text/SpannableString;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->countAppearance:Landroid/text/style/TextAppearanceSpan;

    .line 117
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    .line 118
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    const/16 v6, 0x21

    .line 115
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 121
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->title:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    return-void
.end method

.method private updateMissingListItems()V
    .locals 4

    .prologue
    .line 125
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getLoadedRequestItems()Ljava/util/List;

    move-result-object v0

    .line 126
    .local v0, "allItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v2

    .line 128
    .local v2, "oldItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->clear()V

    .line 129
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->addAll(Ljava/util/Collection;)V

    .line 131
    new-instance v3, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;

    invoke-direct {v3, v2, v0}, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v3}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v1

    .line 132
    .local v1, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-virtual {v1, v3}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 133
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->onStart()V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->setIncrementalLoader(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;)V

    .line 92
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->onStop()V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->setIncrementalLoader(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;)V

    .line 98
    return-void
.end method

.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->updateViewOverride()V

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->setTitle()V

    .line 106
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->updateMissingListItems()V

    .line 108
    :cond_0
    return-void
.end method
