.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2MusicTrackMediaItem.java"


# instance fields
.field public Duration:Ljava/lang/String;

.field private DurationInMMSS:Ljava/lang/String;

.field public IsExplicit:Z

.field public LabelOwner:Ljava/lang/String;

.field public TrackNumber:I

.field public ZuneId:Ljava/lang/String;

.field private generatedProvider:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation
.end field

.field private parentAlbum:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ParentAlbum"
    .end annotation
.end field

.field private primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PrimaryArtist"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 32
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 33
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->Duration:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->Duration:Ljava/lang/String;

    move-object v0, p1

    .line 34
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->IsExplicit:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->IsExplicit:Z

    move-object v0, p1

    .line 35
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->LabelOwner:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->LabelOwner:Ljava/lang/String;

    move-object v0, p1

    .line 36
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->parentAlbum:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->parentAlbum:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    move-object v0, p1

    .line 38
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->ZuneId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->ZuneId:Ljava/lang/String;

    .line 39
    check-cast p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .end local p1    # "source":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->TrackNumber:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->TrackNumber:I

    .line 41
    :cond_0
    const/16 v0, 0x3ef

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->setMediaItemTypeFromInt(I)V

    .line 42
    return-void
.end method


# virtual methods
.method public getAlbum()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->parentAlbum:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    return-object v0
.end method

.method public getAlbumCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->parentAlbum:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->parentAlbum:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->ID:Ljava/lang/String;

    goto :goto_0
.end method

.method public getAlbumName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->parentAlbum:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->parentAlbum:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->Name:Ljava/lang/String;

    .line 82
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getArtistCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->ID:Ljava/lang/String;

    .line 68
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->Name:Ljava/lang/String;

    .line 61
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getArtistZuneId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->ZuneId:Ljava/lang/String;

    .line 75
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 2

    .prologue
    .line 131
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->Duration:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->DurationInMMSS:Ljava/lang/String;

    .line 138
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->DurationInMMSS:Ljava/lang/String;

    return-object v0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->Duration:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->durationStringToSeconds(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getTimeStringMMSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->DurationInMMSS:Ljava/lang/String;

    goto :goto_0
.end method

.method public getFormattedDuration()Ljava/lang/String;
    .locals 5

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getDuration()Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "duration":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x4

    if-ge v3, v4, :cond_1

    .line 160
    .end local v0    # "duration":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 147
    .restart local v0    # "duration":Ljava/lang/String;
    :cond_1
    const/4 v2, -0x1

    .line 148
    .local v2, "substringStart":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    if-ge v1, v3, :cond_3

    .line 149
    move v2, v1

    .line 150
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x3a

    if-eq v3, v4, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x30

    if-ne v3, v4, :cond_3

    .line 148
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 157
    :cond_3
    const/4 v3, -0x1

    if-le v2, v3, :cond_0

    .line 158
    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLabelOwner()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->LabelOwner:Ljava/lang/String;

    return-object v0
.end method

.method public getMusicOffer(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicOfferInfo;
    .locals 1
    .param p1, "right"    # I

    .prologue
    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPreviewOfferId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProviders()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->generatedProvider:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->ID:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->ZuneId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getMusicProviders(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->generatedProvider:Ljava/util/ArrayList;

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->generatedProvider:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getStreamOfferId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubscriptionOfferId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTrackNumber()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->TrackNumber:I

    return v0
.end method

.method public getZuneId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->ZuneId:Ljava/lang/String;

    return-object v0
.end method

.method public setParentAlbum(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;)V
    .locals 0
    .param p1, "parent"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->parentAlbum:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    .line 46
    return-void
.end method

.method public setPrimaryArtist(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;)V
    .locals 0
    .param p1, "artist"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    .line 54
    return-void
.end method
