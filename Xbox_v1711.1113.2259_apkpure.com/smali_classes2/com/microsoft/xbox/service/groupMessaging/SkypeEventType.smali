.class public final enum Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;
.super Ljava/lang/Enum;
.source "SkypeEventType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

.field public static final enum ConsumptionHorizon:Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

.field public static final enum Service:Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;


# instance fields
.field private final type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    const-string v1, "ConsumptionHorizon"

    const-string v2, "308"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->ConsumptionHorizon:Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    .line 5
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    const-string v1, "Service"

    const-string v2, "230"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->Service:Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    .line 3
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->ConsumptionHorizon:Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->Service:Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->$VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput-object p3, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->type:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->$VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    return-object v0
.end method


# virtual methods
.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->type:Ljava/lang/String;

    return-object v0
.end method
