.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
.super Ljava/lang/Object;
.source "EDSV2ParentalRating.java"


# static fields
.field public static PEGI:Ljava/lang/String;


# instance fields
.field public LegacyRatingId:Ljava/lang/String;

.field public LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

.field public Rating:Ljava/lang/String;

.field public RatingDescriptors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field public RatingDisclaimers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;",
            ">;"
        }
    .end annotation
.end field

.field public RatingId:Ljava/lang/String;

.field public RatingMinimumAge:I

.field public RatingSystem:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const-string v0, "PEGI"

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->PEGI:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getUseAllImage()Z
    .locals 2

    .prologue
    .line 17
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->PEGI:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingSystem:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
