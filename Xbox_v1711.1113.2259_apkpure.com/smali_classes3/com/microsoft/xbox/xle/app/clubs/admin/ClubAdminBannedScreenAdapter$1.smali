.class Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter$1;
.super Ljava/lang/Object;
.source "ClubAdminBannedScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProfileClick(J)V
    .locals 3
    .param p1, "xuid"    # J

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public onUnbanClick(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->unban(J)V

    .line 64
    return-void
.end method
