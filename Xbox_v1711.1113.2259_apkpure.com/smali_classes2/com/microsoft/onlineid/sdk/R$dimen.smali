.class public final Lcom/microsoft/onlineid/sdk/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/onlineid/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final accountPickerMargin:I = 0x7f09018e

.field public static final buttonBorder:I = 0x7f0901c1

.field public static final buttonHeight:I = 0x7f0901c2

.field public static final marginFooter:I = 0x7f09036f

.field public static final marginLarge:I = 0x7f090370

.field public static final marginMedium:I = 0x7f090371

.field public static final marginSmall:I = 0x7f090372

.field public static final marginXLarge:I = 0x7f090373

.field public static final maxAccountPickerHeight:I = 0x7f090376

.field public static final maxAccountPickerWidth:I = 0x7f090377

.field public static final menuHeight:I = 0x7f090379

.field public static final menuMarginRight:I = 0x7f09037a

.field public static final menuMarginTopBottom:I = 0x7f09037b

.field public static final menuWidth:I = 0x7f09037c

.field public static final paddingPopupMessageContentMaxWidth:I = 0x7f09041e

.field public static final paddingPopupMessageLarge:I = 0x7f09041f

.field public static final paddingPopupMessageMedium:I = 0x7f090420

.field public static final paddingPopupMessageParagraph:I = 0x7f090421

.field public static final paddingPopupMessageSmall:I = 0x7f090422

.field public static final userTile:I = 0x7f090588


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
