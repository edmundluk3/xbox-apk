.class public Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;
.super Landroid/widget/LinearLayout;
.source "IconFontToggleButton.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private checked:Z

.field private checkedIcon:Ljava/lang/String;

.field private checkedText:Ljava/lang/String;

.field private iconTextView:Landroid/widget/TextView;

.field private labelTextView:Landroid/widget/TextView;

.field private uncheckedIcon:Ljava/lang/String;

.field private uncheckedText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method private applyCustomTypeface(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "typefaceSource"    # Ljava/lang/String;

    .prologue
    .line 62
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->labelTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 63
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 64
    .local v0, "tf":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->labelTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 66
    .end local v0    # "tf":Landroid/graphics/Typeface;
    :cond_0
    return-void
.end method

.method private initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 42
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 43
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f03014e

    invoke-virtual {v2, v3, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 44
    const v3, 0x7f0e070e

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->iconTextView:Landroid/widget/TextView;

    .line 45
    const v3, 0x7f0e070f

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->labelTextView:Landroid/widget/TextView;

    .line 47
    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->CustomTypeface:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 48
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "typeface":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 51
    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->IconFontToggleButton:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 52
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->checkedText:Ljava/lang/String;

    .line 53
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->uncheckedText:Ljava/lang/String;

    .line 54
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->checkedIcon:Ljava/lang/String;

    .line 55
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->uncheckedIcon:Ljava/lang/String;

    .line 56
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 58
    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->applyCustomTypeface(Landroid/content/Context;Ljava/lang/String;)V

    .line 59
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->checked:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 3
    .param p1, "checked"    # Z

    .prologue
    const/4 v2, 0x0

    .line 75
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->checked:Z

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->labelTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->labelTextView:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->checked:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->checkedText:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->labelTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->iconTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->iconTextView:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->checked:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->checkedIcon:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->iconTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 85
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->invalidate()V

    .line 86
    return-void

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->uncheckedText:Ljava/lang/String;

    goto :goto_0

    .line 82
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->uncheckedIcon:Ljava/lang/String;

    goto :goto_1
.end method

.method public setCheckedText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->checkedText:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public setUncheckedText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->uncheckedText:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->checked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setChecked(Z)V

    .line 91
    return-void

    .line 90
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
