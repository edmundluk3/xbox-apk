.class public final Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;
.super Ljava/lang/Object;
.source "TrendingBeamViewImpl_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final titleBarViewProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/ui/TitleBarView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/ui/TitleBarView;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "titleBarViewProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xle/ui/TitleBarView;>;"
    .local p2, "presenterProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-boolean v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;->titleBarViewProvider:Ljavax/inject/Provider;

    .line 23
    sget-boolean v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/ui/TitleBarView;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;",
            ">;)",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "titleBarViewProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xle/ui/TitleBarView;>;"
    .local p1, "presenterProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "presenterProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->presenter:Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;

    .line 50
    return-void
.end method

.method public static injectTitleBarView(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/ui/TitleBarView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "titleBarViewProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xle/ui/TitleBarView;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/TitleBarView;

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->titleBarView:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    .line 45
    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;)V
    .locals 2
    .param p1, "instance"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;

    .prologue
    .line 35
    if-nez p1, :cond_0

    .line 36
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;->titleBarViewProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/TitleBarView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->titleBarView:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->presenter:Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;

    .line 40
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;->injectMembers(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;)V

    return-void
.end method
