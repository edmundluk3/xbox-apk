.class public Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "PeopleScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    const-string v0, "People"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 20
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 22
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onCreateContentView()V

    .line 24
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 25
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 29
    const v0, 0x7f0301b8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->setContentView(I)V

    .line 30
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method
