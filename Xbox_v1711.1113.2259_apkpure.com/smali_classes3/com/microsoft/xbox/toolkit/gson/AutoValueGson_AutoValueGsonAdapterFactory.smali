.class public final Lcom/microsoft/xbox/toolkit/gson/AutoValueGson_AutoValueGsonAdapterFactory;
.super Lcom/microsoft/xbox/toolkit/gson/AutoValueGsonAdapterFactory;
.source "AutoValueGson_AutoValueGsonAdapterFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/gson/AutoValueGsonAdapterFactory;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;
    .locals 2
    .param p1, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/Gson;",
            "Lcom/google/gson/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/gson/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 39
    .local p2, "type":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;"
    invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;

    move-result-object v0

    .line 40
    .local v0, "rawType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const-class v1, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    .line 325
    :goto_0
    return-object v1

    .line 42
    :cond_0
    const-class v1, Lcom/microsoft/xbox/data/service/messaging/SkypeError;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/messaging/SkypeError;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto :goto_0

    .line 44
    :cond_1
    const-class v1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 45
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto :goto_0

    .line 46
    :cond_2
    const-class v1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 47
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto :goto_0

    .line 48
    :cond_3
    const-class v1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 49
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto :goto_0

    .line 50
    :cond_4
    const-class v1, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 51
    invoke-static {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto :goto_0

    .line 52
    :cond_5
    const-class v1, Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 53
    invoke-static {p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto :goto_0

    .line 54
    :cond_6
    const-class v1, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 55
    invoke-static {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto :goto_0

    .line 56
    :cond_7
    const-class v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 57
    invoke-static {p1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto :goto_0

    .line 58
    :cond_8
    const-class v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 59
    invoke-static {p1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto :goto_0

    .line 60
    :cond_9
    const-class v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 61
    invoke-static {p1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 62
    :cond_a
    const-class v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 63
    invoke-static {p1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 64
    :cond_b
    const-class v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 65
    invoke-static {p1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 66
    :cond_c
    const-class v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 67
    invoke-static {p1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 68
    :cond_d
    const-class v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 69
    invoke-static {p1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 70
    :cond_e
    const-class v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 71
    invoke-static {p1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 72
    :cond_f
    const-class v1, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 73
    invoke-static {p1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 74
    :cond_10
    const-class v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PostLocator;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 75
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PostLocator;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 76
    :cond_11
    const-class v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 77
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 78
    :cond_12
    const-class v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 79
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 80
    :cond_13
    const-class v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 81
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 82
    :cond_14
    const-class v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 83
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 84
    :cond_15
    const-class v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 85
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 86
    :cond_16
    const-class v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 87
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 88
    :cond_17
    const-class v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 89
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 90
    :cond_18
    const-class v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 91
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 92
    :cond_19
    const-class v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 93
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 94
    :cond_1a
    const-class v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 95
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 96
    :cond_1b
    const-class v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 97
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 98
    :cond_1c
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 99
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 100
    :cond_1d
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 101
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 102
    :cond_1e
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ReserveClubNameRequest;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 103
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ReserveClubNameRequest;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 104
    :cond_1f
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$CreateClubRequest;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 105
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$CreateClubRequest;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 106
    :cond_20
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 107
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 108
    :cond_21
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 109
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 110
    :cond_22
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 111
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 112
    :cond_23
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotification;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 113
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotification;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 114
    :cond_24
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 115
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 116
    :cond_25
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_26

    .line 117
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 118
    :cond_26
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 119
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 120
    :cond_27
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 121
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 122
    :cond_28
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 123
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 124
    :cond_29
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 125
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 126
    :cond_2a
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 127
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 128
    :cond_2b
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 129
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 130
    :cond_2c
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 131
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 132
    :cond_2d
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 133
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 134
    :cond_2e
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 135
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 136
    :cond_2f
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_30

    .line 137
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 138
    :cond_30
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 139
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 140
    :cond_31
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 141
    invoke-static {p1, p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->typeAdapter(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 142
    :cond_32
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_33

    .line 143
    invoke-static {p1, p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->typeAdapter(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 144
    :cond_33
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 145
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 146
    :cond_34
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_35

    .line 147
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 148
    :cond_35
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_36

    .line 149
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 150
    :cond_36
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_37

    .line 151
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 152
    :cond_37
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_38

    .line 153
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 154
    :cond_38
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_39

    .line 155
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 156
    :cond_39
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 157
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 158
    :cond_3a
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 159
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 160
    :cond_3b
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSearchResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3c

    .line 161
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSearchResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 162
    :cond_3c
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItemsResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 163
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItemsResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 164
    :cond_3d
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 165
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 166
    :cond_3e
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 167
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 168
    :cond_3f
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_40

    .line 169
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 170
    :cond_40
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_41

    .line 171
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 172
    :cond_41
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_42

    .line 173
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 174
    :cond_42
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_43

    .line 175
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 176
    :cond_43
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_44

    .line 177
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 178
    :cond_44
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_45

    .line 179
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 180
    :cond_45
    const-class v1, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_46

    .line 181
    invoke-static {p1}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 182
    :cond_46
    const-class v1, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_47

    .line 183
    invoke-static {p1}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 184
    :cond_47
    const-class v1, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_48

    .line 185
    invoke-static {p1}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 186
    :cond_48
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_49

    .line 187
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 188
    :cond_49
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 189
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 190
    :cond_4a
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_4b

    .line 191
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 192
    :cond_4b
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_4c

    .line 193
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 194
    :cond_4c
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_4d

    .line 195
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 196
    :cond_4d
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_4e

    .line 197
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 198
    :cond_4e
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_4f

    .line 199
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 200
    :cond_4f
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_50

    .line 201
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 202
    :cond_50
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_51

    .line 203
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 204
    :cond_51
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_52

    .line 205
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 206
    :cond_52
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_53

    .line 207
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 208
    :cond_53
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_54

    .line 209
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 210
    :cond_54
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_55

    .line 211
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 212
    :cond_55
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_56

    .line 213
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 214
    :cond_56
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_57

    .line 215
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 216
    :cond_57
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_58

    .line 217
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 218
    :cond_58
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_59

    .line 219
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 220
    :cond_59
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_5a

    .line 221
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 222
    :cond_5a
    const-class v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ErrorResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 223
    invoke-static {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ErrorResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 224
    :cond_5b
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_5c

    .line 225
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 226
    :cond_5c
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_5d

    .line 227
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 228
    :cond_5d
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerQueryResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_5e

    .line 229
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerQueryResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 230
    :cond_5e
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_5f

    .line 231
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 232
    :cond_5f
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_60

    .line 233
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 234
    :cond_60
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_61

    .line 235
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 236
    :cond_61
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_62

    .line 237
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 238
    :cond_62
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgRoleInfo;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_63

    .line 239
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgRoleInfo;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 240
    :cond_63
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_64

    .line 241
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 242
    :cond_64
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ConfirmedRole;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_65

    .line 243
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ConfirmedRole;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 244
    :cond_65
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_66

    .line 245
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 246
    :cond_66
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_67

    .line 247
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 248
    :cond_67
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerServers;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_68

    .line 249
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerServers;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 250
    :cond_68
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerServerCloudCompute;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_69

    .line 251
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerServerCloudCompute;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 252
    :cond_69
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$CloudComputeProperties;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_6a

    .line 253
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$CloudComputeProperties;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 254
    :cond_6a
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionConstants;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_6b

    .line 255
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionConstants;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 256
    :cond_6b
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionConstantsSystem;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_6c

    .line 257
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionConstantsSystem;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 258
    :cond_6c
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$CloudComputePackage;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_6d

    .line 259
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$CloudComputePackage;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 260
    :cond_6d
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_6e

    .line 261
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 262
    :cond_6e
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_6f

    .line 263
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 264
    :cond_6f
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_70

    .line 265
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 266
    :cond_70
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_71

    .line 267
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 268
    :cond_71
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberProperties;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_72

    .line 269
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberProperties;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 270
    :cond_72
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_73

    .line 271
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 272
    :cond_73
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_74

    .line 273
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 274
    :cond_74
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_75

    .line 275
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 276
    :cond_75
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_76

    .line 277
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 278
    :cond_76
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_77

    .line 279
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 280
    :cond_77
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_78

    .line 281
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 282
    :cond_78
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_79

    .line 283
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 284
    :cond_79
    const-class v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_7a

    .line 285
    invoke-static {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 286
    :cond_7a
    const-class v1, Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_7b

    .line 287
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 288
    :cond_7b
    const-class v1, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_7c

    .line 289
    invoke-static {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 290
    :cond_7c
    const-class v1, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubResponse;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_7d

    .line 291
    invoke-static {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubResponse;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 292
    :cond_7d
    const-class v1, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_7e

    .line 293
    invoke-static {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 294
    :cond_7e
    const-class v1, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_7f

    .line 295
    invoke-static {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 296
    :cond_7f
    const-class v1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_80

    .line 297
    invoke-static {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 298
    :cond_80
    const-class v1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_81

    .line 299
    invoke-static {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 300
    :cond_81
    const-class v1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdEvent;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_82

    .line 301
    invoke-static {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdEvent;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 302
    :cond_82
    const-class v1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_83

    .line 303
    invoke-static {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 304
    :cond_83
    const-class v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_84

    .line 305
    invoke-static {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 306
    :cond_84
    const-class v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_85

    .line 307
    invoke-static {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 308
    :cond_85
    const-class v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_86

    .line 309
    invoke-static {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 310
    :cond_86
    const-class v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_87

    .line 311
    invoke-static {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 312
    :cond_87
    const-class v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_88

    .line 313
    invoke-static {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 314
    :cond_88
    const-class v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_89

    .line 315
    invoke-static {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 316
    :cond_89
    const-class v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_8a

    .line 317
    invoke-static {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 318
    :cond_8a
    const-class v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_8b

    .line 319
    invoke-static {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 320
    :cond_8b
    const-class v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_8c

    .line 321
    invoke-static {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 322
    :cond_8c
    const-class v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_8d

    .line 323
    invoke-static {p1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;

    move-result-object v1

    goto/16 :goto_0

    .line 325
    :cond_8d
    const/4 v1, 0x0

    goto/16 :goto_0
.end method
