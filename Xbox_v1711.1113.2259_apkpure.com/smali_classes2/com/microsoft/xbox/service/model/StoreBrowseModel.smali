.class public Lcom/microsoft/xbox/service/model/StoreBrowseModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "StoreBrowseModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;,
        Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
        ">;"
    }
.end annotation


# static fields
.field public static final MAX_BATCH_ITEMS:I = 0xa

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private goldLoungeItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;"
        }
    .end annotation
.end field

.field private lastRefreshgoldLoungeData:Ljava/util/Date;

.field private result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

.field private searchFilter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field private searchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/StoreBrowseType;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)V
    .locals 0
    .param p1, "searchType"    # Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .param p2, "searchFilter"    # Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->searchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    .line 47
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->searchFilter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 48
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/StoreBrowseModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->onGoldLoungeDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private onGoldLoungeDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p1, "result2":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;>;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->TAG:Ljava/lang/String;

    const-string v1, "onGoldLoungeDataCompleted "

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 128
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->goldLoungeItems:Ljava/util/List;

    .line 129
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->lastRefreshgoldLoungeData:Ljava/util/Date;

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->TAG:Ljava/lang/String;

    const-string v1, "Cannot get gold lounge data"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getBrowseFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->searchFilter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    return-object v0
.end method

.method public getBrowseType()Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->searchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    return-object v0
.end method

.method public getGoldLoungeResult()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->goldLoungeItems:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    return-object v0
.end method

.method public load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;-><init>(Lcom/microsoft/xbox/service/model/StoreBrowseModel;Z)V

    .line 82
    .local v0, "runner":Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;
    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public loadGoldLoungeData(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 91
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 92
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->lastRefreshgoldLoungeData:Ljava/util/Date;

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;

    const/4 v0, 0x0

    invoke-direct {v6, p0, v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;-><init>(Lcom/microsoft/xbox/service/model/StoreBrowseModel;Lcom/microsoft/xbox/service/model/StoreBrowseModel$1;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadMore()Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 86
    new-instance v0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;-><init>(Lcom/microsoft/xbox/service/model/StoreBrowseModel;Z)V

    .line 87
    .local v0, "runner":Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;
    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->clearObservers()V

    .line 52
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->lastRefreshTime:Ljava/util/Date;

    .line 53
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->lastRefreshgoldLoungeData:Ljava/util/Date;

    .line 54
    return-void
.end method

.method public shouldRefresh()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->lastRefreshTime:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->shouldRefresh(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldRefreshGoldLoungeData()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->goldLoungeItems:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->goldLoungeItems:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->lastRefreshgoldLoungeData:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->shouldRefresh(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;Z)V
    .locals 5
    .param p2, "loadMore"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 97
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 99
    if-eqz p2, :cond_0

    .line 100
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-eqz v2, :cond_0

    .line 106
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->setItems(Ljava/util/ArrayList;)V

    .line 110
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 111
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .line 112
    .local v0, "edsv2Result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz p2, :cond_4

    .line 113
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 114
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 115
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 123
    .end local v0    # "edsv2Result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .end local v1    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_1
    :goto_1
    return-void

    .line 97
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 117
    .restart local v0    # "edsv2Result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .restart local v1    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_3
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    goto :goto_1

    .line 120
    .end local v1    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_4
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    goto :goto_1
.end method
