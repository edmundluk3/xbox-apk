.class public final Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes;
.super Ljava/lang/Object;
.source "ClubModerationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubBatchDeleteReportRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;,
        Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;,
        Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItemsResponse;,
        Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;,
        Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Type should not be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
