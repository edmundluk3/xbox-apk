.class public Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "FutureShowtimesScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FutureShowtimesScreenViewModel"


# instance fields
.field private isLoadingFutureShowtimesData:Z

.field private loadFutureShowtimesDataTask:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 20
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 26
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getFutureShowtimesAdapter(Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 27
    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->isLoadingFutureShowtimesData:Z

    return p1
.end method

.method private hasFutureShowtimesData()Z
    .locals 2

    .prologue
    .line 88
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getAirings()Ljava/util/ArrayList;

    move-result-object v0

    .line 89
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAirings()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getAirings()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 60
    const v0, 0x7f070683

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->isLoadingFutureShowtimesData:Z

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->loadFutureShowtimesDataTask:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->load(Z)V

    .line 50
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->load(Z)V

    .line 51
    return-void
.end method

.method public onLoadFutureShowtimesDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 68
    const-string v0, "FutureShowtimesScreenViewModel"

    const-string v1, "onLoadFutureShowtimesData Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->isLoadingFutureShowtimesData:Z

    .line 70
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 84
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->updateAdapter()V

    .line 85
    return-void

    .line 74
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->hasFutureShowtimesData()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 78
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 79
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getFutureShowtimesAdapter(Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 37
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->loadFutureShowtimesDataTask:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;

    .line 32
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->loadFutureShowtimesDataTask:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->loadFutureShowtimesDataTask:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->cancel()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->loadFutureShowtimesDataTask:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;

    .line 45
    :cond_0
    return-void
.end method
