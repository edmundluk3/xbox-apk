.class public Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer$ActivityAlertSummaryResult;
.super Ljava/lang/Object;
.source "AlertActivitySummaryResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActivityAlertSummaryResult"
.end annotation


# instance fields
.field public commentCount:I

.field public lastSeenAlertId:Ljava/lang/String;

.field public likeCount:I

.field public newAlertCount:I

.field public shareCount:I

.field public xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer$ActivityAlertSummaryResult;
    .locals 1
    .param p0, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 18
    const-class v0, Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer$ActivityAlertSummaryResult;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer$ActivityAlertSummaryResult;

    return-object v0
.end method
