.class final synthetic Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;

.field private final arg$2:Z

.field private final arg$3:Lcom/microsoft/xbox/service/model/FollowersData;

.field private final arg$4:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;ZLcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;->arg$1:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;

    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;->arg$2:Z

    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;->arg$3:Lcom/microsoft/xbox/service/model/FollowersData;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;->arg$4:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;ZLcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;)Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;ZLcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;)V

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;->arg$1:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;->arg$2:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;->arg$3:Lcom/microsoft/xbox/service/model/FollowersData;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;->arg$4:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->lambda$bindTo$0(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;ZLcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;Landroid/view/View;)V

    return-void
.end method
