.class public Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
.super Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
.source "EnforcementViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EnforcementParameters"
.end annotation


# instance fields
.field private activityFeedFeedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

.field private clubChatModerationUri:Ljava/lang/String;

.field private displayName:Ljava/lang/String;

.field private evidenceId:Ljava/lang/String;

.field private feedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field private isRealNameVisible:Z

.field private sessionReference:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

.field private shouldReportToClubModerator:Z

.field private targetId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;)V
    .locals 7
    .param p1, "feedbackContext"    # Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "evidenceId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "targetId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "displayName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "shouldReportToClubModerator"    # Z
    .param p6, "sessionReference"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 398
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;-><init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 399
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 401
    iput-object p6, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->sessionReference:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 402
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 7
    .param p1, "feedbackContext"    # Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "evidenceId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "targetId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "displayName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "shouldReportToClubModerator"    # Z
    .param p6, "clubChatModerationUri"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 410
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;-><init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 411
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 413
    iput-object p6, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->clubChatModerationUri:Ljava/lang/String;

    .line 414
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "feedbackContext"    # Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "evidenceId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "targetId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "displayName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "isRealNameVisible"    # Z
    .param p6, "shouldReportToClubModerator"    # Z

    .prologue
    .line 378
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 379
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 380
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 381
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 382
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 384
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->targetId:Ljava/lang/String;

    .line 385
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->displayName:Ljava/lang/String;

    .line 386
    iput-boolean p5, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->isRealNameVisible:Z

    .line 387
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->evidenceId:Ljava/lang/String;

    .line 388
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->feedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 389
    iput-boolean p6, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->shouldReportToClubModerator:Z

    .line 390
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->feedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->shouldReportToClubModerator:Z

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->targetId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->activityFeedFeedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->isRealNameVisible:Z

    return v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->evidenceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->clubChatModerationUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->sessionReference:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    return-object v0
.end method


# virtual methods
.method public trySetActivityFeedContextFromScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 417
    instance-of v0, p1, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedScreen;

    if-eqz v0, :cond_2

    .line 418
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;->ActivityFeed:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->activityFeedFeedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    .line 424
    :cond_1
    :goto_0
    return-void

    .line 419
    :cond_2
    instance-of v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;

    if-eqz v0, :cond_3

    .line 420
    sget-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;->Club:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->activityFeedFeedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    goto :goto_0

    .line 421
    :cond_3
    instance-of v0, p1, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    if-eqz v0, :cond_1

    .line 422
    sget-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;->GameHub:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->activityFeedFeedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    goto :goto_0
.end method
