.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;
.super Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;
.source "ClubAdminMembersScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;


# static fields
.field private static final SERVICE_SEARCH_DELAY_MS:J = 0x1f4L


# instance fields
.field private filterOptionAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final filterSpinner:Landroid/widget/Spinner;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

.field private final membersList:Landroid/support/v7/widget/RecyclerView;

.field private final searchClearButton:Landroid/widget/Button;

.field private final searchInput:Landroid/widget/EditText;

.field private final tooManyWarningText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)V
    .locals 4
    .param p1, "clubViewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;)V

    .line 50
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    .line 53
    const v1, 0x7f0e06bd

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 54
    .local v0, "noContent":Landroid/widget/TextView;
    const v1, 0x7f070b62

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 56
    const v1, 0x7f0e0274

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->searchInput:Landroid/widget/EditText;

    .line 57
    const v1, 0x7f0e0275

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->searchClearButton:Landroid/widget/Button;

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->searchClearButton:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const v1, 0x7f0e0278

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->membersList:Landroid/support/v7/widget/RecyclerView;

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->membersList:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 63
    const v1, 0x7f0e0276

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 73
    const v1, 0x7f0e0277

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->tooManyWarningText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 74
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->searchInput:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->filterOptionSelected(I)V

    .line 66
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 67
    .local v0, "item":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 68
    sget-object v1, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 70
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;
    .param p1, "changeEvent"    # Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p1}, Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;->text()Ljava/lang/CharSequence;

    move-result-object v0

    .line 91
    .local v0, "text":Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->setSearchTerm(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->setCurrentSearchTerm(Ljava/lang/CharSequence;)V

    .line 96
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;
    .param p1, "changeEvent"    # Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 101
    invoke-virtual {p1}, Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;->text()Ljava/lang/CharSequence;

    move-result-object v0

    .line 102
    .local v0, "text":Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 103
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->searchClearButton:Landroid/widget/Button;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 104
    return-void

    .line 103
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateFilterCounts()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterOptionAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterOptionAdapter:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->getFilterOptions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 197
    return-void
.end method


# virtual methods
.method public ban(J)V
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 171
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 172
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->updateLoadingIndicator(Z)V

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->ban(J)V

    .line 174
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->updateFilterCounts()V

    .line 175
    return-void
.end method

.method public demoteFromModerator(J)V
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 163
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 164
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->updateLoadingIndicator(Z)V

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->demoteFromModerator(J)V

    .line 166
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->updateFilterCounts()V

    .line 167
    return-void
.end method

.method public onProfileClick(J)V
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 149
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method public onStart()V
    .locals 7

    .prologue
    .line 78
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->onStart()V

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    const/4 v1, 0x2

    new-array v1, v1, [Lio/reactivex/disposables/Disposable;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->searchInput:Landroid/widget/EditText;

    .line 81
    invoke-static {v3}, Lcom/jakewharton/rxbinding2/widget/RxTextView;->textChangeEvents(Landroid/widget/TextView;)Lcom/jakewharton/rxbinding2/InitialValueObservable;

    move-result-object v3

    const-wide/16 v4, 0x1f4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 86
    invoke-virtual {v3, v4, v5, v6}, Lcom/jakewharton/rxbinding2/InitialValueObservable;->debounce(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v3

    .line 88
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 89
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->searchInput:Landroid/widget/EditText;

    .line 98
    invoke-static {v3}, Lcom/jakewharton/rxbinding2/widget/RxTextView;->textChangeEvents(Landroid/widget/TextView;)Lcom/jakewharton/rxbinding2/InitialValueObservable;

    move-result-object v3

    .line 99
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/jakewharton/rxbinding2/InitialValueObservable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 100
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    .line 80
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->addAll([Lio/reactivex/disposables/Disposable;)Z

    .line 106
    return-void
.end method

.method public promoteToModerator(J)V
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 155
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 156
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->updateLoadingIndicator(Z)V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->promoteToModerator(J)V

    .line 158
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->updateFilterCounts()V

    .line 159
    return-void
.end method

.method public remove(J)V
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 179
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 180
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->updateLoadingIndicator(Z)V

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->remove(J)V

    .line 182
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->updateFilterCounts()V

    .line 183
    return-void
.end method

.method public report(JLjava/lang/String;Z)V
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "gamerTag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "isRealNameVisible"    # Z

    .prologue
    .line 189
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 190
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->report(JLjava/lang/String;Z)V

    .line 192
    return-void
.end method

.method protected updateViewOverride()V
    .locals 7

    .prologue
    .line 110
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->updateViewOverride()V

    .line 112
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v3, v4, :cond_1

    .line 114
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    if-nez v3, :cond_0

    .line 115
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->isViewerOwner()Z

    move-result v4

    invoke-direct {v3, p0, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;Z)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    .line 116
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->membersList:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 119
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->searchInput:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->getSearchHint()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 121
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->tooManyWarningText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->showTooManyWarning()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 123
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->getFilteredData()Ljava/util/List;

    move-result-object v0

    .line 124
    .local v0, "allItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v2

    .line 126
    .local v2, "oldItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->clear()V

    .line 127
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->addAll(Ljava/util/Collection;)V

    .line 129
    new-instance v3, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;

    invoke-direct {v3, v2, v0}, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v3}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v1

    .line 130
    .local v1, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-virtual {v1, v3}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 132
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterOptionAdapter:Landroid/widget/ArrayAdapter;

    if-nez v3, :cond_2

    .line 133
    new-instance v3, Landroid/widget/ArrayAdapter;

    .line 134
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x1090008

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    .line 136
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->getFilterOptions()Ljava/util/List;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterOptionAdapter:Landroid/widget/ArrayAdapter;

    .line 137
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterOptionAdapter:Landroid/widget/ArrayAdapter;

    const v4, 0x7f03020a

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 138
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterOptionAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 139
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->getSelectedFilter()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 145
    .end local v0    # "allItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    .end local v1    # "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    .end local v2    # "oldItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    :cond_1
    :goto_0
    return-void

    .line 141
    .restart local v0    # "allItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    .restart local v1    # "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    .restart local v2    # "oldItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterOptionAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3}, Landroid/widget/ArrayAdapter;->clear()V

    .line 142
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->filterOptionAdapter:Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->getFilterOptions()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    goto :goto_0
.end method
