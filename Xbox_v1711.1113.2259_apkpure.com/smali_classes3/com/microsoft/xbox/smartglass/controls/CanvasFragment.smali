.class public Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;
.super Landroid/app/Fragment;
.source "CanvasFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;,
        Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;
    }
.end annotation


# static fields
.field public static IsSmartGlassStudioRunning:Z

.field static final V1:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

.field static final V2:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;


# instance fields
.field private _canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

.field private _componentsStarted:Z

.field private _htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

.field private final _listeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/controls/WebViewListener;",
            ">;"
        }
    .end annotation
.end field

.field private _packageName:Ljava/lang/String;

.field private _sessionListener:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;

.field private _url:Ljava/lang/String;

.field private _webListener:Lcom/microsoft/xbox/smartglass/controls/WebViewListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;-><init>(II)V

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->V1:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    .line 45
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    const/4 v1, 0x2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;-><init>(II)V

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->V2:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    .line 48
    sput-boolean v2, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->IsSmartGlassStudioRunning:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_listeners:Ljava/util/HashSet;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_componentsStarted:Z

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->cloneListeners()Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_componentsStarted:Z

    return v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->startAllComponents()V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Lcom/microsoft/xbox/smartglass/controls/CanvasState;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;
    .param p1, "x1"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->openTitleChannel(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    return-object v0
.end method

.method private cloneListeners()Ljava/util/HashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/controls/WebViewListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_listeners:Ljava/util/HashSet;

    monitor-enter v2

    .line 205
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_listeners:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 206
    .local v0, "clone":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/microsoft/xbox/smartglass/controls/WebViewListener;>;"
    monitor-exit v2

    .line 208
    return-object v0

    .line 206
    .end local v0    # "clone":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/microsoft/xbox/smartglass/controls/WebViewListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private openTitleChannel(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
    .locals 10
    .param p1, "activeTitle"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .prologue
    const/4 v5, 0x0

    .line 300
    iget-object v6, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v6, v6, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    monitor-enter v6

    .line 301
    :try_start_0
    iget-object v7, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v7, v7, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;

    .line 302
    .local v1, "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    iget-object v8, v1, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v8, v8, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    iget v9, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    if-ne v8, v9, :cond_0

    .line 303
    monitor-exit v6

    .line 344
    .end local v1    # "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :cond_1
    :goto_0
    return-void

    .line 306
    :cond_2
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    const/4 v4, 0x0

    .line 310
    .local v4, "permittedTitle":Z
    iget-object v6, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v6, v6, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->allowedTitleIds:[I

    array-length v7, v6

    :goto_1
    if-ge v5, v7, :cond_3

    aget v0, v6, v5

    .line 311
    .local v0, "allowedTitleId":I
    iget v8, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    if-ne v0, v8, :cond_4

    .line 312
    const/4 v4, 0x1

    .line 317
    .end local v0    # "allowedTitleId":I
    :cond_3
    sget-boolean v5, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->IsSmartGlassStudioRunning:Z

    if-nez v5, :cond_5

    if-nez v4, :cond_5

    .line 318
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v5

    const-string v6, "Failed to open channel for unauthorized title"

    sget-object v7, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Verbose:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    goto :goto_0

    .line 306
    .end local v4    # "permittedTitle":Z
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 310
    .restart local v0    # "allowedTitleId":I
    .restart local v4    # "permittedTitle":Z
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 322
    .end local v0    # "allowedTitleId":I
    :cond_5
    new-instance v3, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;

    invoke-direct {v3}, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;-><init>()V

    .line 323
    .local v3, "newChannel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    new-instance v5, Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v6, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    invoke-direct {v5, v6}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    iput-object v5, v3, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    .line 324
    iget-object v5, v3, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v5}, Lcom/microsoft/xbox/smartglass/MessageTarget;->isValid()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 327
    :try_start_2
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v5

    iget-object v6, v3, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/smartglass/SessionManager;->startChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 335
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v6, v5, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    monitor-enter v6

    .line 336
    :try_start_3
    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v7, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Full:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v5, v7, :cond_6

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v7, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Fill:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v5, v7, :cond_7

    .line 338
    :cond_6
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v5, v5, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v5, v7, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 342
    :goto_2
    monitor-exit v6

    goto :goto_0

    :catchall_1
    move-exception v5

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v5

    .line 328
    :catch_0
    move-exception v2

    .line 330
    .local v2, "ex":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Error:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    goto :goto_0

    .line 340
    .end local v2    # "ex":Ljava/lang/Exception;
    :cond_7
    :try_start_4
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v5, v5, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2
.end method

.method private startAllComponents()V
    .locals 4

    .prologue
    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->getVersion()Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->V2:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->isLessThan(Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    const-string v0, "Messaging"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/MessagingV1;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/MessagingV1;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 266
    const-string v0, "Media"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/MediaV1;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/MediaV1;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 267
    const-string v0, "Information"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/InformationV1;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/InformationV1;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 268
    const-string v0, "Input"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/InputV1;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/InputV1;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 269
    const-string v0, "Log"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/LogV1;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/LogV1;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 279
    :goto_0
    const-string v0, "Browser"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/Browser;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/Browser;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;Landroid/content/Context;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 280
    const-string v0, "ServiceProxy"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 281
    const-string v0, "Accelerometer"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/Accelerometer;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/Accelerometer;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 282
    const-string v0, "Gyroscope"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/Gyroscope;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/Gyroscope;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 283
    const-string v0, "Touch"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/Touch;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/Touch;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 284
    const-string v0, "Haptic"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/Haptic;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/Haptic;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;Landroid/content/Context;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 285
    const-string v0, "GamePad"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/GamePad;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/GamePad;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 286
    const-string v0, "GameDvr"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/GameDvr;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/GameDvr;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 288
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_componentsStarted:Z

    .line 289
    return-void

    .line 271
    :cond_0
    const-string v0, "Messaging"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/MessagingV2;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/MessagingV2;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 272
    const-string v0, "Media"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/MediaV2;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/MediaV2;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 273
    const-string v0, "Information"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/InformationV2;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/InformationV2;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 274
    const-string v0, "Input"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/InputV2;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/InputV2;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 275
    const-string v0, "Log"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/LogV2;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/LogV2;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    goto/16 :goto_0
.end method

.method private stopAllComponents()V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->clearComponents()V

    .line 294
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_componentsStarted:Z

    .line 296
    :cond_0
    return-void
.end method


# virtual methods
.method public addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V
    .locals 2
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "instance"    # Lcom/microsoft/xbox/smartglass/controls/WebComponent;

    .prologue
    .line 253
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Component className cannot be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 256
    :cond_1
    if-nez p2, :cond_2

    .line 257
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Component instance cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V

    .line 260
    return-void
.end method

.method public addListener(Lcom/microsoft/xbox/smartglass/controls/WebViewListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .prologue
    .line 186
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_listeners:Ljava/util/HashSet;

    monitor-enter v1

    .line 187
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 188
    monitor-exit v1

    .line 189
    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearCache()V
    .locals 3

    .prologue
    .line 149
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->clearCache(Z)V

    .line 152
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 153
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 154
    .local v0, "cookieManager":Landroid/webkit/CookieManager;
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 155
    return-void
.end method

.method public getAllowedTitleIds()[I
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->allowedTitleIds:[I

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->allowedTitleIds:[I

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    return-object v0
.end method

.method public getAllowedUrlPrefixes()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->allowedUrlPrefixes:[Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->allowedUrlPrefixes:[Ljava/lang/String;

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_url:Ljava/lang/String;

    return-object v0
.end method

.method public initialize(Ljava/lang/String;[Ljava/lang/String;[I)V
    .locals 9
    .param p1, "activityUrl"    # Ljava/lang/String;
    .param p2, "allowedUrlPrefixes"    # [Ljava/lang/String;
    .param p3, "allowedTitleIds"    # [I

    .prologue
    .line 218
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_sessionListener:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/smartglass/SessionManager;->addListener(Ljava/lang/Object;)V

    .line 220
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v4

    const-string v5, "Canvas.Initialize"

    sget-object v6, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, p1}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->stopAllComponents()V

    .line 225
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    if-eqz v4, :cond_2

    .line 226
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v5, v4, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    monitor-enter v5

    .line 227
    :try_start_0
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v4, v4, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;

    .line 228
    .local v3, "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    iget-object v6, v3, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    if-eqz v6, :cond_0

    iget-object v6, v3, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v6}, Lcom/microsoft/xbox/smartglass/MessageTarget;->isValid()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 229
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v6

    iget-object v7, v3, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/smartglass/SessionManager;->stopChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    goto :goto_0

    .line 232
    .end local v3    # "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235
    :cond_2
    new-instance v4, Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    invoke-direct {v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasState;-><init>()V

    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .line 236
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iput-object p2, v4, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->allowedUrlPrefixes:[Ljava/lang/String;

    .line 237
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iput-object p3, v4, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->allowedTitleIds:[I

    .line 239
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/smartglass/SessionManager;->getActiveTitles()Ljava/util/List;

    move-result-object v1

    .line 240
    .local v1, "activeTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/smartglass/ActiveTitleState;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 241
    .local v0, "activeTitle":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->openTitleChannel(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    goto :goto_1

    .line 244
    .end local v0    # "activeTitle":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    :cond_3
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasParameters;

    invoke-direct {v2, p1, p2, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasParameters;-><init>(Ljava/lang/String;[Ljava/lang/String;[I)V

    .line 246
    .local v2, "canvasParameters":Lorg/json/JSONObject;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v4

    const-string v5, "[Canvas.Initialize] %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/smartglass/controls/Json;->escapeJson(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Information:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    .line 247
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    const-string v5, "CanvasParameters"

    invoke-virtual {v4, v5, v2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->addAdditionalData(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 249
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->loadUrl(Ljava/lang/String;)V

    .line 250
    return-void
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_url:Ljava/lang/String;

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->loadUrl(Ljava/lang/String;)V

    .line 145
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 91
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 59
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 61
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_packageName:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 62
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "defaultPackageName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 65
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v3, "R_PACKAGE_NAME"

    .line 66
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .end local v2    # "defaultPackageName":Ljava/lang/String;
    :cond_0
    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_packageName:Ljava/lang/String;

    .line 70
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_packageName:Ljava/lang/String;

    const-string v4, "layout"

    const-string v5, "canvas"

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/ResourceFinder;->find(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 71
    .local v1, "canvasFragmentView":Landroid/view/View;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_packageName:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 72
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_packageName:Ljava/lang/String;

    .line 75
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_packageName:Ljava/lang/String;

    const-string v4, "id"

    const-string v5, "html_surface"

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/ResourceFinder;->find(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    iput-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    .line 76
    new-instance v3, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;

    invoke-direct {v3, p0, v6}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$1;)V

    iput-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_webListener:Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .line 77
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_webListener:Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->addListener(Lcom/microsoft/xbox/smartglass/controls/WebViewListener;)V

    .line 79
    new-instance v3, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;

    invoke-direct {v3, p0, v6}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$1;)V

    iput-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_sessionListener:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;

    .line 81
    return-object v1
.end method

.method public onDestroyView()V
    .locals 5

    .prologue
    .line 97
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_listeners:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 99
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_sessionListener:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/SessionManager;->removeListener(Ljava/lang/Object;)V

    .line 100
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_sessionListener:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    if-eqz v1, :cond_2

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v2, v1, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;

    .line 105
    .local v0, "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    iget-object v3, v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/MessageTarget;->isValid()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 106
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/smartglass/SessionManager;->stopChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    goto :goto_0

    .line 109
    .end local v0    # "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 123
    :catch_0
    move-exception v1

    .line 127
    :goto_1
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 128
    return-void

    .line 109
    :cond_1
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 111
    :try_start_4
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 114
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->stopAllComponents()V

    .line 115
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .line 117
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_webListener:Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->removeListener(Lcom/microsoft/xbox/smartglass/controls/WebViewListener;)V

    .line 118
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_webListener:Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->destroy()V

    .line 122
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_htmlSurface:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1
.end method

.method public onLowMemory()V
    .locals 0

    .prologue
    .line 135
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 136
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/smartglass/controls/WebViewListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .prologue
    .line 196
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_listeners:Ljava/util/HashSet;

    monitor-enter v1

    .line 197
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->_listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 198
    monitor-exit v1

    .line 199
    return-void

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
