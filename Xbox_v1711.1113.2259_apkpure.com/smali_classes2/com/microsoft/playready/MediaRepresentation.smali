.class public Lcom/microsoft/playready/MediaRepresentation;
.super Ljava/lang/Object;
.source "MediaRepresentation.java"


# instance fields
.field private mNativeClass:Lcom/microsoft/playready/Native_Class10;

.field mParentStream:Lcom/microsoft/playready/MediaStream;

.field private mRepIndex:I

.field private mShouldBeEnabled:Ljava/lang/Boolean;

.field private mStreamIndex:I


# direct methods
.method constructor <init>(Lcom/microsoft/playready/MediaStream;Lcom/microsoft/playready/Native_Class10;II)V
    .locals 1
    .param p1, "parentStream"    # Lcom/microsoft/playready/MediaStream;
    .param p2, "nativeClass"    # Lcom/microsoft/playready/Native_Class10;
    .param p3, "streamIndex"    # I
    .param p4, "repIndex"    # I

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/MediaRepresentation;->mShouldBeEnabled:Ljava/lang/Boolean;

    .line 59
    iput-object p2, p0, Lcom/microsoft/playready/MediaRepresentation;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    .line 60
    iput p3, p0, Lcom/microsoft/playready/MediaRepresentation;->mStreamIndex:I

    .line 61
    iput p4, p0, Lcom/microsoft/playready/MediaRepresentation;->mRepIndex:I

    .line 62
    iput-object p1, p0, Lcom/microsoft/playready/MediaRepresentation;->mParentStream:Lcom/microsoft/playready/MediaStream;

    .line 63
    invoke-virtual {p0}, Lcom/microsoft/playready/MediaRepresentation;->isCurrentlyEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/MediaRepresentation;->mShouldBeEnabled:Ljava/lang/Boolean;

    .line 64
    return-void
.end method


# virtual methods
.method enable(Z)V
    .locals 3
    .param p1, "isEnabled"    # Z

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/playready/MediaRepresentation;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/MediaRepresentation;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/MediaRepresentation;->mRepIndex:I

    invoke-virtual {v0, v1, v2, p1}, Lcom/microsoft/playready/Native_Class10;->method_51(IIZ)V

    .line 41
    return-void
.end method

.method public getBitRate()J
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/playready/MediaRepresentation;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/MediaRepresentation;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/MediaRepresentation;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_14(II)J

    move-result-wide v0

    return-wide v0
.end method

.method public getMediaInfo()Lcom/microsoft/playready/MediaInfo;
    .locals 4

    .prologue
    .line 50
    new-instance v0, Lcom/microsoft/playready/RepresentationMediaInfo;

    iget-object v1, p0, Lcom/microsoft/playready/MediaRepresentation;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v2, p0, Lcom/microsoft/playready/MediaRepresentation;->mStreamIndex:I

    iget v3, p0, Lcom/microsoft/playready/MediaRepresentation;->mRepIndex:I

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/RepresentationMediaInfo;-><init>(Lcom/microsoft/playready/Native_Class10;II)V

    return-object v0
.end method

.method public getParentStream()Lcom/microsoft/playready/MediaStream;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/playready/MediaRepresentation;->mParentStream:Lcom/microsoft/playready/MediaStream;

    return-object v0
.end method

.method public getQualityLevelIndex()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/microsoft/playready/MediaRepresentation;->mRepIndex:I

    return v0
.end method

.method public isCurrentlyEnabled()Z
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/playready/MediaRepresentation;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/MediaRepresentation;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/MediaRepresentation;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_15(II)Z

    move-result v0

    return v0
.end method

.method public setShouldBeEnabled(Z)V
    .locals 1
    .param p1, "shouldBeEnabled"    # Z

    .prologue
    .line 32
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/MediaRepresentation;->mShouldBeEnabled:Ljava/lang/Boolean;

    .line 33
    return-void
.end method

.method public shouldBeEnabled()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/playready/MediaRepresentation;->mShouldBeEnabled:Ljava/lang/Boolean;

    return-object v0
.end method
