.class public Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;
.super Lcom/microsoft/xbox/presentation/base/MviPresenter;
.source "PartyDetailsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/presentation/base/MviPresenter",
        "<",
        "Lcom/microsoft/xbox/presentation/party/PartyDetailsView;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final intentToStateTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;",
            ">;"
        }
    .end annotation
.end field

.field private final myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

.field private final partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)V
    .locals 1
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "myXuidProvider"    # Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;
    .param p3, "interactor"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p4, "navigator"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;
    .param p5, "partyChatRepository"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 56
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 57
    iput-object p5, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    .line 59
    invoke-static {p0, p1, p3, p4}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 118
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->stateReducer(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$initialSetup$14(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 148
    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logRender(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/logging/Loggable;)V

    return-void
.end method

.method static synthetic lambda$new$13(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "interactor"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p3, "navigator"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;
    .param p4, "intents"    # Lio/reactivex/Observable;

    .prologue
    .line 60
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p4, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 117
    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;)Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 60
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    return-object v0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$InviteIntent;)Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$InviteIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 69
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteAction;->INSTANCE:Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteAction;

    return-object v0
.end method

.method static synthetic lambda$null$10(Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$NavigateToUserProfileIntent;)V
    .locals 2
    .param p0, "navigator"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;
    .param p1, "intent"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$NavigateToUserProfileIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$NavigateToUserProfileIntent;->memberXuid()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->navigateToProfile(J)V

    return-void
.end method

.method static synthetic lambda$null$11(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p2, "navigator"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;
    .param p3, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 62
    const/16 v0, 0xa

    new-array v0, v0, [Lio/reactivex/ObservableSource;

    const/4 v1, 0x0

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 63
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 64
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 65
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 66
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->getInitialLoadTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$InviteIntent;

    .line 68
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 69
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 70
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 71
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->getInviteTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TextChatIntent;

    .line 73
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    .line 74
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 75
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 76
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 77
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$LeavePartyIntent;

    .line 79
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$12;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 80
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 81
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 82
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 83
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 84
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->getLeavePartyTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyMuteIntent;

    .line 86
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$14;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 87
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 88
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 89
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->getMutePartyTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    .line 91
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$15;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 92
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 93
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 94
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->getToggleJoinabilityTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;

    .line 96
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$16;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 97
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 98
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 99
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->getToggleAllowBroadcastTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$KickMemberIntent;

    .line 101
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$17;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 102
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 103
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 104
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->getKickMemberTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$ToggleMemberMuteIntent;

    .line 106
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$18;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 107
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 108
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 109
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->getToggleMemberMuteTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$NavigateToUserProfileIntent;

    .line 111
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    .line 112
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$19;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 113
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 114
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$20;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 115
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 62
    invoke-static {v0}, Lio/reactivex/Observable;->mergeArray([Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$12(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "interactor"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p3, "navigator"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;
    .param p4, "i"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {p4}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 61
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1, p2, p3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 62
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 60
    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TextChatIntent;)V
    .locals 0
    .param p0, "navigator"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;
    .param p1, "intent"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TextChatIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->navigateToTextChat()V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$LeavePartyIntent;)Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$LeavePartyIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 80
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;->INSTANCE:Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;

    return-object v0
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;)V
    .locals 0
    .param p0, "navigator"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;
    .param p1, "intent"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->navigateBack()V

    return-void
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyMuteIntent;)Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyMuteIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 87
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;->INSTANCE:Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    return-object v0
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;)Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityAction;->INSTANCE:Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityAction;

    return-object v0
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;)Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;->allow()Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;->with(Z)Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$8(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$KickMemberIntent;)Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;
    .locals 2
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$KickMemberIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$KickMemberIntent;->memberXuid()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;->with(J)Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$ToggleMemberMuteIntent;)Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;
    .locals 2
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$ToggleMemberMuteIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$ToggleMemberMuteIntent;->memberXuid()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;->with(Ljava/lang/String;)Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;

    move-result-object v0

    return-object v0
.end method

.method private stateReducer(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    .locals 7
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 121
    instance-of v4, p2, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;

    if-eqz v4, :cond_1

    move-object v0, p2

    .line 122
    check-cast v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;

    .line 123
    .local v0, "initialLoadResult":Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;->memberStream()Lio/reactivex/Observable;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->withStream(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;Lio/reactivex/Observable;)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    move-result-object p1

    .line 137
    .end local v0    # "initialLoadResult":Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    :cond_0
    :goto_0
    return-object p1

    .line 124
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    :cond_1
    instance-of v4, p2, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;

    if-eqz v4, :cond_0

    move-object v2, p2

    .line 125
    check-cast v2, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;

    .line 127
    .local v2, "partyStateResult":Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;
    const/4 v3, 0x0

    .line 128
    .local v3, "userAllowsBroadcasting":Z
    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->getRoster()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 129
    .local v1, "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v6}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 130
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isAllowedInBroadcast()Z

    move-result v3

    goto :goto_1

    .line 134
    .end local v1    # "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    :cond_3
    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v4

    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    invoke-static {p1, v4, v5, v3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->withContent(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;Z)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method protected initialSetup()V
    .locals 3

    .prologue
    .line 142
    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    move-result-object v1

    .line 143
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->bindViewIntents(Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 144
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 145
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 146
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 147
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 148
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 149
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 151
    .local v0, "viewStates":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->bindViewState(Lio/reactivex/Observable;Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;)V

    .line 152
    return-void
.end method
