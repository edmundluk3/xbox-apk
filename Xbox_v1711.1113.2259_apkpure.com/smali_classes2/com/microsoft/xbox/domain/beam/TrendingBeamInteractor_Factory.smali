.class public final Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor_Factory;
.super Ljava/lang/Object;
.source "TrendingBeamInteractor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final repositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/beam/BeamRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/beam/BeamRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p1, "repositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/beam/BeamRepository;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget-boolean v0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor_Factory;->repositoryProvider:Ljavax/inject/Provider;

    .line 18
    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/beam/BeamRepository;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "repositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/beam/BeamRepository;>;"
    new-instance v0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor_Factory;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;
    .locals 2

    .prologue
    .line 22
    new-instance v1, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;

    iget-object v0, p0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor_Factory;->repositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/beam/BeamRepository;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;-><init>(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;)V

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor_Factory;->get()Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;

    move-result-object v0

    return-object v0
.end method
