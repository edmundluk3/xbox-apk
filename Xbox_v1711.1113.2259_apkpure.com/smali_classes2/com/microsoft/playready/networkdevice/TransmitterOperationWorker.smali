.class Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;
.super Ljava/lang/Object;
.source "TransmitterOperationWorker.java"

# interfaces
.implements Ljava/util/concurrent/Callable;
.implements Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$1;,
        Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<TV;>;",
        "Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;"
    }
.end annotation


# instance fields
.field private final mArgument:Ljava/lang/Object;

.field private mContentParser:Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

.field private mErrorCode:I

.field private mEvent:Ljava/lang/Object;

.field private mIsComplete:Z

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

.field private mParentObject:Lcom/microsoft/playready/networkdevice/TransmitterContent;

.field private final mTaskType:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;JLcom/microsoft/playready/networkdevice/TransmitterInfo;Ljava/lang/Object;)V
    .locals 2
    .param p1, "taskType"    # Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;
    .param p2, "handleUPnP"    # J
    .param p4, "transmitter"    # Lcom/microsoft/playready/networkdevice/TransmitterInfo;
    .param p5, "argument"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<TV;>;"
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mParentObject:Lcom/microsoft/playready/networkdevice/TransmitterContent;

    .line 45
    iput-object p5, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mArgument:Ljava/lang/Object;

    .line 46
    iput v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mErrorCode:I

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mEvent:Ljava/lang/Object;

    .line 48
    iput-boolean v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mIsComplete:Z

    .line 49
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mTaskType:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;

    .line 50
    new-instance v0, Lcom/microsoft/playready/networkdevice/Native_Class7;

    invoke-virtual {p4}, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->getUniversalDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p2, p3, v1}, Lcom/microsoft/playready/networkdevice/Native_Class7;-><init>(JLjava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mListeners:Ljava/util/List;

    .line 53
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterContentParser;

    invoke-direct {v0}, Lcom/microsoft/playready/networkdevice/TransmitterContentParser;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mContentParser:Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

    .line 55
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

    invoke-virtual {v0, p0}, Lcom/microsoft/playready/networkdevice/Native_Class7;->setHandler1(Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;)V

    .line 56
    return-void
.end method


# virtual methods
.method addOperationListener(Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;

    .prologue
    .line 73
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<TV;>;"
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mListeners:Ljava/util/List;

    monitor-enter v1

    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    iget-boolean v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mIsComplete:Z

    if-eqz v0, :cond_0

    .line 79
    iget v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mErrorCode:I

    invoke-interface {p1, v0}, Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;->onComplete(I)V

    .line 81
    :cond_0
    monitor-exit v1

    .line 82
    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public call()Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<TV;>;"
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 96
    const/4 v1, 0x0

    .line 97
    .local v1, "result":Ljava/lang/Object;, "TV;"
    const/4 v3, 0x0

    .line 98
    .local v3, "val":Ljava/lang/Object;
    const/4 v2, 0x1

    .line 100
    .local v2, "succeeded":Z
    iput v8, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mErrorCode:I

    .line 101
    iput-boolean v8, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mIsComplete:Z

    .line 102
    new-instance v4, Ljava/lang/Object;

    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    iput-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mEvent:Ljava/lang/Object;

    .line 109
    sget-object v4, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$1;->$SwitchMap$com$microsoft$playready$networkdevice$TransmitterOperationWorker$TransmitterTaskType:[I

    iget-object v5, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mTaskType:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;

    invoke-virtual {v5}, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 140
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Unsupported task type: %d"

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mTaskType:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 112
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

    invoke-virtual {v4}, Lcom/microsoft/playready/networkdevice/Native_Class7;->method_4()Z

    move-result v2

    .line 146
    :goto_0
    if-nez v2, :cond_0

    .line 148
    iput-boolean v7, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mIsComplete:Z

    .line 149
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "TransmitterOperationException"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4

    .line 116
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mArgument:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Lcom/microsoft/playready/networkdevice/Native_Class7;->method_5(I)Z

    move-result v2

    .line 117
    goto :goto_0

    .line 120
    :pswitch_2
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

    invoke-virtual {v4}, Lcom/microsoft/playready/networkdevice/Native_Class7;->method_6()Z

    move-result v2

    .line 121
    goto :goto_0

    .line 124
    :pswitch_3
    iget-object v5, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mArgument:Ljava/lang/Object;

    check-cast v4, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;

    invoke-virtual {v5, v4}, Lcom/microsoft/playready/networkdevice/Native_Class7;->method_7(Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;)Z

    move-result v2

    .line 125
    goto :goto_0

    .line 128
    :pswitch_4
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

    invoke-virtual {v4}, Lcom/microsoft/playready/networkdevice/Native_Class7;->method_8()Z

    move-result v2

    .line 129
    goto :goto_0

    .line 132
    :pswitch_5
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

    invoke-virtual {v4}, Lcom/microsoft/playready/networkdevice/Native_Class7;->method_9()Z

    move-result v2

    .line 133
    goto :goto_0

    .line 136
    :pswitch_6
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

    invoke-virtual {v4}, Lcom/microsoft/playready/networkdevice/Native_Class7;->method_10()Z

    move-result v2

    .line 137
    goto :goto_0

    .line 155
    :cond_0
    iget-object v5, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mEvent:Ljava/lang/Object;

    monitor-enter v5

    .line 157
    :try_start_0
    iget-boolean v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mIsComplete:Z

    if-nez v4, :cond_1

    .line 159
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mEvent:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V

    .line 161
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

    invoke-virtual {v4, v8}, Lcom/microsoft/playready/networkdevice/Native_Class7;->getResult(I)Ljava/lang/Object;

    move-result-object v3

    .line 168
    sget-object v4, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$1;->$SwitchMap$com$microsoft$playready$networkdevice$TransmitterOperationWorker$TransmitterTaskType:[I

    iget-object v5, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mTaskType:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;

    invoke-virtual {v5}, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    :cond_2
    move-object v1, v3

    .line 188
    .end local v3    # "val":Ljava/lang/Object;
    .local v1, "result":Ljava/lang/Object;, "TV;"
    .local v1, "val":Ljava/lang/Object;
    :goto_1
    iget v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mErrorCode:I

    if-eqz v4, :cond_3

    .line 190
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "TransmitterOperationException: %d"

    new-array v6, v7, [Ljava/lang/Object;

    iget v7, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mErrorCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4

    .line 161
    .restart local v3    # "val":Ljava/lang/Object;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :pswitch_7
    move-object v0, v3

    .line 174
    check-cast v0, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass4;

    .line 175
    .local v0, "browseInfo":Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass4;
    iget v4, v0, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass4;->mField1:I

    iput v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mErrorCode:I

    .line 176
    iget v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mErrorCode:I

    if-nez v4, :cond_2

    .line 178
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mContentParser:Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

    iget-object v5, v0, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass4;->mField2:Ljava/lang/String;

    iget-object v6, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mParentObject:Lcom/microsoft/playready/networkdevice/TransmitterContent;

    invoke-interface {v4, v5, v6}, Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;->ParseContents(Ljava/lang/String;Lcom/microsoft/playready/networkdevice/TransmitterContent;)Ljava/util/List;

    move-result-object v3

    .local v3, "val":Ljava/util/List;
    move-object v1, v3

    .local v1, "val":Ljava/lang/Object;
    goto :goto_1

    .line 193
    .end local v0    # "browseInfo":Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass4;
    .end local v3    # "val":Ljava/util/List;
    .local v1, "result":Ljava/lang/Object;, "TV;"
    :cond_3
    return-object v1

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 168
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_7
    .end packed-switch
.end method

.method public call1(I)V
    .locals 4
    .param p1, "errorCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 218
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<TV;>;"
    iput p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mErrorCode:I

    .line 220
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mEvent:Ljava/lang/Object;

    monitor-enter v3

    .line 222
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mIsComplete:Z

    .line 223
    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mEvent:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 224
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 226
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mListeners:Ljava/util/List;

    monitor-enter v3

    .line 228
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;

    .line 230
    .local v1, "listener":Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;
    invoke-interface {v1, p1}, Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;->onComplete(I)V

    goto :goto_0

    .line 232
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 224
    :catchall_1
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 232
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 233
    return-void
.end method

.method cancel(Z)Z
    .locals 3
    .param p1, "mayInterruptIfRunning"    # Z

    .prologue
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<TV;>;"
    const/4 v2, 0x1

    .line 199
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class7;

    invoke-virtual {v0}, Lcom/microsoft/playready/networkdevice/Native_Class7;->method_3()V

    .line 201
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mEvent:Ljava/lang/Object;

    monitor-enter v1

    .line 203
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mIsComplete:Z

    .line 204
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mEvent:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 205
    monitor-exit v1

    .line 207
    return v2

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getErrorCode()I
    .locals 1

    .prologue
    .line 212
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<TV;>;"
    iget v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mErrorCode:I

    return v0
.end method

.method removeOperationListener(Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;

    .prologue
    .line 86
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<TV;>;"
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mListeners:Ljava/util/List;

    monitor-enter v1

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 89
    monitor-exit v1

    .line 90
    return-void

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setContentParser(Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;)V
    .locals 0
    .param p1, "parser"    # Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

    .prologue
    .line 60
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<TV;>;"
    if-eqz p1, :cond_0

    .line 62
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mContentParser:Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

    .line 64
    :cond_0
    return-void
.end method

.method setParentObject(Lcom/microsoft/playready/networkdevice/TransmitterContent;)V
    .locals 0
    .param p1, "parent"    # Lcom/microsoft/playready/networkdevice/TransmitterContent;

    .prologue
    .line 68
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<TV;>;"
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->mParentObject:Lcom/microsoft/playready/networkdevice/TransmitterContent;

    .line 69
    return-void
.end method
