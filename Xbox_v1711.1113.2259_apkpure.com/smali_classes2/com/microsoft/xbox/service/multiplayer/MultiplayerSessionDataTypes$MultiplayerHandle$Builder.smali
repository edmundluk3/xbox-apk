.class public abstract Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;
.super Ljava/lang/Object;
.source "MultiplayerSessionDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract activityInfo(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;
.end method

.method public abstract build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
.end method

.method public abstract id(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;
.end method

.method public abstract relatedInfo(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;
.end method

.method public abstract roleInfo(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;
.end method

.method public abstract searchAttributes(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;
.end method

.method public abstract sessionRef(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;
.end method

.method public type(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleType;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;
    .locals 1
    .param p1, "type"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleType;

    .prologue
    .line 626
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;->type(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public abstract type(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;
.end method
