.class public final enum Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
.super Ljava/lang/Enum;
.source "EPGProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPGProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FetchResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

.field public static final enum errorAuthentication:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

.field public static final enum errorNetwork:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

.field public static final enum errorParseAndStore:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

.field public static final enum errorUnknown:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

.field public static final enum resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 452
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    const-string/jumbo v1, "resultNoError"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    const-string v1, "errorUnknown"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorUnknown:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    const-string v1, "errorNetwork"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorNetwork:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    const-string v1, "errorParseAndStore"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorParseAndStore:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    const-string v1, "errorAuthentication"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorAuthentication:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 451
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorUnknown:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorNetwork:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorParseAndStore:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorAuthentication:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->$VALUES:[Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 451
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 451
    const-class v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    .locals 1

    .prologue
    .line 451
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->$VALUES:[Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    return-object v0
.end method
