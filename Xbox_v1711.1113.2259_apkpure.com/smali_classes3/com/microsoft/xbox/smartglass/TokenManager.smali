.class public Lcom/microsoft/xbox/smartglass/TokenManager;
.super Lcom/microsoft/xbox/smartglass/Listenable;
.source "TokenManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/smartglass/Listenable",
        "<",
        "Lcom/microsoft/xbox/smartglass/TokenManagerListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final _pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

.field public final type:Lcom/microsoft/xbox/smartglass/TokenType;


# direct methods
.method constructor <init>(JLcom/microsoft/xbox/smartglass/TokenType;)V
    .locals 5
    .param p1, "pPlatform"    # J
    .param p3, "tokenType"    # Lcom/microsoft/xbox/smartglass/TokenType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/Listenable;-><init>()V

    .line 15
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {p3}, Lcom/microsoft/xbox/smartglass/TokenType;->getValue()I

    move-result v1

    invoke-direct {p0, p1, p2, v1}, Lcom/microsoft/xbox/smartglass/TokenManager;->initialize(JI)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/TokenManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 16
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/TokenManager;->type:Lcom/microsoft/xbox/smartglass/TokenType;

    .line 17
    return-void
.end method

.method private native getAuthInfo(J)Lcom/microsoft/xbox/smartglass/AuthInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native getToken(JLjava/lang/String;ZLcom/microsoft/xbox/smartglass/TokenListener;)Lcom/microsoft/xbox/smartglass/TokenResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native getXboxLiveToken(JZLcom/microsoft/xbox/smartglass/TokenListener;)Lcom/microsoft/xbox/smartglass/TokenResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native initialize(JI)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private onAuthTicketError(I)V
    .locals 3
    .param p1, "authError"    # I

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/TokenManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/TokenManagerListener;

    .line 95
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/TokenManagerListener;
    invoke-static {p1}, Lcom/microsoft/xbox/smartglass/AuthError;->fromInt(I)Lcom/microsoft/xbox/smartglass/AuthError;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/smartglass/TokenManagerListener;->onAuthTicketError(Lcom/microsoft/xbox/smartglass/AuthError;)V

    goto :goto_0

    .line 97
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/TokenManagerListener;
    :cond_0
    return-void
.end method

.method private onAuthTicketRefreshed()V
    .locals 3

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/TokenManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/TokenManagerListener;

    .line 102
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/TokenManagerListener;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/TokenManagerListener;->onAuthTicketRefreshed()V

    goto :goto_0

    .line 104
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/TokenManagerListener;
    :cond_0
    return-void
.end method

.method private native setAuthInfo(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native setSandboxId(JLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method


# virtual methods
.method public clearAuthInfo()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 38
    invoke-static {}, Lcom/microsoft/xbox/smartglass/AuthInfo;->getAnonymous()Lcom/microsoft/xbox/smartglass/AuthInfo;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/TokenManager;->setAuthInfo(Lcom/microsoft/xbox/smartglass/AuthInfo;Z)V

    .line 39
    return-void
.end method

.method public getAuthInfo()Lcom/microsoft/xbox/smartglass/AuthInfo;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TokenManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/TokenManager;->getAuthInfo(J)Lcom/microsoft/xbox/smartglass/AuthInfo;

    move-result-object v0

    return-object v0
.end method

.method public getToken(Ljava/lang/String;ZLcom/microsoft/xbox/smartglass/TokenListener;)Lcom/microsoft/xbox/smartglass/TokenResult;
    .locals 7
    .param p1, "audienceUri"    # Ljava/lang/String;
    .param p2, "forceRefresh"    # Z
    .param p3, "listener"    # Lcom/microsoft/xbox/smartglass/TokenListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TokenManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/smartglass/TokenManager;->getToken(JLjava/lang/String;ZLcom/microsoft/xbox/smartglass/TokenListener;)Lcom/microsoft/xbox/smartglass/TokenResult;

    move-result-object v0

    return-object v0
.end method

.method public getXboxLiveToken(ZLcom/microsoft/xbox/smartglass/TokenListener;)Lcom/microsoft/xbox/smartglass/TokenResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .param p2, "listener"    # Lcom/microsoft/xbox/smartglass/TokenListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TokenManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/microsoft/xbox/smartglass/TokenManager;->getXboxLiveToken(JZLcom/microsoft/xbox/smartglass/TokenListener;)Lcom/microsoft/xbox/smartglass/TokenResult;

    move-result-object v0

    return-object v0
.end method

.method public setAuthInfo(Lcom/microsoft/xbox/smartglass/AuthInfo;Z)V
    .locals 10
    .param p1, "authInfo"    # Lcom/microsoft/xbox/smartglass/AuthInfo;
    .param p2, "resetAllTokens"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 27
    if-nez p1, :cond_0

    .line 28
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "authInfo cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TokenManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/AuthInfo;->authTicket:Ljava/lang/String;

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/AuthInfo;->refreshToken:Ljava/lang/String;

    iget-object v6, p1, Lcom/microsoft/xbox/smartglass/AuthInfo;->clientId:Ljava/lang/String;

    iget-object v7, p1, Lcom/microsoft/xbox/smartglass/AuthInfo;->policy:Ljava/lang/String;

    iget-object v8, p1, Lcom/microsoft/xbox/smartglass/AuthInfo;->sandboxId:Ljava/lang/String;

    move-object v1, p0

    move v9, p2

    invoke-direct/range {v1 .. v9}, Lcom/microsoft/xbox/smartglass/TokenManager;->setAuthInfo(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 31
    return-void
.end method

.method public setSandboxId(Ljava/lang/String;)V
    .locals 2
    .param p1, "sandboxId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TokenManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/TokenManager;->setSandboxId(JLjava/lang/String;)V

    .line 49
    return-void
.end method
