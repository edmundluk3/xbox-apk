.class public abstract Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "EDSV2MediaItemListViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;",
        ">",
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected isLoadingList:Z

.field protected loadListTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel",
            "<TT;>.",
            "LoadListTask;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 48
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->loadListTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;

    .line 49
    return-void
.end method


# virtual methods
.method public getCurrentScreenData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 114
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getParentMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultImageRid()I
    .locals 1

    .prologue
    .line 65
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getParentMediaType()I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    return v0
.end method

.method protected getDefaultUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;
    .locals 1

    .prologue
    .line 122
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MediaListBrowse:Lcom/microsoft/xbox/service/model/UpdateType;

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getParentImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getModelErrorCode()J
    .locals 2

    .prologue
    .line 109
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    const-wide/16 v0, 0xfaf

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 70
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->isLoadingList:Z

    return v0
.end method

.method protected isScreenDataEmpty()Z
    .locals 1

    .prologue
    .line 118
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 76
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->loadListTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->load(Z)V

    .line 77
    return-void
.end method

.method protected onListDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 80
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    const-string v0, "MediaItemListViewModel"

    const-string v1, "onLoadListCompleted Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->isLoadingList:Z

    .line 83
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 98
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->updateAdapter()V

    .line 99
    return-void

    .line 87
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->isScreenDataEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 91
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 92
    const-string v0, "MediaItemListViewModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error state because "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 103
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>;"
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->onStopOverride()V

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->loadListTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->cancel()V

    .line 105
    return-void
.end method
