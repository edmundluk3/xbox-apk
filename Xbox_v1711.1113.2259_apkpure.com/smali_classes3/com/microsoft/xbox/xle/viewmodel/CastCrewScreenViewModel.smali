.class public Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;
.source "CastCrewScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;-><init>()V

    .line 18
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getCastCrewAdapter(Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 19
    return-void
.end method


# virtual methods
.method protected getCurrentScreenData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCastCrew()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 23
    const v0, 0x7f070683

    return v0
.end method

.method public onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 40
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCastCrew()Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 41
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/CastCrewScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 43
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getCastCrewAdapter(Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 29
    return-void
.end method

.method public searchContributor(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCastCrew()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCastCrew()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v1, p1, :cond_1

    .line 52
    :cond_0
    const-string v1, "CastAndCrewViewModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "data out of bound "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :goto_0
    return-void

    .line 56
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->getData()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;

    .line 57
    .local v0, "contributor":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    iget-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;->Name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSearchTag(Ljava/lang/String;)V

    .line 58
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/SearchResultsActivity;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    goto :goto_0
.end method
