.class Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;
.super Ljava/lang/Object;
.source "PeopleScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClubViewHolder"
.end annotation


# instance fields
.field clubActivityText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d5
    .end annotation
.end field

.field clubGlyph:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d2
    .end annotation
.end field

.field clubImage:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08cf
    .end annotation
.end field

.field clubManagementIndicator:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d6
    .end annotation
.end field

.field clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d1
    .end annotation
.end field

.field clubType:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d3
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 303
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 304
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 305
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 11
    .param p1, "followerData"    # Lcom/microsoft/xbox/service/model/FollowersData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v10, 0x0

    const v5, 0x7f0201fa

    const v4, 0x7f020125

    const/4 v1, 0x0

    .line 308
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 310
    iget-object v8, p1, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 311
    .local v8, "followerClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->clubImage:Landroid/widget/ImageView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v0, v4, v4}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 313
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->clubType:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->localizedTitleFamilyName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->glyphImageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->glyphImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v5, v5}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 323
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->clubManagementIndicator:Landroid/view/View;

    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v8, v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->hasTargetRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->hasManagementActions()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 324
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v9

    .line 325
    .local v9, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v9, :cond_3

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v7

    .line 326
    .local v7, "color":I
    :goto_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->clubManagementIndicator:Landroid/view/View;

    invoke-static {v0, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setBackgroundColorIfNotNull(Landroid/view/View;I)V

    .line 328
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getSearchResultPerson()Lcom/microsoft/xbox/service/model/SearchResultPerson;

    move-result-object v2

    .line 329
    .local v2, "searchResultPerson":Lcom/microsoft/xbox/service/model/SearchResultPerson;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 332
    invoke-static {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    :goto_3
    if-eqz v2, :cond_5

    iget-object v4, v2, Lcom/microsoft/xbox/service/model/SearchResultPerson;->GamertagMatch:Ljava/lang/String;

    :goto_4
    const v5, 0x7f080290

    const v6, 0x7f080209

    .line 329
    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/SearchResultPerson;Ljava/lang/String;Ljava/lang/String;II)V

    .line 337
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->clubActivityText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p1, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v4, v2, Lcom/microsoft/xbox/service/model/SearchResultPerson;->StatusMatch:Ljava/lang/String;

    :goto_5
    const v5, 0x7f08028f

    const v6, 0x7f080206

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/SearchResultPerson;Ljava/lang/String;Ljava/lang/String;II)V

    .line 344
    return-void

    .line 319
    .end local v2    # "searchResultPerson":Lcom/microsoft/xbox/service/model/SearchResultPerson;
    .end local v7    # "color":I
    .end local v9    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 323
    goto :goto_1

    .line 325
    .restart local v9    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_3
    sget v7, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    goto :goto_2

    .line 332
    .restart local v2    # "searchResultPerson":Lcom/microsoft/xbox/service/model/SearchResultPerson;
    .restart local v7    # "color":I
    :cond_4
    const-string v3, ""

    goto :goto_3

    :cond_5
    move-object v4, v10

    goto :goto_4

    :cond_6
    move-object v4, v10

    .line 337
    goto :goto_5
.end method
