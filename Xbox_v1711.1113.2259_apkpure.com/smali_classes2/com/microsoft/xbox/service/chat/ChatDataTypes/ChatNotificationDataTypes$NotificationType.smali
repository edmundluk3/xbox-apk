.class public final enum Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;
.super Ljava/lang/Enum;
.source "ChatNotificationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NotificationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

.field public static final enum ChatAggregateBroadcast:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

.field public static final enum ChatDirectMention:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

.field public static final enum ChatSingleBroadcast:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    const-string v1, "ChatDirectMention"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;->ChatDirectMention:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    const-string v1, "ChatSingleBroadcast"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;->ChatSingleBroadcast:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    const-string v1, "ChatAggregateBroadcast"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;->ChatAggregateBroadcast:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    .line 21
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;->ChatDirectMention:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;->ChatSingleBroadcast:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;->ChatAggregateBroadcast:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;->$VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;->$VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    return-object v0
.end method
