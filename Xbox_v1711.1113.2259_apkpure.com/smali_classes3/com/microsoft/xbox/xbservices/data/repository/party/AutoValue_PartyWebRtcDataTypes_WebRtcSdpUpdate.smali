.class final Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcSdpUpdate;
.super Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;
.source "AutoValue_PartyWebRtcDataTypes_WebRtcSdpUpdate.java"


# instance fields
.field private final sdp:Lorg/webrtc/SessionDescription;


# direct methods
.method constructor <init>(Lorg/webrtc/SessionDescription;)V
    .locals 2
    .param p1, "sdp"    # Lorg/webrtc/SessionDescription;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;-><init>()V

    .line 15
    if-nez p1, :cond_0

    .line 16
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null sdp"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcSdpUpdate;->sdp:Lorg/webrtc/SessionDescription;

    .line 19
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 36
    if-ne p1, p0, :cond_0

    .line 37
    const/4 v1, 0x1

    .line 43
    :goto_0
    return v1

    .line 39
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 40
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;

    .line 41
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcSdpUpdate;->sdp:Lorg/webrtc/SessionDescription;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;->sdp()Lorg/webrtc/SessionDescription;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 43
    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 48
    const/4 v0, 0x1

    .line 49
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcSdpUpdate;->sdp:Lorg/webrtc/SessionDescription;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 51
    return v0
.end method

.method public sdp()Lorg/webrtc/SessionDescription;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcSdpUpdate;->sdp:Lorg/webrtc/SessionDescription;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WebRtcSdpUpdate{sdp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcSdpUpdate;->sdp:Lorg/webrtc/SessionDescription;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
