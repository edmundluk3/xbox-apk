.class public Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "SystemSettingsModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetFriendFinderSettingsRunner;,
        Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetSmartglassSettingsRunner;,
        Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetLiveTVSettingsRunner;,
        Lcom/microsoft/xbox/xle/model/SystemSettingsModel$SystemSettingsModelContainer;,
        Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/model/serialization/Version;",
        ">;"
    }
.end annotation


# static fields
.field private static final SHORSE_PREF_KEY:Ljava/lang/String; = "SHORSE_ENABLE"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private friendFinderSettings:Lcom/microsoft/xbox/xle/app/FriendFinderSettings;

.field private final friendFinderSettingsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private final hiddenMruItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private latestVersion:I

.field private liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

.field private final liveTVSettingsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private marketUrl:Ljava/lang/String;

.field private minRequiredOSVersion:I

.field private minVersion:I

.field private remoteControlSpecialTitleIds:[I

.field private settingsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Lcom/microsoft/xbox/xle/app/SmartglassSettings;",
            ">;"
        }
    .end annotation
.end field

.field sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

.field private final smartglassSettingsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private updateExistListener:Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->hiddenMruItems:Ljava/util/HashSet;

    .line 47
    iput v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->minRequiredOSVersion:I

    .line 48
    iput v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->minVersion:I

    .line 49
    iput v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->latestVersion:I

    .line 63
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettingsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 64
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettingsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 65
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->friendFinderSettingsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 67
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->settingsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 69
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)V

    .line 70
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/model/SystemSettingsModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel$1;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;-><init>()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->onGetLiveTVSettingsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->onGetSmartglassSettingsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->onGetFriendFinderSettingsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$SystemSettingsModelContainer;->access$000()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    return-object v0
.end method

.method private getMinimumVersion()I
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->minVersion:I

    invoke-static {v0}, Lcom/microsoft/xle/test/interop/TestInterop;->getMinimumVersionRequired(I)I

    move-result v0

    return v0
.end method

.method private onGetFriendFinderSettingsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/xle/app/FriendFinderSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 322
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/xle/app/FriendFinderSettings;>;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 324
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_2

    .line 325
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->friendFinderSettings:Lcom/microsoft/xbox/xle/app/FriendFinderSettings;

    .line 326
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->friendFinderSettings:Lcom/microsoft/xbox/xle/app/FriendFinderSettings;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->friendFinderSettings:Lcom/microsoft/xbox/xle/app/FriendFinderSettings;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->friendFinderSettings:Lcom/microsoft/xbox/xle/app/FriendFinderSettings;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->ICONS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->getIconsFromJson(Ljava/lang/String;)V

    .line 332
    :cond_0
    :goto_1
    return-void

    .line 322
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 330
    :cond_2
    const-string v0, "SystemSettingsModel"

    const-string v1, "failed to get friend finder settings"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private onGetLiveTVSettingsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/xle/app/LiveTVSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/xle/app/LiveTVSettings;>;"
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 235
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 237
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v3, :cond_d

    .line 238
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    if-eqz v0, :cond_1

    .line 240
    sget-boolean v0, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v0, :cond_7

    .line 241
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iget v0, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSGUIDEENABLED_BETA:I

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setPremiumLiveTVEnabled(Z)V

    .line 242
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iget v0, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSURCENABLED_BETA:I

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setPremiumUrcEnabled(Z)V

    .line 243
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iget v0, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSCONSOLEPOWERENABLED_BETA:I

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setPowerEnabled(Z)V

    .line 244
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iget v0, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSNETWORKTESTINGENABLED_BETA:I

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setNetworkTestingEnabled(Z)V

    .line 252
    :goto_5
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iget v0, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSSHOWSPECIFICCONTENTRATINGS:I

    if-ne v0, v1, :cond_c

    move v0, v1

    :goto_6
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setShowSpecificContentRatingsEnabled(Z)V

    .line 253
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iget v3, v3, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSTREATMISSINGRATINGASADULT:I

    if-ne v3, v1, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setTreatMissingRatingAsAdult(Z)V

    .line 254
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSNONADULTCONTENTRATINGS:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setNonAdultContentRatings(Ljava/lang/String;)V

    .line 260
    :cond_1
    :goto_7
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->LiveTVSettingsData:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v2, v3, v1}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    invoke-direct {v0, v2, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 261
    return-void

    :cond_2
    move v0, v2

    .line 235
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 241
    goto :goto_1

    :cond_4
    move v0, v2

    .line 242
    goto :goto_2

    :cond_5
    move v0, v2

    .line 243
    goto :goto_3

    :cond_6
    move v0, v2

    .line 244
    goto :goto_4

    .line 246
    :cond_7
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iget v0, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSGUIDEENABLED:I

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setPremiumLiveTVEnabled(Z)V

    .line 247
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iget v0, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSURCENABLED:I

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setPremiumUrcEnabled(Z)V

    .line 248
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iget v0, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSCONSOLEPOWERENABLED:I

    if-ne v0, v1, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setPowerEnabled(Z)V

    .line 249
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettings:Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    iget v0, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSNETWORKTESTINGENABLED:I

    if-ne v0, v1, :cond_b

    move v0, v1

    :goto_b
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setNetworkTestingEnabled(Z)V

    goto :goto_5

    :cond_8
    move v0, v2

    .line 246
    goto :goto_8

    :cond_9
    move v0, v2

    .line 247
    goto :goto_9

    :cond_a
    move v0, v2

    .line 248
    goto :goto_a

    :cond_b
    move v0, v2

    .line 249
    goto :goto_b

    :cond_c
    move v0, v2

    .line 252
    goto :goto_6

    .line 257
    :cond_d
    const-string v0, "SystemSettingsModel"

    const-string v2, "failed to get livetv settings"

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7
.end method

.method private onGetSmartglassSettingsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/xle/app/SmartglassSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/xle/app/SmartglassSettings;>;"
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 264
    sget-object v3, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->TAG:Ljava/lang/String;

    const-string v8, "onGetSmartglassSettingsCompleted"

    invoke-static {v3, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v3, v8, :cond_2

    move v3, v6

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 267
    sget-object v3, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onGetSmartglassSettingsCompleted status="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    sget-object v8, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v3, v8, :cond_d

    .line 270
    sget-object v3, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->TAG:Ljava/lang/String;

    const-string v8, "onGetSmartglassSettingsCompleted status=SUCCESS!"

    invoke-static {v3, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    .line 272
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    if-eqz v3, :cond_a

    .line 273
    sget-boolean v3, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ANDROID_VERSIONMINOS_BETA:I

    :goto_1
    iput v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->minRequiredOSVersion:I

    .line 274
    sget-boolean v3, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ANDROID_VERSIONMIN_BETA:I

    :goto_2
    iput v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->minVersion:I

    .line 275
    sget-boolean v3, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ANDROID_VERSIONLATEST_BETA:I

    :goto_3
    iput v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->latestVersion:I

    .line 276
    sget-boolean v3, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ANDROID_VERSIONURL_BETA:Ljava/lang/String;

    :goto_4
    iput-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->marketUrl:Ljava/lang/String;

    .line 277
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->HIDDEN_MRU_ITEMS:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->populateHiddenMruItems(Ljava/lang/String;)V

    .line 278
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->REMOTE_CONTROL_SPECIALS:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->populateRemoteControlSpecialTitleIds(Ljava/lang/String;)V

    .line 281
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->SNAPPABLE_SYSTEM_APPS:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 282
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->SNAPPABLE_SYSTEM_APPS:Ljava/lang/String;

    const-string v8, "\\|"

    invoke-virtual {v3, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 283
    .local v1, "apps":[Ljava/lang/String;
    array-length v8, v1

    move v3, v7

    :goto_5
    if-ge v3, v8, :cond_7

    aget-object v0, v1, v3

    .line 284
    .local v0, "app":Ljava/lang/String;
    const-string v9, ";"

    invoke-virtual {v0, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 285
    .local v2, "sections":[Ljava/lang/String;
    array-length v9, v2

    const/4 v10, 0x3

    if-ne v9, v10, :cond_1

    .line 286
    aget-object v9, v2, v6

    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->parseHexLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 287
    .local v4, "titleId":J
    const-wide/16 v10, 0x0

    cmp-long v9, v4, v10

    if-lez v9, :cond_0

    .line 288
    invoke-static {}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->getInstance()Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->addSnappableApp(J)V

    .line 290
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->getInstance()Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    move-result-object v9

    const/4 v10, 0x2

    aget-object v10, v2, v10

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->addSnappableApp(Ljava/lang/String;)V

    .line 283
    .end local v4    # "titleId":J
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .end local v0    # "app":Ljava/lang/String;
    .end local v1    # "apps":[Ljava/lang/String;
    .end local v2    # "sections":[Ljava/lang/String;
    :cond_2
    move v3, v7

    .line 265
    goto/16 :goto_0

    .line 273
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ANDROID_VERSIONMINOS:I

    goto :goto_1

    .line 274
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ANDROID_VERSIONMIN:I

    goto :goto_2

    .line 275
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ANDROID_VERSIONLATEST:I

    goto :goto_3

    .line 276
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ANDROID_VERSIONURL:Ljava/lang/String;

    goto :goto_4

    .line 295
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->updateExistListener:Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;

    if-eqz v3, :cond_8

    .line 296
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getVersionCode()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getMustUpdate(I)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 297
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->updateExistListener:Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;->onMustUpdate()V

    .line 306
    :cond_8
    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v6, "SHORSE_ENABLE"

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 307
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->forceEnablePartyChat()V

    .line 312
    :cond_9
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    iget-object v6, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v6, v6, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->HELPCOMPANION_ALLOWEDURLS:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setHelpCompanionWhiteListUrls(Ljava/lang/String;)V

    .line 313
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v3

    iget-object v6, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->processContentBlockedList(Lcom/microsoft/xbox/xle/app/SmartglassSettings;)V

    .line 314
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->settingsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    invoke-virtual {v3, v6}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 319
    :cond_a
    :goto_7
    return-void

    .line 298
    :cond_b
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getVersionCode()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getHasUpdate(I)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 299
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->updateExistListener:Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;->onOptionalUpdate()V

    goto :goto_6

    .line 301
    :cond_c
    const-string v3, "SystemSettingsModel"

    const-string/jumbo v6, "up to date"

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 317
    :cond_d
    const-string v3, "SystemSettingsModel"

    const-string v6, "failed to get smartglass settings"

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7
.end method

.method private populateHiddenMruItems(Ljava/lang/String;)V
    .locals 5
    .param p1, "list"    # Ljava/lang/String;

    .prologue
    .line 335
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->hiddenMruItems:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 336
    if-eqz p1, :cond_0

    .line 337
    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 338
    .local v0, "buf":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 339
    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 340
    .local v1, "titleId":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->hiddenMruItems:Ljava/util/HashSet;

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 339
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 344
    .end local v0    # "buf":[Ljava/lang/String;
    .end local v1    # "titleId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private populateRemoteControlSpecialTitleIds(Ljava/lang/String;)V
    .locals 8
    .param p1, "commaDelimited"    # Ljava/lang/String;

    .prologue
    .line 347
    if-eqz p1, :cond_0

    .line 348
    const-string v5, ","

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 349
    .local v0, "buf":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 350
    array-length v5, v0

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->remoteControlSpecialTitleIds:[I

    .line 351
    const/4 v2, 0x0

    .line 352
    .local v2, "index":I
    array-length v6, v0

    const/4 v5, 0x0

    move v3, v2

    .end local v2    # "index":I
    .local v3, "index":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v4, v0, v5

    .line 353
    .local v4, "titleId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 355
    .local v1, "id":I
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 358
    :goto_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->remoteControlSpecialTitleIds:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "index":I
    .restart local v2    # "index":I
    aput v1, v7, v3

    .line 352
    add-int/lit8 v5, v5, 0x1

    move v3, v2

    .end local v2    # "index":I
    .restart local v3    # "index":I
    goto :goto_0

    .line 356
    :catch_0
    move-exception v7

    goto :goto_1

    .line 362
    .end local v0    # "buf":[Ljava/lang/String;
    .end local v1    # "id":I
    .end local v3    # "index":I
    .end local v4    # "titleId":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public beamAppDeeplinkEnabled()Z
    .locals 2

    .prologue
    .line 155
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    if-eqz v1, :cond_1

    .line 156
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->BEAM_APP_DEEPLINK_ENABLED_BETA:Ljava/lang/String;

    .line 157
    .local v0, "deeplinkEnabled":Ljava/lang/String;
    :goto_0
    const-string/jumbo v1, "true"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 160
    .end local v0    # "deeplinkEnabled":Ljava/lang/String;
    :goto_1
    return v1

    .line 156
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->BEAM_APP_DEEPLINK_ENABLED:Ljava/lang/String;

    goto :goto_0

    .line 160
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public customPicUploadEnabled()Z
    .locals 2

    .prologue
    .line 164
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    if-eqz v1, :cond_1

    .line 165
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->CUSTOM_PIC_UPLOADING_ENABLED_BETA:Ljava/lang/String;

    .line 166
    .local v0, "enabled":Ljava/lang/String;
    :goto_0
    const-string/jumbo v1, "true"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 169
    .end local v0    # "enabled":Ljava/lang/String;
    :goto_1
    return v1

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->CUSTOM_PIC_UPLOADING_ENABLED:Ljava/lang/String;

    goto :goto_0

    .line 169
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public forceEnablePartyChat()V
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SHORSE_ENABLE"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    const-string/jumbo v1, "true"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->SHORSE_ENABLED_BETA:Ljava/lang/String;

    .line 209
    return-void
.end method

.method public getHasUpdate(I)Z
    .locals 4
    .param p1, "currentVersionCode"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 85
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 86
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iget v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->minRequiredOSVersion:I

    if-lt v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getLatestVersion()I

    move-result v0

    if-le v0, p1, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 85
    goto :goto_0

    :cond_1
    move v1, v2

    .line 86
    goto :goto_1
.end method

.method public getLatestVersion()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->latestVersion:I

    invoke-static {v0}, Lcom/microsoft/xle/test/interop/TestInterop;->getLatestVersionAvailable(I)I

    move-result v0

    return v0
.end method

.method public getMarketUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->marketUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getMustUpdate(I)Z
    .locals 4
    .param p1, "currentVersionCode"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 90
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 91
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iget v3, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->minRequiredOSVersion:I

    if-lt v0, v3, :cond_1

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getMinimumVersion()I

    move-result v0

    if-le v0, p1, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 90
    goto :goto_0

    :cond_1
    move v1, v2

    .line 91
    goto :goto_1
.end method

.method public getRemoteControlSpecialTitleIds()[I
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->remoteControlSpecialTitleIds:[I

    return-object v0
.end method

.method public getSettings()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xle/app/SmartglassSettings;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->settingsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public isInHiddenMruItems(Ljava/lang/String;)Z
    .locals 1
    .param p1, "titleId"    # Ljava/lang/String;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->hiddenMruItems:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isPartyChatEnabled()Z
    .locals 4

    .prologue
    .line 196
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    if-eqz v1, :cond_1

    .line 197
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->SHORSE_ENABLED_BETA:Ljava/lang/String;

    .line 198
    .local v0, "enabled":Ljava/lang/String;
    :goto_0
    const-string/jumbo v1, "true"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 203
    .end local v0    # "enabled":Ljava/lang/String;
    :goto_1
    return v1

    .line 197
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->SHORSE_ENABLED:Ljava/lang/String;

    goto :goto_0

    .line 203
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "SHORSE_ENABLE"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_1
.end method

.method public loadAsync(Z)V
    .locals 7
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v4, 0x0

    .line 216
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 217
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->lifetime:J

    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettingsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetLiveTVSettingsRunner;

    invoke-direct {v6, p0, p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetLiveTVSettingsRunner;-><init>(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    .line 218
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->lifetime:J

    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettingsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetSmartglassSettingsRunner;

    invoke-direct {v6, p0, p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetSmartglassSettingsRunner;-><init>(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    .line 219
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->loadFriendFinderSettingsAsync(Z)V

    .line 220
    return-void

    .line 216
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadFriendFinderSettingsAsync(Z)V
    .locals 7
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 231
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->lifetime:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->friendFinderSettingsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetFriendFinderSettingsRunner;

    invoke-direct {v6, p0, p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetFriendFinderSettingsRunner;-><init>(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    .line 232
    return-void
.end method

.method public loadLiveTVSettings(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/xle/app/LiveTVSettings;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->lifetime:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->liveTVSettingsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetLiveTVSettingsRunner;

    invoke-direct {v6, p0, p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetLiveTVSettingsRunner;-><init>(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadSystemSettings(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/xle/app/SmartglassSettings;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->lifetime:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettingsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetSmartglassSettingsRunner;

    invoke-direct {v6, p0, p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetSmartglassSettingsRunner;-><init>(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public setOnUpdateExistListener(Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->updateExistListener:Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;

    .line 82
    return-void
.end method

.method public shouldShowArches()Z
    .locals 2

    .prologue
    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    if-eqz v1, :cond_1

    .line 128
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ARCHES_ENABLED_BETA:Ljava/lang/String;

    .line 129
    .local v0, "archesEnabled":Ljava/lang/String;
    :goto_0
    const-string/jumbo v1, "true"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 132
    .end local v0    # "archesEnabled":Ljava/lang/String;
    :goto_1
    return v1

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ARCHES_ENABLED:Ljava/lang/String;

    goto :goto_0

    .line 132
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public showBeamTrending()Z
    .locals 2

    .prologue
    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isViewingBroadcastsRestricted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 112
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->BEAM_TRENDING_ENABLED_BETA:Ljava/lang/String;

    .line 113
    .local v0, "beamTrendingEnabled":Ljava/lang/String;
    :goto_0
    const-string/jumbo v1, "true"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 116
    .end local v0    # "beamTrendingEnabled":Ljava/lang/String;
    :goto_1
    return v1

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->BEAM_TRENDING_ENABLED:Ljava/lang/String;

    goto :goto_0

    .line 116
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public testEnableArches()V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    const-string/jumbo v1, "true"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ARCHES_ENABLED:Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    const-string/jumbo v1, "true"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ARCHES_ENABLED_BETA:Ljava/lang/String;

    .line 147
    return-void
.end method

.method public testEnableArchesRTMLists()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    const-string v1, "ga"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ARCHES_CATALOG:Ljava/lang/String;

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    const-string v1, "ga"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ARCHES_CATALOG_BETA:Ljava/lang/String;

    .line 152
    return-void
.end method

.method public testEnableBeamTrending()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    const-string/jumbo v1, "true"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->BEAM_TRENDING_ENABLED:Ljava/lang/String;

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    const-string/jumbo v1, "true"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->BEAM_TRENDING_ENABLED_BETA:Ljava/lang/String;

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    const-string/jumbo v1, "true"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->BEAM_APP_DEEPLINK_ENABLED:Ljava/lang/String;

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    const-string/jumbo v1, "true"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->BEAM_APP_DEEPLINK_ENABLED_BETA:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public testEnableCustomPicUpload()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    const-string/jumbo v1, "true"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->CUSTOM_PIC_UPLOADING_ENABLED:Ljava/lang/String;

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    const-string/jumbo v1, "true"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->CUSTOM_PIC_UPLOADING_ENABLED_BETA:Ljava/lang/String;

    .line 175
    return-void
.end method

.method public tutorialExperienceEnabled()Z
    .locals 5

    .prologue
    .line 182
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    if-eqz v2, :cond_1

    .line 183
    sget-boolean v2, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v2, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->TUTORIAL_EXPERIENCE_ENABLED_BETA:Ljava/lang/String;

    .line 184
    .local v0, "enabled":Ljava/lang/String;
    :goto_0
    const-string/jumbo v2, "true"

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 186
    .local v1, "result":Z
    sget-object v2, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Checking to see if tutorial experience live setting is enabled. Result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    .end local v0    # "enabled":Ljava/lang/String;
    .end local v1    # "result":Z
    :goto_1
    return v1

    .line 183
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v2, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->TUTORIAL_EXPERIENCE_ENABLED:Ljava/lang/String;

    goto :goto_0

    .line 189
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->TAG:Ljava/lang/String;

    const-string v3, "SmartglassSettings are somehow null"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public useArchesRTMLists()Z
    .locals 2

    .prologue
    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    if-eqz v1, :cond_1

    .line 137
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ARCHES_CATALOG_BETA:Ljava/lang/String;

    .line 138
    .local v0, "archesCatalog":Ljava/lang/String;
    :goto_0
    const-string v1, "ga"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 141
    .end local v0    # "archesCatalog":Ljava/lang/String;
    :goto_1
    return v1

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->smartglassSettings:Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->ARCHES_CATALOG:Ljava/lang/String;

    goto :goto_0

    .line 141
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
