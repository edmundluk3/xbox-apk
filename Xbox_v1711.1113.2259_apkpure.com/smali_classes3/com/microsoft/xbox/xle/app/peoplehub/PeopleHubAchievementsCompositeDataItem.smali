.class public abstract Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
.super Ljava/lang/Object;
.source "PeopleHubAchievementsCompositeDataItem.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static achievementHeader()Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 26
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;-><init>(ZZLcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;Landroid/util/Pair;)V

    return-object v0
.end method

.method public static achievementItem(Landroid/util/Pair;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
    .locals 3
    .param p0    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;)",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;"
        }
    .end annotation

    .prologue
    .local p0, "achievementItem":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;"
    const/4 v2, 0x0

    .line 35
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 36
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1, p0}, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;-><init>(ZZLcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;Landroid/util/Pair;)V

    return-object v0
.end method

.method public static leaderboardHeader()Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;-><init>(ZZLcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;Landroid/util/Pair;)V

    return-object v0
.end method

.method public static leaderboardItem(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
    .locals 3
    .param p0, "leaderboardListItem"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 30
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 31
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;-><init>(ZZLcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;Landroid/util/Pair;)V

    return-object v0
.end method


# virtual methods
.method public abstract achievementItem()Landroid/util/Pair;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isAchievementHeader()Z
.end method

.method public isAchievementItem()Z
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->achievementItem()Landroid/util/Pair;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract isLeaderboardHeader()Z
.end method

.method public isLeaderboardItem()Z
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->leaderboardItem()Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract leaderboardItem()Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
