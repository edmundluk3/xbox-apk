.class Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "EnforcementViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SendXboxReportAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private reportTargetId:Ljava/lang/String;

.field private reportUserData:Lcom/microsoft/xbox/service/model/sls/ReportUserData;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Lcom/microsoft/xbox/service/model/sls/ReportUserData;Ljava/lang/String;)V
    .locals 0
    .param p2, "reportUserData"    # Lcom/microsoft/xbox/service/model/sls/ReportUserData;
    .param p3, "reportTargetId"    # Ljava/lang/String;

    .prologue
    .line 265
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 266
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->reportUserData:Lcom/microsoft/xbox/service/model/sls/ReportUserData;

    .line 267
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->reportTargetId:Ljava/lang/String;

    .line 268
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 299
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getEnforcementService()Lcom/microsoft/xbox/service/enforcement/IEnforcementService;

    move-result-object v0

    .line 302
    .local v0, "enforcementService":Lcom/microsoft/xbox/service/enforcement/IEnforcementService;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->reportUserData:Lcom/microsoft/xbox/service/model/sls/ReportUserData;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->reportTargetId:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/service/enforcement/IEnforcementService;->sendReport(Lcom/microsoft/xbox/service/model/sls/ReportUserData;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 306
    :goto_0
    return-object v2

    .line 302
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 305
    :catch_0
    move-exception v1

    .line 306
    .local v1, "xex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 294
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->access$902(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Z)Z

    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 279
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->access$902(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Z)Z

    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 290
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 261
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->access$902(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Z)Z

    .line 284
    return-void
.end method
