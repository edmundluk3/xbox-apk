.class public Lcom/microsoft/xbox/xle/ui/LabeledArcView;
.super Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
.source "LabeledArcView.java"


# instance fields
.field private arcColor:I

.field private arcPercentage:F

.field private boundingRectf:Landroid/graphics/RectF;

.field private paint:Landroid/graphics/Paint;

.field private strokeWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->paint:Landroid/graphics/Paint;

    .line 21
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->boundingRectf:Landroid/graphics/RectF;

    .line 27
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->LabeledArcView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 28
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v1, 0xe

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->strokeWidth:F

    .line 29
    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->arcColor:I

    .line 30
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 32
    const/16 v1, 0x11

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->setGravity(I)V

    .line 34
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->paint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 35
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->paint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->strokeWidth:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 36
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->setBackgroundColor(I)V

    .line 37
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 47
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 49
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->strokeWidth:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->strokeWidth:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float v6, v0, v1

    .line 50
    .local v6, "halfStrokeWidth":F
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->boundingRectf:Landroid/graphics/RectF;

    iput v6, v0, Landroid/graphics/RectF;->left:F

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->boundingRectf:Landroid/graphics/RectF;

    iput v6, v0, Landroid/graphics/RectF;->top:F

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->boundingRectf:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->boundingRectf:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->paint:Landroid/graphics/Paint;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0c00ce

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->boundingRectf:Landroid/graphics/RectF;

    const/high16 v3, 0x43b40000    # 360.0f

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->arcColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->boundingRectf:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->arcPercentage:F

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 60
    return-void

    .end local v6    # "halfStrokeWidth":F
    :cond_0
    move v6, v2

    .line 49
    goto :goto_0
.end method

.method public setPercentage(I)V
    .locals 3
    .param p1, "percentage"    # I

    .prologue
    .line 40
    const/16 v1, 0x64

    const/4 v2, 0x0

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 41
    .local v0, "clampedPercentage":I
    const/high16 v1, 0x43b40000    # 360.0f

    int-to-float v2, v0

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->arcPercentage:F

    .line 42
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    return-void
.end method
