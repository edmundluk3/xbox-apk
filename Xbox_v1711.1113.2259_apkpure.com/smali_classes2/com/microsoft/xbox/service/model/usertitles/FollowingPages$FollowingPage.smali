.class public Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;
.super Ljava/lang/Object;
.source "FollowingPages.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FollowingPage"
.end annotation


# instance fields
.field public final followedDateTimeUtc:Ljava/util/Date;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private transient hashCode:I

.field public final pageId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Date;Ljava/lang/String;)V
    .locals 0
    .param p1, "followedDateTimeUtc"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "pageId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;->followedDateTimeUtc:Ljava/util/Date;

    .line 30
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;->pageId:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 35
    if-ne p1, p0, :cond_0

    .line 36
    const/4 v1, 0x1

    .line 41
    :goto_0
    return v1

    .line 37
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;

    if-nez v1, :cond_1

    .line 38
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 40
    check-cast v0, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;

    .line 41
    .local v0, "other":Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;->pageId:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;->pageId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 47
    iget v0, p0, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;->hashCode:I

    if-nez v0, :cond_0

    .line 48
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;->hashCode:I

    .line 49
    iget v0, p0, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;->pageId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;->hashCode:I

    .line 52
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;->hashCode:I

    return v0
.end method
