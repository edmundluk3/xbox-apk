.class public abstract Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;
.super Ljava/lang/Object;
.source "HomeScreenSpinnerItem.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# static fields
.field private static ITEM_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getItemList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    const-class v3, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;->ITEM_LIST:Ljava/util/List;

    if-nez v2, :cond_2

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 80
    .local v0, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->values()[Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v4

    array-length v4, v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v2, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;->ITEM_LIST:Ljava/util/List;

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;->getSelectableValues()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    .line 87
    .local v1, "preference":Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
    sget-object v4, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Friends:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    if-ne v1, v4, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v4

    if-nez v4, :cond_0

    .line 88
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;->ITEM_LIST:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;->with(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 78
    .end local v1    # "preference":Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 93
    :cond_2
    :try_start_1
    sget-object v2, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;->ITEM_LIST:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v3

    return-object v2
.end method

.method private static getSelectableValues()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->ActivityFeed:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Friends:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Achievements:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Trending:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Clubs:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Messages:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static with(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;
    .locals 1
    .param p0, "preference"    # Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 42
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 43
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/AutoValue_HomeScreenSpinnerItem;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/settings/AutoValue_HomeScreenSpinnerItem;-><init>(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 49
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem$1;->$SwitchMap$com$microsoft$xbox$data$repository$homescreen$HomeScreenPreference:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;->homeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 66
    const-string v0, "Unknown home screen preference"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 67
    const-string v0, ""

    :goto_0
    return-object v0

    .line 51
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070438

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 53
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f07043c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 55
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070437

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 57
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070446

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 59
    :pswitch_4
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f07032c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 61
    :pswitch_5
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070441

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 63
    :pswitch_6
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070ccd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;->homeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract homeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
