.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubAdminHomeScreenViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private bannedCount:J

.field private membersCount:J

.field private reportsCount:J

.field private requestsCount:J

.field private settingsVisible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 38
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminHomeScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 40
    return-void
.end method

.method private navigateTo(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;-><init>(J)V

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 89
    return-void
.end method


# virtual methods
.method public getBannedCount()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->bannedCount:J

    return-wide v0
.end method

.method public getMembersCount()J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->membersCount:J

    return-wide v0
.end method

.method public getReportsCount()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->reportsCount:J

    return-wide v0
.end method

.method public getRequestsCount()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->requestsCount:J

    return-wide v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method public isSettingsVisible()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->settingsVisible:Z

    return v0
.end method

.method public loadOverride(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 111
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 113
    return-void
.end method

.method public navigateToBanned()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminBanned(J)V

    .line 79
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreen;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->navigateTo(Ljava/lang/Class;)V

    .line 80
    return-void
.end method

.method public navigateToMembers()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminMembersModerators(J)V

    .line 74
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreen;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->navigateTo(Ljava/lang/Class;)V

    .line 75
    return-void
.end method

.method public navigateToReports()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminReports(J)V

    .line 69
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreen;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->navigateTo(Ljava/lang/Class;)V

    .line 70
    return-void
.end method

.method public navigateToRequests()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminRequestsRecommendations(J)V

    .line 64
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreen;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->navigateTo(Ljava/lang/Class;)V

    .line 65
    return-void
.end method

.method public navigateToSettings()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettings(J)V

    .line 84
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreen;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->navigateTo(Ljava/lang/Class;)V

    .line 85
    return-void
.end method

.method protected onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v1, 0x0

    .line 117
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 149
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->updateAdapter()V

    .line 150
    return-void

    .line 121
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 122
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v2

    .line 123
    .local v2, "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v1

    .line 125
    .local v1, "clubRoster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    :cond_1
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 126
    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 127
    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 128
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-ne v3, v4, :cond_3

    .line 129
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->InvalidState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .end local v1    # "clubRoster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    .end local v2    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_2
    move-object v2, v1

    .line 122
    goto :goto_1

    .line 131
    .restart local v1    # "clubRoster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    .restart local v2    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_3
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 132
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->requestedToJoin()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->recommended()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v3, v4

    int-to-long v4, v3

    iput-wide v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->requestsCount:J

    .line 133
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->reportedItemsCount()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->reportsCount:J

    .line 134
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->membersCount:J

    .line 135
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->banned()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    int-to-long v4, v3

    iput-wide v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->bannedCount:J

    .line 136
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->settingsVisible:Z

    goto/16 :goto_0

    .line 144
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "clubRoster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    .end local v2    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :pswitch_1
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 145
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->TAG:Ljava/lang/String;

    const-string v4, "Club model changed to invalid state."

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 93
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminHomeScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 94
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetActive()V

    .line 100
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 102
    :cond_0
    return-void
.end method
