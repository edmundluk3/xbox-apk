.class public Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ExoVideoPlayerActivityViewModel.java"

# interfaces
.implements Lcom/google/android/exoplayer2/ExoPlayer$EventListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected final dataSource:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field protected gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private isBuffering:Z

.field private model:Lcom/microsoft/xbox/service/model/ProfileModel;

.field protected viewCountIncremented:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 44
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->viewCountIncremented:Z

    .line 45
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->isBuffering:Z

    .line 48
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedDataSource()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->dataSource:Ljava/lang/String;

    .line 49
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->dataSource:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 50
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->dataSource:Ljava/lang/String;

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 51
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 52
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getGameClip()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 53
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->getAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 54
    return-void
.end method

.method private findUriType()Ljava/lang/String;
    .locals 5

    .prologue
    .line 122
    const-string v1, ""

    .line 123
    .local v1, "uriType":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->dataSource:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 124
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;

    .line 125
    .local v0, "gcu":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->dataSource:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 126
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uriType:Ljava/lang/String;

    .line 131
    .end local v0    # "gcu":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    :cond_1
    return-object v1
.end method

.method static synthetic lambda$incrementViewCounter$0(Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;
    .param p1, "gameClip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 108
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->viewCountIncremented:Z

    .line 109
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 110
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->updateAdapter()V

    .line 111
    return-void
.end method

.method static synthetic lambda$incrementViewCounter$1(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 113
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to increment game clip"

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public getAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;)V

    return-object v0
.end method

.method public getDataSource()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->dataSource:Ljava/lang/String;

    return-object v0
.end method

.method protected incrementViewCounter()V
    .locals 5

    .prologue
    .line 96
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->TAG:Ljava/lang/String;

    const-string v2, "incrementViewCounter()"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    if-eqz v1, :cond_0

    .line 98
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 99
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "FileFormat"

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->findUriType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 100
    const-string v1, "Game DVR View Count"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 103
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->incrementGameClipViewCount(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)Lio/reactivex/Single;

    move-result-object v2

    .line 104
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    .line 105
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 106
    invoke-virtual {v2, v3, v4}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    .line 102
    invoke-virtual {v1, v2}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 119
    .end local v0    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :goto_0
    return-void

    .line 117
    :cond_0
    const-string v1, "We should not track progress when there is no gameDvrData"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->isBuffering:Z

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 89
    return-void
.end method

.method public onLoadingChanged(Z)V
    .locals 0
    .param p1, "isLoading"    # Z

    .prologue
    .line 148
    return-void
.end method

.method public onPlayerError(Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 8
    .param p1, "error"    # Lcom/google/android/exoplayer2/ExoPlaybackException;

    .prologue
    .line 168
    new-instance v6, Lcom/google/gson/JsonObject;

    invoke-direct {v6}, Lcom/google/gson/JsonObject;-><init>()V

    .line 169
    .local v6, "callStackJson":Lcom/google/gson/JsonObject;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->dataSource:Ljava/lang/String;

    .line 170
    .local v7, "videoUri":Ljava/lang/String;
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onPlayerError called"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v0, "VideoUri"

    invoke-virtual {v6, v0, v7}, Lcom/google/gson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v0, "ErrorType"

    iget v1, p1, Lcom/google/android/exoplayer2/ExoPlaybackException;->type:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/google/gson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/Number;)V

    .line 174
    const-string v0, "ErrorMessage"

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ExoPlaybackException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/google/gson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string v0, "Game DVR Playback"

    const-string v1, ""

    invoke-virtual {v6}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->TAG:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const/4 v1, 0x0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704c7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070738

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;)V

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->showMustActDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Z)V

    .line 184
    return-void
.end method

.method public onPlayerStateChanged(ZI)V
    .locals 2
    .param p1, "playWhenReady"    # Z
    .param p2, "playbackState"    # I

    .prologue
    .line 152
    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    .line 153
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Started playing video"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->viewCountIncremented:Z

    if-nez v0, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->incrementViewCounter()V

    .line 156
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Game DVR Start"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->endTrackPerformance(Ljava/lang/String;)V

    .line 162
    :cond_0
    :goto_0
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->isBuffering:Z

    .line 163
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->updateAdapter()V

    .line 164
    return-void

    .line 158
    :cond_1
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 159
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Ended playing video"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->goBack()V

    goto :goto_0

    .line 162
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onPositionDiscontinuity()V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->getAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 59
    return-void
.end method

.method protected onStartOverride()V
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    if-eqz v0, :cond_0

    .line 64
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getting ProfileModel for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "No gameClip => no need for ProfileModel"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method public onTimelineChanged(Lcom/google/android/exoplayer2/Timeline;Ljava/lang/Object;)V
    .locals 0
    .param p1, "timeline"    # Lcom/google/android/exoplayer2/Timeline;
    .param p2, "manifest"    # Ljava/lang/Object;

    .prologue
    .line 138
    return-void
.end method

.method public onTracksChanged(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/TrackSelectionArray;)V
    .locals 0
    .param p1, "trackGroups"    # Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .param p2, "trackSelections"    # Lcom/google/android/exoplayer2/trackselection/TrackSelectionArray;

    .prologue
    .line 143
    return-void
.end method
