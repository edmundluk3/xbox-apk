.class Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter$1;
.super Ljava/lang/Object;
.source "ClubWhosHereScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->values()[Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    move-result-object v0

    .line 91
    .local v0, "values":[Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;
    if-ltz p3, :cond_0

    array-length v1, v0

    if-ge p3, v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->values()[Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    move-result-object v2

    aget-object v2, v2, p3

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->setFilter(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;)V

    .line 94
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
