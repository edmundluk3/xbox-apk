.class final synthetic Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$5;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# static fields
.field private static final instance:Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$5;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$5;

    invoke-direct {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$5;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$5;->instance:Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$5;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/Function;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$5;->instance:Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$5;

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->serializedName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
