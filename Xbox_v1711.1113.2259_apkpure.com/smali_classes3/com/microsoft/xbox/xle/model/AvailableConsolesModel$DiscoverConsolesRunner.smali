.class Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "AvailableConsolesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DiscoverConsolesRunner"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

.field private defaultConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

.field private discoverDone:Lcom/microsoft/xbox/toolkit/Ready;

.field private discoverListener:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;

.field private discoveredConsoles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;"
        }
    .end annotation
.end field

.field private neverList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;"
        }
    .end annotation
.end field

.field private shouldLoadAutoConnectConsoles:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;Z)V
    .locals 2
    .param p2, "caller"    # Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;
    .param p3, "shouldLoadAutoConnectConsoles"    # Z

    .prologue
    const/4 v1, 0x0

    .line 210
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->this$0:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoveredConsoles:Ljava/util/List;

    .line 205
    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->defaultConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 211
    iput-object p2, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->caller:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    .line 212
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->shouldLoadAutoConnectConsoles:Z

    .line 213
    new-instance v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;-><init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoverListener:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;

    .line 214
    new-instance v0, Lcom/microsoft/xbox/toolkit/Ready;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/Ready;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoverDone:Lcom/microsoft/xbox/toolkit/Ready;

    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoverDone:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 216
    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;)Lcom/microsoft/xbox/toolkit/Ready;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoverDone:Lcom/microsoft/xbox/toolkit/Ready;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoveredConsoles:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;Lcom/microsoft/xbox/xle/model/ConsoleData;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 200
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->getIsNotAutoConnect(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v0

    return v0
.end method

.method private getIsNotAutoConnect(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z
    .locals 6
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    const/4 v2, 0x0

    .line 334
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->neverList:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->neverList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 335
    :cond_0
    const-string v3, "AvailableConsolesModel"

    const-string v4, "never list is empty"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :goto_0
    return v2

    .line 339
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->neverList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 340
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->neverList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 341
    .local v0, "c":Lcom/microsoft/xbox/xle/model/ConsoleData;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 342
    const/4 v2, 0x1

    goto :goto_0

    .line 339
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 346
    .end local v0    # "c":Lcom/microsoft/xbox/xle/model/ConsoleData;
    :cond_3
    const-string v3, "AvailableConsolesModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "did not find in neverlist "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    .line 220
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->getPlatformReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/Ready;->waitForReady()V

    .line 222
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->shouldLoadAutoConnectConsoles:Z

    if-eqz v4, :cond_3

    .line 223
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "autoconnect_v1.json"

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 224
    .local v0, "autoconnectFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 226
    :try_start_0
    new-instance v3, Lcom/google/gson/Gson;

    invoke-direct {v3}, Lcom/google/gson/Gson;-><init>()V

    .line 227
    .local v3, "gson":Lcom/google/gson/Gson;
    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    new-instance v5, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$1;-><init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;)V

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->neverList:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    .end local v3    # "gson":Lcom/google/gson/Gson;
    :goto_0
    const-string v4, "AvailableConsolesModel"

    const-string v5, "neverlist loaded. "

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    .end local v0    # "autoconnectFile":Ljava/io/File;
    :cond_0
    :goto_1
    const-string v4, "AvailableConsolesModel"

    const-string v5, "calling discover"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoverListener:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->addListener(Ljava/lang/Object;)V

    .line 244
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->this$0:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    iput-boolean v10, v4, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->isDiscovering:Z

    .line 246
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->discover()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 252
    :goto_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoverDone:Lcom/microsoft/xbox/toolkit/Ready;

    const/16 v5, 0x2710

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/Ready;->waitForReady(I)V

    .line 253
    const-string v4, "AvailableConsolesModel"

    const-string v5, "wake up"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoveredConsoles:Ljava/util/List;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoveredConsoles:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_4

    .line 257
    :cond_1
    const-string v4, "AvailableConsolesModel"

    const-string v5, "Did not find any console"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_2
    :goto_3
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    return-object v4

    .line 228
    .restart local v0    # "autoconnectFile":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 229
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "AvailableConsolesModel"

    const-string v5, "Failed to load never list json: "

    invoke-static {v4, v5, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 230
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 238
    .end local v0    # "autoconnectFile":Ljava/io/File;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->this$0:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->access$500(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->neverList:Ljava/util/List;

    .line 239
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->neverList:Ljava/util/List;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    goto :goto_1

    .line 261
    :cond_4
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoveredConsoles:Ljava/util/List;

    new-instance v5, Lcom/microsoft/xbox/xle/model/ConsoleData$ConsoleComparator;

    invoke-direct {v5}, Lcom/microsoft/xbox/xle/model/ConsoleData$ConsoleComparator;-><init>()V

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 262
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoveredConsoles:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 263
    .local v1, "console":Lcom/microsoft/xbox/xle/model/ConsoleData;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isAutoConnect()Z

    move-result v5

    if-eqz v5, :cond_5

    iget-wide v6, v1, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    .line 264
    const-string v4, "AvailableConsolesModel"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mark console as default "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->defaultConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    goto :goto_3

    .line 247
    .end local v1    # "console":Lcom/microsoft/xbox/xle/model/ConsoleData;
    :catch_1
    move-exception v4

    goto/16 :goto_2
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 296
    const-wide/16 v0, 0x1b5a

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 282
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoverListener:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->removeListener(Ljava/lang/Object;)V

    .line 285
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->neverList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->neverList:Ljava/util/List;

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoveredConsoles:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 290
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->neverList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->caller:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->defaultConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->discoveredConsoles:Ljava/util/List;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->neverList:Ljava/util/List;

    invoke-static {v0, p1, v1, v2, v3}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->access$600(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/model/ConsoleData;Ljava/util/List;Ljava/util/List;)V

    .line 292
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 278
    return-void
.end method
