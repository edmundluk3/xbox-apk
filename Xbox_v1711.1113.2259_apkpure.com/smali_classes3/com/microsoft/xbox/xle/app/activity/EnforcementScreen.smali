.class public Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "EnforcementScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "Enforcement"

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 24
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 26
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;->onCreateContentView()V

    .line 27
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;->setShowUtilityBar(Z)V

    .line 29
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 30
    .local v0, "enforcementViewModel":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 31
    return-void
.end method

.method public onCreateContentView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    const v0, 0x7f0300e9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;->setContentView(I)V

    .line 37
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;->setShowUtilityBar(Z)V

    .line 38
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;->setShowRightPane(Z)V

    .line 39
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method
