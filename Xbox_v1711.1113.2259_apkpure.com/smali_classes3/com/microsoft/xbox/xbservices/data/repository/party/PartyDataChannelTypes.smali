.class public final Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes;
.super Ljava/lang/Object;
.source "PartyDataChannelTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$PartyDataChannelMessage;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type cannot be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
