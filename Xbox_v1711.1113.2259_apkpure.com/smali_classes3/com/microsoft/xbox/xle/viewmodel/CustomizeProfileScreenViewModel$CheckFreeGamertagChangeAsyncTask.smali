.class Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "CustomizeProfileScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckFreeGamertagChangeAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field isFreeChangeAvailable:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
    .locals 1

    .prologue
    .line 819
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 821
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->isFreeChangeAvailable:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$1;

    .prologue
    .line 819
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 825
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 826
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 855
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    .line 860
    .local v1, "serviceManager":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    :try_start_0
    const-string v2, ""

    const/4 v3, 0x1

    const-string v4, "0"

    invoke-interface {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->changeGamertag(Ljava/lang/String;ZLjava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->isFreeChangeAvailable:Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 865
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v2

    .line 861
    :catch_0
    move-exception v0

    .line 862
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 819
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 850
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 819
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 1

    .prologue
    .line 831
    const-string v0, "ChangeGamertagAsyncTask should always run if requested"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 832
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 844
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$1402(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z

    .line 845
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->isFreeChangeAvailable:Z

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$1500(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V

    .line 846
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 819
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 836
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 837
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$1402(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z

    .line 839
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateGamertagPickerDialog()V

    .line 840
    return-void
.end method
