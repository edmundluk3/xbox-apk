.class public Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;
.super Landroid/widget/LinearLayout;
.source "ClubTypeDescriptionView.java"


# instance fields
.field private anyoneCanTextView:Landroid/widget/TextView;

.field private anyoneCanTitleTextView:Landroid/widget/TextView;

.field private membersCanTextView:Landroid/widget/TextView;

.field private titleTextView:Lcom/microsoft/xbox/xle/ui/IconFontButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 39
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->setOrientation(I)V

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020098

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 42
    const/high16 v4, 0x41200000    # 10.0f

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    .line 42
    invoke-static {v6, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v2, v4

    .line 44
    .local v2, "padding":I
    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->setPadding(IIII)V

    .line 46
    const-string v4, "layout_inflater"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 47
    .local v1, "layoutInflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030078

    invoke-virtual {v1, v4, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 49
    const v4, 0x7f0e031b

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->titleTextView:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 50
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->titleTextView:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0b0016

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconFontSize(I)V

    .line 52
    const v4, 0x7f0e031c

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->anyoneCanTitleTextView:Landroid/widget/TextView;

    .line 53
    const v4, 0x7f0e031d

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->anyoneCanTextView:Landroid/widget/TextView;

    .line 54
    const v4, 0x7f0e031e

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->membersCanTextView:Landroid/widget/TextView;

    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xboxone/smartglass/R$styleable;->ClubTypeDescriptionView:[I

    invoke-virtual {v4, p2, v5, v7, v7}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 58
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 59
    .local v3, "selectedType":I
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->values()[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v4

    aget-object v4, v4, v3

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->setClubType(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 63
    return-void

    .line 61
    .end local v3    # "selectedType":I
    :catchall_0
    move-exception v4

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v4
.end method


# virtual methods
.method public setClubType(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V
    .locals 4
    .param p1, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v3, 0x7f07016f

    .line 66
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 67
    sget-object v0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 90
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Unknown club type"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 69
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->titleTextView:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07019b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->titleTextView:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070f64

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->anyoneCanTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->anyoneCanTextView:Landroid/widget/TextView;

    const v1, 0x7f07019a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->membersCanTextView:Landroid/widget/TextView;

    const v1, 0x7f07019c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 92
    :goto_0
    return-void

    .line 76
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->titleTextView:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070198

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->titleTextView:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070f79

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->anyoneCanTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->anyoneCanTextView:Landroid/widget/TextView;

    const v1, 0x7f070197

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->membersCanTextView:Landroid/widget/TextView;

    const v1, 0x7f070199

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 83
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->titleTextView:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07018d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->titleTextView:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070efe

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->anyoneCanTitleTextView:Landroid/widget/TextView;

    const v1, 0x7f070191

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->anyoneCanTextView:Landroid/widget/TextView;

    const v1, 0x7f07018c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->membersCanTextView:Landroid/widget/TextView;

    const v1, 0x7f07018e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
