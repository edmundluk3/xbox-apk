.class public Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
.super Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
.source "IRecent360ProgressAndAchievementResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Recent360ProgressAndAchievementItem"
.end annotation


# instance fields
.field public currentAchievements:I

.field public lastPlayed:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public platforms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public sequence:I

.field public titleId:I

.field public titleType:I

.field public totalAchievements:I

.field public totalGamerscore:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;-><init>()V

    .line 29
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/SLSResponseType;->Xbox360:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    .line 30
    return-void
.end method


# virtual methods
.method public getCompletionPercentage()I
    .locals 4

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->getCurrentGamerscore()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->totalGamerscore:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-double v0, v2

    .line 44
    .local v0, "percent":D
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    :goto_0
    return v2

    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    goto :goto_0
.end method

.method public getEarnedAchievements()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->currentAchievements:I

    return v0
.end method

.method public getLastPlayed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->lastPlayed:Ljava/util/Date;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateUptoWeekToDurationNow(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMaxGamerscore()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->totalGamerscore:I

    return v0
.end method

.method public getResponseType()Lcom/microsoft/xbox/service/network/managers/SLSResponseType;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    return-object v0
.end method
