.class public final Lcom/facebook/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f09012d

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f09012e

.field public static final abc_action_bar_default_height_material:I = 0x7f090001

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f09012f

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f090130

.field public static final abc_action_bar_elevation_material:I = 0x7f090155

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f090156

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f090157

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f090158

.field public static final abc_action_bar_progress_bar_size:I = 0x7f090002

.field public static final abc_action_bar_stacked_max_height:I = 0x7f090159

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f09015a

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f09015b

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f09015c

.field public static final abc_action_button_min_height_material:I = 0x7f09015d

.field public static final abc_action_button_min_width_material:I = 0x7f09015e

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f09015f

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f090000

.field public static final abc_button_inset_horizontal_material:I = 0x7f090160

.field public static final abc_button_inset_vertical_material:I = 0x7f090161

.field public static final abc_button_padding_horizontal_material:I = 0x7f090162

.field public static final abc_button_padding_vertical_material:I = 0x7f090163

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f090164

.field public static final abc_config_prefDialogWidth:I = 0x7f090005

.field public static final abc_control_corner_material:I = 0x7f090165

.field public static final abc_control_inset_material:I = 0x7f090166

.field public static final abc_control_padding_material:I = 0x7f090167

.field public static final abc_dialog_fixed_height_major:I = 0x7f090006

.field public static final abc_dialog_fixed_height_minor:I = 0x7f090007

.field public static final abc_dialog_fixed_width_major:I = 0x7f090008

.field public static final abc_dialog_fixed_width_minor:I = 0x7f090009

.field public static final abc_dialog_min_width_major:I = 0x7f09000a

.field public static final abc_dialog_min_width_minor:I = 0x7f09000b

.field public static final abc_dialog_padding_material:I = 0x7f09016a

.field public static final abc_dialog_padding_top_material:I = 0x7f09016b

.field public static final abc_disabled_alpha_material_dark:I = 0x7f09016d

.field public static final abc_disabled_alpha_material_light:I = 0x7f09016e

.field public static final abc_dropdownitem_icon_width:I = 0x7f09016f

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f090170

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f090171

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f090172

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f090173

.field public static final abc_edit_text_inset_top_material:I = 0x7f090174

.field public static final abc_floating_window_z:I = 0x7f090175

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f090176

.field public static final abc_panel_menu_list_width:I = 0x7f090177

.field public static final abc_progress_bar_height_material:I = 0x7f090178

.field public static final abc_search_view_preferred_height:I = 0x7f090179

.field public static final abc_search_view_preferred_width:I = 0x7f09017a

.field public static final abc_seekbar_track_background_height_material:I = 0x7f09017b

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f09017c

.field public static final abc_select_dialog_padding_start_material:I = 0x7f09017d

.field public static final abc_switch_padding:I = 0x7f090148

.field public static final abc_text_size_body_1_material:I = 0x7f09017e

.field public static final abc_text_size_body_2_material:I = 0x7f09017f

.field public static final abc_text_size_button_material:I = 0x7f090180

.field public static final abc_text_size_caption_material:I = 0x7f090181

.field public static final abc_text_size_display_1_material:I = 0x7f090182

.field public static final abc_text_size_display_2_material:I = 0x7f090183

.field public static final abc_text_size_display_3_material:I = 0x7f090184

.field public static final abc_text_size_display_4_material:I = 0x7f090185

.field public static final abc_text_size_headline_material:I = 0x7f090186

.field public static final abc_text_size_large_material:I = 0x7f090187

.field public static final abc_text_size_medium_material:I = 0x7f090188

.field public static final abc_text_size_menu_header_material:I = 0x7f090189

.field public static final abc_text_size_menu_material:I = 0x7f09018a

.field public static final abc_text_size_small_material:I = 0x7f09018b

.field public static final abc_text_size_subhead_material:I = 0x7f09018c

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f090003

.field public static final abc_text_size_title_material:I = 0x7f09018d

.field public static final abc_text_size_title_material_toolbar:I = 0x7f090004

.field public static final cardview_compat_inset_shadow:I = 0x7f0901d1

.field public static final cardview_default_elevation:I = 0x7f0901d2

.field public static final cardview_default_radius:I = 0x7f0901d3

.field public static final com_facebook_auth_dialog_corner_radius:I = 0x7f0901f6

.field public static final com_facebook_auth_dialog_corner_radius_oversized:I = 0x7f0901f7

.field public static final com_facebook_button_corner_radius:I = 0x7f0901f8

.field public static final com_facebook_button_login_corner_radius:I = 0x7f0901f9

.field public static final com_facebook_likeboxcountview_border_radius:I = 0x7f0901fa

.field public static final com_facebook_likeboxcountview_border_width:I = 0x7f0901fb

.field public static final com_facebook_likeboxcountview_caret_height:I = 0x7f0901fc

.field public static final com_facebook_likeboxcountview_caret_width:I = 0x7f0901fd

.field public static final com_facebook_likeboxcountview_text_padding:I = 0x7f0901fe

.field public static final com_facebook_likeboxcountview_text_size:I = 0x7f0901ff

.field public static final com_facebook_likeview_edge_padding:I = 0x7f090200

.field public static final com_facebook_likeview_internal_padding:I = 0x7f090201

.field public static final com_facebook_likeview_text_size:I = 0x7f090202

.field public static final com_facebook_profilepictureview_preset_size_large:I = 0x7f090203

.field public static final com_facebook_profilepictureview_preset_size_normal:I = 0x7f090204

.field public static final com_facebook_profilepictureview_preset_size_small:I = 0x7f090205

.field public static final com_facebook_share_button_compound_drawable_padding:I = 0x7f090206

.field public static final com_facebook_share_button_padding_bottom:I = 0x7f090207

.field public static final com_facebook_share_button_padding_left:I = 0x7f090208

.field public static final com_facebook_share_button_padding_right:I = 0x7f090209

.field public static final com_facebook_share_button_padding_top:I = 0x7f09020a

.field public static final com_facebook_share_button_text_size:I = 0x7f09020b

.field public static final com_facebook_tooltip_horizontal_padding:I = 0x7f09020c

.field public static final disabled_alpha_material_dark:I = 0x7f09026a

.field public static final disabled_alpha_material_light:I = 0x7f09026b

.field public static final highlight_alpha_material_colored:I = 0x7f090325

.field public static final highlight_alpha_material_dark:I = 0x7f090326

.field public static final highlight_alpha_material_light:I = 0x7f090327

.field public static final hint_alpha_material_dark:I = 0x7f090328

.field public static final hint_alpha_material_light:I = 0x7f090329

.field public static final hint_pressed_alpha_material_dark:I = 0x7f09032a

.field public static final hint_pressed_alpha_material_light:I = 0x7f09032b

.field public static final notification_action_icon_size:I = 0x7f0903e5

.field public static final notification_action_text_size:I = 0x7f0903e6

.field public static final notification_big_circle_margin:I = 0x7f0903e7

.field public static final notification_content_margin_start:I = 0x7f090149

.field public static final notification_large_icon_height:I = 0x7f0903e8

.field public static final notification_large_icon_width:I = 0x7f0903e9

.field public static final notification_main_column_padding_top:I = 0x7f09014a

.field public static final notification_media_narrow_margin:I = 0x7f09014b

.field public static final notification_right_icon_size:I = 0x7f0903ea

.field public static final notification_right_side_padding_top:I = 0x7f090147

.field public static final notification_small_icon_background_padding:I = 0x7f0903eb

.field public static final notification_small_icon_size_as_large:I = 0x7f0903ec

.field public static final notification_subtext_size:I = 0x7f0903ed

.field public static final notification_top_pad:I = 0x7f0903ee

.field public static final notification_top_pad_large_text:I = 0x7f0903ef


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
