.class Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$PartyDetailItemDiffCallback;
.super Landroid/support/v7/util/DiffUtil$Callback;
.source "PartyDetailsViewImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PartyDetailItemDiffCallback"
.end annotation


# instance fields
.field private final newList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final oldList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 186
    .local p2, "oldList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;>;"
    .local p3, "newList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$PartyDetailItemDiffCallback;->this$0:Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;

    invoke-direct {p0}, Landroid/support/v7/util/DiffUtil$Callback;-><init>()V

    .line 187
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 188
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 189
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$PartyDetailItemDiffCallback;->oldList:Ljava/util/List;

    .line 190
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$PartyDetailItemDiffCallback;->newList:Ljava/util/List;

    .line 191
    return-void
.end method


# virtual methods
.method public areContentsTheSame(II)Z
    .locals 1
    .param p1, "oldItemPosition"    # I
    .param p2, "newItemPosition"    # I

    .prologue
    .line 221
    const/4 v0, 0x1

    return v0
.end method

.method public areItemsTheSame(II)Z
    .locals 4
    .param p1, "oldItemPosition"    # I
    .param p2, "newItemPosition"    # I

    .prologue
    .line 205
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 207
    const/4 v2, 0x1

    .line 216
    :goto_0
    return v2

    .line 208
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_2

    .line 210
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 213
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$PartyDetailItemDiffCallback;->oldList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    .line 214
    .local v1, "oldItem":Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$PartyDetailItemDiffCallback;->newList:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    .line 216
    .local v0, "newItem":Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->partyMember()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->partyMember()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method public getNewListSize()I
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$PartyDetailItemDiffCallback;->newList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getOldListSize()I
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$PartyDetailItemDiffCallback;->oldList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
