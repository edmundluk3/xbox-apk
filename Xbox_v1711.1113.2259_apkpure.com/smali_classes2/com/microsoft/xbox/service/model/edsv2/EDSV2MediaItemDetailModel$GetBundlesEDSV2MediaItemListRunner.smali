.class Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "EDSV2MediaItemDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetBundlesEDSV2MediaItemListRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<TU;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V
    .locals 0

    .prologue
    .line 912
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetBundlesEDSV2MediaItemListRunner;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;

    .prologue
    .line 912
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetBundlesEDSV2MediaItemListRunner;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 912
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetBundlesEDSV2MediaItemListRunner;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<TU;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 920
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetBundlesEDSV2MediaItemListRunner;"
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-boolean v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->IsPartOfAnyBundle:Z

    if-eqz v5, :cond_1

    .line 921
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 922
    .local v2, "productsLists":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;>;"
    const/4 v3, 0x0

    .line 924
    .local v3, "skipCount":I
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v3}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductInBundleList(Ljava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 925
    .local v0, "bundleList":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v5, v0}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductsReducedInfoFromList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v1

    .line 926
    .local v1, "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 927
    add-int/lit8 v3, v3, 0x19

    .line 929
    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->pagingInfo:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;

    if-eqz v5, :cond_0

    .line 930
    iget-object v5, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->pagingInfo:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;

    iget v4, v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;->totalItems:I

    .line 931
    .local v4, "totalCount":I
    :goto_0
    if-le v4, v3, :cond_0

    .line 932
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v3}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductInBundleList(Ljava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 933
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v5, v0}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductsReducedInfoFromList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v1

    .line 934
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 935
    add-int/lit8 v3, v3, 0x19

    goto :goto_0

    .line 939
    .end local v4    # "totalCount":I
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getListFromBigCatProductLists(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v5

    return-object v5

    .line 942
    .end local v0    # "bundleList":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .end local v1    # "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .end local v2    # "productsLists":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;>;"
    .end local v3    # "skipCount":I
    :cond_1
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x2199

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v5
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 952
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetBundlesEDSV2MediaItemListRunner;"
    const-wide/16 v0, 0x2199

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TU;>;>;)V"
        }
    .end annotation

    .prologue
    .line 947
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetBundlesEDSV2MediaItemListRunner;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<TU;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->access$1000(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 948
    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .prologue
    .line 915
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetBundlesEDSV2MediaItemListRunner;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingBundles:Z

    .line 916
    return-void
.end method
