.class public final Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$PlaceholderTag;
.super Ljava/lang/Object;
.source "EditorialDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlaceholderTag"
.end annotation


# static fields
.field public static final transient PLACEHOLDER_ID:Ljava/lang/String; = "placeholder"


# instance fields
.field public final tagCount:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "tagCount"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$PlaceholderTag;->tagCount:I

    .line 44
    return-void
.end method


# virtual methods
.method public getDisplayText()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 61
    iget v0, p0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$PlaceholderTag;->tagCount:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 49
    const-string v0, "placeholder"

    return-object v0
.end method

.method public getSymbol()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 55
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070e9a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
