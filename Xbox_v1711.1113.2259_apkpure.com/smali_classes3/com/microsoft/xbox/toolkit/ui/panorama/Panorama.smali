.class public Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;
.super Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;
.source "Panorama.java"


# static fields
.field private static final ANIMATION_MS:I = 0x96

.field private static final TOUCH_BLOCK_TIMEOUT_MS:I = 0x7530


# instance fields
.field private animating:Z

.field private background:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

.field private isClipChildren:Z

.field private isClipToPadding:Z

.field private onCurrentPaneChanged:Ljava/lang/Runnable;

.field private savedX:I

.field private scrollPaddingLeft:I

.field private scrollPaddingRight:I

.field private scrolling:Z

.field private scrollingRunner:Ljava/lang/Runnable;

.field private startPaneIndex:I

.field touchX0:F

.field touchY0:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 78
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;-><init>(Landroid/content/Context;)V

    .line 54
    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    .line 55
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrolling:Z

    .line 56
    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrollingRunner:Ljava/lang/Runnable;

    .line 57
    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->onCurrentPaneChanged:Ljava/lang/Runnable;

    .line 58
    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->background:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 59
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->savedX:I

    .line 66
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->touchX0:F

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->touchY0:F

    .line 69
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->startPaneIndex:I

    .line 70
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->animating:Z

    .line 81
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v7, -0x40800000    # -1.0f

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 91
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    .line 55
    iput-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrolling:Z

    .line 56
    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrollingRunner:Ljava/lang/Runnable;

    .line 57
    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->onCurrentPaneChanged:Ljava/lang/Runnable;

    .line 58
    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->background:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 59
    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->savedX:I

    .line 66
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->touchX0:F

    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->touchY0:F

    .line 69
    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->startPaneIndex:I

    .line 70
    iput-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->animating:Z

    .line 93
    const/4 v4, 0x2

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    .line 96
    .local v2, "clipAttrs":[I
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 97
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->isClipChildren:Z

    .line 98
    const/4 v3, 0x1

    .line 99
    .local v3, "clipPaddingResId":I
    invoke-virtual {v0, v3, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->isClipToPadding:Z

    .line 100
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 102
    sget-object v4, Lcom/microsoft/xboxone/smartglass/R$styleable;->Panorama:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 103
    .local v1, "array":Landroid/content/res/TypedArray;
    invoke-virtual {v1, v5, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrollPaddingLeft:I

    .line 104
    invoke-virtual {v1, v6, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrollPaddingRight:I

    .line 105
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 106
    return-void

    .line 93
    :array_0
    .array-data 4
        0x10100ea
        0x10100eb
    .end array-data
.end method

.method private setScrolling(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrolling:Z

    if-eq v0, p1, :cond_0

    .line 163
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrolling:Z

    .line 165
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrolling:Z

    if-eqz v0, :cond_1

    .line 166
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->PivotScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x7530

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    .line 172
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrollingRunner:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrollingRunner:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 176
    :cond_0
    return-void

    .line 168
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->PivotScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->clearBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;)V

    goto :goto_0
.end method


# virtual methods
.method public addPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "index"    # I

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->addPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V

    .line 147
    return-void
.end method

.method public adjustBottomMargin(I)V
    .locals 1
    .param p1, "bottomMargin"    # I

    .prologue
    .line 321
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->adjustBottomMargin(I)V

    .line 322
    return-void
.end method

.method public adjustPaneSize(Ljava/lang/Class;I)V
    .locals 1
    .param p2, "percent"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 353
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->adjustPaneSize(Ljava/lang/Class;I)V

    .line 355
    return-void
.end method

.method public animateToCurrentPaneIndex(I)V
    .locals 1
    .param p1, "newPane"    # I

    .prologue
    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scrollToChild(I)V

    .line 202
    return-void
.end method

.method public forceRefresh()V
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->forceRefresh()V

    .line 360
    return-void
.end method

.method public getAnimateIn(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 5
    .param p1, "goingBack"    # Z

    .prologue
    .line 295
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getAnimateIn(Z)Ljava/util/ArrayList;

    move-result-object v2

    .line 297
    .local v2, "animations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 298
    new-instance v1, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;-><init>()V

    .line 299
    .local v1, "animationPackage":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    .line 300
    .local v0, "animation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;->add(Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;)V

    goto :goto_0

    .line 304
    .end local v0    # "animation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    .end local v1    # "animationPackage":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1
.end method

.method public getAnimateOut(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 5
    .param p1, "goingBack"    # Z

    .prologue
    .line 308
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getAnimateOut(Z)Ljava/util/ArrayList;

    move-result-object v2

    .line 310
    .local v2, "animations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 311
    new-instance v1, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;-><init>()V

    .line 312
    .local v1, "animationPackage":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    .line 313
    .local v0, "animation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;->add(Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;)V

    goto :goto_0

    .line 317
    .end local v0    # "animation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    .end local v1    # "animationPackage":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1
.end method

.method public getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->getCurrentPaneIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getPane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 215
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPaneIndex()I
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getFirstVisibleChild()I

    move-result v0

    return v0
.end method

.method public getIndexOfScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)I
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 348
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getIndexOfScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)I

    move-result v0

    return v0
.end method

.method public getIndexOfScreen(Ljava/lang/Class;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 329
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getIndexOfScreen(Ljava/lang/Class;)I

    move-result v0

    return v0
.end method

.method protected getInitialPanes()[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 4

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->getChildCount()I

    move-result v3

    new-array v2, v3, [Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 138
    .local v2, "panes":[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 139
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 140
    .local v1, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    aput-object v1, v2, v0

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    .end local v1    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-object v2
.end method

.method public getIsScrolling()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrolling:Z

    return v0
.end method

.method public getTotalPaneCount()I
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->size()I

    move-result v0

    return v0
.end method

.method protected initialize()V
    .locals 5

    .prologue
    .line 113
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    .line 116
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->getInitialPanes()[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    .line 119
    .local v1, "panes":[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->removeAllViews()V

    .line 122
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->initialize([Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 123
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->isClipChildren:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->setClipChildren(Z)V

    .line 124
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->isClipToPadding:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->setClipToPadding(Z)V

    .line 125
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrollPaddingLeft:I

    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrollPaddingRight:I

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->setScrollPadding(II)V

    .line 128
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 129
    .local v0, "bodyParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 130
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 131
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {p0, v2, v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 133
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->setScrolling(Z)V

    .line 134
    return-void
.end method

.method public onAnimateInCompleted()V
    .locals 3

    .prologue
    .line 252
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 253
    .local v0, "bodyWeakPtr":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama$1;

    invoke-direct {v2, p0, v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->postRunnableAfterReady(Ljava/lang/Runnable;)V

    .line 262
    return-void
.end method

.method public onAnimateInStarted()V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->onAnimateInStarted()V

    .line 249
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->onCreate()V

    .line 229
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 344
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->onDestroy()V

    .line 292
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->onPause()V

    .line 241
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->onRehydrate()V

    .line 271
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->onResume()V

    .line 245
    return-void
.end method

.method public onSetActive()V
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->setActive()V

    .line 279
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->savedX:I

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->savedX:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->setScrollX(I)V

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->startPaneIndex:I

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->startPaneIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scrollToChild(I)V

    goto :goto_0
.end method

.method public onSetActive(I)V
    .locals 0
    .param p1, "PaneIndex"    # I

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->onSetActive()V

    .line 275
    return-void
.end method

.method public onSetInactive()V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->setInactive()V

    .line 288
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->onStart()V

    .line 233
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->onStop()V

    .line 237
    return-void
.end method

.method public onTombstone()V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->savedX:I

    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->onTombstone()V

    .line 267
    return-void
.end method

.method public removePane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->removePane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    return-object v0
.end method

.method public resetBottomMargin()V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->resetBottomMargin()V

    .line 326
    return-void
.end method

.method public setActiveOnAdd(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 0
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 369
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetActive()V

    .line 370
    return-void
.end method

.method public setCurrentPaneIndex(I)V
    .locals 1
    .param p1, "newPane"    # I

    .prologue
    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scrollToChild(I)V

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->onCurrentPaneChanged:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->onCurrentPaneChanged:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 189
    :cond_0
    return-void
.end method

.method public setOnCurrentPaneChangedRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->onCurrentPaneChanged:Ljava/lang/Runnable;

    .line 225
    return-void
.end method

.method public setOnScrollingChangedRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runner"    # Ljava/lang/Runnable;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->scrollingRunner:Ljava/lang/Runnable;

    .line 159
    return-void
.end method

.method public setScreenState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 364
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->setScreenState(I)V

    .line 365
    return-void
.end method

.method public setStartPaneIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 197
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->startPaneIndex:I

    .line 198
    return-void
.end method

.method public switchToPane(I)V
    .locals 1
    .param p1, "newIndex"    # I

    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->animating:Z

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    .line 334
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/Panorama;->body:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scrollToChild(I)V

    .line 336
    :cond_0
    return-void
.end method
