.class public Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;
.super Ljava/lang/Object;
.source "BlockExplicitContentPerShowInfo.java"


# instance fields
.field public blockExplicitContentPerShow:Z

.field public userCanViewShow:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->blockExplicitContentPerShow:Z

    return-void
.end method

.method public static parse(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;
    .locals 4
    .param p0, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 10
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;-><init>()V

    .line 11
    .local v0, "info":Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;
    const-string v2, "BlockExplicitContentPerShowInfo"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 12
    .local v1, "params":Lorg/json/JSONObject;
    if-eqz v1, :cond_0

    .line 13
    const-string v2, "blockExplicitContentPerShow"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v0, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->blockExplicitContentPerShow:Z

    .line 14
    const-string v2, "userCanViewShow"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->userCanViewShow:Ljava/lang/String;

    .line 16
    :cond_0
    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BlockExplicitContentPerShow = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->blockExplicitContentPerShow:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", UserCanViewShow = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->userCanViewShow:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
