.class final enum Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;
.super Ljava/lang/Enum;
.source "SystemUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/system/SystemUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "MarketplaceApp"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

.field public static final enum Amazon:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

.field public static final enum GearVR:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

.field public static final enum Google:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    const-string v1, "Google"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->Google:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    .line 68
    new-instance v0, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    const-string v1, "Amazon"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->Amazon:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    .line 69
    new-instance v0, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    const-string v1, "GearVR"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->GearVR:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    .line 66
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    sget-object v1, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->Google:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->Amazon:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->GearVR:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->$VALUES:[Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-class v0, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->$VALUES:[Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    return-object v0
.end method
