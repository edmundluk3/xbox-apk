.class public Lcom/microsoft/xbox/xle/ui/ConnectButton;
.super Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;
.source "ConnectButton.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectionIcon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/ui/ConnectButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/ui/ConnectButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 40
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f03026c

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 42
    const v1, 0x7f0e0428

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConnectButton;->connectionIcon:Landroid/widget/ImageView;

    .line 43
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConnectButton;->connectionIcon:Landroid/widget/ImageView;

    const v2, 0x7f0200f9

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/ConnectButton;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConnectButton;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->handleConnectDialogRequest()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/ConnectButton;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConnectButton;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConnectButton;->connectionIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method private handleConnectDialogRequest()V
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showConnectDialog()V

    .line 111
    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 85
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->setListener(Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$Listener;)V

    .line 87
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 49
    new-instance v0, Lcom/microsoft/xbox/xle/ui/ConnectButton$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/ConnectButton$1;-><init>(Lcom/microsoft/xbox/xle/ui/ConnectButton;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    new-instance v0, Lcom/microsoft/xbox/xle/ui/ConnectButton$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/ConnectButton$2;-><init>(Lcom/microsoft/xbox/xle/ui/ConnectButton;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->setListener(Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$Listener;)V

    .line 81
    return-void
.end method

.method public refresh()V
    .locals 5

    .prologue
    .line 101
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v1

    .line 102
    .local v1, "sessionState":I
    const-string v2, "ConnectButton"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handle session state to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->fromSessionState(I)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    move-result-object v0

    .line 105
    .local v0, "consoleState":Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->setConsoleState(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;)V

    .line 106
    return-void
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConnectButton$3;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 98
    :goto_0
    return-void

    .line 93
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->refresh()V

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
