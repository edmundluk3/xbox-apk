.class public Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "CompanionDetailHeaderScreenAdapter.java"


# instance fields
.field private final imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final titleSmallTextView:Landroid/widget/TextView;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;

    .line 21
    const v0, 0x7f0e03f2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->screenBody:Landroid/view/View;

    .line 22
    const v0, 0x7f0e03f3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 23
    const v0, 0x7f0e03f4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 24
    const v0, 0x7f0e03f5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->titleSmallTextView:Landroid/widget/TextView;

    .line 25
    return-void
.end method


# virtual methods
.method public updateViewOverride()V
    .locals 3

    .prologue
    const v2, 0x7f020056

    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->titleSmallTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 32
    return-void
.end method
