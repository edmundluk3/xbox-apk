.class Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubSearchScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ClubSearchSuggestAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final callbackRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final clubSearchService:Lcom/microsoft/xbox/service/clubs/IClubSearchService;

.field private final searchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 451
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1, "searchParams"    # Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 460
    .local p2, "callback":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Landroid/support/v4/util/Pair<Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;>;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 461
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 462
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 464
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubSearchService()Lcom/microsoft/xbox/service/clubs/IClubSearchService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->clubSearchService:Lcom/microsoft/xbox/service/clubs/IClubSearchService;

    .line 465
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->searchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    .line 466
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->callbackRef:Ljava/lang/ref/WeakReference;

    .line 467
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 471
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;
    .locals 4

    .prologue
    .line 486
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->clubSearchService:Lcom/microsoft/xbox/service/clubs/IClubSearchService;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->searchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/clubs/IClubSearchService;->getSuggestions(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 491
    :goto_0
    return-object v1

    .line 487
    :catch_0
    move-exception v0

    .line 488
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to search for:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->searchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 449
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;
    .locals 1

    .prologue
    .line 480
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 449
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->onError()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 476
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;)V
    .locals 3
    .param p1, "clubSearchResultSet"    # Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;

    .prologue
    .line 500
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->callbackRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 502
    .local v0, "callback":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Landroid/support/v4/util/Pair<Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;>;>;"
    if-eqz v0, :cond_0

    .line 503
    new-instance v1, Landroid/support/v4/util/Pair;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->searchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    invoke-direct {v1, v2, p1}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 505
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 449
    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->onPostExecute(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 496
    return-void
.end method
