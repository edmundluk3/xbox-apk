.class public final Lcom/microsoft/xbox/service/model/sls/ReportUserData;
.super Ljava/lang/Object;
.source "ReportUserData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;,
        Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;
    }
.end annotation


# static fields
.field public static final CLUB_CHAT_CONTENT_TYPE:Ljava/lang/String; = "Chat"

.field public static final CLUB_COMMENT_CONTENT_TYPE:Ljava/lang/String; = "Comment"

.field public static final CLUB_FEED_CONTENT_TYPE:Ljava/lang/String; = "ActivityFeedItem"


# instance fields
.field public final evidenceId:Ljava/lang/Object;

.field public final feedbackContext:Ljava/lang/String;

.field public feedbackType:Ljava/lang/String;

.field public final sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

.field public textReason:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;)V
    .locals 0
    .param p1, "feedbackContext"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "feedbackType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "evidenceId"    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "textReason"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "sessionRef"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 48
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->feedbackContext:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->feedbackType:Ljava/lang/String;

    .line 52
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->evidenceId:Ljava/lang/Object;

    .line 53
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->textReason:Ljava/lang/String;

    .line 54
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 55
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
