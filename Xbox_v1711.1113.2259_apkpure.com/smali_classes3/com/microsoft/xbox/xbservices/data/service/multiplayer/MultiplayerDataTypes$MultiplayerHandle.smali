.class public abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;
.super Ljava/lang/Object;
.source "MultiplayerDataTypes.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MultiplayerHandle"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;",
        ">;"
    }
.end annotation


# static fields
.field private static final transient MPSD_SESSION_VERSION:I = 0x1

.field private static final transient MS_IN_A_DAY:I = 0x5265c00


# instance fields
.field public final version:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 499
    const/4 v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->version:I

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;
    .locals 1

    .prologue
    .line 570
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;-><init>()V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 599
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerHandle$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerHandle$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract activityInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public compareTo(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;)I
    .locals 6
    .param p1, "otherHandle"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 551
    if-nez p1, :cond_1

    move v3, v4

    .line 564
    :cond_0
    :goto_0
    return v3

    .line 554
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;->scheduledTime()Ljava/util/Date;

    move-result-object v5

    if-eqz v5, :cond_2

    move v0, v2

    .line 555
    .local v0, "hasScheduledTime":Z
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;->scheduledTime()Ljava/util/Date;

    move-result-object v5

    if-eqz v5, :cond_3

    move v1, v2

    .line 557
    .local v1, "otherHasScheduledTime":Z
    :goto_2
    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    .line 558
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;->scheduledTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;->scheduledTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v3

    goto :goto_0

    .end local v0    # "hasScheduledTime":Z
    .end local v1    # "otherHasScheduledTime":Z
    :cond_2
    move v0, v3

    .line 554
    goto :goto_1

    .restart local v0    # "hasScheduledTime":Z
    :cond_3
    move v1, v3

    .line 555
    goto :goto_2

    .line 559
    .restart local v1    # "otherHasScheduledTime":Z
    :cond_4
    if-nez v0, :cond_5

    if-eqz v1, :cond_0

    .line 561
    :cond_5
    if-nez v0, :cond_6

    move v3, v4

    .line 562
    goto :goto_0

    :cond_6
    move v3, v2

    .line 564
    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 488
    check-cast p1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->compareTo(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;)I

    move-result v0

    return v0
.end method

.method public getConfirmedCount()I
    .locals 1

    .prologue
    .line 537
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->getConfirmedCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHostXuid()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 521
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 522
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;->sessionOwners()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 524
    .local v0, "sessionOwners":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 525
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 529
    .end local v0    # "sessionOwners":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRemainingNeedCount()I
    .locals 1

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->getNeedCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract id()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract invitedXuid()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public isXuidConfirmed(Ljava/lang/String;)Z
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 541
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 543
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 544
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 545
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 546
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;->memberXuids()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 541
    :goto_0
    return v0

    .line 546
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract searchAttributes()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract sessionRef()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract type()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
