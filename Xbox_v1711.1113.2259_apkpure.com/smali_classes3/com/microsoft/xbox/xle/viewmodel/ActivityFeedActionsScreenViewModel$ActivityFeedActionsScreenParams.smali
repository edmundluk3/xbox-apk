.class public Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;
.super Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
.source "ActivityFeedActionsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActivityFeedActionsScreenParams"
.end annotation


# instance fields
.field private final fromScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

.field private final isCapture:Z

.field private final item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field private final itemActionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

.field private final itemLocator:Ljava/lang/String;

.field private final shouldAutoFocus:Z


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZ)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "itemLocator"    # Ljava/lang/String;
    .param p3, "itemActionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .param p4, "fromScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p5, "shouldAutoFocus"    # Z
    .param p6, "isCapture"    # Z

    .prologue
    .line 1251
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 1252
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 1253
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->itemLocator:Ljava/lang/String;

    .line 1254
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->itemActionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 1255
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->fromScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 1256
    iput-boolean p5, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->shouldAutoFocus:Z

    .line 1257
    iput-boolean p6, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->isCapture:Z

    .line 1258
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->itemLocator:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->itemActionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    .prologue
    .line 1237
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->shouldAutoFocus:Z

    return v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    .prologue
    .line 1237
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->isCapture:Z

    return v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->fromScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    return-object v0
.end method

.method public static with(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZ)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;
    .locals 7
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "itemActionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "fromScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "shouldAutoFocus"    # Z
    .param p4, "isCapture"    # Z

    .prologue
    .line 1266
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1267
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1269
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;-><init>(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZ)V

    return-object v0
.end method

.method public static with(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZ)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;
    .locals 7
    .param p0, "itemLocator"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "itemActionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "fromScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "shouldAutoFocus"    # Z
    .param p4, "isCapture"    # Z

    .prologue
    .line 1284
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1285
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1287
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    const/4 v1, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;-><init>(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZ)V

    return-object v0
.end method
