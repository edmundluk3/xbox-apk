.class public interface abstract Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
.super Ljava/lang/Object;
.source "IXBLog.java"


# virtual methods
.method public abstract Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract Error(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract Info(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract Info(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract Warning(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.end method
