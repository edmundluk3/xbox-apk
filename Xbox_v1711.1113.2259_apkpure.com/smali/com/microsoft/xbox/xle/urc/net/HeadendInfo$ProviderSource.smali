.class public final enum Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
.super Ljava/lang/Enum;
.source "HeadendInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ProviderSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

.field public static final enum UNDEFINED:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

.field public static final enum hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

.field public static final enum streaming:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

.field public static final enum tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    const-string v1, "hdmi"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    const-string/jumbo v1, "streaming"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->streaming:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    const-string/jumbo v1, "tuner"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->UNDEFINED:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->streaming:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->UNDEFINED:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->$VALUES:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 19
    if-nez p0, :cond_0

    .line 20
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->UNDEFINED:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    .line 30
    :goto_0
    return-object v0

    .line 21
    :cond_0
    const-string v0, "hdmi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    goto :goto_0

    .line 23
    :cond_1
    const-string/jumbo v0, "streaming"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->streaming:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    goto :goto_0

    .line 25
    :cond_2
    const-string/jumbo v0, "tuner"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 26
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    goto :goto_0

    .line 29
    :cond_3
    const-string v0, "HeadendInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provider source string received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->UNDEFINED:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->$VALUES:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    return-object v0
.end method
