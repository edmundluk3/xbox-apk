.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubDataTypes_ClubNotificationDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;",
        ">;"
    }
.end annotation


# instance fields
.field private final actorGamertagAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final actorIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final clubIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final clubNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultActorGamertag:Ljava/lang/String;

.field private defaultActorId:Ljava/lang/String;

.field private defaultClubId:Ljava/lang/String;

.field private defaultClubName:Ljava/lang/String;

.field private defaultNotificationType:Ljava/lang/String;

.field private defaultTargetGamertag:Ljava/lang/String;

.field private defaultTargetId:Ljava/lang/String;

.field private defaultVersion:I

.field private final notificationTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final targetGamertagAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final targetIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final versionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 2
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultVersion:I

    .line 30
    iput-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultNotificationType:Ljava/lang/String;

    .line 31
    iput-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultClubId:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultClubName:Ljava/lang/String;

    .line 33
    iput-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultActorId:Ljava/lang/String;

    .line 34
    iput-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultActorGamertag:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultTargetId:Ljava/lang/String;

    .line 36
    iput-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultTargetGamertag:Ljava/lang/String;

    .line 38
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->versionAdapter:Lcom/google/gson/TypeAdapter;

    .line 39
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->notificationTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 40
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->clubIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 41
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->clubNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 42
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->actorIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 43
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->actorGamertagAdapter:Lcom/google/gson/TypeAdapter;

    .line 44
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->targetIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 45
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->targetGamertagAdapter:Lcom/google/gson/TypeAdapter;

    .line 46
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;
    .locals 11
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v10, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v10, :cond_0

    .line 108
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 109
    const/4 v0, 0x0

    .line 165
    :goto_0
    return-object v0

    .line 111
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 112
    iget v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultVersion:I

    .line 113
    .local v1, "version":I
    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultNotificationType:Ljava/lang/String;

    .line 114
    .local v2, "notificationType":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultClubId:Ljava/lang/String;

    .line 115
    .local v3, "clubId":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultClubName:Ljava/lang/String;

    .line 116
    .local v4, "clubName":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultActorId:Ljava/lang/String;

    .line 117
    .local v5, "actorId":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultActorGamertag:Ljava/lang/String;

    .line 118
    .local v6, "actorGamertag":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultTargetId:Ljava/lang/String;

    .line 119
    .local v7, "targetId":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultTargetGamertag:Ljava/lang/String;

    .line 120
    .local v8, "targetGamertag":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v9

    .line 122
    .local v9, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v10, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v10, :cond_1

    .line 123
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 126
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 160
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 126
    :sswitch_0
    const-string v10, "version"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v10, "notificationType"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v10, "clubId"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v10, "clubName"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v10, "actorId"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v10, "actorGamertag"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v10, "targetId"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x6

    goto :goto_2

    :sswitch_7
    const-string v10, "targetGamertag"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x7

    goto :goto_2

    .line 128
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->versionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 129
    goto/16 :goto_1

    .line 132
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->notificationTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "notificationType":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 133
    .restart local v2    # "notificationType":Ljava/lang/String;
    goto/16 :goto_1

    .line 136
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->clubIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "clubId":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 137
    .restart local v3    # "clubId":Ljava/lang/String;
    goto/16 :goto_1

    .line 140
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->clubNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "clubName":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 141
    .restart local v4    # "clubName":Ljava/lang/String;
    goto/16 :goto_1

    .line 144
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->actorIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "actorId":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 145
    .restart local v5    # "actorId":Ljava/lang/String;
    goto/16 :goto_1

    .line 148
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->actorGamertagAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "actorGamertag":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 149
    .restart local v6    # "actorGamertag":Ljava/lang/String;
    goto/16 :goto_1

    .line 152
    :pswitch_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->targetIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "targetId":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 153
    .restart local v7    # "targetId":Ljava/lang/String;
    goto/16 :goto_1

    .line 156
    :pswitch_7
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->targetGamertagAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "targetGamertag":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 157
    .restart local v8    # "targetGamertag":Ljava/lang/String;
    goto/16 :goto_1

    .line 164
    .end local v9    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 165
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails;

    invoke-direct/range {v0 .. v8}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 126
    nop

    :sswitch_data_0
    .sparse-switch
        -0x50e7a78f -> :sswitch_2
        -0x453cf610 -> :sswitch_4
        -0x1a57a574 -> :sswitch_6
        0x14f51cd8 -> :sswitch_0
        0x37b5498b -> :sswitch_7
        0x4a664861 -> :sswitch_3
        0x5dbfddef -> :sswitch_5
        0x600e1ec5 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultActorGamertag(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultActorGamertag"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultActorGamertag:Ljava/lang/String;

    .line 69
    return-object p0
.end method

.method public setDefaultActorId(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultActorId"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultActorId:Ljava/lang/String;

    .line 65
    return-object p0
.end method

.method public setDefaultClubId(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultClubId"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultClubId:Ljava/lang/String;

    .line 57
    return-object p0
.end method

.method public setDefaultClubName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultClubName"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultClubName:Ljava/lang/String;

    .line 61
    return-object p0
.end method

.method public setDefaultNotificationType(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultNotificationType"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultNotificationType:Ljava/lang/String;

    .line 53
    return-object p0
.end method

.method public setDefaultTargetGamertag(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTargetGamertag"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultTargetGamertag:Ljava/lang/String;

    .line 77
    return-object p0
.end method

.method public setDefaultTargetId(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTargetId"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultTargetId:Ljava/lang/String;

    .line 73
    return-object p0
.end method

.method public setDefaultVersion(I)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultVersion"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->defaultVersion:I

    .line 49
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    if-nez p2, :cond_0

    .line 83
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 104
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 87
    const-string v0, "version"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->versionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->version()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 89
    const-string v0, "notificationType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->notificationTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->notificationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 91
    const-string v0, "clubId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->clubIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->clubId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 93
    const-string v0, "clubName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->clubNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->clubName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 95
    const-string v0, "actorId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->actorIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->actorId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 97
    const-string v0, "actorGamertag"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->actorGamertagAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->actorGamertag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 99
    const-string v0, "targetId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->targetIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->targetId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 101
    const-string v0, "targetGamertag"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->targetGamertagAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->targetGamertag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 103
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubDataTypes_ClubNotificationDetails$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;)V

    return-void
.end method
