.class final Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;
.super Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
.source "AutoValue_ActivityHubDataTypes_TrendingResponseList.java"


# instance fields
.field private final description:Ljava/lang/String;

.field private final items:Lcom/google/gson/JsonArray;

.field private final queryType:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

.field private final title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/gson/JsonArray;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "items"    # Lcom/google/gson/JsonArray;
    .param p2, "queryType"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;-><init>()V

    .line 21
    if-nez p1, :cond_0

    .line 22
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null items"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->items:Lcom/google/gson/JsonArray;

    .line 25
    if-nez p2, :cond_1

    .line 26
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null queryType"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->queryType:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    .line 29
    if-nez p3, :cond_2

    .line 30
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null title"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->title:Ljava/lang/String;

    .line 33
    if-nez p4, :cond_3

    .line 34
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null description"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->description:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->description:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    if-ne p1, p0, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v1

    .line 78
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 79
    check-cast v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    .line 80
    .local v0, "that":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->items:Lcom/google/gson/JsonArray;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->items()Lcom/google/gson/JsonArray;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/gson/JsonArray;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->queryType:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    .line 81
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->queryType()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->title:Ljava/lang/String;

    .line 82
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->title()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->description:Ljava/lang/String;

    .line 83
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->description()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    :cond_3
    move v1, v2

    .line 85
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 90
    const/4 v0, 0x1

    .line 91
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->items:Lcom/google/gson/JsonArray;

    invoke-virtual {v1}, Lcom/google/gson/JsonArray;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 93
    mul-int/2addr v0, v2

    .line 94
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->queryType:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 95
    mul-int/2addr v0, v2

    .line 96
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 97
    mul-int/2addr v0, v2

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 99
    return v0
.end method

.method public items()Lcom/google/gson/JsonArray;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->items:Lcom/google/gson/JsonArray;

    return-object v0
.end method

.method public queryType()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->queryType:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    return-object v0
.end method

.method public title()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->title:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TrendingResponseList{items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->items:Lcom/google/gson/JsonArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", queryType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->queryType:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
