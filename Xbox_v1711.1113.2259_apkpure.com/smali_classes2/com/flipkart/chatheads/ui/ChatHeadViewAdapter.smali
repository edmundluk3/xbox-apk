.class public interface abstract Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;
.super Ljava/lang/Object;
.source "ChatHeadViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract attachView(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<+",
            "Ljava/io/Serializable;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract detachView(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<+",
            "Ljava/io/Serializable;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation
.end method

.method public abstract getChatHeadDrawable(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation
.end method

.method public abstract removeView(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<+",
            "Ljava/io/Serializable;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation
.end method
