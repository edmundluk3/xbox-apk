.class public Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
.super Ljava/lang/Object;
.source "PartyChatRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$Names;
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final CLIENT_CAPABILITY_KEY:Ljava/lang/String; = "clientCapability"

.field private static final GSI_SET_ID:Ljava/lang/String;

.field private static final MPSD_SUBSCRIBE_MESSAGE:Ljava/lang/String; = "https://sessiondirectory.xboxlive.com/connections/"

.field private static final PARTY_CLIENT_CAPABILITY:I = 0x4

.field private static final PARTY_CLIENT_TYPE:I = 0x2

.field private static final PARTY_PROTOCOL_VERSION:I = 0x20000

.field public static final PARTY_SCID:Ljava/lang/String; = "7492BACA-C1B4-440D-A391-B7EF364A8D40"

.field public static final PARTY_TEMPLATE:Ljava/lang/String; = "chat"

.field private static final PARTY_TITLE_ID:Ljava/lang/String; = "1554276081"

.field private static final RTA_SUBSCRIBE_MAX_RETRIES:I = 0x4

.field private static final TAG:Ljava/lang/String;

.field private static final VOICE_SESSION_ID_KEY:Ljava/lang/String; = "voicesessionid"

.field private static final XRNXBL_KEY:Ljava/lang/String; = "xrnxbl"


# instance fields
.field private cachedRoster:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation
.end field

.field private connectionId:Ljava/lang/String;

.field private currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

.field private currentPartyId:Ljava/lang/String;

.field private deviceId:Ljava/lang/String;

.field private final gsonBuilder:Lcom/google/gson/Gson;

.field private hasProcessedServerWebrtcInfo:Z

.field private isAudioEnabled:Z

.field private isRemoteAudioEnabled:Z

.field private logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

.field private multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

.field private partyEvents:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private partyMemberChanges:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation
.end field

.field private partyMessages:Lio/reactivex/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/ReplaySubject",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;"
        }
    .end annotation
.end field

.field private partySubject:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartySession;",
            ">;"
        }
    .end annotation
.end field

.field private privacyService:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;

.field private qosRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;

.field private rtaConnection:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation
.end field

.field private rtaRepository:Lcom/microsoft/xbox/xbservices/rta/RtaRepository;

.field private telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

.field private webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

.field private xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    const-class v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    .line 123
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->GSI_SET_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;)V
    .locals 4
    .param p1, "rtaRepository"    # Lcom/microsoft/xbox/xbservices/rta/RtaRepository;
    .param p2, "multiplayerService"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;
    .param p3, "privacyService"    # Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;
    .param p4, "qosRepository"    # Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;
    .param p5, "xuidProvider"    # Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;
    .param p6, "logger"    # Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
    .param p7, "webRtcManager"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;
    .param p8, "telemetryProvider"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isAudioEnabled:Z

    .line 149
    iput-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled:Z

    .line 151
    invoke-static {}, Lio/reactivex/subjects/BehaviorSubject;->create()Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partySubject:Lio/reactivex/subjects/BehaviorSubject;

    .line 152
    invoke-static {}, Lio/reactivex/subjects/ReplaySubject;->create()Lio/reactivex/subjects/ReplaySubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMessages:Lio/reactivex/subjects/ReplaySubject;

    .line 153
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMemberChanges:Lio/reactivex/subjects/PublishSubject;

    .line 154
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyEvents:Lio/reactivex/subjects/PublishSubject;

    .line 204
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->rtaRepository:Lcom/microsoft/xbox/xbservices/rta/RtaRepository;

    .line 205
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    .line 206
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->privacyService:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;

    .line 207
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->qosRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;

    .line 208
    iput-object p5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 209
    iput-object p6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    .line 211
    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->gsonBuilder:Lcom/google/gson/Gson;

    .line 212
    iput-object p7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    .line 213
    iput-object p8, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->rtaRepository:Lcom/microsoft/xbox/xbservices/rta/RtaRepository;

    const-string v1, "https://sessiondirectory.xboxlive.com/connections/"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->subscribeToRtaEvents(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 218
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v2, 0x4

    .line 241
    invoke-virtual {v0, v2, v3}, Lio/reactivex/Observable;->retry(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Lio/reactivex/Observable;->publish()Lio/reactivex/observables/ConnectableObservable;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->autoConnect()Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->rtaConnection:Lio/reactivex/Observable;

    .line 244
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)V
    .locals 0

    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cleanupParty()V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->processPrivacySettings(Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionResponse;)V

    return-void
.end method

.method private allocateCloudCompute()Lio/reactivex/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 857
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v1, :cond_2

    .line 858
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 859
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->cloudCompute()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServerCloudCompute;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;->system()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;->allocateCloudCompute()Ljava/lang/Boolean;

    move-result-object v1

    if-nez v1, :cond_1

    .line 860
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 861
    invoke-interface {v2}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v2

    .line 862
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 863
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v4

    .line 864
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v5

    .line 865
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getCurrentUserIndex()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->index(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v5

    .line 866
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v5

    .line 864
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v4

    .line 867
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;

    move-result-object v4

    .line 863
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->constants(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 868
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v3

    .line 861
    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->withSelf(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    .line 870
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;

    move-result-object v2

    .line 871
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 872
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;->allocateCloudCompute(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v3

    .line 873
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;

    move-result-object v3

    .line 871
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;

    move-result-object v2

    .line 874
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;

    move-result-object v2

    .line 869
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->properties(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    .line 875
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v0

    .line 877
    .local v0, "sessionRequest":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v1

    .line 884
    .end local v0    # "sessionRequest":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    :goto_0
    return-object v1

    .line 879
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v3, "Skipping allocating a relay. Party already has a relay or one has been requested."

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0

    .line 883
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSessionAllocateCloudCompute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 884
    new-instance v1, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    invoke-direct {v1}, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;-><init>()V

    invoke-static {v1}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0
.end method

.method private declared-synchronized cleanupParty()V
    .locals 1

    .prologue
    .line 1276
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v0, :cond_0

    .line 1277
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->close()Lio/reactivex/Observable;

    .line 1278
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 1279
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 1280
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->deviceId:Ljava/lang/String;

    .line 1281
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->hasProcessedServerWebrtcInfo:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1283
    :cond_0
    monitor-exit p0

    return-void

    .line 1276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private createPartyInternal()Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 676
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->initializeParty(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v1

    .line 678
    .local v1, "multiplayerMember":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v5, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v6, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatStarted:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v5, v6}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    const-string v6, "PartyId"

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 680
    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v5

    .line 678
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 682
    new-instance v0, Lcom/google/gson/JsonObject;

    invoke-direct {v0}, Lcom/google/gson/JsonObject;-><init>()V

    .line 683
    .local v0, "multiplayerConstants":Lcom/google/gson/JsonObject;
    const-string/jumbo v4, "xrnxbl"

    new-instance v5, Lcom/google/gson/JsonPrimitive;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/Boolean;)V

    invoke-virtual {v0, v4, v5}, Lcom/google/gson/JsonObject;->add(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 685
    new-instance v2, Lcom/google/gson/JsonObject;

    invoke-direct {v2}, Lcom/google/gson/JsonObject;-><init>()V

    .line 686
    .local v2, "multiplayerProperties":Lcom/google/gson/JsonObject;
    const-string/jumbo v4, "voicesessionid"

    new-instance v5, Lcom/google/gson/JsonPrimitive;

    const-string v6, "!"

    invoke-direct {v5, v6}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4, v5}, Lcom/google/gson/JsonObject;->add(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 689
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 690
    invoke-interface {v5}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->withSelf(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v4

    .line 691
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;

    move-result-object v5

    .line 692
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;->Followed:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;

    .line 693
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;->joinRestriction(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;->Followed:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;

    .line 694
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;->readRestriction(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v6

    .line 695
    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;

    move-result-object v6

    .line 692
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;

    move-result-object v5

    .line 696
    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;->custom(Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;

    move-result-object v5

    .line 697
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;

    move-result-object v5

    .line 691
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->properties(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v4

    const/4 v5, 0x0

    .line 698
    invoke-static {v5, v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionConstants;->with(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionConstantsSystem;Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionConstants;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->constants(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionConstants;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v4

    .line 702
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v3

    .line 704
    .local v3, "sessionRequest":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    const-string v5, "7492BACA-C1B4-440D-A391-B7EF364A8D40"

    const-string v6, "chat"

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-interface {v4, v5, v6, v7, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->createMultiplayerSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v4

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v5

    .line 705
    invoke-virtual {v4, v5}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v4

    .line 704
    return-object v4
.end method

.method private ensureSinglePointOfPresence(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 4
    .param p1, "targetPartyId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    const-string v1, "7492BACA-C1B4-440D-A391-B7EF364A8D40"

    const-string v2, "chat"

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v3}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;->with(Ljava/util/List;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->getMultiplayerSessionsForCurrentUser(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 251
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/String;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 252
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->flatMapObservable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 250
    return-object v0
.end method

.method private getCurrentUserIndex()Ljava/lang/Integer;
    .locals 5

    .prologue
    .line 843
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 844
    .local v0, "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v3}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 845
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 853
    .end local v0    # "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :goto_0
    return-object v1

    .line 849
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatCurrentUserNotFound:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    const-string v3, "PartyId"

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 850
    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v2

    const-string v3, "MemberXuid"

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 851
    invoke-interface {v4}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v2

    .line 849
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 852
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v3, "Could not find current user in party!"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getWebRtcEvents()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 373
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->getWebRtcEvents()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 374
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 373
    return-object v0
.end method

.method private handleRtaShoulderTap(Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;)Lio/reactivex/Single;
    .locals 6
    .param p1, "shoulderTap"    # Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 889
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;->shoulderTaps()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;

    .line 890
    .local v0, "firstTap":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;->resource()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "~"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 892
    .local v1, "resourceElements":[Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v2, :cond_1

    .line 893
    array-length v2, v1

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 894
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;->changeNumber()I

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;->with(I)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->sendDataChannelMessage(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$PartyDataChannelMessage;)Z

    .line 895
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    aget-object v3, v1, v4

    const/4 v4, 0x1

    aget-object v4, v1, v4

    const/4 v5, 0x2

    aget-object v5, v1, v5

    invoke-interface {v2, v3, v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->getMultiplayerSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$16;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 896
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->doOnError(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$17;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 900
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v2

    .line 985
    :goto_0
    return-object v2

    .line 981
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-static {v2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v2

    goto :goto_0

    .line 984
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToHandleRTATapNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 985
    new-instance v2, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    invoke-direct {v2}, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;-><init>()V

    invoke-static {v2}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v2

    goto :goto_0
.end method

.method private initializeParty(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    .locals 6
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 424
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 425
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->deviceId:Ljava/lang/String;

    .line 426
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    .line 428
    new-instance v0, Lcom/google/gson/JsonObject;

    invoke-direct {v0}, Lcom/google/gson/JsonObject;-><init>()V

    .line 429
    .local v0, "mulitplayerUserConstants":Lcom/google/gson/JsonObject;
    const-string v1, "clientCapability"

    new-instance v2, Lcom/google/gson/JsonPrimitive;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/Number;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/JsonObject;->add(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 431
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v1

    .line 433
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v2

    .line 434
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->custom(Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v2

    .line 435
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 436
    invoke-interface {v4}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->xuid(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v3

    .line 437
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->initialize(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v3

    .line 438
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v3

    .line 435
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v2

    .line 439
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;

    move-result-object v2

    .line 432
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->constants(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v1

    .line 440
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v2

    .line 441
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->deviceId:Ljava/lang/String;

    .line 442
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->deviceId(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v3

    .line 443
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->simpleConnectionState(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v3

    const/4 v4, 0x0

    .line 444
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->isBroadcasting(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v3

    const/4 v4, 0x2

    .line 445
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->clientType(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v3

    const/high16 v4, 0x20000

    .line 446
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->protocolVersion(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v3

    .line 447
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;

    move-result-object v3

    .line 441
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->custom(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v2

    .line 448
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->connectionId:Ljava/lang/String;

    .line 449
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;->addConnection(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;

    move-result-object v3

    .line 450
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;

    move-result-object v3

    .line 448
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v2

    .line 451
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;

    move-result-object v2

    .line 440
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->properties(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v1

    .line 452
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v1

    .line 431
    return-object v1
.end method

.method private joinPartyInternal(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 5
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "handleId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 659
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->initializeParty(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v0

    .line 661
    .local v0, "multiplayerMember":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatJoined:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    const-string v4, "PartyId"

    .line 662
    invoke-virtual {v3, v4, p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v3

    .line 661
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 664
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 665
    invoke-interface {v3}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->withSelf(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v2

    .line 666
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v1

    .line 668
    .local v1, "sessionRequest":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 669
    invoke-direct {p0, p2, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateMultiplayerHandle(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v2

    .line 671
    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v2

    goto :goto_0
.end method

.method static synthetic lambda$createParty$8(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/ObservableSource;
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "party"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 295
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->create()V

    .line 298
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getWebRtcEvents()Lio/reactivex/Observable;

    move-result-object v0

    .line 300
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateConnectionIdForSession()Lio/reactivex/Single;

    move-result-object v1

    .line 301
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateQoSMeasurements()Lio/reactivex/Single;

    move-result-object v2

    const-wide/16 v4, 0x1388

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 302
    invoke-virtual {v2, v4, v5, v3}, Lio/reactivex/Single;->delay(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$26;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 303
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v2

    .line 299
    invoke-static {v1, v2}, Lio/reactivex/Single;->concat(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;)Lio/reactivex/Flowable;

    move-result-object v1

    .line 318
    invoke-virtual {v1}, Lio/reactivex/Flowable;->toObservable()Lio/reactivex/Observable;

    move-result-object v1

    .line 297
    invoke-static {v0, v1}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 319
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$27;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 320
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorResumeNext(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 297
    return-object v0
.end method

.method static synthetic lambda$createPartyInternal$19(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "updatedSession"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 706
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v3, "Updating new party instance"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 708
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateLocalRoster()V

    .line 710
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 711
    .local v0, "currentRoster":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partySubject:Lio/reactivex/subjects/BehaviorSubject;

    invoke-static {}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->builder()Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v1

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 712
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setMultiplayerSession(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v1

    .line 713
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setRoster(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v3

    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    .line 714
    :goto_0
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setMuted(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v1

    .line 715
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->build()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v1

    .line 711
    invoke-virtual {v2, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 716
    return-void

    .line 713
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$ensureSinglePointOfPresence$1(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;)Z
    .locals 1
    .param p0, "activeSessions"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 251
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;->results()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;->results()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$ensureSinglePointOfPresence$3(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;)Lio/reactivex/ObservableSource;
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "targetPartyId"    # Ljava/lang/String;
    .param p2, "activeSessions"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 253
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 255
    .local v1, "leaveMultiplayerRequests":Ljava/util/List;, "Ljava/util/List<Lio/reactivex/Observable<Lretrofit2/Response<Ljava/lang/Void;>;>;>;"
    invoke-virtual {p2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;->results()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;

    .line 256
    .local v0, "activeSession":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;->sessionRef()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v3, "active"

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;->status()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 257
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;->sessionRef()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 260
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    const-string v4, "7492BACA-C1B4-440D-A391-B7EF364A8D40"

    const-string v5, "chat"

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;->sessionRef()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->leaveMultiplayerSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v3

    const-wide/16 v4, 0x1388

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 261
    invoke-virtual {v3, v4, v5, v6}, Lio/reactivex/Observable;->delay(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v3

    .line 260
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 264
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    const-string v4, "7492BACA-C1B4-440D-A391-B7EF364A8D40"

    const-string v5, "chat"

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;->sessionRef()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->leaveMultiplayerSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 270
    .end local v0    # "activeSession":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cleanupParty()V

    .line 277
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 278
    invoke-static {v1}, Lio/reactivex/Observable;->merge(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$30;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 279
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 281
    :goto_1
    return-object v2

    :cond_3
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v2

    goto :goto_1
.end method

.method static synthetic lambda$getWebRtcEvents$13(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$PartyWebRtcEvent;)Lio/reactivex/ObservableSource;
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "webRtcEvent"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$PartyWebRtcEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 375
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 376
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 381
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;

    .line 382
    .local v0, "statusChange":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;->userSsrc()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;->isTalking()Z

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->onMemberStatusChange(Ljava/lang/String;Z)V

    .line 383
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v3

    .line 418
    .end local v0    # "statusChange":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;
    :goto_0
    return-object v3

    .line 384
    :cond_0
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcConnected;

    if-eqz v3, :cond_1

    .line 385
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v5, "WebRTC is connected"

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v5, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebRTCConnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    const-string v5, "PartyId"

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 387
    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v4

    .line 386
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 388
    sget-object v3, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;->Connected:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateConnectionState(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;)Lio/reactivex/Single;

    move-result-object v3

    .line 389
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v3

    .line 390
    invoke-virtual {v3}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v3

    goto :goto_0

    .line 391
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcDisconnected;

    if-eqz v3, :cond_2

    .line 392
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v5, "WebRTC is disconnected"

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v5, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatDisconnectedUnintentionally:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    const-string v5, "PartyId"

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 394
    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v4

    .line 393
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 395
    sget-object v3, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;->Disconnected:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateConnectionState(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;)Lio/reactivex/Single;

    move-result-object v3

    .line 396
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v3

    .line 397
    invoke-virtual {v3}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v3

    goto :goto_0

    .line 398
    :cond_2
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcDataChannelConnected;

    if-eqz v3, :cond_3

    .line 399
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v5, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebRTCConnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    const-string v5, "PartyId"

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 400
    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v4

    .line 399
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 401
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->onDataChannelConnected()V

    .line 402
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v3

    goto/16 :goto_0

    .line 403
    :cond_3
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcTextMessage;

    if-eqz v3, :cond_4

    move-object v1, p1

    .line 404
    check-cast v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcTextMessage;

    .line 405
    .local v1, "textMessage":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcTextMessage;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcTextMessage;->fromIndex()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcTextMessage;->textMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->onTextChatMessageReceived(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v3

    goto/16 :goto_0

    .line 407
    .end local v1    # "textMessage":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcTextMessage;
    :cond_4
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;

    if-eqz v3, :cond_5

    move-object v2, p1

    .line 408
    check-cast v2, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;

    .line 409
    .local v2, "update":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;->sdp()Lorg/webrtc/SessionDescription;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateWebrtcConfiguration(Lorg/webrtc/SessionDescription;)Lio/reactivex/Single;

    move-result-object v3

    .line 410
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v3

    .line 411
    invoke-virtual {v3}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v3

    goto/16 :goto_0

    .line 413
    .end local v2    # "update":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown webRtcEvent type! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v3

    goto/16 :goto_0

    .line 417
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v5, "Received event from webRtc when no party is active."

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v3

    goto/16 :goto_0
.end method

.method static synthetic lambda$handleRtaShoulderTap$22(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/Throwable;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 897
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateMultiplayerSessionRTATap:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 898
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v2, "Failed to update session"

    invoke-interface {v0, v1, v2, p1}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 899
    return-void
.end method

.method static synthetic lambda$handleRtaShoulderTap$24(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V
    .locals 12
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "updatedSession"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 901
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v5, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v6, "onShouldertap"

    invoke-interface {v2, v5, v6}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 903
    .local v1, "previousSession":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 904
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateLocalRoster()V

    .line 906
    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partySubject:Lio/reactivex/subjects/BehaviorSubject;

    invoke-static {}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->builder()Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v2

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 907
    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setMultiplayerSession(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v6

    iget-boolean v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled:Z

    if-nez v2, :cond_2

    move v2, v3

    .line 908
    :goto_0
    invoke-virtual {v6, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setMuted(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v2

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    .line 909
    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setRoster(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v2

    .line 910
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->build()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v2

    .line 906
    invoke-virtual {v5, v2}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 912
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;->custom()Lcom/google/gson/JsonObject;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 913
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;->custom()Lcom/google/gson/JsonObject;

    move-result-object v2

    const-string v5, "kickusers"

    invoke-virtual {v2, v5}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 914
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;->custom()Lcom/google/gson/JsonObject;

    move-result-object v2

    const-string v5, "kickusers"

    invoke-virtual {v2, v5}, Lcom/google/gson/JsonObject;->getAsJsonObject(Ljava/lang/String;)Lcom/google/gson/JsonObject;

    move-result-object v0

    .line 916
    .local v0, "kickusersObject":Lcom/google/gson/JsonObject;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v2}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 917
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v5, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v6, "Current user was kicked, leaving party"

    invoke-interface {v2, v5, v6}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    const-string v5, "7492BACA-C1B4-440D-A391-B7EF364A8D40"

    const-string v6, "chat"

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "me_%s"

    new-array v10, v3, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 923
    invoke-interface {v11}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v4

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 922
    invoke-static {v4, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RemoveMultiplayerMemberRequest;->with(Ljava/lang/String;Z)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RemoveMultiplayerMemberRequest;

    move-result-object v4

    .line 918
    invoke-interface {v2, v5, v6, v7, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->kickSelfFromSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RemoveMultiplayerMemberRequest;)Lio/reactivex/Single;

    move-result-object v2

    .line 925
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v2, v4}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    .line 926
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v2, v4}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$22;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 927
    invoke-virtual {v2, v4}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 935
    .end local v0    # "kickusersObject":Lcom/google/gson/JsonObject;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 936
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 937
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->q10()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 938
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->q10()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 939
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->q10()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;->custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerPropertiesCustom;

    move-result-object v2

    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    .line 941
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 942
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->q10()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 943
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->q10()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 944
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->q10()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;->custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerPropertiesCustom;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 945
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->q10()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;->custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerPropertiesCustom;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerPropertiesCustom;->iteration()I

    move-result v2

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->q10()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;->custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerPropertiesCustom;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerPropertiesCustom;->iteration()I

    move-result v4

    if-le v2, v4, :cond_3

    .line 947
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Existing q10 relay has changed, updating connection. Previous iteration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 948
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->q10()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;->custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerPropertiesCustom;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerPropertiesCustom;->iteration()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " current iteration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->q10()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;->custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerPropertiesCustom;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerPropertiesCustom;->iteration()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 947
    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v2, v4

    .line 907
    goto/16 :goto_0

    .line 968
    :cond_3
    iget-boolean v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->hasProcessedServerWebrtcInfo:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 969
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 970
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->cloudCompute()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServerCloudCompute;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 971
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->cloudCompute()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServerCloudCompute;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServerCloudCompute;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 972
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->cloudCompute()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServerCloudCompute;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServerCloudCompute;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;->custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 973
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->cloudCompute()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServerCloudCompute;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServerCloudCompute;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;->custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;->webRtc()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 974
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v5, "Got server webrtc info"

    invoke-interface {v2, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    iput-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->hasProcessedServerWebrtcInfo:Z

    .line 976
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->servers()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;->cloudCompute()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServerCloudCompute;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServerCloudCompute;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;->custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;->webRtc()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->setRemoteDescription(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;)V

    goto/16 :goto_1
.end method

.method static synthetic lambda$joinParty$12(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/ObservableSource;
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "party"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 341
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->create()V

    .line 344
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getWebRtcEvents()Lio/reactivex/Observable;

    move-result-object v0

    .line 346
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updatePrivacySettings()Lio/reactivex/Single;

    move-result-object v1

    .line 347
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateConnectionIdForSession()Lio/reactivex/Single;

    move-result-object v2

    .line 348
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateQoSMeasurements()Lio/reactivex/Single;

    move-result-object v3

    const-wide/16 v4, 0x1388

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 349
    invoke-virtual {v3, v4, v5, v6}, Lio/reactivex/Single;->delay(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Single;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$23;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 350
    invoke-virtual {v3, v4}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v3

    .line 345
    invoke-static {v1, v2, v3}, Lio/reactivex/Single;->concat(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;)Lio/reactivex/Flowable;

    move-result-object v1

    .line 366
    invoke-virtual {v1}, Lio/reactivex/Flowable;->toObservable()Lio/reactivex/Observable;

    move-result-object v1

    .line 343
    invoke-static {v0, v1}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 367
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 343
    return-object v0
.end method

.method static synthetic lambda$leaveParty$14(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/Throwable;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 475
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToLeave:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    .line 476
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->setException(Ljava/lang/Throwable;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    move-result-object v1

    const-string v2, "PartyId"

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 477
    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v1

    .line 475
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 478
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v2, "Encountered an error leaving party. Cleaning up local party state."

    invoke-interface {v0, v1, v2, p1}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 479
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cleanupParty()V

    .line 480
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;)Lio/reactivex/ObservableSource;
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "rtaResponse"    # Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 219
    instance-of v2, p1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;

    if-eqz v2, :cond_1

    .line 220
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v4, "Received subscription response from RTA"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->gsonBuilder:Lcom/google/gson/Gson;

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->gsonBuilder:Lcom/google/gson/Gson;

    invoke-interface {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;->payload()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;->eventPayload()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/gson/Gson;->toJsonTree(Ljava/lang/Object;)Lcom/google/gson/JsonElement;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdSubscription;

    invoke-virtual {v2, v3, v4}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdSubscription;

    .line 222
    .local v1, "subscription":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdSubscription;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdSubscription;->connectionId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->connectionId:Ljava/lang/String;

    .line 224
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v2, :cond_0

    .line 225
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateConnectionIdForSession()Lio/reactivex/Single;

    move-result-object v2

    invoke-virtual {v2}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v2

    .line 238
    .end local v1    # "subscription":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdSubscription;
    :goto_0
    return-object v2

    .line 227
    .restart local v1    # "subscription":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdSubscription;
    :cond_0
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v2

    goto :goto_0

    .line 229
    .end local v1    # "subscription":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdSubscription;
    :cond_1
    instance-of v2, p1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaEventResponse;

    if-eqz v2, :cond_2

    .line 230
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v4, "Received event response from RTA"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->gsonBuilder:Lcom/google/gson/Gson;

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->gsonBuilder:Lcom/google/gson/Gson;

    invoke-interface {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;->payload()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;->eventPayload()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/gson/Gson;->toJsonTree(Ljava/lang/Object;)Lcom/google/gson/JsonElement;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;

    invoke-virtual {v2, v3, v4}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;

    .line 233
    .local v0, "rtaEvent":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->handleRtaShoulderTap(Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;)Lio/reactivex/Single;

    move-result-object v2

    .line 234
    invoke-virtual {v2}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v2

    .line 235
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->onErrorResumeNext(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v2

    goto :goto_0

    .line 237
    .end local v0    # "rtaEvent":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v4, "Received unknown response type from RTA"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v2

    goto :goto_0
.end method

.method static synthetic lambda$null$10(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/Throwable;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 357
    instance-of v0, p1, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v2, "No party was active when attempting to allocate the relay. Ignoring."

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    return-void

    .line 361
    :cond_0
    invoke-static {p1}, Lio/reactivex/exceptions/Exceptions;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method static synthetic lambda$null$11(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "ignore"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 351
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->allocateCloudCompute()Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$24;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$25;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 352
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 365
    return-void
.end method

.method static synthetic lambda$null$2(Lretrofit2/Response;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "s"    # Lretrofit2/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 279
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$23(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "success"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 928
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyEvents:Lio/reactivex/subjects/PublishSubject;

    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberKickedEvent;->INSTANCE:Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberKickedEvent;

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 929
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cleanupParty()V

    .line 930
    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V
    .locals 0
    .param p0, "ignoreResult"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 307
    return-void
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/Throwable;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 310
    instance-of v0, p1, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v2, "No party was active when attempting to allocate the relay. Ignoring."

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    return-void

    .line 314
    :cond_0
    invoke-static {p1}, Lio/reactivex/exceptions/Exceptions;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "ignore"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 304
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->allocateCloudCompute()Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$28;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$29;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 305
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 318
    return-void
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/Throwable;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 322
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v2, "Encountered an error during party initialization"

    invoke-interface {v0, v1, v2, p1}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 323
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToStart:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    .line 325
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->setException(Ljava/lang/Throwable;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    move-result-object v1

    .line 323
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 326
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V
    .locals 0
    .param p0, "ignoreResult"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 354
    return-void
.end method

.method static synthetic lambda$toggleMemberMute$15(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lretrofit2/Response;)Lio/reactivex/SingleSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "result"    # Lretrofit2/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 596
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updatePrivacySettings()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$toggleMemberMute$16(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 598
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUnmute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    const-string v2, "PartyId"

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 599
    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v1

    const-string v2, "MemberXuid"

    .line 600
    invoke-virtual {v1, v2, p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v1

    .line 598
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 602
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hit error unmuting user: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, p2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 603
    return-void
.end method

.method static synthetic lambda$toggleMemberMute$17(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lretrofit2/Response;)Lio/reactivex/SingleSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "result"    # Lretrofit2/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 608
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updatePrivacySettings()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$toggleMemberMute$18(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 610
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToMute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    const-string v2, "PartyId"

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 611
    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v1

    const-string v2, "MemberXuid"

    .line 612
    invoke-virtual {v1, v2, p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v1

    .line 610
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 613
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hit error muting user: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, p2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 614
    return-void
.end method

.method static synthetic lambda$updateMultiplayerHandle$26(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "updatedSession"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1014
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 1015
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateLocalRoster()V

    .line 1017
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partySubject:Lio/reactivex/subjects/BehaviorSubject;

    invoke-static {}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->builder()Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 1018
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setMultiplayerSession(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v2

    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1019
    :goto_0
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setMuted(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    .line 1020
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setRoster(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v0

    .line 1021
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->build()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v0

    .line 1017
    invoke-virtual {v1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 1022
    return-void

    .line 1018
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$updateMultiplayerSession$25(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "updatedSession"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 993
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 994
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateLocalRoster()V

    .line 996
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partySubject:Lio/reactivex/subjects/BehaviorSubject;

    invoke-static {}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->builder()Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 997
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setMultiplayerSession(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v2

    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 998
    :goto_0
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setMuted(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    .line 999
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setRoster(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v0

    .line 1000
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->build()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v0

    .line 996
    invoke-virtual {v1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 1001
    return-void

    .line 997
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$updatePrivacySettings$27(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionResponse;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "ignore"    # Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1162
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    return-object v0
.end method

.method static synthetic lambda$updateQoSMeasurements$20(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/util/Map;)Lio/reactivex/SingleSource;
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "qosTimes"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 805
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatPing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    const-string v3, "PingInfo"

    .line 806
    invoke-virtual {v2, v3, p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v2

    .line 805
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 807
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 808
    invoke-interface {v2}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v2

    .line 809
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 810
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v4

    .line 811
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v5

    .line 812
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getCurrentUserIndex()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->index(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v5

    .line 813
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v5

    .line 811
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v4

    .line 814
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;

    move-result-object v4

    .line 810
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->constants(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 815
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v4

    .line 816
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;

    move-result-object v5

    .line 817
    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;->serverMeasurements(Ljava/util/Map;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;

    move-result-object v5

    .line 818
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;

    move-result-object v5

    .line 816
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v4

    .line 819
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;

    move-result-object v4

    .line 815
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->properties(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 820
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v3

    .line 808
    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->withSelf(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    .line 821
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v0

    .line 823
    .local v0, "sessionUpdate":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v1, :cond_0

    .line 824
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v1

    .line 827
    :goto_0
    return-object v1

    .line 826
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateQOSMeasurementsNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 827
    new-instance v1, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    invoke-direct {v1}, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;-><init>()V

    invoke-static {v1}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic lambda$updateQoSMeasurements$21(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/Throwable;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p1, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 831
    instance-of v0, p1, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    if-eqz v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateQOSMeasurementsNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 833
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v2, "No party was active when attempting to send QoS data."

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    :cond_0
    return-void
.end method

.method private onDataChannelConnected()V
    .locals 8

    .prologue
    .line 1081
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v3, "onDataChannelConnected"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1086
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1087
    .local v0, "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;->with(IJ)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->sendDataChannelMessage(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$PartyDataChannelMessage;)Z

    goto :goto_0

    .line 1089
    .end local v0    # "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatDataChannelConnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    const-string v3, "PartyId"

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 1090
    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v2

    .line 1089
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 1094
    :goto_1
    return-void

    .line 1092
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DataChannelConnected but currentParty is null or currentPartyId is missing. CurrentParty is null?: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " currentPartyId: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private onMemberStatusChange(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "userSsrc"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "isTalking"    # Z

    .prologue
    .line 1111
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1115
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v2, :cond_3

    .line 1116
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1117
    .local v0, "memberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    const-string/jumbo v2, "self"

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1118
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v4}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1121
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 1122
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->toBuilder()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v2

    .line 1123
    invoke-virtual {v2, p2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->isTalking(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v2

    .line 1124
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->build()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v1

    .line 1126
    .local v1, "partyUpdate":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1127
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMemberChanges:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v2, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 1145
    .end local v0    # "memberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    .end local v1    # "partyUpdate":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    :cond_1
    :goto_0
    return-void

    .line 1129
    .restart local v0    # "memberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1131
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 1132
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->toBuilder()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v2

    .line 1133
    invoke-virtual {v2, p2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->isTalking(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v2

    .line 1134
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->build()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v1

    .line 1136
    .restart local v1    # "partyUpdate":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1137
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMemberChanges:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v2, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 1142
    .end local v0    # "memberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    .end local v1    # "partyUpdate":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSessionMemberStatusChange:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 1143
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v4, "Received member status change when no party was active"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onTextChatMessageReceived(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "fromIndex"    # Ljava/lang/String;
    .param p2, "textMessage"    # Ljava/lang/String;

    .prologue
    .line 1097
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1098
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatMessageReceived:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    const-string v2, "PartyId"

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 1099
    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v2

    const-string v3, "MemberXuid"

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    .line 1100
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v0

    .line 1098
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 1101
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMessages:Lio/reactivex/subjects/ReplaySubject;

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-static {v0, p2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->with(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/reactivex/subjects/ReplaySubject;->onNext(Ljava/lang/Object;)V

    .line 1108
    :goto_0
    return-void

    .line 1105
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToReceiveMessageUserMissing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 1106
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not find text message sender with source: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processPrivacySettings(Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionResponse;)V
    .locals 7
    .param p1, "currentPermissions"    # Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionResponse;

    .prologue
    .line 1170
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionResponse;->responses()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;

    .line 1171
    .local v1, "permission":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;->permissions()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    .line 1173
    .local v2, "permissionValue":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1174
    .local v0, "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;->user()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;->xuid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1175
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1176
    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    .line 1177
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 1178
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->toBuilder()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v3

    .line 1179
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->privacyPermissions(Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v3

    .line 1180
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->build()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v3

    .line 1176
    invoke-interface {v5, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1182
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMemberChanges:Lio/reactivex/subjects/PublishSubject;

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 1189
    .end local v0    # "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    .end local v1    # "permission":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;
    .end local v2    # "permissionValue":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;
    :cond_2
    return-void
.end method

.method private setJoinability(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;)Lio/reactivex/Single;
    .locals 7
    .param p1, "newSetting"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 547
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 549
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v1, :cond_0

    .line 550
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 551
    invoke-interface {v2}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v2

    .line 552
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 553
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v4

    .line 554
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v5

    .line 555
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getCurrentUserIndex()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->index(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v5

    .line 556
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v5

    .line 554
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v4

    .line 557
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;

    move-result-object v4

    .line 553
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->constants(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 558
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v3

    .line 551
    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->withSelf(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    .line 559
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;

    move-result-object v2

    .line 560
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v3

    .line 561
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;->joinRestriction(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v3

    .line 562
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;

    move-result-object v3

    .line 560
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;

    move-result-object v2

    .line 563
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;

    move-result-object v2

    .line 559
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->properties(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    .line 564
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v0

    .line 566
    .local v0, "sessionRequest":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v1

    .line 569
    .end local v0    # "sessionRequest":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    :goto_0
    return-object v1

    .line 568
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateJoinabilityNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 569
    new-instance v1, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    invoke-direct {v1}, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;-><init>()V

    invoke-static {v1}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0
.end method

.method private updateConnectionIdForSession()Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 720
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateConnectionIdForSession. ConnectionId: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->connectionId:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " has currentParty "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v2, :cond_2

    const-string v2, " true"

    :goto_0
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 723
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->connectionId:Ljava/lang/String;

    .line 724
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 725
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    .line 726
    .local v0, "member":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v3}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 727
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;->system()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;->active()Ljava/lang/Boolean;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;->system()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;->active()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->connectionId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;->system()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;->connection()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 729
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 730
    invoke-interface {v3}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v3

    .line 731
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v4

    .line 732
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v5

    .line 733
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v6

    .line 734
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getCurrentUserIndex()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->index(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v6

    .line 735
    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v6

    .line 733
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v5

    .line 736
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;

    move-result-object v5

    .line 732
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->constants(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v4

    .line 738
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v5

    .line 740
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->connectionId:Ljava/lang/String;

    .line 741
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;->addConnection(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;

    move-result-object v6

    .line 742
    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;

    move-result-object v6

    .line 739
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v5

    .line 743
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;

    move-result-object v5

    .line 737
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->properties(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v4

    .line 744
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v4

    .line 730
    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->withSelf(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v2

    .line 745
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v1

    .line 747
    .local v1, "sessionRequest":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v2

    .line 756
    .end local v0    # "member":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    .end local v1    # "sessionRequest":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    :goto_1
    return-object v2

    .line 720
    :cond_2
    const-string v2, "false"

    goto/16 :goto_0

    .line 752
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v2, :cond_4

    .line 753
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-static {v2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v2

    goto :goto_1

    .line 755
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateConnectionIdNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 756
    new-instance v2, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    invoke-direct {v2}, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;-><init>()V

    invoke-static {v2}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v2

    goto :goto_1
.end method

.method private updateConnectionState(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;)Lio/reactivex/Single;
    .locals 7
    .param p1, "simpleConnectionState"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1192
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 1193
    invoke-interface {v2}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v2

    .line 1194
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 1195
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v4

    .line 1196
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v5

    .line 1197
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getCurrentUserIndex()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->index(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v5

    .line 1198
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v5

    .line 1196
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v4

    .line 1199
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;

    move-result-object v4

    .line 1195
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->constants(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 1200
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v4

    .line 1201
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v5

    .line 1202
    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->simpleConnectionState(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v5

    .line 1203
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;

    move-result-object v5

    .line 1201
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->custom(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v4

    .line 1204
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;

    move-result-object v4

    .line 1200
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->properties(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 1205
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v3

    .line 1193
    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->withSelf(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    .line 1206
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v0

    .line 1208
    .local v0, "sessionTemplate":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v1

    return-object v1
.end method

.method private updateLocalRoster()V
    .locals 14

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1212
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v9, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "updatingLocalRoster"

    invoke-interface {v6, v9, v10}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v6, :cond_8

    .line 1216
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1217
    .local v3, "memberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->membersInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MembersInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MembersInfo;->first()I

    move-result v10

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    if-ne v10, v6, :cond_2

    move v2, v7

    .line 1219
    .local v2, "currentUserIsHost":Z
    :goto_1
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v6, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1220
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v6, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 1221
    .local v0, "cachedMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    .line 1223
    .local v1, "currentMember":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->multiplayerMember()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isHost()Z

    move-result v6

    if-eq v6, v2, :cond_0

    .line 1224
    :cond_1
    iget-object v10, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    .line 1225
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    .line 1226
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->toBuilder()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v6

    .line 1227
    invoke-virtual {v6, v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->multiplayerMember(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v6

    .line 1228
    invoke-virtual {v6, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->isHost(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v12

    if-eqz v2, :cond_3

    iget-boolean v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isAudioEnabled:Z

    if-nez v6, :cond_3

    move v6, v7

    .line 1229
    :goto_2
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v12, v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->isSelfMuted(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v6

    .line 1230
    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->build()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v6

    .line 1224
    invoke-interface {v10, v11, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1232
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMemberChanges:Lio/reactivex/subjects/PublishSubject;

    iget-object v10, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v6, v10}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 1233
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isBroadcasting()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->isBroadcasting()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1234
    iget-object v10, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyEvents:Lio/reactivex/subjects/PublishSubject;

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v6, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-static {v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;->with(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;

    move-result-object v6

    invoke-virtual {v10, v6}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_0

    .end local v0    # "cachedMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .end local v1    # "currentMember":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    .end local v2    # "currentUserIsHost":Z
    :cond_2
    move v2, v8

    .line 1217
    goto/16 :goto_1

    .restart local v0    # "cachedMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .restart local v1    # "currentMember":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    .restart local v2    # "currentUserIsHost":Z
    :cond_3
    move v6, v8

    .line 1228
    goto :goto_2

    .line 1238
    .end local v0    # "cachedMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .end local v1    # "currentMember":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    :cond_4
    iget-object v10, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v11, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Discovered new party member: "

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v10, v11, v6}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1239
    iget-object v10, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    .line 1240
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    .line 1241
    invoke-static {}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->builder()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v12

    .line 1242
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    invoke-virtual {v12, v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->multiplayerMember(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v12

    .line 1243
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v12, v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->rosterKey(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v6

    .line 1244
    invoke-virtual {v6, v8}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->isTalking(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v6

    .line 1245
    invoke-virtual {v6, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->isHost(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v12

    if-eqz v2, :cond_5

    iget-boolean v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isAudioEnabled:Z

    if-nez v6, :cond_5

    move v6, v7

    .line 1246
    :goto_3
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v12, v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->isSelfMuted(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v6

    .line 1247
    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->build()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v6

    .line 1239
    invoke-interface {v10, v11, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1248
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMemberChanges:Lio/reactivex/subjects/PublishSubject;

    iget-object v10, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v6, v10}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 1249
    iget-object v10, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyEvents:Lio/reactivex/subjects/PublishSubject;

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v6, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-static {v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;->with(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;

    move-result-object v6

    invoke-virtual {v10, v6}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 1251
    iget-object v10, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v11, v12, v13}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;->with(IJ)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;

    move-result-object v6

    invoke-virtual {v10, v6}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->sendDataChannelMessage(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$PartyDataChannelMessage;)Z

    .line 1252
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updatePrivacySettings()Lio/reactivex/Single;

    move-result-object v6

    .line 1253
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v10

    invoke-virtual {v6, v10}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v6

    .line 1254
    invoke-virtual {v6}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    goto/16 :goto_0

    :cond_5
    move v6, v8

    .line 1245
    goto :goto_3

    .line 1261
    .end local v2    # "currentUserIsHost":Z
    .end local v3    # "memberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;>;"
    :cond_6
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "partyEntryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;>;"
    :cond_7
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1262
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1264
    .local v4, "partyEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 1265
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyEvents:Lio/reactivex/subjects/PublishSubject;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-static {v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberLeftEvent;->with(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberLeftEvent;

    move-result-object v6

    invoke-virtual {v7, v6}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 1266
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 1270
    .end local v4    # "partyEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    .end local v5    # "partyEntryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;>;"
    :cond_8
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v7, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSessionUpdateRoster:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v7, v8}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 1271
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v7, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v8, "Attempted to update local roster with no current active party!"

    invoke-interface {v6, v7, v8}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1273
    :cond_9
    return-void
.end method

.method private updateMultiplayerHandle(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;
    .locals 2
    .param p1, "handleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "sessionUpdate"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1010
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1012
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    invoke-interface {v0, p1, p2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$19;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 1013
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 1012
    return-object v0
.end method

.method private updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;
    .locals 3
    .param p1, "partyId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "sessionUpdate"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 990
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 991
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    const-string v1, "7492BACA-C1B4-440D-A391-B7EF364A8D40"

    const-string v2, "chat"

    invoke-interface {v0, v1, v2, p1, p2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->updateMultiplayerSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$18;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 992
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 1005
    :goto_0
    return-object v0

    .line 1004
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateMultiplayerSessionNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 1005
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    invoke-direct {v0}, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;-><init>()V

    invoke-static {v0}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0
.end method

.method private updatePrivacySettings()Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1148
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "updatingPrivacySettings"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1152
    .local v1, "targetXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1153
    .local v0, "memberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v4}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1154
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1158
    .end local v0    # "memberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 1159
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->privacyService:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v3}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionTypes;->CommunicateUsingVoice:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionTypes;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;->with(Ljava/util/List;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionTypes;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;->validatePermissions(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$20;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 1160
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$21;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 1161
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v2

    .line 1165
    :goto_1
    return-object v2

    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-static {v2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v2

    goto :goto_1
.end method

.method private updateQoSMeasurements()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 800
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v0, :cond_0

    .line 801
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->qosRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->getQoSReport()Lio/reactivex/Single;

    move-result-object v0

    .line 802
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 803
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 804
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 830
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnError(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 839
    :goto_0
    return-object v0

    .line 838
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 839
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    invoke-direct {v0}, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;-><init>()V

    invoke-static {v0}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0
.end method

.method private updateWebrtcConfiguration(Lorg/webrtc/SessionDescription;)Lio/reactivex/Single;
    .locals 10
    .param p1, "sdp"    # Lorg/webrtc/SessionDescription;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/webrtc/SessionDescription;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 761
    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v4, :cond_4

    .line 762
    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v7, "Updating webrtc config"

    invoke-interface {v4, v6, v7}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v0

    .line 766
    .local v0, "customBuilder":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    iget-object v4, p1, Lorg/webrtc/SessionDescription;->description:Ljava/lang/String;

    const-string v6, "\r\n"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    move v4, v5

    :goto_0
    if-ge v4, v7, :cond_3

    aget-object v2, v6, v4

    .line 767
    .local v2, "sdpPart":Ljava/lang/String;
    const-string v8, "a=fingerprint:"

    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 768
    const-string v8, "a=fingerprint:"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 769
    .local v1, "fingerprintParts":[Ljava/lang/String;
    aget-object v8, v1, v5

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->webRtcDtlsCertificateAlgorithm(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v0

    .line 770
    const/4 v8, 0x1

    aget-object v8, v1, v8

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->webRtcDtlsCertificateThumbprint(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v0

    .line 766
    .end local v1    # "fingerprintParts":[Ljava/lang/String;
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 771
    :cond_1
    const-string v8, "a=ice-ufrag:"

    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 772
    const-string v8, "a=ice-ufrag:"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->webRtcIceUfrag(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v0

    goto :goto_1

    .line 773
    :cond_2
    const-string v8, "a=ice-pwd"

    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 774
    const-string v8, "a=ice-pwd:"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->webRtcIcePwd(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v0

    goto :goto_1

    .line 779
    .end local v2    # "sdpPart":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 780
    invoke-interface {v5}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v5

    .line 781
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v6

    .line 782
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v7

    .line 783
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v8

    .line 784
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getCurrentUserIndex()Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->index(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v8

    .line 785
    invoke-virtual {v8}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v8

    .line 783
    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v7

    .line 786
    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;

    move-result-object v7

    .line 782
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->constants(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v6

    .line 787
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v7

    .line 788
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->custom(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v7

    .line 789
    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;

    move-result-object v7

    .line 787
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->properties(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v6

    .line 790
    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v6

    .line 780
    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->withSelf(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v4

    .line 791
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v3

    .line 793
    .local v3, "sessionRequest":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-direct {p0, v4, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v4

    .line 796
    .end local v0    # "customBuilder":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .end local v3    # "sessionRequest":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    :goto_2
    return-object v4

    .line 795
    :cond_4
    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v5, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v6, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateWebRTCConfigNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v5, v6}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 796
    new-instance v4, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    invoke-direct {v4}, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;-><init>()V

    invoke-static {v4}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v4

    goto :goto_2
.end method


# virtual methods
.method public createParty()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v2, "Creating party"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->rtaConnection:Lio/reactivex/Observable;

    const/4 v1, 0x0

    .line 291
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->ensureSinglePointOfPresence(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v1

    .line 292
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->createPartyInternal()Lio/reactivex/Single;

    move-result-object v2

    .line 293
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 294
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->flatMapObservable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 289
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getCachedRoster()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    return-object v0
.end method

.method public getJoinRestriction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;->system()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;->joinRestriction()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPartiesForUser(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 3
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 484
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 485
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    const-string v1, "7492BACA-C1B4-440D-A391-B7EF364A8D40"

    const-string v2, "chat"

    invoke-interface {v0, v1, v2, p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->getMultiplayerSessionsForUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public getPartyDetails(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 489
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 490
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    const-string v1, "7492BACA-C1B4-440D-A391-B7EF364A8D40"

    const-string v2, "chat"

    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionDetailsRequest;->getInstance()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionDetailsRequest;

    move-result-object v3

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->getMultiplayerSessionDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionDetailsRequest;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public getPartyEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyEvents:Lio/reactivex/subjects/PublishSubject;

    return-object v0
.end method

.method public getPartyMemberChanges()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMemberChanges:Lio/reactivex/subjects/PublishSubject;

    return-object v0
.end method

.method public getPartyMessages()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMessages:Lio/reactivex/subjects/ReplaySubject;

    return-object v0
.end method

.method public getPartySubject()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartySession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partySubject:Lio/reactivex/subjects/BehaviorSubject;

    return-object v0
.end method

.method public isAudioEnabled()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isAudioEnabled:Z

    return v0
.end method

.method public isRemoteAudioEnabled()Z
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled:Z

    return v0
.end method

.method public joinParty(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 4
    .param p1, "sessionId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "handleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 335
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->rtaConnection:Lio/reactivex/Observable;

    .line 337
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->ensureSinglePointOfPresence(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v1

    .line 338
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->joinPartyInternal(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    .line 339
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 340
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->flatMapObservable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 335
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public kickUser(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 9
    .param p1, "userXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1026
    new-instance v0, Lcom/google/gson/JsonObject;

    invoke-direct {v0}, Lcom/google/gson/JsonObject;-><init>()V

    .line 1027
    .local v0, "kickUserList":Lcom/google/gson/JsonObject;
    new-instance v1, Lcom/google/gson/JsonObject;

    invoke-direct {v1}, Lcom/google/gson/JsonObject;-><init>()V

    .line 1029
    .local v1, "kickUserObject":Lcom/google/gson/JsonObject;
    new-instance v3, Lcom/google/gson/JsonPrimitive;

    const-string v4, "kick"

    invoke-direct {v3, v4}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, v3}, Lcom/google/gson/JsonObject;->add(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 1030
    const-string v3, "kickusers"

    invoke-virtual {v0, v3, v1}, Lcom/google/gson/JsonObject;->add(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 1031
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v5, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatKick:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    const-string v5, "PartyId"

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 1032
    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v4

    const-string v5, "MemberXuid"

    .line 1033
    invoke-virtual {v4, v5, p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v4

    .line 1031
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 1034
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 1035
    invoke-interface {v4}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v4

    .line 1036
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v5

    .line 1037
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v6

    .line 1038
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v7

    .line 1039
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getCurrentUserIndex()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->index(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;

    move-result-object v7

    .line 1040
    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v7

    .line 1038
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;

    move-result-object v6

    .line 1041
    invoke-virtual {v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;

    move-result-object v6

    .line 1037
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->constants(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v5

    .line 1042
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v5

    .line 1035
    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->withSelf(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v3

    .line 1043
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;

    move-result-object v4

    .line 1044
    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;->custom(Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;

    move-result-object v4

    .line 1045
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;

    move-result-object v4

    .line 1043
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->properties(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v3

    .line 1046
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v2

    .line 1048
    .local v2, "sessionRequest":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-direct {p0, v3, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v3

    return-object v3
.end method

.method public leaveParty()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lretrofit2/Response",
            "<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 472
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    const-string v1, "7492BACA-C1B4-440D-A391-B7EF364A8D40"

    const-string v2, "chat"

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->leaveMultiplayerSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Action;

    move-result-object v1

    .line 473
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnComplete(Lio/reactivex/functions/Action;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 474
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnError(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 472
    return-object v0
.end method

.method public sendPartyInvite(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 5
    .param p1, "invitedXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 456
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 458
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v2, "Sending party invite"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatSendInvite:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    const-string v2, "PartyId"

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 460
    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v1

    const-string v2, "MemberXuid"

    .line 461
    invoke-virtual {v1, v2, p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v1

    .line 459
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 462
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->multiplayerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    .line 463
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandleType;->Invite:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandleType;

    .line 464
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;->type(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandleType;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;

    move-result-object v1

    const-string v2, "7492BACA-C1B4-440D-A391-B7EF364A8D40"

    const-string v3, "chat"

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 465
    invoke-static {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;->sessionRef(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;

    move-result-object v1

    .line 466
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;->invitedXuid(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;

    move-result-object v1

    .line 467
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;

    move-result-object v1

    .line 462
    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;->createMultiplayerHandle(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public sendTextChatMessage(Ljava/lang/String;)V
    .locals 6
    .param p1, "chatMessage"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 1052
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1054
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1055
    const-string v0, ""

    .line 1056
    .local v0, "currentMemberIndex":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1057
    .local v1, "memberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v4}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1058
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "currentMemberIndex":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 1063
    .end local v1    # "memberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;>;"
    .restart local v0    # "currentMemberIndex":Ljava/lang/String;
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1064
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;->with(ILjava/lang/String;)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->sendDataChannelMessage(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$PartyDataChannelMessage;)Z

    .line 1067
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMessages:Lio/reactivex/subjects/ReplaySubject;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-static {v2, p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->with(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    move-result-object v2

    invoke-virtual {v3, v2}, Lio/reactivex/subjects/ReplaySubject;->onNext(Ljava/lang/Object;)V

    .line 1078
    .end local v0    # "currentMemberIndex":Ljava/lang/String;
    :goto_0
    return-void

    .line 1070
    .restart local v0    # "currentMemberIndex":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToSendTextMessageUserMissing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    const-string v4, "PartyId"

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 1071
    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v3

    .line 1070
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 1072
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v4, "Could not find current user in party"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1075
    .end local v0    # "currentMemberIndex":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToSendTextMessageNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 1076
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v4, "There is no current active party!"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAllowedInBroadcast(Z)Lio/reactivex/Single;
    .locals 7
    .param p1, "allowedInBroadcast"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 514
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v1, :cond_0

    .line 515
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 516
    invoke-interface {v2}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v2

    .line 517
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 518
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v4

    .line 519
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v5

    .line 520
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->allowedInBroadcast(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v5

    .line 521
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;

    move-result-object v5

    .line 519
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->custom(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v4

    .line 522
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;

    move-result-object v4

    .line 518
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->properties(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 523
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v3

    .line 516
    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->withSelf(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    .line 524
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v0

    .line 526
    .local v0, "sessionTemplate":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v1

    .line 529
    .end local v0    # "sessionTemplate":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    :goto_0
    return-object v1

    .line 528
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateAbilityToBroadcastNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 529
    new-instance v1, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    invoke-direct {v1}, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;-><init>()V

    invoke-static {v1}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0
.end method

.method public setIsBroadcasting(Z)Lio/reactivex/Single;
    .locals 7
    .param p1, "isBroadcasting"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 494
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v1, :cond_0

    .line 495
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 496
    invoke-interface {v2}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v2

    .line 497
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 498
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v4

    .line 499
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;->builder()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v5

    .line 500
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->isBroadcasting(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v5

    .line 501
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;

    move-result-object v5

    .line 499
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->custom(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;

    move-result-object v4

    .line 502
    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;

    move-result-object v4

    .line 498
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->properties(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;

    move-result-object v3

    .line 503
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v3

    .line 496
    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->withSelf(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    .line 504
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v0

    .line 506
    .local v0, "sessionTemplate":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v1

    .line 509
    .end local v0    # "sessionTemplate":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    :goto_0
    return-object v1

    .line 508
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateBroadcastingStateNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 509
    new-instance v1, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    invoke-direct {v1}, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;-><init>()V

    invoke-static {v1}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0
.end method

.method public toggleJoinability()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 534
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v0, :cond_1

    .line 535
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;->Local:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->properties()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;->system()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;->joinRestriction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;->Followed:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->setJoinability(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;)Lio/reactivex/Single;

    move-result-object v0

    .line 542
    :goto_0
    return-object v0

    .line 538
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;->Local:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->setJoinability(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;)Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0

    .line 541
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateJoinabilityNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 542
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;

    invoke-direct {v0}, Lcom/microsoft/xbox/xbservices/domain/party/NoActivePartyException;-><init>()V

    invoke-static {v0}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0
.end method

.method public toggleMemberMute(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 7
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 574
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 575
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "toggleMemberMute"

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v3}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 578
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->toggleSelfMute()V

    .line 579
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-static {v3}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v3

    .line 607
    :goto_0
    return-object v3

    .line 581
    :cond_0
    const/4 v0, 0x0

    .line 583
    .local v0, "isUserMuted":Z
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 584
    .local v1, "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 585
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;->isAllowed()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x1

    .line 590
    .end local v1    # "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :cond_2
    :goto_1
    if-eqz v0, :cond_4

    .line 591
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unmuting user: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 593
    .local v2, "users":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;->with(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 595
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->privacyService:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v4}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;->with(Ljava/util/List;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;->unmuteUser(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;)Lio/reactivex/Single;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Function;

    move-result-object v4

    .line 596
    invoke-virtual {v3, v4}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v3

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/String;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 597
    invoke-virtual {v3, v4}, Lio/reactivex/Single;->doOnError(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v3

    goto/16 :goto_0

    .line 585
    .end local v2    # "users":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;>;"
    .restart local v1    # "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 606
    .end local v1    # "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Muting user: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->privacyService:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v4}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;->with(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;->muteUser(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;)Lio/reactivex/Single;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)Lio/reactivex/functions/Function;

    move-result-object v4

    .line 608
    invoke-virtual {v3, v4}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v3

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/String;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 609
    invoke-virtual {v3, v4}, Lio/reactivex/Single;->doOnError(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public toggleMuteRemoteAudio()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 647
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Toggling mute for remote audio. Previous value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled:Z

    .line 649
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled:Z

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->setRemoteAudioEnabled(Z)V

    .line 651
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partySubject:Lio/reactivex/subjects/BehaviorSubject;

    invoke-static {}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->builder()Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 652
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setMultiplayerSession(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled:Z

    if-nez v4, :cond_1

    .line 653
    :goto_1
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setMuted(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    .line 654
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->setRoster(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;

    move-result-object v1

    .line 655
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;->build()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v1

    .line 651
    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 656
    return-void

    :cond_0
    move v0, v2

    .line 648
    goto :goto_0

    :cond_1
    move v1, v2

    .line 652
    goto :goto_1
.end method

.method public toggleSelfMute()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 620
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentParty:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 621
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Toggling mute for local user. Previous value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isAudioEnabled:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isAudioEnabled:Z

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isAudioEnabled:Z

    .line 623
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->webRtcManager:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    iget-boolean v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isAudioEnabled:Z

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->setAudioEnabled(Z)V

    .line 625
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getCurrentUserIndex()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 626
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getCurrentUserIndex()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 627
    .local v0, "self":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->toBuilder()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v1

    iget-boolean v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isAudioEnabled:Z

    if-nez v4, :cond_1

    .line 628
    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->isSelfMuted(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;

    move-result-object v1

    .line 629
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;->build()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v0

    .line 631
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->cachedRoster:Ljava/util/Map;

    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getCurrentUserIndex()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 632
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Updated self mute state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isSelfMuted()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->partyMemberChanges:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v1, v0}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 644
    .end local v0    # "self":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    :goto_2
    return-void

    :cond_0
    move v1, v3

    .line 622
    goto :goto_0

    .restart local v0    # "self":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    :cond_1
    move v2, v3

    .line 627
    goto :goto_1

    .line 636
    .end local v0    # "self":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isAudioEnabled:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUnmute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    :goto_3
    invoke-direct {v3, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    const-string v1, "PartyId"

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->currentPartyId:Ljava/lang/String;

    .line 637
    invoke-virtual {v3, v1, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v1

    const-string v3, "MemberXuid"

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->xuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 638
    invoke-interface {v4}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v1

    .line 636
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 639
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v3, "Could not find local user in roster"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 636
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToMute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    goto :goto_3

    .line 642
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->TAG:Ljava/lang/String;

    const-string v3, "Attempted to mute when no party was present."

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
