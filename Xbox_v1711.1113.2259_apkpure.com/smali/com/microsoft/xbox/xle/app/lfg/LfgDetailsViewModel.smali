.class public Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "LfgDetailsViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;
    }
.end annotation


# static fields
.field private static CLUB_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

.field private static MULTIPLAYER_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

.field private static final TAG:Ljava/lang/String;

.field private static TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;


# instance fields
.field private approvedMembers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;",
            ">;"
        }
    .end annotation
.end field

.field private club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

.field private getPersonSummaries:Lio/reactivex/disposables/Disposable;

.field private getStats:Lio/reactivex/disposables/Disposable;

.field private handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

.field private handleId:Ljava/lang/String;

.field private hostMember:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

.field private interestedMembers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;",
            ">;"
        }
    .end annotation
.end field

.field private isLoading:Z

.field private isPreparingLfgShare:Z

.field multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private personSummaries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field private selectedTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

.field private selectedTitleId:J

.field private session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

.field private timePlayedValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const-class v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    .line 86
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->CLUB_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    .line 87
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->MULTIPLAYER_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 8
    .param p1, "parentScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 114
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 116
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V

    .line 118
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->selectedTitleId:J

    .line 119
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getMultiplayerHandleId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handleId:Ljava/lang/String;

    .line 121
    const-string v3, "CreateLfgDetailsViewModel requires a valid titleId in the activity parameters."

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->selectedTitleId:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 122
    const-string v0, "LfgDetailsViewModel requires a valid handleId in the activity parameters"

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handleId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->approvedMembers:Ljava/util/List;

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->interestedMembers:Ljava/util/List;

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->personSummaries:Ljava/util/Map;

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->timePlayedValue:Ljava/util/Map;

    .line 128
    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;-><init>(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 129
    return-void

    :cond_0
    move v0, v2

    .line 121
    goto :goto_0

    :cond_1
    move v1, v2

    .line 122
    goto :goto_1
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)Lio/reactivex/Single;
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->loadClubModel(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->onLoadCompleted(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;)V

    return-void
.end method

.method static synthetic access$lambda$2(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->onLfgSharePrepared(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)V

    return-void
.end method

.method static synthetic access$lambda$3(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->onUserDataLoadCompleted(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$4(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->onUserStatsLoadCompleted(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;)V

    return-void
.end method

.method static synthetic lambda$joinSession$6(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Ljava/lang/String;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
    .param p1, "userText"    # Ljava/lang/String;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handleId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isMemberOfSession()Z

    move-result v0

    if-nez v0, :cond_0

    .line 421
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isLoading:Z

    .line 422
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->MULTIPLAYER_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handleId:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->joinSession(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 423
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 424
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$19;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$20;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 425
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 437
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->updateAdapter()V

    .line 440
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissEditDialog()V

    .line 441
    return-void
.end method

.method static synthetic lambda$joinSession$7(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Lretrofit2/Response;)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
    .param p1, "success"    # Lretrofit2/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 453
    invoke-virtual {p1}, Lretrofit2/Response;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Successfully cancelled LFG"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->AddForceRefresh(Ljava/lang/Class;)V

    .line 457
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->AddForceRefresh(Ljava/lang/Class;)V

    .line 458
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->AddForceRefresh(Ljava/lang/Class;)V

    .line 459
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->AddForceRefresh(Ljava/lang/Class;)V

    .line 460
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->goBack()V

    .line 469
    :goto_0
    return-void

    .line 462
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Encountered error leaving LFG: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lretrofit2/Response;->code()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070b6d

    .line 464
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0707ba

    .line 465
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 466
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 463
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic lambda$joinSession$8(Ljava/lang/Throwable;)V
    .locals 5
    .param p0, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 471
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Encountered unknown error leaving LFG"

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 472
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070b6d

    .line 473
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0707ba

    .line 474
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 475
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 472
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 478
    return-void
.end method

.method static synthetic lambda$load$0(ZLcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)Lio/reactivex/SingleSource;
    .locals 6
    .param p0, "forceRefresh"    # Z
    .param p1, "titleData"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 179
    iget-wide v2, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    .line 181
    .local v0, "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {v0, p0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->rxLoadGameProgressAchievements(ZLcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$22;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 183
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    .line 186
    :goto_0
    return-object v1

    .line 185
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not load titleModel for title id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic lambda$load$1(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;Lcom/google/common/base/Optional;Lcom/google/common/base/Optional;)Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
    .param p1, "x$0"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .param p2, "x$1"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .param p3, "x$2"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    .param p4, "x$3"    # Lcom/google/common/base/Optional;
    .param p5, "x$4"    # Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 191
    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;-><init>(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;Lcom/google/common/base/Optional;Lcom/google/common/base/Optional;)V

    return-object v0
.end method

.method static synthetic lambda$load$3(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Ljava/lang/Throwable;)V
    .locals 8
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
    .param p1, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 196
    sget-object v3, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v4, "Failure during loading:"

    invoke-static {v3, v4, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 197
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isLoading:Z

    .line 198
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0706e4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 199
    .local v1, "dialogTitle":Ljava/lang/String;
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070708

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 200
    .local v2, "errorMessage":Ljava/lang/String;
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0706b5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 202
    .local v0, "buttonText":Ljava/lang/String;
    instance-of v3, p1, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v3, :cond_0

    check-cast p1, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local p1    # "err":Ljava/lang/Throwable;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v4

    const-wide/16 v6, 0x15

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 203
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07071c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 206
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$21;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-interface {v3, v1, v2, v0, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 207
    return-void
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->goBack()V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
    .param p1, "result"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 426
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    .line 427
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isLoading:Z

    .line 428
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->updateAdapter()V

    .line 429
    return-void
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Ljava/lang/Throwable;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
    .param p1, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 431
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Encountered an error joining the LFG: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f0706eb

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    .line 433
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isLoading:Z

    .line 434
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->updateAdapter()V

    .line 435
    return-void
.end method

.method static synthetic lambda$showLfgShareDialog$10(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
    .param p1, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 576
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to prepare LFG Share"

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 577
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isPreparingLfgShare:Z

    .line 578
    const v0, 0x7f0706b8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->showError(I)V

    .line 579
    return-void
.end method

.method static synthetic lambda$showLfgShareDialog$9(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 1
    .param p0, "postTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 571
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getUserPostsService()Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;->prepareLfgPost(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$updateSession$11(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "memberXuids"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 644
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->immediateQuery(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$updateSession$12(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 648
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to load person summaries"

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic lambda$updateSession$13(Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .locals 2
    .param p0, "request"    # Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 660
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;->getProfileStatisticsRequestBody(Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getProfileStatisticsInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$updateSession$14(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 664
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to load user stats"

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private loadClubModel(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)Lio/reactivex/Single;
    .locals 6
    .param p1, "handle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 213
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->clubId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 214
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->clubId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 216
    .local v0, "clubId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 217
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->CLUB_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    const/4 v3, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    invoke-static {v5}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->rxLoad(ZLjava/lang/Long;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 218
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v2

    .line 222
    .end local v0    # "clubId":J
    :goto_0
    return-object v2

    :cond_0
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v2

    invoke-static {v2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v2

    goto :goto_0
.end method

.method private onLfgSharePrepared(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)V
    .locals 4
    .param p1, "result"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 584
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isPreparingLfgShare:Z

    .line 586
    if-eqz p1, :cond_0

    .line 587
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackShareExistingLFG(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 588
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postUri()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Lfg:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->validShareTargets()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;Ljava/util/List;)V

    .line 590
    :cond_0
    return-void
.end method

.method private onLoadCompleted(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;)V
    .locals 2
    .param p1, "loadCompletionBundle"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;

    .prologue
    .line 226
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Load completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isLoading:Z

    .line 229
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->selectedTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 230
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;->multiplayerHandle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 231
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;->multiplayerSession:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    .line 232
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 234
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->updateAdapter()V

    .line 235
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->updateSession(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V

    .line 236
    return-void
.end method

.method private declared-synchronized onUserDataLoadCompleted(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 670
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v2, "onUserDataLoadCompleted"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 673
    .local v0, "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->personSummaries:Ljava/util/Map;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 670
    .end local v0    # "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 676
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 677
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized onUserStatsLoadCompleted(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;)V
    .locals 7
    .param p1, "result"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    .prologue
    .line 680
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onUserStatsLoadCompleted "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    if-eqz p1, :cond_2

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 683
    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 684
    .local v1, "statistics":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 685
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 686
    .local v0, "stat":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    if-eqz v0, :cond_1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    sget-object v5, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 687
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->timePlayedValue:Ljava/util/Map;

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->xuid:Ljava/lang/String;

    iget-object v6, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 680
    .end local v0    # "stat":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .end local v1    # "statistics":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 694
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 695
    monitor-exit p0

    return-void
.end method

.method private updateSession(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V
    .locals 10
    .param p1, "session"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 617
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 619
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    .line 620
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v0

    .line 621
    .local v0, "host":Ljava/lang/String;
    :goto_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->approvedMembers:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 622
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->interestedMembers:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 623
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    .line 624
    .local v4, "sessionMembers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 626
    .local v2, "memberXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    .line 627
    .local v1, "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 629
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 630
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->hostMember:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    goto :goto_1

    .line 620
    .end local v0    # "host":Ljava/lang/String;
    .end local v1    # "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    .end local v2    # "memberXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "sessionMembers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 631
    .restart local v0    # "host":Ljava/lang/String;
    .restart local v1    # "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    .restart local v2    # "memberXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v4    # "sessionMembers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    :cond_1
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->roles()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->roles()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;->lfg()Ljava/lang/String;

    move-result-object v7

    const-string v8, "confirmed"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 632
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->approvedMembers:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 634
    :cond_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->interestedMembers:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 638
    .end local v1    # "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    :cond_3
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v6

    if-lez v6, :cond_6

    .line 639
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getPersonSummaries:Lio/reactivex/disposables/Disposable;

    if-eqz v6, :cond_4

    .line 640
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getPersonSummaries:Lio/reactivex/disposables/Disposable;

    invoke-virtual {v6, v7}, Lio/reactivex/disposables/CompositeDisposable;->remove(Lio/reactivex/disposables/Disposable;)Z

    .line 641
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getPersonSummaries:Lio/reactivex/disposables/Disposable;

    invoke-interface {v6}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 644
    :cond_4
    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$13;->lambdaFactory$(Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v6

    invoke-static {v6}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v6

    .line 645
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v7

    invoke-virtual {v6, v7}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v6

    .line 646
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v7

    invoke-virtual {v6, v7}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v6

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v7

    invoke-static {}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$15;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v8

    .line 647
    invoke-virtual {v6, v7, v8}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v6

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getPersonSummaries:Lio/reactivex/disposables/Disposable;

    .line 649
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getPersonSummaries:Lio/reactivex/disposables/Disposable;

    invoke-virtual {v6, v7}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 651
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getStats:Lio/reactivex/disposables/Disposable;

    if-eqz v6, :cond_5

    .line 652
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getStats:Lio/reactivex/disposables/Disposable;

    invoke-virtual {v6, v7}, Lio/reactivex/disposables/CompositeDisposable;->remove(Lio/reactivex/disposables/Disposable;)Z

    .line 653
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getStats:Lio/reactivex/disposables/Disposable;

    invoke-interface {v6}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 656
    :cond_5
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 657
    .local v5, "stats":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;>;"
    new-instance v6, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;

    sget-object v7, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->toString()Ljava/lang/String;

    move-result-object v7

    iget-wide v8, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->selectedTitleId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 658
    new-instance v3, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    invoke-direct {v3, v2, v6, v5}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 660
    .local v3, "request":Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$16;->lambdaFactory$(Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;)Ljava/util/concurrent/Callable;

    move-result-object v6

    invoke-static {v6}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v6

    .line 661
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v7

    invoke-virtual {v6, v7}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v6

    .line 662
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v7

    invoke-virtual {v6, v7}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v6

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$17;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v7

    invoke-static {}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$18;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v8

    .line 663
    invoke-virtual {v6, v7, v8}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v6

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getStats:Lio/reactivex/disposables/Disposable;

    .line 665
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getStats:Lio/reactivex/disposables/Disposable;

    invoke-virtual {v6, v7}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 667
    .end local v3    # "request":Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;
    .end local v5    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;>;"
    :cond_6
    return-void
.end method

.method private validShareTargets()Ljava/util/List;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;",
            ">;"
        }
    .end annotation

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isPreparingLfgShare:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->hostMember:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    if-eqz v0, :cond_4

    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->selectedTitleId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 317
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 318
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 319
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 320
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 321
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->postedTime()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->clubId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 333
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->searchHandleVisibility()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;->XboxLive:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    if-ne v0, v1, :cond_0

    .line 334
    invoke-static {}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->allTargets()Ljava/util/List;

    move-result-object v0

    .line 361
    :goto_0
    return-object v0

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 337
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 338
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 339
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 340
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->allowedValues()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 341
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->allowedValues()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 342
    invoke-static {}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->allTargets()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 345
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsMemberOf()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 346
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->SpecificClub:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->Messages:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 351
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->searchHandleVisibility()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;->XboxLive:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    if-ne v0, v1, :cond_3

    .line 352
    invoke-static {}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->allTargets()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 354
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->hostMember:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 355
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->Messages:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 361
    :cond_4
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public canShareLfg()Z
    .locals 1

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->validShareTargets()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declineMember(Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;)V
    .locals 4
    .param p1, "member"    # Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 390
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 392
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 393
    .local v0, "multiplayerMember":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->getMemberSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v3

    iget-object v3, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 394
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->getMemberSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackDeclineMember(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;Ljava/lang/String;)V

    .line 395
    sget-object v2, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->declineLfgMember(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    .end local v0    # "multiplayerMember":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->updateAdapter()V

    .line 402
    return-void
.end method

.method public dismissLfgViewAllDetailDialog()V
    .locals 1

    .prologue
    .line 535
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissLfgViewAllDetailDialog()V

    .line 536
    return-void
.end method

.method public getApprovedMembers()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 371
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->approvedMembers:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getClub()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 255
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    return-object v0
.end method

.method public getHostMember()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 366
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->hostMember:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    return-object v0
.end method

.method public declared-synchronized getInterestedMembers()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 376
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->interestedMembers:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMemberSummary(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 381
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->personSummaries:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method public getMembershipForCurrentUser()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 265
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    if-eqz v2, :cond_1

    .line 266
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    .line 268
    .local v1, "sessionMembers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 269
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    .line 270
    .local v0, "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 277
    .end local v0    # "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    .end local v1    # "sessionMembers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNeedCount()Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getMembershipForCurrentUser()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v1

    .line 284
    .local v1, "selfMembership":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getHostMember()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 285
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->roles()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 286
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v3, 0x7f0706df

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 298
    :goto_0
    return-object v2

    .line 288
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v3, 0x7f0706e3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 291
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    .line 292
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->getNeedCount()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->interestedMembers:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    sub-int v0, v2, v3

    .line 295
    .local v0, "needCount":I
    :goto_1
    if-lez v0, :cond_3

    .line 296
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 292
    .end local v0    # "needCount":I
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 293
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getRemainingNeedCount()I

    move-result v0

    goto :goto_1

    .line 298
    .restart local v0    # "needCount":I
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v3, 0x7f0706e1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getSelectedTitle()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->selectedTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    return-object v0
.end method

.method public getSessionDetails()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    return-object v0
.end method

.method public getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 245
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    return-object v0
.end method

.method public declared-synchronized getTimePlayedForMember(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 386
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->timePlayedValue:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isBlockingBusy()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isLoading:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isPreparingLfgShare:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isLoading:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isPreparingLfgShare:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHost()Z
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->hostMember:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->hostMember:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMemberOfSession()Z
    .locals 3

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getMembershipForCurrentUser()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v0

    .line 260
    .local v0, "selfMembership":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public joinSession()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 405
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v7

    .line 407
    .local v7, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCreateOrJoinLFG()Z

    move-result v1

    if-eqz v7, :cond_1

    .line 408
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanCommunicateWithTextAndVoice()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07049d

    .line 409
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0706ba

    .line 410
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 407
    invoke-static {v1, v0, v2, v3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFailedPermissionsDialog(ZZLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 411
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isMemberOfSession()Z

    move-result v0

    if-nez v0, :cond_2

    .line 412
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isHost()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackInterested(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Z)V

    .line 413
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0706ca

    .line 414
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0706dc

    .line 415
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0b0017

    .line 416
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;

    move-result-object v6

    .line 413
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showEditTextDialog(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;)V

    .line 482
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v5

    .line 408
    goto :goto_0

    .line 444
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isHost()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackLeaveOrCancelGroup(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Z)V

    .line 445
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 446
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;->scid()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 447
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;->templateName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 448
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;->name()Ljava/lang/String;

    move-result-object v3

    .line 445
    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;->leaveMultiplayerSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    .line 449
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 450
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 451
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    goto :goto_1
.end method

.method public load(Z)V
    .locals 10
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 158
    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isLoading:Z

    if-nez v5, :cond_0

    .line 159
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isLoading:Z

    .line 162
    sget-object v5, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    const/4 v6, 0x0

    iget-wide v8, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->selectedTitleId:J

    invoke-virtual {v5, v6, v8, v9}, Lcom/microsoft/xbox/service/model/TitleHubModel;->rxLoad(ZJ)Lio/reactivex/Single;

    move-result-object v0

    .line 164
    .local v0, "titleDataSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handleId:Ljava/lang/String;

    invoke-interface {v5, v6}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;->getMultiplayerHandle(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v5

    invoke-virtual {v5}, Lio/reactivex/Single;->cache()Lio/reactivex/Single;

    move-result-object v1

    .line 167
    .local v1, "multiplayerHandleSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handleId:Ljava/lang/String;

    invoke-interface {v5, v6}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;->getMultiplayerSession(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    .line 171
    .local v2, "multiplayerSessionSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;>;"
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->computation()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v1, v5}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Function;

    move-result-object v6

    .line 172
    invoke-virtual {v5, v6}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v3

    .line 177
    .local v3, "clubSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/google/common/base/Optional<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->computation()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v0, v5}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v5

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$2;->lambdaFactory$(Z)Lio/reactivex/functions/Function;

    move-result-object v6

    .line 178
    invoke-virtual {v5, v6}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v4

    .line 190
    .local v4, "achievementsSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/google/common/base/Optional<Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;>;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Function5;

    move-result-object v5

    .line 191
    invoke-static/range {v0 .. v5}, Lio/reactivex/Single;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Single;

    move-result-object v5

    .line 192
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v7

    invoke-virtual {v5, v7}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v5

    .line 193
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v7

    invoke-virtual {v5, v7}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v7

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v8

    .line 194
    invoke-virtual {v5, v7, v8}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v5

    .line 190
    invoke-virtual {v6, v5}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 209
    .end local v0    # "titleDataSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    .end local v1    # "multiplayerHandleSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .end local v2    # "multiplayerSessionSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;>;"
    .end local v3    # "clubSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/google/common/base/Optional<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    .end local v4    # "achievementsSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/google/common/base/Optional<Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;>;>;"
    :cond_0
    return-void
.end method

.method public navigateToEnforcement()V
    .locals 7

    .prologue
    const v3, 0x7f070b6d

    .line 486
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v1

    if-nez v1, :cond_0

    .line 487
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Cannot navigate to Enforcement screen, sessionHandle is null!"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->showError(I)V

    .line 513
    :goto_0
    return-void

    .line 490
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v1

    if-nez v1, :cond_1

    .line 491
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Cannot navigate to Enforcement screen, sessionReference is null!"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->showError(I)V

    goto :goto_0

    .line 494
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 495
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Cannot navigate to Enforcement screen, sessionHandle id is null!"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->showError(I)V

    goto :goto_0

    .line 498
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getHostMember()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v1

    if-nez v1, :cond_3

    .line 499
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Cannot navigate to Enforcement screen, hostMember is null!"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->showError(I)V

    goto :goto_0

    .line 503
    :cond_3
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->LfgHost:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handleId:Ljava/lang/String;

    .line 506
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getHostMember()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v3

    .line 507
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getHostMember()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->gamertag()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 509
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;-><init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;)V

    .line 510
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getHostMember()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackReportHost(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Ljava/lang/String;)V

    .line 511
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;-><init>(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 139
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->MULTIPLAYER_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 134
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->MULTIPLAYER_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 144
    return-void
.end method

.method public onTapCloseButton()V
    .locals 1

    .prologue
    .line 526
    const-string v0, "LFG - Close"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 527
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->goBack()V

    .line 528
    return-void
.end method

.method public onTapInterestedButton()V
    .locals 4

    .prologue
    .line 516
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    if-eqz v1, :cond_0

    .line 517
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 518
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMultiplayerSession(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V

    .line 519
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMultiplayerHandle(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 520
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->selectedTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 521
    const-class v1, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreen;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 523
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method public showLfgShareDialog()V
    .locals 14

    .prologue
    .line 539
    iget-boolean v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isPreparingLfgShare:Z

    if-nez v10, :cond_2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->canShareLfg()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 540
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isPreparingLfgShare:Z

    .line 542
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->hostMember:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v0

    .line 543
    .local v0, "hostXuid":Ljava/lang/String;
    iget-wide v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->selectedTitleId:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 544
    .local v1, "selectedTitleId":Ljava/lang/String;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;->scid()Ljava/lang/String;

    move-result-object v2

    .line 545
    .local v2, "scid":Ljava/lang/String;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v3

    .line 546
    .local v3, "sessionId":Ljava/lang/String;
    const/4 v4, 0x0

    .line 547
    .local v4, "description":Ljava/lang/String;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->tags()Ljava/util/List;

    move-result-object v5

    .line 548
    .local v5, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->achievementIds()Ljava/util/List;

    move-result-object v6

    .line 549
    .local v6, "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 550
    .local v7, "scheduledTime":Ljava/util/Date;
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    .line 552
    .local v8, "postedTime":Ljava/util/Date;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 553
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 554
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;->text()Ljava/lang/String;

    move-result-object v4

    .line 556
    :cond_0
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->scheduledTime()Ljava/util/Date;

    move-result-object v7

    .line 557
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->postedTime()Ljava/util/Date;

    move-result-object v8

    .line 560
    :cond_1
    invoke-static/range {v0 .. v8}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/Date;Ljava/util/Date;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    move-result-object v9

    .line 570
    .local v9, "postTypeData":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v9}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)Ljava/util/concurrent/Callable;

    move-result-object v11

    .line 571
    invoke-static {v11}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v11

    .line 572
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v12

    invoke-virtual {v11, v12}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v11

    .line 573
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v12

    invoke-virtual {v11, v12}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v11

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v12

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v13

    .line 574
    invoke-virtual {v11, v12, v13}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v11

    .line 570
    invoke-virtual {v10, v11}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 581
    .end local v0    # "hostXuid":Ljava/lang/String;
    .end local v1    # "selectedTitleId":Ljava/lang/String;
    .end local v2    # "scid":Ljava/lang/String;
    .end local v3    # "sessionId":Ljava/lang/String;
    .end local v4    # "description":Ljava/lang/String;
    .end local v5    # "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "scheduledTime":Ljava/util/Date;
    .end local v8    # "postedTime":Ljava/util/Date;
    .end local v9    # "postTypeData":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    :cond_2
    return-void
.end method

.method public showLfgViewAllDetailDialog()V
    .locals 1

    .prologue
    .line 531
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showLfgViewAllDetailDialog(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V

    .line 532
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 594
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "updateOverride"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 597
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 598
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 600
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 608
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown update type ignored: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 614
    return-void

    .line 602
    .restart local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Completed call to declineLfgSession"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->load(Z)V

    .line 604
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->updateAdapter()V

    goto :goto_0

    .line 600
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
