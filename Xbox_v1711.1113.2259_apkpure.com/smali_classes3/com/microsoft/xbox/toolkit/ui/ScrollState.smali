.class public Lcom/microsoft/xbox/toolkit/ui/ScrollState;
.super Ljava/lang/Object;
.source "ScrollState.java"


# instance fields
.field private final index:I

.field private final offset:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "offset"    # I

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/ScrollState;->index:I

    .line 9
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/ScrollState;->offset:I

    .line 10
    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScrollState;->index:I

    return v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScrollState;->offset:I

    return v0
.end method
