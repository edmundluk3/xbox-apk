.class public Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;
.super Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;
.source "LeadingIconTextView.java"


# static fields
.field private static final nbsp:Ljava/lang/String; = "\u00a0"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected getFormatSpans(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v8, 0x7fc00000    # NaNf

    .line 26
    sget-object v7, Lcom/microsoft/xboxone/smartglass/R$styleable;->LeadingIconTextView:[I

    invoke-virtual {p1, p2, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 27
    .local v0, "array":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 29
    const/4 v7, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v5

    .line 30
    .local v5, "textSize":F
    const/4 v7, 0x2

    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 31
    .local v4, "textFont":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v7

    invoke-virtual {v7, p1, v4}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    .line 32
    .local v6, "textTypeface":Landroid/graphics/Typeface;
    new-instance v7, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    invoke-direct {v7, p0, v4, v6, v5}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;-><init>(Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;Ljava/lang/String;Landroid/graphics/Typeface;F)V

    iput-object v7, p0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->mainTypefaceSpan1:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    .line 33
    new-instance v7, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    invoke-direct {v7, p0, v4, v6, v5}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;-><init>(Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;Ljava/lang/String;Landroid/graphics/Typeface;F)V

    iput-object v7, p0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->mainTypefaceSpan2:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    .line 36
    const-string v1, "fonts/SegXboxSymbol.ttf"

    .line 37
    .local v1, "iconFontFace":Ljava/lang/String;
    const/4 v7, 0x4

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    .line 38
    .local v2, "iconFontSize":F
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v7

    invoke-virtual {v7, p1, v1}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 39
    .local v3, "iconFontTypeface":Landroid/graphics/Typeface;
    new-instance v7, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    invoke-direct {v7, p0, v1, v3, v2}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;-><init>(Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;Ljava/lang/String;Landroid/graphics/Typeface;F)V

    iput-object v7, p0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->formatTypefaceSpan:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    .line 41
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 43
    .end local v1    # "iconFontFace":Ljava/lang/String;
    .end local v2    # "iconFontSize":F
    .end local v3    # "iconFontTypeface":Landroid/graphics/Typeface;
    .end local v4    # "textFont":Ljava/lang/String;
    .end local v5    # "textSize":F
    .end local v6    # "textTypeface":Landroid/graphics/Typeface;
    :cond_0
    return-void
.end method

.method protected getFormatStrings(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v8, 0x0

    .line 47
    const/4 v2, 0x0

    .line 48
    .local v2, "leadingIcon":Ljava/lang/String;
    const/4 v4, 0x0

    .line 49
    .local v4, "spacesBeforeText":I
    const/4 v3, 0x0

    .line 51
    .local v3, "spacesBeforeIcon":I
    sget-object v7, Lcom/microsoft/xboxone/smartglass/R$styleable;->LeadingIconTextView:[I

    invoke-virtual {p1, p2, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 52
    .local v0, "array":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 53
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->unformattedString:Ljava/lang/String;

    .line 54
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 55
    const/4 v7, 0x5

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    .line 56
    const/4 v7, 0x6

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    .line 57
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 60
    :cond_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->unformattedString:Ljava/lang/String;

    if-nez v7, :cond_1

    .line 61
    const-string v7, ""

    iput-object v7, p0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->unformattedString:Ljava/lang/String;

    .line 64
    :cond_1
    if-eqz v2, :cond_4

    .line 65
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    .local v5, "spacesPost":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 67
    const-string v7, "\u00a0"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    .local v6, "spacesPre":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_3

    .line 72
    const-string v7, "\u00a0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 75
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->formatString:Ljava/lang/String;

    .line 76
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "%1$s "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->unformattedString:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->unformattedString:Ljava/lang/String;

    .line 79
    .end local v1    # "i":I
    .end local v5    # "spacesPost":Ljava/lang/StringBuilder;
    .end local v6    # "spacesPre":Ljava/lang/StringBuilder;
    :cond_4
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->formatString:Ljava/lang/String;

    if-nez v7, :cond_5

    .line 80
    const-string v7, ""

    iput-object v7, p0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->formatString:Ljava/lang/String;

    .line 82
    :cond_5
    return-void
.end method
