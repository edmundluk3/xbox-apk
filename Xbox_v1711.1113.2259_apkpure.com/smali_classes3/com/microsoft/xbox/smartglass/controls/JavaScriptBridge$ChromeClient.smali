.class Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ChromeClient;
.super Landroid/webkit/WebChromeClient;
.source "JavaScriptBridge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ChromeClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ChromeClient;->this$0:Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onConsoleMessage(Landroid/webkit/ConsoleMessage;)Z
    .locals 6
    .param p1, "message"    # Landroid/webkit/ConsoleMessage;

    .prologue
    const/4 v5, 0x1

    .line 150
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v0

    const-string v1, "Console: %s (%d) %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->sourceId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->lineNumber()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->message()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Information:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    .line 151
    return v5
.end method
