.class final enum Lcom/microsoft/xbox/xle/app/Feedback$LogType;
.super Ljava/lang/Enum;
.source "Feedback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/Feedback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "LogType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/Feedback$LogType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/Feedback$LogType;

.field public static final enum APP:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

.field public static final enum CRASH:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

.field public static final enum PLATFORM:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

.field public static final enum SCREENSHOT:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

.field public static final enum VIDEO:Lcom/microsoft/xbox/xle/app/Feedback$LogType;


# instance fields
.field private final extension:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 108
    new-instance v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    const-string v1, "APP"

    const-string v2, "log"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/xle/app/Feedback$LogType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->APP:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    .line 109
    new-instance v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    const-string v1, "PLATFORM"

    const-string v2, "platform.log"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/xle/app/Feedback$LogType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->PLATFORM:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    .line 110
    new-instance v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    const-string v1, "SCREENSHOT"

    const-string v2, ".jpg"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/xle/app/Feedback$LogType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->SCREENSHOT:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    .line 111
    new-instance v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    const-string v1, "VIDEO"

    const-string v2, "video.log"

    invoke-direct {v0, v1, v6, v2}, Lcom/microsoft/xbox/xle/app/Feedback$LogType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->VIDEO:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    .line 112
    new-instance v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    const-string v1, "CRASH"

    const-string v2, "crash.log"

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/xle/app/Feedback$LogType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->CRASH:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    .line 107
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->APP:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->PLATFORM:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->SCREENSHOT:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->VIDEO:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->CRASH:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->$VALUES:[Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "extension"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 117
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->extension:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/Feedback$LogType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 107
    const-class v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/Feedback$LogType;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->$VALUES:[Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/Feedback$LogType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    return-object v0
.end method


# virtual methods
.method public getFilename()Ljava/lang/String;
    .locals 5

    .prologue
    .line 121
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s.%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "SmartGlass"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->extension:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
