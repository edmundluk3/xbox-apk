.class public final Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "RefreshScreenInvokerRnModule.java"


# static fields
.field private static final NAME:Ljava/lang/String; = "RefreshScreenInvoker"


# instance fields
.field private volatile callback:Lcom/facebook/react/bridge/Callback;

.field private final refreshEventDisposable:Lio/reactivex/disposables/Disposable;


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 3
    .param p1, "reactContext"    # Lcom/facebook/react/bridge/ReactApplicationContext;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    .line 22
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 24
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getRefreshEvents()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 26
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;->refreshEventDisposable:Lio/reactivex/disposables/Disposable;

    .line 31
    :goto_0
    return-void

    .line 28
    :cond_0
    const-string v1, "Main activity was null when React tried to hook to refresh events"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 29
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;->refreshEventDisposable:Lio/reactivex/disposables/Disposable;

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;Lcom/microsoft/xbox/toolkit/rx/RxUtils$RxNotification;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;
    .param p1, "ignore"    # Lcom/microsoft/xbox/toolkit/rx/RxUtils$RxNotification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;->callback:Lcom/facebook/react/bridge/Callback;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string v0, "RefreshScreenInvoker"

    return-object v0
.end method

.method public onCatalystInstanceDestroy()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;->onCatalystInstanceDestroy()V

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;->refreshEventDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;->refreshEventDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 49
    :cond_0
    return-void
.end method

.method public setCallback(Lcom/facebook/react/bridge/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/facebook/react/bridge/Callback;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;->callback:Lcom/facebook/react/bridge/Callback;

    .line 41
    return-void
.end method
