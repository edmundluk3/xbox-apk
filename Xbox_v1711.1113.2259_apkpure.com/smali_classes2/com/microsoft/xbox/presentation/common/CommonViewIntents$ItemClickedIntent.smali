.class public abstract Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;
.super Ljava/lang/Object;
.source "CommonViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/common/CommonViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ItemClickedIntent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    .local p0, "this":Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;, "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "item":Ljava/lang/Object;, "TT;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 63
    new-instance v0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_ItemClickedIntent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_ItemClickedIntent;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public abstract item()Ljava/lang/Object;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    .local p0, "this":Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;, "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent<TT;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
