.class public abstract Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;
.super Ljava/lang/Object;
.source "PartyInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/party/PartyInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ToggleMemberMuteAction"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/String;)Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;
    .locals 1
    .param p0, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 600
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 601
    new-instance v0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_ToggleMemberMuteAction;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_ToggleMemberMuteAction;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 606
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ToggleMemberMuteAction: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;->xuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract xuid()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
