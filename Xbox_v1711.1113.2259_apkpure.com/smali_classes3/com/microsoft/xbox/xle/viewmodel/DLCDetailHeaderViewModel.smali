.class public Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "DLCDetailHeaderViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 15
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getDLCDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 16
    return-void
.end method


# virtual methods
.method public getAverageUserRating()F
    .locals 5

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    instance-of v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getAverageUserRating()F

    move-result v0

    .line 43
    :goto_0
    return v0

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_1

    .line 33
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Expected EDSV2GameContentDetailModel mediaModel!  Existing class: %s - Existing Description: %s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    .line 36
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    .line 37
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getDescription()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    .line 34
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 33
    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 40
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->TAG:Ljava/lang/String;

    const-string v1, "mediaModel is null!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 20
    const v0, 0x7f070683

    return v0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getDLCDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 26
    return-void
.end method
