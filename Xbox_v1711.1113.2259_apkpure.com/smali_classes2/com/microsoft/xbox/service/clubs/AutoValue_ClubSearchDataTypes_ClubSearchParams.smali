.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;
.super Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
.source "AutoValue_ClubSearchDataTypes_ClubSearchParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;
    }
.end annotation


# instance fields
.field private final query:Ljava/lang/String;

.field private final tags:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titles:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p2, "tags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    .local p3, "titles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->query:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->tags:Lcom/google/common/collect/ImmutableList;

    .line 22
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->titles:Lcom/google/common/collect/ImmutableList;

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lcom/google/common/collect/ImmutableList;
    .param p3, "x2"    # Lcom/google/common/collect/ImmutableList;
    .param p4, "x3"    # Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$1;

    .prologue
    .line 10
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    if-ne p1, p0, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    if-eqz v3, :cond_6

    move-object v0, p1

    .line 58
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    .line 59
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->query:Ljava/lang/String;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->query()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->tags:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_4

    .line 60
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->tags()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->titles:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_5

    .line 61
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->titles()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 59
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->query:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->query()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 60
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->tags:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->tags()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 61
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->titles:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->titles()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    :cond_6
    move v1, v2

    .line 63
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 68
    const/4 v0, 0x1

    .line 69
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->query:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 71
    mul-int/2addr v0, v3

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->tags:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 73
    mul-int/2addr v0, v3

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->titles:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_2

    :goto_2
    xor-int/2addr v0, v2

    .line 75
    return v0

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->query:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 72
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->tags:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_1

    .line 74
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->titles:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public query()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->query:Ljava/lang/String;

    return-object v0
.end method

.method public tags()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->tags:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public titles()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->titles:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public toBuilder()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;-><init>(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubSearchParams{query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->tags:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;->titles:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
