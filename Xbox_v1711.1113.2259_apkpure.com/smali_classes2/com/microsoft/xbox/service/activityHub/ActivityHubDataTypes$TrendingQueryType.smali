.class public final enum Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;
.super Ljava/lang/Enum;
.source "ActivityHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrendingQueryType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

.field public static final enum Global:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

.field public static final enum Title:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

.field public static final enum Topic:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

.field public static final enum User:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    const-string v1, "Global"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->Global:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    .line 42
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    const-string v1, "Topic"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->Topic:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    const-string v1, "Title"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->Title:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    const-string v1, "User"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->User:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    .line 40
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->Global:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->Topic:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->Title:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->User:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->$VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    const-class v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->$VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    return-object v0
.end method
