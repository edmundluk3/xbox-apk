.class public Lcom/microsoft/xbox/xle/remote/VolumeDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "VolumeDialog.java"


# instance fields
.field private closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private containerView:Landroid/view/View;

.field private downButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private muteToggleButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private softDismissView:Landroid/view/View;

.field private upButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private viewModel:Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    const v0, 0x7f0802e2

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 27
    const v0, 0x7f030273

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->setContentView(I)V

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->setCanceledOnTouchOutside(Z)V

    .line 30
    const v0, 0x7f0e0bc9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->softDismissView:Landroid/view/View;

    .line 31
    const v0, 0x7f0e0bcb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->containerView:Landroid/view/View;

    .line 32
    const v0, 0x7f0e0bca

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 33
    const v0, 0x7f0e0bcc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->downButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 34
    const v0, 0x7f0e0bce

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->upButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 35
    const v0, 0x7f0e0bcd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->muteToggleButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 37
    new-instance v0, Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->viewModel:Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->isAmazonDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->forceKindleRespectDimOptions()V

    .line 42
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/remote/VolumeDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->dismissSelf()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/remote/VolumeDialog;)Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->viewModel:Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/remote/VolumeDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->downButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/remote/VolumeDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->upButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/remote/VolumeDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->muteToggleButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissVolumeDialog()V

    .line 58
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->dismissSelf()V

    .line 54
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, -0x1

    .line 46
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 49
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->viewModel:Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->viewModel:Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;->onResume()V

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->softDismissView:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/remote/VolumeDialog$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog$1;-><init>(Lcom/microsoft/xbox/xle/remote/VolumeDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->containerView:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/remote/VolumeDialog$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog$2;-><init>(Lcom/microsoft/xbox/xle/remote/VolumeDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/remote/VolumeDialog$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog$3;-><init>(Lcom/microsoft/xbox/xle/remote/VolumeDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->downButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/remote/VolumeDialog$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog$4;-><init>(Lcom/microsoft/xbox/xle/remote/VolumeDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->upButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/remote/VolumeDialog$5;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog$5;-><init>(Lcom/microsoft/xbox/xle/remote/VolumeDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->muteToggleButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/remote/VolumeDialog$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog$6;-><init>(Lcom/microsoft/xbox/xle/remote/VolumeDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->viewModel:Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->viewModel:Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;->onPause()V

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->softDismissView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->containerView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->downButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->upButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->muteToggleButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    return-void
.end method
