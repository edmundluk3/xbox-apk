.class Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetPeopleHubPeopleDataRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/FollowersData;",
        ">;>;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private filter:Lcom/microsoft/xbox/service/model/FollowersFilter;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;Lcom/microsoft/xbox/service/model/FollowersFilter;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "filter"    # Lcom/microsoft/xbox/service/model/FollowersFilter;

    .prologue
    .line 3052
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 3053
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 3054
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->xuid:Ljava/lang/String;

    .line 3055
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->filter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    .line 3056
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3047
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 3060
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3061
    .local v4, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->xuid:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 3062
    const/4 v3, 0x0

    .line 3063
    .local v3, "recentsFromPeopleHub":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    sget-object v5, Lcom/microsoft/xbox/service/model/ProfileModel$3;->$SwitchMap$com$microsoft$xbox$service$model$FollowersFilter:[I

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->filter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 3086
    :cond_0
    :goto_0
    if-eqz v3, :cond_2

    .line 3087
    if-eqz v3, :cond_2

    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 3088
    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 3089
    .local v1, "p":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    new-instance v6, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-direct {v6, v1}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 3065
    .end local v1    # "p":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->xuid:Ljava/lang/String;

    invoke-interface {v5, v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getFollowingFromPeopleHub(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    move-result-object v3

    .line 3066
    goto :goto_0

    .line 3068
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    invoke-interface {v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getRecentsFromPeopleHub()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    move-result-object v3

    .line 3069
    goto :goto_0

    .line 3071
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    invoke-interface {v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getFollowersFromPeopleHub()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    move-result-object v3

    .line 3074
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->xuid:Ljava/lang/String;

    invoke-interface {v5, v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getProfileSummaryInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;

    move-result-object v2

    .line 3075
    .local v2, "profileSummary":Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    if-eqz v2, :cond_0

    .line 3076
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget v6, v2, Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;->recentChangeCount:I

    invoke-static {v5, v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$2702(Lcom/microsoft/xbox/service/model/ProfileModel;I)I
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3078
    .end local v2    # "profileSummary":Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    :catch_0
    move-exception v0

    .line 3081
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v5, Lcom/microsoft/xbox/service/model/ProfileModel;->TAG:Ljava/lang/String;

    const-string v6, "Error in getting recent change count"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3093
    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->filter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    sget-object v6, Lcom/microsoft/xbox/service/model/FollowersFilter;->RECENTPLAYERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

    if-ne v5, v6, :cond_3

    .line 3094
    new-instance v5, Lcom/microsoft/xbox/service/model/ProfileModel$FollowersAndRecentPlayersTitleComparator;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/service/model/ProfileModel$FollowersAndRecentPlayersTitleComparator;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel$1;)V

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 3102
    .end local v3    # "recentsFromPeopleHub":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :cond_2
    :goto_2
    return-object v4

    .line 3095
    .restart local v3    # "recentsFromPeopleHub":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :cond_3
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->filter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    sget-object v6, Lcom/microsoft/xbox/service/model/FollowersFilter;->FOLLOWERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

    if-eq v5, v6, :cond_2

    .line 3096
    new-instance v5, Lcom/microsoft/xbox/service/model/ProfileModel$FollowingAndFavoritesComparator;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/service/model/ProfileModel$FollowingAndFavoritesComparator;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel$1;)V

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_2

    .line 3063
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 3117
    const-wide/16 v0, 0xbc2

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 3111
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleHubPeopleDataRunner;->filter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$2900(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/service/model/FollowersFilter;)V

    .line 3112
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 3107
    return-void
.end method
