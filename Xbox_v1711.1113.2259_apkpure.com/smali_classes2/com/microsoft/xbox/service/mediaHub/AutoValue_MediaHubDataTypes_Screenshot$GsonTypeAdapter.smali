.class public final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MediaHubDataTypes_Screenshot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;",
        ">;"
    }
.end annotation


# instance fields
.field private final captureDateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final commentCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final contentIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final contentLocatorsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;",
            ">;>;"
        }
    .end annotation
.end field

.field private final contentStateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final creationTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final dateUploadedAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private defaultCaptureDate:Ljava/util/Date;

.field private defaultCommentCount:Ljava/lang/Integer;

.field private defaultContentId:Ljava/lang/String;

.field private defaultContentLocators:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;",
            ">;"
        }
    .end annotation
.end field

.field private defaultContentState:Ljava/lang/String;

.field private defaultCreationType:Ljava/lang/String;

.field private defaultDateUploaded:Ljava/util/Date;

.field private defaultEnforcementState:Ljava/lang/String;

.field private defaultLikeCount:Ljava/lang/Integer;

.field private defaultLocalId:Ljava/lang/String;

.field private defaultOwnerXuid:Ljava/lang/Long;

.field private defaultResolutionHeight:Ljava/lang/Integer;

.field private defaultResolutionWidth:Ljava/lang/Integer;

.field private defaultSandboxId:Ljava/lang/String;

.field private defaultSessions:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;",
            ">;"
        }
    .end annotation
.end field

.field private defaultShareCount:Ljava/lang/Integer;

.field private defaultStats:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;",
            ">;"
        }
    .end annotation
.end field

.field private defaultTitleData:Ljava/lang/String;

.field private defaultTitleId:Ljava/lang/String;

.field private defaultTitleName:Ljava/lang/String;

.field private defaultTournaments:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;",
            ">;"
        }
    .end annotation
.end field

.field private defaultUploadDeviceType:Ljava/lang/String;

.field private defaultUploadLanguage:Ljava/lang/String;

.field private defaultUploadRegion:Ljava/lang/String;

.field private defaultUploadTitleId:Ljava/lang/Long;

.field private defaultUserCaption:Ljava/lang/String;

.field private defaultViewCount:Ljava/lang/Integer;

.field private final enforcementStateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final likeCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final localIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final ownerXuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final resolutionHeightAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final resolutionWidthAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final sandboxIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;",
            ">;>;"
        }
    .end annotation
.end field

.field private final shareCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final statsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;",
            ">;>;"
        }
    .end annotation
.end field

.field private final titleDataAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titleIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titleNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tournamentsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;",
            ">;>;"
        }
    .end annotation
.end field

.field private final uploadDeviceTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadLanguageAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadRegionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadTitleIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final userCaptionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final viewCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 5
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 86
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultCaptureDate:Ljava/util/Date;

    .line 60
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 61
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultContentLocators:Lcom/google/common/collect/ImmutableList;

    .line 62
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultCreationType:Ljava/lang/String;

    .line 63
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultLocalId:Ljava/lang/String;

    .line 64
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/Long;

    .line 65
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultResolutionHeight:Ljava/lang/Integer;

    .line 66
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultResolutionWidth:Ljava/lang/Integer;

    .line 67
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultSandboxId:Ljava/lang/String;

    .line 68
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTitleData:Ljava/lang/String;

    .line 69
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTitleId:Ljava/lang/String;

    .line 70
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 71
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultDateUploaded:Ljava/util/Date;

    .line 72
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadLanguage:Ljava/lang/String;

    .line 73
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadRegion:Ljava/lang/String;

    .line 74
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadTitleId:Ljava/lang/Long;

    .line 75
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadDeviceType:Ljava/lang/String;

    .line 76
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUserCaption:Ljava/lang/String;

    .line 77
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultCommentCount:Ljava/lang/Integer;

    .line 78
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultLikeCount:Ljava/lang/Integer;

    .line 79
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultShareCount:Ljava/lang/Integer;

    .line 80
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultViewCount:Ljava/lang/Integer;

    .line 81
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultContentState:Ljava/lang/String;

    .line 82
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultEnforcementState:Ljava/lang/String;

    .line 83
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultSessions:Lcom/google/common/collect/ImmutableList;

    .line 84
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultStats:Lcom/google/common/collect/ImmutableList;

    .line 85
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTournaments:Lcom/google/common/collect/ImmutableList;

    .line 87
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->captureDateAdapter:Lcom/google/gson/TypeAdapter;

    .line 88
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 89
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->contentLocatorsAdapter:Lcom/google/gson/TypeAdapter;

    .line 90
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->creationTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 91
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->localIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 92
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 93
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->resolutionHeightAdapter:Lcom/google/gson/TypeAdapter;

    .line 94
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->resolutionWidthAdapter:Lcom/google/gson/TypeAdapter;

    .line 95
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->sandboxIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 96
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->titleDataAdapter:Lcom/google/gson/TypeAdapter;

    .line 97
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 98
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 99
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->dateUploadedAdapter:Lcom/google/gson/TypeAdapter;

    .line 100
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadLanguageAdapter:Lcom/google/gson/TypeAdapter;

    .line 101
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadRegionAdapter:Lcom/google/gson/TypeAdapter;

    .line 102
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadTitleIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 103
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadDeviceTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 104
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->userCaptionAdapter:Lcom/google/gson/TypeAdapter;

    .line 105
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->commentCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 106
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->likeCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 107
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->shareCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 108
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->viewCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 109
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->contentStateAdapter:Lcom/google/gson/TypeAdapter;

    .line 110
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->enforcementStateAdapter:Lcom/google/gson/TypeAdapter;

    .line 111
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->sessionsAdapter:Lcom/google/gson/TypeAdapter;

    .line 112
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->statsAdapter:Lcom/google/gson/TypeAdapter;

    .line 113
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->tournamentsAdapter:Lcom/google/gson/TypeAdapter;

    .line 114
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;
    .locals 31
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 289
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v30, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v30

    if-ne v1, v0, :cond_0

    .line 290
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 291
    const/4 v1, 0x0

    .line 442
    :goto_0
    return-object v1

    .line 293
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 294
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultCaptureDate:Ljava/util/Date;

    .line 295
    .local v2, "captureDate":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 296
    .local v3, "contentId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultContentLocators:Lcom/google/common/collect/ImmutableList;

    .line 297
    .local v4, "contentLocators":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultCreationType:Ljava/lang/String;

    .line 298
    .local v5, "creationType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultLocalId:Ljava/lang/String;

    .line 299
    .local v6, "localId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/Long;

    .line 300
    .local v7, "ownerXuid":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultResolutionHeight:Ljava/lang/Integer;

    .line 301
    .local v8, "resolutionHeight":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultResolutionWidth:Ljava/lang/Integer;

    .line 302
    .local v9, "resolutionWidth":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultSandboxId:Ljava/lang/String;

    .line 303
    .local v10, "sandboxId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTitleData:Ljava/lang/String;

    .line 304
    .local v11, "titleData":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTitleId:Ljava/lang/String;

    .line 305
    .local v12, "titleId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 306
    .local v13, "titleName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultDateUploaded:Ljava/util/Date;

    .line 307
    .local v14, "dateUploaded":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadLanguage:Ljava/lang/String;

    .line 308
    .local v15, "uploadLanguage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadRegion:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 309
    .local v16, "uploadRegion":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadTitleId:Ljava/lang/Long;

    move-object/from16 v17, v0

    .line 310
    .local v17, "uploadTitleId":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadDeviceType:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 311
    .local v18, "uploadDeviceType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUserCaption:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 312
    .local v19, "userCaption":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultCommentCount:Ljava/lang/Integer;

    move-object/from16 v20, v0

    .line 313
    .local v20, "commentCount":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultLikeCount:Ljava/lang/Integer;

    move-object/from16 v21, v0

    .line 314
    .local v21, "likeCount":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultShareCount:Ljava/lang/Integer;

    move-object/from16 v22, v0

    .line 315
    .local v22, "shareCount":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultViewCount:Ljava/lang/Integer;

    move-object/from16 v23, v0

    .line 316
    .local v23, "viewCount":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultContentState:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 317
    .local v24, "contentState":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultEnforcementState:Ljava/lang/String;

    move-object/from16 v25, v0

    .line 318
    .local v25, "enforcementState":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultSessions:Lcom/google/common/collect/ImmutableList;

    move-object/from16 v26, v0

    .line 319
    .local v26, "sessions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultStats:Lcom/google/common/collect/ImmutableList;

    move-object/from16 v27, v0

    .line 320
    .local v27, "stats":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTournaments:Lcom/google/common/collect/ImmutableList;

    move-object/from16 v28, v0

    .line 321
    .local v28, "tournaments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;>;"
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 322
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v29

    .line 323
    .local v29, "_name":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v30, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v30

    if-ne v1, v0, :cond_1

    .line 324
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 327
    :cond_1
    const/4 v1, -0x1

    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->hashCode()I

    move-result v30

    sparse-switch v30, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v1, :pswitch_data_0

    .line 437
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 327
    :sswitch_0
    const-string v30, "captureDate"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_1
    const-string v30, "contentId"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :sswitch_2
    const-string v30, "contentLocators"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/4 v1, 0x2

    goto :goto_2

    :sswitch_3
    const-string v30, "creationType"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/4 v1, 0x3

    goto :goto_2

    :sswitch_4
    const-string v30, "localId"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/4 v1, 0x4

    goto :goto_2

    :sswitch_5
    const-string v30, "ownerXuid"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/4 v1, 0x5

    goto :goto_2

    :sswitch_6
    const-string v30, "resolutionHeight"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/4 v1, 0x6

    goto :goto_2

    :sswitch_7
    const-string v30, "resolutionWidth"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/4 v1, 0x7

    goto :goto_2

    :sswitch_8
    const-string v30, "sandboxId"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x8

    goto :goto_2

    :sswitch_9
    const-string v30, "titleData"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x9

    goto :goto_2

    :sswitch_a
    const-string v30, "titleId"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0xa

    goto :goto_2

    :sswitch_b
    const-string v30, "titleName"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0xb

    goto/16 :goto_2

    :sswitch_c
    const-string v30, "dateUploaded"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0xc

    goto/16 :goto_2

    :sswitch_d
    const-string v30, "uploadLanguage"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0xd

    goto/16 :goto_2

    :sswitch_e
    const-string v30, "uploadRegion"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0xe

    goto/16 :goto_2

    :sswitch_f
    const-string v30, "uploadTitleId"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0xf

    goto/16 :goto_2

    :sswitch_10
    const-string v30, "uploadDeviceType"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x10

    goto/16 :goto_2

    :sswitch_11
    const-string v30, "userCaption"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x11

    goto/16 :goto_2

    :sswitch_12
    const-string v30, "commentCount"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x12

    goto/16 :goto_2

    :sswitch_13
    const-string v30, "likeCount"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x13

    goto/16 :goto_2

    :sswitch_14
    const-string v30, "shareCount"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x14

    goto/16 :goto_2

    :sswitch_15
    const-string v30, "viewCount"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x15

    goto/16 :goto_2

    :sswitch_16
    const-string v30, "contentState"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x16

    goto/16 :goto_2

    :sswitch_17
    const-string v30, "enforcementState"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x17

    goto/16 :goto_2

    :sswitch_18
    const-string v30, "sessions"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x18

    goto/16 :goto_2

    :sswitch_19
    const-string v30, "stats"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x19

    goto/16 :goto_2

    :sswitch_1a
    const-string v30, "tournaments"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    const/16 v1, 0x1a

    goto/16 :goto_2

    .line 329
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->captureDateAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "captureDate":Ljava/util/Date;
    check-cast v2, Ljava/util/Date;

    .line 330
    .restart local v2    # "captureDate":Ljava/util/Date;
    goto/16 :goto_1

    .line 333
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "contentId":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 334
    .restart local v3    # "contentId":Ljava/lang/String;
    goto/16 :goto_1

    .line 337
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->contentLocatorsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "contentLocators":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    check-cast v4, Lcom/google/common/collect/ImmutableList;

    .line 338
    .restart local v4    # "contentLocators":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    goto/16 :goto_1

    .line 341
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->creationTypeAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "creationType":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 342
    .restart local v5    # "creationType":Ljava/lang/String;
    goto/16 :goto_1

    .line 345
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->localIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "localId":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 346
    .restart local v6    # "localId":Ljava/lang/String;
    goto/16 :goto_1

    .line 349
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "ownerXuid":Ljava/lang/Long;
    check-cast v7, Ljava/lang/Long;

    .line 350
    .restart local v7    # "ownerXuid":Ljava/lang/Long;
    goto/16 :goto_1

    .line 353
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->resolutionHeightAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "resolutionHeight":Ljava/lang/Integer;
    check-cast v8, Ljava/lang/Integer;

    .line 354
    .restart local v8    # "resolutionHeight":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 357
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->resolutionWidthAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "resolutionWidth":Ljava/lang/Integer;
    check-cast v9, Ljava/lang/Integer;

    .line 358
    .restart local v9    # "resolutionWidth":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 361
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->sandboxIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "sandboxId":Ljava/lang/String;
    check-cast v10, Ljava/lang/String;

    .line 362
    .restart local v10    # "sandboxId":Ljava/lang/String;
    goto/16 :goto_1

    .line 365
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->titleDataAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "titleData":Ljava/lang/String;
    check-cast v11, Ljava/lang/String;

    .line 366
    .restart local v11    # "titleData":Ljava/lang/String;
    goto/16 :goto_1

    .line 369
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "titleId":Ljava/lang/String;
    check-cast v12, Ljava/lang/String;

    .line 370
    .restart local v12    # "titleId":Ljava/lang/String;
    goto/16 :goto_1

    .line 373
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "titleName":Ljava/lang/String;
    check-cast v13, Ljava/lang/String;

    .line 374
    .restart local v13    # "titleName":Ljava/lang/String;
    goto/16 :goto_1

    .line 377
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->dateUploadedAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "dateUploaded":Ljava/util/Date;
    check-cast v14, Ljava/util/Date;

    .line 378
    .restart local v14    # "dateUploaded":Ljava/util/Date;
    goto/16 :goto_1

    .line 381
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadLanguageAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "uploadLanguage":Ljava/lang/String;
    check-cast v15, Ljava/lang/String;

    .line 382
    .restart local v15    # "uploadLanguage":Ljava/lang/String;
    goto/16 :goto_1

    .line 385
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadRegionAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "uploadRegion":Ljava/lang/String;
    check-cast v16, Ljava/lang/String;

    .line 386
    .restart local v16    # "uploadRegion":Ljava/lang/String;
    goto/16 :goto_1

    .line 389
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadTitleIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "uploadTitleId":Ljava/lang/Long;
    check-cast v17, Ljava/lang/Long;

    .line 390
    .restart local v17    # "uploadTitleId":Ljava/lang/Long;
    goto/16 :goto_1

    .line 393
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadDeviceTypeAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "uploadDeviceType":Ljava/lang/String;
    check-cast v18, Ljava/lang/String;

    .line 394
    .restart local v18    # "uploadDeviceType":Ljava/lang/String;
    goto/16 :goto_1

    .line 397
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->userCaptionAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v19

    .end local v19    # "userCaption":Ljava/lang/String;
    check-cast v19, Ljava/lang/String;

    .line 398
    .restart local v19    # "userCaption":Ljava/lang/String;
    goto/16 :goto_1

    .line 401
    :pswitch_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->commentCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "commentCount":Ljava/lang/Integer;
    check-cast v20, Ljava/lang/Integer;

    .line 402
    .restart local v20    # "commentCount":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 405
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->likeCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v21

    .end local v21    # "likeCount":Ljava/lang/Integer;
    check-cast v21, Ljava/lang/Integer;

    .line 406
    .restart local v21    # "likeCount":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 409
    :pswitch_14
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->shareCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v22

    .end local v22    # "shareCount":Ljava/lang/Integer;
    check-cast v22, Ljava/lang/Integer;

    .line 410
    .restart local v22    # "shareCount":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 413
    :pswitch_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->viewCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v23

    .end local v23    # "viewCount":Ljava/lang/Integer;
    check-cast v23, Ljava/lang/Integer;

    .line 414
    .restart local v23    # "viewCount":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 417
    :pswitch_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->contentStateAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v24

    .end local v24    # "contentState":Ljava/lang/String;
    check-cast v24, Ljava/lang/String;

    .line 418
    .restart local v24    # "contentState":Ljava/lang/String;
    goto/16 :goto_1

    .line 421
    :pswitch_17
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->enforcementStateAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v25

    .end local v25    # "enforcementState":Ljava/lang/String;
    check-cast v25, Ljava/lang/String;

    .line 422
    .restart local v25    # "enforcementState":Ljava/lang/String;
    goto/16 :goto_1

    .line 425
    :pswitch_18
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->sessionsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v26

    .end local v26    # "sessions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;>;"
    check-cast v26, Lcom/google/common/collect/ImmutableList;

    .line 426
    .restart local v26    # "sessions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;>;"
    goto/16 :goto_1

    .line 429
    :pswitch_19
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->statsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v27

    .end local v27    # "stats":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;>;"
    check-cast v27, Lcom/google/common/collect/ImmutableList;

    .line 430
    .restart local v27    # "stats":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;>;"
    goto/16 :goto_1

    .line 433
    :pswitch_1a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->tournamentsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v28

    .end local v28    # "tournaments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;>;"
    check-cast v28, Lcom/google/common/collect/ImmutableList;

    .line 434
    .restart local v28    # "tournaments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;>;"
    goto/16 :goto_1

    .line 441
    .end local v29    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 442
    new-instance v1, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot;

    invoke-direct/range {v1 .. v28}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot;-><init>(Ljava/util/Date;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;)V

    goto/16 :goto_0

    .line 327
    :sswitch_data_0
    .sparse-switch
        -0x7ff88b87 -> :sswitch_d
        -0x7f5465de -> :sswitch_9
        -0x7f4fdafd -> :sswitch_b
        -0x73f9af86 -> :sswitch_7
        -0x6c1edb90 -> :sswitch_14
        -0x672b23ab -> :sswitch_e
        -0x5f4efa96 -> :sswitch_15
        -0x4deb0a6d -> :sswitch_a
        -0x30571c56 -> :sswitch_1a
        -0x2aeedc6f -> :sswitch_10
        -0x250b3b0d -> :sswitch_6
        -0x1843fc8c -> :sswitch_1
        -0xce113ae -> :sswitch_f
        -0xb4d2848 -> :sswitch_13
        -0x2999bd2 -> :sswitch_c
        0x10f262b -> :sswitch_5
        0x1c5c636 -> :sswitch_2
        0x68ac49f -> :sswitch_19
        0x92a2831 -> :sswitch_17
        0x9af1814 -> :sswitch_0
        0xef17b9b -> :sswitch_11
        0x142bb9e6 -> :sswitch_4
        0x2ddf95f0 -> :sswitch_12
        0x30eb8398 -> :sswitch_16
        0x53bfd09d -> :sswitch_18
        0x5e88eab9 -> :sswitch_3
        0x623b3242 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultCaptureDate(Ljava/util/Date;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCaptureDate"    # Ljava/util/Date;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultCaptureDate:Ljava/util/Date;

    .line 117
    return-object p0
.end method

.method public setDefaultCommentCount(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCommentCount"    # Ljava/lang/Integer;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultCommentCount:Ljava/lang/Integer;

    .line 189
    return-object p0
.end method

.method public setDefaultContentId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultContentId"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 121
    return-object p0
.end method

.method public setDefaultContentLocators(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;",
            ">;)",
            "Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "defaultContentLocators":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultContentLocators:Lcom/google/common/collect/ImmutableList;

    .line 125
    return-object p0
.end method

.method public setDefaultContentState(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultContentState"    # Ljava/lang/String;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultContentState:Ljava/lang/String;

    .line 205
    return-object p0
.end method

.method public setDefaultCreationType(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCreationType"    # Ljava/lang/String;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultCreationType:Ljava/lang/String;

    .line 129
    return-object p0
.end method

.method public setDefaultDateUploaded(Ljava/util/Date;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDateUploaded"    # Ljava/util/Date;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultDateUploaded:Ljava/util/Date;

    .line 165
    return-object p0
.end method

.method public setDefaultEnforcementState(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultEnforcementState"    # Ljava/lang/String;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultEnforcementState:Ljava/lang/String;

    .line 209
    return-object p0
.end method

.method public setDefaultLikeCount(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultLikeCount"    # Ljava/lang/Integer;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultLikeCount:Ljava/lang/Integer;

    .line 193
    return-object p0
.end method

.method public setDefaultLocalId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultLocalId"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultLocalId:Ljava/lang/String;

    .line 133
    return-object p0
.end method

.method public setDefaultOwnerXuid(Ljava/lang/Long;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultOwnerXuid"    # Ljava/lang/Long;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/Long;

    .line 137
    return-object p0
.end method

.method public setDefaultResolutionHeight(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultResolutionHeight"    # Ljava/lang/Integer;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultResolutionHeight:Ljava/lang/Integer;

    .line 141
    return-object p0
.end method

.method public setDefaultResolutionWidth(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultResolutionWidth"    # Ljava/lang/Integer;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultResolutionWidth:Ljava/lang/Integer;

    .line 145
    return-object p0
.end method

.method public setDefaultSandboxId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSandboxId"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultSandboxId:Ljava/lang/String;

    .line 149
    return-object p0
.end method

.method public setDefaultSessions(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;",
            ">;)",
            "Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "defaultSessions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultSessions:Lcom/google/common/collect/ImmutableList;

    .line 213
    return-object p0
.end method

.method public setDefaultShareCount(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultShareCount"    # Ljava/lang/Integer;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultShareCount:Ljava/lang/Integer;

    .line 197
    return-object p0
.end method

.method public setDefaultStats(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;",
            ">;)",
            "Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 216
    .local p1, "defaultStats":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultStats:Lcom/google/common/collect/ImmutableList;

    .line 217
    return-object p0
.end method

.method public setDefaultTitleData(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleData"    # Ljava/lang/String;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTitleData:Ljava/lang/String;

    .line 153
    return-object p0
.end method

.method public setDefaultTitleId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleId"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTitleId:Ljava/lang/String;

    .line 157
    return-object p0
.end method

.method public setDefaultTitleName(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleName"    # Ljava/lang/String;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 161
    return-object p0
.end method

.method public setDefaultTournaments(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;",
            ">;)",
            "Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 220
    .local p1, "defaultTournaments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultTournaments:Lcom/google/common/collect/ImmutableList;

    .line 221
    return-object p0
.end method

.method public setDefaultUploadDeviceType(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUploadDeviceType"    # Ljava/lang/String;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadDeviceType:Ljava/lang/String;

    .line 181
    return-object p0
.end method

.method public setDefaultUploadLanguage(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUploadLanguage"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadLanguage:Ljava/lang/String;

    .line 169
    return-object p0
.end method

.method public setDefaultUploadRegion(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUploadRegion"    # Ljava/lang/String;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadRegion:Ljava/lang/String;

    .line 173
    return-object p0
.end method

.method public setDefaultUploadTitleId(Ljava/lang/Long;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUploadTitleId"    # Ljava/lang/Long;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUploadTitleId:Ljava/lang/Long;

    .line 177
    return-object p0
.end method

.method public setDefaultUserCaption(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUserCaption"    # Ljava/lang/String;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultUserCaption:Ljava/lang/String;

    .line 185
    return-object p0
.end method

.method public setDefaultViewCount(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultViewCount"    # Ljava/lang/Integer;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->defaultViewCount:Ljava/lang/Integer;

    .line 201
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    if-nez p2, :cond_0

    .line 227
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 286
    :goto_0
    return-void

    .line 230
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 231
    const-string v0, "captureDate"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->captureDateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->captureDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 233
    const-string v0, "contentId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 234
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->contentId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 235
    const-string v0, "contentLocators"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 236
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->contentLocatorsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->contentLocators()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 237
    const-string v0, "creationType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 238
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->creationTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->creationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 239
    const-string v0, "localId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->localIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->localId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 241
    const-string v0, "ownerXuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->ownerXuid()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 243
    const-string v0, "resolutionHeight"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 244
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->resolutionHeightAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->resolutionHeight()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 245
    const-string v0, "resolutionWidth"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 246
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->resolutionWidthAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->resolutionWidth()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 247
    const-string v0, "sandboxId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->sandboxIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->sandboxId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 249
    const-string v0, "titleData"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->titleDataAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->titleData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 251
    const-string v0, "titleId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->titleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 253
    const-string v0, "titleName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->titleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 255
    const-string v0, "dateUploaded"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 256
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->dateUploadedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->dateUploaded()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 257
    const-string v0, "uploadLanguage"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 258
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadLanguageAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->uploadLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 259
    const-string v0, "uploadRegion"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 260
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadRegionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->uploadRegion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 261
    const-string v0, "uploadTitleId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadTitleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->uploadTitleId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 263
    const-string v0, "uploadDeviceType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->uploadDeviceTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->uploadDeviceType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 265
    const-string v0, "userCaption"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->userCaptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->userCaption()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 267
    const-string v0, "commentCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->commentCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->commentCount()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 269
    const-string v0, "likeCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->likeCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->likeCount()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 271
    const-string v0, "shareCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 272
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->shareCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->shareCount()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 273
    const-string v0, "viewCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->viewCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->viewCount()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 275
    const-string v0, "contentState"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->contentStateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->contentState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 277
    const-string v0, "enforcementState"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->enforcementStateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->enforcementState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 279
    const-string v0, "sessions"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 280
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->sessionsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->sessions()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 281
    const-string v0, "stats"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->statsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->stats()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 283
    const-string v0, "tournaments"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->tournamentsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->tournaments()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 285
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    check-cast p2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;)V

    return-void
.end method
