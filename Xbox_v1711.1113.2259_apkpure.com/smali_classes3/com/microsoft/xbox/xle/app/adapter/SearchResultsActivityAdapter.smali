.class public Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "SearchResultsActivityAdapter.java"


# instance fields
.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;

.field private searchResultQueryTermView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private searchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;)V
    .locals 3
    .param p1, "searchResultsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    .line 40
    const v1, 0x7f0e09d9

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->screenBody:Landroid/view/View;

    .line 41
    const v1, 0x7f0e09db

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->content:Landroid/view/View;

    .line 42
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->content:Landroid/view/View;

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 44
    const v1, 0x7f0e09d5

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->setLoadMoreListener(Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;)V

    .line 91
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03016c

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;-><init>(Landroid/app/Activity;I)V

    .line 92
    .local v0, "listAdapter":Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->setShouldAlwaysShowTitle(Z)V

    .line 94
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;

    .line 95
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 97
    const v1, 0x7f0e09da

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->searchResultQueryTermView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 98
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;)Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onDestroy()V

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 131
    return-void
.end method

.method public updateViewOverride()V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->updateLoadingIndicator(Z)V

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->searchResultQueryTermView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->searchResultQueryTermView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->getSearchTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->searchResults:Ljava/util/List;

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->clear()V

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->searchResults:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->searchResults:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->addAll(Ljava/util/Collection;)V

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isLoadMoreFinished()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->onLoadMoreFinished()V

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->notifyDataSetChanged()V

    .line 124
    :cond_3
    return-void
.end method
