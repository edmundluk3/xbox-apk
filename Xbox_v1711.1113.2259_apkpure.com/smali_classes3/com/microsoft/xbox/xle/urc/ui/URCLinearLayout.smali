.class public Lcom/microsoft/xbox/xle/urc/ui/URCLinearLayout;
.super Landroid/widget/LinearLayout;
.source "URCLinearLayout.java"


# instance fields
.field private handler:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 13
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCLinearLayout;->init(Landroid/util/AttributeSet;I)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCLinearLayout;->init(Landroid/util/AttributeSet;I)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    invoke-direct {p0, p2, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCLinearLayout;->init(Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method

.method private init(Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "defStyle"    # I

    .prologue
    .line 27
    new-instance v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCLinearLayout;->handler:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

    .line 28
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCLinearLayout;->handler:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->onAttachedToWindow()V

    .line 33
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 34
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCLinearLayout;->handler:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->onDetachedFromWindow()V

    .line 41
    return-void
.end method
