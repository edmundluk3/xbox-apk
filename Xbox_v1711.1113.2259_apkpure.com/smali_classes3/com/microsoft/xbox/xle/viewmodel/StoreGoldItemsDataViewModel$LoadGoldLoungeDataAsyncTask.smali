.class Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "StoreGoldItemsDataViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadGoldLoungeDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$1;

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->shouldRefreshGoldLoungeData()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->loadGoldLoungeData(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 124
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 126
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 137
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 114
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 130
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->updateAdapter()V

    .line 132
    return-void
.end method
