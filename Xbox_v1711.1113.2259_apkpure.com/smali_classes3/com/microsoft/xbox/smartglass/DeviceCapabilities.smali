.class public final enum Lcom/microsoft/xbox/smartglass/DeviceCapabilities;
.super Ljava/lang/Enum;
.source "DeviceCapabilities.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/DeviceCapabilities;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

.field public static final enum SupportsAccelerometer:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

.field public static final enum SupportsAll:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

.field public static final enum SupportsAudio:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

.field public static final enum SupportsCompass:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

.field public static final enum SupportsGyrometer:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

.field public static final enum SupportsInclinometer:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

.field public static final enum SupportsNone:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

.field public static final enum SupportsOrientation:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

.field public static final enum SupportsStreaming:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

.field private static final _lookup:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/xbox/smartglass/DeviceCapabilities;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:J


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 13
    new-instance v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    const-string v3, "SupportsNone"

    const-wide/16 v4, 0x0

    invoke-direct {v2, v3, v1, v4, v5}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;-><init>(Ljava/lang/String;IJ)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsNone:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    .line 15
    new-instance v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    const-string v3, "SupportsStreaming"

    const-wide/16 v4, 0x1

    invoke-direct {v2, v3, v8, v4, v5}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;-><init>(Ljava/lang/String;IJ)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsStreaming:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    .line 17
    new-instance v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    const-string v3, "SupportsAudio"

    const-wide/16 v4, 0x2

    invoke-direct {v2, v3, v9, v4, v5}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;-><init>(Ljava/lang/String;IJ)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsAudio:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    .line 19
    new-instance v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    const-string v3, "SupportsAccelerometer"

    const-wide/16 v4, 0x4

    invoke-direct {v2, v3, v10, v4, v5}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;-><init>(Ljava/lang/String;IJ)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsAccelerometer:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    .line 21
    new-instance v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    const-string v3, "SupportsCompass"

    const-wide/16 v4, 0x8

    invoke-direct {v2, v3, v11, v4, v5}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;-><init>(Ljava/lang/String;IJ)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsCompass:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    .line 23
    new-instance v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    const-string v3, "SupportsGyrometer"

    const/4 v4, 0x5

    const-wide/16 v6, 0x10

    invoke-direct {v2, v3, v4, v6, v7}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;-><init>(Ljava/lang/String;IJ)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsGyrometer:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    .line 25
    new-instance v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    const-string v3, "SupportsInclinometer"

    const/4 v4, 0x6

    const-wide/16 v6, 0x20

    invoke-direct {v2, v3, v4, v6, v7}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;-><init>(Ljava/lang/String;IJ)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsInclinometer:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    .line 27
    new-instance v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    const-string v3, "SupportsOrientation"

    const/4 v4, 0x7

    const-wide/16 v6, 0x40

    invoke-direct {v2, v3, v4, v6, v7}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;-><init>(Ljava/lang/String;IJ)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsOrientation:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    .line 29
    new-instance v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    const-string v3, "SupportsAll"

    const/16 v4, 0x8

    const-wide/16 v6, -0x1

    invoke-direct {v2, v3, v4, v6, v7}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;-><init>(Ljava/lang/String;IJ)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsAll:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    .line 11
    const/16 v2, 0x9

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    sget-object v3, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsNone:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsStreaming:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsAudio:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    aput-object v3, v2, v9

    sget-object v3, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsAccelerometer:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    aput-object v3, v2, v10

    sget-object v3, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsCompass:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    aput-object v3, v2, v11

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsGyrometer:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsInclinometer:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsOrientation:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsAll:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->$VALUES:[Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    .line 32
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->_lookup:Ljava/util/HashMap;

    .line 35
    invoke-static {}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->values()[Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    move-result-object v2

    array-length v3, v2

    .local v0, "cap":Lcom/microsoft/xbox/smartglass/DeviceCapabilities;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 36
    sget-object v4, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->_lookup:Ljava/util/HashMap;

    iget-wide v6, v0, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->_value:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 38
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .param p3, "value"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput-wide p3, p0, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->_value:J

    .line 42
    return-void
.end method

.method public static fromLong(J)Ljava/util/EnumSet;
    .locals 10
    .param p0, "value"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/DeviceCapabilities;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    const-class v2, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 59
    .local v1, "set":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/DeviceCapabilities;>;"
    invoke-static {}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->values()[Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 60
    .local v0, "cap":Lcom/microsoft/xbox/smartglass/DeviceCapabilities;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->getValue()J

    move-result-wide v6

    and-long/2addr v6, p0

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 61
    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 65
    .end local v0    # "cap":Lcom/microsoft/xbox/smartglass/DeviceCapabilities;
    :cond_1
    return-object v1
.end method

.method public static getValue(Ljava/util/EnumSet;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/DeviceCapabilities;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "capabilities":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/DeviceCapabilities;>;"
    const-wide/16 v2, 0x0

    .line 76
    .local v2, "value":J
    if-eqz p0, :cond_0

    .line 77
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    .line 78
    .local v0, "cap":Lcom/microsoft/xbox/smartglass/DeviceCapabilities;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->getValue()J

    move-result-wide v4

    or-long/2addr v2, v4

    .line 79
    goto :goto_0

    .line 82
    .end local v0    # "cap":Lcom/microsoft/xbox/smartglass/DeviceCapabilities;
    :cond_0
    return-wide v2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/DeviceCapabilities;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/DeviceCapabilities;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->$VALUES:[Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    return-object v0
.end method


# virtual methods
.method public getValue()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->_value:J

    return-wide v0
.end method
