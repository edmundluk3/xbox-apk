.class Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TrendingModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TrendingModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TrendingRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TrendingModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/TrendingModel;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/TrendingModel;Lcom/microsoft/xbox/service/model/TrendingModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/TrendingModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/TrendingModel$1;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;-><init>(Lcom/microsoft/xbox/service/model/TrendingModel;)V

    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Ljava/util/ArrayList;

    const/4 v7, 0x1

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 113
    .local v0, "categoryFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;>;"
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$100(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    const/4 v3, 0x0

    .line 117
    .local v3, "response":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    :try_start_0
    sget-object v7, Lcom/microsoft/xbox/service/model/TrendingModel$1;->$SwitchMap$com$microsoft$xbox$service$activityHub$ActivityHubDataTypes$TrendingQueryType:[I

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$200(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->ordinal()I

    move-result v8

    aget v7, v7, v8
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v7, :pswitch_data_0

    .line 150
    :cond_0
    :goto_0
    if-nez v3, :cond_1

    .line 151
    invoke-static {}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$800()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Returning empty response"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_1
    return-object v3

    .line 119
    :pswitch_0
    :try_start_1
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$400(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$300(Lcom/microsoft/xbox/service/model/TrendingModel;)I

    move-result v8

    invoke-interface {v7, v8, v0}, Lcom/microsoft/xbox/service/activityHub/IActivityHubService;->getTrendingCategoryItems(ILjava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;

    move-result-object v1

    .line 120
    .local v1, "categoryItems":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;
    if-eqz v1, :cond_0

    .line 121
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$200(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$100(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    move-result-object v8

    invoke-static {v7, v1, v8}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->with(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v3

    goto :goto_0

    .line 126
    .end local v1    # "categoryItems":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;
    :pswitch_1
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$400(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$300(Lcom/microsoft/xbox/service/model/TrendingModel;)I

    move-result v8

    iget-object v9, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v9}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$500(Lcom/microsoft/xbox/service/model/TrendingModel;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lcom/microsoft/xbox/service/activityHub/IActivityHubService;->getTrendingTopicItems(ILjava/lang/String;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;

    move-result-object v5

    .line 127
    .local v5, "topicItems":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;
    if-eqz v5, :cond_0

    .line 128
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$200(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    move-result-object v7

    invoke-static {v7, v5}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->with(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v3

    goto :goto_0

    .line 133
    .end local v5    # "topicItems":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;
    :pswitch_2
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$400(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$300(Lcom/microsoft/xbox/service/model/TrendingModel;)I

    move-result v8

    iget-object v9, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v9}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$600(Lcom/microsoft/xbox/service/model/TrendingModel;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9, v0}, Lcom/microsoft/xbox/service/activityHub/IActivityHubService;->getTrendingUserItems(ILjava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;

    move-result-object v6

    .line 134
    .local v6, "userItems":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    if-eqz v6, :cond_0

    .line 135
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$200(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    move-result-object v7

    invoke-static {v7, v6}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->with(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v3

    goto :goto_0

    .line 140
    .end local v6    # "userItems":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    :pswitch_3
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$400(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$300(Lcom/microsoft/xbox/service/model/TrendingModel;)I

    move-result v8

    iget-object v9, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v9}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$700(Lcom/microsoft/xbox/service/model/TrendingModel;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9, v0}, Lcom/microsoft/xbox/service/activityHub/IActivityHubService;->getTrendingTitleItems(ILjava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;

    move-result-object v4

    .line 141
    .local v4, "titleItems":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    if-eqz v4, :cond_0

    .line 142
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$200(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    move-result-object v7

    invoke-static {v7, v4}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->with(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto/16 :goto_0

    .line 146
    .end local v4    # "titleItems":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    :catch_0
    move-exception v2

    .line 147
    .local v2, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/TrendingModel;->access$800()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Error getting trending data"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 117
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->buildData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 164
    const-wide/16 v0, 0xd

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 159
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;->this$0:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/TrendingModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 160
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 108
    return-void
.end method
