.class public Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ConversationsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
        "Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final BLOCK_USER:Ljava/lang/String;

.field private static final FAVORITE_OFFLINE_COLOR:I

.field private static final FAVORITE_ONLINE_COLOR:I

.field private static final MAX_COLLAPSED_LINE_COUNT:I = 0x2

.field private static final MAX_FRIENDS_COUNT_SHOWN:I = 0x4

.field private static final READ_COLOR:I

.field private static final UNBLOCK_USER:Ljava/lang/String;

.field private static final UNREAD_COLOR:I


# instance fields
.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070ac6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->BLOCK_USER:Ljava/lang/String;

    .line 46
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070ad5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->UNBLOCK_USER:Ljava/lang/String;

    .line 48
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c0139

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->READ_COLOR:I

    .line 50
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c0142

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->UNREAD_COLOR:I

    .line 52
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->FAVORITE_ONLINE_COLOR:I

    .line 54
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->FAVORITE_OFFLINE_COLOR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 60
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    .line 61
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 62
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->READ_COLOR:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->UNREAD_COLOR:I

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->getMessageText(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->FAVORITE_ONLINE_COLOR:I

    return v0
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->FAVORITE_OFFLINE_COLOR:I

    return v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    return-object v0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->UNBLOCK_USER:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->BLOCK_USER:Ljava/lang/String;

    return-object v0
.end method

.method private getMessageText(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/String;
    .locals 14
    .param p1, "conversation"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 293
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 295
    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v8, :cond_f

    .line 296
    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v8, v8, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v7

    .line 297
    .local v7, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    sget-object v8, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$serialization$SkypeMessageType:[I

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 320
    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v8, v8, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 321
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v8

    invoke-virtual {v8, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->getMostRecentMessageWithText(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    move-result-object v3

    .line 322
    .local v3, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    sget-object v8, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Activity:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 323
    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v8, v8, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->senderXuid:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 325
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f07065b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 351
    .end local v3    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .end local v7    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :goto_0
    return-object v8

    .line 299
    .restart local v7    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :pswitch_0
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-object v10, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v10, v10, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v11, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberInitiator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupSenderGamertag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 300
    .local v2, "initiator":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-object v10, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v10, v10, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v11, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    invoke-virtual {v8, v9, v10, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupSenderGamertag(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 301
    .local v5, "target":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f070639

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v2, v10, v11

    const/4 v11, 0x1

    aput-object v5, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    :cond_0
    const-string v8, ""

    goto :goto_0

    .line 303
    .end local v2    # "initiator":Ljava/lang/String;
    .end local v5    # "target":Ljava/lang/String;
    :pswitch_1
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-object v10, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v10, v10, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v11, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getDeleteMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupSenderGamertag(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 304
    .local v1, "gamertag":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f07065a

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v1, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    :cond_1
    const-string v8, ""

    goto :goto_0

    .line 306
    .end local v1    # "gamertag":Ljava/lang/String;
    :pswitch_2
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-object v10, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v10, v10, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v11, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicChangedById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupSenderGamertag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 307
    .local v6, "topicChangeInitiator":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f07063d

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v6, v10, v11

    const/4 v11, 0x1

    iget-object v12, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v12, v12, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v13, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v13, v13, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v12, v13}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    :cond_2
    const-string v8, ""

    goto/16 :goto_0

    .line 310
    .end local v6    # "topicChangeInitiator":Ljava/lang/String;
    :pswitch_3
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f07075d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 311
    .local v4, "messageText":Ljava/lang/String;
    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v8, v8, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 312
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v9, v9, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_3
    move-object v8, v4

    .line 314
    goto/16 :goto_0

    .line 316
    .end local v4    # "messageText":Ljava/lang/String;
    :pswitch_4
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f07075d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 328
    .restart local v3    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_4
    iget-boolean v8, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-eqz v8, :cond_5

    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v9, v9, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->senderXuid:Ljava/lang/String;

    invoke-static {v9}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupSenderGamertagText(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 329
    .local v0, "gamerTag":Ljava/lang/String;
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f070b0d

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 328
    .end local v0    # "gamerTag":Ljava/lang/String;
    :cond_5
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    goto :goto_1

    .line 329
    .restart local v0    # "gamerTag":Ljava/lang/String;
    :cond_6
    const-string v8, ""

    goto/16 :goto_0

    .line 331
    .end local v0    # "gamerTag":Ljava/lang/String;
    :cond_7
    sget-object v8, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->AddMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 332
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-object v10, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v10, v10, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v11, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberInitiator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupSenderGamertag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 333
    .restart local v2    # "initiator":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-object v10, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v10, v10, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v11, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupSenderGamertag(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 334
    .restart local v5    # "target":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_8

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_8

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f070639

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v2, v10, v11

    const/4 v11, 0x1

    aput-object v5, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    :cond_8
    const-string v8, ""

    goto/16 :goto_0

    .line 335
    .end local v2    # "initiator":Ljava/lang/String;
    .end local v5    # "target":Ljava/lang/String;
    :cond_9
    sget-object v8, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->RemoveMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 336
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-object v10, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v10, v10, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v11, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getDeleteMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupSenderGamertag(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 337
    .restart local v1    # "gamertag":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_a

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f07065a

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v1, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    :cond_a
    const-string v8, ""

    goto/16 :goto_0

    .line 338
    .end local v1    # "gamertag":Ljava/lang/String;
    :cond_b
    sget-object v8, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->TopicUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 339
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-object v10, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v10, v10, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v11, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicChangedById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupSenderGamertag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 340
    .restart local v6    # "topicChangeInitiator":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_c

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f07063d

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v6, v10, v11

    const/4 v11, 0x1

    iget-object v12, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v12, v12, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v13, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v13, v13, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v12, v13}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    :cond_c
    const-string v8, ""

    goto/16 :goto_0

    .line 342
    .end local v6    # "topicChangeInitiator":Ljava/lang/String;
    :cond_d
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getContent()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 346
    .end local v3    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_e
    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v8, v8, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    goto/16 :goto_0

    .line 351
    .end local v7    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :cond_f
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 297
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 41
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 71
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->bindTo(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 72
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 66
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f0300c4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;Landroid/view/View;)V

    return-object v0
.end method
