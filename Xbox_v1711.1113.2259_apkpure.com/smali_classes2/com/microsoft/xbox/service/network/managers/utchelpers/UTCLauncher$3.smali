.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$3;
.super Ljava/lang/Object;
.source "UTCLauncher.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackPinToggleAction(JZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$pinToHome:Z

.field final synthetic val$titleId:J


# direct methods
.method constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 40
    iput-wide p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$3;->val$titleId:J

    iput-boolean p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$3;->val$pinToHome:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 4

    .prologue
    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 44
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TitleId"

    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$3;->val$titleId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 46
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$3;->val$pinToHome:Z

    if-eqz v1, :cond_0

    .line 47
    const-string v1, "Launcher - Pin to Home"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    const-string v1, "Launcher - Unpin"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0
.end method
