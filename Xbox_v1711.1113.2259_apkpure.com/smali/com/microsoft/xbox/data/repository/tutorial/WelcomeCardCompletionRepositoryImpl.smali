.class public Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;
.super Ljava/lang/Object;
.source "WelcomeCardCompletionRepositoryImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final COMPLETION_STATE_SHARED_PREF_KEY:Ljava/lang/String; = "welcome_card_completion_states"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

.field private currentStatesObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;",
            ">;"
        }
    .end annotation
.end field

.field private final dataMapper:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;

.field private final rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V
    .locals 3
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "dataMapper"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;
    .param p3, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 32
    invoke-static {p1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->create(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    .line 33
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "welcome_card_completion_states"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    const-string/jumbo v1, "welcome_card_completion_states"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    .line 37
    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p2}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStatesObservable:Lio/reactivex/Observable;

    .line 40
    invoke-interface {p3}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->getAuthStates()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 43
    return-void
.end method

.method private applyCurrentStatesToSharedPreferences()V
    .locals 4

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "welcome_card_completion_states"

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 97
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;->toString(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 98
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 99
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/domain/auth/AuthState;)Z
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;Lcom/microsoft/xbox/domain/auth/AuthState;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->resetStates()V

    return-void
.end method


# virtual methods
.method public getStates()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStatesObservable:Lio/reactivex/Observable;

    return-object v0
.end method

.method public resetStates()V
    .locals 1

    .prologue
    .line 52
    invoke-static {}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->defaultStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 53
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->applyCurrentStatesToSharedPreferences()V

    .line 54
    return-void
.end method

.method public setClubWelcomeCardCompleted(Z)V
    .locals 1
    .param p1, "clubWelcomeCardCompleted"    # Z

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->withClubWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 89
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->applyCurrentStatesToSharedPreferences()V

    .line 90
    return-void
.end method

.method public setFacebookWelcomeCardCompleted(Z)V
    .locals 1
    .param p1, "friendWelcomeCardCompleted"    # Z

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->withFacebookWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 77
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->applyCurrentStatesToSharedPreferences()V

    .line 78
    return-void
.end method

.method public setRedeemWelcomeCardCompleted(Z)V
    .locals 1
    .param p1, "redeemCardCompleted"    # Z

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->withRedeemWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 65
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->applyCurrentStatesToSharedPreferences()V

    .line 66
    return-void
.end method

.method public setSetupWelcomeCardCompleted(Z)V
    .locals 1
    .param p1, "setupCardCompleted"    # Z

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->withSetupWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 59
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->applyCurrentStatesToSharedPreferences()V

    .line 60
    return-void
.end method

.method public setShopWelcomeCardCompleted(Z)V
    .locals 1
    .param p1, "shopWelcomeCardCompleted"    # Z

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->withShopWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 71
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->applyCurrentStatesToSharedPreferences()V

    .line 72
    return-void
.end method

.method public setSuggestedFriendsWelcomeCardCompleted(Z)V
    .locals 1
    .param p1, "suggestedFriendsWelcomeCardCompleted"    # Z

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->withSuggestedFriendsWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->currentStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 83
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;->applyCurrentStatesToSharedPreferences()V

    .line 84
    return-void
.end method
