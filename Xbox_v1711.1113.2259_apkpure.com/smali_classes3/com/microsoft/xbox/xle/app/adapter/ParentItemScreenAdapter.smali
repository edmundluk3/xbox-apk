.class public Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "ParentItemScreenAdapter.java"


# instance fields
.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 27
    const v0, 0x7f0e0833

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->screenBody:Landroid/view/View;

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;

    .line 30
    const v0, 0x7f0e0835

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 31
    const v0, 0x7f0e0834

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03019e

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->updateLoadingIndicator(Z)V

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->items:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;->getData()Ljava/util/List;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;->getData()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->items:Ljava/util/List;

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;->clear()V

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->items:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;->addAll(Ljava/util/Collection;)V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 56
    return-void
.end method
