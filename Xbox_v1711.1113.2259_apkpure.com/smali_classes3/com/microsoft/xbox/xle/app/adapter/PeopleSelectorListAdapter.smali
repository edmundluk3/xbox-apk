.class public Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PeopleSelectorListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p3, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 28
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->notifyDataSetChanged()V

    .line 29
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 34
    move-object v7, p2

    .line 36
    .local v7, "v":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_4

    .line 37
    if-eqz v7, :cond_0

    instance-of v9, v7, Landroid/widget/LinearLayout;

    if-nez v9, :cond_1

    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "layout_inflater"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/LayoutInflater;

    .line 39
    .local v8, "vi":Landroid/view/LayoutInflater;
    const v9, 0x7f03010a

    const/4 v10, 0x0

    invoke-virtual {v8, v9, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 42
    .end local v8    # "vi":Landroid/view/LayoutInflater;
    :cond_1
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .line 43
    .local v3, "person":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    if-eqz v3, :cond_4

    .line 44
    invoke-virtual {v7, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 45
    const v9, 0x7f0e05d5

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 46
    .local v0, "gamertagView":Landroid/widget/TextView;
    const v9, 0x7f0e05d6

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 48
    .local v6, "realnameView":Landroid/widget/TextView;
    const v9, 0x7f0e05d3

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 49
    .local v2, "listTileView":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    const v9, 0x7f0e0231

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 50
    .local v4, "presenceIcon":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    const v9, 0x7f0e05d7

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 52
    .local v1, "iconSelect":Landroid/widget/TextView;
    if-eqz v0, :cond_2

    .line 53
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getGamerTag()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    :cond_2
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getGamerRealName()Ljava/lang/String;

    move-result-object v5

    .line 57
    .local v5, "realName":Ljava/lang/String;
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    const/16 v9, 0x8

    :goto_0
    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 61
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getIsOnline()Z

    move-result v9

    if-eqz v9, :cond_6

    const/4 v9, 0x0

    :goto_1
    invoke-static {v4, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 63
    if-eqz v2, :cond_3

    .line 65
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v9

    const v10, 0x7f020125

    const v11, 0x7f020125

    .line 64
    invoke-virtual {v2, v9, v10, v11}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 70
    :cond_3
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getIsSelected()Z

    move-result v9

    if-eqz v9, :cond_7

    const/4 v9, 0x0

    :goto_2
    invoke-static {v1, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 72
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getIsSelected()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/view/View;->setBackgroundColor(I)V

    .line 80
    .end local v0    # "gamertagView":Landroid/widget/TextView;
    .end local v1    # "iconSelect":Landroid/widget/TextView;
    .end local v2    # "listTileView":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .end local v3    # "person":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    .end local v4    # "presenceIcon":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .end local v5    # "realName":Ljava/lang/String;
    .end local v6    # "realnameView":Landroid/widget/TextView;
    :cond_4
    :goto_3
    return-object v7

    .line 58
    .restart local v0    # "gamertagView":Landroid/widget/TextView;
    .restart local v1    # "iconSelect":Landroid/widget/TextView;
    .restart local v2    # "listTileView":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .restart local v3    # "person":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    .restart local v4    # "presenceIcon":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .restart local v5    # "realName":Ljava/lang/String;
    .restart local v6    # "realnameView":Landroid/widget/TextView;
    :cond_5
    const/4 v9, 0x0

    goto :goto_0

    .line 61
    :cond_6
    const/16 v9, 0x8

    goto :goto_1

    .line 70
    :cond_7
    const/16 v9, 0x8

    goto :goto_2

    .line 75
    :cond_8
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c0145

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_3
.end method
