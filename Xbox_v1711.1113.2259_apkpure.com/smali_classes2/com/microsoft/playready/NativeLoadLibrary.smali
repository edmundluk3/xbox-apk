.class Lcom/microsoft/playready/NativeLoadLibrary;
.super Ljava/lang/Object;
.source "NativeLoadLibrary.java"


# static fields
.field public static DrmInitLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/playready/NativeLoadLibrary;->DrmInitLock:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static loadLibrary()V
    .locals 2

    .prologue
    .line 23
    const-string v0, "LibMediaPlayer"

    const-string v1, "Attempting to load native libmediaplayer.so"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    const-string v0, "mediaplayer"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 27
    const-string v0, "LibMediaPlayer"

    const-string v1, "libmediaplayer.so is successfully loaded."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    return-void
.end method
