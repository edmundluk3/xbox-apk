.class Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$3;
.super Ljava/lang/Object;
.source "EPGInlineDetailsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setProgram(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

.field final synthetic val$channel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

.field final synthetic val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$3;->this$0:Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$3;->val$channel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$3;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 222
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$3;->val$channel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getHeadendId()Ljava/lang/String;

    move-result-object v1

    .line 223
    .local v1, "providerId":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$3;->val$channel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getChannelGuid()Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "channelId":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$3;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    if-nez v3, :cond_1

    const/4 v2, 0x0

    .line 225
    .local v2, "showId":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getStreamerState()Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v3, v4, :cond_0

    .line 226
    invoke-static {v1, v0, v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->trackStreamLaunchAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    :cond_0
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->streamChannel(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    return-void

    .line 224
    .end local v2    # "showId":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$3;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
