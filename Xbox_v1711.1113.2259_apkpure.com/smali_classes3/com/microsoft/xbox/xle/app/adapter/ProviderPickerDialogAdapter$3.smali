.class Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$3;
.super Ljava/lang/Object;
.source "ProviderPickerDialogAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    .line 162
    .local v0, "airing":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
    if-eqz v0, :cond_0

    .line 163
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    const-string v5, "ShowTuned"

    iget-object v6, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->HeadendId:Ljava/lang/String;

    iget-object v7, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->ChannelId:Ljava/lang/String;

    iget-object v8, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->ID:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v4, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->HeadendId:Ljava/lang/String;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    .line 165
    .local v1, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-nez v1, :cond_1

    .line 175
    .end local v1    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_0
    :goto_0
    return-void

    .line 169
    .restart local v1    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_1
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getTitleId()Ljava/lang/String;

    move-result-object v2

    .line 170
    .local v2, "titleId":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getTitleLocation()Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v3

    .line 172
    .local v3, "titleLocation":Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    const-string v4, "ms-xbl-%s://media-playback?contentType=tvChannel&contentId=%s&lineupInstanceId=%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    iget-object v7, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->ChannelId:Ljava/lang/String;

    invoke-static {v7}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-object v7, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->HeadendId:Ljava/lang/String;

    invoke-static {v7}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v3, v5}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    .line 173
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->onCloseTouched()V

    goto :goto_0
.end method
