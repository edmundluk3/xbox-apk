.class public final Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;
.super Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.source "ClubPivotScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>()V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "Clubs"

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 26
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onCreate()V

    .line 28
    const v2, 0x7f030092

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;->setContentView(I)V

    .line 32
    const v2, 0x7f0e03b5

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .local v1, "pivotWithTabs":Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    .line 34
    if-eqz v1, :cond_0

    .line 35
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHubPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->TAB_NAMES:[Ljava/lang/String;

    sget-object v4, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->ACTION_NAMES:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTelemetryTabSelectCallback(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 36
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHubPageResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->TAB_NAMES:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTelemetrySetActiveTabCallback(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;)V

    .line 39
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    .line 40
    .local v0, "parameters":Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;
    iget-wide v2, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;->clubId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->setClubId(J)V

    .line 42
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0c0149

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderBackgroundColor(I)V

    .line 43
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setSelectedTabIndicatorColorToProfileColor()V

    .line 45
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 47
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 48
    return-void
.end method
