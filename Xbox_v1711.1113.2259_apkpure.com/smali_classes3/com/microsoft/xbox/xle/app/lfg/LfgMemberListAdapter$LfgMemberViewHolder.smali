.class public final Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "LfgMemberListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "LfgMemberViewHolder"
.end annotation


# instance fields
.field private final gameStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private final gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private popup:Landroid/widget/PopupMenu;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;Landroid/view/View;)V
    .locals 3
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    .line 62
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 64
    const v0, 0x7f0e076c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 65
    const v0, 0x7f0e076d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 66
    const v0, 0x7f0e076e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 67
    const v0, 0x7f0e076f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->gameStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 69
    new-instance v0, Landroid/widget/PopupMenu;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->itemView:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->popup:Landroid/widget/PopupMenu;

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->popup:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0008

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->popup:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    return-void
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;Landroid/view/MenuItem;)Z
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "member"    # Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 96
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e0c2e

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 104
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 98
    :cond_0
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e0c2f

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->access$300(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->access$300(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    goto :goto_0

    .line 101
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown menu item "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;)V
    .locals 6
    .param p1, "member"    # Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v5, 0x7f020125

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 75
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 77
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->getMemberSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v0

    .line 79
    .local v0, "memberSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->isHeaderRow()Z

    move-result v4

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 80
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->isHeaderRow()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 81
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->isHeaderRow()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    invoke-static {v4, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 82
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->gameStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->isHeaderRow()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->timePlayed:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    move v1, v2

    :goto_2
    invoke-static {v4, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 84
    if-eqz v0, :cond_4

    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    invoke-virtual {v1, v4, v5, v5}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->gamertag:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 88
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->timePlayed:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->gameStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0706d0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->timePlayed:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 95
    :cond_0
    :goto_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->popup:Landroid/widget/PopupMenu;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 106
    return-void

    :cond_1
    move v1, v3

    .line 80
    goto :goto_0

    :cond_2
    move v1, v3

    .line 81
    goto :goto_1

    :cond_3
    move v1, v3

    .line 82
    goto :goto_2

    .line 92
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->getHeader()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->popup:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    goto :goto_0
.end method
