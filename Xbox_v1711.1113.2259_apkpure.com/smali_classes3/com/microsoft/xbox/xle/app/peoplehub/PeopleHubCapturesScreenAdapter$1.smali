.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter$1;
.super Ljava/lang/Object;
.source "PeopleHubCapturesScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-ltz p3, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_1

    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    .line 108
    .local v0, "filter":Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackCapturesFilterAction(Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;)V

    .line 109
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 110
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackCapturesFilterAction(Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;)V

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->setFilter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;)V

    .line 116
    .end local v0    # "filter":Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    const-string v1, "PeopleHubCapturesScreenAdapter"

    const-string v2, "Out of scope selection."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
