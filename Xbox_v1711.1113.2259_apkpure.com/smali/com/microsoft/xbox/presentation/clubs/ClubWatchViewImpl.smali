.class public Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;
.super Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;
.source "ClubWatchViewImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/clubs/ClubWatchView;
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# instance fields
.field contentList:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e03d0
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

.field private okCancelDialogIntents:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field swipeRefreshLayoutContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e03cf
    .end annotation
.end field

.field swipeRefreshLayoutNoContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e03d1
    .end annotation
.end field

.field private viewIntents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method static synthetic lambda$onCreateContentView$0(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 78
    sget-object v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$1(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    return-void
.end method

.method static synthetic lambda$onCreateContentView$2(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 87
    sget-object v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$3(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    sget-object v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;

    return-object v0
.end method

.method static synthetic lambda$showConfirmationDialog$4(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->okCancelDialogIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p1, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$showConfirmationDialog$5(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    .prologue
    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->okCancelDialogIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p1, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private renderContent(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "content":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    const/4 v1, 0x0

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->swipeRefreshLayoutContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->swipeRefreshLayoutContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->swipeRefreshLayoutNoContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->swipeRefreshLayoutNoContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 160
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 166
    :goto_0
    return-void

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 164
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->setListContent(Ljava/util/List;)V

    goto :goto_0
.end method

.method private renderError()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 174
    return-void
.end method

.method private renderLoading()V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 170
    return-void
.end method

.method private setListContent(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 177
    .local p1, "content":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v1

    .line 179
    .local v1, "oldContent":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->clear()V

    .line 180
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->addAll(Ljava/util/Collection;)V

    .line 182
    new-instance v2, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;

    invoke-direct {v2, v1, p1}, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v2}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v0

    .line 183
    .local v0, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v0, v2}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 184
    return-void
.end method

.method private showConfirmationDialog(Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V
    .locals 7
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    .prologue
    .line 187
    iget-boolean v0, p1, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->deeplinkToAppEnabled:Z

    if-eqz v0, :cond_0

    const v6, 0x7f070119

    .line 192
    .local v6, "dialogTitle":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 193
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070118

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 194
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070738

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)Ljava/lang/Runnable;

    move-result-object v3

    .line 196
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070736

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)Ljava/lang/Runnable;

    move-result-object v5

    .line 191
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 198
    return-void

    .line 187
    .end local v6    # "dialogTitle":I
    :cond_0
    const v6, 0x7f0705d5

    goto :goto_0
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    const-class v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getContentScreenId()I
    .locals 1

    .prologue
    .line 110
    const v0, 0x7f03009b

    return v0
.end method

.method protected getErrorScreenId()I
    .locals 1

    .prologue
    .line 120
    const v0, 0x7f03009c

    return v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0702e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->presenter:Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;

    return-object v0
.end method

.method protected getNoContentScreenId()I
    .locals 1

    .prologue
    .line 115
    const v0, 0x7f03009d

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreate()V

    .line 67
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;)V

    .line 68
    return-void
.end method

.method public onCreateContentView()V
    .locals 8

    .prologue
    .line 72
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreateContentView()V

    .line 73
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v1

    .line 75
    .local v1, "itemClickedRelayed":Lcom/jakewharton/rxrelay2/PublishRelay;, "Lcom/jakewharton/rxrelay2/PublishRelay<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    new-instance v5, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl$$Lambda$1;->lambdaFactory$(Lcom/jakewharton/rxrelay2/PublishRelay;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v5, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    .line 76
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 78
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->refresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/jakewharton/rxrelay2/PublishRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v6

    .line 79
    invoke-virtual {v5, v6}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 81
    .local v2, "refreshIntent":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/jakewharton/rxrelay2/PublishRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 83
    .local v0, "itemClicked":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->okCancelDialogIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 86
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->swipeRefreshLayoutContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-static {v5}, Lcom/jakewharton/rxbinding2/support/v4/widget/RxSwipeRefreshLayout;->refreshes(Landroid/support/v4/widget/SwipeRefreshLayout;)Lio/reactivex/Observable;

    move-result-object v5

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v6

    .line 87
    invoke-virtual {v5, v6}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v3

    .line 90
    .local v3, "swipeRefreshesContent":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->swipeRefreshLayoutNoContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-static {v5}, Lcom/jakewharton/rxbinding2/support/v4/widget/RxSwipeRefreshLayout;->refreshes(Landroid/support/v4/widget/SwipeRefreshLayout;)Lio/reactivex/Observable;

    move-result-object v5

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v6

    .line 91
    invoke-virtual {v5, v6}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v4

    .line 93
    .local v4, "swipeRefreshesNoContent":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;>;"
    const/4 v5, 0x5

    new-array v5, v5, [Lio/reactivex/ObservableSource;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    const/4 v6, 0x2

    aput-object v4, v5, v6

    const/4 v6, 0x3

    aput-object v0, v5, v6

    const/4 v6, 0x4

    iget-object v7, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->okCancelDialogIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    aput-object v7, v5, v6

    invoke-static {v5}, Lio/reactivex/Observable;->mergeArray([Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->viewIntents:Lio/reactivex/Observable;

    .line 96
    return-void
.end method

.method public render(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;)V
    .locals 3
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    .prologue
    .line 131
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isContent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 132
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->content()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->renderContent(Ljava/util/List;)V

    .line 141
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->dialogViewState()Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    move-result-object v0

    .line 143
    .local v0, "dialogViewState":Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
    if-eqz v0, :cond_0

    .line 144
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->showConfirmationDialog(Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V

    .line 146
    :cond_0
    return-void

    .line 133
    .end local v0    # "dialogViewState":Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoading()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 134
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->renderLoading()V

    goto :goto_0

    .line 135
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isError()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 136
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->renderError()V

    goto :goto_0

    .line 138
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown view state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic render(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->render(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;)V

    return-void
.end method

.method public viewIntents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->viewIntents:Lio/reactivex/Observable;

    return-object v0
.end method
