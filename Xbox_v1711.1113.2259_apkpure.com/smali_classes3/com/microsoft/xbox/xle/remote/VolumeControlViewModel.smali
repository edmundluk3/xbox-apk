.class public Lcom/microsoft/xbox/xle/remote/VolumeControlViewModel;
.super Ljava/lang/Object;
.source "VolumeControlViewModel.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 0

    .prologue
    .line 16
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 13
    return-void
.end method

.method public volumeDown()V
    .locals 3

    .prologue
    .line 26
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 27
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 28
    const/4 v1, 0x0

    const-string v2, "btn.vol_down"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->sendButton(Ljava/lang/String;Ljava/lang/String;)Z

    .line 30
    :cond_0
    return-void
.end method

.method public volumeMuteToggle()V
    .locals 3

    .prologue
    .line 33
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 34
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 35
    const/4 v1, 0x0

    const-string v2, "btn.vol_mute"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->sendButton(Ljava/lang/String;Ljava/lang/String;)Z

    .line 37
    :cond_0
    return-void
.end method

.method public volumeUp()V
    .locals 3

    .prologue
    .line 19
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 20
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 21
    const/4 v1, 0x0

    const-string v2, "btn.vol_up"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->sendButton(Ljava/lang/String;Ljava/lang/String;)Z

    .line 23
    :cond_0
    return-void
.end method
