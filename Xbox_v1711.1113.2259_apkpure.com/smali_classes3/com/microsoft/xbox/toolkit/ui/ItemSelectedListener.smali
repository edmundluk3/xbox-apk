.class public final Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;
.super Ljava/lang/Object;
.source "ItemSelectedListener.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;
    }
.end annotation


# instance fields
.field private final callback:Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;->callback:Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    .line 34
    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;->callback:Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 39
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 40
    .local v6, "item":Landroid/widget/TextView;
    if-eqz v6, :cond_0

    .line 41
    sget-object v0, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 43
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
