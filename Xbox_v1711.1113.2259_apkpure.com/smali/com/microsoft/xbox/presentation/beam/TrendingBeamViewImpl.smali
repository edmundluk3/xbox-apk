.class public Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;
.super Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;
.source "TrendingBeamViewImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/beam/TrendingBeamView;
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# instance fields
.field contentList:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0aea
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

.field private okCancelDialogIntents:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field titleBarView:Lcom/microsoft/xbox/xle/ui/TitleBarView;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private viewIntents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method static synthetic lambda$onCreateContentView$0(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$LoadMoreIntent;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    sget-object v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$LoadMoreIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$LoadMoreIntent;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$1(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$LoadMoreIntent;)Z
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$LoadMoreIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->titleBarView:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    .line 78
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/TitleBarView;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreateContentView$2(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 80
    sget-object v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$3(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    return-void
.end method

.method static synthetic lambda$showConfirmationDialog$4(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->okCancelDialogIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p1, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$showConfirmationDialog$5(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->okCancelDialogIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p1, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private renderContent(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "content":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->titleBarView:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/TitleBarView;->updateIsLoading(Z)V

    .line 129
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->setListContent(Ljava/util/List;)V

    .line 130
    return-void
.end method

.method private renderError()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->titleBarView:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/TitleBarView;->updateIsLoading(Z)V

    .line 145
    return-void
.end method

.method private renderLoading()V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 134
    return-void
.end method

.method private renderLoadingMore(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "content":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->titleBarView:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/TitleBarView;->updateIsLoading(Z)V

    .line 139
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->setListContent(Ljava/util/List;)V

    .line 140
    return-void
.end method

.method private setListContent(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "content":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v1

    .line 150
    .local v1, "oldContent":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->clear()V

    .line 151
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->addAll(Ljava/util/Collection;)V

    .line 153
    new-instance v2, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;

    invoke-direct {v2, v1, p1}, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v2}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v0

    .line 154
    .local v0, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v0, v2}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 155
    return-void
.end method

.method private showConfirmationDialog(Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V
    .locals 7
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    .prologue
    .line 158
    iget-boolean v0, p1, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->deeplinkToAppEnabled:Z

    if-eqz v0, :cond_0

    const v6, 0x7f070119

    .line 163
    .local v6, "dialogTitle":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070118

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 165
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070738

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)Ljava/lang/Runnable;

    move-result-object v3

    .line 167
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070736

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)Ljava/lang/Runnable;

    move-result-object v5

    .line 162
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 169
    return-void

    .line 158
    .end local v6    # "dialogTitle":I
    :cond_0
    const v6, 0x7f0705d5

    goto :goto_0
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    const-class v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getContentScreenId()I
    .locals 1

    .prologue
    .line 102
    const v0, 0x7f030237

    return v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070cbd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->presenter:Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreate()V

    .line 62
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;)V

    .line 63
    return-void
.end method

.method public onCreateContentView()V
    .locals 6

    .prologue
    .line 67
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreateContentView()V

    .line 69
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v1

    .line 71
    .local v1, "itemClickedRelayed":Lcom/jakewharton/rxrelay2/PublishRelay;, "Lcom/jakewharton/rxrelay2/PublishRelay<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    new-instance v4, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl$$Lambda$1;->lambdaFactory$(Lcom/jakewharton/rxrelay2/PublishRelay;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v4, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    .line 72
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 74
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    .line 75
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/rx/RxViewUtils;->loadMoreOnScroll(Landroid/support/v7/widget/RecyclerView;)Lio/reactivex/Observable;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v5

    .line 76
    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v4

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;)Lio/reactivex/functions/Predicate;

    move-result-object v5

    .line 77
    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    .line 80
    .local v2, "loadMoreIntents":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$LoadMoreIntent;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->refresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/jakewharton/rxrelay2/PublishRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v4

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v5

    .line 81
    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v3

    .line 83
    .local v3, "refreshIntent":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/jakewharton/rxrelay2/PublishRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 85
    .local v0, "itemClicked":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->okCancelDialogIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 87
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->okCancelDialogIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {v2, v3, v0, v4}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->viewIntents:Lio/reactivex/Observable;

    .line 88
    return-void
.end method

.method public render(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;)V
    .locals 3
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    .prologue
    .line 107
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isContent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->content()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->renderContent(Ljava/util/List;)V

    .line 119
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->dialogViewState()Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    move-result-object v0

    .line 121
    .local v0, "dialogViewState":Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
    if-eqz v0, :cond_0

    .line 122
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->showConfirmationDialog(Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V

    .line 124
    :cond_0
    return-void

    .line 109
    .end local v0    # "dialogViewState":Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoading()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 110
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->renderLoading()V

    goto :goto_0

    .line 111
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoadingMore()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 112
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->content()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->renderLoadingMore(Ljava/util/List;)V

    goto :goto_0

    .line 113
    :cond_3
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isError()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 114
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->renderError()V

    goto :goto_0

    .line 116
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown view state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic render(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->render(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;)V

    return-void
.end method

.method public viewIntents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->viewIntents:Lio/reactivex/Observable;

    return-object v0
.end method
