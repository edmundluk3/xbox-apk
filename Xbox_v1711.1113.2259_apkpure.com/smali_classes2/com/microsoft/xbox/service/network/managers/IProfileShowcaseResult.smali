.class public interface abstract Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult;
.super Ljava/lang/Object;
.source "IProfileShowcaseResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;,
        Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$PagingInfo;,
        Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;,
        Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;,
        Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;,
        Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;,
        Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;,
        Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;
    }
.end annotation
