.class Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;
.super Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;
.source "MRUScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FullAppHolder"
.end annotation


# instance fields
.field protected final achievements:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final heroStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field protected final mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final trophyIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 3
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "parentWidth"    # I
    .param p3, "nowPlayingViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p4, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    const v2, 0x7f0b0020

    .line 279
    invoke-direct {p0, p1, p3, p4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;-><init>(Landroid/view/View;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    .line 280
    const v0, 0x7f0e00df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 281
    const v0, 0x7f0e07cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 282
    const v0, 0x7f0e07cc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->heroStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 283
    const v0, 0x7f0e07ce

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->trophyIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 284
    const v0, 0x7f0e07cf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->achievements:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 285
    const v0, 0x7f0e07d0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 286
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, p2, v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->setRowHeight(II)V

    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {p0, v0, p2, v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->setImageWidth(Landroid/widget/ImageView;II)V

    .line 288
    return-void
.end method


# virtual methods
.method public bind(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V
    .locals 5
    .param p1, "npi"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .prologue
    .line 292
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->bindNPI(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 294
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v1

    .line 296
    .local v1, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getImageUri(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v2

    .line 297
    .local v2, "uri":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getBATDefaultImageRid(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)I

    move-result v0

    .line 298
    .local v0, "defaultRid":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v4, 0x7f0e001d

    invoke-virtual {v3, v4, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 299
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v3, v2, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 300
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-nez v2, :cond_0

    const/4 v3, 0x4

    :goto_0
    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 301
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->updateImageBackground(Landroid/widget/ImageView;)V

    .line 303
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeader(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 304
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->heroStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeroStat()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 305
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->achievements:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getAchievementsEarned()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 306
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 307
    return-void

    .line 300
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 311
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->tappedAtBat()V

    .line 312
    return-void
.end method
