.class public abstract Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;
.super Ljava/lang/Object;
.source "EditorialDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CustomSocialTag"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    new-instance v0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_CustomSocialTag$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_CustomSocialTag$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;
    .locals 1
    .param p0, "tagText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 130
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 131
    new-instance v0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_CustomSocialTag;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_CustomSocialTag;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getDisplayText()Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 116
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070705

    .line 118
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "#"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 120
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;->sourceId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 116
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;->sourceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSymbol()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 126
    const-string v0, ""

    return-object v0
.end method

.method public abstract sourceId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
