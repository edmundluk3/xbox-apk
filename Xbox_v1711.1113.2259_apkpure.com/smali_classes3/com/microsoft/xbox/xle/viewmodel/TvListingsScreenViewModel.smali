.class public Lcom/microsoft/xbox/xle/viewmodel/TvListingsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;
.source "TvListingsScreenViewModel.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    sget v0, Lcom/microsoft/xbox/xle/viewmodel/TvListingsScreenViewModel;->TV_CHANNELS:I

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;-><init>(I)V

    .line 11
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvListingsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvListingsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 12
    const-string v0, "EPGTvListingsScreenViewModel"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvListingsScreenViewModel;->diagTag:Ljava/lang/String;

    .line 13
    return-void
.end method


# virtual methods
.method public onRehydrate()V
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvListingsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvListingsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 18
    return-void
.end method
