.class public abstract Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "PivotViewModelBase.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 8
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v0, 0x1

    .line 11
    invoke-direct {p0, p1, v0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZ)V

    .line 12
    return-void
.end method


# virtual methods
.method public abstract isBusy()Z
.end method

.method public abstract load(Z)V
.end method

.method protected abstract onStartOverride()V
.end method

.method protected abstract onStopOverride()V
.end method
