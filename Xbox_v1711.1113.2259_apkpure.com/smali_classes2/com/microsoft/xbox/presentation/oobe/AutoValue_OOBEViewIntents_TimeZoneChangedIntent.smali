.class final Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_TimeZoneChangedIntent;
.super Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;
.source "AutoValue_OOBEViewIntents_TimeZoneChangedIntent.java"


# instance fields
.field private final timeZone:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "timeZone"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;-><init>()V

    .line 13
    if-nez p1, :cond_0

    .line 14
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null timeZone"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_TimeZoneChangedIntent;->timeZone:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 33
    if-ne p1, p0, :cond_0

    .line 34
    const/4 v1, 0x1

    .line 40
    :goto_0
    return v1

    .line 36
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;

    .line 38
    .local v0, "that":Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_TimeZoneChangedIntent;->timeZone:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;->timeZone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 40
    .end local v0    # "that":Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 45
    const/4 v0, 0x1

    .line 46
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_TimeZoneChangedIntent;->timeZone:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 48
    return v0
.end method

.method public timeZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_TimeZoneChangedIntent;->timeZone:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TimeZoneChangedIntent{timeZone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_TimeZoneChangedIntent;->timeZone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
