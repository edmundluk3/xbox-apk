.class Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$VerticalListAdapter;
.super Landroid/widget/BaseAdapter;
.source "VirtualGrid.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VerticalListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V
    .locals 0

    .prologue
    .line 759
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$VerticalListAdapter;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$1;

    .prologue
    .line 759
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$VerticalListAdapter;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 763
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$VerticalListAdapter;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;->getRowCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 768
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 773
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 783
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$VerticalListAdapter;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getRowType(I)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 778
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$VerticalListAdapter;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 788
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$VerticalListAdapter;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getRowTypeCount()I

    move-result v0

    return v0
.end method
