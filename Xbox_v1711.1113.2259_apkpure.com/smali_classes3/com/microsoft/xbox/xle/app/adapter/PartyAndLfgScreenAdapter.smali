.class public Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "PartyAndLfgScreenAdapter.java"


# instance fields
.field private betaIndicator:Landroid/view/View;

.field private createPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private errorPane:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

.field private hostMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field private lfgClubSessions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field private lfgFollowingSessions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field private lfgUpcomingSessions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field private listViewAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private titleView:Landroid/widget/TextView;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

.field private viewPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;)V
    .locals 6
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    .line 52
    const v1, 0x7f0e0837

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->screenBody:Landroid/view/View;

    .line 53
    const v1, 0x7f0e0678

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->content:Landroid/view/View;

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->content:Landroid/view/View;

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 55
    const v1, 0x7f0e0679

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 57
    const v1, 0x7f0e083d

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->errorPane:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    .line 59
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    .line 60
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f03015d

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/toolkit/generics/Action;Z)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->listViewAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->listViewAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 74
    const v1, 0x7f0e0838

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->titleView:Landroid/widget/TextView;

    .line 75
    const v1, 0x7f0e0839

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->betaIndicator:Landroid/view/View;

    .line 77
    const v1, 0x7f0e083c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 78
    .local v0, "createButton":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    const v1, 0x7f0e083a

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->createPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->createPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    const v1, 0x7f0e083b

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 4
    .param p0, "handle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .prologue
    .line 63
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 65
    .local v0, "activityParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMultiplayerHandleId(Ljava/lang/String;)V

    .line 68
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackSelectLFG(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 69
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsScreen;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 71
    .end local v0    # "activityParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->showCreateLfgDialog()V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->createParty()V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewParty()V

    return-void
.end method

.method static synthetic lambda$updateViewOverride$4(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)I
    .locals 1
    .param p0, "h1"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .param p1, "h2"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .prologue
    .line 141
    if-ne p0, p1, :cond_0

    .line 142
    const/4 v0, 0x0

    .line 146
    :goto_0
    return v0

    .line 143
    :cond_0
    if-nez p0, :cond_1

    .line 144
    const/4 v0, 0x1

    goto :goto_0

    .line 146
    :cond_1
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->compareTo(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    return-object v0
.end method

.method protected updateViewOverride()V
    .locals 13

    .prologue
    .line 99
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v9

    .line 100
    .local v9, "viewModelState":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 101
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isBusy()Z

    move-result v10

    invoke-virtual {p0, v10}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->updateLoadingIndicator(Z)V

    .line 102
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->errorPane:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getErrorString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setText(Ljava/lang/String;)V

    .line 104
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isPartyEnabled()Z

    move-result v4

    .line 105
    .local v4, "isPartyEnabled":Z
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->titleView:Landroid/widget/TextView;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    if-eqz v4, :cond_4

    const v10, 0x7f0701f2

    :goto_0
    invoke-virtual {v12, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->betaIndicator:Landroid/view/View;

    if-eqz v4, :cond_5

    const/4 v10, 0x0

    :goto_1
    invoke-virtual {v11, v10}, Landroid/view/View;->setVisibility(I)V

    .line 108
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isInParty()Ljava/lang/Boolean;

    move-result-object v3

    .line 109
    .local v3, "isInParty":Ljava/lang/Boolean;
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->createPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-nez v10, :cond_6

    const/4 v10, 0x0

    :goto_2
    invoke-virtual {v11, v10}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 110
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_7

    const/4 v10, 0x0

    :goto_3
    invoke-virtual {v11, v10}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 112
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isBusy()Z

    move-result v10

    if-nez v10, :cond_b

    .line 113
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getPeopleSummaries()Ljava/util/Map;

    move-result-object v8

    .line 114
    .local v8, "viewModelHostMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getLfgUpcomingSessions()Ljava/util/List;

    move-result-object v7

    .line 115
    .local v7, "upcomingSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getLfgFollowingSessions()Ljava/util/List;

    move-result-object v1

    .line 116
    .local v1, "followingSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getClubSessions()Ljava/util/List;

    move-result-object v0

    .line 117
    .local v0, "clubSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v6, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 119
    .local v6, "suggestedSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgUpcomingSessions:Ljava/util/List;

    if-ne v7, v10, :cond_0

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgFollowingSessions:Ljava/util/List;

    if-ne v1, v10, :cond_0

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgClubSessions:Ljava/util/List;

    if-ne v0, v10, :cond_0

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->hostMap:Ljava/util/Map;

    if-ne v8, v10, :cond_0

    sget-object v10, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v9, v10, :cond_b

    .line 126
    :cond_0
    sget-object v10, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v9, v10, :cond_1

    .line 127
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v11, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 130
    :cond_1
    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgUpcomingSessions:Ljava/util/List;

    .line 131
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgFollowingSessions:Ljava/util/List;

    .line 132
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgClubSessions:Ljava/util/List;

    .line 133
    iput-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->hostMap:Ljava/util/Map;

    .line 136
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgClubSessions:Ljava/util/List;

    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgFollowingSessions:Ljava/util/List;

    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 137
    :cond_2
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgFollowingSessions:Ljava/util/List;

    invoke-interface {v6, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 138
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgClubSessions:Ljava/util/List;

    invoke-interface {v6, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 140
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter$$Lambda$5;->lambdaFactory$()Ljava/util/Comparator;

    move-result-object v10

    invoke-static {v6, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 151
    :cond_3
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->listViewAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->clear()V

    .line 153
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .local v5, "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;>;"
    new-instance v10, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;

    sget-object v11, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->Upcoming:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    const/4 v12, 0x0

    invoke-direct {v10, v11, v12}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;-><init>(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgUpcomingSessions:Ljava/util/List;

    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 158
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->lfgUpcomingSessions:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 159
    .local v2, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    new-instance v11, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;

    sget-object v12, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->Upcoming:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    invoke-direct {v11, v12, v2}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;-><init>(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 105
    .end local v0    # "clubSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .end local v1    # "followingSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .end local v2    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v3    # "isInParty":Ljava/lang/Boolean;
    .end local v5    # "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;>;"
    .end local v6    # "suggestedSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .end local v7    # "upcomingSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .end local v8    # "viewModelHostMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    :cond_4
    const v10, 0x7f0706e4

    goto/16 :goto_0

    .line 106
    :cond_5
    const/16 v10, 0x8

    goto/16 :goto_1

    .line 109
    .restart local v3    # "isInParty":Ljava/lang/Boolean;
    :cond_6
    const/16 v10, 0x8

    goto/16 :goto_2

    .line 110
    :cond_7
    const/16 v10, 0x8

    goto/16 :goto_3

    .line 162
    .restart local v0    # "clubSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .restart local v1    # "followingSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .restart local v5    # "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;>;"
    .restart local v6    # "suggestedSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .restart local v7    # "upcomingSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .restart local v8    # "viewModelHostMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    :cond_8
    new-instance v10, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;

    sget-object v11, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->UpcomingNoData:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    const/4 v12, 0x0

    invoke-direct {v10, v11, v12}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;-><init>(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_9
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-nez v10, :cond_a

    .line 166
    new-instance v10, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;

    sget-object v11, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->Suggested:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    const/4 v12, 0x0

    invoke-direct {v10, v11, v12}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;-><init>(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_a

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 169
    .restart local v2    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    new-instance v11, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;

    sget-object v12, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->Suggested:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    invoke-direct {v11, v12, v2}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;-><init>(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 173
    .end local v2    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    :cond_a
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->listViewAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->hostMap:Ljava/util/Map;

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->setUserMap(Ljava/util/Map;)V

    .line 174
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->listViewAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-virtual {v10, v5}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 175
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;->listViewAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->notifyDataSetChanged()V

    .line 178
    .end local v0    # "clubSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .end local v1    # "followingSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .end local v5    # "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;>;"
    .end local v6    # "suggestedSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .end local v7    # "upcomingSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    .end local v8    # "viewModelHostMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    :cond_b
    return-void
.end method
