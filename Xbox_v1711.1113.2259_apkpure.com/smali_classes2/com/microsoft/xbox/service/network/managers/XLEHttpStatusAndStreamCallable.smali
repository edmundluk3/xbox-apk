.class public final Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;
.super Ljava/lang/Object;
.source "XLEHttpStatusAndStreamCallable.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This class shouldn\'t be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static newDeleteInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$4;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$4;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public static newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$1;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public static newPostInstance(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$2;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newPutInstance(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$3;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method
