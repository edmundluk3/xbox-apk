.class public Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;
.super Lcom/microsoft/xbox/xle/app/ApplicationBarManager;
.source "ApplicationBarManagerPhone.java"


# static fields
.field private static instance:Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;


# instance fields
.field protected pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->instance:Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;-><init>()V

    .line 20
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->instance:Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;

    return-object v0
.end method


# virtual methods
.method protected disableButtons()V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->disableButtons()V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->setEnabled(Z)V

    .line 68
    return-void
.end method

.method protected enableButtons()V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->enableButtons()V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->setEnabled(Z)V

    .line 75
    return-void
.end method

.method protected getNowPlayingSecondaryText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 89
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0707c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPageIndicator()Lcom/microsoft/xbox/toolkit/ui/PageIndicator;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onCreate()V

    .line 31
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    .line 32
    return-void
.end method

.method public setCurrentPage(I)V
    .locals 1
    .param p1, "pageIndex"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->setCurrentPage(I)V

    .line 46
    :cond_0
    return-void
.end method

.method public setPageIndicatorBackground(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->setBackgroundColor(I)V

    .line 61
    :cond_0
    return-void
.end method

.method public setPageIndicatorDrawables(II)V
    .locals 1
    .param p1, "activeDrawableID"    # I
    .param p2, "inactiveDrawableID"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->setActivePageDrawableID(I)V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->setInactivePageDrawableID(I)V

    .line 54
    :cond_0
    return-void
.end method

.method public setTotalPageCount(I)V
    .locals 1
    .param p1, "totalCount"    # I

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->pageIndicator:Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->setTotalPageCount(I)V

    .line 39
    :cond_0
    return-void
.end method

.method protected shouldShowSwapButtonAnimation()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    return v0
.end method
