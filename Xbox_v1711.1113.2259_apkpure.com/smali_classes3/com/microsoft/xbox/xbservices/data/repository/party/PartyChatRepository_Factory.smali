.class public final Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;
.super Ljava/lang/Object;
.source "PartyChatRepository_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final loggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;"
        }
    .end annotation
.end field

.field private final multiplayerServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;",
            ">;"
        }
    .end annotation
.end field

.field private final privacyServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;",
            ">;"
        }
    .end annotation
.end field

.field private final qosRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final rtaRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final telemetryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final webRtcManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final xuidProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "rtaRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/rta/RtaRepository;>;"
    .local p2, "multiplayerServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;>;"
    .local p3, "privacyServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;>;"
    .local p4, "qosRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;>;"
    .local p5, "xuidProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;>;"
    .local p6, "loggerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;>;"
    .local p7, "webRtcManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;>;"
    .local p8, "telemetryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->rtaRepositoryProvider:Ljavax/inject/Provider;

    .line 45
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->multiplayerServiceProvider:Ljavax/inject/Provider;

    .line 47
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->privacyServiceProvider:Ljavax/inject/Provider;

    .line 49
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 50
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->qosRepositoryProvider:Ljavax/inject/Provider;

    .line 51
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 52
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->xuidProvider:Ljavax/inject/Provider;

    .line 53
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 54
    :cond_5
    iput-object p6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->loggerProvider:Ljavax/inject/Provider;

    .line 55
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 56
    :cond_6
    iput-object p7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->webRtcManagerProvider:Ljavax/inject/Provider;

    .line 57
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_7

    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 58
    :cond_7
    iput-object p8, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->telemetryProvider:Ljavax/inject/Provider;

    .line 59
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    .local p0, "rtaRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/rta/RtaRepository;>;"
    .local p1, "multiplayerServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;>;"
    .local p2, "privacyServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;>;"
    .local p3, "qosRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;>;"
    .local p4, "xuidProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;>;"
    .local p5, "loggerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;>;"
    .local p6, "webRtcManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;>;"
    .local p7, "telemetryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;>;"
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .locals 9

    .prologue
    .line 63
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->rtaRepositoryProvider:Ljavax/inject/Provider;

    .line 64
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->multiplayerServiceProvider:Ljavax/inject/Provider;

    .line 65
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->privacyServiceProvider:Ljavax/inject/Provider;

    .line 66
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->qosRepositoryProvider:Ljavax/inject/Provider;

    .line 67
    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->xuidProvider:Ljavax/inject/Provider;

    .line 68
    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->loggerProvider:Ljavax/inject/Provider;

    .line 69
    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->webRtcManagerProvider:Ljavax/inject/Provider;

    .line 70
    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    iget-object v8, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->telemetryProvider:Ljavax/inject/Provider;

    .line 71
    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    invoke-direct/range {v0 .. v8}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;-><init>(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;)V

    .line 63
    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->get()Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    move-result-object v0

    return-object v0
.end method
