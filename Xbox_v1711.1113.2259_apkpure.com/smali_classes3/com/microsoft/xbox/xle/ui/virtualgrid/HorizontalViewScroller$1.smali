.class Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$1;
.super Ljava/lang/Object;
.source "HorizontalViewScroller.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    .prologue
    .line 434
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$1;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 438
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$1;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$000(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$1;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$1;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$000(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)Landroid/widget/Scroller;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setContentOffset(I)V

    .line 440
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$1;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$100(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)V

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 441
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$1;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$200(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$1;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$300(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    goto :goto_0
.end method
