.class Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$2;
.super Ljava/lang/Object;
.source "PhoneContactChangeRegionDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$2;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$2;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 61
    .local v0, "countryName":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getRegionFromCountryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "region":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$2;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->setRegionAndCountryName(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$2;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->dismiss()V

    .line 64
    return-void
.end method
