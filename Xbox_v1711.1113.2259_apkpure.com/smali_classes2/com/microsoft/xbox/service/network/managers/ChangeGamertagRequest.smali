.class public Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;
.super Ljava/lang/Object;
.source "ChangeGamertagRequest.java"


# instance fields
.field public gamertag:Ljava/lang/String;

.field public preview:Z

.field public reservationId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0
    .param p1, "gamertag"    # Ljava/lang/String;
    .param p2, "preview"    # Z
    .param p3, "reservationId"    # Ljava/lang/String;

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;->gamertag:Ljava/lang/String;

    .line 10
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;->preview:Z

    .line 11
    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;->reservationId:Ljava/lang/String;

    .line 12
    return-void
.end method
