.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Availability"
.end annotation


# instance fields
.field private final actions:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Actions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final affirmationId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AffirmationId"
    .end annotation
.end field

.field public final availabilityId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AvailabilityId"
    .end annotation
.end field

.field public final displayRank:I
    .annotation build Landroid/support/annotation/IntRange;
        from = 0x0L
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "DisplayRank"
    .end annotation
.end field

.field public final orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "OrderManagementData"
    .end annotation
.end field

.field public final properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Properties"
    .end annotation
.end field

.field public final remediationRequired:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "RemediationRequired"
    .end annotation
.end field

.field public final skuId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "SkuId"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;ZILjava/lang/String;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "affirmationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "availabilityId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "orderManagementData"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "availabilityProperties"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "remediationRequired"    # Z
    .param p7, "displayRank"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p8, "skuId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;",
            "ZI",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 528
    .local p1, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->actions:Ljava/util/List;

    .line 530
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->affirmationId:Ljava/lang/String;

    .line 531
    iput-object p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->availabilityId:Ljava/lang/String;

    .line 532
    iput-object p4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;

    .line 533
    iput-object p5, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;

    .line 534
    iput-boolean p6, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->remediationRequired:Z

    .line 535
    iput p7, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->displayRank:I

    .line 536
    iput-object p8, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->skuId:Ljava/lang/String;

    .line 537
    return-void
.end method


# virtual methods
.method public getActions()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->actions:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
