.class public Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "PeopleScreenListAdapter$PersonViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;

    .line 25
    const v0, 0x7f0e08e9

    const-string v1, "field \'rootView\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->rootView:Landroid/view/View;

    .line 26
    const v0, 0x7f0e03d4

    const-string v1, "field \'gamerPicView\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->gamerPicView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 27
    const v0, 0x7f0e0230

    const-string v1, "field \'favoriteIconView\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->favoriteIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 28
    const v0, 0x7f0e08ea

    const-string v1, "field \'broadcastingIconView\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->broadcastingIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 29
    const v0, 0x7f0e08eb

    const-string v1, "field \'partyIconView\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->partyIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 30
    const v0, 0x7f0e03d5

    const-string v1, "field \'presenceImageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->presenceImageView:Landroid/widget/ImageView;

    .line 31
    const v0, 0x7f0e06cb

    const-string v1, "field \'gamertagTextView\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->gamertagTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 32
    const v0, 0x7f0e08ec

    const-string v1, "field \'gameractivityTextView\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->gameractivityTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 33
    const v0, 0x7f0e06cc

    const-string v1, "field \'realnameTextView\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->realnameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 34
    const v0, 0x7f0e06cd

    const-string v1, "field \'recommendationIconImage\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 35
    const v0, 0x7f0e06ce

    const-string v1, "field \'recommendationIconText\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->recommendationIconText:Landroid/view/View;

    .line 36
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;

    .line 42
    .local v0, "target":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 43
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;

    .line 45
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->rootView:Landroid/view/View;

    .line 46
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->gamerPicView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 47
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->favoriteIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 48
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->broadcastingIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 49
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->partyIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 50
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->presenceImageView:Landroid/widget/ImageView;

    .line 51
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->gamertagTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 52
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->gameractivityTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 53
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->realnameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 54
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 55
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->recommendationIconText:Landroid/view/View;

    .line 56
    return-void
.end method
