.class Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$3;
.super Ljava/lang/Object;
.source "URCGeneratedGroupView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->updateButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$3;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/4 v8, -0x1

    .line 120
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    float-to-double v4, v3

    const-wide/high16 v6, 0x4033000000000000L    # 19.0

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v6

    double-to-int v1, v4

    .line 122
    .local v1, "margin":I
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v8, v8}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 123
    .local v0, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 125
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const-string v3, "btn.live"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget v3, Lcom/microsoft/xbox/xle/urc/R$layout;->urc_numpad_small_button:I

    :goto_0
    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 126
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 128
    const-string v3, "btn.play"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 129
    sget v3, Lcom/microsoft/xbox/xle/urc/R$drawable;->btn_white_circle:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 133
    :cond_0
    :goto_1
    return-object v2

    .line 125
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    sget v3, Lcom/microsoft/xbox/xle/urc/R$layout;->urc_playback_button:I

    goto :goto_0

    .line 130
    .restart local v2    # "v":Landroid/view/View;
    :cond_2
    const-string v3, "btn.live"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 131
    sget v3, Lcom/microsoft/xbox/xle/urc/R$drawable;->btn_dark_circle:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method
