.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$LFG;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LFG"
.end annotation


# static fields
.field public static final AddDescription:Ljava/lang/String; = "LFG - Add Description"

.field public static final AddTags:Ljava/lang/String; = "LFG - Add Tags"

.field public static final CancelPost:Ljava/lang/String; = "LFG - Cancel Post"

.field public static final Close:Ljava/lang/String; = "LFG - Close"

.field public static final Confirm:Ljava/lang/String; = "LFG - Confirm"

.field public static final Create:Ljava/lang/String; = "LFG - Create"

.field public static final Decline:Ljava/lang/String; = "LFG - Decline"

.field public static final DeclineAll:Ljava/lang/String; = "LFG - Decline All"

.field public static final Done:Ljava/lang/String; = "LFG - Done"

.field public static final GameSearch:Ljava/lang/String; = "LFG - Game Search"

.field public static final GroupSelect:Ljava/lang/String; = "LFG - Group Select"

.field public static final Interested:Ljava/lang/String; = "LFG - Interested"

.field public static final InterestedList:Ljava/lang/String; = "LFG - Interested List"

.field public static final LeaveGroup:Ljava/lang/String; = "LFG - Leave Group"

.field public static final Post:Ljava/lang/String; = "LFG - Post"

.field public static final Report:Ljava/lang/String; = "LFG - Report"

.field public static final ReportHost:Ljava/lang/String; = "LFG - Report Host"

.field public static final SendMessage:Ljava/lang/String; = "LFG - Send Message"

.field public static final Share:Ljava/lang/String; = "LFG - Share"

.field public static final ShareNewLFGToFeed:Ljava/lang/String; = "LFG - Share New LFG To Feed"

.field public static final ShareToActivityFeed:Ljava/lang/String; = "LFG - Share To Activity Feed"

.field public static final ShareToMessage:Ljava/lang/String; = "LFG - Share To Message"

.field public static final TagSearch:Ljava/lang/String; = "LFG - Tag Search"

.field public static final ViewHistory:Ljava/lang/String; = "LFG - View History"

.field public static final ViewProfile:Ljava/lang/String; = "LFG - View Profile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
