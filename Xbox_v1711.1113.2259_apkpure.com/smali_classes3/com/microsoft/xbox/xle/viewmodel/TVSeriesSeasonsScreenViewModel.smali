.class public Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;
.source "TVSeriesSeasonsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;",
        ">;"
    }
.end annotation


# instance fields
.field private currentSelectedTVSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

.field private espisodesListState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private finishedLoadingEpisodes:Z

.field private isLoadingEpisodes:Z

.field private loadSeasonEpisodeTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;

.field private selectedTVSeasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;-><init>()V

    .line 20
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->currentSelectedTVSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .line 21
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->loadSeasonEpisodeTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;

    .line 22
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->finishedLoadingEpisodes:Z

    .line 23
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->isLoadingEpisodes:Z

    .line 24
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->espisodesListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 25
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->selectedTVSeasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    .line 30
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVSeriesSeasonsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->onSeasonEpisodesListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->finishedLoadingEpisodes:Z

    return p1
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->isLoadingEpisodes:Z

    return p1
.end method

.method private onSeasonChanged(Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;)V
    .locals 2
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .prologue
    .line 81
    if-nez p1, :cond_0

    .line 96
    :goto_0
    return-void

    .line 85
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->currentSelectedTVSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->currentSelectedTVSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->selectedTVSeasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->loadSeasonEpisodeTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->loadSeasonEpisodeTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->cancel()V

    .line 93
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->currentSelectedTVSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->loadSeasonEpisodeTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->loadSeasonEpisodeTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->load(Z)V

    .line 95
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->updateAdapter()V

    goto :goto_0
.end method

.method private onSeasonEpisodesListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 136
    const-string v0, "TVSeriesDetailsViewModel"

    const-string v1, "onSeasonEpisodesCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->isLoadingEpisodes:Z

    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->finishedLoadingEpisodes:Z

    .line 140
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 155
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->updateAdapter()V

    .line 156
    return-void

    .line 144
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->selectedTVSeasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->espisodesListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 148
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 149
    const-string v0, "MediaItemListViewModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error state because "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 140
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public NavigateToTvEpisodeDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 104
    return-void
.end method

.method public finishedLoadingEpisodes()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->finishedLoadingEpisodes:Z

    return v0
.end method

.method public getEpisodesListState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->espisodesListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public getEpisodesOfSelectedSeason()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->selectedTVSeasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->selectedTVSeasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v0

    .line 115
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 66
    const v0, 0x7f070683

    return v0
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSeasons()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->isLoadingDetail:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->isLoadingEpisodes:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->isLoadingList:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->load(Z)V

    .line 42
    return-void
.end method

.method protected onListDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->currentSelectedTVSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .line 122
    .local v0, "defaultSeason":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->onSeasonChanged(Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;)V

    .line 128
    .end local v0    # "defaultSeason":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    :goto_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->onListDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 129
    return-void

    .line 124
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->finishedLoadingEpisodes:Z

    goto :goto_0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVSeriesSeasonsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 36
    return-void
.end method

.method public onSeasonChanged(I)V
    .locals 4
    .param p1, "seasonIndex"    # I

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->getSeasons()Ljava/util/ArrayList;

    move-result-object v1

    .line 71
    .local v1, "seasons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, p1, :cond_0

    .line 72
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .line 73
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->onSeasonChanged(Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;)V

    .line 78
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    :goto_0
    return-void

    .line 75
    :cond_0
    const-string v2, "TVSeriesSeasonScreen"

    const-string v3, "selected season out of index"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->loadListTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->loadListTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->cancel()V

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->loadSeasonEpisodeTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->loadSeasonEpisodeTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->cancel()V

    .line 52
    :cond_1
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->onStop()V

    .line 53
    return-void
.end method
