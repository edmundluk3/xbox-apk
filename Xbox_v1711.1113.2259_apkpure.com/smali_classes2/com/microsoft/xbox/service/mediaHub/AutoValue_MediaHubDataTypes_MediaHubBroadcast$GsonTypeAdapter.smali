.class public final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MediaHubDataTypes_MediaHubBroadcast.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;",
        ">;"
    }
.end annotation


# instance fields
.field private final broadcastIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final contentIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultBroadcastId:Ljava/lang/String;

.field private defaultContentId:Ljava/lang/String;

.field private defaultOwnerXuid:Ljava/lang/String;

.field private defaultProvider:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

.field private defaultStartDate:Ljava/util/Date;

.field private defaultTitleId:J

.field private defaultTitleName:Ljava/lang/String;

.field private defaultViewers:I

.field private final ownerXuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final providerAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final startDateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final titleIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final titleNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final viewersAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 3
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 31
    iput-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultBroadcastId:Ljava/lang/String;

    .line 32
    iput-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 33
    iput-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/String;

    .line 34
    iput-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultProvider:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    .line 35
    iput-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultStartDate:Ljava/util/Date;

    .line 36
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultTitleId:J

    .line 37
    iput-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultViewers:I

    .line 40
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->broadcastIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 41
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 42
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 43
    const-class v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->providerAdapter:Lcom/google/gson/TypeAdapter;

    .line 44
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->startDateAdapter:Lcom/google/gson/TypeAdapter;

    .line 45
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 46
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 47
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->viewersAdapter:Lcom/google/gson/TypeAdapter;

    .line 48
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;
    .locals 12
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v11, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v11, :cond_0

    .line 110
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 111
    const/4 v0, 0x0

    .line 167
    :goto_0
    return-object v0

    .line 113
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultBroadcastId:Ljava/lang/String;

    .line 115
    .local v1, "broadcastId":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 116
    .local v2, "contentId":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/String;

    .line 117
    .local v3, "ownerXuid":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultProvider:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    .line 118
    .local v4, "provider":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultStartDate:Ljava/util/Date;

    .line 119
    .local v5, "startDate":Ljava/util/Date;
    iget-wide v6, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultTitleId:J

    .line 120
    .local v6, "titleId":J
    iget-object v8, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 121
    .local v8, "titleName":Ljava/lang/String;
    iget v9, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultViewers:I

    .line 122
    .local v9, "viewers":I
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v10

    .line 124
    .local v10, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v11, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v11, :cond_1

    .line 125
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 128
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 162
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 128
    :sswitch_0
    const-string v11, "broadcastId"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v11, "contentId"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v11, "ownerXuid"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v11, "provider"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v11, "startDate"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v11, "titleId"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v11, "titleName"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v0, 0x6

    goto :goto_2

    :sswitch_7
    const-string v11, "viewers"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v0, 0x7

    goto :goto_2

    .line 130
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->broadcastIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "broadcastId":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 131
    .restart local v1    # "broadcastId":Ljava/lang/String;
    goto :goto_1

    .line 134
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "contentId":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 135
    .restart local v2    # "contentId":Ljava/lang/String;
    goto/16 :goto_1

    .line 138
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "ownerXuid":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 139
    .restart local v3    # "ownerXuid":Ljava/lang/String;
    goto/16 :goto_1

    .line 142
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->providerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "provider":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;
    check-cast v4, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    .line 143
    .restart local v4    # "provider":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;
    goto/16 :goto_1

    .line 146
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->startDateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "startDate":Ljava/util/Date;
    check-cast v5, Ljava/util/Date;

    .line 147
    .restart local v5    # "startDate":Ljava/util/Date;
    goto/16 :goto_1

    .line 150
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 151
    goto/16 :goto_1

    .line 154
    :pswitch_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "titleName":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 155
    .restart local v8    # "titleName":Ljava/lang/String;
    goto/16 :goto_1

    .line 158
    :pswitch_7
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->viewersAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 159
    goto/16 :goto_1

    .line 166
    .end local v10    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 167
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast;

    invoke-direct/range {v0 .. v9}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;Ljava/util/Date;JLjava/lang/String;I)V

    goto/16 :goto_0

    .line 128
    :sswitch_data_0
    .sparse-switch
        -0x7f4fdafd -> :sswitch_6
        -0x7ef1d8d0 -> :sswitch_4
        -0x4deb0a6d -> :sswitch_5
        -0x3adbfa0f -> :sswitch_3
        -0x39553a44 -> :sswitch_0
        -0x1843fc8c -> :sswitch_1
        0x10f262b -> :sswitch_2
        0x1b1310a1 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultBroadcastId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultBroadcastId"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultBroadcastId:Ljava/lang/String;

    .line 51
    return-object p0
.end method

.method public setDefaultContentId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultContentId"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 55
    return-object p0
.end method

.method public setDefaultOwnerXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultOwnerXuid"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/String;

    .line 59
    return-object p0
.end method

.method public setDefaultProvider(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultProvider"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultProvider:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    .line 63
    return-object p0
.end method

.method public setDefaultStartDate(Ljava/util/Date;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultStartDate"    # Ljava/util/Date;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultStartDate:Ljava/util/Date;

    .line 67
    return-object p0
.end method

.method public setDefaultTitleId(J)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultTitleId"    # J

    .prologue
    .line 70
    iput-wide p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultTitleId:J

    .line 71
    return-object p0
.end method

.method public setDefaultTitleName(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleName"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 75
    return-object p0
.end method

.method public setDefaultViewers(I)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultViewers"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->defaultViewers:I

    .line 79
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    if-nez p2, :cond_0

    .line 85
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 106
    :goto_0
    return-void

    .line 88
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 89
    const-string v0, "broadcastId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->broadcastIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;->broadcastId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 91
    const-string v0, "contentId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;->contentId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 93
    const-string v0, "ownerXuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;->ownerXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 95
    const-string v0, "provider"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->providerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;->provider()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 97
    const-string v0, "startDate"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->startDateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;->startDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 99
    const-string v0, "titleId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;->titleId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 101
    const-string v0, "titleName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;->titleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 103
    const-string v0, "viewers"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->viewersAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;->viewers()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 105
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_MediaHubBroadcast$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;)V

    return-void
.end method
