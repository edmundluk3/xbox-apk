.class public Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "SkypeConversationDetailsActivityAdapter.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;
    }
.end annotation


# static fields
.field private static final CHARACTER_LIMIT:I

.field private static final CHARACTER_MAX_COUNT:I

.field private static final MAX_FRIENDS_COUNT_SHOWN:I = 0x4

.field private static final TAG:Ljava/lang/String;

.field private static final TYPING_MESSAGE_FREQUENCY_MS:I = 0x7d0


# instance fields
.field private addPeopleView:Landroid/widget/RelativeLayout;

.field private final characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private createGroupConversation:Landroid/widget/RelativeLayout;

.field private final detailListLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private friendsPickerCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private gamertagTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private iconsContainerRow2:Landroid/widget/TableRow;

.field private imageTile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private imageTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private imageTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private imageTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private imageTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private isKeyboardVisible:Z

.field private lastTypingMessageTimestamp:J

.field private final layoutListener:Landroid/view/View$OnLayoutChangeListener;

.field private leaveConversationView:Landroid/widget/RelativeLayout;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

.field private listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private messageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private messagesMenuArrow:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private messagesMenuView:Landroid/widget/LinearLayout;

.field private messagesPeopleIconsContainer:Landroid/widget/TableLayout;

.field private muteNotificationView:Landroid/widget/RelativeLayout;

.field private newMessage:Ljava/lang/String;

.field private oldMessage:Landroid/text/Editable;

.field private onlineCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private openHoverChat:Landroid/widget/RelativeLayout;

.field private presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private renameConversationView:Landroid/widget/RelativeLayout;

.field private sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private slideIn:Landroid/view/animation/Animation;

.field private slideOut:Landroid/view/animation/Animation;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final textWatcher:Landroid/text/TextWatcher;

.field private typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private viewAllMembersView:Landroid/widget/RelativeLayout;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

.field private viewProfile:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 61
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->TAG:Ljava/lang/String;

    .line 64
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->CHARACTER_LIMIT:I

    .line 66
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0b0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->CHARACTER_MAX_COUNT:I

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V
    .locals 6
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 109
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 106
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->lastTypingMessageTimestamp:J

    .line 590
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->detailListLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 674
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->layoutListener:Landroid/view/View$OnLayoutChangeListener;

    .line 683
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->textWatcher:Landroid/text/TextWatcher;

    .line 111
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    .line 113
    .local v0, "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .line 115
    const v1, 0x7f0e045f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->screenBody:Landroid/view/View;

    .line 116
    const v1, 0x7f0e0462

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->content:Landroid/view/View;

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->content:Landroid/view/View;

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 120
    const v1, 0x7f0e0470

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->gamertagTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->gamertagTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->gamertagTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setGravity(I)V

    .line 124
    :cond_0
    const v1, 0x7f0e0412

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 125
    const v1, 0x7f0e0465

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 126
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

    const v2, 0x7f0300bb

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Z)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 129
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setChoiceMode(I)V

    .line 130
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setStackFromBottom(Z)V

    .line 132
    const v1, 0x7f0e0413

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 133
    const v1, 0x7f0e0410

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 135
    const v1, 0x7f0e0464

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-array v2, v4, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    sget v4, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->CHARACTER_MAX_COUNT:I

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 138
    const v1, 0x7f0e0471

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->onlineCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 140
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->setConversationIconsContainer()V

    .line 142
    const v1, 0x7f0e0472

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->friendsPickerCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 143
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->friendsPickerCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 145
    const v1, 0x7f0e040e

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 147
    const v1, 0x7f04000e

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->slideIn:Landroid/view/animation/Animation;

    .line 148
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->slideIn:Landroid/view/animation/Animation;

    invoke-virtual {v1, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 150
    const v1, 0x7f04000f

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->slideOut:Landroid/view/animation/Animation;

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->slideOut:Landroid/view/animation/Animation;

    invoke-virtual {v1, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 153
    const v1, 0x7f0e0473

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuArrow:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuArrow:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 156
    const v1, 0x7f0e02d5

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    .line 157
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 159
    const v1, 0x7f0e0469

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TableLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesPeopleIconsContainer:Landroid/widget/TableLayout;

    .line 161
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e047b

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->renameConversationView:Landroid/widget/RelativeLayout;

    .line 162
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0479

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->addPeopleView:Landroid/widget/RelativeLayout;

    .line 163
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0478

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewAllMembersView:Landroid/widget/RelativeLayout;

    .line 164
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e047c

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->leaveConversationView:Landroid/widget/RelativeLayout;

    .line 165
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e047a

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->muteNotificationView:Landroid/widget/RelativeLayout;

    .line 166
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0474

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewProfile:Landroid/widget/RelativeLayout;

    .line 167
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0476

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->openHoverChat:Landroid/widget/RelativeLayout;

    .line 168
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0477

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->createGroupConversation:Landroid/widget/RelativeLayout;

    .line 169
    return-void
.end method

.method static synthetic access$000(Ljava/util/Date;)Z
    .locals 1
    .param p0, "x0"    # Ljava/util/Date;

    .prologue
    .line 59
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->isOldMessage(Ljava/util/Date;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEListView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEListView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->setSendButtonEnabled(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->lastTypingMessageTimestamp:J

    return-wide v0
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "x1"    # J

    .prologue
    .line 59
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->lastTypingMessageTimestamp:J

    return-wide p1
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateCharacterCountAndVisibility()V

    return-void
.end method

.method private disableMenuOptions()V
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->muteNotificationView:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->disableMenuOptions(Landroid/widget/RelativeLayout;)V

    .line 515
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->createGroupConversation:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->disableMenuOptions(Landroid/widget/RelativeLayout;)V

    .line 516
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewProfile:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->disableMenuOptions(Landroid/widget/RelativeLayout;)V

    .line 517
    return-void
.end method

.method private disableMenuOptions(Landroid/widget/RelativeLayout;)V
    .locals 4
    .param p1, "menuItemView"    # Landroid/widget/RelativeLayout;

    .prologue
    const/4 v3, 0x0

    .line 530
    if-eqz p1, :cond_0

    .line 531
    const v2, 0x7f0e02d7

    invoke-virtual {p1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 532
    .local v0, "menuIcon":Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 533
    const v2, 0x7f0e0475

    invoke-virtual {p1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 534
    .local v1, "menuText":Landroid/view/View;
    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 535
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 537
    .end local v0    # "menuIcon":Landroid/view/View;
    .end local v1    # "menuText":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private static isOldMessage(Ljava/util/Date;)Z
    .locals 6
    .param p0, "originalArrivalTime"    # Ljava/util/Date;

    .prologue
    const/4 v3, 0x0

    .line 636
    const/16 v1, 0x1e

    .line 638
    .local v1, "days":I
    if-nez p0, :cond_1

    .line 646
    :cond_0
    :goto_0
    return v3

    .line 642
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 643
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 644
    const/4 v4, 0x5

    const/16 v5, -0x1e

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 645
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    .line 646
    .local v2, "earliest":Ljava/util/Date;
    invoke-virtual {p0, v2}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v4

    if-gez v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->setMessageBody(Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->sendSkypeReply()V

    .line 211
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->setSendButtonEnabled(Z)V

    .line 212
    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->launchUnsharedFeed()V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateDropdown(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    .prologue
    .line 239
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 241
    .local v1, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 242
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v4, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    sub-int v0, v2, v3

    .line 244
    .local v0, "heightDiff":I
    const/16 v2, 0x64

    if-le v0, v2, :cond_2

    .line 245
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->isKeyboardVisible:Z

    if-nez v2, :cond_0

    .line 246
    const-string v2, "ConversationDetailsActivityAdapter"

    const-string v3, " keyboard is visible "

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->hide()V

    .line 249
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->isKeyboardVisible:Z

    .line 256
    :cond_1
    :goto_0
    return-void

    .line 250
    :cond_2
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->isKeyboardVisible:Z

    if-eqz v2, :cond_1

    .line 251
    const-string v2, "ConversationDetailsActivityAdapter"

    const-string v3, " keyboard not visible"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->isKeyboardVisible:Z

    .line 253
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->show()V

    goto :goto_0
.end method

.method static synthetic lambda$setMenuItemListenersAndVisibility$10(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 305
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateDropdown(Z)V

    .line 306
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->openAsHoverChat()V

    .line 307
    return-void
.end method

.method static synthetic lambda$setMenuItemListenersAndVisibility$11(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 312
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateDropdown(Z)V

    .line 313
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->createGroupConversation()V

    .line 314
    return-void
.end method

.method static synthetic lambda$setMenuItemListenersAndVisibility$4(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 263
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateDropdown(Z)V

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->navigateToRenameDialog()V

    .line 265
    return-void
.end method

.method static synthetic lambda$setMenuItemListenersAndVisibility$5(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 270
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateDropdown(Z)V

    .line 271
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->navigateToAddPeople()V

    .line 272
    return-void
.end method

.method static synthetic lambda$setMenuItemListenersAndVisibility$6(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 277
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateDropdown(Z)V

    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->navigateToViewMemberScreen()V

    .line 279
    return-void
.end method

.method static synthetic lambda$setMenuItemListenersAndVisibility$7(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 284
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateDropdown(Z)V

    .line 285
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->handleDeleteConversation()V

    .line 286
    return-void
.end method

.method static synthetic lambda$setMenuItemListenersAndVisibility$8(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 291
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateDropdown(Z)V

    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->setMuteConversation()V

    .line 293
    return-void
.end method

.method static synthetic lambda$setMenuItemListenersAndVisibility$9(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 298
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateDropdown(Z)V

    .line 299
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->navigateToConversationMemberProfile()V

    .line 300
    return-void
.end method

.method private restoreSoftInputAdjustMode()V
    .locals 3

    .prologue
    .line 659
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 661
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 662
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 663
    .local v1, "wnd":Landroid/view/Window;
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 665
    .end local v1    # "wnd":Landroid/view/Window;
    :cond_0
    return-void
.end method

.method private saveAndAdjustSoftInputAdjustMode()V
    .locals 3

    .prologue
    .line 650
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 652
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 653
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 654
    .local v1, "wnd":Landroid/view/Window;
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 656
    .end local v1    # "wnd":Landroid/view/Window;
    :cond_0
    return-void
.end method

.method private setConversationIconsContainer()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 172
    const v0, 0x7f0e046b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 175
    const v0, 0x7f0e046c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 178
    const v0, 0x7f0e046e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 181
    const v0, 0x7f0e046f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 186
    const v0, 0x7f0e046d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->iconsContainerRow2:Landroid/widget/TableRow;

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->iconsContainerRow2:Landroid/widget/TableRow;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 189
    const v0, 0x7f0e0468

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 192
    const v0, 0x7f0e0231

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 194
    return-void
.end method

.method private setMenuItemListenersAndVisibility()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->renameConversationView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->renameConversationView:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->addPeopleView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->addPeopleView:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewAllMembersView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewAllMembersView:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->leaveConversationView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->leaveConversationView:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->muteNotificationView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_4

    .line 290
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->muteNotificationView:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewProfile:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_5

    .line 297
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewProfile:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 303
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->openHoverChat:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_6

    .line 304
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->openHoverChat:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 310
    :cond_6
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->createGroupConversation:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_7

    .line 311
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->createGroupConversation:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 317
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewAllMembersView:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeGroupConversation()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 318
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->addPeopleView:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeGroupConversation()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_1
    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 319
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->renameConversationView:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeGroupConversation()Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_2
    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 320
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->leaveConversationView:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeGroupConversation()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    :goto_3
    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 321
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewProfile:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeGroupConversation()Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v2

    :goto_4
    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 322
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->createGroupConversation:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeGroupConversation()Z

    move-result v3

    if-eqz v3, :cond_d

    :goto_5
    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 323
    return-void

    :cond_8
    move v0, v2

    .line 317
    goto :goto_0

    :cond_9
    move v0, v2

    .line 318
    goto :goto_1

    :cond_a
    move v0, v2

    .line 319
    goto :goto_2

    :cond_b
    move v0, v2

    .line 320
    goto :goto_3

    :cond_c
    move v0, v1

    .line 321
    goto :goto_4

    :cond_d
    move v2, v1

    .line 322
    goto :goto_5
.end method

.method private setSendButtonEnabled(Z)V
    .locals 2
    .param p1, "enableSendButton"    # Z

    .prologue
    .line 668
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 670
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 672
    :cond_0
    return-void

    .line 670
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateCharacterCountAndVisibility()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 738
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v1, :cond_0

    .line 739
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 741
    .local v0, "length":I
    sget v1, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->CHARACTER_LIMIT:I

    if-lt v0, v1, :cond_1

    .line 742
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 743
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%d/%d"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    sget v5, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->CHARACTER_MAX_COUNT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 744
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070d71

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    sget v5, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->CHARACTER_MAX_COUNT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 749
    .end local v0    # "length":I
    :cond_0
    :goto_0
    return-void

    .line 746
    .restart local v0    # "length":I
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateConversationIcons(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "urlList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v11, 0x8

    const/4 v7, 0x3

    const/4 v10, 0x1

    const v8, 0x7f020125

    const/4 v9, 0x0

    .line 540
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 541
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 542
    .local v2, "recipientImageUrlsCount":I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 543
    .local v4, "viewList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-interface {v4, v9, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 544
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-interface {v4, v10, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 545
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-interface {v4, v5, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 546
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-interface {v4, v7, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 549
    const/4 v5, 0x4

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 551
    .local v0, "actualImagesCount":I
    if-lt v0, v7, :cond_0

    .line 553
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->iconsContainerRow2:Landroid/widget/TableRow;

    invoke-virtual {v5, v9}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 556
    :cond_0
    if-ne v0, v10, :cond_3

    .line 558
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->iconsContainerRow2:Landroid/widget/TableRow;

    invoke-virtual {v5, v11}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 559
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesPeopleIconsContainer:Landroid/widget/TableLayout;

    invoke-static {v5, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 560
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v5, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 561
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSelectedSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v7

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v6, v7, v9}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getGroupMemberPic(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 569
    :cond_1
    add-int/lit8 v3, v2, -0x4

    .line 570
    .local v3, "recipientsCount":I
    if-lez v3, :cond_2

    .line 571
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->friendsPickerCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070666

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 574
    .end local v0    # "actualImagesCount":I
    .end local v2    # "recipientImageUrlsCount":I
    .end local v3    # "recipientsCount":I
    .end local v4    # "viewList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;>;"
    :cond_2
    return-void

    .line 563
    .restart local v0    # "actualImagesCount":I
    .restart local v2    # "recipientImageUrlsCount":I
    .restart local v4    # "viewList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;>;"
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 564
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v5, v9}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 565
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 563
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private updateMuteMenuItem(Z)V
    .locals 4
    .param p1, "conversationMuted"    # Z

    .prologue
    .line 520
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->muteNotificationView:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    .line 521
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->muteNotificationView:Landroid/widget/RelativeLayout;

    const v3, 0x7f0e02d7

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 522
    .local v0, "menuIcon":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->muteNotificationView:Landroid/widget/RelativeLayout;

    const v3, 0x7f0e0475

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 524
    .local v1, "menuText":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz p1, :cond_1

    const v2, 0x7f071052

    :goto_0
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 525
    if-eqz p1, :cond_2

    const v2, 0x7f07066b

    :goto_1
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 527
    .end local v0    # "menuIcon":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .end local v1    # "menuText":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_0
    return-void

    .line 524
    .restart local v0    # "menuIcon":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .restart local v1    # "menuText":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_1
    const v2, 0x7f071050

    goto :goto_0

    .line 525
    :cond_2
    const v2, 0x7f07065c

    goto :goto_1
.end method

.method private updateViewForGroupConversation(Z)V
    .locals 7
    .param p1, "skypeGroupConversation"    # Z

    .prologue
    const v6, 0x7f020180

    const v5, 0x7f020125

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 487
    if-eqz p1, :cond_1

    .line 488
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->gamertagTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getCurrentTopicName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 489
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesPeopleIconsContainer:Landroid/widget/TableLayout;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 490
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getMemberPics()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateConversationIcons(Ljava/util/List;)V

    .line 491
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 492
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 493
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getMemberOnlineCount()I

    move-result v0

    .line 494
    .local v0, "onlineCount":I
    if-lez v0, :cond_0

    .line 495
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->onlineCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070661

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 511
    .end local v0    # "onlineCount":I
    :cond_0
    :goto_0
    return-void

    .line 498
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesPeopleIconsContainer:Landroid/widget/TableLayout;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 500
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSelectedSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 501
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSelectedSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v3

    iget-boolean v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-eqz v3, :cond_3

    .line 502
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSenderGamerPicUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v6, v6}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 508
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 509
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSelectedSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isUserOnline(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z

    move-result v4

    if-eqz v4, :cond_4

    :goto_2
    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0

    .line 504
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->imageTile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSenderGamerPicUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v5, v5}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    goto :goto_1

    :cond_4
    move v1, v2

    .line 509
    goto :goto_2
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 578
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    return-object v0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 344
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->slideIn:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->slideOut:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 351
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 355
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 340
    return-void
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 722
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 724
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 725
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateDropdown(Z)V

    .line 735
    :goto_0
    return-void

    .line 726
    :cond_0
    if-eqz v0, :cond_1

    .line 727
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->goBack()V

    goto :goto_0

    .line 730
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GoBack()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 731
    :catch_0
    move-exception v1

    .line 732
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->TAG:Ljava/lang/String;

    const-string v3, "onBackPressed failed to navigate back"

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 365
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onPause()V

    .line 366
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateMessageBody()V

    .line 367
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 371
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onResume()V

    .line 372
    return-void
.end method

.method public onScreenTouchEvent()V
    .locals 1

    .prologue
    .line 718
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateDropdown(Z)V

    .line 719
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 198
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStart()V

    .line 199
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->saveAndAdjustSoftInputAdjustMode()V

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->detailListLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->layoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSelectedSenderService()Z

    move-result v0

    if-nez v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setVisibility(I)V

    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_2

    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 220
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->setSendButtonEnabled(Z)V

    .line 226
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->textWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->requestFocus()Z

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showKeyboard()V

    .line 232
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuArrow:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_3

    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuArrow:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->setMenuItemListenersAndVisibility()V

    .line 238
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 257
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 258
    return-void

    .line 222
    :cond_4
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->setSendButtonEnabled(Z)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 376
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 378
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 379
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->layoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 382
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 383
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_2

    .line 387
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->textWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 390
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_3

    .line 391
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 393
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewAllMembersView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_4

    .line 394
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewAllMembersView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 396
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->addPeopleView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_5

    .line 397
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->addPeopleView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 399
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->muteNotificationView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_6

    .line 400
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->muteNotificationView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 402
    :cond_6
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->renameConversationView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_7

    .line 403
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->renameConversationView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 405
    :cond_7
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->leaveConversationView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_8

    .line 406
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->leaveConversationView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    :cond_8
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewProfile:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_9

    .line 409
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewProfile:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 411
    :cond_9
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->createGroupConversation:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_a

    .line 412
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->createGroupConversation:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 415
    :cond_a
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->restoreSoftInputAdjustMode()V

    .line 416
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 417
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStop()V

    .line 418
    return-void
.end method

.method public updateDropdown(Z)V
    .locals 2
    .param p1, "shouldShow"    # Z

    .prologue
    .line 326
    if-nez p1, :cond_0

    .line 328
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuArrow:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v1, 0x7f070ef3

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 329
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->slideIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 336
    :goto_0
    return-void

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuArrow:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v1, 0x7f070ef6

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 333
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 334
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messagesMenuView:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->slideOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public updateMessageBody()V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->setMessageBody(Ljava/lang/String;)V

    .line 361
    :cond_0
    return-void
.end method

.method public updateViewOverride()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 422
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isBusy()Z

    move-result v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateLoadingIndicator(Z)V

    .line 423
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getListState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 425
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSkypeMessageList()Ljava/util/List;

    move-result-object v0

    .line 426
    .local v0, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v4, :cond_3

    if-eqz v0, :cond_3

    .line 427
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageList:Ljava/util/List;

    if-ne v0, v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->shouldForceUpdateAdapter()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 428
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->clearForceUpdateAdapter()V

    .line 429
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageList:Ljava/util/List;

    .line 430
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getCount()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    .line 431
    .local v1, "oldBottom":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->clear()V

    .line 432
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageList:Ljava/util/List;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 434
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->shouldScrollToBottom()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->getLastVisiblePosition()I

    move-result v4

    if-ne v4, v1, :cond_2

    .line 435
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->clearShouldScrollToBottom()V

    .line 436
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setSelection(I)V

    .line 438
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 444
    .end local v1    # "oldBottom":I
    :cond_3
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getMessageBody()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->newMessage:Ljava/lang/String;

    .line 446
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v4, :cond_5

    .line 447
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->oldMessage:Landroid/text/Editable;

    .line 449
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->oldMessage:Landroid/text/Editable;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->oldMessage:Landroid/text/Editable;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->newMessage:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 450
    :cond_4
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->newMessage:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 454
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->gamertagTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getGamerTagTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 455
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeGroupConversation()Z

    move-result v4

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateViewForGroupConversation(Z)V

    .line 456
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getMemberPics()Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateConversationIcons(Ljava/util/List;)V

    .line 457
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isConversationMuted()Z

    move-result v4

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateMuteMenuItem(Z)V

    .line 458
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeServiceConversation()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 459
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->disableMenuOptions()V

    .line 462
    :cond_6
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getTypingData()Ljava/util/List;

    move-result-object v2

    .line 463
    .local v2, "typingData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 464
    .local v3, "typingDataSize":I
    packed-switch v3, :pswitch_data_0

    .line 478
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070310

    new-array v7, v10, [Ljava/lang/Object;

    .line 479
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    .line 478
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 483
    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->openHoverChat:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->shouldShowHoverChatOption()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 484
    return-void

    .line 440
    .end local v2    # "typingData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "typingDataSize":I
    :cond_7
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 466
    .restart local v2    # "typingData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "typingDataSize":I
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 469
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070320

    new-array v7, v10, [Ljava/lang/Object;

    .line 470
    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v7, v9

    .line 469
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 473
    :pswitch_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070323

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    .line 474
    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v7, v9

    .line 475
    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v7, v10

    .line 473
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 464
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
