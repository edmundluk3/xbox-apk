.class public Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;
.super Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;
.source "GamerscoreLeaderboardPageViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;",
        "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader",
        "<",
        "Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final FOCUS_ITEM_COUNT:I = 0x1

.field private static final PAGE_ITEM_COUNT:I = 0x5


# instance fields
.field private final listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ListenerManager",
            "<",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 28
    new-instance v0, Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ListenerManager;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->initializeAdapter()V

    .line 30
    return-void
.end method

.method private initializeAdapter()V
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGamerscoreLeaderboardAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 39
    return-void
.end method

.method static synthetic lambda$updateLeaderboard$0(Ljava/util/List;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 0
    .param p0, "newItems"    # Ljava/util/List;
    .param p1, "collectionAction"    # Lcom/microsoft/xbox/toolkit/generics/Action;

    .prologue
    .line 83
    invoke-interface {p1, p0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public addOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "listener":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/Collection<+Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ListenerManager;->addListener(Ljava/lang/Object;)V

    .line 109
    return-void
.end method

.method public hasMoreItemsToLoad()Z
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->hasLoadedAllData()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->load(Z)V

    .line 45
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->hasData()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, p1, v2, v3}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->loadMoreAsync(ZJ)V

    .line 51
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_3

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->hasFocusData()Z

    move-result v0

    if-nez v0, :cond_3

    .line 52
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->loadFocusAsync(ZLjava/lang/String;J)V

    .line 55
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->updateLeaderboard()V

    .line 56
    return-void
.end method

.method public loadMoreItemsAsync()V
    .locals 4

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    const/4 v1, 0x0

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->loadMoreAsync(ZJ)V

    .line 104
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->initializeAdapter()V

    .line 35
    return-void
.end method

.method public removeOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "listener":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/Collection<+Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ListenerManager;->removeListener(Ljava/lang/Object;)V

    .line 114
    return-void
.end method

.method protected updateLeaderboard()V
    .locals 13

    .prologue
    .line 60
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->userDataMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->hasData()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->hasFocusData()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 61
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v11

    .line 62
    .local v11, "selfXuid":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getData()Ljava/util/List;

    move-result-object v9

    .line 63
    .local v9, "leaderboardData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getFocusData()Ljava/util/List;

    move-result-object v8

    .line 66
    .local v8, "focusData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v10, "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->leaderboardList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v9, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;

    .line 68
    .local v7, "entry":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;
    iget-object v2, v7, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    invoke-static {v2, v11}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 70
    .local v6, "isSelf":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->userDataMap:Ljava/util/Map;

    iget-object v3, v7, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 71
    .local v1, "userSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v1, :cond_1

    .line 72
    new-instance v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    iget-wide v2, v7, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->rank:J

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->getGamerscore()J

    move-result-wide v4

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;JJZ)V

    .line 73
    .local v0, "newItem":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    .end local v0    # "newItem":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    :cond_1
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->getGamerscore()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->topMonthlyGamerscore:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->topMonthlyGamerscore:J

    .line 78
    if-eqz v6, :cond_0

    .line 79
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->getGamerscore()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->selfMonthlyGamerscore:J

    goto :goto_1

    .line 61
    .end local v1    # "userSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v6    # "isSelf":Z
    .end local v7    # "entry":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;
    .end local v8    # "focusData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    .end local v9    # "leaderboardData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    .end local v10    # "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;"
    .end local v11    # "selfXuid":Ljava/lang/String;
    :cond_2
    const-string v11, ""

    goto :goto_0

    .line 83
    .restart local v8    # "focusData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    .restart local v9    # "leaderboardData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    .restart local v10    # "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;"
    .restart local v11    # "selfXuid":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel$$Lambda$1;->lambdaFactory$(Ljava/util/List;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ListenerManager;->run(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 84
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->leaderboardList:Ljava/util/List;

    invoke-interface {v2, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 86
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->selfMonthlyGamerscore:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 87
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->getGamerscore()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->selfMonthlyGamerscore:J

    .line 90
    :cond_4
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 92
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->updateAdapter()V

    .line 94
    .end local v8    # "focusData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    .end local v9    # "leaderboardData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    .end local v10    # "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;"
    .end local v11    # "selfXuid":Ljava/lang/String;
    :cond_5
    return-void
.end method
