.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression;
.super Ljava/lang/Object;
.source "UTCImpression.java"


# static fields
.field public static final IMPRESSION_TIMER_INTERVAL:I = 0x3e8

.field public static final IMPRESSION_WAIT_TIME:I = 0x1f4

.field private static PART_C_VERSION:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    sput v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression;->PART_C_VERSION:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    .prologue
    .line 23
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression;->getImpressionContentSource(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .prologue
    .line 23
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression;->getImpressionContentType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 23
    sget v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression;->PART_C_VERSION:I

    return v0
.end method

.method private static getImpressionContentSource(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;)Ljava/lang/String;
    .locals 2
    .param p0, "ContentSource"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    .prologue
    .line 117
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$2;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$AuthorInfo$AuthorType:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 126
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 119
    :pswitch_0
    const-string v0, "User"

    goto :goto_0

    .line 121
    :pswitch_1
    const-string v0, "TitleUser"

    goto :goto_0

    .line 123
    :pswitch_2
    const-string v0, "PageUser"

    goto :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static getImpressionContentType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)Ljava/lang/String;
    .locals 2
    .param p0, "ContentType"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .prologue
    .line 78
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$2;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 112
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 80
    :pswitch_0
    const-string v0, "ChallengeOrAchievement"

    goto :goto_0

    .line 82
    :pswitch_1
    const-string v0, "UNKNOWN"

    goto :goto_0

    .line 84
    :pswitch_2
    const-string v0, "UNKNOWN"

    goto :goto_0

    .line 86
    :pswitch_3
    const-string v0, "UNKNOWN"

    goto :goto_0

    .line 88
    :pswitch_4
    const-string v0, "GameClip"

    goto :goto_0

    .line 90
    :pswitch_5
    const-string v0, "Followed"

    goto :goto_0

    .line 92
    :pswitch_6
    const-string v0, "Broadcast"

    goto :goto_0

    .line 94
    :pswitch_7
    const-string v0, "Broadcast"

    goto :goto_0

    .line 96
    :pswitch_8
    const-string v0, "TextEntry"

    goto :goto_0

    .line 98
    :pswitch_9
    const-string v0, "GamertagChanged"

    goto :goto_0

    .line 100
    :pswitch_a
    const-string v0, "Screenshot"

    goto :goto_0

    .line 102
    :pswitch_b
    const-string v0, "SocialRecommendationFeedContainer"

    goto :goto_0

    .line 104
    :pswitch_c
    const-string v0, "GenericFeedContainer"

    goto :goto_0

    .line 106
    :pswitch_d
    const-string v0, "UNKNOWN"

    goto :goto_0

    .line 108
    :pswitch_e
    const-string v0, "LegacyAchievement"

    goto :goto_0

    .line 110
    :pswitch_f
    const-string v0, "UNKNOWN"

    goto :goto_0

    .line 78
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public static trackImpressionEvents(Ljava/lang/String;JLjava/util/ArrayList;)V
    .locals 1
    .param p0, "pageName"    # Ljava/lang/String;
    .param p1, "resolutionHeight"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p3, "profileRecentItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$1;

    invoke-direct {v0, p3, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$1;-><init>(Ljava/util/ArrayList;Ljava/lang/String;J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 75
    return-void
.end method
