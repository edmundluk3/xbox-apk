.class Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "EDSV2GameDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BigCatAddonsLookUpRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$1;

    .prologue
    .line 223
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 231
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductAddOns(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 232
    .local v0, "addons":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v2, v0}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductsReducedInfoFromList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getListFromBigCatProduct(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 242
    const-wide/16 v0, 0xfaf

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 237
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->onBrowseMediaItemListCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 238
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 227
    return-void
.end method
