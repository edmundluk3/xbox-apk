.class public Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;
.super Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;
.source "TvMyChannelsScreenAdapter.java"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)V
    .locals 11
    .param p1, "model"    # Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    .prologue
    const/4 v10, 0x1

    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)V

    .line 29
    const v7, 0x7f0e0b27

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->screenBody:Landroid/view/View;

    .line 30
    const v7, 0x7f0e0b28

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 31
    const-string v7, "TvMyChannelsScreenAdapter"

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->diagTag:Ljava/lang/String;

    .line 33
    const v7, 0x7f0e0b29

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/xle/ui/EPGView;

    sget-object v8, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->FAVORITES:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {p0, v7, v8}, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->setEPGView(Lcom/microsoft/xbox/xle/ui/EPGView;Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;)V

    .line 36
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 37
    .local v2, "inflator":Landroid/view/LayoutInflater;
    const v7, 0x7f030240

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    const/4 v9, 0x0

    invoke-virtual {v2, v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 38
    .local v0, "header":Landroid/view/ViewGroup;
    sget-object v7, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedDrawable(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 39
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v7, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->setHeaderView(Landroid/view/View;)V

    .line 41
    const v7, 0x7f0e0b06

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->providerBar:Landroid/widget/LinearLayout;

    .line 43
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/ui/EPGView;->createExtraRow()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    move-result-object v4

    .line 44
    .local v4, "row":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v7

    iput-boolean v10, v7, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mRequestDisallowInterceptTouchEventonTouchDown:Z

    .line 45
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->providerBar:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 46
    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->setAlpha(F)V

    .line 47
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v7, v4}, Lcom/microsoft/xbox/xle/ui/EPGView;->addExtraRow(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;)V

    .line 48
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v7, v4}, Lcom/microsoft/xbox/xle/ui/EPGView;->createDayView(Landroid/view/ViewGroup;)V

    .line 50
    const v7, 0x7f0e0b08

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 51
    const v7, 0x7f0e0b09

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->providerChevron:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 52
    sget-object v7, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 53
    sget-object v7, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->providerChevron:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 55
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->screenBody:Landroid/view/View;

    const v8, 0x7f0e0b22

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->errorStateProviderBar:Landroid/widget/LinearLayout;

    .line 56
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->errorStateProviderBar:Landroid/widget/LinearLayout;

    const v8, 0x7f0e0b23

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->errorStateProviderName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 58
    const v7, 0x7f0e0b07

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->providerLogo:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 61
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->updateProviderName()V

    .line 62
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->updateProviderBarState()V

    .line 65
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/ui/EPGView;->createExtraRow()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    move-result-object v5

    .line 66
    .local v5, "timeLine":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 70
    .local v3, "resources":Landroid/content/res/Resources;
    const v7, 0x7f090584

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v6, v7

    .line 71
    .local v6, "timeLineHeight":I
    const v7, 0x7f0902c8

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v1, v7

    .line 72
    .local v1, "horizontalMargin":I
    new-instance v7, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v8, -0x1

    invoke-direct {v7, v8, v6}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    invoke-virtual {v0, v5, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 74
    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getX()F

    move-result v7

    int-to-float v8, v1

    sub-float/2addr v7, v8

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->setX(F)V

    .line 75
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v7, v5}, Lcom/microsoft/xbox/xle/ui/EPGView;->addExtraRow(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;)V

    .line 78
    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v7

    iput-boolean v10, v7, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mRequestDisallowInterceptTouchEventonTouchDown:Z

    .line 79
    return-void
.end method


# virtual methods
.method public updateViewOverride()V
    .locals 5

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->updateViewLoadingIndicator()V

    .line 98
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    if-eqz v2, :cond_1

    .line 102
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter$1;->$SwitchMap$com$microsoft$xbox$toolkit$network$ListState:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 105
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getHasData()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 106
    const/4 v0, 0x0

    .line 130
    .local v0, "panelIndex":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v2

    if-eq v2, v0, :cond_0

    .line 131
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 134
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->diagTag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Switch to view "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .end local v0    # "panelIndex":I
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getProviderName()Ljava/lang/String;

    move-result-object v3

    if-eq v2, v3, :cond_2

    .line 138
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getProviderName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    if-eqz v2, :cond_5

    .line 142
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    .line 144
    .local v1, "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v1, v2, :cond_3

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v1, v2, :cond_4

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getHasData()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 145
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->setModel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V

    .line 149
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->updateHeaderVisibility()V

    .line 151
    .end local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    :cond_5
    return-void

    .line 108
    :cond_6
    const/4 v0, 0x1

    .line 111
    .restart local v0    # "panelIndex":I
    goto :goto_0

    .line 114
    .end local v0    # "panelIndex":I
    :pswitch_0
    const/4 v0, 0x4

    .line 115
    .restart local v0    # "panelIndex":I
    goto :goto_0

    .line 117
    .end local v0    # "panelIndex":I
    :pswitch_1
    const/4 v0, 0x0

    .line 118
    .restart local v0    # "panelIndex":I
    goto :goto_0

    .line 120
    .end local v0    # "panelIndex":I
    :pswitch_2
    const/4 v0, 0x3

    .restart local v0    # "panelIndex":I
    goto :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
