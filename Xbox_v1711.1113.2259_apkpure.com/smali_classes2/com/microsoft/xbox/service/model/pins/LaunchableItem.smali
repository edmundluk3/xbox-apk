.class public interface abstract Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
.super Ljava/lang/Object;
.source "LaunchableItem.java"


# static fields
.field public static final APPX_LAUNCH_URI_FORMAT:Ljava/lang/String; = "appx:%1$s!%2$s"


# virtual methods
.method public abstract getBoxArtBackgroundColor()I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end method

.method public abstract getContentType()Ljava/lang/String;
.end method

.method public abstract getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
.end method

.method public abstract getImageUrl()Ljava/lang/String;
.end method

.method public abstract getIsMusicPlayList()Z
.end method

.method public abstract getIsTVChannel()Z
.end method

.method public abstract getIsWebLink()Z
.end method

.method public abstract getItemId()Ljava/lang/String;
.end method

.method public abstract getLaunchUri()Ljava/lang/String;
.end method

.method public abstract getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
.end method

.method public abstract getProviderMediaId()Ljava/lang/String;
.end method

.method public abstract getProviderName()Ljava/lang/String;
.end method

.method public abstract getProviderTitleId()J
.end method

.method public abstract getProviderTitleIdString()Ljava/lang/String;
.end method

.method public abstract getShouldShowBackgroundColor()Z
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract hasCompanion()Z
.end method

.method public abstract hasDetails()Z
.end method

.method public abstract hasProvider()Z
.end method

.method public abstract isDLC()Z
.end method
