.class public abstract Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;
.super Ljava/lang/Object;
.source "OOBEViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "InstantOnChangedIntent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Z)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;
    .locals 1
    .param p0, "enabled"    # Z

    .prologue
    .line 64
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_InstantOnChangedIntent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_InstantOnChangedIntent;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method public abstract enabled()Z
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
