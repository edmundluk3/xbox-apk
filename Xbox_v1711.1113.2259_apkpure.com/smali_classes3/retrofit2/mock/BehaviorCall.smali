.class final Lretrofit2/mock/BehaviorCall;
.super Ljava/lang/Object;
.source "BehaviorCall.java"

# interfaces
.implements Lretrofit2/Call;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lretrofit2/Call",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final backgroundExecutor:Ljava/util/concurrent/ExecutorService;

.field final behavior:Lretrofit2/mock/NetworkBehavior;

.field volatile canceled:Z

.field final delegate:Lretrofit2/Call;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit2/Call",
            "<TT;>;"
        }
    .end annotation
.end field

.field private volatile executed:Z

.field private volatile task:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lretrofit2/mock/NetworkBehavior;Ljava/util/concurrent/ExecutorService;Lretrofit2/Call;)V
    .locals 0
    .param p1, "behavior"    # Lretrofit2/mock/NetworkBehavior;
    .param p2, "backgroundExecutor"    # Ljava/util/concurrent/ExecutorService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/mock/NetworkBehavior;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lretrofit2/Call",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lretrofit2/mock/BehaviorCall;, "Lretrofit2/mock/BehaviorCall<TT;>;"
    .local p3, "delegate":Lretrofit2/Call;, "Lretrofit2/Call<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lretrofit2/mock/BehaviorCall;->behavior:Lretrofit2/mock/NetworkBehavior;

    .line 41
    iput-object p2, p0, Lretrofit2/mock/BehaviorCall;->backgroundExecutor:Ljava/util/concurrent/ExecutorService;

    .line 42
    iput-object p3, p0, Lretrofit2/mock/BehaviorCall;->delegate:Lretrofit2/Call;

    .line 43
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .local p0, "this":Lretrofit2/mock/BehaviorCall;, "Lretrofit2/mock/BehaviorCall<TT;>;"
    const/4 v1, 0x1

    .line 139
    iput-boolean v1, p0, Lretrofit2/mock/BehaviorCall;->canceled:Z

    .line 140
    iget-object v0, p0, Lretrofit2/mock/BehaviorCall;->task:Ljava/util/concurrent/Future;

    .line 141
    .local v0, "task":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    if-eqz v0, :cond_0

    .line 142
    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 144
    :cond_0
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lretrofit2/mock/BehaviorCall;, "Lretrofit2/mock/BehaviorCall<TT;>;"
    invoke-virtual {p0}, Lretrofit2/mock/BehaviorCall;->clone()Lretrofit2/Call;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lretrofit2/Call;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/Call",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lretrofit2/mock/BehaviorCall;, "Lretrofit2/mock/BehaviorCall<TT;>;"
    new-instance v0, Lretrofit2/mock/BehaviorCall;

    iget-object v1, p0, Lretrofit2/mock/BehaviorCall;->behavior:Lretrofit2/mock/NetworkBehavior;

    iget-object v2, p0, Lretrofit2/mock/BehaviorCall;->backgroundExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v3, p0, Lretrofit2/mock/BehaviorCall;->delegate:Lretrofit2/Call;

    invoke-interface {v3}, Lretrofit2/Call;->clone()Lretrofit2/Call;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lretrofit2/mock/BehaviorCall;-><init>(Lretrofit2/mock/NetworkBehavior;Ljava/util/concurrent/ExecutorService;Lretrofit2/Call;)V

    return-object v0
.end method

.method public enqueue(Lretrofit2/Callback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lretrofit2/mock/BehaviorCall;, "Lretrofit2/mock/BehaviorCall<TT;>;"
    .local p1, "callback":Lretrofit2/Callback;, "Lretrofit2/Callback<TT;>;"
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "callback == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    monitor-enter p0

    .line 58
    :try_start_0
    iget-boolean v0, p0, Lretrofit2/mock/BehaviorCall;->executed:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already executed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 59
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lretrofit2/mock/BehaviorCall;->executed:Z

    .line 60
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    iget-object v0, p0, Lretrofit2/mock/BehaviorCall;->backgroundExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lretrofit2/mock/BehaviorCall$1;

    invoke-direct {v1, p0, p1}, Lretrofit2/mock/BehaviorCall$1;-><init>(Lretrofit2/mock/BehaviorCall;Lretrofit2/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lretrofit2/mock/BehaviorCall;->task:Ljava/util/concurrent/Future;

    .line 104
    return-void
.end method

.method public execute()Lretrofit2/Response;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/Response",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    .local p0, "this":Lretrofit2/mock/BehaviorCall;, "Lretrofit2/mock/BehaviorCall<TT;>;"
    new-instance v5, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v5}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 112
    .local v5, "responseRef":Ljava/util/concurrent/atomic/AtomicReference;, "Ljava/util/concurrent/atomic/AtomicReference<Lretrofit2/Response<TT;>;>;"
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 113
    .local v2, "failureRef":Ljava/util/concurrent/atomic/AtomicReference;, "Ljava/util/concurrent/atomic/AtomicReference<Ljava/lang/Throwable;>;"
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v6, 0x1

    invoke-direct {v3, v6}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 114
    .local v3, "latch":Ljava/util/concurrent/CountDownLatch;
    new-instance v6, Lretrofit2/mock/BehaviorCall$2;

    invoke-direct {v6, p0, v5, v3, v2}, Lretrofit2/mock/BehaviorCall$2;-><init>(Lretrofit2/mock/BehaviorCall;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/CountDownLatch;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-virtual {p0, v6}, Lretrofit2/mock/BehaviorCall;->enqueue(Lretrofit2/Callback;)V

    .line 126
    :try_start_0
    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lretrofit2/Response;

    .line 131
    .local v4, "response":Lretrofit2/Response;, "Lretrofit2/Response<TT;>;"
    if-eqz v4, :cond_0

    return-object v4

    .line 127
    .end local v4    # "response":Lretrofit2/Response;, "Lretrofit2/Response<TT;>;"
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Ljava/lang/InterruptedException;
    new-instance v6, Ljava/io/IOException;

    const-string v7, "canceled"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 132
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v4    # "response":Lretrofit2/Response;, "Lretrofit2/Response<TT;>;"
    :cond_0
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    .line 133
    .local v1, "failure":Ljava/lang/Throwable;
    instance-of v6, v1, Ljava/lang/RuntimeException;

    if-eqz v6, :cond_1

    check-cast v1, Ljava/lang/RuntimeException;

    .end local v1    # "failure":Ljava/lang/Throwable;
    throw v1

    .line 134
    .restart local v1    # "failure":Ljava/lang/Throwable;
    :cond_1
    instance-of v6, v1, Ljava/io/IOException;

    if-eqz v6, :cond_2

    check-cast v1, Ljava/io/IOException;

    .end local v1    # "failure":Ljava/lang/Throwable;
    throw v1

    .line 135
    .restart local v1    # "failure":Ljava/lang/Throwable;
    :cond_2
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 147
    .local p0, "this":Lretrofit2/mock/BehaviorCall;, "Lretrofit2/mock/BehaviorCall<TT;>;"
    iget-boolean v0, p0, Lretrofit2/mock/BehaviorCall;->canceled:Z

    return v0
.end method

.method public declared-synchronized isExecuted()Z
    .locals 1

    .prologue
    .line 107
    .local p0, "this":Lretrofit2/mock/BehaviorCall;, "Lretrofit2/mock/BehaviorCall<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lretrofit2/mock/BehaviorCall;->executed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public request()Lokhttp3/Request;
    .locals 1

    .prologue
    .line 51
    .local p0, "this":Lretrofit2/mock/BehaviorCall;, "Lretrofit2/mock/BehaviorCall<TT;>;"
    iget-object v0, p0, Lretrofit2/mock/BehaviorCall;->delegate:Lretrofit2/Call;

    invoke-interface {v0}, Lretrofit2/Call;->request()Lokhttp3/Request;

    move-result-object v0

    return-object v0
.end method
