.class public Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;
.super Ljava/lang/Object;
.source "ConnectDialogViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;,
        Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field public static final DELAY_AUTODISMISS_MS:J = 0x190L

.field private static final TAG:Ljava/lang/String; = "ConnectDialogViewModel"


# instance fields
.field private discoverConsoleTask:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

.field private displayedAvailableConsole:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;"
        }
    .end annotation
.end field

.field private final expandedConsoles:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;"
        }
    .end annotation
.end field

.field private internalState:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

.field private isConnecting:Z

.field private isPoweringOnOff:Z

.field private isSuccess:Z

.field private lastResult:Lcom/microsoft/xbox/toolkit/AsyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;"
        }
    .end annotation
.end field

.field private model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

.field private onConnectHandler:Ljava/lang/Runnable;

.field private triedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

.field private view:Lcom/microsoft/xbox/toolkit/IViewUpdate;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/IViewUpdate;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/toolkit/IViewUpdate;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->NORMAL:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->internalState:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    .line 59
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->expandedConsoles:Ljava/util/HashSet;

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->triedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 93
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    .line 94
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getInstance()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getDiscoveredConsoles()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->displayedAvailableConsole:Ljava/util/List;

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;)Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->onConnectHandler:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->onConnectHandler:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isPoweringOnOff:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;)Lcom/microsoft/xbox/toolkit/IViewUpdate;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    return-object v0
.end method

.method private addTempConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 5
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 172
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 173
    .local v2, "newList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/model/ConsoleData;>;"
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    const/4 v1, 0x0

    .line 175
    .local v1, "found":Z
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->displayedAvailableConsole:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 176
    .local v0, "consoledata":Lcom/microsoft/xbox/xle/model/ConsoleData;
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 178
    const/4 v1, 0x1

    goto :goto_0

    .line 182
    .end local v0    # "consoledata":Lcom/microsoft/xbox/xle/model/ConsoleData;
    :cond_1
    if-nez v1, :cond_2

    .line 183
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->displayedAvailableConsole:Ljava/util/List;

    .line 184
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lcom/microsoft/xbox/toolkit/IViewUpdate;->updateView(Ljava/lang/Object;)V

    .line 186
    :cond_2
    return-void
.end method

.method private connectInternal(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 3
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    const/4 v2, 0x1

    .line 189
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/SessionModel;->setCurrentConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 190
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isConnecting:Z

    .line 191
    const-string v0, "ConnectDialog"

    const-string v1, "Connect starts"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddressManuallyEntered()Z

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/microsoft/xbox/service/model/SessionModel;->connectToConsole(ZZ)V

    .line 193
    return-void
.end method

.method private handleSessionState()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 325
    const-string v0, "ConnectDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "update session state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 366
    :goto_0
    :pswitch_0
    return-void

    .line 328
    :pswitch_1
    const-string v0, "ConnectDialog"

    const-string v1, "session state connected"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isConnecting:Z

    .line 330
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isSuccess:Z

    .line 331
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->triedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->triedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->addConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 333
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->triedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->updateAutoConnectState(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 334
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->triedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isAutoConnect()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->triedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->setLastConnectedConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 339
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$3;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;)V

    const-wide/16 v2, 0x190

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 353
    :pswitch_2
    const-string v0, "ConnectDialog"

    const-string v1, "Connection failed, force update list"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isConnecting:Z

    .line 355
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isSuccess:Z

    .line 356
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$4;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 326
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 309
    const-string v0, "ConnectDialogViewModel"

    const-string v1, "close the dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->triedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    if-nez v0, :cond_0

    .line 313
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->onConnectCancelled()V

    .line 315
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->dismissDialog()V

    .line 317
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->onConnectHandler:Ljava/lang/Runnable;

    .line 318
    return-void
.end method

.method public connect(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 4
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 196
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->internalState:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->DISCONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    if-eq v1, v2, :cond_3

    .line 197
    const-string v1, "ConnectDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connect to console "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->getInstance()Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismiss()V

    .line 199
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->triedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 200
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->addTempConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 202
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    .line 204
    .local v0, "sm":Lcom/microsoft/xbox/service/model/SessionModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 206
    const-string v1, "ConnectDialog"

    const-string v2, "Need to disconnect first, wait for disconnect"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->DISCONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->internalState:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    .line 208
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;->MANUAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SessionModel;->leaveSession(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V

    .line 217
    .end local v0    # "sm":Lcom/microsoft/xbox/service/model/SessionModel;
    :goto_0
    return-void

    .line 209
    .restart local v0    # "sm":Lcom/microsoft/xbox/service/model/SessionModel;
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 210
    :cond_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->connectInternal(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    goto :goto_0

    .line 212
    :cond_2
    const-string v1, "ConnectDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "already connected to console "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 215
    .end local v0    # "sm":Lcom/microsoft/xbox/service/model/SessionModel;
    :cond_3
    const-string v1, "ConnectDialog"

    const-string v2, "already disconnecting"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public disconnect(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 4
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    const/4 v3, 0x0

    .line 225
    const-string v0, "ConnectDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disconnect console "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->getInstance()Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismiss()V

    .line 227
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 228
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 230
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 231
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isConnecting:Z

    .line 232
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getInstance()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->setLastConnectedConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 233
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getInstance()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->updateAutoConnectState(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 235
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;->MANUAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SessionModel;->leaveSession(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V

    .line 236
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/SessionModel;->setCurrentConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 237
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 248
    :cond_1
    :goto_0
    return-void

    .line 246
    :cond_2
    const-string v0, "ConnectDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ignore disconnect request because console is not connected nor connecting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected dismissDialog()V
    .locals 1

    .prologue
    .line 321
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissConnectDialog()V

    .line 322
    return-void
.end method

.method public getConsoleConnected(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z
    .locals 2
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 147
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 148
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 149
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 152
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getConsoleState(Lcom/microsoft/xbox/xle/model/ConsoleData;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    .locals 1
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 156
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->getConsoleState(Lcom/microsoft/xbox/xle/model/ConsoleData;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    move-result-object v0

    return-object v0
.end method

.method public getDiscoveredConsoles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->displayedAvailableConsole:Ljava/util/List;

    return-object v0
.end method

.method public getIsConnecting()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isConnecting:Z

    return v0
.end method

.method public getIsPoweringOnOff()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isPoweringOnOff:Z

    return v0
.end method

.method public getIsSuccess()Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isSuccess:Z

    return v0
.end method

.method public getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->lastResult:Lcom/microsoft/xbox/toolkit/AsyncResult;

    return-object v0
.end method

.method public getTryingConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->triedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    return-object v0
.end method

.method public isExpanded(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z
    .locals 1
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->expandedConsoles:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 107
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 108
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 100
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->loadAsync(Z)V

    .line 103
    return-void
.end method

.method public powerOff(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 4
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 252
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v1

    iget-object v2, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->powerOff(Ljava/lang/String;)V

    .line 253
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, p1, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;Ljava/util/concurrent/ExecutorService;Lcom/microsoft/xbox/xle/model/ConsoleData;Z)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->discoverConsoleTask:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    .line 254
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->discoverConsoleTask:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->execute()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 259
    :goto_0
    return-void

    .line 255
    :catch_0
    move-exception v0

    .line 256
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "ConnectDialogViewModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to power off device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 257
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    const v2, 0x7f070362

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0
.end method

.method public powerOn(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 4
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 263
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v1

    iget-object v2, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->powerOn(Ljava/lang/String;)V

    .line 264
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    const/4 v3, 0x1

    invoke-direct {v1, p0, v2, p1, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;Ljava/util/concurrent/ExecutorService;Lcom/microsoft/xbox/xle/model/ConsoleData;Z)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->discoverConsoleTask:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    .line 265
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->discoverConsoleTask:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->execute()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    :goto_0
    return-void

    .line 266
    :catch_0
    move-exception v0

    .line 267
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "ConnectDialogViewModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to power on device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 268
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    const v2, 0x7f070363

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0
.end method

.method public removeDiscoveredConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 1
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->displayedAvailableConsole:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->removeDiscoveredConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 121
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 128
    return-void
.end method

.method public setExpanded(Lcom/microsoft/xbox/xle/model/ConsoleData;Z)V
    .locals 1
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;
    .param p2, "expanded"    # Z

    .prologue
    .line 164
    if-eqz p2, :cond_0

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->expandedConsoles:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 169
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->expandedConsoles:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setOnConnectHandler(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "onConnectHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->onConnectHandler:Ljava/lang/Runnable;

    .line 221
    return-void
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->lastResult:Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 275
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 276
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$5;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 305
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/microsoft/xbox/toolkit/IViewUpdate;->updateView(Ljava/lang/Object;)V

    .line 306
    return-void

    .line 278
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$5;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$ConnectDialogViewModel$InteralState:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->internalState:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 280
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    .line 281
    .local v1, "sm":Lcom/microsoft/xbox/service/model/SessionModel;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v2

    if-nez v2, :cond_0

    .line 282
    const-string v2, "ConnectDialog"

    const-string v3, "disconnect waitor done, now connect to new console"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->NORMAL:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->internalState:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    .line 284
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->triedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->connectInternal(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    goto :goto_0

    .line 288
    .end local v1    # "sm":Lcom/microsoft/xbox/service/model/SessionModel;
    :pswitch_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->handleSessionState()V

    goto :goto_0

    .line 293
    :pswitch_3
    const-string v2, "ConnectDialog"

    const-string v3, "new console discovery received, rebuild "

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->displayedAvailableConsole:Ljava/util/List;

    .line 295
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getDiscoveredConsoles()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 296
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->model:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getDiscoveredConsoles()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 297
    .local v0, "console":Lcom/microsoft/xbox/xle/model/ConsoleData;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->displayedAvailableConsole:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 302
    .end local v0    # "console":Lcom/microsoft/xbox/xle/model/ConsoleData;
    :cond_1
    const-string v2, "ConnectDialog"

    const-string v3, "update is not final. ignore"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 278
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
