.class Lcom/microsoft/xbox/xle/ui/ConnectButton$2;
.super Ljava/lang/Object;
.source "ConnectButton.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/ConnectButton;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/ConnectButton;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/ConnectButton;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/ConnectButton;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/ConnectButton$2;->this$0:Lcom/microsoft/xbox/xle/ui/ConnectButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawableStateChanged()V
    .locals 9

    .prologue
    .line 60
    const/4 v2, 0x0

    .line 62
    .local v2, "connectionState":I
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/ConnectButton$2;->this$0:Lcom/microsoft/xbox/xle/ui/ConnectButton;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->getDrawableState()[I

    move-result-object v4

    .line 63
    .local v4, "drawableState":[I
    array-length v7, v4

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v7, :cond_3

    aget v5, v4, v6

    .line 64
    .local v5, "state":I
    const v8, 0x7f01011b

    if-ne v8, v5, :cond_1

    .line 65
    const/4 v2, 0x0

    .line 63
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 66
    :cond_1
    const v8, 0x7f010118

    if-ne v8, v5, :cond_2

    .line 67
    const/4 v2, 0x1

    .line 69
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/ConnectButton$2;->this$0:Lcom/microsoft/xbox/xle/ui/ConnectButton;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->access$100(Lcom/microsoft/xbox/xle/ui/ConnectButton;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    .line 70
    .local v0, "background":Landroid/graphics/drawable/StateListDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 71
    .local v3, "current":Landroid/graphics/drawable/Drawable;
    instance-of v8, v3, Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v8, :cond_0

    move-object v1, v3

    .line 72
    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    .line 73
    .local v1, "connectingAnimation":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    goto :goto_1

    .line 75
    .end local v0    # "background":Landroid/graphics/drawable/StateListDrawable;
    .end local v1    # "connectingAnimation":Landroid/graphics/drawable/AnimationDrawable;
    .end local v3    # "current":Landroid/graphics/drawable/Drawable;
    :cond_2
    const v8, 0x7f010117

    if-ne v8, v5, :cond_0

    .line 76
    const/4 v2, 0x2

    goto :goto_1

    .line 79
    .end local v5    # "state":I
    :cond_3
    return-void
.end method
