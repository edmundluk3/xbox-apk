.class public Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
.super Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
.source "ProfileRecentsResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProfileRecentItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    }
.end annotation


# static fields
.field private static final BROADCAST_THUMBNAIL_SUFFIX:Ljava/lang/String; = "672x378.jpg"

.field private static transient dateFormat:Ljava/text/DateFormat;


# instance fields
.field public achievementDescription:Ljava/lang/String;

.field public achievementIcon:Ljava/lang/String;

.field public achievementId:Ljava/lang/String;

.field public achievementName:Ljava/lang/String;

.field public achievementScid:Ljava/lang/String;

.field public achievementType:Ljava/lang/String;

.field public activityItemType:Ljava/lang/String;

.field public authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

.field public backgroundImage:Ljava/lang/String;

.field public bingId:Ljava/lang/String;

.field public broadcastGuid:Ljava/lang/String;

.field public broadcastId:Ljava/lang/String;

.field public broadcastProvider:Ljava/lang/String;

.field public broadcastThumbnail:Ljava/lang/String;

.field public checkInEnd:Ljava/util/Date;

.field public checkStart:Ljava/util/Date;

.field public clipId:Ljava/lang/String;

.field public clipName:Ljava/lang/String;

.field public clipScid:Ljava/lang/String;

.field public clipThumbnail:Ljava/lang/String;

.field public contentImageUri:Ljava/lang/String;

.field public contentTitle:Ljava/lang/String;

.field public contentType:Ljava/lang/String;

.field public data:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;

.field public date:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCRoundtripDateConverterJSONDeserializer;
    .end annotation
.end field

.field public description:Ljava/lang/String;

.field public displayName:Ljava/lang/String;

.field public feedItemId:Ljava/lang/String;

.field public feedItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation
.end field

.field public gamerscore:I

.field private gamertag:Ljava/lang/String;

.field public hasAppAward:Z

.field public hasArtAward:Z

.field public hasLiked:Z

.field private isHidden:Z

.field public isSecret:Z

.field public itemImage:Ljava/lang/String;

.field public itemRoot:Ljava/lang/String;

.field public itemText:Ljava/lang/String;

.field public leaderboardDescription:Ljava/lang/String;

.field public lfgAchievementTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LfgAchievementTag;",
            ">;"
        }
    .end annotation
.end field

.field public lfgDescription:Ljava/lang/String;

.field public lfgHostInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

.field public lfgPostedTime:Ljava/util/Date;

.field public lfgSessionId:Ljava/lang/String;

.field public lfgStartTime:Ljava/util/Date;

.field public lfgTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public numComments:J

.field public numLikes:J

.field public numShares:J

.field public originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

.field public originalShortDescription:Ljava/lang/String;

.field public pinned:Z

.field public platform:Ljava/lang/String;

.field public playingStart:Ljava/util/Date;

.field public postDetails:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

.field public postType:Ljava/lang/String;

.field private profilePictureUrl:Ljava/lang/String;

.field private profileUserStatus:Lcom/microsoft/xbox/service/model/UserStatus;

.field public rarityCategory:Ljava/lang/String;

.field public rarityPercentage:I

.field private realName:Ljava/lang/String;

.field public recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

.field public registrationEnd:Ljava/util/Date;

.field public registrationStart:Ljava/util/Date;

.field public screenshotId:Ljava/lang/String;

.field public screenshotName:Ljava/lang/String;

.field public screenshotScid:Ljava/lang/String;

.field public screenshotThumbnail:Ljava/lang/String;

.field public screenshotUri:Ljava/lang/String;

.field public shareRoot:Ljava/lang/String;

.field public shortDescription:Ljava/lang/String;

.field public socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

.field public targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

.field public targetUserId:Ljava/lang/String;

.field public targetUserName:Ljava/lang/String;

.field public timeline:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public titleId:Ljava/lang/String;

.field public tournamentDescription:Ljava/lang/String;

.field public tournamentHeroArt:Ljava/lang/String;

.field public tournamentId:Ljava/lang/String;

.field public tournamentName:Ljava/lang/String;

.field public tournamentOrganizer:Ljava/lang/String;

.field public tournamentRegions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public tournamentSharingArt:Ljava/lang/String;

.field public tournamentStatus:Ljava/lang/String;

.field public tournamentTileArt:Ljava/lang/String;

.field public ugcCaption:Ljava/lang/String;

.field public userImageUri:Ljava/lang/String;

.field public userXuid:Ljava/lang/String;

.field public viewCount:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/SimpleDateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->dateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;-><init>()V

    .line 186
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 192
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .locals 4
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 203
    if-eqz p0, :cond_0

    .line 204
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;-><init>()V

    .line 205
    .local v0, "profileRecentItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamertag:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamertag:Ljava/lang/String;

    .line 206
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentType:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentType:Ljava/lang/String;

    .line 207
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    .line 208
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentImageUri:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentImageUri:Ljava/lang/String;

    .line 209
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    .line 210
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->date:Ljava/util/Date;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->date:Ljava/util/Date;

    .line 211
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->titleId:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->titleId:Ljava/lang/String;

    .line 212
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->platform:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->platform:Ljava/lang/String;

    .line 213
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getLastUnlock()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setLastUnlock(Ljava/util/Date;)V

    .line 214
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getEarnedAchievements()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setEarnedAchievements(I)V

    .line 215
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getCurrentGamerscore()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setCurrentGamerscore(I)V

    .line 216
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getMaxGamerscore()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setMaxGamerscore(I)V

    .line 217
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getHeroStats()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setHeroStats(Ljava/util/ArrayList;)V

    .line 218
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getMinutesPlayed()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setMinutesPlayed(Ljava/lang/String;)V

    .line 220
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numLikes:J

    iput-wide v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numLikes:J

    .line 221
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numShares:J

    iput-wide v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numShares:J

    .line 222
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numComments:J

    iput-wide v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numComments:J

    .line 223
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->viewCount:J

    iput-wide v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->viewCount:J

    .line 224
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->hasLiked:Z

    iput-boolean v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->hasLiked:Z

    .line 225
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->pinned:Z

    iput-boolean v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->pinned:Z

    .line 227
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->timeline:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->timeline:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;

    .line 229
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUserId:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUserId:Ljava/lang/String;

    .line 230
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUserName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUserName:Ljava/lang/String;

    .line 232
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    .line 233
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItems:Ljava/util/ArrayList;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItems:Ljava/util/ArrayList;

    .line 235
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipThumbnail:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipThumbnail:Ljava/lang/String;

    .line 236
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipName:Ljava/lang/String;

    .line 237
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipId:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipId:Ljava/lang/String;

    .line 238
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipScid:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipScid:Ljava/lang/String;

    .line 240
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotThumbnail:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotThumbnail:Ljava/lang/String;

    .line 241
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotUri:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotUri:Ljava/lang/String;

    .line 242
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotId:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotId:Ljava/lang/String;

    .line 243
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotName:Ljava/lang/String;

    .line 244
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotScid:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotScid:Ljava/lang/String;

    .line 246
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementIcon:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementIcon:Ljava/lang/String;

    .line 247
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementId:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementId:Ljava/lang/String;

    .line 248
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementScid:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementScid:Ljava/lang/String;

    .line 249
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementType:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementType:Ljava/lang/String;

    .line 250
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    .line 251
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementName:Ljava/lang/String;

    .line 252
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementDescription:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementDescription:Ljava/lang/String;

    .line 253
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->hasAppAward:Z

    iput-boolean v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->hasAppAward:Z

    .line 254
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->hasArtAward:Z

    iput-boolean v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->hasArtAward:Z

    .line 255
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isSecret:Z

    iput-boolean v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isSecret:Z

    .line 256
    iget v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamerscore:I

    iput v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamerscore:I

    .line 257
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->rarityCategory:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->rarityCategory:Ljava/lang/String;

    .line 258
    iget v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->rarityPercentage:I

    iput v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->rarityPercentage:I

    .line 260
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastGuid:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastGuid:Ljava/lang/String;

    .line 261
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastId:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastId:Ljava/lang/String;

    .line 262
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastProvider:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastProvider:Ljava/lang/String;

    .line 263
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastThumbnail:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastThumbnail:Ljava/lang/String;

    .line 265
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userImageUri:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userImageUri:Ljava/lang/String;

    .line 266
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    .line 267
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->shortDescription:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->shortDescription:Ljava/lang/String;

    .line 268
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->ugcCaption:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->ugcCaption:Ljava/lang/String;

    .line 269
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    .line 270
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalShortDescription:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalShortDescription:Ljava/lang/String;

    .line 271
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->realName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->realName:Ljava/lang/String;

    .line 272
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->displayName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->displayName:Ljava/lang/String;

    .line 273
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    .line 274
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    .line 275
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->shareRoot:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->shareRoot:Ljava/lang/String;

    .line 277
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getIsHidden()Z

    move-result v1

    iput-boolean v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isHidden:Z

    .line 279
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->timeline:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->timeline:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;

    .line 281
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentName:Ljava/lang/String;

    .line 282
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentDescription:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentDescription:Ljava/lang/String;

    .line 283
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentStatus:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentStatus:Ljava/lang/String;

    .line 284
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentOrganizer:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentOrganizer:Ljava/lang/String;

    .line 285
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentId:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentId:Ljava/lang/String;

    .line 286
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentRegions:Ljava/util/List;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentRegions:Ljava/util/List;

    .line 287
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentHeroArt:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentHeroArt:Ljava/lang/String;

    .line 288
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentTileArt:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentTileArt:Ljava/lang/String;

    .line 289
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentSharingArt:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentSharingArt:Ljava/lang/String;

    .line 290
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->checkInEnd:Ljava/util/Date;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->checkInEnd:Ljava/util/Date;

    .line 291
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->checkStart:Ljava/util/Date;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->checkStart:Ljava/util/Date;

    .line 292
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->playingStart:Ljava/util/Date;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->playingStart:Ljava/util/Date;

    .line 293
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->registrationEnd:Ljava/util/Date;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->registrationEnd:Ljava/util/Date;

    .line 294
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->registrationStart:Ljava/util/Date;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->registrationStart:Ljava/util/Date;

    .line 299
    .end local v0    # "profileRecentItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getActivityItemType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    .locals 1
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 388
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    .locals 2

    .prologue
    .line 376
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Unknown:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 377
    .local v0, "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->activityItemType:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 379
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->activityItemType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 384
    :cond_0
    :goto_0
    return-object v0

    .line 380
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getAuthorInfoForAchievementComparison()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .locals 1

    .prologue
    .line 392
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    goto :goto_0
.end method

.method public getContentImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentImageUri:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 303
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->dateFormat:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->date:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 353
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->displayName:Ljava/lang/String;

    .line 354
    .local v0, "ret":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 355
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    if-eqz v1, :cond_0

    .line 356
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v0, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->name:Ljava/lang/String;

    .line 359
    :cond_0
    return-object v0
.end method

.method public getIsHidden()Z
    .locals 1

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isHidden:Z

    return v0
.end method

.method public getProfilePictureUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->profilePictureUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileStatus()Lcom/microsoft/xbox/service/model/UserStatus;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->profileUserStatus:Lcom/microsoft/xbox/service/model/UserStatus;

    return-object v0
.end method

.method public getRealName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->realName:Ljava/lang/String;

    .line 341
    .local v0, "ret":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    if-eqz v1, :cond_0

    .line 343
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v0, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->secondName:Ljava/lang/String;

    .line 346
    :cond_0
    return-object v0
.end method

.method public getUserImageUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userImageUri:Ljava/lang/String;

    .line 367
    .local v0, "ret":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 368
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    if-eqz v1, :cond_0

    .line 369
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v0, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->imageUrl:Ljava/lang/String;

    .line 372
    :cond_0
    return-object v0
.end method

.method public getXuidForLoadingGameClip()Ljava/lang/String;
    .locals 1

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->id:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    goto :goto_0
.end method

.method public isRareAchievement()Z
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->rarityCategory:Ljava/lang/String;

    const-string v1, "Rare"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isRecommendation()Z
    .locals 2

    .prologue
    .line 404
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->SocialRecommendation:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShared()Z
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->id:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTrending()Z
    .locals 2

    .prologue
    .line 408
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Container:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setHidden(Z)V
    .locals 0
    .param p1, "hidden"    # Z

    .prologue
    .line 195
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isHidden:Z

    .line 196
    return-void
.end method

.method public setProfilePictureURI(Ljava/lang/String;)V
    .locals 1
    .param p1, "imageUrl"    # Ljava/lang/String;

    .prologue
    .line 316
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 317
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->profilePictureUrl:Ljava/lang/String;

    .line 319
    :cond_0
    return-void
.end method

.method public setProfileStatus(Lcom/microsoft/xbox/service/model/UserStatus;)V
    .locals 0
    .param p1, "status"    # Lcom/microsoft/xbox/service/model/UserStatus;

    .prologue
    .line 326
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->profileUserStatus:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 327
    return-void
.end method

.method public setRealName(Ljava/lang/String;)V
    .locals 1
    .param p1, "realName"    # Ljava/lang/String;

    .prologue
    .line 330
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->realName:Ljava/lang/String;

    .line 331
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iput-object p1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->secondName:Ljava/lang/String;

    .line 334
    :cond_0
    return-void
.end method

.method public toGameClip()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .locals 4

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->GameDVR:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 426
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->GameDVR:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-eq v2, v3, :cond_1

    .line 427
    const/4 v0, 0x0

    .line 448
    :goto_1
    return-object v0

    .line 425
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 430
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;-><init>()V

    .line 431
    .local v0, "result":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamertag:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->setGamerTag(Ljava/lang/String;)V

    .line 432
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipScid:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->scid:Ljava/lang/String;

    .line 433
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    .line 434
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->titleName:Ljava/lang/String;

    .line 435
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->ugcCaption:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->userCaption:Ljava/lang/String;

    .line 436
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->thumbnails:Ljava/util/ArrayList;

    .line 437
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;-><init>()V

    .line 438
    .local v1, "thumbnail":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipThumbnail:Ljava/lang/String;

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->uri:Ljava/lang/String;

    .line 439
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->viewCount:J

    long-to-int v2, v2

    iput v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->views:I

    .line 441
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 442
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->realName:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->capturerRealName:Ljava/lang/String;

    .line 443
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipId:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipId:Ljava/lang/String;

    .line 444
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getDate()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->dateRecorded:Ljava/lang/String;

    .line 445
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->date:Ljava/util/Date;

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->rawDate:Ljava/util/Date;

    .line 446
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipName:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->clipName:Ljava/lang/String;

    goto :goto_1
.end method

.method public toScreenshot()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    .locals 6

    .prologue
    .line 452
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Screenshot:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v3, v4, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 453
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Screenshot:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-eq v3, v4, :cond_1

    .line 454
    const/4 v0, 0x0

    .line 479
    :goto_1
    return-object v0

    .line 452
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 457
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;-><init>()V

    .line 458
    .local v0, "result":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamertag:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->setGamerTag(Ljava/lang/String;)V

    .line 459
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotScid:Ljava/lang/String;

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->scid:Ljava/lang/String;

    .line 460
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->xuid:Ljava/lang/String;

    .line 461
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->titleName:Ljava/lang/String;

    .line 462
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->ugcCaption:Ljava/lang/String;

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->userCaption:Ljava/lang/String;

    .line 463
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->thumbnails:Ljava/util/ArrayList;

    .line 464
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;-><init>()V

    .line 465
    .local v2, "thumbnail":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotThumbnail:Ljava/lang/String;

    iput-object v3, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->uri:Ljava/lang/String;

    .line 466
    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    iget-wide v4, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->viewCount:J

    long-to-int v3, v4

    iput v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->views:I

    .line 468
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 469
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->realName:Ljava/lang/String;

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->capturerRealName:Ljava/lang/String;

    .line 470
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotId:Ljava/lang/String;

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotId:Ljava/lang/String;

    .line 471
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getDate()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->dateTaken:Ljava/lang/String;

    .line 472
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->date:Ljava/util/Date;

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->rawDate:Ljava/util/Date;

    .line 473
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotUris:Ljava/util/ArrayList;

    .line 474
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;-><init>()V

    .line 475
    .local v1, "screenshot":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    iput-object v3, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;->uri:Ljava/lang/String;

    .line 476
    sget-object v3, Lcom/microsoft/xbox/service/model/ScreenshotType;->Download:Lcom/microsoft/xbox/service/model/ScreenshotType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ScreenshotType;->name()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;->uriType:Ljava/lang/String;

    .line 477
    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotUris:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public updateSocialInfo()V
    .locals 4

    .prologue
    .line 416
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numLikes:J

    long-to-int v1, v2

    iput v1, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 417
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numShares:J

    long-to-int v1, v2

    iput v1, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    .line 418
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numComments:J

    long-to-int v1, v2

    iput v1, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->commentCount:I

    .line 419
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->hasLiked:Z

    iput-boolean v1, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    .line 420
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    .line 421
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->activityItemType:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->type:Ljava/lang/String;

    .line 422
    return-void
.end method
