.class public Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;
.super Ljava/lang/Object;
.source "AppChannelLineups.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProviderInfo"
.end annotation


# instance fields
.field public channels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;",
            ">;"
        }
    .end annotation
.end field

.field public id:Ljava/lang/String;

.field public imageUrl:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public primaryColor:I

.field public secondaryColor:I


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 6
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v3, "id"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->id:Ljava/lang/String;

    .line 28
    const-string v3, "providerName"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->name:Ljava/lang/String;

    .line 29
    const-string v3, "providerImageUrl"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->imageUrl:Ljava/lang/String;

    .line 30
    const-string v3, "primaryColor"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->colorFromString(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->primaryColor:I

    .line 31
    const-string v3, "secondaryColor"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->colorFromString(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->secondaryColor:I

    .line 33
    const-string v3, "channels"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 34
    .local v0, "array":Lorg/json/JSONArray;
    if-eqz v0, :cond_0

    .line 36
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->channels:Ljava/util/List;

    .line 38
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    .line 39
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 40
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->channels:Ljava/util/List;

    new-instance v4, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;-><init>(Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;Lorg/json/JSONObject;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 43
    .end local v1    # "count":I
    .end local v2    # "index":I
    :cond_0
    return-void
.end method

.method public static colorFromString(Ljava/lang/String;)I
    .locals 4
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 48
    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    const/16 v2, 0x10

    invoke-static {p0, v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    long-to-int v1, v2

    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/lang/Throwable;
    goto :goto_0
.end method
