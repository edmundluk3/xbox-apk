.class public final Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;
.super Ljava/lang/Object;
.source "PeopleHubSocialListItems.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GameListItem"
.end annotation


# instance fields
.field private final game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

.field private volatile transient hashCode:I


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)V
    .locals 0
    .param p1, "game"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 274
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 275
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;)I
    .locals 3
    .param p1, "another"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;

    .prologue
    .line 327
    if-nez p1, :cond_0

    .line 328
    const/4 v1, -0x1

    .line 333
    :goto_0
    return v1

    .line 329
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 330
    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;

    .line 331
    .local v0, "other":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-object v1, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-object v2, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringCompareUserLocale(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 333
    .end local v0    # "other":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 267
    check-cast p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->compareTo(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 300
    if-ne p1, p0, :cond_0

    .line 301
    const/4 v1, 0x1

    .line 306
    :goto_0
    return v1

    .line 302
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;

    if-nez v1, :cond_1

    .line 303
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 305
    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;

    .line 306
    .local v0, "other":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getTitleId()J
    .locals 4

    .prologue
    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-wide v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->modernTitleId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-wide v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->modernTitleId:J

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-wide v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    goto :goto_0
.end method

.method public getTitleImage()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTitleName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 312
    iget v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->hashCode:I

    if-nez v0, :cond_0

    .line 313
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->hashCode:I

    .line 314
    iget v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->game:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->hashCode:I

    .line 317
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->hashCode:I

    return v0
.end method

.method public onClick(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 3
    .param p1, "viewModelBase"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 294
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 295
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->getTitleId()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->getTitleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->navigateToGameProfile(JLjava/lang/String;)V

    .line 296
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 322
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
