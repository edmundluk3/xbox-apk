.class Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener$1;
.super Ljava/lang/Object;
.source "XboxAuthActivityAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "item"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->enviroments:[Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    aget-object v0, v0, p2

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;->STUB:Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    if-ne v0, v1, :cond_0

    .line 137
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->setStub(Z)V

    .line 144
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEnvironment()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->setEnvironement(Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;)V

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->this$0:Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEnvironment()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 147
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 148
    return-void

    .line 139
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->setStub(Z)V

    .line 140
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->enviroments:[Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    aget-object v1, v1, p2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->setEnvironment(Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;)V

    goto :goto_0
.end method
