.class public Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;
.super Ljava/lang/Object;
.source "TitlePurchaseInfoResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TitlePurchaseInfoResult"
.end annotation


# instance fields
.field public AvailabilityId:Ljava/lang/String;

.field public DisplayListPrice:Ljava/lang/String;

.field public DisplayPrice:Ljava/lang/String;

.field public HomeConsoleId:Ljava/lang/String;

.field public IsAdult:Z

.field public IsFree:Z

.field public IsOwned:Z

.field public IsOwnedWithSubscription:Z

.field public IsPlayable:Z

.field public IsPurchasable:Z

.field public PurchaseDate:Ljava/util/Date;

.field public SignedOffer:Ljava/lang/String;

.field public SubscriptionImageUrl:Ljava/lang/String;

.field public SubscriptionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;
    .locals 3
    .param p0, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 28
    const-class v0, Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;

    const-class v1, Ljava/util/Date;

    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterShortDateFormatJSONDeserializer;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterShortDateFormatJSONDeserializer;-><init>()V

    invoke-static {p0, v0, v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;

    return-object v0
.end method
