.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
.source "$AutoValue_ClubHubDataTypes_ClubRosterItem.java"


# instance fields
.field private final actorXuid:J

.field private final createdDate:Ljava/lang/String;

.field private final localizedRole:Ljava/lang/String;

.field private final xuid:J


# direct methods
.method constructor <init>(JJLjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "actorXuid"    # J
    .param p3, "xuid"    # J
    .param p5, "createdDate"    # Ljava/lang/String;
    .param p6, "localizedRole"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;-><init>()V

    .line 21
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->actorXuid:J

    .line 22
    iput-wide p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->xuid:J

    .line 23
    if-nez p5, :cond_0

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null createdDate"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    iput-object p5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->createdDate:Ljava/lang/String;

    .line 27
    iput-object p6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->localizedRole:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public actorXuid()J
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->actorXuid:J

    return-wide v0
.end method

.method public createdDate()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->createdDate:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    if-ne p1, p0, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v1

    .line 67
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 68
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 69
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->actorXuid:J

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->actorXuid()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->xuid:J

    .line 70
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->createdDate:Ljava/lang/String;

    .line 71
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->localizedRole:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 72
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->localizedRole()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->localizedRole:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->localizedRole()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    :cond_4
    move v1, v2

    .line 74
    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v1, 0x20

    const v8, 0xf4243

    .line 79
    const/4 v0, 0x1

    .line 80
    .local v0, "h":I
    mul-int/2addr v0, v8

    .line 81
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->actorXuid:J

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->actorXuid:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 82
    mul-int/2addr v0, v8

    .line 83
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->xuid:J

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->xuid:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 84
    mul-int/2addr v0, v8

    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->createdDate:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 86
    mul-int/2addr v0, v8

    .line 87
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->localizedRole:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 88
    return v0

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->localizedRole:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public localizedRole()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->localizedRole:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubRosterItem{actorXuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->actorXuid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", xuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->xuid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", createdDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->createdDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", localizedRole="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->localizedRole:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public xuid()J
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterItem;->xuid:J

    return-wide v0
.end method
