.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;
.super Ljava/lang/Object;
.source "UTCRealNameSharing.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing;->trackRealNameSharingToggle(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$actionStatus:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

.field final synthetic val$setting:Ljava/lang/String;

.field final synthetic val$shareIdentityStatus:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;->val$shareIdentityStatus:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;->val$actionStatus:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;->val$setting:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;->val$shareIdentityStatus:Ljava/lang/String;

    sget-object v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Blocked:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->toString()Ljava/lang/String;

    move-result-object v2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;->val$actionStatus:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_1

    .line 80
    const-string v1, "Real Name Share - Turn off Real Name Sharing"

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;->val$actionStatus:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 83
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 85
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;->val$shareIdentityStatus:Ljava/lang/String;

    sget-object v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Everyone:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->toString()Ljava/lang/String;

    move-result-object v2

    if-ne v1, v2, :cond_2

    .line 86
    const-string v1, "realNameShareScope"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 94
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;->val$setting:Ljava/lang/String;

    const-string v2, "ShareIdentity"

    if-ne v1, v2, :cond_5

    .line 95
    const-string v1, "realNameSharingSetting"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 102
    :goto_2
    const-string v1, "Real Name Share - Turn on Real Name Sharing"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0

    .line 87
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;->val$shareIdentityStatus:Ljava/lang/String;

    sget-object v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->FriendCategoryShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->toString()Ljava/lang/String;

    move-result-object v2

    if-ne v1, v2, :cond_3

    .line 88
    const-string v1, "realNameShareScope"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 89
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;->val$shareIdentityStatus:Ljava/lang/String;

    sget-object v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->PeopleOnMyList:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->toString()Ljava/lang/String;

    move-result-object v2

    if-ne v1, v2, :cond_4

    .line 90
    const-string v1, "realNameShareScope"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 92
    :cond_4
    const-string v1, "realNameShareScope"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 96
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;->val$setting:Ljava/lang/String;

    const-string v2, "ShareIdentityTransitively"

    if-ne v1, v2, :cond_6

    .line 97
    const-string v1, "realNameSharingSetting"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    .line 99
    :cond_6
    const-string v1, "realNameSharingSetting"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2
.end method
