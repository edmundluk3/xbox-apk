.class public Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
.super Ljava/lang/Object;
.source "ClubProfileDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private activityFeedEnabled:Z

.field private backgroundImageUrl:Ljava/lang/String;

.field private chatEnabled:Z

.field private creationDateUtc:Ljava/util/Date;

.field private description:Ljava/lang/String;

.field private displayImageUrl:Ljava/lang/String;

.field private founder:Ljava/lang/String;

.field private isPromotedClub:Z

.field private lfgEnabled:Z

.field private matureContentEnabled:Z

.field private name:Ljava/lang/String;

.field private preferredColor:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;

.field private preferredLocale:Ljava/lang/String;

.field private requestToJoinEnabled:Z

.field private shortName:Ljava/lang/String;

.field private tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private titles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private type:Ljava/lang/String;

.field private watchClubTitlesOnly:Z

.field private whoCanChat:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private whoCanCreateLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private whoCanInvite:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private whoCanJoinLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private whoCanPostToFeed:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private whoCanSetChatTopic:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private whoCanViewChat:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private whoCanViewFeed:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private whoCanViewLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private whoCanViewRoster:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;
    .locals 32

    .prologue
    .line 371
    new-instance v1, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->type:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->description:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->founder:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->creationDateUtc:Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->displayImageUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->backgroundImageUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->preferredColor:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->activityFeedEnabled:Z

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->chatEnabled:Z

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->lfgEnabled:Z

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->matureContentEnabled:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->watchClubTitlesOnly:Z

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->preferredLocale:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->requestToJoinEnabled:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->isPromotedClub:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->shortName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->titles:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->tags:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanPostToFeed:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanInvite:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanChat:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanCreateLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanJoinLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanViewRoster:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanViewFeed:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanViewLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanViewChat:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanSetChatTopic:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-direct/range {v1 .. v31}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;ZZZZZLjava/lang/String;ZZLjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$1;)V

    return-object v1
.end method

.method public setActivityFeedEnabled(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 266
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->activityFeedEnabled:Z

    .line 267
    return-object p0
.end method

.method public setBackgroundImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "backgroundImageUrl"    # Ljava/lang/String;

    .prologue
    .line 256
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->backgroundImageUrl:Ljava/lang/String;

    .line 257
    return-object p0
.end method

.method public setChatEnabled(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 271
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->chatEnabled:Z

    .line 272
    return-object p0
.end method

.method public setCreationDateUtc(Ljava/util/Date;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 4
    .param p1, "creationDateUtc"    # Ljava/util/Date;

    .prologue
    .line 246
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->creationDateUtc:Ljava/util/Date;

    .line 247
    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 236
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->description:Ljava/lang/String;

    .line 237
    return-object p0
.end method

.method public setDisplayImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "displayImageUrl"    # Ljava/lang/String;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->displayImageUrl:Ljava/lang/String;

    .line 252
    return-object p0
.end method

.method public setFounder(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "founder"    # Ljava/lang/String;

    .prologue
    .line 241
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->founder:Ljava/lang/String;

    .line 242
    return-object p0
.end method

.method public setIsPromotedClub(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "isPromotedClub"    # Z

    .prologue
    .line 301
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->isPromotedClub:Z

    .line 302
    return-object p0
.end method

.method public setLfgEnabled(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 276
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->lfgEnabled:Z

    .line 277
    return-object p0
.end method

.method public setMatureContentEnabled(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 281
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->matureContentEnabled:Z

    .line 282
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->name:Ljava/lang/String;

    .line 227
    return-object p0
.end method

.method public setPreferredColor(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "preferredColor"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;

    .prologue
    .line 261
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->preferredColor:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;

    .line 262
    return-object p0
.end method

.method public setPreferredLocale(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "preferredLocale"    # Ljava/lang/String;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->preferredLocale:Ljava/lang/String;

    .line 292
    return-object p0
.end method

.method public setRequestToJoinEnabled(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 296
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->requestToJoinEnabled:Z

    .line 297
    return-object p0
.end method

.method public setShortName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "shortName"    # Ljava/lang/String;

    .prologue
    .line 306
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->shortName:Ljava/lang/String;

    .line 307
    return-object p0
.end method

.method public setTags(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;"
        }
    .end annotation

    .prologue
    .line 316
    .local p1, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->tags:Ljava/util/List;

    .line 317
    return-object p0
.end method

.method public setTitles(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;"
        }
    .end annotation

    .prologue
    .line 311
    .local p1, "titles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->titles:Ljava/util/List;

    .line 312
    return-object p0
.end method

.method public setType(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 231
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->type:Ljava/lang/String;

    .line 232
    return-object p0
.end method

.method public setWatchClubTitlesOnly(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 286
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->watchClubTitlesOnly:Z

    .line 287
    return-object p0
.end method

.method public setWhoCanChat(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "who"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanChat:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 332
    return-object p0
.end method

.method public setWhoCanCreateLfg(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "who"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .prologue
    .line 336
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanCreateLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 337
    return-object p0
.end method

.method public setWhoCanInvite(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "who"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .prologue
    .line 326
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanInvite:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 327
    return-object p0
.end method

.method public setWhoCanJoinLfg(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "who"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanJoinLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 342
    return-object p0
.end method

.method public setWhoCanPostToFeed(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "who"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .prologue
    .line 321
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanPostToFeed:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 322
    return-object p0
.end method

.method public setWhoCanSetChatTopic(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "who"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanSetChatTopic:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 367
    return-object p0
.end method

.method public setWhoCanViewChat(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "who"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .prologue
    .line 361
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanViewChat:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 362
    return-object p0
.end method

.method public setWhoCanViewFeed(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "who"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .prologue
    .line 351
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanViewFeed:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 352
    return-object p0
.end method

.method public setWhoCanViewLfg(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "who"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .prologue
    .line 356
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanViewLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 357
    return-object p0
.end method

.method public setWhoCanViewRoster(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    .locals 0
    .param p1, "who"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .prologue
    .line 346
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->whoCanViewRoster:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 347
    return-object p0
.end method
