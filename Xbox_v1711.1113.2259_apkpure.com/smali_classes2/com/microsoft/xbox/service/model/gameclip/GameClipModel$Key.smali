.class public Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;
.super Ljava/lang/Object;
.source "GameClipModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Key"
.end annotation


# instance fields
.field public final gameClipId:Ljava/lang/String;

.field public final scid:Ljava/lang/String;

.field public final xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "gameClipId"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->xuid:Ljava/lang/String;

    .line 71
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->scid:Ljava/lang/String;

    .line 72
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->gameClipId:Ljava/lang/String;

    .line 73
    return-void
.end method

.method private static equals(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x"    # Ljava/lang/String;
    .param p1, "y"    # Ljava/lang/String;

    .prologue
    .line 109
    if-eqz p0, :cond_0

    .line 110
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 116
    :goto_0
    return v0

    .line 112
    :cond_0
    if-eqz p1, :cond_1

    .line 113
    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 116
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static hash(Ljava/lang/String;)I
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 120
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 86
    if-nez p1, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    if-ne p1, p0, :cond_2

    move v1, v2

    .line 90
    goto :goto_0

    .line 92
    :cond_2
    instance-of v3, p1, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 95
    check-cast v0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    .line 96
    .local v0, "other":Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->xuid:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->xuid:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 99
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->scid:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->scid:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 102
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->gameClipId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->gameClipId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 105
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 77
    const/16 v0, 0x11

    .line 78
    .local v0, "hash":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->xuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->hash(Ljava/lang/String;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 79
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->scid:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->hash(Ljava/lang/String;)I

    move-result v2

    add-int v0, v1, v2

    .line 80
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->gameClipId:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->hash(Ljava/lang/String;)I

    move-result v2

    add-int v0, v1, v2

    .line 81
    return v0
.end method
