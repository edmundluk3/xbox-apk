.class public final Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;
.super Ljava/lang/Object;
.source "TitleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TitleAchievement"
.end annotation


# instance fields
.field public final currentAchievements:I

.field public final currentGamerscore:I

.field private volatile transient hashCode:I

.field public final progressPercentage:I

.field public final sourceVersion:I

.field public final totalAchievements:I

.field public final totalGamerscore:I


# direct methods
.method public constructor <init>(IIIIII)V
    .locals 0
    .param p1, "currentAchievements"    # I
    .param p2, "currentGamerscore"    # I
    .param p3, "progressPercentage"    # I
    .param p4, "sourceVersion"    # I
    .param p5, "totalAchievements"    # I
    .param p6, "totalGamerscore"    # I

    .prologue
    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 428
    iput p1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->currentAchievements:I

    .line 429
    iput p2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->currentGamerscore:I

    .line 430
    iput p3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->progressPercentage:I

    .line 431
    iput p4, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->sourceVersion:I

    .line 432
    iput p5, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->totalAchievements:I

    .line 433
    iput p6, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->totalGamerscore:I

    .line 434
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 438
    if-ne p1, p0, :cond_1

    .line 444
    :cond_0
    :goto_0
    return v1

    .line 440
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    if-nez v3, :cond_2

    move v1, v2

    .line 441
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 443
    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    .line 444
    .local v0, "other":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;
    iget v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->currentAchievements:I

    iget v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->currentAchievements:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->currentGamerscore:I

    iget v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->currentGamerscore:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->progressPercentage:I

    iget v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->progressPercentage:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->sourceVersion:I

    iget v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->sourceVersion:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->totalAchievements:I

    iget v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->totalAchievements:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->totalGamerscore:I

    iget v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->totalGamerscore:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 455
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    if-nez v0, :cond_0

    .line 456
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    .line 457
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->currentAchievements:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    .line 458
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->currentGamerscore:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    .line 459
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->progressPercentage:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    .line 460
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->sourceVersion:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    .line 461
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->totalAchievements:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    .line 462
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->totalGamerscore:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    .line 465
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 470
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
