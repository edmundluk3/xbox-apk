.class Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PrepareLfgShareRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "UserPostsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrepareLfgShareRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "postTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PrepareLfgShareRunner;->this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 189
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 190
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PrepareLfgShareRunner;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .line 191
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 199
    invoke-static {}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->access$000()Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PrepareLfgShareRunner;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;->prepareLfgPost(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PrepareLfgShareRunner;->buildData()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 208
    const-wide/16 v0, 0x2330

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;>;"
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 195
    return-void
.end method
