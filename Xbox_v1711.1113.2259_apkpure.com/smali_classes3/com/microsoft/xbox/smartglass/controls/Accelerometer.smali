.class Lcom/microsoft/xbox/smartglass/controls/Accelerometer;
.super Lcom/microsoft/xbox/smartglass/controls/Sensor;
.source "Accelerometer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/smartglass/controls/Accelerometer$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/smartglass/controls/Sensor",
        "<",
        "Lcom/microsoft/xbox/smartglass/AccelerometerListener;",
        ">;"
    }
.end annotation


# static fields
.field public static final ComponentName:Ljava/lang/String; = "Accelerometer"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 1
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 18
    const-string v0, "Accelerometer"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/Sensor;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected createListener()Lcom/microsoft/xbox/smartglass/AccelerometerListener;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/Accelerometer$Listener;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/smartglass/controls/Accelerometer$Listener;-><init>(Lcom/microsoft/xbox/smartglass/controls/Accelerometer;)V

    return-object v0
.end method

.method protected bridge synthetic createListener()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Accelerometer;->createListener()Lcom/microsoft/xbox/smartglass/AccelerometerListener;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentState()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 24
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonAccelerometerCurrentState;

    invoke-static {}, Lcom/microsoft/xbox/smartglass/Accelerometer;->isSupported()Z

    move-result v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/smartglass/controls/Accelerometer;->_isActive:Z

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/controls/JsonAccelerometerCurrentState;-><init>(ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    :goto_0
    return-object v0

    .line 25
    :catch_0
    move-exception v0

    .line 29
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getMetricMethodName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string v0, "StartAccelerometer"

    return-object v0
.end method

.method protected getSensor()Lcom/microsoft/xbox/smartglass/Sensor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/smartglass/Sensor",
            "<",
            "Lcom/microsoft/xbox/smartglass/AccelerometerListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSensorManager()Lcom/microsoft/xbox/smartglass/SensorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/SensorManager;->getAccelerometer()Lcom/microsoft/xbox/smartglass/Accelerometer;

    move-result-object v0

    return-object v0
.end method

.method protected isSupported()Z
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/microsoft/xbox/smartglass/Accelerometer;->isSupported()Z

    move-result v0

    return v0
.end method
