.class final Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;
.super Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;
.source "AutoValue_ClubCardModel.java"


# instance fields
.field private final backgroundColor:I

.field private final bannerImageUrl:Ljava/lang/String;

.field private final clubId:J

.field private final clubTypeName:Ljava/lang/String;

.field private final description:Ljava/lang/String;

.field private final displayImageUrl:Ljava/lang/String;

.field private final glyphImageUrl:Ljava/lang/String;

.field private final memberCount:J

.field private final name:Ljava/lang/String;

.field private final presenceCount:J

.field private final socialTags:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation
.end field

.field private final tagColor:I

.field private final type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;


# direct methods
.method constructor <init>(JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLcom/google/common/collect/ImmutableList;)V
    .locals 4
    .param p1, "clubId"    # J
    .param p3, "type"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .param p4, "clubTypeName"    # Ljava/lang/String;
    .param p5, "backgroundColor"    # I
    .param p6, "tagColor"    # I
    .param p7, "displayImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "bannerImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p9, "glyphImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p10, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p11, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p12, "presenceCount"    # J
    .param p14, "memberCount"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p16, "socialTags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;-><init>()V

    .line 43
    iput-wide p1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->clubId:J

    .line 44
    if-nez p3, :cond_0

    .line 45
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null type"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 47
    :cond_0
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 48
    if-nez p4, :cond_1

    .line 49
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null clubTypeName"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 51
    :cond_1
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->clubTypeName:Ljava/lang/String;

    .line 52
    iput p5, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->backgroundColor:I

    .line 53
    iput p6, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->tagColor:I

    .line 54
    iput-object p7, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->displayImageUrl:Ljava/lang/String;

    .line 55
    iput-object p8, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->bannerImageUrl:Ljava/lang/String;

    .line 56
    iput-object p9, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->glyphImageUrl:Ljava/lang/String;

    .line 57
    iput-object p10, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->name:Ljava/lang/String;

    .line 58
    iput-object p11, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->description:Ljava/lang/String;

    .line 59
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->presenceCount:J

    .line 60
    move-wide/from16 v0, p14

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->memberCount:J

    .line 61
    if-nez p16, :cond_2

    .line 62
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null socialTags"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 64
    :cond_2
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->socialTags:Lcom/google/common/collect/ImmutableList;

    .line 65
    return-void
.end method


# virtual methods
.method public backgroundColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 87
    iget v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->backgroundColor:I

    return v0
.end method

.method public bannerImageUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->bannerImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public clubId()J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->clubId:J

    return-wide v0
.end method

.method public clubTypeName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->clubTypeName:Ljava/lang/String;

    return-object v0
.end method

.method public description()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->description:Ljava/lang/String;

    return-object v0
.end method

.method public displayImageUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->displayImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 162
    if-ne p1, p0, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v1

    .line 165
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    if-eqz v3, :cond_8

    move-object v0, p1

    .line 166
    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    .line 167
    .local v0, "that":Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->clubId:J

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->clubId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 168
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->clubTypeName:Ljava/lang/String;

    .line 169
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->clubTypeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->backgroundColor:I

    .line 170
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->backgroundColor()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->tagColor:I

    .line 171
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->tagColor()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->displayImageUrl:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 172
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->displayImageUrl()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->bannerImageUrl:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 173
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->bannerImageUrl()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->glyphImageUrl:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 174
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->glyphImageUrl()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->name:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 175
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->name()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->description:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 176
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->description()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->presenceCount:J

    .line 177
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->presenceCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->memberCount:J

    .line 178
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->memberCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->socialTags:Lcom/google/common/collect/ImmutableList;

    .line 179
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->socialTags()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto/16 :goto_0

    .line 172
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->displayImageUrl:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->displayImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 173
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->bannerImageUrl:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->bannerImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 174
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->glyphImageUrl:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->glyphImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 175
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->name:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 176
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->description:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->description()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .end local v0    # "that":Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;
    :cond_8
    move v1, v2

    .line 181
    goto/16 :goto_0
.end method

.method public glyphImageUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->glyphImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 12

    .prologue
    const/16 v11, 0x20

    const/4 v2, 0x0

    const v10, 0xf4243

    .line 186
    const/4 v0, 0x1

    .line 187
    .local v0, "h":I
    mul-int/2addr v0, v10

    .line 188
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->clubId:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->clubId:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 189
    mul-int/2addr v0, v10

    .line 190
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 191
    mul-int/2addr v0, v10

    .line 192
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->clubTypeName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 193
    mul-int/2addr v0, v10

    .line 194
    iget v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->backgroundColor:I

    xor-int/2addr v0, v1

    .line 195
    mul-int/2addr v0, v10

    .line 196
    iget v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->tagColor:I

    xor-int/2addr v0, v1

    .line 197
    mul-int/2addr v0, v10

    .line 198
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->displayImageUrl:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 199
    mul-int/2addr v0, v10

    .line 200
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->bannerImageUrl:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 201
    mul-int/2addr v0, v10

    .line 202
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->glyphImageUrl:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 203
    mul-int/2addr v0, v10

    .line 204
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->name:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 205
    mul-int/2addr v0, v10

    .line 206
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->description:Ljava/lang/String;

    if-nez v1, :cond_4

    :goto_4
    xor-int/2addr v0, v2

    .line 207
    mul-int/2addr v0, v10

    .line 208
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->presenceCount:J

    ushr-long/2addr v4, v11

    iget-wide v6, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->presenceCount:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 209
    mul-int/2addr v0, v10

    .line 210
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->memberCount:J

    ushr-long/2addr v4, v11

    iget-wide v6, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->memberCount:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 211
    mul-int/2addr v0, v10

    .line 212
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->socialTags:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 213
    return v0

    .line 198
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->displayImageUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 200
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->bannerImageUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 202
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->glyphImageUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 204
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 206
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4
.end method

.method public memberCount()J
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->memberCount:J

    return-wide v0
.end method

.method public name()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public presenceCount()J
    .locals 2

    .prologue
    .line 128
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->presenceCount:J

    return-wide v0
.end method

.method public socialTags()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->socialTags:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public tagColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 93
    iget v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->tagColor:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubCardModel{clubId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->clubId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clubTypeName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->clubTypeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", backgroundColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->backgroundColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tagColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->tagColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", displayImageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->displayImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bannerImageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->bannerImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", glyphImageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->glyphImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presenceCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->presenceCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", memberCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->memberCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", socialTags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->socialTags:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    return-object v0
.end method
