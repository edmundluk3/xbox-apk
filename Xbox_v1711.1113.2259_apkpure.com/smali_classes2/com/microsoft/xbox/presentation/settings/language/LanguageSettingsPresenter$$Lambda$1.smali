.class final synthetic Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/ObservableTransformer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;

.field private final arg$2:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;

.field private final arg$3:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$1;->arg$1:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$1;->arg$2:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;

    iput-object p3, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$1;->arg$3:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)Lio/reactivex/ObservableTransformer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$1;-><init>(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)V

    return-object v0
.end method


# virtual methods
.method public apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$1;->arg$1:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$1;->arg$2:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$1;->arg$3:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;

    invoke-static {v0, v1, v2, p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->lambda$new$8(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
