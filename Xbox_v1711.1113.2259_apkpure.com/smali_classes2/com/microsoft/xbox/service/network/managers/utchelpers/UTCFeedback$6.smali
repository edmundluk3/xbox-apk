.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;
.super Ljava/lang/Object;
.source "UTCFeedback.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;->trackFeedback(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$comment:Ljava/lang/String;

.field final synthetic val$logData:Ljava/lang/String;

.field final synthetic val$pageName:Ljava/lang/String;

.field final synthetic val$rating:I

.field final synthetic val$ratingType:I


# direct methods
.method constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$rating:I

    iput p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$ratingType:I

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$pageName:Ljava/lang/String;

    iput-object p4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$logData:Ljava/lang/String;

    iput-object p5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$comment:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 5

    .prologue
    .line 85
    new-instance v0, Lxbox/smartglass/Feedback;

    invoke-direct {v0}, Lxbox/smartglass/Feedback;-><init>()V

    .line 86
    .local v0, "feedback":Lxbox/smartglass/Feedback;
    iget v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$rating:I

    invoke-virtual {v0, v1}, Lxbox/smartglass/Feedback;->setRating(I)V

    .line 87
    iget v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$ratingType:I

    invoke-virtual {v0, v1}, Lxbox/smartglass/Feedback;->setRatingType(I)V

    .line 88
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$pageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lxbox/smartglass/Feedback;->setPageName(Ljava/lang/String;)V

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$logData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lxbox/smartglass/Feedback;->setLogData(Ljava/lang/String;)V

    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$comment:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lxbox/smartglass/Feedback;->setComment(Ljava/lang/String;)V

    .line 92
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;->access$000()I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/Feedback;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 94
    const-string v1, "Feedback - rating: %d ratingType: %d pageName: %s logData: %s comment: %s"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$rating:I

    .line 95
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$ratingType:I

    .line 96
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$pageName:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$logData:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;->val$comment:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 94
    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 101
    return-void
.end method
