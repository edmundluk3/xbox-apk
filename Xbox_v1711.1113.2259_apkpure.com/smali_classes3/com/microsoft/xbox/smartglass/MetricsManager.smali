.class public Lcom/microsoft/xbox/smartglass/MetricsManager;
.super Ljava/lang/Object;
.source "MetricsManager.java"


# instance fields
.field private final _pMetricsManager:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method constructor <init>(J)V
    .locals 5
    .param p1, "pPlatform"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/MetricsManager;->initialize(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/MetricsManager;->_pMetricsManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 13
    return-void
.end method

.method private native initialize(J)J
.end method

.method private native recordEvent(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native start(JLjava/lang/String;I)V
.end method

.method private native stop(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
.end method


# virtual methods
.method public recordEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "metricId"    # Ljava/lang/String;
    .param p2, "origin"    # Ljava/lang/String;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/MetricsManager;->_pMetricsManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    const-string v6, ""

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public recordEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "metricId"    # Ljava/lang/String;
    .param p2, "origin"    # Ljava/lang/String;
    .param p3, "context"    # Ljava/lang/String;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/MetricsManager;->_pMetricsManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public start(Ljava/lang/String;)V
    .locals 1
    .param p1, "metricId"    # Ljava/lang/String;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/smartglass/MetricsManager;->start(Ljava/lang/String;I)V

    .line 21
    return-void
.end method

.method public start(Ljava/lang/String;I)V
    .locals 2
    .param p1, "metricId"    # Ljava/lang/String;
    .param p2, "requestId"    # I

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/MetricsManager;->_pMetricsManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/microsoft/xbox/smartglass/MetricsManager;->start(JLjava/lang/String;I)V

    .line 30
    return-void
.end method

.method public stop(Ljava/lang/String;ILjava/lang/String;)V
    .locals 8
    .param p1, "metricId"    # Ljava/lang/String;
    .param p2, "requestId"    # I
    .param p3, "origin"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/MetricsManager;->_pMetricsManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    const-string v7, ""

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public stop(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "metricId"    # Ljava/lang/String;
    .param p2, "requestId"    # I
    .param p3, "origin"    # Ljava/lang/String;
    .param p4, "context"    # Ljava/lang/String;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/MetricsManager;->_pMetricsManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public stop(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "metricId"    # Ljava/lang/String;
    .param p2, "origin"    # Ljava/lang/String;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/MetricsManager;->_pMetricsManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    const/4 v5, 0x0

    const-string v7, ""

    move-object v1, p0

    move-object v4, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public stop(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "metricId"    # Ljava/lang/String;
    .param p2, "origin"    # Ljava/lang/String;
    .param p3, "context"    # Ljava/lang/String;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/MetricsManager;->_pMetricsManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    const/4 v5, 0x0

    move-object v1, p0

    move-object v4, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 49
    return-void
.end method
