.class public Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "PageUserInfoScreenAdapter.java"


# instance fields
.field private descriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private followButton:Landroid/widget/RelativeLayout;

.field private followButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private followButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private pageIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private pageInfoScrollView:Landroid/support/v4/widget/NestedScrollView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    .line 30
    const v0, 0x7f0e0829

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->screenBody:Landroid/view/View;

    .line 31
    const v0, 0x7f0e082a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 32
    const v0, 0x7f0e082b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/NestedScrollView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->pageInfoScrollView:Landroid/support/v4/widget/NestedScrollView;

    .line 34
    const v0, 0x7f0e082c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->pageIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 35
    const v0, 0x7f0e0665

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->followButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 36
    const v0, 0x7f0e0664

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->followButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 37
    const v0, 0x7f0e082d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->followButton:Landroid/widget/RelativeLayout;

    .line 38
    const v0, 0x7f0e082e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->descriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 39
    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->followUnfollow()V

    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->followButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->followButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->followButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->followButton:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    :cond_0
    return-void
.end method

.method protected updateViewOverride()V
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->updateLoadingIndicator(Z)V

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->pageInfoScrollView:Landroid/support/v4/widget/NestedScrollView;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->pageInfoScrollView:Landroid/support/v4/widget/NestedScrollView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->getPageColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/NestedScrollView;->setBackgroundColor(I)V

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->pageIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->getPageIconURI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->descriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isFollowingPage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->followButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070e9a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->followButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705cc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 81
    :goto_0
    return-void

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->followButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070fe0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;->followButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
