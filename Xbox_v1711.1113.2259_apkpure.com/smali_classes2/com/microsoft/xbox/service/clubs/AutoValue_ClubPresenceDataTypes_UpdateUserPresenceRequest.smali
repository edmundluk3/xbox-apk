.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubPresenceDataTypes_UpdateUserPresenceRequest;
.super Lcom/microsoft/xbox/service/clubs/ClubPresenceDataTypes$UpdateUserPresenceRequest;
.source "AutoValue_ClubPresenceDataTypes_UpdateUserPresenceRequest.java"


# instance fields
.field private final userPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V
    .locals 2
    .param p1, "userPresenceState"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubPresenceDataTypes$UpdateUserPresenceRequest;-><init>()V

    .line 13
    if-nez p1, :cond_0

    .line 14
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null userPresenceState"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubPresenceDataTypes_UpdateUserPresenceRequest;->userPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 17
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 33
    if-ne p1, p0, :cond_0

    .line 34
    const/4 v1, 0x1

    .line 40
    :goto_0
    return v1

    .line 36
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/clubs/ClubPresenceDataTypes$UpdateUserPresenceRequest;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubPresenceDataTypes$UpdateUserPresenceRequest;

    .line 38
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubPresenceDataTypes$UpdateUserPresenceRequest;
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubPresenceDataTypes_UpdateUserPresenceRequest;->userPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubPresenceDataTypes$UpdateUserPresenceRequest;->userPresenceState()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 40
    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubPresenceDataTypes$UpdateUserPresenceRequest;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 45
    const/4 v0, 0x1

    .line 46
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubPresenceDataTypes_UpdateUserPresenceRequest;->userPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 48
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateUserPresenceRequest{userPresenceState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubPresenceDataTypes_UpdateUserPresenceRequest;->userPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userPresenceState()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubPresenceDataTypes_UpdateUserPresenceRequest;->userPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    return-object v0
.end method
