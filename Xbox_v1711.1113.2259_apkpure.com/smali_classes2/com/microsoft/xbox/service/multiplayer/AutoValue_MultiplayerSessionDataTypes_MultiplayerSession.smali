.class final Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSession;
.super Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerSession;
.source "AutoValue_MultiplayerSessionDataTypes_MultiplayerSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSession$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionConstants;Ljava/util/Map;Ljava/util/Date;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerServers;)V
    .locals 0
    .param p1, "branch"    # Ljava/lang/String;
    .param p2, "changeNumber"    # Ljava/lang/Integer;
    .param p3, "contractVersion"    # Ljava/lang/Integer;
    .param p4, "correlationId"    # Ljava/lang/String;
    .param p5, "searchHandle"    # Ljava/lang/String;
    .param p6, "properties"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties;
    .param p7, "constants"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionConstants;
    .param p9, "startTime"    # Ljava/util/Date;
    .param p10, "roleTypes"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;
    .param p11, "servers"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerServers;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionConstants;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;",
            ">;",
            "Ljava/util/Date;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerServers;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    .local p8, "members":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    invoke-direct/range {p0 .. p11}, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerSession;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionConstants;Ljava/util/Map;Ljava/util/Date;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerServers;)V

    .line 25
    return-void
.end method
