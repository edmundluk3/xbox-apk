.class public Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "EPGErrorDialog.java"


# direct methods
.method public constructor <init>(IIZ)V
    .locals 7
    .param p1, "errorHeader"    # I
    .param p2, "errorString"    # I
    .param p3, "center"    # Z

    .prologue
    .line 17
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f0802d4

    invoke-direct {p0, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 19
    const v4, 0x7f030243

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->setContentView(I)V

    .line 20
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->setCanceledOnTouchOutside(Z)V

    .line 22
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-virtual {v4, v5, v6}, Landroid/view/Window;->setLayout(II)V

    .line 23
    if-eqz p3, :cond_4

    .line 24
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x11

    invoke-virtual {v4, v5}, Landroid/view/Window;->setGravity(I)V

    .line 29
    :goto_0
    const v4, 0x7f0e0b00

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 30
    .local v3, "root":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 31
    new-instance v4, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog$1;

    invoke-direct {v4, p0, v3}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog$1;-><init>(Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    :cond_0
    const v4, 0x7f0e022a

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 44
    .local v0, "closeButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    if-eqz v0, :cond_1

    .line 45
    new-instance v4, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog$2;

    invoke-direct {v4, p0, v0}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog$2;-><init>(Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    :cond_1
    const v4, 0x7f0e0b1c

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 58
    .local v1, "mHeaderView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz v1, :cond_2

    .line 59
    if-nez p1, :cond_5

    .line 60
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 67
    :cond_2
    :goto_1
    const v4, 0x7f0e0b1d

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 68
    .local v2, "mTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz v2, :cond_3

    .line 69
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    :cond_3
    return-void

    .line 26
    .end local v0    # "closeButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .end local v1    # "mHeaderView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .end local v2    # "mTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .end local v3    # "root":Landroid/view/View;
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x50

    invoke-virtual {v4, v5}, Landroid/view/Window;->setGravity(I)V

    goto :goto_0

    .line 63
    .restart local v0    # "closeButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .restart local v1    # "mHeaderView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .restart local v3    # "root":Landroid/view/View;
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->onClosePressed()V

    return-void
.end method

.method private onClosePressed()V
    .locals 0

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->safeDismiss()V

    .line 85
    return-void
.end method

.method public static show(IIZ)V
    .locals 2
    .param p0, "errorHeader"    # I
    .param p1, "errorString"    # I
    .param p2, "center"    # Z

    .prologue
    .line 74
    new-instance v0, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;-><init>(IIZ)V

    .line 75
    .local v0, "dlg":Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 76
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->onClosePressed()V

    .line 81
    return-void
.end method
