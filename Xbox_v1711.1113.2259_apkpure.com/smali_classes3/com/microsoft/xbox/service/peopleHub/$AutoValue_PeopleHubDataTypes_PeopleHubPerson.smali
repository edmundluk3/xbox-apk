.class abstract Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;
.super Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;
.source "$AutoValue_PeopleHubDataTypes_PeopleHubPerson.java"


# instance fields
.field private final addedDateTimeUtc:Ljava/util/Date;

.field private final displayName:Ljava/lang/String;

.field private final displayPicRaw:Ljava/lang/String;

.field private final gamerScore:J

.field private final gamertag:Ljava/lang/String;

.field private final isBroadcasting:Z

.field private final isFavorite:Z

.field private final isFollowedByCaller:Z

.field private final isFollowingCaller:Z

.field private final isIdentityShared:Z

.field private final multiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

.field private final preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

.field private final presenceState:Lcom/microsoft/xbox/service/model/UserStatus;

.field private final presenceText:Ljava/lang/String;

.field private final realName:Ljava/lang/String;

.field private final xuid:J


# direct methods
.method constructor <init>(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZZZZZLcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;Lcom/microsoft/xbox/service/model/UserStatus;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2
    .param p1, "addedDateTimeUtc"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "displayName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "displayPicRaw"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "gamerScore"    # J
    .param p6, "gamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "isBroadcasting"    # Z
    .param p8, "isFavorite"    # Z
    .param p9, "isFollowedByCaller"    # Z
    .param p10, "isFollowingCaller"    # Z
    .param p11, "isIdentityShared"    # Z
    .param p12, "multiplayerSummary"    # Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p13, "preferredColor"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p14, "presenceState"    # Lcom/microsoft/xbox/service/model/UserStatus;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p15, "presenceText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p16, "realName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p17, "xuid"    # J

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->addedDateTimeUtc:Ljava/util/Date;

    .line 48
    iput-object p2, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayName:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayPicRaw:Ljava/lang/String;

    .line 50
    iput-wide p4, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamerScore:J

    .line 51
    iput-object p6, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamertag:Ljava/lang/String;

    .line 52
    iput-boolean p7, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isBroadcasting:Z

    .line 53
    iput-boolean p8, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFavorite:Z

    .line 54
    iput-boolean p9, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFollowedByCaller:Z

    .line 55
    iput-boolean p10, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFollowingCaller:Z

    .line 56
    iput-boolean p11, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isIdentityShared:Z

    .line 57
    iput-object p12, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->multiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    .line 58
    iput-object p13, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 59
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceState:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 60
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceText:Ljava/lang/String;

    .line 61
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->realName:Ljava/lang/String;

    .line 62
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->xuid:J

    .line 63
    return-void
.end method


# virtual methods
.method public addedDateTimeUtc()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->addedDateTimeUtc:Ljava/util/Date;

    return-object v0
.end method

.method public displayName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public displayPicRaw()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayPicRaw:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178
    if-ne p1, p0, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v1

    .line 181
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;

    if-eqz v3, :cond_c

    move-object v0, p1

    .line 182
    check-cast v0, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;

    .line 183
    .local v0, "that":Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->addedDateTimeUtc:Ljava/util/Date;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->addedDateTimeUtc()Ljava/util/Date;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayName:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 184
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->displayName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayPicRaw:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 185
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->displayPicRaw()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-wide v4, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamerScore:J

    .line 186
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->gamerScore()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamertag:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 187
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->gamertag()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isBroadcasting:Z

    .line 188
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->isBroadcasting()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFavorite:Z

    .line 189
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->isFavorite()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFollowedByCaller:Z

    .line 190
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->isFollowedByCaller()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFollowingCaller:Z

    .line 191
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->isFollowingCaller()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isIdentityShared:Z

    .line 192
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->isIdentityShared()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->multiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    if-nez v3, :cond_7

    .line 193
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->multiplayerSummary()Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-nez v3, :cond_8

    .line 194
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceState:Lcom/microsoft/xbox/service/model/UserStatus;

    if-nez v3, :cond_9

    .line 195
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->presenceState()Lcom/microsoft/xbox/service/model/UserStatus;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceText:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 196
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->presenceText()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->realName:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 197
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->realName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_9
    iget-wide v4, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->xuid:J

    .line 198
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->xuid()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto/16 :goto_0

    .line 183
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->addedDateTimeUtc:Ljava/util/Date;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->addedDateTimeUtc()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_1

    .line 184
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->displayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_2

    .line 185
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayPicRaw:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->displayPicRaw()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_3

    .line 187
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamertag:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->gamertag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_4

    .line 193
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->multiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->multiplayerSummary()Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 194
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_6

    .line 195
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceState:Lcom/microsoft/xbox/service/model/UserStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->presenceState()Lcom/microsoft/xbox/service/model/UserStatus;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/UserStatus;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_7

    .line 196
    :cond_a
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceText:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->presenceText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_8

    .line 197
    :cond_b
    iget-object v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->realName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->realName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_9

    .end local v0    # "that":Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;
    :cond_c
    move v1, v2

    .line 200
    goto/16 :goto_0
.end method

.method public gamerScore()J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamerScore:J

    return-wide v0
.end method

.method public gamertag()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamertag:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 13

    .prologue
    const/16 v12, 0x20

    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v2, 0x0

    const v5, 0xf4243

    .line 205
    const/4 v0, 0x1

    .line 206
    .local v0, "h":I
    mul-int/2addr v0, v5

    .line 207
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->addedDateTimeUtc:Ljava/util/Date;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 208
    mul-int/2addr v0, v5

    .line 209
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayName:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 210
    mul-int/2addr v0, v5

    .line 211
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayPicRaw:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 212
    mul-int/2addr v0, v5

    .line 213
    int-to-long v6, v0

    iget-wide v8, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamerScore:J

    ushr-long/2addr v8, v12

    iget-wide v10, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamerScore:J

    xor-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v0, v6

    .line 214
    mul-int/2addr v0, v5

    .line 215
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamertag:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 216
    mul-int/2addr v0, v5

    .line 217
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isBroadcasting:Z

    if-eqz v1, :cond_4

    move v1, v3

    :goto_4
    xor-int/2addr v0, v1

    .line 218
    mul-int/2addr v0, v5

    .line 219
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFavorite:Z

    if-eqz v1, :cond_5

    move v1, v3

    :goto_5
    xor-int/2addr v0, v1

    .line 220
    mul-int/2addr v0, v5

    .line 221
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFollowedByCaller:Z

    if-eqz v1, :cond_6

    move v1, v3

    :goto_6
    xor-int/2addr v0, v1

    .line 222
    mul-int/2addr v0, v5

    .line 223
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFollowingCaller:Z

    if-eqz v1, :cond_7

    move v1, v3

    :goto_7
    xor-int/2addr v0, v1

    .line 224
    mul-int/2addr v0, v5

    .line 225
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isIdentityShared:Z

    if-eqz v1, :cond_8

    :goto_8
    xor-int/2addr v0, v3

    .line 226
    mul-int/2addr v0, v5

    .line 227
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->multiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    if-nez v1, :cond_9

    move v1, v2

    :goto_9
    xor-int/2addr v0, v1

    .line 228
    mul-int/2addr v0, v5

    .line 229
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-nez v1, :cond_a

    move v1, v2

    :goto_a
    xor-int/2addr v0, v1

    .line 230
    mul-int/2addr v0, v5

    .line 231
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceState:Lcom/microsoft/xbox/service/model/UserStatus;

    if-nez v1, :cond_b

    move v1, v2

    :goto_b
    xor-int/2addr v0, v1

    .line 232
    mul-int/2addr v0, v5

    .line 233
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceText:Ljava/lang/String;

    if-nez v1, :cond_c

    move v1, v2

    :goto_c
    xor-int/2addr v0, v1

    .line 234
    mul-int/2addr v0, v5

    .line 235
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->realName:Ljava/lang/String;

    if-nez v1, :cond_d

    :goto_d
    xor-int/2addr v0, v2

    .line 236
    mul-int/2addr v0, v5

    .line 237
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->xuid:J

    ushr-long/2addr v4, v12

    iget-wide v6, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->xuid:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 238
    return v0

    .line 207
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->addedDateTimeUtc:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    goto :goto_0

    .line 209
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 211
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayPicRaw:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 215
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamertag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    move v1, v4

    .line 217
    goto :goto_4

    :cond_5
    move v1, v4

    .line 219
    goto :goto_5

    :cond_6
    move v1, v4

    .line 221
    goto :goto_6

    :cond_7
    move v1, v4

    .line 223
    goto :goto_7

    :cond_8
    move v3, v4

    .line 225
    goto :goto_8

    .line 227
    :cond_9
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->multiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_9

    .line 229
    :cond_a
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_a

    .line 231
    :cond_b
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceState:Lcom/microsoft/xbox/service/model/UserStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UserStatus;->hashCode()I

    move-result v1

    goto :goto_b

    .line 233
    :cond_c
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    .line 235
    :cond_d
    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->realName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_d
.end method

.method public isBroadcasting()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isBroadcasting:Z

    return v0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFavorite:Z

    return v0
.end method

.method public isFollowedByCaller()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFollowedByCaller:Z

    return v0
.end method

.method public isFollowingCaller()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFollowingCaller:Z

    return v0
.end method

.method public isIdentityShared()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isIdentityShared:Z

    return v0
.end method

.method public multiplayerSummary()Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->multiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    return-object v0
.end method

.method public preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    return-object v0
.end method

.method public presenceState()Lcom/microsoft/xbox/service/model/UserStatus;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceState:Lcom/microsoft/xbox/service/model/UserStatus;

    return-object v0
.end method

.method public presenceText()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceText:Ljava/lang/String;

    return-object v0
.end method

.method public realName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->realName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PeopleHubPerson{addedDateTimeUtc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->addedDateTimeUtc:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", displayName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", displayPicRaw="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->displayPicRaw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gamerScore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamerScore:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gamertag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->gamertag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isBroadcasting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isBroadcasting:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isFavorite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFavorite:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isFollowedByCaller="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFollowedByCaller:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isFollowingCaller="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isFollowingCaller:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isIdentityShared="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->isIdentityShared:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", multiplayerSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->multiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", preferredColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presenceState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceState:Lcom/microsoft/xbox/service/model/UserStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presenceText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->presenceText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", realName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->realName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", xuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->xuid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public xuid()J
    .locals 2

    .prologue
    .line 151
    iget-wide v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;->xuid:J

    return-wide v0
.end method
