.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$MuteRequest;
.super Ljava/lang/Object;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MuteRequest"
.end annotation


# instance fields
.field private final alerts:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "muted"    # Z

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$MuteRequest;->alerts:Z

    .line 169
    return-void

    .line 168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
