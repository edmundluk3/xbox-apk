.class public Lcom/microsoft/xbox/xle/app/activity/TVSeriesOverviewScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "TVSeriesOverviewScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "TVSeriesOverview"

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 25
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/TVSeriesOverviewScreen;->onCreateContentView()V

    .line 29
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    const/4 v2, 0x0

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-direct {v3}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    const v4, 0x7f0e0b79

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;-><init>(I)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;-><init>([Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TVSeriesOverviewScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 30
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 34
    const v0, 0x7f030252

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/TVSeriesOverviewScreen;->setContentView(I)V

    .line 35
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method
