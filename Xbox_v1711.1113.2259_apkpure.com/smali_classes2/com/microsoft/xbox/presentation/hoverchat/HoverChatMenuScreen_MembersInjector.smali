.class public final Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen_MembersInjector;
.super Ljava/lang/Object;
.source "HoverChatMenuScreen_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final telemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen_MembersInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen_MembersInjector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p1, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 19
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen_MembersInjector;->telemetryServiceProvider:Ljavax/inject/Provider;

    .line 20
    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;",
            ">;)",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen_MembersInjector;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectTelemetryService(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    .line 38
    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;)V
    .locals 2
    .param p1, "instance"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;

    .prologue
    .line 29
    if-nez p1, :cond_0

    .line 30
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen_MembersInjector;->telemetryServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    .line 33
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen_MembersInjector;->injectMembers(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;)V

    return-void
.end method
