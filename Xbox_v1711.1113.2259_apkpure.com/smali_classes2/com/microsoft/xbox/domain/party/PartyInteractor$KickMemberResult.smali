.class public abstract Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;
.super Ljava/lang/Object;
.source "PartyInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/party/PartyInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "KickMemberResult"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static completedInstance()Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;
    .locals 2

    .prologue
    .line 489
    new-instance v0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_KickMemberResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_KickMemberResult;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;
    .locals 1
    .param p0, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 493
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 494
    new-instance v0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_KickMemberResult;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_KickMemberResult;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method


# virtual methods
.method public abstract error()Ljava/lang/Throwable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 498
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;->error()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;->isError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KickMemberResult error "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;->error()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 506
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "KickMemberResult completed"

    goto :goto_0
.end method
