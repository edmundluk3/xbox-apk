.class final Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem;
.super Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem;
.source "AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/util/Date;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLcom/google/common/collect/ImmutableList;)V
    .locals 0
    .param p1, "startTime"    # Ljava/util/Date;
    .param p2, "sessionRef"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    .param p3, "accepted"    # J
    .param p5, "visibility"    # Ljava/lang/String;
    .param p6, "joinRestriction"    # Ljava/lang/String;
    .param p7, "readRestriction"    # Ljava/lang/String;
    .param p8, "clubId"    # J
    .param p10, "firstMemberXuid"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p12, "keywords":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p12}, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem;-><init>(Ljava/util/Date;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLcom/google/common/collect/ImmutableList;)V

    .line 22
    return-void
.end method
