.class Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateTopicNameRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final conversationId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final topic:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p3, "conversationId"    # Ljava/lang/String;
    .param p4, "topic"    # Ljava/lang/String;

    .prologue
    .line 2148
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2149
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 2150
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;->conversationId:Ljava/lang/String;

    .line 2151
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;->topic:Ljava/lang/String;

    .line 2152
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2141
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;->buildData()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/lang/Void;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2157
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$500()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;->conversationId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;->topic:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->renameGroupConversation(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 2158
    .local v0, "result":Z
    if-nez v0, :cond_0

    .line 2159
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x264d

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .line 2162
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 2176
    const-wide/16 v0, 0x264d

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2171
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;->conversationId:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;->topic:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1800(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V

    .line 2172
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 2167
    return-void
.end method
