.class public Lcom/microsoft/xbox/authenticate/DelegateKeyService;
.super Landroid/app/Service;
.source "DelegateKeyService.java"


# static fields
.field private static final APP_URI_FMT:Ljava/lang/String; = "android-app://%s.%s"

.field private static final BROKER_SCOPE_FMT:Ljava/lang/String; = "scope=service::user.auth.xboxlive.com::MBI_SSL&client_id=%s"

.field private static final POLICY:Ljava/lang/String; = "TOKEN_BROKER"

.field private static final TAG:Ljava/lang/String; = "DelegateKeyService"

.field private static final appUriWhitelist:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final packageWhitelist:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final signatureWhitelist:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBinder:Lcom/microsoft/xbox/authenticate/IDelegateKeyService$Stub;

.field private final mCallbacks:Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;

.field private resultCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "com.mojang.minecraftvr"

    aput-object v2, v1, v3

    const-string v2, "com.microsoft.onlineid.sample"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->packageWhitelist:Ljava/util/Set;

    .line 47
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "android-app://com.mojang.minecraftvr.CDYMJCBHNIHQLZ4EX3DUNW66DMS2KBQR"

    aput-object v2, v1, v3

    const-string v2, "android-app://com.mojang.minecraftvr.H62DKCBHJP6WXXIV7RBFOGOL4NAK4E6Y"

    aput-object v2, v1, v4

    const-string v2, "android-app://com.microsoft.onlineid.sample.CDYMJCBHNIHQLZ4EX3DUNW66DMS2KBQR"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->appUriWhitelist:Ljava/util/Set;

    .line 55
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "H62DKCBHJP6WXXIV7RBFOGOL4NAK4E6Y"

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->signatureWhitelist:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 64
    sget-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_SUCCESS:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    iput-object v0, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->resultCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 72
    new-instance v0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;-><init>(Lcom/microsoft/xbox/authenticate/DelegateKeyService;)V

    iput-object v0, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->mBinder:Lcom/microsoft/xbox/authenticate/IDelegateKeyService$Stub;

    .line 176
    new-instance v0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/authenticate/DelegateKeyService$2;-><init>(Lcom/microsoft/xbox/authenticate/DelegateKeyService;)V

    iput-object v0, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->mCallbacks:Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;

    return-void
.end method

.method private VerifyAndGetCallingAppUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 19
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 115
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v12

    .line 116
    .local v12, "uid":I
    const-string v13, "DelegateKeyService"

    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v15, "Calling UID: %d"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v14 .. v16}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 121
    .local v8, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v8, v12}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v9

    .line 122
    .local v9, "packages":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 124
    .local v7, "packageId":Ljava/lang/String;
    array-length v14, v9

    const/4 v13, 0x0

    :goto_0
    if-ge v13, v14, :cond_0

    aget-object v10, v9, v13

    .line 125
    .local v10, "pkg":Ljava/lang/String;
    const-string v15, "DelegateKeyService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Found package: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    sget-object v15, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->packageWhitelist:Ljava/util/Set;

    invoke-interface {v15, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 127
    move-object v7, v10

    .line 132
    .end local v10    # "pkg":Ljava/lang/String;
    :cond_0
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 133
    sget-object v13, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_UNKNOWN_PACKAGE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->resultCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 134
    const-string v13, "DelegateKeyService"

    const-string v14, "No applicable package found in the package whitelist."

    invoke-static {v13, v14}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/4 v1, 0x0

    .line 173
    :goto_1
    return-object v1

    .line 124
    .restart local v10    # "pkg":Ljava/lang/String;
    :cond_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 139
    .end local v10    # "pkg":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x0

    .line 141
    .local v6, "info":Landroid/content/pm/PackageInfo;
    const/16 v13, 0x40

    :try_start_0
    invoke-virtual {v8, v7, v13}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 148
    invoke-static {}, Lcom/microsoft/onlineid/sts/Cryptography;->getShaDigester()Ljava/security/MessageDigest;

    move-result-object v2

    .line 149
    .local v2, "digest":Ljava/security/MessageDigest;
    const/4 v4, 0x0

    .line 151
    .local v4, "firstSignature":Ljava/lang/String;
    iget-object v14, v6, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v15, v14

    const/4 v13, 0x0

    :goto_2
    if-ge v13, v15, :cond_5

    aget-object v11, v14, v13

    .line 152
    .local v11, "signature":Landroid/content/pm/Signature;
    invoke-virtual {v11}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/microsoft/onlineid/sts/Cryptography;->encodeBase32([B)Ljava/lang/String;

    move-result-object v5

    .line 153
    .local v5, "hash":Ljava/lang/String;
    sget-object v16, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->signatureWhitelist:Ljava/util/Set;

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_3

    .line 154
    sget-object v13, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_INVALID_SIGNATURE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->resultCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 155
    const-string v13, "DelegateKeyService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Signature not found in whitelist: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const/4 v1, 0x0

    goto :goto_1

    .line 142
    .end local v2    # "digest":Ljava/security/MessageDigest;
    .end local v4    # "firstSignature":Ljava/lang/String;
    .end local v5    # "hash":Ljava/lang/String;
    .end local v11    # "signature":Landroid/content/pm/Signature;
    :catch_0
    move-exception v3

    .line 143
    .local v3, "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v13, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_INVALID_PACKAGE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->resultCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 144
    const-string v13, "DelegateKeyService"

    const-string v14, "Package manager exception occurred"

    invoke-static {v13, v14, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 145
    const/4 v1, 0x0

    goto :goto_1

    .line 159
    .end local v3    # "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "digest":Ljava/security/MessageDigest;
    .restart local v4    # "firstSignature":Ljava/lang/String;
    .restart local v5    # "hash":Ljava/lang/String;
    .restart local v11    # "signature":Landroid/content/pm/Signature;
    :cond_3
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 160
    move-object v4, v5

    .line 151
    :cond_4
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 165
    .end local v5    # "hash":Ljava/lang/String;
    .end local v11    # "signature":Landroid/content/pm/Signature;
    :cond_5
    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "android-app://%s.%s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v7, v15, v16

    const/16 v16, 0x1

    aput-object v4, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 166
    .local v1, "appUri":Ljava/lang/String;
    sget-object v13, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->appUriWhitelist:Ljava/util/Set;

    invoke-interface {v13, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_6

    .line 167
    sget-object v13, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_INVALID_APPURI:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->resultCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 168
    const-string v13, "DelegateKeyService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Generated appUri is not in whitelist: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 172
    :cond_6
    sget-object v13, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_SUCCESS:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->resultCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    goto/16 :goto_1
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/authenticate/DelegateKeyService;Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/authenticate/DelegateKeyService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->VerifyAndGetCallingAppUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/authenticate/DelegateKeyService;)Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->resultCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/authenticate/DelegateKeyService;Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;)Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/authenticate/DelegateKeyService;
    .param p1, "x1"    # Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->resultCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/authenticate/DelegateKeyService;)Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->mCallbacks:Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->mBinder:Lcom/microsoft/xbox/authenticate/IDelegateKeyService$Stub;

    return-object v0
.end method
