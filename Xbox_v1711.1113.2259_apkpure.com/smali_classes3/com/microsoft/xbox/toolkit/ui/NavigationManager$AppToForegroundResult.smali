.class public final enum Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;
.super Ljava/lang/Enum;
.source "NavigationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppToForegroundResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

.field public static final enum AlreadyInForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

.field public static final enum BroughtToForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

.field public static final enum NewActivityStarted:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    const-string v1, "AlreadyInForeground"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->AlreadyInForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    .line 60
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    const-string v1, "BroughtToForeground"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->BroughtToForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    .line 61
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    const-string v1, "NewActivityStarted"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->NewActivityStarted:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    .line 58
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->AlreadyInForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->BroughtToForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->NewActivityStarted:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->$VALUES:[Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    const-class v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->$VALUES:[Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    return-object v0
.end method
