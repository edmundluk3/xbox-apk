.class public abstract Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$BaseCellListener;
.super Ljava/lang/Object;
.source "HomeScreenAdapterUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BaseCellListener"
.end annotation


# instance fields
.field protected final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$BaseCellListener;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final getPopup()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    .locals 1

    .prologue
    .line 256
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v0

    return-object v0
.end method
