.class Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RosterChangeAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final membersResponse:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;"
        }
    .end annotation
.end field

.field private final role:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final rosterService:Lcom/microsoft/xbox/service/clubs/IClubRosterService;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

.field private final type:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

.field private final userIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "type"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "role"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;",
            "Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1201
    .local p3, "userIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1202
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1203
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 1205
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->type:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    .line 1206
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->userIds:Ljava/util/List;

    .line 1207
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->role:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    .line 1208
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    .line 1209
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubRosterService()Lcom/microsoft/xbox/service/clubs/IClubRosterService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->rosterService:Lcom/microsoft/xbox/service/clubs/IClubRosterService;

    .line 1210
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    .line 1211
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;
    .param p3, "x2"    # Ljava/util/List;
    .param p4, "x3"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;
    .param p5, "x4"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
    .param p6, "x5"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;

    .prologue
    .line 1183
    invoke-direct/range {p0 .. p5}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    return-void
.end method

.method static synthetic lambda$loadDataInBackground$0(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;
    .param p1, "batchResponses"    # Ljava/util/List;
    .param p2, "batch"    # Ljava/util/List;

    .prologue
    .line 1253
    const/4 v0, 0x0

    .line 1255
    .local v0, "batchResponse":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->rosterService:Lcom/microsoft/xbox/service/clubs/IClubRosterService;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$400(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)J

    move-result-wide v4

    invoke-interface {v2, v4, v5, p2}, Lcom/microsoft/xbox/service/clubs/IClubRosterService;->addMembersToRoster(JLjava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1260
    :goto_0
    if-eqz v0, :cond_0

    .line 1261
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1265
    :goto_1
    return-void

    .line 1256
    :catch_0
    move-exception v1

    .line 1257
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to modify roster for xuids: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-static {v4, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1263
    .end local v1    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_0
    invoke-static {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;->unknownErrorForXuids(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method static synthetic lambda$loadDataInBackground$1(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;
    .param p1, "batchResponses"    # Ljava/util/List;
    .param p2, "batch"    # Ljava/util/List;

    .prologue
    .line 1274
    const/4 v0, 0x0

    .line 1276
    .local v0, "batchResponse":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->rosterService:Lcom/microsoft/xbox/service/clubs/IClubRosterService;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$400(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)J

    move-result-wide v4

    invoke-interface {v2, v4, v5, p2}, Lcom/microsoft/xbox/service/clubs/IClubRosterService;->removeMembersFromRoster(JLjava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1281
    :goto_0
    if-eqz v0, :cond_0

    .line 1282
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1286
    :goto_1
    return-void

    .line 1277
    :catch_0
    move-exception v1

    .line 1278
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to modify roster for xuids: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-static {v4, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1284
    .end local v1    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_0
    invoke-static {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;->unknownErrorForXuids(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private onPostExecuteFailure()V
    .locals 2

    .prologue
    .line 1389
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    if-eqz v0, :cond_0

    .line 1390
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;->onRosterChangeFailure(Ljava/util/List;)V

    .line 1392
    :cond_0
    return-void
.end method

.method private onPostExecuteSuccess()V
    .locals 5

    .prologue
    .line 1364
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1365
    .local v2, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1367
    .local v1, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    .line 1368
    .local v0, "clubMember":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->getError()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v4

    if-nez v4, :cond_0

    .line 1369
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1371
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1375
    .end local v0    # "clubMember":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$600(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)V

    .line 1377
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    if-eqz v3, :cond_2

    .line 1378
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1379
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    invoke-interface {v3, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;->onRosterChangeSuccess(Ljava/util/List;)V

    .line 1386
    :cond_2
    :goto_1
    return-void

    .line 1380
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1381
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    invoke-interface {v3, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;->onRosterChangeFailure(Ljava/util/List;)V

    goto :goto_1

    .line 1383
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    invoke-interface {v3, v2, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;->onPartialRosterChangeSuccess(Ljava/util/List;Ljava/util/List;)V

    goto :goto_1
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1215
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 18

    .prologue
    .line 1230
    :try_start_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1232
    .local v9, "batchResponses":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;>;"
    sget-object v3, Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;->$SwitchMap$com$microsoft$xbox$service$model$clubs$ClubModel$RosterChangeType:[I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->type:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1324
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown modification type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->type:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 1325
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 1336
    .end local v9    # "batchResponses":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;>;"
    :goto_0
    return-object v3

    .line 1234
    .restart local v9    # "batchResponses":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;>;"
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->userIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 1235
    const/4 v12, 0x0

    .line 1238
    .local v12, "memberResponse":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->rosterService:Lcom/microsoft/xbox/service/clubs/IClubRosterService;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$400(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->userIds:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-interface {v4, v6, v7, v0, v1}, Lcom/microsoft/xbox/service/clubs/IClubRosterService;->addMemberToRoster(JJ)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v12

    .line 1243
    :goto_1
    if-eqz v12, :cond_1

    .line 1244
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    invoke-interface {v3, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1328
    .end local v12    # "memberResponse":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    :cond_0
    :goto_2
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;

    .line 1329
    .local v2, "batchResponse":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;->responses()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_2
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 1334
    .end local v2    # "batchResponse":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;
    .end local v9    # "batchResponses":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;>;"
    :catch_0
    move-exception v11

    .line 1335
    .local v11, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$500()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1336
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 1239
    .end local v11    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    .restart local v9    # "batchResponses":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;>;"
    .restart local v12    # "memberResponse":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    :catch_1
    move-exception v10

    .line 1240
    .local v10, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_3
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$500()Ljava/lang/String;

    move-result-object v4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to modify roster for xuid: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->userIds:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1246
    .end local v10    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$400(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->unknownErrorWithXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1249
    .end local v12    # "memberResponse":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->userIds:Ljava/util/List;

    const/16 v4, 0x14

    move-object/from16 v0, p0

    invoke-static {v0, v9}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ListUtil;->batchAction(Ljava/util/List;ILcom/microsoft/xbox/toolkit/generics/Action;)V

    goto :goto_2

    .line 1270
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->userIds:Ljava/util/List;

    const/16 v4, 0x14

    move-object/from16 v0, p0

    invoke-static {v0, v9}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ListUtil;->batchAction(Ljava/util/List;ILcom/microsoft/xbox/toolkit/generics/Action;)V

    goto/16 :goto_2

    .line 1290
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->role:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 1291
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->userIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    const/4 v3, 0x1

    :goto_4
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1293
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->role:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    if-eqz v3, :cond_0

    .line 1294
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->userIds:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 1295
    .local v14, "xuid":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->rosterService:Lcom/microsoft/xbox/service/clubs/IClubRosterService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$400(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)J

    move-result-wide v4

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->role:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    invoke-interface/range {v3 .. v8}, Lcom/microsoft/xbox/service/clubs/IClubRosterService;->addRoleToMember(JJLcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    move-result-object v13

    .line 1297
    .local v13, "response":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    if-eqz v13, :cond_4

    .line 1298
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    invoke-interface {v3, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1291
    .end local v13    # "response":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .end local v14    # "xuid":Ljava/lang/String;
    :cond_3
    const/4 v3, 0x0

    goto :goto_4

    .line 1300
    .restart local v13    # "response":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .restart local v14    # "xuid":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    invoke-static {v14}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->unknownErrorWithXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1307
    .end local v13    # "response":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .end local v14    # "xuid":Ljava/lang/String;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->role:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 1308
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->userIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    const/4 v3, 0x1

    :goto_5
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1310
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->role:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    if-eqz v3, :cond_0

    .line 1311
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->userIds:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 1312
    .restart local v14    # "xuid":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->rosterService:Lcom/microsoft/xbox/service/clubs/IClubRosterService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$400(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)J

    move-result-wide v4

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->role:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    invoke-interface/range {v3 .. v8}, Lcom/microsoft/xbox/service/clubs/IClubRosterService;->removeRoleFromMember(JJLcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    move-result-object v13

    .line 1314
    .restart local v13    # "response":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    if-eqz v13, :cond_6

    .line 1315
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    invoke-interface {v3, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1308
    .end local v13    # "response":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .end local v14    # "xuid":Ljava/lang/String;
    :cond_5
    const/4 v3, 0x0

    goto :goto_5

    .line 1317
    .restart local v13    # "response":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .restart local v14    # "xuid":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    invoke-static {v14}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->unknownErrorWithXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1332
    .end local v13    # "response":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .end local v14    # "xuid":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->membersResponse:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_8

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto/16 :goto_0

    :cond_8
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :try_end_3
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 1232
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1183
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1224
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1183
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 1220
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1346
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1361
    :goto_0
    return-void

    .line 1349
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->onPostExecuteSuccess()V

    goto :goto_0

    .line 1353
    :pswitch_1
    const-string v0, "Change request returned NO_CHANGE"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 1358
    :pswitch_2
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->onPostExecuteFailure()V

    goto :goto_0

    .line 1346
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1183
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1342
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1396
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Roster change task - (type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->type:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", role: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->role:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userIds: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->userIds:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
