.class public Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountHelper;
.super Ljava/lang/Object;
.source "LinkedAccountHelpers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LinkedAccountHelper"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ToLinkedAccountState(Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;)Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;
    .locals 1
    .param p0, "linkedAccountOptInStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;->Unknown:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountHelper;->ToLinkedAccountState(Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;)Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    move-result-object v0

    return-object v0
.end method

.method public static ToLinkedAccountState(Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;)Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;
    .locals 3
    .param p0, "linkedAccountOptInStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    .param p1, "linkedAccountTokenStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;

    .prologue
    .line 30
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Unknown:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    .line 32
    .local v0, "result":Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;
    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$1;->$SwitchMap$com$microsoft$xbox$service$model$friendfinder$FriendFinderState$LinkedAccountOptInStatus:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 49
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Unknown:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    .line 53
    :goto_0
    return-object v0

    .line 35
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;->TokenRenewalRequired:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;

    if-ne p1, v1, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->NeedsRepair:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    .line 38
    :goto_1
    goto :goto_0

    .line 35
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Linked:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    goto :goto_1

    .line 41
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Unlinked:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    .line 42
    goto :goto_0

    .line 45
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Omitted:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    .line 46
    goto :goto_0

    .line 32
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
