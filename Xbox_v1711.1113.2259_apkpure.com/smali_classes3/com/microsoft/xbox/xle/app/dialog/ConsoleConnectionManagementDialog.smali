.class public Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ConsoleConnectionManagementDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e044f
    .end annotation
.end field

.field navigateToOOBESettingsButton:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0453
    .end annotation
.end field

.field navigateToOOBESettingsButtonText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0454
    .end annotation
.end field

.field private showConfirmationDialog:Landroid/view/View$OnClickListener;

.field toggleConnectionButton:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0450
    .end annotation
.end field

.field toggleConnectionButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0451
    .end annotation
.end field

.field toggleConnectionButtonText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0452
    .end annotation
.end field

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    .prologue
    .line 70
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 48
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->showConfirmationDialog:Landroid/view/View$OnClickListener;

    .line 71
    const v0, 0x7f0300b5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->setContentView(I)V

    .line 72
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Dialog;)Lbutterknife/Unbinder;

    .line 74
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    .line 75
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;Landroid/view/View;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;
    .param p1, "ignore"    # Landroid/view/View;

    .prologue
    .line 49
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->TAG:Ljava/lang/String;

    const-string v1, "Switch console button pressed."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070380

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 53
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07037f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070738

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;)Ljava/lang/Runnable;

    move-result-object v3

    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f070736

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$7;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v5

    .line 51
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 67
    return-void
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;

    .prologue
    .line 56
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    .line 58
    .local v0, "consoleData":Lcom/microsoft/xbox/xle/model/ConsoleData;
    if-eqz v0, :cond_0

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->disconnectConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 61
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackSwitchConsole()V

    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connect()V

    .line 63
    return-void
.end method

.method static synthetic lambda$null$1()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;
    .param p1, "ignore"    # Landroid/view/View;

    .prologue
    .line 123
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackCloseConsoleManager()V

    .line 124
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->dismiss()V

    .line 125
    return-void
.end method

.method static synthetic lambda$onStart$5(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;
    .param p1, "ignore"    # Landroid/view/View;

    .prologue
    .line 131
    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->TAG:Ljava/lang/String;

    const-string v2, "Navigate to oobe settings flow"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackSetupConsole()V

    .line 134
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;)V

    .line 135
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->dismiss()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->TAG:Ljava/lang/String;

    const-string v2, "Failed to navigate to OOBE from console management screen"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$setToggleConnectionButtonState$3(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;
    .param p1, "ignore"    # Landroid/view/View;

    .prologue
    .line 104
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackConnectoToXbox()V

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connect()V

    .line 106
    return-void
.end method


# virtual methods
.method protected onStart()V
    .locals 2

    .prologue
    .line 119
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStart()V

    .line 120
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackViewConsoleManager()V

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->getConnectionStateStream()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 128
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->navigateToOOBESettingsButton:Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    return-void
.end method

.method public setToggleConnectionButtonEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 82
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0142

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 83
    .local v0, "color":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButtonText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    .line 84
    return-void

    .line 82
    .end local v0    # "color":I
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c014d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public setToggleConnectionButtonState(I)V
    .locals 3
    .param p1, "sessionState"    # I

    .prologue
    const/4 v0, 0x1

    .line 92
    packed-switch p1, :pswitch_data_0

    .line 112
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->setToggleConnectionButtonEnabled(Z)V

    .line 115
    :goto_0
    return-void

    .line 94
    :pswitch_1
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->setToggleConnectionButtonEnabled(Z)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07100d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButtonText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07037e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButton:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->showConfirmationDialog:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 100
    :pswitch_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->setToggleConnectionButtonEnabled(Z)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07100e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButtonText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070375

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButton:Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
