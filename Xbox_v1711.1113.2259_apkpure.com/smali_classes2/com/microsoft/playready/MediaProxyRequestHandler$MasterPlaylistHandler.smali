.class Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;
.super Lcom/microsoft/playready/MediaProxyRequestHandler$ElementHandlerBase;
.source "MediaProxyRequestHandler.java"

# interfaces
.implements Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/MediaProxyRequestHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MasterPlaylistHandler"
.end annotation


# instance fields
.field private final mMediaProxy:Lcom/microsoft/playready/MediaProxy;

.field private mStreamPlaylistUrls:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/MediaProxy;Lcom/microsoft/playready/MediaProxyRequestHandler;)V
    .locals 1
    .param p1, "mediaProxy"    # Lcom/microsoft/playready/MediaProxy;
    .param p2, "proxyHandler"    # Lcom/microsoft/playready/MediaProxyRequestHandler;

    .prologue
    .line 210
    invoke-direct {p0, p2}, Lcom/microsoft/playready/MediaProxyRequestHandler$ElementHandlerBase;-><init>(Lcom/microsoft/playready/MediaProxyRequestHandler;)V

    .line 207
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->mStreamPlaylistUrls:Ljava/util/Collection;

    .line 211
    iput-object p1, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    .line 213
    return-void
.end method


# virtual methods
.method public HandleElementRequest(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 11
    .param p1, "request"    # Lorg/apache/http/HttpRequest;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .param p3, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    iget-object v7, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    invoke-virtual {v7}, Lcom/microsoft/playready/MediaProxy;->getHLSMasterPlaylist()Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;

    move-result-object v2

    .line 222
    .local v2, "masterPlaylistInfo":Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;
    iget-object v7, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->mStreamPlaylistUrls:Ljava/util/Collection;

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 229
    invoke-virtual {v2}, Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;->getStreamPlaylistMap()Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 235
    new-instance v6, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;

    iget-object v7, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    invoke-virtual {p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct {v6, v7, v8, v9}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;-><init>(Lcom/microsoft/playready/MediaProxy;Lcom/microsoft/playready/MediaProxyRequestHandler;I)V

    .line 240
    .local v6, "streamPlaylistHandler":Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;
    invoke-virtual {v2}, Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;->getMasterPlaylistBody()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/playready/DeviceAccommodations;->adjustMasterPlaylist(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 241
    .local v3, "playlist":Ljava/lang/String;
    new-instance v0, Lorg/apache/http/entity/ByteArrayEntity;

    const-string v7, "UTF-8"

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v7

    invoke-direct {v0, v7}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 243
    .local v0, "body":Lorg/apache/http/entity/AbstractHttpEntity;
    const/16 v7, 0xc8

    invoke-interface {p2, v7}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 244
    const-string v7, "text/html"

    invoke-virtual {v0, v7}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentType(Ljava/lang/String;)V

    .line 245
    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 246
    return-void

    .line 222
    .end local v0    # "body":Lorg/apache/http/entity/AbstractHttpEntity;
    .end local v3    # "playlist":Ljava/lang/String;
    .end local v6    # "streamPlaylistHandler":Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;
    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 223
    .local v5, "relativeUrl":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/microsoft/playready/MediaProxyRequestHandler;->requestUnregister(Ljava/lang/String;)V

    goto :goto_0

    .line 229
    .end local v5    # "relativeUrl":Ljava/lang/String;
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 230
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/playready/MediaProxyRequestHandler;->getRootUri()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 231
    .local v4, "playlistURL":Ljava/lang/String;
    new-instance v6, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;

    iget-object v9, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    invoke-virtual {p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v10

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {v6, v9, v10, v7}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;-><init>(Lcom/microsoft/playready/MediaProxy;Lcom/microsoft/playready/MediaProxyRequestHandler;I)V

    .line 232
    .restart local v6    # "streamPlaylistHandler":Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;
    invoke-virtual {p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v7

    invoke-virtual {v7, v4, v6}, Lcom/microsoft/playready/MediaProxyRequestHandler;->register(Ljava/lang/String;Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;)V

    .line 233
    iget-object v7, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->mStreamPlaylistUrls:Ljava/util/Collection;

    invoke-interface {v7, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public Unregester()V
    .locals 3

    .prologue
    .line 250
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->mStreamPlaylistUrls:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 253
    return-void

    .line 250
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 251
    .local v0, "relativeUrl":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/playready/MediaProxyRequestHandler;->requestUnregister(Ljava/lang/String;)V

    goto :goto_0
.end method
