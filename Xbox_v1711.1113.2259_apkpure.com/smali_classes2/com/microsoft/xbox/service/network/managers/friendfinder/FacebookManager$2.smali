.class Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;
.super Ljava/lang/Object;
.source "FacebookManager.java"

# interfaces
.implements Lcom/facebook/FacebookCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/FacebookCallback",
        "<",
        "Lcom/facebook/login/LoginResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loadPeopleHubFriendFinderState()V

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->resetFacebookToken(Z)V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$100(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->LoginCancel:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 158
    return-void
.end method

.method public onError(Lcom/facebook/FacebookException;)V
    .locals 4
    .param p1, "e"    # Lcom/facebook/FacebookException;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loadPeopleHubFriendFinderState()V

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->resetFacebookToken(Z)V

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$100(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->LoginError:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$300(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->NONE:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFriendFinderConfirmDialog(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;Z)V

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->SHARE_TO_FACEBOOK:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyFriendFinderConfirmDialogAsyncTaskFailed(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V

    goto :goto_0
.end method

.method public onSuccess(Lcom/facebook/login/LoginResult;)V
    .locals 2
    .param p1, "loginResult"    # Lcom/facebook/login/LoginResult;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-virtual {p1}, Lcom/facebook/login/LoginResult;->getAccessToken()Lcom/facebook/AccessToken;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$202(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/facebook/AccessToken;)Lcom/facebook/AccessToken;

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$200(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/facebook/AccessToken;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$100(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->LoginSuccess:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$300(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$302(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Z)Z

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$200(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/facebook/AccessToken;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/AccessToken;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$402(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Ljava/lang/String;)Ljava/lang/String;

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$500(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v0, v1, :cond_1

    .line 140
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFriendFinderConfirmDialog(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$500(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->TokenRenewalRequired:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v0, v1, :cond_0

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->startUpdateOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    goto :goto_0

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$600(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V

    .line 148
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissFriendFinderConfirmDialog()V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 130
    check-cast p1, Lcom/facebook/login/LoginResult;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;->onSuccess(Lcom/facebook/login/LoginResult;)V

    return-void
.end method
