.class public final enum Lcom/microsoft/xbox/service/model/UpdateType;
.super Ljava/lang/Enum;
.source "UpdateType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AccessTokenRefreshComplete:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AchievementData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ActivitiesSummary:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ActivityAlertsSummary:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ActivityDetail:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AddMemberPrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AlbumDetailsModel:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ApproveToLFG:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ArtistDetailsModel:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AutoConnectCompleted:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AutoConnectStarted:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AutoSuggestResult:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AvatarClosetData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AvatarEditorInitialize:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AvatarEditorLoadedAsset:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AvatarEditorLoadingAsset:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AvatarEditorSave:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AvatarManifestLoad:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AvatarManifestSave:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum AvatarStockClosetData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum BackCompatItems:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum BeamChannelDetails:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum BeamChannelList:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum CheckPlaylistTitleModel:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum CloudCollectionModify:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum CloudCollectionSync:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ClubData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ClubFeedLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum CombinedContentRating:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum CommentDeleted:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum CommentPosted:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ConsoleDiscovery:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ConsolePresence:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ConversationDelete:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ConversationProfilePresenceUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum DiscoverData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum DiscoverMusicData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum DownloadStatusChanged:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ExperimentData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ExternalAccountCreation:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ExternalAccountTroubleshootRequired:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum FeedItemDeleted:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum FriendFinder:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum FriendsData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum FullLoginRequired:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GameData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GamerContext:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GamerscoreLeaderboard:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GamerscoreLeaderboardUri:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GenreList:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GetLfgClubSessions:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GetLfgSessions:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GetLfgSessionsFollowing:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GetLfgSessionsUpcoming:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GetPins:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GetSystemTags:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum GetTrendingTags:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum InitialData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum InternetExplorerData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ItemLikedOrUnliked:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ItemSharedToFeed:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum JoinLFGSession:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum LeaveLFGSession:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum LiveTVSettingsData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum LoadingLogin:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum LoggedIntoXbox:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum LoggingIntoWL:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum LoggingIntoXbox:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum LoginError:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MeProfileData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MediaItemDetail:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MediaItemDetailRelated:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MediaListBrowse:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MergedActivitiesSummary:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MessageData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MessageDelete:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MessageDetailsData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MessageSend:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MessageUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MessageWithAttachementSend:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MoreAllMusicResult:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MoreSearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum MyMusicL1Model:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum NowPlayingDetail:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum NowPlayingHeroActivity:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum NowPlayingQuickplay:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum NowPlayingRelated:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum PeopleHubPersonSummaryLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum PinMoved:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum PinsUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum PlaybackServiceReady:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum PlayerStateChanged:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum PlayerTrackInfoChanged:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum PlaylistChanged:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum PopularNow:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ProfileColorChange:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ProfileData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ProjectLoginError:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ProjectLoginStarted:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ProjectLoginSuccess:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum RecentsData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum RefreshingRootLicense:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum RegisterTuner:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum SearchDetails:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum SearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum SessionLaunchRequestComplete:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum SessionRequestFailure:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum SessionRetryConnectFailed:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum SessionState:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum SkypeToken:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum StatusPosted:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum TitleDataLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum TitleFeedLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum TopicChangePrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum UpdateFriend:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum UserIdentity:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum VersionData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ViewCountIncremented:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum WhiteListData:Lcom/microsoft/xbox/service/model/UpdateType;

.field public static final enum ZestSignIn:Lcom/microsoft/xbox/service/model/UpdateType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "InitialData"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->InitialData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "LoadingLogin"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->LoadingLogin:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "LoggingIntoWL"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->LoggingIntoWL:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "LoggingIntoXbox"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->LoggingIntoXbox:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 27
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "LoggedIntoXbox"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->LoggedIntoXbox:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ProjectLoginStarted"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ProjectLoginStarted:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ProjectLoginSuccess"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ProjectLoginSuccess:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ProjectLoginError"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ProjectLoginError:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "LoginError"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->LoginError:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "FullLoginRequired"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->FullLoginRequired:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ExternalAccountTroubleshootRequired"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ExternalAccountTroubleshootRequired:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ExternalAccountCreation"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ExternalAccountCreation:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AccessTokenRefreshComplete"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AccessTokenRefreshComplete:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MeProfileData"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MeProfileData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GamerContext"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GamerContext:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ConsolePresence"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ConsolePresence:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "CombinedContentRating"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->CombinedContentRating:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ProfileData"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ProfileData:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 30
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "FriendsData"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->FriendsData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GameData"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GameData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AchievementData"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AchievementData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MessageData"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MessageDetailsData"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageDetailsData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MessageUpdated"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MessageDelete"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageDelete:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MessageSend"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageSend:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AvatarManifestLoad"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarManifestLoad:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AvatarManifestSave"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarManifestSave:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AvatarStockClosetData"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarStockClosetData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AvatarClosetData"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarClosetData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AvatarEditorInitialize"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarEditorInitialize:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AvatarEditorLoadingAsset"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarEditorLoadingAsset:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AvatarEditorLoadedAsset"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarEditorLoadedAsset:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AvatarEditorSave"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarEditorSave:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "UpdateFriend"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->UpdateFriend:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "VersionData"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->VersionData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "WhiteListData"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->WhiteListData:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "RecentsData"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->RecentsData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "DiscoverData"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->DiscoverData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MediaItemDetail"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MediaItemDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MediaListBrowse"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MediaListBrowse:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MediaItemDetailRelated"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MediaItemDetailRelated:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "SearchDetails"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->SearchDetails:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "SessionState"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->SessionState:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "SessionRetryConnectFailed"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->SessionRetryConnectFailed:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "SessionLaunchRequestComplete"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->SessionLaunchRequestComplete:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "SessionRequestFailure"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->SessionRequestFailure:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "NowPlayingState"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "NowPlayingDetail"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "NowPlayingRelated"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingRelated:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "NowPlayingQuickplay"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingQuickplay:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "NowPlayingHeroActivity"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingHeroActivity:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "PopularNow"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->PopularNow:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "SearchSummaryResult"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->SearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MoreSearchSummaryResult"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MoreSearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ActivitiesSummary"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ActivitiesSummary:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ActivityAlertsSummary"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ActivityAlertsSummary:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ActivityDetail"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ActivityDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MergedActivitiesSummary"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MergedActivitiesSummary:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ZestSignIn"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ZestSignIn:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "RegisterTuner"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->RegisterTuner:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "RefreshingRootLicense"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->RefreshingRootLicense:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "PlayerStateChanged"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->PlayerStateChanged:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GenreList"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GenreList:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "PlayerTrackInfoChanged"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->PlayerTrackInfoChanged:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "PlaylistChanged"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->PlaylistChanged:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 40
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "DownloadStatusChanged"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->DownloadStatusChanged:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "CloudCollectionSync"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->CloudCollectionSync:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "CloudCollectionModify"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->CloudCollectionModify:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "DiscoverMusicData"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->DiscoverMusicData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AlbumDetailsModel"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AlbumDetailsModel:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ArtistDetailsModel"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ArtistDetailsModel:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MoreAllMusicResult"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MoreAllMusicResult:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "CheckPlaylistTitleModel"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->CheckPlaylistTitleModel:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AutoSuggestResult"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AutoSuggestResult:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MyMusicL1Model"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MyMusicL1Model:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 42
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "PlaybackServiceReady"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->PlaybackServiceReady:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "UserIdentity"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->UserIdentity:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "LiveTVSettingsData"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->LiveTVSettingsData:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 45
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ConsoleDiscovery"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ConsoleDiscovery:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 46
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "InternetExplorerData"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->InternetExplorerData:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 47
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AutoConnectStarted"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AutoConnectStarted:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 48
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AutoConnectCompleted"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AutoConnectCompleted:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GetPins"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GetPins:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "PinsUpdated"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->PinsUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "PinMoved"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->PinMoved:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ViewCountIncremented"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ViewCountIncremented:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ConversationDelete"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ConversationDelete:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "MessageWithAttachementSend"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageWithAttachementSend:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 51
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "StatusPosted"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->StatusPosted:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "CommentPosted"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->CommentPosted:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ItemSharedToFeed"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ItemSharedToFeed:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ItemLikedOrUnliked"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ItemLikedOrUnliked:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "FeedItemDeleted"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->FeedItemDeleted:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "CommentDeleted"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->CommentDeleted:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "TitleFeedLoaded"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->TitleFeedLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "TitleDataLoaded"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->TitleDataLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 53
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "FriendFinder"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->FriendFinder:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 54
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "SkypeToken"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->SkypeToken:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 55
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ProfileColorChange"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ProfileColorChange:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 56
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GetLfgSessions"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgSessions:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "JoinLFGSession"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->JoinLFGSession:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "LeaveLFGSession"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->LeaveLFGSession:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ApproveToLFG"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ApproveToLFG:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GetLfgSessionsUpcoming"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgSessionsUpcoming:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GetLfgSessionsFollowing"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgSessionsFollowing:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GetLfgClubSessions"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgClubSessions:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 57
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ClubData"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ClubData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ClubFeedLoaded"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ClubFeedLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 58
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GetSystemTags"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GetSystemTags:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GetTrendingTags"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GetTrendingTags:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 59
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "BackCompatItems"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->BackCompatItems:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 60
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "PeopleHubPersonSummaryLoaded"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->PeopleHubPersonSummaryLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 61
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "AddMemberPrivacyLoaded"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->AddMemberPrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 62
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "TopicChangePrivacyLoaded"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->TopicChangePrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 63
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ExperimentData"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ExperimentData:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 64
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "BeamChannelList"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->BeamChannelList:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 65
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "BeamChannelDetails"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->BeamChannelDetails:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 66
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "ConversationProfilePresenceUpdated"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ConversationProfilePresenceUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 67
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GamerscoreLeaderboard"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GamerscoreLeaderboard:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 68
    new-instance v0, Lcom/microsoft/xbox/service/model/UpdateType;

    const-string v1, "GamerscoreLeaderboardUri"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GamerscoreLeaderboardUri:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 24
    const/16 v0, 0x79

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/UpdateType;

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->InitialData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->LoadingLogin:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->LoggingIntoWL:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->LoggingIntoXbox:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->LoggedIntoXbox:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ProjectLoginStarted:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ProjectLoginSuccess:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ProjectLoginError:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->LoginError:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->FullLoginRequired:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ExternalAccountTroubleshootRequired:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ExternalAccountCreation:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AccessTokenRefreshComplete:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MeProfileData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GamerContext:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ConsolePresence:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->CombinedContentRating:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ProfileData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->FriendsData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GameData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AchievementData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MessageData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MessageDetailsData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MessageUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MessageDelete:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MessageSend:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarManifestLoad:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarManifestSave:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarStockClosetData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarClosetData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarEditorInitialize:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarEditorLoadingAsset:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarEditorLoadedAsset:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AvatarEditorSave:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->UpdateFriend:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->VersionData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->WhiteListData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->RecentsData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->DiscoverData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MediaItemDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MediaListBrowse:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MediaItemDetailRelated:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->SearchDetails:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->SessionState:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->SessionRetryConnectFailed:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->SessionLaunchRequestComplete:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->SessionRequestFailure:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingRelated:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingQuickplay:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingHeroActivity:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->PopularNow:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->SearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MoreSearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ActivitiesSummary:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ActivityAlertsSummary:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ActivityDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MergedActivitiesSummary:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ZestSignIn:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->RegisterTuner:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->RefreshingRootLicense:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->PlayerStateChanged:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GenreList:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->PlayerTrackInfoChanged:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->PlaylistChanged:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->DownloadStatusChanged:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->CloudCollectionSync:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->CloudCollectionModify:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->DiscoverMusicData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AlbumDetailsModel:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ArtistDetailsModel:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MoreAllMusicResult:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->CheckPlaylistTitleModel:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AutoSuggestResult:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MyMusicL1Model:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->PlaybackServiceReady:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->UserIdentity:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->LiveTVSettingsData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ConsoleDiscovery:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->InternetExplorerData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AutoConnectStarted:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AutoConnectCompleted:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetPins:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->PinsUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->PinMoved:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ViewCountIncremented:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ConversationDelete:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MessageWithAttachementSend:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->StatusPosted:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->CommentPosted:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ItemSharedToFeed:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ItemLikedOrUnliked:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->FeedItemDeleted:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->CommentDeleted:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->TitleFeedLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->TitleDataLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->FriendFinder:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->SkypeToken:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ProfileColorChange:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgSessions:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->JoinLFGSession:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->LeaveLFGSession:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ApproveToLFG:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgSessionsUpcoming:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgSessionsFollowing:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgClubSessions:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ClubData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ClubFeedLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetSystemTags:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetTrendingTags:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->BackCompatItems:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->PeopleHubPersonSummaryLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AddMemberPrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->TopicChangePrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ExperimentData:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->BeamChannelList:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->BeamChannelDetails:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ConversationProfilePresenceUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GamerscoreLeaderboard:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GamerscoreLeaderboardUri:Lcom/microsoft/xbox/service/model/UpdateType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->$VALUES:[Lcom/microsoft/xbox/service/model/UpdateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/UpdateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/UpdateType;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->$VALUES:[Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/UpdateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/UpdateType;

    return-object v0
.end method
