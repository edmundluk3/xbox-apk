.class abstract Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;
.super Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
.source "$AutoValue_EditorialDataTypes_SystemTag.java"


# instance fields
.field private final icon:Ljava/lang/String;

.field private final id:Ljava/lang/String;

.field private final name:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "icon"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null id"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->id:Ljava/lang/String;

    .line 23
    if-nez p2, :cond_1

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null icon"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->icon:Ljava/lang/String;

    .line 27
    if-nez p3, :cond_2

    .line 28
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null name"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->name:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 65
    if-ne p1, p0, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v1

    .line 68
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 69
    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    .line 70
    .local v0, "that":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->id:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;->id()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->icon:Ljava/lang/String;

    .line 71
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;->icon()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->name:Ljava/lang/String;

    .line 72
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    :cond_3
    move v1, v2

    .line 74
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 79
    const/4 v0, 0x1

    .line 80
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 82
    mul-int/2addr v0, v2

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->icon:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 84
    mul-int/2addr v0, v2

    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 86
    return v0
.end method

.method public icon()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "systemTag.tagEmoji"
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->icon:Ljava/lang/String;

    return-object v0
.end method

.method public id()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "systemTag.tagID"
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->id:Ljava/lang/String;

    return-object v0
.end method

.method public name()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "systemTag.tagName"
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->name:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SystemTag{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", icon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->icon:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTag;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
