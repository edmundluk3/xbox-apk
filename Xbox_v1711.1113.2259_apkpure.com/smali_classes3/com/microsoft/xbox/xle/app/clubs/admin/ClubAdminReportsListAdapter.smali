.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;
.super Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;
.source "ClubAdminReportsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final COMMENT_VIEW_TYPE:I = 0x1

.field private static final FEED_VIEW_TYPE:I = 0x0

.field private static final MESSAGE_VIEW_TYPE:I = 0x2


# instance fields
.field private final actionListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

.field private final clubId:J

.field private final viewerIsOwner:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;ZJ)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "viewerIsOwner"    # Z
    .param p3, "clubId"    # J

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;-><init>()V

    .line 63
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->actionListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    .line 65
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->viewerIsOwner:Z

    .line 66
    iput-wide p3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->clubId:J

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->viewerIsOwner:Z

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->clubId:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->actionListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .line 100
    .local v0, "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    if-eqz v0, :cond_3

    .line 101
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;

    if-eqz v1, :cond_0

    .line 102
    const/4 v1, 0x0

    .line 112
    :goto_0
    return v1

    .line 103
    :cond_0
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;

    if-eqz v1, :cond_1

    .line 104
    const/4 v1, 0x1

    goto :goto_0

    .line 105
    :cond_1
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;

    if-eqz v1, :cond_2

    .line 106
    const/4 v1, 0x2

    goto :goto_0

    .line 108
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown ISocialListItem type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 112
    :cond_3
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 90
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .line 91
    .local v0, "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    .line 94
    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 71
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03005b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 73
    .local v0, "v":Landroid/view/View;
    packed-switch p2, :pswitch_data_0

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown viewType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 85
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 75
    :pswitch_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 78
    :pswitch_1
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 81
    :pswitch_2
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
