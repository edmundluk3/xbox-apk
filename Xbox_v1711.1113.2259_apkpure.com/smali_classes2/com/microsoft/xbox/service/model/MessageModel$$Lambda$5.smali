.class final synthetic Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final arg$2:Ljava/util/List;

.field private final arg$3:Ljava/lang/String;

.field private final arg$4:Lcom/microsoft/xbox/service/model/UpdateType;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UpdateType;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;->arg$1:Lcom/microsoft/xbox/service/model/MessageModel;

    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;->arg$2:Ljava/util/List;

    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;->arg$3:Ljava/lang/String;

    iput-object p4, p0, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;->arg$4:Lcom/microsoft/xbox/service/model/UpdateType;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UpdateType;)Ljava/lang/Runnable;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UpdateType;)V

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;->arg$1:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;->arg$2:Ljava/util/List;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;->arg$3:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;->arg$4:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->lambda$canContributeToGroupConversationAsync$7(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UpdateType;)V

    return-void
.end method
