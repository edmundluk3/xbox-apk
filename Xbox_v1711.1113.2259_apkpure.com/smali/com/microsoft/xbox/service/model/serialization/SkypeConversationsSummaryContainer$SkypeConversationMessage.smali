.class public Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
.super Ljava/lang/Object;
.source "SkypeConversationsSummaryContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SkypeConversationMessage"
.end annotation


# instance fields
.field public clientmessageid:Ljava/lang/String;

.field public content:Ljava/lang/String;

.field public conversationId:Ljava/lang/String;

.field public conversationLink:Ljava/lang/String;

.field public from:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public messagetype:Ljava/lang/String;

.field public originalarrivaltime:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;
    .end annotation
.end field

.field public skypeeditedid:Ljava/lang/String;

.field public syncState:Ljava/lang/String;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSkypeAttachmentMessageContent(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;
    .locals 6
    .param p0, "content"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 164
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 166
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 167
    .local v1, "root":Lorg/json/JSONObject;
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;-><init>()V

    .line 168
    .local v2, "skypeAttachmentContent":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;
    const-string v4, "message"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;->message:Ljava/lang/String;

    .line 169
    const-string v4, "locator"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;->locator:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    .end local v1    # "root":Lorg/json/JSONObject;
    .end local v2    # "skypeAttachmentContent":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;
    :goto_0
    return-object v2

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Lorg/json/JSONException;
    const-string v4, "SkypeConversationMessage"

    const-string v5, "Error while parsing skype attachment content"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 174
    goto :goto_0

    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    move-object v2, v3

    .line 178
    goto :goto_0
.end method


# virtual methods
.method public getContent()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 145
    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Activity:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 146
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSkypeAttachmentMessageContent(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;

    move-result-object v1

    .line 147
    .local v1, "skypeAttachmentMessage":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;
    if-eqz v1, :cond_0

    .line 148
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;->message:Ljava/lang/String;

    .line 159
    .end local v1    # "skypeAttachmentMessage":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;
    :cond_0
    :goto_0
    return-object v2

    .line 152
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Service:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 153
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;->deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;

    move-result-object v0

    .line 154
    .local v0, "serviceMessageContent":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;->getServiceMessageText()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 155
    .end local v0    # "serviceMessageContent":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Attachment:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 156
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07075d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 159
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    goto :goto_0
.end method

.method public getConversationSkypeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationLink:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getSkypeIdFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPartnerXuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationLink:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSenderSkypeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->from:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getSkypeIdFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSenderXuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->from:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasAttachment()Z
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Activity:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
