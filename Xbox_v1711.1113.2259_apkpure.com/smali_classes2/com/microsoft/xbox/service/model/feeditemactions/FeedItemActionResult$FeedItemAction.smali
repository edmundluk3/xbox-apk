.class public Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
.super Ljava/lang/Object;
.source "FeedItemActionResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FeedItemAction"
.end annotation


# instance fields
.field public date:Ljava/util/Date;

.field public gamertag:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public parentPath:Ljava/lang/String;

.field public path:Ljava/lang/String;

.field public profileUser:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;

.field public rootPath:Ljava/lang/String;

.field public socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

.field public text:Ljava/lang/String;

.field public xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    return-void
.end method


# virtual methods
.method public getGamerPicUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->profileUser:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->profileUser:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->profileUser:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->profileUser:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;->getGamerTag()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPathForLikeInfoRequest()Ljava/lang/String;
    .locals 2

    .prologue
    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "comments.xboxlive.com/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->getPathForSummaryRequest()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPathForSummaryRequest()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->rootPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/comments"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRealName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->profileUser:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->profileUser:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;->getRealName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
