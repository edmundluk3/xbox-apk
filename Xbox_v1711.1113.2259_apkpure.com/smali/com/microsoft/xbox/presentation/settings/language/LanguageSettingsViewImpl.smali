.class public Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;
.super Lcom/microsoft/xbox/presentation/base/MviActivityBase;
.source "LanguageSettingsViewImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsView;


# instance fields
.field private currentPrefs:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

.field private firstRefreshComplete:Z

.field private languageAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;",
            ">;"
        }
    .end annotation
.end field

.field languageDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e09f1
    .end annotation
.end field

.field private languageSettingSpinnerItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;",
            ">;"
        }
    .end annotation
.end field

.field languageSpinner:Landroid/widget/Spinner;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e09f0
    .end annotation
.end field

.field languageTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e09ef
    .end annotation
.end field

.field languageWarningText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e09f5
    .end annotation
.end field

.field private locationAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;",
            ">;"
        }
    .end annotation
.end field

.field locationDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e09f4
    .end annotation
.end field

.field private locationSettingSpinnerItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;",
            ">;"
        }
    .end annotation
.end field

.field locationSpinner:Landroid/widget/Spinner;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e09f3
    .end annotation
.end field

.field locationTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e09f2
    .end annotation
.end field

.field presenter:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private viewIntents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->firstRefreshComplete:Z

    .line 80
    return-void
.end method

.method private buildLanguageSpinner()V
    .locals 13

    .prologue
    const v12, 0x7f03020a

    .line 197
    invoke-static {}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->values()[Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .line 198
    .local v6, "supportedLanguages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    invoke-direct {v2, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 201
    .local v2, "languageSettingSpinnerItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 202
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v8, 0x0

    invoke-virtual {v0, v12, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 203
    .local v5, "sampleSpinnerView":Landroid/widget/TextView;
    invoke-virtual {v5}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v7

    .line 204
    .local v7, "textMeasurer":Landroid/graphics/Paint;
    const/4 v3, 0x0

    .line 206
    .local v3, "maxTextSize":F
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    .line 207
    .local v1, "language":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    invoke-static {v1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;->with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;

    move-result-object v4

    .line 208
    .local v4, "newItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;
    invoke-virtual {v4}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v9

    invoke-static {v3, v9}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 209
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 212
    .end local v1    # "language":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    .end local v4    # "newItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;
    :cond_0
    iput-object v2, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageSettingSpinnerItems:Ljava/util/List;

    .line 214
    iget-object v8, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    if-nez v8, :cond_1

    .line 215
    new-instance v8, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f030202

    iget-object v11, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageSettingSpinnerItems:Ljava/util/List;

    invoke-direct {v8, v9, v10, v11}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v8, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 216
    iget-object v8, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v8, v12}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 217
    iget-object v8, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageSpinner:Landroid/widget/Spinner;

    iget-object v9, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v8, v9}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 223
    :goto_1
    iget-object v8, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageSpinner:Landroid/widget/Spinner;

    float-to-int v9, v3

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v8, v9}, Landroid/widget/Spinner;->setDropDownWidth(I)V

    .line 224
    return-void

    .line 219
    :cond_1
    iget-object v8, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->clear()V

    .line 220
    iget-object v8, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    iget-object v9, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageSettingSpinnerItems:Ljava/util/List;

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->addAll(Ljava/util/Collection;)V

    goto :goto_1
.end method

.method private buildLocationSpinner(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)V
    .locals 14
    .param p1, "language"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    .prologue
    const v13, 0x7f03020a

    .line 228
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->getSettableValues()Ljava/util/List;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 229
    .local v7, "supportedLocations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 232
    .local v3, "locationSettingSpinnerItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 233
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v9, 0x0

    invoke-virtual {v0, v13, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 234
    .local v6, "sampleSpinnerView":Landroid/widget/TextView;
    invoke-virtual {v6}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v8

    .line 235
    .local v8, "textMeasurer":Landroid/graphics/Paint;
    const/4 v4, 0x0

    .line 237
    .local v4, "maxTextSize":F
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    .line 238
    .local v2, "location":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    invoke-static {v2}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;->with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;

    move-result-object v5

    .line 239
    .local v5, "newItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    invoke-virtual {v5}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v10

    invoke-static {v4, v10}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 240
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 243
    .end local v2    # "location":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .end local v5    # "newItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->getLocale()Ljava/util/Locale;

    move-result-object v9

    invoke-static {v9}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v1

    .line 244
    .local v1, "langSpecificComparator":Ljava/text/Collator;
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Ljava/text/Collator;->setStrength(I)V

    .line 245
    invoke-static {v1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$13;->lambdaFactory$(Ljava/text/Collator;)Ljava/util/Comparator;

    move-result-object v9

    invoke-static {v3, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 247
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSettingSpinnerItems:Ljava/util/List;

    .line 248
    new-instance v9, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->getContext()Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f030202

    iget-object v12, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSettingSpinnerItems:Ljava/util/List;

    invoke-direct {v9, v10, v11, v12}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v9, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 249
    iget-object v9, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v9, v13}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 250
    iget-object v9, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSpinner:Landroid/widget/Spinner;

    iget-object v10, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v9, v10}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 251
    iget-object v9, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSpinner:Landroid/widget/Spinner;

    float-to-int v10, v4

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/Spinner;->setDropDownWidth(I)V

    .line 252
    return-void
.end method

.method static synthetic lambda$buildLocationSpinner$8(Ljava/text/Collator;Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;)I
    .locals 2
    .param p0, "langSpecificComparator"    # Ljava/text/Collator;
    .param p1, "lhs"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    .param p2, "rhs"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;

    .prologue
    .line 245
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic lambda$onCreateContentView$0(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;Ljava/lang/Integer;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;
    .param p1, "ignore"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->firstRefreshComplete:Z

    return v0
.end method

.method static synthetic lambda$onCreateContentView$1(Ljava/lang/Integer;)Z
    .locals 2
    .param p0, "index"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 103
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreateContentView$2(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;Ljava/lang/Integer;)Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;
    .param p1, "index"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$3(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;
    .param p1, "newLanguage"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->currentPrefs:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v0

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreateContentView$4(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;Ljava/lang/Integer;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;
    .param p1, "ignore"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->firstRefreshComplete:Z

    return v0
.end method

.method static synthetic lambda$onCreateContentView$5(Ljava/lang/Integer;)Z
    .locals 2
    .param p0, "index"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 112
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreateContentView$6(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;Ljava/lang/Integer;)Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;
    .param p1, "index"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$7(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;
    .param p1, "newLocation"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->currentPrefs:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentMarket()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v0

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    const-class v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070b8a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->presenter:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;->onCreate()V

    .line 85
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)V

    .line 86
    return-void
.end method

.method public onCreateContentView()V
    .locals 4

    .prologue
    .line 95
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;->onCreateContentView()V

    .line 97
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->buildLanguageSpinner()V

    .line 98
    sget-object v2, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->DefaultLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->buildLocationSpinner(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)V

    .line 100
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageSpinner:Landroid/widget/Spinner;

    .line 101
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/widget/RxAdapterView;->itemSelections(Landroid/widget/AdapterView;)Lcom/jakewharton/rxbinding2/InitialValueObservable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 102
    invoke-virtual {v2, v3}, Lcom/jakewharton/rxbinding2/InitialValueObservable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 103
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 104
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 105
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 106
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 107
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 109
    .local v0, "languageChanged":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LanguageChangedIntent;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSpinner:Landroid/widget/Spinner;

    .line 110
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/widget/RxAdapterView;->itemSelections(Landroid/widget/AdapterView;)Lcom/jakewharton/rxbinding2/InitialValueObservable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 111
    invoke-virtual {v2, v3}, Lcom/jakewharton/rxbinding2/InitialValueObservable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 112
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 113
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$10;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 114
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 115
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$12;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 116
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 118
    .local v1, "locationChanged":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LocationChangedIntent;>;"
    invoke-static {v0, v1}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->viewIntents:Lio/reactivex/Observable;

    .line 119
    return-void
.end method

.method public render(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;)V
    .locals 6
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;

    .prologue
    .line 140
    iget-boolean v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->firstRefreshComplete:Z

    if-nez v4, :cond_5

    .line 141
    const/4 v1, 0x0

    .line 142
    .local v1, "languageSpinnerUpdated":Z
    const/4 v2, 0x0

    .line 145
    .local v2, "locationSpinnerUpdated":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageSettingSpinnerItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 146
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageSettingSpinnerItems:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;

    .line 147
    .local v3, "spinnerItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;
    invoke-virtual {v3}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;->language()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;->prefs()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v5

    if-ne v4, v5, :cond_3

    .line 148
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 149
    const/4 v1, 0x1

    .line 154
    .end local v3    # "spinnerItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSettingSpinnerItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 155
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSettingSpinnerItems:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;

    .line 156
    .local v3, "spinnerItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    invoke-virtual {v3}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;->location()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;->prefs()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentMarket()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v5

    if-ne v4, v5, :cond_4

    .line 157
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 158
    const/4 v2, 0x1

    .line 163
    .end local v3    # "spinnerItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Language spinner not updated; expected lang: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;->prefs()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 164
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Location spinner not updated; expected loc: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;->prefs()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentMarket()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 166
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->firstRefreshComplete:Z

    .line 188
    .end local v0    # "i":I
    .end local v1    # "languageSpinnerUpdated":Z
    .end local v2    # "locationSpinnerUpdated":Z
    :cond_2
    :goto_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;->prefs()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->currentPrefs:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    .line 189
    return-void

    .line 145
    .restart local v0    # "i":I
    .restart local v1    # "languageSpinnerUpdated":Z
    .restart local v2    # "locationSpinnerUpdated":Z
    .local v3, "spinnerItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LanguageSettingSpinnerItem;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 154
    .local v3, "spinnerItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 169
    .end local v0    # "i":I
    .end local v1    # "languageSpinnerUpdated":Z
    .end local v2    # "locationSpinnerUpdated":Z
    .end local v3    # "spinnerItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    :cond_5
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;->prefs()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->currentPrefs:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    invoke-virtual {v5}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v5

    if-eq v4, v5, :cond_2

    .line 170
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;->prefs()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->buildLocationSpinner(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)V

    .line 171
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSettingSpinnerItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_6

    .line 172
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSettingSpinnerItems:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;

    .line 173
    .restart local v3    # "spinnerItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    invoke-virtual {v3}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;->location()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;->prefs()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentMarket()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v5

    if-ne v4, v5, :cond_7

    .line 174
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 179
    .end local v3    # "spinnerItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    :cond_6
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v5, 0x7f070b96

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 180
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v5, 0x7f070b83

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 181
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v5, 0x7f070bc1

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 182
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v5, 0x7f070ba5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 183
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageWarningText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v5, 0x7f070b98

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 184
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageWarningText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_2

    .line 171
    .restart local v3    # "spinnerItem":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public bridge synthetic render(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->render(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;)V

    return-void
.end method

.method protected setContentView()V
    .locals 1

    .prologue
    .line 90
    const v0, 0x7f0301f9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->setContentView(I)V

    .line 91
    return-void
.end method

.method public viewIntents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->viewIntents:Lio/reactivex/Observable;

    return-object v0
.end method
