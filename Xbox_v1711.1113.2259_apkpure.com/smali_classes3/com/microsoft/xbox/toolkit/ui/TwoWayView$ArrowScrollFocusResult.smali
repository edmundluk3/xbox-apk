.class Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;
.super Ljava/lang/Object;
.source "TwoWayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArrowScrollFocusResult"
.end annotation


# instance fields
.field private mAmountToScroll:I

.field private mSelectedPosition:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 6152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;

    .prologue
    .line 6152
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;-><init>()V

    return-void
.end method


# virtual methods
.method public getAmountToScroll()I
    .locals 1

    .prologue
    .line 6169
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;->mAmountToScroll:I

    return v0
.end method

.method public getSelectedPosition()I
    .locals 1

    .prologue
    .line 6165
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;->mSelectedPosition:I

    return v0
.end method

.method populate(II)V
    .locals 0
    .param p1, "selectedPosition"    # I
    .param p2, "amountToScroll"    # I

    .prologue
    .line 6160
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;->mSelectedPosition:I

    .line 6161
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;->mAmountToScroll:I

    .line 6162
    return-void
.end method
