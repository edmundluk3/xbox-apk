.class public Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;
.super Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$BaseCellListener;
.source "HomeScreenAdapterUtil.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CellListenerPins"
.end annotation


# instance fields
.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$BaseCellListener;-><init>()V

    .line 264
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .line 265
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;

    .prologue
    .line 260
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    return-object v0
.end method


# virtual methods
.method public bindPin(Landroid/view/View;Lcom/microsoft/xbox/service/model/pins/PinItem;I)V
    .locals 6
    .param p1, "cell"    # Landroid/view/View;
    .param p2, "pin"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p3, "position"    # I

    .prologue
    const v5, 0x7f0e001e

    const v3, 0x7f0e001a

    const/4 v4, 0x0

    .line 291
    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 292
    .local v2, "oldPin":Lcom/microsoft/xbox/service/model/pins/PinItem;
    if-eq p2, v2, :cond_5

    .line 293
    invoke-virtual {p1, v3, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 294
    const v3, 0x7f0e06d6

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 295
    .local v0, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    if-nez p2, :cond_0

    .line 296
    invoke-virtual {p1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    invoke-virtual {p1, v5, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 298
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->clearImage()V

    .line 299
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundResource(I)V

    .line 300
    invoke-virtual {p1, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 301
    invoke-static {p1, v4, v4}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->bindCellTitle(Landroid/view/View;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 302
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;->TAG:Ljava/lang/String;

    const-string v4, "Null pin, hide the image"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    .end local v0    # "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :goto_0
    return-void

    .line 304
    .restart local v0    # "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_0
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 305
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v5, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 306
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/pins/PinItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 307
    .local v1, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getIsWebLink()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 308
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;->TAG:Ljava/lang/String;

    const-string v4, "Binding WebLink"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const v3, 0x7f070f64

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    .line 310
    const v3, 0x7f0c00f6

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundResource(I)V

    .line 319
    :goto_1
    iget-object v3, p2, Lcom/microsoft/xbox/service/model/pins/PinItem;->Title:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 320
    invoke-static {p1, p2, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->bindCellTitle(Landroid/view/View;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 311
    :cond_1
    iget-object v3, p2, Lcom/microsoft/xbox/service/model/pins/PinItem;->ImageUrl:Ljava/lang/String;

    if-eqz v3, :cond_2

    invoke-static {p2}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 312
    :cond_2
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v3

    if-eqz v3, :cond_3

    const v3, 0x7f070fca

    :goto_2
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    .line 313
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setUserBackground(Landroid/view/View;)V

    goto :goto_1

    .line 312
    :cond_3
    const v3, 0x7f070fb5

    goto :goto_2

    .line 315
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Binding regular media item "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lcom/microsoft/xbox/service/model/pins/PinItem;->Title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v3, p2, Lcom/microsoft/xbox/service/model/pins/PinItem;->ImageUrl:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setImageUrl(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Ljava/lang/String;)V

    .line 317
    invoke-static {p2, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setUserBackgroundOptional(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/view/View;)V

    goto :goto_1

    .line 323
    .end local v0    # "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .end local v1    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;->TAG:Ljava/lang/String;

    const-string v4, "Same pin, no binding necessary"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "cell"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 269
    const v7, 0x7f0e001a

    invoke-virtual {p1, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 270
    .local v2, "pin":Lcom/microsoft/xbox/service/model/pins/PinItem;
    const v7, 0x7f0e001e

    invoke-virtual {p1, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 271
    .local v0, "indexPosition":Ljava/lang/Integer;
    if-nez v0, :cond_1

    move v4, v5

    .line 272
    .local v4, "realIndex":I
    :goto_0
    if-eqz v2, :cond_0

    move v5, v6

    :cond_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 273
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedPin(Lcom/microsoft/xbox/service/model/pins/PinItem;)V

    .line 275
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 276
    .local v1, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v5, "MediaId"

    iget-object v7, v2, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    invoke-virtual {v1, v5, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 277
    const-string v5, "ListIndex"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v5, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 278
    const-string v5, "Home Pins"

    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 280
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;->getPopup()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v3

    .line 281
    .local v3, "popup":Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    new-instance v5, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;

    new-instance v7, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins$1;

    invoke-direct {v7, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;)V

    invoke-direct {v5, v7}, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;-><init>(Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;)V

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->setScreen(Landroid/view/View;)V

    .line 287
    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeFullScreen(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeTransparent(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->show()V

    .line 288
    return-void

    .line 271
    .end local v1    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .end local v3    # "popup":Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    .end local v4    # "realIndex":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_0
.end method
