.class public final Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes;
.super Ljava/lang/Object;
.source "PrivacyDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;,
        Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;,
        Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;,
        Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;,
        Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;,
        Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionResponse;,
        Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;,
        Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;,
        Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionTypes;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type cannot be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
