.class abstract Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;
.super Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;
.source "$AutoValue_MediaHubDataTypes_ContentSegment.java"


# instance fields
.field private final creationType:Ljava/lang/String;

.field private final creatorXuid:Ljava/lang/Long;

.field private final durationInSeconds:Ljava/lang/Integer;

.field private final offset:Ljava/lang/Integer;

.field private final recordDate:Ljava/util/Date;

.field private final segmentId:Ljava/lang/Long;

.field private final titleId:Ljava/lang/Long;


# direct methods
.method constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V
    .locals 0
    .param p1, "segmentId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "creationType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "creatorXuid"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "recordDate"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "durationInSeconds"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "offset"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "titleId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->segmentId:Ljava/lang/Long;

    .line 28
    iput-object p2, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creationType:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creatorXuid:Ljava/lang/Long;

    .line 30
    iput-object p4, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->recordDate:Ljava/util/Date;

    .line 31
    iput-object p5, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->durationInSeconds:Ljava/lang/Integer;

    .line 32
    iput-object p6, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->offset:Ljava/lang/Integer;

    .line 33
    iput-object p7, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->titleId:Ljava/lang/Long;

    .line 34
    return-void
.end method


# virtual methods
.method public creationType()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creationType:Ljava/lang/String;

    return-object v0
.end method

.method public creatorXuid()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creatorXuid:Ljava/lang/Long;

    return-object v0
.end method

.method public durationInSeconds()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->durationInSeconds:Ljava/lang/Integer;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 93
    if-ne p1, p0, :cond_1

    .line 106
    :cond_0
    :goto_0
    return v1

    .line 96
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;

    if-eqz v3, :cond_a

    move-object v0, p1

    .line 97
    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;

    .line 98
    .local v0, "that":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->segmentId:Ljava/lang/Long;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->segmentId()Ljava/lang/Long;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creationType:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 99
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->creationType()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creatorXuid:Ljava/lang/Long;

    if-nez v3, :cond_5

    .line 100
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->creatorXuid()Ljava/lang/Long;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->recordDate:Ljava/util/Date;

    if-nez v3, :cond_6

    .line 101
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->recordDate()Ljava/util/Date;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->durationInSeconds:Ljava/lang/Integer;

    if-nez v3, :cond_7

    .line 102
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->durationInSeconds()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->offset:Ljava/lang/Integer;

    if-nez v3, :cond_8

    .line 103
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->offset()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->titleId:Ljava/lang/Long;

    if-nez v3, :cond_9

    .line 104
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->titleId()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 98
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->segmentId:Ljava/lang/Long;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->segmentId()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 99
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creationType:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->creationType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 100
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creatorXuid:Ljava/lang/Long;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->creatorXuid()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 101
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->recordDate:Ljava/util/Date;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->recordDate()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 102
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->durationInSeconds:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->durationInSeconds()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 103
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->offset:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->offset()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_6

    .line 104
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->titleId:Ljava/lang/Long;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->titleId()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;
    :cond_a
    move v1, v2

    .line 106
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 111
    const/4 v0, 0x1

    .line 112
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->segmentId:Ljava/lang/Long;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 114
    mul-int/2addr v0, v3

    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creationType:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 116
    mul-int/2addr v0, v3

    .line 117
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creatorXuid:Ljava/lang/Long;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 118
    mul-int/2addr v0, v3

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->recordDate:Ljava/util/Date;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 120
    mul-int/2addr v0, v3

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->durationInSeconds:Ljava/lang/Integer;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 122
    mul-int/2addr v0, v3

    .line 123
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->offset:Ljava/lang/Integer;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    xor-int/2addr v0, v1

    .line 124
    mul-int/2addr v0, v3

    .line 125
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->titleId:Ljava/lang/Long;

    if-nez v1, :cond_6

    :goto_6
    xor-int/2addr v0, v2

    .line 126
    return v0

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->segmentId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creationType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 117
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creatorXuid:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    .line 119
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->recordDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    goto :goto_3

    .line 121
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->durationInSeconds:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    .line 123
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->offset:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5

    .line 125
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->titleId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    goto :goto_6
.end method

.method public offset()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->offset:Ljava/lang/Integer;

    return-object v0
.end method

.method public recordDate()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->recordDate:Ljava/util/Date;

    return-object v0
.end method

.method public segmentId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->segmentId:Ljava/lang/Long;

    return-object v0
.end method

.method public titleId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->titleId:Ljava/lang/Long;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ContentSegment{segmentId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->segmentId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", creationType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creationType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", creatorXuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->creatorXuid:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", recordDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->recordDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", durationInSeconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->durationInSeconds:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", offset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->offset:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_ContentSegment;->titleId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
