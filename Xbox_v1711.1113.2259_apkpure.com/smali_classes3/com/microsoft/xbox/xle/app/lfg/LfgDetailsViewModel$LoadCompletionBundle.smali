.class Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;
.super Ljava/lang/Object;
.source "LfgDetailsViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadCompletionBundle"
.end annotation


# instance fields
.field achievements:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;"
        }
    .end annotation
.end field

.field club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field multiplayerHandle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field multiplayerSession:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

.field titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;Lcom/google/common/base/Optional;Lcom/google/common/base/Optional;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "titleData"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "multiplayerHandle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "multiplayerSession"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lcom/google/common/base/Optional;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;",
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;",
            "Lcom/google/common/base/Optional",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 717
    .local p5, "club":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    .local p6, "achievements":Lcom/google/common/base/Optional;, "Lcom/google/common/base/Optional<Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 718
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 719
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 720
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 721
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 722
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 724
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;->multiplayerHandle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 725
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;->multiplayerSession:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    .line 726
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 727
    invoke-virtual {p5}, Lcom/google/common/base/Optional;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 728
    invoke-virtual {p6}, Lcom/google/common/base/Optional;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;->achievements:Ljava/util/List;

    .line 729
    return-void
.end method
