.class Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "TwoWayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AdapterDataSetObserver"
.end annotation


# instance fields
.field private mInstanceState:Landroid/os/Parcelable;

.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 1

    .prologue
    .line 5862
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 5863
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p2, "x1"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;

    .prologue
    .line 5862
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 5867
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$502(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Z)Z

    .line 5868
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1800(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1702(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    .line 5869
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1802(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    .line 5873
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1900(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1700(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1800(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v0

    if-lez v0, :cond_0

    .line 5874
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 5875
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    .line 5880
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2100(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    .line 5881
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->requestLayout()V

    .line 5882
    return-void

    .line 5877
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2000(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    goto :goto_0
.end method

.method public onInvalidated()V
    .locals 6

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 5886
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$502(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Z)Z

    .line 5888
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1900(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5891
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    .line 5895
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1800(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1702(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    .line 5896
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1802(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    .line 5898
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2202(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    .line 5899
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2302(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;J)J

    .line 5901
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2402(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    .line 5902
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2502(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;J)J

    .line 5904
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2602(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Z)Z

    .line 5906
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2100(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    .line 5907
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->requestLayout()V

    .line 5908
    return-void
.end method
