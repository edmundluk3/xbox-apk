.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "PeopleHubInfoScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/IAddFollowingUserManager;
.implements Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

.field private addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;

.field private addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

.field private basicData:Lcom/microsoft/xbox/service/model/FollowersData;

.field private broadcastCount:Ljava/lang/String;

.field private broadcastId:Ljava/lang/String;

.field private broadcastProvider:Ljava/lang/String;

.field private broadcastTitleId:J

.field private broadcastTitleString:Ljava/lang/String;

.field changeFriendshipDialogViewModel:Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;

.field private changeFriendshipForm:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;",
            ">;"
        }
    .end annotation
.end field

.field private forceInitialReload:Z

.field private isAddingUserToBlockList:Z

.field private isAddingUserToFollowingList:Z

.field private isAddingUserToShareIdentityList:Z

.field private isBlocked:Z

.field private isBroadcasting:Z

.field private isFavorite:Z

.field private isFollowing:Z

.field private isLoadingUserProfile:Z

.field private isQuarantined:Z

.field private isRemovingUserFromBlockList:Z

.field private loadUserProfileTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;

.field protected model:Lcom/microsoft/xbox/service/model/ProfileModel;

.field partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private partyCount:Ljava/lang/String;

.field private partyDescription:Ljava/lang/String;

.field private partyJoinable:Z

.field private partySessionId:Ljava/lang/String;

.field private removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;

.field private reputationLevel:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

.field systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 5
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 138
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 96
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->changeFriendshipForm:Ljava/util/HashSet;

    .line 98
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isFollowing:Z

    .line 99
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isFavorite:Z

    .line 100
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBlocked:Z

    .line 101
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isQuarantined:Z

    .line 102
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->GoodPlayer:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->reputationLevel:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    .line 132
    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partySessionId:Ljava/lang/String;

    .line 133
    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyCount:Ljava/lang/String;

    .line 134
    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyDescription:Ljava/lang/String;

    .line 135
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyJoinable:Z

    .line 140
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v2}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v2

    invoke-interface {v2, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V

    .line 142
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 143
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedProfile()Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "profileXuid":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 145
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPeopleHubInfoScreenAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 146
    return-void

    .line 144
    :cond_0
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isLoadingUserProfile:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->onAddUserToBlockListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isAddingUserToBlockList:Z

    return p1
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->onRemoveUserFromBlockListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$902(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isRemovingUserFromBlockList:Z

    return p1
.end method

.method private banFromClub(J)V
    .locals 5
    .param p1, "clubId"    # J

    .prologue
    .line 379
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v0

    .line 380
    .local v0, "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 381
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v2

    new-instance v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$2;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V

    .line 380
    invoke-virtual {v0, v2, v3, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->ban(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 403
    return-void
.end method

.method static synthetic lambda$inviteToClub$0(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z
    .locals 2
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 301
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$inviteToClub$1(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;J)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "clubId"    # J

    .prologue
    .line 307
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v0

    .line 308
    .local v0, "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$1;

    invoke-direct {v2, p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$1;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Lcom/microsoft/xbox/service/model/clubs/ClubModel;)V

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->sendInvites(Ljava/util/List;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 339
    return-void
.end method

.method static synthetic lambda$load$5(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;)Z
    .locals 1
    .param p0, "response"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 460
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;->results()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$load$6(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "response"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 462
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;->results()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;->sessionRef()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partySessionId:Ljava/lang/String;

    .line 463
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;->results()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;->joinRestriction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;->getMultiplayerSessionRestriction(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;->Followed:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyJoinable:Z

    .line 464
    return-void

    :cond_0
    move v0, v1

    .line 463
    goto :goto_0
.end method

.method static synthetic lambda$load$9(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;)Lio/reactivex/MaybeSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "ignored"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 466
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyJoinable:Z

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partySessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getPartyDetails(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 468
    invoke-virtual {v0}, Lio/reactivex/Single;->toMaybe()Lio/reactivex/Maybe;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 469
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 470
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 480
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lio/reactivex/Maybe;->empty()Lio/reactivex/Maybe;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;J)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "clubId"    # J

    .prologue
    .line 369
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->banFromClub(J)V

    return-void
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Z
    .locals 1
    .param p0, "response"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$null$8(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "response"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 471
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 472
    .local v0, "gamertags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    .line 473
    .local v1, "member":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->gamertag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 476
    .end local v1    # "member":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    :cond_0
    const-string v2, ", "

    invoke-static {v2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyDescription:Ljava/lang/String;

    .line 477
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyCount:Ljava/lang/String;

    .line 478
    return-void
.end method

.method static synthetic lambda$showBanFromClubPicker$2(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z
    .locals 2
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 356
    invoke-static {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$showBanFromClubPicker$4(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;J)V
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p1, "clubId"    # J

    .prologue
    .line 364
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissClubPickerDialog()V

    .line 365
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070225

    .line 366
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070224

    .line 367
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070614

    .line 368
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;J)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07060d

    .line 370
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    .line 365
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 372
    return-void
.end method

.method private notifyDialogAsyncTaskCompleted()V
    .locals 1

    .prologue
    .line 542
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->load(Z)V

    .line 543
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyChangeFriendshipDialogAsyncTaskCompleted()V

    .line 544
    return-void
.end method

.method private notifyDialogAsyncTaskFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 547
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyChangeFriendshipDialogAsyncTaskFailed(Ljava/lang/String;)V

    .line 548
    return-void
.end method

.method private onAddUserToBlockListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "blockUserXuid"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 778
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "onAddUserToBlockListCompleted"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isAddingUserToBlockList:Z

    .line 781
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$4;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 801
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->updateAdapter()V

    .line 802
    return-void

    .line 785
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "User added to never list"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 787
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 788
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBlocked:Z

    .line 789
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNeverListData()Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    move-result-object v1

    .line 790
    .local v1, "neverList":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    if-eqz v1, :cond_0

    .line 791
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;->contains(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBlocked:Z

    goto :goto_0

    .line 797
    .end local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v1    # "neverList":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Failed to add user to block list"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    const v2, 0x7f070758

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    goto :goto_0

    .line 781
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onAddUserToFollowingListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "isFollowing"    # Z

    .prologue
    .line 748
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "onAddUserToFavoriteListCompleted"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isAddingUserToFollowingList:Z

    .line 751
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$4;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 774
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->updateAdapter()V

    .line 775
    return-void

    .line 755
    :pswitch_0
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isFollowing:Z

    .line 756
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->AddForceRefresh(Ljava/lang/Class;)V

    .line 757
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->notifyDialogAsyncTaskCompleted()V

    goto :goto_0

    .line 761
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Failed to add user to following list"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    const/4 v1, 0x0

    .line 763
    .local v1, "result":Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 764
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 765
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAddUserToFollowingResult()Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;

    move-result-object v1

    .line 768
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->getAddFollowingRequestStatus()Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, v1, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->code:I

    const/16 v3, 0x404

    if-ne v2, v3, :cond_1

    .line 769
    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->description:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->notifyDialogAsyncTaskFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 771
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f070b1c

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->notifyDialogAsyncTaskFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 751
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onRemoveUserFromBlockListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "blockUserXuid"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 805
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "onRemoveUserFromBlockListCompleted"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isRemovingUserFromBlockList:Z

    .line 808
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$4;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 828
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->updateAdapter()V

    .line 829
    return-void

    .line 812
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "User removed from never list"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 814
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 815
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBlocked:Z

    .line 816
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNeverListData()Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    move-result-object v1

    .line 817
    .local v1, "neverList":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    if-eqz v1, :cond_0

    .line 818
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;->contains(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBlocked:Z

    goto :goto_0

    .line 824
    .end local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v1    # "neverList":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Failed to remove user from block list"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    const v2, 0x7f07075a

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    goto :goto_0

    .line 808
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public addFollowingUser()V
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->basicData:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 518
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToAddFriend()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;->cancel()V

    .line 522
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->basicData:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData;->getPersonSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/IAddFollowingUserManager;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    .line 523
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;->load(Z)V

    .line 528
    :goto_0
    return-void

    .line 526
    :cond_1
    const v0, 0x7f07061c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    goto :goto_0
.end method

.method public addUserToShareIdentityList()V
    .locals 3

    .prologue
    .line 531
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    if-eqz v1, :cond_0

    .line 532
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;->cancel()V

    .line 534
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 535
    .local v0, "users":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 536
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;Ljava/util/List;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    .line 537
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;->load(Z)V

    .line 538
    return-void
.end method

.method public blockUser()V
    .locals 7

    .prologue
    .line 561
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 563
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackBlockClicked(Ljava/lang/String;)V

    .line 565
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f07073e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07073d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070738

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$3;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$3;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070736

    .line 570
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    .line 565
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 572
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->updateAdapter()V

    .line 573
    return-void

    .line 561
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public blockUserInternal()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 576
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackBlockUnblockConfirmed(Ljava/lang/String;Z)V

    .line 577
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->cancel()V

    .line 580
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;

    .line 581
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->load(Z)V

    .line 582
    return-void
.end method

.method public getBio()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getBio()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBroadcastCount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->broadcastCount:Ljava/lang/String;

    return-object v0
.end method

.method public getBroadcastId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->broadcastId:Ljava/lang/String;

    return-object v0
.end method

.method public getBroadcastProvider()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->broadcastProvider:Ljava/lang/String;

    return-object v0
.end method

.method public getBroadcastTitleId()J
    .locals 2

    .prologue
    .line 232
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->broadcastTitleId:J

    return-wide v0
.end method

.method public getBroadcastingTitleString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->broadcastTitleString:Ljava/lang/String;

    return-object v0
.end method

.method public getFollowerText()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 197
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isMeProfile()Z

    move-result v0

    if-nez v0, :cond_2

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfFollowers()I

    move-result v0

    if-nez v0, :cond_0

    .line 199
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070a4b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 208
    :goto_0
    return-object v0

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isCallerFollowingTarget()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfFollowers()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 202
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070abc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 204
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070a4c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfFollowers()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 208
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "%d"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfFollowers()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getFollowingText()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 182
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isMeProfile()Z

    move-result v0

    if-nez v0, :cond_2

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfFollowing()I

    move-result v0

    if-nez v0, :cond_0

    .line 184
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070a4b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 193
    :goto_0
    return-object v0

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isTargetFollowingCaller()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfFollowing()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 187
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070abc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 189
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070a4c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfFollowing()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 193
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "%d"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfFollowing()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getGamerPicUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGamerScore()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerScore()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIsAddingUserToBlockList()Z
    .locals 1

    .prologue
    .line 500
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isAddingUserToBlockList:Z

    return v0
.end method

.method public getIsBlocked()Z
    .locals 1

    .prologue
    .line 492
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBlocked:Z

    return v0
.end method

.method public getIsFavorite()Z
    .locals 1

    .prologue
    .line 488
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isFavorite:Z

    return v0
.end method

.method public getIsRemovingUserFromBlockList()Z
    .locals 1

    .prologue
    .line 504
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isRemovingUserFromBlockList:Z

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getLocation()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPartyCount()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyCount:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "0"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyCount:Ljava/lang/String;

    goto :goto_0
.end method

.method public getPartyDescription()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getPersonSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->basicData:Lcom/microsoft/xbox/service/model/FollowersData;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->basicData:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getPersonSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreferredColor()I
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v0

    return v0
.end method

.method public getRealName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRealName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReputation()Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 513
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->reputationLevel:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    return-object v0
.end method

.method public getWatermarks()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getWatermarkUris()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getXuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasLoadedBasicData()Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->basicData:Lcom/microsoft/xbox/service/model/FollowersData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public inviteToClub()V
    .locals 5

    .prologue
    .line 289
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 291
    .local v1, "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v1, :cond_0

    .line 292
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getClubs()Ljava/util/List;

    move-result-object v2

    .line 294
    .local v2, "myClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    if-nez v2, :cond_1

    .line 295
    const v3, 0x7f07060f

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    .line 343
    .end local v2    # "myClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :cond_0
    :goto_0
    return-void

    .line 296
    .restart local v2    # "myClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 297
    const v3, 0x7f070aa7

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    goto :goto_0

    .line 299
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v0

    .line 303
    .local v0, "invitableClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 304
    const v3, 0x7f070aa8

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    goto :goto_0

    .line 306
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showInviteClubPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V

    goto :goto_0
.end method

.method public isBroadcasting()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBroadcasting:Z

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 407
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isLoadingUserProfile:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isAddingUserToFollowingList:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isAddingUserToShareIdentityList:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isRemovingUserFromBlockList:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isAddingUserToBlockList:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCallerFollowingTarget()Z
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isCallerFollowingTarget()Z

    move-result v0

    return v0
.end method

.method public isFacebookFriend()Z
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->basicData:Lcom/microsoft/xbox/service/model/FollowersData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->basicData:Lcom/microsoft/xbox/service/model/FollowersData;

    instance-of v0, v0, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->basicData:Lcom/microsoft/xbox/service/model/FollowersData;

    check-cast v0, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getIsFacebookFriend()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInJoinableParty()Z
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->isPartyChatEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyJoinable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partySessionId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMeProfile()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v0

    return v0
.end method

.method public isSendMessageAllowed()Z
    .locals 1

    .prologue
    .line 496
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v0

    return v0
.end method

.method public joinParty()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 274
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isInJoinableParty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 275
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partySessionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->joinParty(Ljava/lang/String;)V

    .line 276
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partySessionId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->joinParty(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v1

    .line 277
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 278
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 279
    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    .line 281
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 282
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Could not navigate to party details"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public load(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 445
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->forceInitialReload:Z

    if-eqz v0, :cond_0

    .line 446
    const/4 p1, 0x1

    .line 447
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->forceInitialReload:Z

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->loadUserProfileTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;

    if-eqz v0, :cond_1

    .line 450
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->loadUserProfileTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->cancel()V

    .line 453
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->loadUserProfileTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;

    .line 454
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->loadUserProfileTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->load(Z)V

    .line 456
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 458
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getPartiesForUser(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v1

    .line 459
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v2

    .line 460
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 461
    invoke-virtual {v1, v2}, Lio/reactivex/Maybe;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Maybe;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v2

    .line 465
    invoke-virtual {v1, v2}, Lio/reactivex/Maybe;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v1

    .line 483
    invoke-virtual {v1}, Lio/reactivex/Maybe;->onErrorComplete()Lio/reactivex/Maybe;

    move-result-object v1

    .line 484
    invoke-virtual {v1}, Lio/reactivex/Maybe;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 458
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 485
    return-void
.end method

.method public navigateToCustomize()V
    .locals 1

    .prologue
    .line 622
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/CustomizeProfileScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 623
    return-void
.end method

.method public navigateToEnforcement()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 626
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getGamerTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 627
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Cannot navigate to Enforcement, gamertag is empty!"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    const v1, 0x7f070b6d

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    .line 640
    :goto_0
    return-void

    .line 631
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->YouProfile:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 633
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    .line 634
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getXuid()Ljava/lang/String;

    move-result-object v3

    .line 635
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getGamerTag()Ljava/lang/String;

    move-result-object v4

    .line 636
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getRealName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x1

    :goto_1
    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;-><init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 638
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .end local v0    # "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    :cond_1
    move v5, v6

    .line 636
    goto :goto_1
.end method

.method public navigateToSendMessage()V
    .locals 6

    .prologue
    .line 602
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 605
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v2

    if-eqz v0, :cond_1

    .line 606
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanCommunicateWithTextAndVoice()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0704a9

    .line 607
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07061c

    .line 608
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 604
    invoke-static {v2, v1, v3, v4}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFailedPermissionsDialog(ZZLjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 609
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getGamerTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 610
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/MultiSelection;->reset()V

    .line 611
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel;)V

    .line 612
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/MultiSelection;->add(Ljava/lang/Object;)V

    .line 613
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 615
    :cond_0
    return-void

    .line 606
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public navigateToSettings()V
    .locals 1

    .prologue
    .line 618
    const-class v0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 619
    return-void
.end method

.method public onAddFollowingUserNoAction(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 1
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "person"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 693
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 694
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 696
    iget-boolean v0, p2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->isFollowedByCaller:Z

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->onAddUserToFollowingListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V

    .line 697
    return-void
.end method

.method public onLoadUserProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 7
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v6, 0x0

    .line 644
    sget-object v4, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v5, "onLoadUserProfile Completed"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isLoadingUserProfile:Z

    .line 651
    sget-object v4, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$4;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 688
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->updateAdapter()V

    .line 689
    return-void

    .line 656
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    .line 657
    .local v2, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isMeProfile()Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz v2, :cond_2

    .line 658
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBlocked:Z

    .line 659
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNeverListData()Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    move-result-object v1

    .line 660
    .local v1, "meNeverList":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    if-eqz v1, :cond_1

    .line 661
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;->contains(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBlocked:Z

    .line 664
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->isCallerFollowingTarget()Z

    move-result v4

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isFollowing:Z

    .line 667
    .end local v1    # "meNeverList":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPeopleHubPersonSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v3

    .line 669
    .local v3, "personSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v3, :cond_0

    .line 670
    new-instance v4, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-direct {v4, v3}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->basicData:Lcom/microsoft/xbox/service/model/FollowersData;

    .line 671
    iget-boolean v4, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->isBroadcasting:Z

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBroadcasting:Z

    .line 672
    iget-boolean v4, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->isQuarantined:Z

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isQuarantined:Z

    .line 673
    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xboxOneRep:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->reputationLevel:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    .line 675
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBroadcasting:Z

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->broadcast:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 676
    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->broadcast:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    .line 677
    .local v0, "broadcast":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;
    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->presenceText:Ljava/lang/String;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->broadcastTitleString:Ljava/lang/String;

    .line 678
    iget-wide v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->viewers:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->broadcastCount:Ljava/lang/String;

    .line 679
    iget-wide v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->titleId:J

    iput-wide v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->broadcastTitleId:J

    .line 680
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->id:Ljava/lang/String;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->broadcastId:Ljava/lang/String;

    .line 681
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->provider:Ljava/lang/String;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->broadcastProvider:Ljava/lang/String;

    goto :goto_0

    .line 651
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 150
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPeopleHubInfoScreenAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 151
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 415
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    .line 416
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 417
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->isForceReload()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->forceInitialReload:Z

    .line 418
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->loadUserProfileTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->loadUserProfileTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->cancel()V

    .line 426
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    if-eqz v0, :cond_1

    .line 427
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;->cancel()V

    .line 430
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    if-eqz v0, :cond_2

    .line 431
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;->cancel()V

    .line 434
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;

    if-eqz v0, :cond_3

    .line 435
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->cancel()V

    .line 438
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;

    if-eqz v0, :cond_4

    .line 439
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;->cancel()V

    .line 441
    :cond_4
    return-void
.end method

.method public postAddFollowingUser(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 1
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "person"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 707
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 708
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 710
    iget-boolean v0, p2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->isFollowedByCaller:Z

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->onAddUserToFollowingListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V

    .line 711
    return-void
.end method

.method public postAddUsersToShareList(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 731
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAddUseToShareIdentityListCompleted "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isAddingUserToShareIdentityList:Z

    .line 733
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$4;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 744
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->updateAdapter()V

    .line 745
    return-void

    .line 737
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->notifyDialogAsyncTaskCompleted()V

    goto :goto_0

    .line 741
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070b1e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->notifyDialogAsyncTaskFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 733
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public postRemoveUserFromShareList(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 726
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "PeopleHubInfoScreenViewModel.postRemoveUserFromShareList not implemented!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public preAddFollowingUser()V
    .locals 1

    .prologue
    .line 701
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isAddingUserToFollowingList:Z

    .line 702
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->updateAdapter()V

    .line 703
    return-void
.end method

.method public preAddUsersToShareList()V
    .locals 1

    .prologue
    .line 715
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isAddingUserToShareIdentityList:Z

    .line 716
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->updateAdapter()V

    .line 717
    return-void
.end method

.method public preRemoveUserFromShareList()V
    .locals 2

    .prologue
    .line 721
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "PeopleHubInfoScreenViewModel.preRemoveUserFromShareList not implemented!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public shouldShowQuarantinedBanner()Z
    .locals 1

    .prologue
    .line 508
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isMeProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isQuarantined:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showBanFromClubPicker()V
    .locals 5

    .prologue
    .line 346
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 348
    .local v1, "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v1, :cond_0

    .line 349
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getClubs()Ljava/util/List;

    move-result-object v2

    .line 351
    .local v2, "myClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    if-nez v2, :cond_1

    .line 352
    const v3, 0x7f07060f

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    .line 376
    .end local v2    # "myClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :cond_0
    :goto_0
    return-void

    .line 354
    .restart local v2    # "myClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$$Lambda$3;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v0

    .line 358
    .local v0, "bannableClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 359
    const v3, 0x7f070250

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showError(I)V

    goto :goto_0

    .line 361
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showClubPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V

    goto :goto_0
.end method

.method public showChangeFriendshipDialog()V
    .locals 2

    .prologue
    .line 551
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->changeFriendshipDialogViewModel:Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;

    if-nez v0, :cond_0

    .line 552
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->changeFriendshipDialogViewModel:Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->basicData:Lcom/microsoft/xbox/service/model/FollowersData;

    if-eqz v0, :cond_1

    .line 555
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->changeFriendshipDialogViewModel:Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->basicData:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData;->getPersonSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;->setPersonSummary(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 557
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->changeFriendshipDialogViewModel:Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showChangeFriendshipDialog(Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 558
    return-void
.end method

.method public unblockUser()V
    .locals 2

    .prologue
    .line 585
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackBlockUnblockConfirmed(Ljava/lang/String;Z)V

    .line 586
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;->cancel()V

    .line 589
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;

    .line 590
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$RemoveUserToNeverListAsyncTask;->load(Z)V

    .line 591
    return-void
.end method
