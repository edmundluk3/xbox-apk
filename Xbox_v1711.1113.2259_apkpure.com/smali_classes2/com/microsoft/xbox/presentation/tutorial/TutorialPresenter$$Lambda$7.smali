.class final synthetic Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;

.field private final arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final arg$3:Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;

.field private final arg$4:Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

.field private final arg$5:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;->arg$1:Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iput-object p3, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;->arg$3:Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;

    iput-object p4, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;->arg$4:Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    iput-object p5, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;->arg$5:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lio/reactivex/functions/Function;
    .locals 6

    new-instance v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;-><init>(Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;->arg$1:Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;->arg$3:Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;->arg$4:Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;->arg$5:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-object v5, p1

    check-cast v5, Lio/reactivex/Observable;

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->lambda$null$21(Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
