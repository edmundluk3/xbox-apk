.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ListSetting;
.super Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting;
.source "AutoValue_ClubHubDataTypes_ListSetting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ListSetting$GsonTypeAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting",
        "<TT;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;ZZ)V
    .locals 0
    .param p3, "canViewerAct"    # Z
    .param p4, "canViewerChangeSetting"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;",
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;ZZ)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ListSetting;, "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ListSetting<TT;>;"
    .local p1, "value":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<TT;>;"
    .local p2, "allowedValues":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<TT;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting;-><init>(Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;ZZ)V

    .line 21
    return-void
.end method
