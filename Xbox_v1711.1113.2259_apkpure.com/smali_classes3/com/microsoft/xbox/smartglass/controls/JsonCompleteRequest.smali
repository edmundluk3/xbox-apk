.class Lcom/microsoft/xbox/smartglass/controls/JsonCompleteRequest;
.super Lorg/json/JSONObject;
.source "JsonCompleteRequest.java"


# direct methods
.method public constructor <init>(ILjava/lang/String;Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "result"    # Ljava/lang/String;
    .param p3, "response"    # Lorg/json/JSONObject;

    .prologue
    .line 11
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 14
    :try_start_0
    const-string v0, "id"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/JsonCompleteRequest;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 15
    if-eqz p2, :cond_0

    .line 16
    const-string v0, "result"

    invoke-virtual {p0, v0, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonCompleteRequest;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19
    :cond_0
    if-eqz p3, :cond_1

    .line 20
    const-string v0, "response"

    invoke-virtual {p0, v0, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonCompleteRequest;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    :cond_1
    :goto_0
    return-void

    .line 22
    :catch_0
    move-exception v0

    goto :goto_0
.end method
