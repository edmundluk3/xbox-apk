.class public Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;
.super Landroid/view/ViewGroup;
.source "URCPercentView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;
    }
.end annotation


# instance fields
.field private command:Ljava/lang/String;

.field private contentEnabled:Z

.field private showContent:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 18
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 14
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->showContent:Z

    .line 15
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->contentEnabled:Z

    .line 19
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x1

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->showContent:Z

    .line 15
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->contentEnabled:Z

    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x1

    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->showContent:Z

    .line 15
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->contentEnabled:Z

    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 33
    sget-object v1, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPercentView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 34
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPercentView_command:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->command:Ljava/lang/String;

    .line 35
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 36
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 124
    instance-of v0, p1, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;

    return v0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 7

    .prologue
    const/4 v1, -0x2

    const/4 v3, 0x0

    .line 109
    new-instance v0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;

    move v2, v1

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;-><init>(IIFFFF)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 119
    new-instance v0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 114
    new-instance v0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getCommand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->command:Ljava/lang/String;

    return-object v0
.end method

.method public isContentEnabled()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->contentEnabled:Z

    return v0
.end method

.method public isShowContent()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->showContent:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 22
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getPaddingLeft()I

    move-result v13

    .line 83
    .local v13, "pl":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getPaddingTop()I

    move-result v15

    .line 84
    .local v15, "pt":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getPaddingRight()I

    move-result v14

    .line 85
    .local v14, "pr":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getPaddingBottom()I

    move-result v12

    .line 87
    .local v12, "pb":I
    sub-int v2, p4, p2

    sub-int/2addr v2, v13

    sub-int v16, v2, v14

    .line 88
    .local v16, "w":I
    sub-int v2, p5, p3

    sub-int/2addr v2, v15

    sub-int v9, v2, v12

    .line 90
    .local v9, "h":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildCount()I

    move-result v8

    .line 92
    .local v8, "count":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v8, :cond_1

    .line 93
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 94
    .local v3, "child":Landroid/view/View;
    if-nez v3, :cond_0

    .line 92
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 98
    :cond_0
    const/high16 v2, 0x40000000    # 2.0f

    move/from16 v0, v16

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/4 v5, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v9, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 100
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;

    .line 101
    .local v11, "lps":Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;
    iget v2, v11, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->leftMargin:I

    add-int/2addr v2, v13

    iget v4, v11, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->left:F

    move/from16 v0, v16

    int-to-float v5, v0

    mul-float/2addr v4, v5

    float-to-double v4, v4

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v6

    double-to-int v4, v4

    add-int/2addr v2, v4

    iget v4, v11, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->topMargin:I

    add-int/2addr v4, v15

    iget v5, v11, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->top:F

    int-to-float v6, v9

    mul-float/2addr v5, v6

    float-to-double v6, v5

    const-wide/high16 v18, 0x3fe0000000000000L    # 0.5

    add-double v6, v6, v18

    double-to-int v5, v6

    add-int/2addr v4, v5

    iget v5, v11, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->rightMargin:I

    sub-int v5, v13, v5

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    iget v0, v11, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->right:F

    move/from16 v17, v0

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    sub-double v6, v6, v18

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v18, v0

    mul-double v6, v6, v18

    const-wide/high16 v18, 0x3fe0000000000000L    # 0.5

    add-double v6, v6, v18

    double-to-int v6, v6

    add-int/2addr v5, v6

    iget v6, v11, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->bottomMargin:I

    sub-int v6, v15, v6

    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    iget v7, v11, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->bottom:F

    float-to-double v0, v7

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    int-to-double v0, v9

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    add-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v7, v0

    add-int/2addr v6, v7

    invoke-virtual {v3, v2, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    goto :goto_1

    .line 105
    .end local v3    # "child":Landroid/view/View;
    .end local v11    # "lps":Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;
    :cond_1
    return-void
.end method

.method public setCommand(Ljava/lang/String;)V
    .locals 0
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->command:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setContentIsEnabled(Z)V
    .locals 3
    .param p1, "isContentEnabled"    # Z

    .prologue
    .line 67
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->contentEnabled:Z

    if-eq v2, p1, :cond_2

    .line 68
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 69
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 70
    .local v1, "v":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 71
    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 72
    if-eqz p1, :cond_1

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 68
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_1
    const/high16 v2, 0x3f000000    # 0.5f

    goto :goto_1

    .line 76
    .end local v0    # "i":I
    .end local v1    # "v":Landroid/view/View;
    :cond_2
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->contentEnabled:Z

    .line 77
    return-void
.end method

.method public setShowContent(Z)V
    .locals 3
    .param p1, "showContent"    # Z

    .prologue
    .line 55
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->showContent:Z

    if-eq v2, p1, :cond_2

    .line 56
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 57
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 58
    .local v1, "v":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 59
    if-eqz p1, :cond_1

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 56
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_1
    const/4 v2, 0x4

    goto :goto_1

    .line 63
    .end local v0    # "i":I
    .end local v1    # "v":Landroid/view/View;
    :cond_2
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->showContent:Z

    .line 64
    return-void
.end method
