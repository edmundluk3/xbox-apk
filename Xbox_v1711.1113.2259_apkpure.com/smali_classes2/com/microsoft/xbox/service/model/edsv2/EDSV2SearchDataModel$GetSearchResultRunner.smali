.class Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "EDSV2SearchDataModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetSearchResultRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

.field private isCanceled:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;Ljava/lang/String;Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)V
    .locals 1
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;
    .param p3, "searchTag"    # Ljava/lang/String;
    .param p4, "selectedFilterType"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 188
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;->caller:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    .line 189
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;->isCanceled:Z

    .line 190
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 200
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->access$300(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    array-length v2, v2

    const/16 v3, 0xfa

    if-ge v2, v3, :cond_0

    .line 201
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getEDSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->access$300(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->access$400(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->getValue()I

    move-result v4

    const-string v5, ""

    const/16 v6, 0xa

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;->searchMediaItems(Ljava/lang/String;ILjava/lang/String;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 206
    :cond_0
    :goto_0
    return-object v1

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;->buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    return-object v0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;->isCanceled:Z

    .line 194
    return-void
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 223
    const-wide/16 v0, 0xfb3

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;->isCanceled:Z

    if-nez v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;->caller:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->access$500(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 219
    :cond_0
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 212
    return-void
.end method
