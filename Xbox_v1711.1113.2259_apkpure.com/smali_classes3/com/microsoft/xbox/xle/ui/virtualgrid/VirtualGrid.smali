.class public Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;
.super Landroid/widget/RelativeLayout;
.source "VirtualGrid.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;,
        Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$VerticalListAdapter;,
        Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;,
        Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IRowProcessor;,
        Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;,
        Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;
    }
.end annotation


# static fields
.field static HEADER_VIEW_ID:I = 0x0
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "VirtualGrid"


# instance fields
.field mActiveScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

.field mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

.field private mAlignment:I

.field mExtraRows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;",
            ">;"
        }
    .end annotation
.end field

.field mHasScrollableHeader:Z

.field mHeaderView:Landroid/view/View;

.field mHorizontalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

.field private mLastScrollTo:I

.field protected mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

.field mListener:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;

.field private final mScrollToEndedCallback:Ljava/lang/Runnable;

.field mScrollingHorizontally:Z

.field mUpdatingOffset:Z

.field mVerticalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

.field mVirtualOffset:I

.field mVirtualWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x1

    sput v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->HEADER_VIEW_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 115
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 97
    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVerticalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .line 98
    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHorizontalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .line 104
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mExtraRows:Ljava/util/ArrayList;

    .line 694
    iput v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mLastScrollTo:I

    .line 711
    new-instance v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$5;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$5;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mScrollToEndedCallback:Ljava/lang/Runnable;

    .line 117
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->VirtualGrid:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 118
    .local v0, "array":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 120
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHasScrollableHeader:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 126
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 128
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->setDividerHeight(I)V

    .line 129
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 130
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->setCacheColorHint(I)V

    .line 131
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->addView(Landroid/view/View;)V

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->setClickable(Z)V

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    const/high16 v2, 0x60000

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->setDescendantFocusability(I)V

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    new-instance v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$1;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 142
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    new-instance v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$2;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 169
    return-void

    .line 122
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mLastScrollTo:I

    return p1
.end method


# virtual methods
.method public OffsetVirtualSpace(I)V
    .locals 8
    .param p1, "offset"    # I

    .prologue
    const/4 v7, 0x0

    .line 278
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mUpdatingOffset:Z

    .line 280
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualOffset:I

    add-int/2addr v5, p1

    invoke-static {v7, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualOffset:I

    .line 282
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildCount()I

    move-result v0

    .line 283
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 284
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 285
    .local v4, "v":Landroid/view/View;
    instance-of v5, v4, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    if-eqz v5, :cond_0

    move-object v3, v4

    .line 286
    check-cast v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    .line 287
    .local v3, "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->shiftContent(I)V

    .line 283
    .end local v3    # "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 291
    .end local v4    # "v":Landroid/view/View;
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mExtraRows:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    .line 292
    .local v2, "row":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->shiftContent(I)V

    goto :goto_1

    .line 294
    .end local v2    # "row":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    :cond_2
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualOffset:I

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->onVirtualOffsetUpdated(I)V

    .line 296
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mUpdatingOffset:Z

    .line 297
    return-void
.end method

.method public addExtraRow(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;)V
    .locals 1
    .param p1, "row"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mExtraRows:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mExtraRows:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    :cond_0
    return-void
.end method

.method protected alignHorizontalOffset(I)I
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 606
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAlignment:I

    if-nez v0, :cond_0

    .end local p1    # "offset":I
    :goto_0
    return p1

    .restart local p1    # "offset":I
    :cond_0
    int-to-float v0, p1

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAlignment:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAlignment:I

    mul-int p1, v0, v1

    goto :goto_0
.end method

.method public contentOffsetChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;I)V
    .locals 0
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
    .param p2, "offset"    # I

    .prologue
    .line 753
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setVirtualOffset(I)V

    .line 754
    return-void
.end method

.method public createExtraRow()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    .locals 4

    .prologue
    .line 384
    new-instance v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V

    .line 385
    .local v0, "row":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 386
    return-object v0
.end method

.method public enumerateVisibleRows(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IRowProcessor;)Ljava/lang/Object;
    .locals 4
    .param p1, "processor"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IRowProcessor;

    .prologue
    .line 356
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildCount()I

    move-result v0

    .line 357
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 358
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IRowProcessor;->onRow(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v2

    .line 359
    .local v2, "obj":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 363
    .end local v2    # "obj":Ljava/lang/Object;
    :goto_1
    return-object v2

    .line 357
    .restart local v2    # "obj":Ljava/lang/Object;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 363
    .end local v2    # "obj":Ljava/lang/Object;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getAdapter()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    return-object v0
.end method

.method public getExtraRow(I)Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 380
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mExtraRows:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mExtraRows:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getHeaderRowCount()I
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;->getHeaderRowCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    return-object v0
.end method

.method protected getHeaderViewHeight()I
    .locals 1

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getHeaderView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getHeaderRowCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 426
    :cond_0
    const/4 v0, 0x0

    .line 428
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getHorizontalScrollState()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHorizontalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    return-object v0
.end method

.method public getListFrame()Landroid/widget/RelativeLayout;
    .locals 0

    .prologue
    .line 176
    return-object p0
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    return-object v0
.end method

.method protected getRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 797
    if-eqz p2, :cond_0

    instance-of v1, p2, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    if-nez v1, :cond_2

    .line 798
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V

    .line 803
    .local v0, "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    :goto_0
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->bind(I)V

    .line 807
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getHeaderView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 808
    if-nez p1, :cond_3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getHeaderViewHeight()I

    move-result v1

    :goto_1
    invoke-virtual {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setRowPadding(Landroid/view/View;IZ)V

    .line 811
    :cond_1
    return-object v0

    .end local v0    # "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    :cond_2
    move-object v0, p2

    .line 800
    check-cast v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    .restart local v0    # "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    goto :goto_0

    :cond_3
    move v1, v2

    .line 808
    goto :goto_1
.end method

.method protected getRowType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 819
    const/4 v0, 0x0

    return v0
.end method

.method protected getRowTypeCount()I
    .locals 1

    .prologue
    .line 815
    const/4 v0, 0x1

    return v0
.end method

.method public getRowView(I)Landroid/view/View;
    .locals 3
    .param p1, "row"    # I

    .prologue
    const/4 v1, 0x0

    .line 519
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getFirstVisiblePosition()I

    move-result v2

    if-ge p1, v2, :cond_1

    .line 529
    :cond_0
    :goto_0
    return-object v1

    .line 522
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getLastVisiblePosition()I

    move-result v2

    if-gt p1, v2, :cond_0

    .line 525
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int v0, p1, v2

    .line 526
    .local v0, "index":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 529
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public getVerticalScrollState()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVerticalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    return-object v0
.end method

.method public getVirtualGridRow(I)Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    .locals 2
    .param p1, "row"    # I

    .prologue
    .line 534
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getRowView(I)Landroid/view/View;

    move-result-object v0

    .line 535
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    if-eqz v1, :cond_0

    .line 536
    check-cast v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    .line 539
    .end local v0    # "v":Landroid/view/View;
    :goto_0
    return-object v0

    .restart local v0    # "v":Landroid/view/View;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVirtualGridRowItem(II)Landroid/view/View;
    .locals 2
    .param p1, "row"    # I
    .param p2, "index"    # I

    .prologue
    .line 544
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getVirtualGridRow(I)Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    move-result-object v0

    .line 545
    .local v0, "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public getVirtualOffset()I
    .locals 1

    .prologue
    .line 273
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualOffset:I

    return v0
.end method

.method public getVirtualWidth()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualWidth:I

    return v0
.end method

.method public isRowVisible(II)Z
    .locals 2
    .param p1, "row"    # I
    .param p2, "margin"    # I

    .prologue
    const/4 v0, 0x0

    .line 508
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getFirstVisiblePosition()I

    move-result v1

    add-int/2addr v1, p2

    if-ge p1, v1, :cond_1

    .line 514
    :cond_0
    :goto_0
    return v0

    .line 511
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getLastVisiblePosition()I

    move-result v1

    sub-int/2addr v1, p2

    if-gt p1, v1, :cond_0

    .line 514
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected notifyItemsAboutViewPort()V
    .locals 5

    .prologue
    .line 550
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildCount()I

    move-result v0

    .line 551
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 552
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 553
    .local v3, "v":Landroid/view/View;
    instance-of v4, v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    if-eqz v4, :cond_0

    move-object v2, v3

    .line 554
    check-cast v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    .line 555
    .local v2, "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->notifyChildrenAboutViewPort()V

    .line 551
    .end local v2    # "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 558
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    return-void
.end method

.method protected onHorizontalScrollingStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V
    .locals 2
    .param p1, "oldState"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    .param p2, "newState"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .prologue
    .line 582
    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    if-ne p2, v1, :cond_1

    .line 585
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualOffset:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->alignHorizontalOffset(I)I

    move-result v0

    .line 586
    .local v0, "offset":I
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualOffset:I

    if-eq v1, v0, :cond_0

    .line 587
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setVirtualOffset(I)V

    .line 590
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->notifyItemsAboutViewPort()V

    .line 592
    .end local v0    # "offset":I
    :cond_1
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 469
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 471
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->updateHeaderVisibility()V

    .line 474
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getHeaderViewHeight()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setRowPadding(Landroid/view/View;IZ)V

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHorizontalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    if-ne v0, v1, :cond_1

    .line 479
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->notifyItemsAboutViewPort()V

    .line 481
    :cond_1
    return-void
.end method

.method protected onRowClick(ILandroid/view/View;)V
    .locals 1
    .param p1, "row"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 320
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListener:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListener:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;

    invoke-interface {v0, p1, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;->onItemClick(ILandroid/view/View;)V

    .line 322
    :cond_0
    return-void
.end method

.method protected onVerticalScrollingStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V
    .locals 13
    .param p1, "oldState"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    .param p2, "newState"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .prologue
    const/4 v12, 0x0

    .line 632
    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildCount()I

    move-result v10

    if-nez v10, :cond_2

    .line 633
    :cond_0
    const-string v10, "VirtualGrid"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onVerticalScrollingStateChanged precondition failed, bailing out ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v12}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildCount()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " children)"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    :cond_1
    :goto_0
    return-void

    .line 638
    :cond_2
    sget-object v10, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    if-ne p2, v10, :cond_9

    .line 639
    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v10, v12}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 640
    .local v3, "firstRow":Landroid/view/View;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    iget-object v11, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildCount()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 641
    .local v6, "lastRow":Landroid/view/View;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getFirstVisiblePosition()I

    move-result v9

    .line 645
    .local v9, "topRowPosition":I
    if-eqz v3, :cond_3

    if-nez v6, :cond_4

    .line 646
    :cond_3
    const-string v10, "VirtualGrid"

    const-string v11, "onVerticalScrollingStateChanged precondition failed (null rows), bailing out"

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 650
    :cond_4
    const/4 v7, 0x1

    .line 651
    .local v7, "shouldSnap":Z
    const/4 v5, 0x0

    .line 652
    .local v5, "headerOffset":I
    iget-boolean v10, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHasScrollableHeader:Z

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    if-eqz v10, :cond_5

    .line 653
    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getY()F

    move-result v10

    iget-object v11, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v11

    int-to-float v11, v11

    add-float/2addr v10, v11

    float-to-int v5, v10

    .line 654
    invoke-static {v5, v12}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 657
    :cond_5
    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getLastVisiblePosition()I

    move-result v10

    iget-object v11, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v11

    invoke-interface {v11}, Landroid/widget/ListAdapter;->getCount()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    if-ne v10, v11, :cond_6

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v10

    iget-object v11, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getHeight()I

    move-result v11

    if-gt v10, v11, :cond_6

    .line 665
    const/4 v7, 0x0

    .line 669
    :cond_6
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v8

    .line 670
    .local v8, "top":I
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 671
    .local v2, "bottom":I
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v10

    div-int/lit8 v4, v10, 0x2

    .line 672
    .local v4, "halfHeight":I
    sub-int v10, v8, v5

    add-int v1, v10, v4

    .line 673
    .local v1, "adjustedTop":I
    sub-int v0, v2, v5

    .line 674
    .local v0, "adjustedBottom":I
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v10

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v11

    if-le v10, v11, :cond_7

    .line 675
    add-int/lit8 v9, v9, 0x1

    .line 678
    :cond_7
    if-eqz v7, :cond_8

    .line 679
    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-static {v10, v9, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPositionWithOffset(Landroid/widget/ListView;II)V

    .line 682
    :cond_8
    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    invoke-interface {v10, v9}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;->saveStartingRow(I)V

    .line 684
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->notifyItemsAboutViewPort()V

    .line 689
    .end local v0    # "adjustedBottom":I
    .end local v1    # "adjustedTop":I
    .end local v2    # "bottom":I
    .end local v3    # "firstRow":Landroid/view/View;
    .end local v4    # "halfHeight":I
    .end local v5    # "headerOffset":I
    .end local v6    # "lastRow":Landroid/view/View;
    .end local v7    # "shouldSnap":Z
    .end local v8    # "top":I
    .end local v9    # "topRowPosition":I
    :cond_9
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isFast()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isFast()Z

    move-result v10

    if-nez v10, :cond_1

    .line 690
    invoke-virtual {p0, v12}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->refreshViewPort(Z)V

    goto/16 :goto_0
.end method

.method protected onVirtualOffsetUpdated(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 301
    return-void
.end method

.method public refreshAll()V
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 337
    return-void
.end method

.method public refreshViewPort(Z)V
    .locals 5
    .param p1, "clear"    # Z

    .prologue
    .line 325
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildCount()I

    move-result v0

    .line 326
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 327
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 328
    .local v3, "v":Landroid/view/View;
    instance-of v4, v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    if-eqz v4, :cond_0

    move-object v2, v3

    .line 329
    check-cast v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    .line 330
    .local v2, "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->refresh(Z)V

    .line 326
    .end local v2    # "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 333
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public removeExtraRow(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;)V
    .locals 1
    .param p1, "row"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    .prologue
    .line 376
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mExtraRows:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 377
    return-void
.end method

.method public scrollStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V
    .locals 2
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
    .param p2, "state"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    .prologue
    .line 725
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    if-eq p2, v0, :cond_1

    .line 726
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setActiveScroller(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)V

    .line 731
    :cond_0
    :goto_0
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$6;->$SwitchMap$com$microsoft$xbox$xle$ui$virtualgrid$HorizontalViewScroller$ScrollState:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 749
    :goto_1
    :pswitch_0
    return-void

    .line 727
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mActiveScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    if-ne v0, p1, :cond_0

    .line 728
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setActiveScroller(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)V

    goto :goto_0

    .line 736
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setHorizontalScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V

    goto :goto_1

    .line 741
    :pswitch_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getFlingStop()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->alignHorizontalOffset(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setFlingStop(I)V

    .line 742
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setHorizontalScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V

    goto :goto_1

    .line 746
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setHorizontalScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V

    goto :goto_1

    .line 731
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public scrollToRow(I)V
    .locals 4
    .param p1, "row"    # I

    .prologue
    .line 698
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->setSelection(I)V

    .line 699
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mLastScrollTo:I

    if-eq v0, p1, :cond_0

    if-gez p1, :cond_1

    .line 709
    :cond_0
    :goto_0
    return-void

    .line 702
    :cond_1
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mLastScrollTo:I

    if-ltz v0, :cond_2

    .line 703
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->JUMP:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setVerticalScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V

    .line 704
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mScrollToEndedCallback:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 707
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mScrollToEndedCallback:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1e

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 708
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mLastScrollTo:I

    goto :goto_0
.end method

.method protected setActiveScroller(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    .prologue
    .line 596
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mActiveScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    if-ne v0, p1, :cond_0

    .line 603
    :goto_0
    return-void

    .line 599
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mActiveScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    if-eqz v0, :cond_1

    .line 600
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mActiveScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->stopScrolling()V

    .line 602
    :cond_1
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mActiveScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    goto :goto_0
.end method

.method public setAdapter(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;)V
    .locals 4
    .param p1, "adapter"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    .prologue
    const/4 v2, 0x0

    .line 180
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    .line 182
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$VerticalListAdapter;

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$VerticalListAdapter;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$1;)V

    :goto_0
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 184
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    if-eqz v1, :cond_2

    .line 186
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mExtraRows:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    .line 187
    .local v0, "row":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->bind(I)V

    goto :goto_1

    .end local v0    # "row":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    :cond_0
    move-object v1, v2

    .line 182
    goto :goto_0

    .line 191
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$3;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->post(Ljava/lang/Runnable;)Z

    .line 198
    :cond_2
    return-void
.end method

.method public setAlignment(I)V
    .locals 1
    .param p1, "alignment"    # I

    .prologue
    .line 213
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAlignment:I

    if-ne v0, p1, :cond_0

    .line 220
    :goto_0
    return-void

    .line 216
    :cond_0
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAlignment:I

    .line 219
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualWidth:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setVirtualWidth(I)V

    goto :goto_0
.end method

.method public setHeaderView(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 397
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    if-ne v0, p1, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 400
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 401
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->removeView(Landroid/view/View;)V

    .line 404
    :cond_2
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    .line 406
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 408
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->addView(Landroid/view/View;)V

    .line 410
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 411
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    sget v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->HEADER_VIEW_ID:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    goto :goto_0
.end method

.method protected setHorizontalScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V
    .locals 2
    .param p1, "state"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .prologue
    .line 569
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHorizontalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    if-ne v1, p1, :cond_0

    .line 577
    :goto_0
    return-void

    .line 572
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHorizontalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .line 574
    .local v0, "oldState":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHorizontalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .line 576
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHorizontalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->onHorizontalScrollingStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V

    goto :goto_0
.end method

.method public setListener(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListener:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;

    .line 207
    return-void
.end method

.method protected setRowPadding(Landroid/view/View;IZ)V
    .locals 3
    .param p1, "rv"    # Landroid/view/View;
    .param p2, "padding"    # I
    .param p3, "async"    # Z

    .prologue
    .line 485
    if-nez p1, :cond_1

    .line 501
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 491
    if-eqz p3, :cond_2

    .line 492
    new-instance v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$4;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;Landroid/view/View;I)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 499
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p1, v0, p2, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method protected setVerticalScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V
    .locals 2
    .param p1, "state"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .prologue
    .line 618
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVerticalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    if-ne v1, p1, :cond_0

    .line 626
    :goto_0
    return-void

    .line 621
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVerticalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .line 623
    .local v0, "oldState":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVerticalScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .line 625
    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->onVerticalScrollingStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V

    goto :goto_0
.end method

.method public setVirtualOffset(I)V
    .locals 7
    .param p1, "offset"    # I

    .prologue
    .line 248
    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mUpdatingOffset:Z

    if-eqz v5, :cond_0

    .line 270
    :goto_0
    return-void

    .line 251
    :cond_0
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mUpdatingOffset:Z

    .line 253
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualOffset:I

    .line 255
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildCount()I

    move-result v0

    .line 256
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 257
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 258
    .local v4, "v":Landroid/view/View;
    instance-of v5, v4, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    if-eqz v5, :cond_1

    move-object v3, v4

    .line 259
    check-cast v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    .line 260
    .local v3, "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setContentOffset(I)V

    .line 256
    .end local v3    # "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 264
    .end local v4    # "v":Landroid/view/View;
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mExtraRows:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    .line 265
    .local v2, "row":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setContentOffset(I)V

    goto :goto_2

    .line 267
    .end local v2    # "row":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    :cond_3
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualOffset:I

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->onVirtualOffsetUpdated(I)V

    .line 269
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mUpdatingOffset:Z

    goto :goto_0
.end method

.method public setVirtualWidth(I)V
    .locals 6
    .param p1, "width"    # I

    .prologue
    .line 224
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAlignment:I

    if-eqz v4, :cond_0

    .line 225
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAlignment:I

    div-int v4, p1, v4

    iget v5, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAlignment:I

    mul-int p1, v4, v5

    .line 227
    :cond_0
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualWidth:I

    if-ne v4, p1, :cond_2

    .line 240
    :cond_1
    return-void

    .line 230
    :cond_2
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualWidth:I

    .line 232
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getChildCount()I

    move-result v0

    .line 233
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 234
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 235
    .local v3, "v":Landroid/view/View;
    instance-of v4, v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    if-eqz v4, :cond_3

    move-object v2, v3

    .line 236
    check-cast v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    .line 237
    .local v2, "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setContentWidth(I)V

    .line 233
    .end local v2    # "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public smoothScrollTo(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 341
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setActiveScroller(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)V

    .line 343
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getExtraRow(I)Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    move-result-object v0

    .line 344
    .local v0, "row":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    if-eqz v0, :cond_0

    .line 345
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->smoothScrollTo(I)V

    .line 348
    :goto_0
    return-void

    .line 347
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setVirtualOffset(I)V

    goto :goto_0
.end method

.method public smoothScrollToRow(I)V
    .locals 1
    .param p1, "row"    # I

    .prologue
    .line 351
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->smoothScrollToPosition(I)V

    .line 352
    return-void
.end method

.method public updateBottomView(IZ)Z
    .locals 3
    .param p1, "row"    # I
    .param p2, "animate"    # Z

    .prologue
    .line 306
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getRowView(I)Landroid/view/View;

    move-result-object v1

    .line 307
    .local v1, "v":Landroid/view/View;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 309
    check-cast v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    .line 310
    .local v0, "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->updateBottomView(Z)V

    .line 311
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->requestLayout()V

    .line 312
    const/4 v2, 0x1

    .line 315
    .end local v0    # "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public updateHeaderVisibility()V
    .locals 9

    .prologue
    .line 437
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getHeaderView()Landroid/view/View;

    move-result-object v7

    if-nez v7, :cond_1

    .line 464
    :cond_0
    :goto_0
    return-void

    .line 440
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getHeaderRowCount()I

    move-result v0

    .line 442
    .local v0, "headerRowCount":I
    const/4 v1, 0x0

    .line 443
    .local v1, "height":I
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getFirstVisiblePosition()I

    move-result v7

    if-ge v7, v0, :cond_3

    .line 445
    const/4 v6, 0x0

    .line 446
    .local v6, "rvNoHeader":Landroid/view/View;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getLastVisiblePosition()I

    move-result v7

    if-lt v7, v0, :cond_2

    .line 447
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getFirstVisiblePosition()I

    move-result v8

    sub-int v8, v0, v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 450
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getHeaderViewHeight()I

    move-result v2

    .line 451
    .local v2, "heightMax":I
    if-eqz v6, :cond_4

    .line 452
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 457
    .end local v2    # "heightMax":I
    .end local v6    # "rvNoHeader":Landroid/view/View;
    :cond_3
    :goto_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 458
    .local v3, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v7

    sub-int v4, v1, v7

    .line 459
    .local v4, "margin":I
    iget v7, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int v5, v4, v7

    .line 460
    .local v5, "offset":I
    if-eqz v5, :cond_0

    .line 461
    iget v7, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v7, v5

    iput v7, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 462
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    invoke-virtual {v7, v5}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_0

    .line 454
    .end local v3    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "margin":I
    .end local v5    # "offset":I
    .restart local v2    # "heightMax":I
    .restart local v6    # "rvNoHeader":Landroid/view/View;
    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method protected withinHeader(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getHeaderView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mHeaderView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
