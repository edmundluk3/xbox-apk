.class Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedSocialRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetActivityFeedSocialRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4324
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedSocialRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 4325
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedSocialRunner;->items:Ljava/util/List;

    .line 4326
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4320
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedSocialRunner;->buildData()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4330
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedSocialRunner;->items:Ljava/util/List;

    if-eqz v8, :cond_7

    .line 4331
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 4332
    .local v3, "likeCountUrls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedSocialRunner;->items:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 4333
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    if-eqz v1, :cond_0

    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    if-eqz v9, :cond_0

    .line 4334
    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4338
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_1
    const/4 v6, 0x0

    .line 4339
    .local v6, "socialActionsSummaries":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    const/4 v4, 0x0

    .line 4340
    .local v4, "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_2

    .line 4341
    new-instance v8, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;

    invoke-direct {v8, v3}, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;-><init>(Ljava/util/List;)V

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;->getCommentsServiceBatchRequestBody(Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;)Ljava/lang/String;

    move-result-object v5

    .line 4343
    .local v5, "postBody":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v8

    invoke-interface {v8, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getLikeDataInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;

    move-result-object v6

    .line 4344
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v8

    invoke-interface {v8, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getMeLikeInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 4350
    .end local v5    # "postBody":Ljava/lang/String;
    :cond_2
    :goto_1
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedSocialRunner;->items:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 4352
    .restart local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    if-eqz v1, :cond_3

    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    if-eqz v9, :cond_3

    .line 4353
    if-eqz v6, :cond_5

    iget-object v9, v6, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    if-eqz v9, :cond_5

    .line 4354
    iget-object v9, v6, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 4355
    .local v7, "sum":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
    iget-object v10, v7, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    if-eqz v10, :cond_4

    iget-object v10, v7, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    iget-object v11, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 4356
    iput-object v7, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 4361
    .end local v7    # "sum":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
    :cond_5
    if-eqz v4, :cond_3

    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    if-eqz v9, :cond_3

    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v9, :cond_3

    .line 4362
    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;

    .line 4363
    .local v2, "like":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    if-eqz v2, :cond_6

    iget-object v10, v2, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;->rootPath:Ljava/lang/String;

    if-eqz v10, :cond_6

    iget-object v10, v2, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;->rootPath:Ljava/lang/String;

    iget-object v11, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 4364
    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    goto :goto_2

    .line 4345
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .end local v2    # "like":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    .restart local v5    # "postBody":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 4346
    .local v0, "e":Ljava/lang/Exception;
    const-string v8, "GetSocialActionItemSocialInfoRunner"

    const-string v9, "Social Info Request Failed"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 4373
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "likeCountUrls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .end local v5    # "postBody":Ljava/lang/String;
    .end local v6    # "socialActionsSummaries":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :cond_7
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedSocialRunner;->items:Ljava/util/List;

    return-object v8
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 4389
    const-wide/16 v0, 0xd1c

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 4383
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;>;"
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 4379
    return-void
.end method
