.class public Lcom/microsoft/xbox/toolkit/ui/XLEWebView;
.super Landroid/webkit/WebView;
.source "XLEWebView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 11
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->addViewThatCausesAndroidLeaks(Landroid/view/View;)V

    .line 13
    return-void
.end method
