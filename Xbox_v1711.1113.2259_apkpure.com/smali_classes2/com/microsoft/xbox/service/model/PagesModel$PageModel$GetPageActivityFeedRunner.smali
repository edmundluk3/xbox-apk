.class Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageActivityFeedRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "PagesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetPageActivityFeedRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final continuationToken:Ljava/lang/String;

.field final synthetic this$1:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/PagesModel$PageModel;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "continuationToken"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageActivityFeedRunner;->this$1:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 165
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageActivityFeedRunner;->continuationToken:Ljava/lang/String;

    .line 166
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageActivityFeedRunner;->this$1:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->access$100(Lcom/microsoft/xbox/service/model/PagesModel$PageModel;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageActivityFeedRunner;->continuationToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getPageActivityFeedInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageActivityFeedRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 184
    const-wide/16 v0, 0xc85

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 179
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageActivityFeedRunner;->this$1:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageActivityFeedRunner;->continuationToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, p1, v0}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->access$200(Lcom/microsoft/xbox/service/model/PagesModel$PageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Z)V

    .line 180
    return-void

    .line 179
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 175
    return-void
.end method
