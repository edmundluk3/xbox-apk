.class public final Lcom/microsoft/xbox/presentation/common/CommonViewIntents;
.super Ljava/lang/Object;
.source "CommonViewIntents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;,
        Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;,
        Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ConfirmIntent;,
        Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;,
        Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;,
        Lcom/microsoft/xbox/presentation/common/CommonViewIntents$LoadMoreIntent;,
        Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;,
        Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
