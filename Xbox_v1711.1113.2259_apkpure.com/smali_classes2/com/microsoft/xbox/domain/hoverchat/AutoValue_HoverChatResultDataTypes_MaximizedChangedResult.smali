.class final Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;
.super Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;
.source "AutoValue_HoverChatResultDataTypes_MaximizedChangedResult.java"


# instance fields
.field private final isMaximized:Z

.field private final position:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;


# direct methods
.method constructor <init>(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)V
    .locals 2
    .param p1, "isMaximized"    # Z
    .param p2, "position"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;-><init>()V

    .line 17
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;->isMaximized:Z

    .line 18
    if-nez p2, :cond_0

    .line 19
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null position"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_0
    iput-object p2, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;->position:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    .line 22
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    if-ne p1, p0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    .line 48
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 49
    check-cast v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;

    .line 50
    .local v0, "that":Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;
    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;->isMaximized:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;->isMaximized()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;->position:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    .line 51
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;->position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;
    :cond_3
    move v1, v2

    .line 53
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 58
    const/4 v0, 0x1

    .line 59
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 60
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;->isMaximized:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 61
    mul-int/2addr v0, v2

    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;->position:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    invoke-virtual {v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 63
    return v0

    .line 60
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0
.end method

.method public isMaximized()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;->isMaximized:Z

    return v0
.end method

.method public position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;->position:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MaximizedChangedResult{isMaximized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;->isMaximized:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;->position:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
