.class public Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;
.super Landroid/widget/RelativeLayout;
.source "EPGChannelHeaderView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;


# static fields
.field private static DEFAULT_COLOR:I

.field private static NULL_COLOR:I


# instance fields
.field private mBackgroundColor:I

.field private mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

.field private mFavoriteIndicator:Landroid/widget/TextView;

.field private mForegroundColor:I

.field private mLogoIsSet:Z

.field private mLogoView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

.field private mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

.field private mNumberTextView:Landroid/widget/TextView;

.field private mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput v0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->DEFAULT_COLOR:I

    .line 51
    const v0, 0xffffff

    sput v0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->NULL_COLOR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    sget v0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->NULL_COLOR:I

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mBackgroundColor:I

    .line 60
    sget v0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->NULL_COLOR:I

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mForegroundColor:I

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->updateFavoriteStatus()V

    return-void
.end method

.method public static getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;
    .locals 1
    .param p0, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 69
    const v0, 0x7f0300ed

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->getInstance(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;
    .locals 2
    .param p0, "root"    # Landroid/view/ViewGroup;
    .param p1, "layoutId"    # I

    .prologue
    .line 85
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 86
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    return-object v1
.end method

.method public static getInstance(Landroid/view/ViewGroup;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;)Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;
    .locals 2
    .param p0, "root"    # Landroid/view/ViewGroup;
    .param p1, "type"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    .prologue
    .line 73
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$3;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$EPGViewModel$Channel$Type:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 80
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 76
    :pswitch_0
    const v0, 0x7f0300ed

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->getInstance(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    move-result-object v0

    goto :goto_0

    .line 78
    :pswitch_1
    const v0, 0x7f0300ea

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->getInstance(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    move-result-object v0

    goto :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateFavoriteStatus()V
    .locals 4

    .prologue
    .line 312
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->isFavorite()Z

    move-result v0

    .line 313
    .local v0, "favorite":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteIndicator:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 314
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteIndicator:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 317
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    if-eqz v2, :cond_1

    .line 318
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->setChecked(Z)V

    .line 319
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 320
    .local v1, "r":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 321
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    const v3, 0x7f070f33

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->setText(Ljava/lang/CharSequence;)V

    .line 327
    .end local v1    # "r":Landroid/content/res/Resources;
    :cond_1
    :goto_1
    return-void

    .line 314
    :cond_2
    const/4 v2, 0x4

    goto :goto_0

    .line 323
    .restart local v1    # "r":Landroid/content/res/Resources;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    const v3, 0x7f070f2f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public getModel()Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    return-object v0
.end method

.method public isFavoriteChanged()V
    .locals 1

    .prologue
    .line 349
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$2;-><init>(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->post(Ljava/lang/Runnable;)Z

    .line 355
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V

    .line 333
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->updateFavoriteStatus()V

    .line 335
    :cond_0
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 336
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V

    .line 343
    :cond_0
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 344
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 93
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    const v0, 0x7f0e056a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteIndicator:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e056d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mNumberTextView:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0e056c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    .line 100
    const v0, 0x7f0e056b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    .line 102
    const v0, 0x7f0e056e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    new-instance v1, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;-><init>(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setColors(II)V
    .locals 10
    .param p1, "background"    # I
    .param p2, "foreground"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 245
    sget v5, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->DEFAULT_COLOR:I

    if-ne p2, v5, :cond_5

    .line 246
    sget-object v5, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 247
    sget-object v5, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mNumberTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 248
    sget-object v5, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteIndicator:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 249
    sget-object v5, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 257
    :goto_0
    const/4 v3, 0x0

    .line 260
    .local v3, "colorsChanged":Z
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mBackgroundColor:I

    if-eq p1, v5, :cond_0

    .line 262
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mBackgroundColor:I

    .line 264
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mBackgroundColor:I

    sget v6, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->DEFAULT_COLOR:I

    if-ne v5, v6, :cond_6

    .line 267
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0096

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 268
    .local v4, "d":Landroid/graphics/drawable/Drawable;
    sget-object v5, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedDrawable(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedDrawable(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->defaultPressedSelectedDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 273
    .end local v4    # "d":Landroid/graphics/drawable/Drawable;
    :goto_1
    const/4 v3, 0x1

    .line 277
    :cond_0
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mForegroundColor:I

    if-eq p2, v5, :cond_2

    .line 279
    iput p2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mForegroundColor:I

    .line 281
    sget v5, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->DEFAULT_COLOR:I

    if-eq p2, v5, :cond_1

    .line 282
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v5, p2}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->setTextColor(I)V

    .line 283
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteIndicator:Landroid/widget/TextView;

    invoke-virtual {v5, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 284
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mNumberTextView:Landroid/widget/TextView;

    invoke-virtual {v5, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 287
    :cond_1
    const/4 v3, 0x1

    .line 291
    :cond_2
    if-eqz v3, :cond_4

    .line 293
    sget v5, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->DEFAULT_COLOR:I

    if-ne p1, v5, :cond_3

    .line 294
    sget-object v5, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->getColor()I

    move-result p1

    .line 297
    :cond_3
    sget v5, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->DEFAULT_COLOR:I

    if-eq p2, v5, :cond_4

    .line 298
    new-array v1, v9, [[I

    new-array v5, v8, [I

    const v6, 0x10100a7

    aput v6, v5, v7

    aput-object v5, v1, v7

    new-array v5, v8, [I

    const v6, -0x10100a7

    aput v6, v5, v7

    aput-object v5, v1, v8

    .line 299
    .local v1, "colorStates":[[I
    new-array v2, v9, [I

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0c013a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    aput v5, v2, v7

    aput p2, v2, v8

    .line 303
    .local v2, "colors":[I
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 305
    .local v0, "colorStateList":Landroid/content/res/ColorStateList;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 308
    .end local v0    # "colorStateList":Landroid/content/res/ColorStateList;
    .end local v1    # "colorStates":[[I
    .end local v2    # "colors":[I
    :cond_4
    return-void

    .line 251
    .end local v3    # "colorsChanged":Z
    :cond_5
    sget-object v5, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->removeTextView(Landroid/widget/TextView;)V

    .line 252
    sget-object v5, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mNumberTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->removeTextView(Landroid/widget/TextView;)V

    .line 253
    sget-object v5, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteIndicator:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->removeTextView(Landroid/widget/TextView;)V

    .line 254
    sget-object v5, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mFavoriteButton:Lcom/microsoft/xbox/xle/ui/XLECheckableButton;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->removeTextView(Landroid/widget/TextView;)V

    goto/16 :goto_0

    .line 270
    .restart local v3    # "colorsChanged":Z
    :cond_6
    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    iget v6, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mBackgroundColor:I

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method

.method public setModel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;ZZ)V
    .locals 7
    .param p1, "data"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .param p2, "showLogo"    # Z
    .param p3, "animate"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x4

    const/4 v4, 0x0

    .line 133
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v3, :cond_0

    .line 134
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v3, p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V

    .line 137
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    .line 139
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v3, :cond_1

    .line 140
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v3, p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V

    .line 143
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mNumberTextView:Landroid/widget/TextView;

    if-eqz v3, :cond_2

    .line 145
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getNumber()Ljava/lang/String;

    move-result-object v1

    .line 146
    .local v1, "number":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mNumberTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mNumberTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_8

    move v3, v4

    :goto_1
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 151
    .end local v1    # "number":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v3, :cond_9

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getColors()[I

    move-result-object v0

    .line 152
    .local v0, "colors":[I
    :goto_2
    if-nez v0, :cond_a

    .line 153
    sget v2, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->DEFAULT_COLOR:I

    sget v3, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->DEFAULT_COLOR:I

    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->setColors(II)V

    .line 158
    :goto_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->updateFavoriteStatus()V

    .line 161
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    if-eqz v2, :cond_3

    .line 162
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->clear()V

    .line 163
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setVisibility(I)V

    .line 165
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    if-eqz v2, :cond_4

    .line 166
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 167
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->setVisibility(I)V

    .line 169
    :cond_4
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoIsSet:Z

    .line 171
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v2, :cond_5

    .line 172
    if-eqz p2, :cond_5

    .line 173
    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->updateLogo(Z)V

    .line 177
    :cond_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v2, :cond_6

    .line 178
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->setNarratorContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_6
    return-void

    .end local v0    # "colors":[I
    :cond_7
    move-object v1, v2

    .line 145
    goto :goto_0

    .line 147
    .restart local v1    # "number":Ljava/lang/String;
    :cond_8
    const/16 v3, 0x8

    goto :goto_1

    .end local v1    # "number":Ljava/lang/String;
    :cond_9
    move-object v0, v2

    .line 151
    goto :goto_2

    .line 155
    .restart local v0    # "colors":[I
    :cond_a
    aget v2, v0, v4

    const/4 v3, 0x1

    aget v3, v0, v3

    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->setColors(II)V

    goto :goto_3
.end method

.method public setNarratorContent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tvTitle"    # Ljava/lang/String;
    .param p2, "tvNumber"    # Ljava/lang/String;

    .prologue
    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 359
    return-void
.end method

.method public updateLogo(Z)V
    .locals 11
    .param p1, "animate"    # Z

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 184
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoIsSet:Z

    if-eqz v4, :cond_0

    .line 239
    :goto_0
    return-void

    .line 187
    :cond_0
    const/4 v3, 0x0

    .line 188
    .local v3, "uriValid":Z
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    if-eqz v4, :cond_1

    .line 190
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v4, :cond_1

    .line 192
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v4}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "imageUrl":Ljava/lang/String;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 195
    const/4 v3, 0x1

    .line 196
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setVisibility(I)V

    .line 197
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    if-eqz p1, :cond_3

    sget-wide v4, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->FADE_IN_DURATION:J

    :goto_1
    invoke-virtual {v6, v0, v4, v5}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setImageURI3(Ljava/lang/String;J)V

    .line 199
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v4}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getType()Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->AppChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    if-ne v4, v5, :cond_1

    .line 200
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f09028f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v2, v4

    .line 201
    .local v2, "padding":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    invoke-virtual {v4, v2, v2, v2, v2}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setPadding(IIII)V

    .line 213
    .end local v0    # "imageUrl":Ljava/lang/String;
    .end local v2    # "padding":I
    :cond_1
    :goto_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    if-eqz v4, :cond_2

    .line 214
    if-eqz v3, :cond_5

    .line 215
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v4, v10}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->setVisibility(I)V

    .line 238
    :cond_2
    :goto_3
    iput-boolean v9, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoIsSet:Z

    goto :goto_0

    .line 197
    .restart local v0    # "imageUrl":Ljava/lang/String;
    :cond_3
    const-wide/16 v4, 0x0

    goto :goto_1

    .line 205
    :cond_4
    const/4 v3, 0x0

    .line 206
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mLogoView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    invoke-virtual {v4, v10}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setVisibility(I)V

    goto :goto_2

    .line 217
    .end local v0    # "imageUrl":Ljava/lang/String;
    :cond_5
    const/4 v1, 0x1

    .line 219
    .local v1, "maxLines":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v4, :cond_6

    .line 220
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v4}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getType()Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->AppChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    if-ne v4, v5, :cond_6

    .line 221
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v4, v9}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->setIsAutoSizingEnabled(Z)V

    .line 224
    :cond_6
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->setMaxLines(I)V

    .line 225
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v4}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getName()Ljava/lang/String;

    move-result-object v4

    :goto_4
    invoke-virtual {v5, v4}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->setVisibility(I)V

    .line 229
    if-eqz p1, :cond_8

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 230
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->setAlpha(F)V

    .line 231
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    sget-wide v6, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->FADE_IN_DURATION:J

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_3

    .line 225
    :cond_7
    const/4 v4, 0x0

    goto :goto_4

    .line 233
    :cond_8
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 234
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->mTitleTextView:Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->setAlpha(F)V

    goto :goto_3
.end method
