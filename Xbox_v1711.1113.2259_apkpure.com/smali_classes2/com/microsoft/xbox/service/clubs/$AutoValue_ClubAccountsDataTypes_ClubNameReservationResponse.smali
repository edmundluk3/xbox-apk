.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;
.super Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
.source "$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse.java"


# instance fields
.field private final code:I

.field private final description:Ljava/lang/String;

.field private final expires:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final owner:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "code"    # I
    .param p2, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "owner"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "expires"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;-><init>()V

    .line 22
    iput p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->code:I

    .line 23
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->description:Ljava/lang/String;

    .line 24
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->name:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->owner:Ljava/lang/String;

    .line 26
    iput-object p5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->expires:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public code()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->code:I

    return v0
.end method

.method public description()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->description:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 71
    if-ne p1, p0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v1

    .line 74
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    if-eqz v3, :cond_7

    move-object v0, p1

    .line 75
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    .line 76
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    iget v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->code:I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->code()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->description:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 77
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->description()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->name:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 78
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->name()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->owner:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 79
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->owner()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->expires:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 80
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->expires()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 77
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->description:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->description()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 78
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->name:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 79
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->owner:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->owner()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 80
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->expires:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->expires()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    :cond_7
    move v1, v2

    .line 82
    goto :goto_0
.end method

.method public expires()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->expires:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 87
    const/4 v0, 0x1

    .line 88
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 89
    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->code:I

    xor-int/2addr v0, v1

    .line 90
    mul-int/2addr v0, v3

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->description:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 92
    mul-int/2addr v0, v3

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->name:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 94
    mul-int/2addr v0, v3

    .line 95
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->owner:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 96
    mul-int/2addr v0, v3

    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->expires:Ljava/lang/String;

    if-nez v1, :cond_3

    :goto_3
    xor-int/2addr v0, v2

    .line 98
    return v0

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 93
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 95
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->owner:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 97
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->expires:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public name()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->name:Ljava/lang/String;

    return-object v0
.end method

.method public owner()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->owner:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubNameReservationResponse{code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->code:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", owner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->owner:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", expires="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;->expires:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
