.class public Lcom/microsoft/xbox/service/model/epg/EPGChannel;
.super Ljava/lang/Object;
.source "EPGChannel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;,
        Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;
    }
.end annotation


# static fields
.field public static final TuneFormat:Ljava/lang/String; = "ms-xbl-%s://media-playback?contentType=tvChannel&contentId=%s&lineupInstanceId=%s"


# instance fields
.field private mChannelCallSign:Ljava/lang/String;

.field private mChannelGuid:Ljava/lang/String;

.field private mChannelHDEquivalent:Ljava/lang/String;

.field private mChannelImageUrl:Ljava/lang/String;

.field private mChannelNumber:Ljava/lang/String;

.field private mChannelOrdinal:I

.field private mHeadend:Ljava/lang/String;

.field private mIsAdult:Z

.field private mIsFavorite:Z

.field private mIsFoundChannel:Z

.field private mIsHD:Z

.field public final propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

.field private toggleTask:Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    .line 144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->toggleTask:Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;)V
    .locals 3
    .param p1, "headendId"    # Ljava/lang/String;
    .param p2, "channel"    # Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    .line 144
    iput-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->toggleTask:Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;

    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mHeadend:Ljava/lang/String;

    .line 45
    iget-object v0, p2, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;->channelNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelNumber:Ljava/lang/String;

    .line 46
    iget-object v0, p2, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;->channelName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelCallSign:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelImageUrl:Ljava/lang/String;

    .line 48
    iget-object v0, p2, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;->channelId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelGuid:Ljava/lang/String;

    .line 49
    iput-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelHDEquivalent:Ljava/lang/String;

    .line 50
    iput v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelOrdinal:I

    .line 51
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsFavorite:Z

    .line 52
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsHD:Z

    .line 53
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsAdult:Z

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsFoundChannel:Z

    .line 55
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsFavorite:Z

    return v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/service/model/epg/EPGChannel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsFavorite:Z

    return p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelCallSign:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;)Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->toggleTask:Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mHeadend:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelGuid:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public copyFavorite(Z)V
    .locals 2
    .param p1, "favorite"    # Z

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsFavorite:Z

    if-eq v0, p1, :cond_0

    .line 163
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsFavorite:Z

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    const-string v1, "favorite"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->notify(Ljava/lang/String;)V

    .line 166
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 63
    const/4 v0, 0x0

    .line 64
    .local v0, "result":Z
    instance-of v1, p1, Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    if-eqz v1, :cond_0

    .line 65
    check-cast p1, Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .end local p1    # "other":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->isTheSameChannel(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Z

    move-result v0

    .line 67
    :cond_0
    return v0
.end method

.method public final getChannelCallSign()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelCallSign:Ljava/lang/String;

    return-object v0
.end method

.method public final getChannelGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelGuid:Ljava/lang/String;

    return-object v0
.end method

.method public final getChannelHDEquivalent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelHDEquivalent:Ljava/lang/String;

    return-object v0
.end method

.method public final getChannelImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getChannelNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getChannelOrdinal()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelOrdinal:I

    return v0
.end method

.method public final getCurrentProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 125
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsFoundChannel:Z

    if-eqz v3, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-object v2

    .line 129
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mHeadend:Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    .line 130
    .local v1, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-eqz v1, :cond_0

    .line 134
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v2

    iget v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelOrdinal:I

    const-string v4, "UTC"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v4, v4

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->getEPGIterator(IIZ)Lcom/microsoft/xbox/service/model/epg/EPGIterator;

    move-result-object v0

    .line 135
    .local v0, "iterator":Lcom/microsoft/xbox/service/model/epg/EPGIterator;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->readNext()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v2

    goto :goto_0
.end method

.method public final getHeadendID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mHeadend:Ljava/lang/String;

    return-object v0
.end method

.method public final getIsAdult()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsAdult:Z

    return v0
.end method

.method public final getIsFoundChannel()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsFoundChannel:Z

    return v0
.end method

.method public final getIsHD()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsHD:Z

    return v0
.end method

.method public final getTuneToUrl()Ljava/lang/String;
    .locals 6

    .prologue
    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mHeadend:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    .line 116
    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-nez v0, :cond_0

    .line 117
    const/4 v1, 0x0

    .line 120
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "ms-xbl-%s://media-playback?contentType=tvChannel&contentId=%s&lineupInstanceId=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getTitleId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mHeadend:Ljava/lang/String;

    invoke-static {v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final isFavorite()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsFavorite:Z

    return v0
.end method

.method public final isTheSameChannel(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .prologue
    .line 58
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelGuid:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelGuid:Ljava/lang/String;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mHeadend:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mHeadend:Ljava/lang/String;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggleFavorite()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelNumber:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mChannelNumber:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsFavorite:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->mIsFavorite:Z

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->toggleTask:Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;

    if-nez v0, :cond_2

    .line 154
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGChannel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->toggleTask:Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->toggleTask:Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->load(Z)V

    .line 158
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    const-string v1, "favorite"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->notify(Ljava/lang/String;)V

    goto :goto_0

    .line 151
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
