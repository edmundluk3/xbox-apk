.class public abstract Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LanguageChangedIntent;
.super Ljava/lang/Object;
.source "LanguageSettingsViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LanguageChangedIntent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LanguageChangedIntent;
    .locals 1
    .param p0, "newLanguage"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 19
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 20
    new-instance v0, Lcom/microsoft/xbox/presentation/settings/language/AutoValue_LanguageSettingsViewIntents_LanguageChangedIntent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/settings/language/AutoValue_LanguageSettingsViewIntents_LanguageChangedIntent;-><init>(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)V

    return-object v0
.end method


# virtual methods
.method public abstract newLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
