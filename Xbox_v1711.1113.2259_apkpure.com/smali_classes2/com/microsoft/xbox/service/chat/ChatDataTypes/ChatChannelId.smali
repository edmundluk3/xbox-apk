.class public abstract Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
.super Ljava/lang/Object;
.source "ChatChannelId.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    }
.end annotation


# static fields
.field public static final CHATCHANNELID_SIZE:I = 0x14

.field private static final ID_OFFSET:I = 0x4

.field private static final TYPE_OFFSET:I


# instance fields
.field public final channelType:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "channelType"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->channelType:I

    .line 55
    return-void
.end method

.method public static readObject(Ljava/nio/ByteBuffer;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .locals 1
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->readObject(Ljava/nio/ByteBuffer;I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    move-result-object v0

    return-object v0
.end method

.method public static readObject(Ljava/nio/ByteBuffer;I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .locals 6
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "offset"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 67
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 68
    const-wide/16 v2, 0x0

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 70
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    sub-int/2addr v1, p1

    const/16 v2, 0x14

    if-ge v1, v2, :cond_0

    .line 71
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "not enough buffer space to read ChatChannelId"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 74
    :cond_0
    add-int/lit8 v1, p1, 0x0

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->fromValue(I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    move-result-object v0

    .line 75
    .local v0, "channelType":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$1;->$SwitchMap$com$microsoft$xbox$service$chat$ChatDataTypes$ChatChannelId$ChannelType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not supported channelType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 80
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 77
    :pswitch_0
    new-instance v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    add-int/lit8 v2, p1, 0x4

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;-><init>(J)V

    goto :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 102
    if-ne p1, p0, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v1

    .line 104
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    if-nez v3, :cond_2

    move v1, v2

    .line 105
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 107
    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 108
    .local v0, "other":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    iget v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->channelType:I

    iget v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->channelType:I

    if-ne v3, v4, :cond_3

    .line 109
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public abstract getId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 115
    const/16 v0, 0x11

    .line 116
    .local v0, "result":I
    iget v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->channelType:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 117
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 119
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeObject(Ljava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->writeObject(Ljava/nio/ByteBuffer;I)V

    .line 86
    return-void
.end method

.method public writeObject(Ljava/nio/ByteBuffer;I)V
    .locals 4
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "offset"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 89
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 90
    const-wide/16 v0, 0x0

    int-to-long v2, p2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 92
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    sub-int/2addr v0, p2

    const/16 v1, 0x14

    if-ge v0, v1, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "not enough buffer space to write ChatChannelId"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->channelType:I

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 97
    add-int/lit8 v0, p2, 0x4

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    .line 98
    return-void
.end method
