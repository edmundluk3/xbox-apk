.class Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PhoneContactInviteFriendsDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UploadContactsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$1;

    .prologue
    .line 195
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 7

    .prologue
    .line 213
    const/4 v4, 0x0

    .line 215
    .local v4, "response":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getContacts()Ljava/util/ArrayList;

    move-result-object v1

    .line 217
    .local v1, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 218
    new-instance v3, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;

    .line 219
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getContacts()Ljava/util/ArrayList;

    move-result-object v5

    .line 220
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getFullPhoneNumber()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 221
    .local v3, "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    invoke-interface {v5, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->updatePhoneContacts(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;

    move-result-object v4

    .line 222
    if-eqz v4, :cond_0

    iget-boolean v5, v4, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;->isErrorResponse:Z

    if-nez v5, :cond_0

    .line 223
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;->getXboxPhoneContacts()Ljava/util/Set;

    move-result-object v0

    .line 224
    .local v0, "aliases":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->updateXboxContacts(Ljava/util/Set;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    .end local v0    # "aliases":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v3    # "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;
    :cond_0
    sget-object v5, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .end local v1    # "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;>;"
    :goto_0
    return-object v5

    .line 227
    :catch_0
    move-exception v2

    .line 228
    .local v2, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v5, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 204
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 241
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 195
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 236
    return-void
.end method
