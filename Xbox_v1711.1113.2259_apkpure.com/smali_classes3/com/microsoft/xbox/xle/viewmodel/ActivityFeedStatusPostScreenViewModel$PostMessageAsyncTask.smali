.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedStatusPostScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PostMessageAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private pinAfterPosting:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Z)V
    .locals 0
    .param p2, "pinAfterPosting"    # Z

    .prologue
    .line 292
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 293
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->pinAfterPosting:Z

    .line 294
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 5

    .prologue
    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    .line 319
    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    .line 320
    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    move-result-object v4

    .line 318
    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->postTextToClubFeed(Ljava/lang/String;JLcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    .line 322
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    .line 323
    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    move-result-object v2

    .line 322
    invoke-virtual {v1, v0, v2}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->postTextToUserFeed(Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 323
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->onError()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 303
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;Z)V

    .line 304
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    .prologue
    .line 329
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->pinAfterPosting:Z

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;Z)V

    .line 330
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 289
    check-cast p1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->onPostExecute(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 313
    return-void
.end method
