.class public Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ViewModelWithConversation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "LoadConversationDetailsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final targetXuid:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;
    .param p2, "targetXuid"    # Ljava/lang/String;

    .prologue
    .line 319
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 320
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->targetXuid:Ljava/lang/String;

    .line 321
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 325
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 326
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->targetXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->shouldLoadConversationDetails(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 353
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 354
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->targetXuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-boolean v2, v2, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isGroupConversation:Z

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->loadSkypeConversationMessages(Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 348
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 331
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->targetXuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->onLoadSkypeConversationMessagesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    .line 333
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->targetXuid:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->onLoadSkypeConversationMessagesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    .line 344
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 316
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 337
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 338
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isLoadingConversationDetail:Z

    .line 339
    return-void
.end method
