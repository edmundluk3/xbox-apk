.class public final Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_RtaDataTypes_ShoulderTap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;",
        ">;"
    }
.end annotation


# instance fields
.field private final branchAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final changeNumberAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private defaultBranch:Ljava/lang/String;

.field private defaultChangeNumber:I

.field private defaultResource:Ljava/lang/String;

.field private defaultResourceType:Ljava/lang/String;

.field private defaultSubscription:Ljava/lang/String;

.field private defaultTimestamp:Ljava/util/Date;

.field private final resourceAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final resourceTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final subscriptionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final timestampAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 2
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 27
    iput-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultBranch:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultChangeNumber:I

    .line 29
    iput-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultResource:Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultResourceType:Ljava/lang/String;

    .line 31
    iput-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultSubscription:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultTimestamp:Ljava/util/Date;

    .line 34
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->branchAdapter:Lcom/google/gson/TypeAdapter;

    .line 35
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->changeNumberAdapter:Lcom/google/gson/TypeAdapter;

    .line 36
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->resourceAdapter:Lcom/google/gson/TypeAdapter;

    .line 37
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->resourceTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 38
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->subscriptionAdapter:Lcom/google/gson/TypeAdapter;

    .line 39
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->timestampAdapter:Lcom/google/gson/TypeAdapter;

    .line 40
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;
    .locals 9
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v8, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v8, :cond_0

    .line 89
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 90
    const/4 v0, 0x0

    .line 136
    :goto_0
    return-object v0

    .line 92
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultBranch:Ljava/lang/String;

    .line 94
    .local v1, "branch":Ljava/lang/String;
    iget v2, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultChangeNumber:I

    .line 95
    .local v2, "changeNumber":I
    iget-object v3, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultResource:Ljava/lang/String;

    .line 96
    .local v3, "resource":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultResourceType:Ljava/lang/String;

    .line 97
    .local v4, "resourceType":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultSubscription:Ljava/lang/String;

    .line 98
    .local v5, "subscription":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultTimestamp:Ljava/util/Date;

    .line 99
    .local v6, "timestamp":Ljava/util/Date;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 100
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v7

    .line 101
    .local v7, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v8, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v8, :cond_1

    .line 102
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 105
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 131
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 105
    :sswitch_0
    const-string v8, "branch"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v8, "changeNumber"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v8, "resource"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v8, "resourceType"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v8, "subscription"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v8, "timestamp"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    .line 107
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->branchAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "branch":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 108
    .restart local v1    # "branch":Ljava/lang/String;
    goto :goto_1

    .line 111
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->changeNumberAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 112
    goto :goto_1

    .line 115
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->resourceAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "resource":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 116
    .restart local v3    # "resource":Ljava/lang/String;
    goto :goto_1

    .line 119
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->resourceTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "resourceType":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 120
    .restart local v4    # "resourceType":Ljava/lang/String;
    goto/16 :goto_1

    .line 123
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->subscriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "subscription":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 124
    .restart local v5    # "subscription":Ljava/lang/String;
    goto/16 :goto_1

    .line 127
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->timestampAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "timestamp":Ljava/util/Date;
    check-cast v6, Ljava/util/Date;

    .line 128
    .restart local v6    # "timestamp":Ljava/util/Date;
    goto/16 :goto_1

    .line 135
    .end local v7    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 136
    new-instance v0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap;

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)V

    goto/16 :goto_0

    .line 105
    :sswitch_data_0
    .sparse-switch
        -0x5250da5e -> :sswitch_0
        -0x16e8ef98 -> :sswitch_3
        -0x14543bf2 -> :sswitch_2
        -0xdfca8e7 -> :sswitch_1
        0x3492916 -> :sswitch_5
        0x1456591d -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultBranch(Ljava/lang/String;)Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultBranch"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultBranch:Ljava/lang/String;

    .line 43
    return-object p0
.end method

.method public setDefaultChangeNumber(I)Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultChangeNumber"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultChangeNumber:I

    .line 47
    return-object p0
.end method

.method public setDefaultResource(Ljava/lang/String;)Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultResource"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultResource:Ljava/lang/String;

    .line 51
    return-object p0
.end method

.method public setDefaultResourceType(Ljava/lang/String;)Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultResourceType"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultResourceType:Ljava/lang/String;

    .line 55
    return-object p0
.end method

.method public setDefaultSubscription(Ljava/lang/String;)Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSubscription"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultSubscription:Ljava/lang/String;

    .line 59
    return-object p0
.end method

.method public setDefaultTimestamp(Ljava/util/Date;)Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTimestamp"    # Ljava/util/Date;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->defaultTimestamp:Ljava/util/Date;

    .line 63
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    if-nez p2, :cond_0

    .line 68
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 85
    :goto_0
    return-void

    .line 71
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 72
    const-string v0, "branch"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->branchAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;->branch()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 74
    const-string v0, "changeNumber"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->changeNumberAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;->changeNumber()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 76
    const-string v0, "resource"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->resourceAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;->resource()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 78
    const-string v0, "resourceType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->resourceTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;->resourceType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 80
    const-string v0, "subscription"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->subscriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;->subscription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 82
    const-string v0, "timestamp"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->timestampAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;->timestamp()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 84
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    check-cast p2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_ShoulderTap$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;)V

    return-void
.end method
