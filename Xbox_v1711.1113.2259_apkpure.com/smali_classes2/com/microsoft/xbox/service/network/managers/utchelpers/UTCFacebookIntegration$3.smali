.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$3;
.super Ljava/lang/Object;
.source "UTCFacebookIntegration.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration;->trackLoginStatus(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$actionStatus:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

.field final synthetic val$status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$3;->val$status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$3;->val$actionStatus:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$3;->val$status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$3;->val$actionStatus:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_1

    .line 63
    const-string v0, "Facebook - Login"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$3;->val$status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$3;->val$actionStatus:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 65
    const-string v0, "Facebook - Logout"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    goto :goto_0
.end method
