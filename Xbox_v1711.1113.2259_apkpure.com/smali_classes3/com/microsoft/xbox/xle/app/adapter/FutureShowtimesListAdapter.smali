.class public Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FutureShowtimesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 25
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;->notifyDataSetChanged()V

    .line 26
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 18
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 32
    if-nez p2, :cond_7

    .line 33
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;->getContext()Landroid/content/Context;

    move-result-object v12

    const-string v13, "layout_inflater"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/LayoutInflater;

    .line 34
    .local v11, "vi":Landroid/view/LayoutInflater;
    const v12, 0x7f03016a

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 36
    new-instance v4, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;

    const/4 v12, 0x0

    invoke-direct {v4, v12}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$1;)V

    .line 37
    .local v4, "holder":Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;
    const v12, 0x7f0e0796

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v12}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$102(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 38
    const v12, 0x7f0e0797

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v12}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$202(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 39
    const v12, 0x7f0e0798

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v12}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$302(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 40
    const v12, 0x7f0e079b

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v12}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$402(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 41
    const v12, 0x7f0e0799

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/xle/ui/EPGImageView;

    invoke-static {v4, v12}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$502(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/xle/ui/EPGImageView;)Lcom/microsoft/xbox/xle/ui/EPGImageView;

    .line 42
    const v12, 0x7f0e079a

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v12}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$602(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 44
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 49
    .end local v11    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual/range {p0 .. p1}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    .line 50
    .local v7, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
    if-eqz v7, :cond_6

    .line 52
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 53
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    iget-object v13, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->EpisodeName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    :cond_0
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    if-eqz v12, :cond_1

    .line 58
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v10

    .line 59
    .local v10, "timeFormat":Ljava/text/DateFormat;
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    .line 60
    .local v2, "dateFormat":Ljava/text/DateFormat;
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    sget-object v13, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v14, 0x7f070598

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget-object v0, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->StartTime:Ljava/util/Date;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    iget-object v0, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->EndTime:Ljava/util/Date;

    move-object/from16 v16, v0

    .line 61
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    iget-object v0, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->StartTime:Ljava/util/Date;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    .line 60
    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/HashMap;->size()I

    move-result v12

    const/4 v13, 0x1

    if-le v12, v13, :cond_8

    .line 64
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v12 .. v16}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setPadding(IIII)V

    .line 71
    .end local v2    # "dateFormat":Ljava/text/DateFormat;
    .end local v10    # "timeFormat":Ljava/text/DateFormat;
    :cond_1
    :goto_1
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    if-eqz v12, :cond_9

    iget-object v12, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->HeadendId:Ljava/lang/String;

    if-eqz v12, :cond_9

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/HashMap;->size()I

    move-result v12

    const/4 v13, 0x1

    if-le v12, v13, :cond_9

    .line 72
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 73
    iget-object v12, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->HeadendId:Ljava/lang/String;

    invoke-static {v12}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v9

    .line 74
    .local v9, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-eqz v9, :cond_2

    .line 75
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    iget-object v13, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->HeadendId:Ljava/lang/String;

    invoke-static {v13}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    .end local v9    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_2
    :goto_2
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$400(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    if-eqz v12, :cond_3

    .line 83
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$400(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    iget-object v13, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->ChannelNumber:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    :cond_3
    const/4 v3, 0x0

    .line 88
    .local v3, "didHaveLogo":Z
    const/4 v8, 0x0

    .line 90
    .local v8, "logoURI":Ljava/lang/String;
    iget-object v12, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->Images:Ljava/util/ArrayList;

    if-eqz v12, :cond_4

    .line 91
    iget-object v12, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->Images:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 92
    .local v1, "count":I
    const/4 v6, 0x0

    .line 93
    .local v6, "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    if-ge v5, v1, :cond_4

    .line 94
    iget-object v12, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->Images:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    check-cast v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    .line 95
    .restart local v6    # "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    if-eqz v6, :cond_a

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getPurpose()Ljava/lang/String;

    move-result-object v12

    const-string v13, "Logo"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 96
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getUrl()Ljava/lang/String;

    move-result-object v8

    .line 97
    const/4 v3, 0x1

    .line 103
    .end local v1    # "count":I
    .end local v5    # "i":I
    .end local v6    # "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    :cond_4
    if-eqz v3, :cond_b

    .line 104
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/EPGImageView;

    move-result-object v12

    if-eqz v12, :cond_5

    .line 105
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/EPGImageView;

    move-result-object v12

    invoke-virtual {v12, v8}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setImageURI2(Ljava/lang/String;)V

    .line 106
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/EPGImageView;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setVisibility(I)V

    .line 108
    :cond_5
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    if-eqz v12, :cond_6

    .line 109
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 123
    .end local v3    # "didHaveLogo":Z
    .end local v8    # "logoURI":Ljava/lang/String;
    :cond_6
    :goto_4
    return-object p2

    .line 46
    .end local v4    # "holder":Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;
    .end local v7    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
    :cond_7
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;

    .restart local v4    # "holder":Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;
    goto/16 :goto_0

    .line 66
    .restart local v2    # "dateFormat":Ljava/text/DateFormat;
    .restart local v7    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
    .restart local v10    # "timeFormat":Ljava/text/DateFormat;
    :cond_8
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    sget-object v16, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v17, 0x7f090256

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    invoke-virtual/range {v12 .. v16}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setPadding(IIII)V

    goto/16 :goto_1

    .line 78
    .end local v2    # "dateFormat":Ljava/text/DateFormat;
    .end local v10    # "timeFormat":Ljava/text/DateFormat;
    :cond_9
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 93
    .restart local v1    # "count":I
    .restart local v3    # "didHaveLogo":Z
    .restart local v5    # "i":I
    .restart local v6    # "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    .restart local v8    # "logoURI":Ljava/lang/String;
    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 113
    .end local v1    # "count":I
    .end local v5    # "i":I
    .end local v6    # "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    :cond_b
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/EPGImageView;

    move-result-object v12

    if-eqz v12, :cond_c

    .line 114
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/EPGImageView;

    move-result-object v12

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setVisibility(I)V

    .line 116
    :cond_c
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    if-eqz v12, :cond_6

    .line 117
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    iget-object v13, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->CallSign:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_4
.end method
