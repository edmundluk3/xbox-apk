.class public final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MediaHubDataTypes_ContentSegment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;",
        ">;"
    }
.end annotation


# instance fields
.field private final creationTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final creatorXuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private defaultCreationType:Ljava/lang/String;

.field private defaultCreatorXuid:Ljava/lang/Long;

.field private defaultDurationInSeconds:Ljava/lang/Integer;

.field private defaultOffset:Ljava/lang/Integer;

.field private defaultRecordDate:Ljava/util/Date;

.field private defaultSegmentId:Ljava/lang/Long;

.field private defaultTitleId:Ljava/lang/Long;

.field private final durationInSecondsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final offsetAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final recordDateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final segmentIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final titleIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultSegmentId:Ljava/lang/Long;

    .line 30
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultCreationType:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultCreatorXuid:Ljava/lang/Long;

    .line 32
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultRecordDate:Ljava/util/Date;

    .line 33
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultDurationInSeconds:Ljava/lang/Integer;

    .line 34
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultOffset:Ljava/lang/Integer;

    .line 35
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultTitleId:Ljava/lang/Long;

    .line 37
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->segmentIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 38
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->creationTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 39
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->creatorXuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 40
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->recordDateAdapter:Lcom/google/gson/TypeAdapter;

    .line 41
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->durationInSecondsAdapter:Lcom/google/gson/TypeAdapter;

    .line 42
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->offsetAdapter:Lcom/google/gson/TypeAdapter;

    .line 43
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 44
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;
    .locals 10
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v9, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v9, :cond_0

    .line 100
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 101
    const/4 v0, 0x0

    .line 152
    :goto_0
    return-object v0

    .line 103
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 104
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultSegmentId:Ljava/lang/Long;

    .line 105
    .local v1, "segmentId":Ljava/lang/Long;
    iget-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultCreationType:Ljava/lang/String;

    .line 106
    .local v2, "creationType":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultCreatorXuid:Ljava/lang/Long;

    .line 107
    .local v3, "creatorXuid":Ljava/lang/Long;
    iget-object v4, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultRecordDate:Ljava/util/Date;

    .line 108
    .local v4, "recordDate":Ljava/util/Date;
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultDurationInSeconds:Ljava/lang/Integer;

    .line 109
    .local v5, "durationInSeconds":Ljava/lang/Integer;
    iget-object v6, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultOffset:Ljava/lang/Integer;

    .line 110
    .local v6, "offset":Ljava/lang/Integer;
    iget-object v7, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultTitleId:Ljava/lang/Long;

    .line 111
    .local v7, "titleId":Ljava/lang/Long;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 112
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v8

    .line 113
    .local v8, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v9, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v9, :cond_1

    .line 114
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 117
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 147
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 117
    :sswitch_0
    const-string v9, "segmentId"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v9, "creationType"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v9, "creatorXuid"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v9, "recordDate"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v9, "durationInSeconds"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v9, "offset"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v9, "titleId"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x6

    goto :goto_2

    .line 119
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->segmentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "segmentId":Ljava/lang/Long;
    check-cast v1, Ljava/lang/Long;

    .line 120
    .restart local v1    # "segmentId":Ljava/lang/Long;
    goto :goto_1

    .line 123
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->creationTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "creationType":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 124
    .restart local v2    # "creationType":Ljava/lang/String;
    goto :goto_1

    .line 127
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->creatorXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "creatorXuid":Ljava/lang/Long;
    check-cast v3, Ljava/lang/Long;

    .line 128
    .restart local v3    # "creatorXuid":Ljava/lang/Long;
    goto/16 :goto_1

    .line 131
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->recordDateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "recordDate":Ljava/util/Date;
    check-cast v4, Ljava/util/Date;

    .line 132
    .restart local v4    # "recordDate":Ljava/util/Date;
    goto/16 :goto_1

    .line 135
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->durationInSecondsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "durationInSeconds":Ljava/lang/Integer;
    check-cast v5, Ljava/lang/Integer;

    .line 136
    .restart local v5    # "durationInSeconds":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 139
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->offsetAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "offset":Ljava/lang/Integer;
    check-cast v6, Ljava/lang/Integer;

    .line 140
    .restart local v6    # "offset":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 143
    :pswitch_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "titleId":Ljava/lang/Long;
    check-cast v7, Ljava/lang/Long;

    .line 144
    .restart local v7    # "titleId":Ljava/lang/Long;
    goto/16 :goto_1

    .line 151
    .end local v8    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 152
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment;

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    goto/16 :goto_0

    .line 117
    nop

    :sswitch_data_0
    .sparse-switch
        -0x61065852 -> :sswitch_0
        -0x4deb0a6d -> :sswitch_6
        -0x485d759a -> :sswitch_4
        -0x3cc89b6d -> :sswitch_5
        -0xb5bb0dc -> :sswitch_2
        0x2bc63f9f -> :sswitch_3
        0x5e88eab9 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultCreationType(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCreationType"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultCreationType:Ljava/lang/String;

    .line 51
    return-object p0
.end method

.method public setDefaultCreatorXuid(Ljava/lang/Long;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCreatorXuid"    # Ljava/lang/Long;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultCreatorXuid:Ljava/lang/Long;

    .line 55
    return-object p0
.end method

.method public setDefaultDurationInSeconds(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDurationInSeconds"    # Ljava/lang/Integer;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultDurationInSeconds:Ljava/lang/Integer;

    .line 63
    return-object p0
.end method

.method public setDefaultOffset(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultOffset"    # Ljava/lang/Integer;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultOffset:Ljava/lang/Integer;

    .line 67
    return-object p0
.end method

.method public setDefaultRecordDate(Ljava/util/Date;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRecordDate"    # Ljava/util/Date;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultRecordDate:Ljava/util/Date;

    .line 59
    return-object p0
.end method

.method public setDefaultSegmentId(Ljava/lang/Long;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSegmentId"    # Ljava/lang/Long;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultSegmentId:Ljava/lang/Long;

    .line 47
    return-object p0
.end method

.method public setDefaultTitleId(Ljava/lang/Long;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleId"    # Ljava/lang/Long;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->defaultTitleId:Ljava/lang/Long;

    .line 71
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    if-nez p2, :cond_0

    .line 77
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 96
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 81
    const-string v0, "segmentId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->segmentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->segmentId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 83
    const-string v0, "creationType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->creationTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->creationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 85
    const-string v0, "creatorXuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->creatorXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->creatorXuid()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 87
    const-string v0, "recordDate"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->recordDateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->recordDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 89
    const-string v0, "durationInSeconds"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->durationInSecondsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->durationInSeconds()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 91
    const-string v0, "offset"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->offsetAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->offset()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 93
    const-string v0, "titleId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->titleId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 95
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ContentSegment$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;)V

    return-void
.end method
