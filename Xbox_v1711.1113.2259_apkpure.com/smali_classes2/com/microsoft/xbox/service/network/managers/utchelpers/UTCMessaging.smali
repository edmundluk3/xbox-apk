.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;
.super Ljava/lang/Object;
.source "UTCMessaging.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$trackMarkConversationRead$0(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 3
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 500
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 502
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 503
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ConversationId"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 505
    const-string v1, "Messaging - MarkConversationRead"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 506
    return-void
.end method

.method public static trackAddPeople(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 259
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$11;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$11;-><init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 270
    return-void
.end method

.method public static trackBlockUserState(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Z)V
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "block"    # Z

    .prologue
    .line 93
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$3;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$3;-><init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Z)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 104
    return-void
.end method

.method public static trackConversationView(Ljava/lang/String;)V
    .locals 1
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 464
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$19;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$19;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 472
    return-void
.end method

.method public static trackConversationsView()V
    .locals 1

    .prologue
    .line 450
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$18;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$18;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 456
    return-void
.end method

.method public static trackCopyText(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 1
    .param p0, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 432
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$17;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$17;-><init>(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 444
    return-void
.end method

.method public static trackCreateGroupConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 221
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$9;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$9;-><init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 232
    return-void
.end method

.method public static trackCreateMessage()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$1;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 53
    return-void
.end method

.method public static trackDeleteConversation(Ljava/lang/String;)V
    .locals 1
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 112
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$4;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 123
    return-void
.end method

.method public static trackDeleteMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "messageId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 132
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$5;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$5;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 145
    return-void
.end method

.method public static trackLeaveConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 318
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$14;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$14;-><init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 329
    return-void
.end method

.method public static trackMarkConversationRead(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 499
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 507
    return-void
.end method

.method public static trackMuteState(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 278
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$12;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$12;-><init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 291
    return-void
.end method

.method public static trackReadConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 153
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$6;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$6;-><init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 170
    return-void
.end method

.method public static trackRenameConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 299
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$13;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$13;-><init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 310
    return-void
.end method

.method public static trackReport(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 1
    .param p0, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 412
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$16;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$16;-><init>(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 424
    return-void
.end method

.method public static trackSendMessage(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "members":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$2;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$2;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 83
    return-void
.end method

.method public static trackSendMessageError(J)V
    .locals 2
    .param p0, "errorCode"    # J

    .prologue
    .line 479
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$20;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$20;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 490
    return-void
.end method

.method public static trackShowAttachment(Lcom/microsoft/xbox/service/model/entity/Entity;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 1
    .param p0, "attachment"    # Lcom/microsoft/xbox/service/model/entity/Entity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "skypeMessage"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 339
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;-><init>(Lcom/microsoft/xbox/service/model/entity/Entity;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 403
    return-void
.end method

.method public static trackViewPeople(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 240
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$10;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$10;-><init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 251
    return-void
.end method

.method public static trackViewProfile(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 180
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$7;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$7;-><init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 191
    return-void
.end method

.method public static trackViewSpecificProfile(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/lang/String;)V
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 200
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$8;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$8;-><init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 213
    return-void
.end method
