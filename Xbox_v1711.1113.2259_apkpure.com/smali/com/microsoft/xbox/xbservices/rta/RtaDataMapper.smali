.class public final Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;
.super Ljava/lang/Object;
.source "RtaDataMapper.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/data/repository/DataMapper;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/xbservices/data/repository/DataMapper",
        "<",
        "Ljava/lang/String;",
        "Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private parseEventResponse([Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 4
    .param p1, "rtaResponse"    # [Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x3L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 175
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 176
    array-length v1, p1

    const/4 v3, 0x3

    if-lt v1, v3, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->isTrue(Z)V

    .line 178
    aget-object v1, p1, v2

    instance-of v1, v1, Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 179
    aget-object v1, p1, v2

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->intValue()I

    move-result v0

    .line 181
    .local v0, "subscriptionId":I
    new-instance v1, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;

    .line 182
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v3, p1, v3

    .line 183
    invoke-static {v3}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;->with(Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;-><init>(Ljava/lang/Integer;Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;)V

    .line 181
    invoke-static {v1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    .line 187
    .end local v0    # "subscriptionId":I
    :goto_1
    return-object v1

    .line 176
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 186
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    const-string v2, "Invalid RTA EVENT"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v1

    goto :goto_1
.end method

.method private parseSubscribeResponse([Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 11
    .param p1, "rtaResponse"    # [Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x3L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v8, 0x3

    .line 105
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 106
    array-length v0, p1

    if-lt v0, v8, :cond_0

    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->isTrue(Z)V

    .line 108
    aget-object v0, p1, v2

    instance-of v0, v0, Ljava/lang/Double;

    if-eqz v0, :cond_2

    aget-object v0, p1, v9

    instance-of v0, v0, Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 109
    aget-object v0, p1, v2

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v3

    .line 110
    .local v3, "sequenceNumber":I
    aget-object v0, p1, v9

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v6

    .line 111
    .local v6, "errorCode":I
    sget-object v4, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;->Unknown:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    .line 112
    .local v4, "errorType":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;
    const/4 v5, 0x0

    .line 113
    .local v5, "errorMessage":Ljava/lang/String;
    const/4 v1, 0x0

    .line 114
    .local v1, "subscriptionId":Ljava/lang/Integer;
    const/4 v7, 0x0

    .line 116
    .local v7, "payload":Ljava/lang/Object;
    packed-switch v6, :pswitch_data_0

    .line 138
    .end local v7    # "payload":Ljava/lang/Object;
    :goto_1
    new-instance v0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;

    .line 140
    invoke-static {v7}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;->with(Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;-><init>(Ljava/lang/Integer;Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;ILcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;Ljava/lang/String;)V

    .line 138
    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 147
    .end local v1    # "subscriptionId":Ljava/lang/Integer;
    .end local v3    # "sequenceNumber":I
    .end local v4    # "errorType":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;
    .end local v5    # "errorMessage":Ljava/lang/String;
    .end local v6    # "errorCode":I
    :goto_2
    return-object v0

    .line 106
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 118
    .restart local v1    # "subscriptionId":Ljava/lang/Integer;
    .restart local v3    # "sequenceNumber":I
    .restart local v4    # "errorType":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;
    .restart local v5    # "errorMessage":Ljava/lang/String;
    .restart local v6    # "errorCode":I
    .restart local v7    # "payload":Ljava/lang/Object;
    :pswitch_0
    aget-object v0, p1, v8

    instance-of v0, v0, Ljava/lang/Double;

    if-eqz v0, :cond_1

    array-length v0, p1

    if-le v0, v10, :cond_1

    .line 119
    sget-object v4, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;->Success:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    .line 120
    aget-object v0, p1, v8

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 121
    aget-object v7, p1, v10

    goto :goto_1

    .line 123
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    const-string v2, "Malformed RTA SUBSCRIBE message!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_2

    .line 129
    :pswitch_1
    sget-object v4, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;->MaxSubscriptionLimit:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    .line 130
    aget-object v5, p1, v8

    .end local v5    # "errorMessage":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 131
    .restart local v5    # "errorMessage":Ljava/lang/String;
    goto :goto_1

    .line 133
    :pswitch_2
    sget-object v4, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;->NoResourceData:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    .line 134
    aget-object v5, p1, v8

    .end local v5    # "errorMessage":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .restart local v5    # "errorMessage":Ljava/lang/String;
    goto :goto_1

    .line 147
    .end local v1    # "subscriptionId":Ljava/lang/Integer;
    .end local v3    # "sequenceNumber":I
    .end local v4    # "errorType":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;
    .end local v5    # "errorMessage":Ljava/lang/String;
    .end local v6    # "errorCode":I
    .end local v7    # "payload":Ljava/lang/Object;
    :cond_2
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_2

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private parseUnsubscribeResponse([Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 7
    .param p1, "rtaResponse"    # [Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x3L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v3, 0x1

    .line 155
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 156
    array-length v2, p1

    const/4 v4, 0x3

    if-lt v2, v4, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->isTrue(Z)V

    .line 158
    aget-object v2, p1, v3

    instance-of v2, v2, Ljava/lang/Double;

    if-eqz v2, :cond_1

    aget-object v2, p1, v5

    instance-of v2, v2, Ljava/lang/Double;

    if-eqz v2, :cond_1

    .line 159
    aget-object v2, p1, v3

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->intValue()I

    move-result v1

    .line 160
    .local v1, "sequenceNumber":I
    aget-object v2, p1, v5

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->intValue()I

    move-result v0

    .line 162
    .local v0, "errorCode":I
    new-instance v2, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;

    invoke-direct {v2, v6, v6, v1, v0}, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;-><init>(Ljava/lang/Integer;Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;II)V

    invoke-static {v2}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v2

    .line 169
    .end local v0    # "errorCode":I
    .end local v1    # "sequenceNumber":I
    :goto_1
    return-object v2

    .line 156
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 168
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    const-string v3, "Failed to parse RTA UNSUBSCRIBE"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 29
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->apply(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public apply(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 11
    .param p1, "responseText"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    .line 43
    sget-object v5, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Got RTA message: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    const/4 v3, 0x0

    .line 47
    .local v3, "rtaResponse":[Ljava/lang/Object;
    :try_start_0
    new-instance v5, Lcom/google/gson/GsonBuilder;

    invoke-direct {v5}, Lcom/google/gson/GsonBuilder;-><init>()V

    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/16 v8, 0x80

    aput v8, v6, v7

    .line 48
    invoke-virtual {v5, v6}, Lcom/google/gson/GsonBuilder;->excludeFieldsWithModifiers([I)Lcom/google/gson/GsonBuilder;

    move-result-object v5

    .line 49
    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/gson/AutoValueGsonAdapterFactory;->create()Lcom/google/gson/TypeAdapterFactory;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v5

    new-instance v6, Lcom/microsoft/xbox/xbservices/toolkit/gson/PostProcessingEnablerGson;

    invoke-direct {v6}, Lcom/microsoft/xbox/xbservices/toolkit/gson/PostProcessingEnablerGson;-><init>()V

    .line 50
    invoke-virtual {v5, v6}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v5

    const-class v6, Lcom/google/common/collect/ImmutableList;

    new-instance v7, Lcom/microsoft/xbox/xbservices/toolkit/gson/ImmutableListDeserializer;

    invoke-direct {v7}, Lcom/microsoft/xbox/xbservices/toolkit/gson/ImmutableListDeserializer;-><init>()V

    .line 51
    invoke-virtual {v5, v6, v7}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v5

    const-class v6, Lcom/google/common/collect/ImmutableSet;

    new-instance v7, Lcom/microsoft/xbox/xbservices/toolkit/gson/ImmutableSetDeserializer;

    invoke-direct {v7}, Lcom/microsoft/xbox/xbservices/toolkit/gson/ImmutableSetDeserializer;-><init>()V

    .line 52
    invoke-virtual {v5, v6, v7}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v5

    .line 53
    invoke-virtual {v5}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v2

    .line 55
    .local v2, "gson":Lcom/google/gson/Gson;
    const-class v5, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, [Ljava/lang/Object;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    if-eqz v3, :cond_3

    array-length v5, v3

    if-lez v5, :cond_3

    aget-object v5, v3, v9

    instance-of v5, v5, Ljava/lang/Double;

    if-eqz v5, :cond_3

    .line 62
    aget-object v5, v3, v9

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->intValue()I

    move-result v5

    invoke-static {v5}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponseType;->valueOf(I)Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponseType;

    move-result-object v4

    .line 64
    .local v4, "rtaResposeTypeCode":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponseType;
    sget-object v5, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper$1;->$SwitchMap$com$microsoft$xbox$xbservices$rta$RtaDataTypes$RtaResponseType:[I

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponseType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 90
    sget-object v5, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Received unknown RTA message type. Message: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v5

    .line 97
    .end local v2    # "gson":Lcom/google/gson/Gson;
    .end local v4    # "rtaResposeTypeCode":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponseType;
    :goto_0
    return-object v5

    .line 56
    :catch_0
    move-exception v1

    .line 57
    .local v1, "ex":Ljava/lang/Exception;
    sget-object v5, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error deserializing json: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 58
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v5

    goto :goto_0

    .line 66
    .end local v1    # "ex":Ljava/lang/Exception;
    .restart local v2    # "gson":Lcom/google/gson/Gson;
    .restart local v4    # "rtaResposeTypeCode":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponseType;
    :pswitch_0
    array-length v5, v3

    if-le v5, v10, :cond_0

    .line 67
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->parseSubscribeResponse([Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v5

    goto :goto_0

    .line 69
    :cond_0
    sget-object v5, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RTA SUBSCRIBE payload does not have the required number of fields! Message: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    .end local v4    # "rtaResposeTypeCode":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponseType;
    :goto_1
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v5

    goto :goto_0

    .line 73
    .restart local v4    # "rtaResposeTypeCode":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponseType;
    :pswitch_1
    array-length v5, v3

    if-lt v5, v10, :cond_1

    .line 74
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->parseUnsubscribeResponse([Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v5

    goto :goto_0

    .line 76
    :cond_1
    sget-object v5, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RTA UNSUBSCRIBE payload does not have the required number of fields! Message: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 80
    :pswitch_2
    array-length v5, v3

    if-ne v5, v10, :cond_2

    .line 81
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->parseEventResponse([Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v5

    goto :goto_0

    .line 83
    :cond_2
    sget-object v5, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RTA EVENT payload does not have the required number of fields! Message: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 87
    :pswitch_3
    sget-object v5, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    const-string v6, "RTA UNSUBSCRIBE ALL is not currently supported."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v5

    goto/16 :goto_0

    .line 94
    .end local v4    # "rtaResposeTypeCode":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponseType;
    :cond_3
    sget-object v5, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Received invalid RTA response payload! Message: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 29
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->apply(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
