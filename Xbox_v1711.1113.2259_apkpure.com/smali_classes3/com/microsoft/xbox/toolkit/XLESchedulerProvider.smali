.class public final Lcom/microsoft/xbox/toolkit/XLESchedulerProvider;
.super Ljava/lang/Object;
.source "XLESchedulerProvider.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public computation()Lio/reactivex/Scheduler;
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->computation()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public io()Lio/reactivex/Scheduler;
    .locals 1

    .prologue
    .line 15
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public main()Lio/reactivex/Scheduler;
    .locals 1

    .prologue
    .line 10
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public trampoline()Lio/reactivex/Scheduler;
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->trampoline()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method
