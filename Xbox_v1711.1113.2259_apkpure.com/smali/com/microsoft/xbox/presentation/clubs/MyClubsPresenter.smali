.class public Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;
.super Lcom/microsoft/xbox/presentation/base/MviPresenter;
.source "MyClubsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/presentation/base/MviPresenter",
        "<",
        "Lcom/microsoft/xbox/presentation/clubs/MyClubsView;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        "Lcom/microsoft/xbox/presentation/lce/LceViewState",
        "<",
        "Lcom/google/common/collect/ImmutableList",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;>;>;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final intentToStateTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;)V
    .locals 1
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "interactor"    # Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;
    .param p3, "navigator"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 43
    invoke-static {p0, p2, p3}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 60
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;Lcom/microsoft/xbox/presentation/lce/LceViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->stateReducer(Lcom/microsoft/xbox/presentation/lce/LceViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$initialSetup$7(Lcom/microsoft/xbox/presentation/lce/LceViewState;)V
    .locals 2
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    sget-object v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->TAG:Ljava/lang/String;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/lce/LceViewState;)Lcom/microsoft/xbox/toolkit/logging/Loggable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logRender(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/logging/Loggable;)V

    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;
    .param p2, "navigator"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;
    .param p3, "intents"    # Lio/reactivex/Observable;

    .prologue
    .line 44
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 59
    invoke-static {}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;)Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 44
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    return-object v0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 52
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;)V
    .locals 1
    .param p0, "navigator"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;
    .param p1, "itemClickedIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;->item()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;->navigateToClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "interactor"    # Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;
    .param p1, "navigator"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;
    .param p2, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    const-class v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 47
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 48
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;->initialLoadTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;

    .line 51
    invoke-virtual {p2, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$10;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 52
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 53
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;->initialLoadTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;

    .line 55
    invoke-virtual {p2, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 56
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$12;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 57
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 46
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;
    .param p2, "navigator"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;
    .param p3, "i"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    invoke-static {p3}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 45
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 44
    return-object v0
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/presentation/lce/LceViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/lce/LceViewState;

    .prologue
    .line 93
    invoke-static {p0}, Lcom/microsoft/xbox/presentation/lce/DefaultLceViewStateLogger;->toLogString(Lcom/microsoft/xbox/presentation/lce/LceViewState;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private stateReducer(Lcom/microsoft/xbox/presentation/lce/LceViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .locals 5
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;",
            ")",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 67
    .local p1, "previousState":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    instance-of v1, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    if-eqz v1, :cond_3

    move-object v0, p2

    .line 68
    check-cast v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    .line 70
    .local v0, "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 71
    iget-object v1, v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->content:Ljava/lang/Object;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->withContent(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object p1

    .line 82
    .end local v0    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    :cond_0
    :goto_0
    return-object p1

    .line 72
    .restart local v0    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->isFailure()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 73
    iget-object v1, v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->error:Ljava/lang/Throwable;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object p1

    goto :goto_0

    .line 75
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object p1

    goto :goto_0

    .line 77
    .end local v0    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    :cond_3
    instance-of v1, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult;

    if-nez v1, :cond_0

    .line 81
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Couldn\'t explicitly reduce state. previousState: %s. result: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected initialSetup()V
    .locals 3

    .prologue
    .line 87
    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    move-result-object v1

    .line 88
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->bindViewIntents(Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 89
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 90
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 91
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 92
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 93
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 94
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 96
    .local v0, "viewStates":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->bindViewState(Lio/reactivex/Observable;Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;)V

    .line 97
    return-void
.end method
