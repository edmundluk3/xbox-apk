.class public Lcom/microsoft/xbox/xle/app/activity/ActivityFeedScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ActivityFeedScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    const-string v0, "Home"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 20
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 21
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedScreen;->onCreateContentView()V

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 24
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f030136

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedScreen;->setContentView(I)V

    .line 37
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStart()V

    .line 29
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Launch"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isTrackingPerformance(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Launch"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->endTrackPerformance(Ljava/lang/String;)V

    .line 32
    :cond_0
    return-void
.end method
