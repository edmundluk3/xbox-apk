.class public final enum Lcom/microsoft/xbox/service/clubs/ClubProfileService;
.super Ljava/lang/Enum;
.source "ClubProfileService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/clubs/IClubProfileService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubProfileService;",
        ">;",
        "Lcom/microsoft/xbox/service/clubs/IClubProfileService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubProfileService;

.field private static final CONTRACT_VERSION:I = 0x2

.field private static final ENDPOINT:Ljava/lang/String; = "https://clubprofile.xboxlive.com/clubs/%d/profile "

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubProfileService;

.field private static final REQUEST_GSON:Lcom/google/gson/Gson;

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubProfileService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubProfileService;

    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/clubs/ClubProfileService;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubProfileService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubProfileService;

    .line 27
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->TAG:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->STATIC_HEADERS:Ljava/util/List;

    .line 38
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-contract-version"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 42
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    .line 43
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->serializeNulls()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequestSerializer;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequestSerializer;-><init>()V

    .line 44
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->REQUEST_GSON:Lcom/google/gson/Gson;

    .line 46
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic lambda$updateClubProfile$0(JLcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 6
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "updateRequest"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 55
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://clubprofile.xboxlive.com/clubs/%d/profile "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 56
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->STATIC_HEADERS:Ljava/util/List;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->REQUEST_GSON:Lcom/google/gson/Gson;

    .line 58
    invoke-virtual {v2, p2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 55
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubProfileService;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubProfileService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubProfileService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubProfileService;

    return-object v0
.end method


# virtual methods
.method public updateClubProfile(JLcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;)Z
    .locals 3
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "updateRequest"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 51
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 52
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 54
    invoke-static {p1, p2, p3}, Lcom/microsoft/xbox/service/clubs/ClubProfileService$$Lambda$1;->lambdaFactory$(JLcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->requestAccepting2xxs(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0
.end method
