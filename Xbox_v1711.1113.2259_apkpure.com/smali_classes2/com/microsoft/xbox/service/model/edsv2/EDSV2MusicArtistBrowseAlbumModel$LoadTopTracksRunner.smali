.class Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "EDSV2MusicArtistBrowseAlbumModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadTopTracksRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$1;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 103
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getEDSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    .line 105
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3ef

    const/16 v3, 0x3f1

    sget-object v5, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_MOSTPOPULAR:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const/4 v7, 0x0

    const/16 v8, 0x14

    move-object v6, v4

    invoke-interface/range {v0 .. v8}, Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;->browseMediaItemList(Ljava/lang/String;IILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v0

    .line 102
    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 116
    const-wide/16 v0, 0xfaf

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->access$100(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 112
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 97
    return-void
.end method
