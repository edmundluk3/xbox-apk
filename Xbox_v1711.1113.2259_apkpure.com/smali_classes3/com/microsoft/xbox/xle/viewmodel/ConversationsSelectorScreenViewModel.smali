.class public Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ConversationsSelectorScreenViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final activityFeedItemLocator:Ljava/lang/String;

.field private final messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final originatingScreen:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation
.end field

.field private shouldForceUpdateSummaryAdapter:Z

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 36
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 41
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 43
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 44
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getActivityFeedItemLocator()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->activityFeedItemLocator:Ljava/lang/String;

    .line 45
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getMessagesOriginatingScreen()Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->originatingScreen:Ljava/lang/Class;

    .line 46
    return-void
.end method

.method public static getGroupSenderGamertag(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "conversationSummary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupMemberGamertag(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clearForceUpdateSummaryAdapter()V
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->shouldForceUpdateSummaryAdapter:Z

    .line 152
    return-void
.end method

.method public confirm()V
    .locals 0

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->goBack()V

    .line 70
    return-void
.end method

.method public getGroupMemberGamertag(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "conversationSummary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 96
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 98
    new-array v1, v3, [Ljava/lang/String;

    aput-object p2, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->getGroupSenderGamertag(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 99
    .local v0, "gamertags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 100
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 103
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getGroupSenderGamertagText(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .param p1, "conversationSummary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 107
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->getGroupSenderGamertag(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 109
    .local v0, "gamertags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 110
    const-string v1, ", "

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 113
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMembers()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    if-eqz v0, :cond_0

    .line 84
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageData:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->setUpdateTypesToCheck(Ljava/util/EnumSet;)V

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->loadMessageListAsync(Z)V

    .line 87
    :cond_0
    return-void
.end method

.method public navigateToFriendsPicker()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 159
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 160
    .local v0, "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putIsGroupConversation(Z)V

    .line 161
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->originatingScreen:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMessagesOriginatingScreen(Ljava/lang/Class;)V

    .line 162
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/FriendsSelectorScreen;

    invoke-virtual {p0, v1, v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 163
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 51
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 59
    :cond_0
    return-void

    .line 57
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 66
    :cond_0
    return-void
.end method

.method public sendMessageToConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 5
    .param p1, "conversationSummary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 166
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 167
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 168
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putComposeMessageConversationSummary(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 169
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->activityFeedItemLocator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putActivityFeedItemLocator(Ljava/lang/String;)V

    .line 170
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->originatingScreen:Ljava/lang/Class;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMessagesOriginatingScreen(Ljava/lang/Class;)V

    .line 173
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->originatingScreen:Ljava/lang/Class;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;

    invoke-virtual {v2, v3, v4, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopTillScreenThenPush(Ljava/lang/Class;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_0
    return-void

    .line 174
    :catch_0
    move-exception v0

    .line 175
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Failed to pop till the originating screen and push the ActivityFeedShareToMessageScreen back"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public shouldForceUpdateSummaryAdapter()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->shouldForceUpdateSummaryAdapter:Z

    return v0
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    .line 119
    .local v0, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 139
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected update type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->updateAdapter()V

    .line 144
    :goto_1
    return-void

    .line 121
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-nez v1, :cond_0

    .line 122
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "update not final, ignore update"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 126
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_1

    .line 127
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 136
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->shouldForceUpdateSummaryAdapter:Z

    goto :goto_0

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_2

    .line 129
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_2

    .line 130
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 131
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_2

    .line 133
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_2

    .line 119
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
