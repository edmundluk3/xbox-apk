.class Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;
.super Ljava/lang/Object;
.source "ExponentialGeometricAverage.java"


# instance fields
.field private mCount:I

.field private final mCutover:I

.field private final mDecayConstant:D

.field private mValue:D


# direct methods
.method public constructor <init>(D)V
    .locals 3
    .param p1, "decayConstant"    # D

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mValue:D

    .line 25
    iput-wide p1, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mDecayConstant:D

    .line 26
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    iput v0, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mCutover:I

    .line 29
    return-void

    .line 26
    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    div-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public addMeasurement(D)V
    .locals 13
    .param p1, "measurement"    # D

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 36
    iget-wide v6, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mDecayConstant:D

    sub-double v0, v10, v6

    .line 37
    .local v0, "keepConstant":D
    iget v6, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mCount:I

    iget v7, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mCutover:I

    if-le v6, v7, :cond_0

    .line 38
    iget-wide v6, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mValue:D

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    mul-double/2addr v6, v0

    iget-wide v8, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mDecayConstant:D

    invoke-static {p1, p2}, Ljava/lang/Math;->log(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mValue:D

    .line 46
    :goto_0
    iget v6, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mCount:I

    .line 47
    return-void

    .line 39
    :cond_0
    iget v6, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mCount:I

    if-lez v6, :cond_1

    .line 40
    iget v6, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mCount:I

    int-to-double v6, v6

    mul-double/2addr v6, v0

    iget v8, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mCount:I

    int-to-double v8, v8

    add-double/2addr v8, v10

    div-double v4, v6, v8

    .line 41
    .local v4, "retained":D
    sub-double v2, v10, v4

    .line 42
    .local v2, "newcomer":D
    iget-wide v6, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mValue:D

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    mul-double/2addr v6, v4

    invoke-static {p1, p2}, Ljava/lang/Math;->log(D)D

    move-result-wide v8

    mul-double/2addr v8, v2

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mValue:D

    goto :goto_0

    .line 44
    .end local v2    # "newcomer":D
    .end local v4    # "retained":D
    :cond_1
    iput-wide p1, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mValue:D

    goto :goto_0
.end method

.method public getAverage()D
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mValue:D

    return-wide v0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 57
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mValue:D

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/network/connectionclass/ExponentialGeometricAverage;->mCount:I

    .line 59
    return-void
.end method
