.class Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SkypeDeleteMessageRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final clientMessageId:Ljava/lang/String;

.field private final messageId:Ljava/lang/String;

.field private final targetXuid:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "messageId"    # Ljava/lang/String;
    .param p3, "targetXuid"    # Ljava/lang/String;
    .param p4, "clientMessageId"    # Ljava/lang/String;

    .prologue
    .line 913
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 914
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->messageId:Ljava/lang/String;

    .line 915
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->targetXuid:Ljava/lang/String;

    .line 916
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->clientMessageId:Ljava/lang/String;

    .line 917
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 921
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->targetXuid:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 922
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    const-string v1, "8:xbox:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->targetXuid:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->messageId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->clientMessageId:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->deleteSkypeMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 906
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 936
    const-wide/16 v0, 0xbed

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 931
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->targetXuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;->messageId:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->access$300(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 927
    return-void
.end method
