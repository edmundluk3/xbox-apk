.class public Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "GameOverviewScreenAdapter.java"


# instance fields
.field private final availableOnPlatforms:Landroid/view/View;

.field private final buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final buySubscriptionText:Landroid/widget/TextView;

.field private final companionButton:Landroid/widget/RelativeLayout;

.field private final context:Landroid/content/Context;

.field private final coopTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final descriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final descriptionTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final desktopMinRequirementsView:Landroid/widget/LinearLayout;

.field private final desktopRecRequirementsView:Landroid/widget/LinearLayout;

.field private final desktopSystemRequirements:Landroid/view/View;

.field private final developerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final filterLayout:Landroid/widget/LinearLayout;

.field private final gamepassDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final gridAdapter:Landroid/widget/BaseAdapter;

.field private final helpButton:Landroid/widget/RelativeLayout;

.field private final inflater:Landroid/view/LayoutInflater;

.field private final installButton:Landroid/widget/RelativeLayout;

.field private final installButtonSubscriptionLayout:Landroid/widget/LinearLayout;

.field private final multiplayerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final playButton:Landroid/widget/RelativeLayout;

.field private final playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final playSubscriptionText:Landroid/widget/TextView;

.field private final profileButton:Landroid/widget/RelativeLayout;

.field private final publisherTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final purchaseButton:Landroid/widget/RelativeLayout;

.field private final purchaseButtonIconView:Landroid/widget/TextView;

.field private final purchaseButtonLabel:Landroid/widget/TextView;

.field private final purchaseButtonPrice:Landroid/view/View;

.field private final purchaseButtonStrikethroughText:Landroid/widget/TextView;

.field private final purchaseButtonText:Landroid/widget/TextView;

.field private final purchasedDateTextView:Landroid/widget/TextView;

.field private final purchasedTextLayout:Landroid/widget/LinearLayout;

.field private final ratingLevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

.field private final ratingPEGILevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

.field private final ratingView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final releaseDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;)V
    .locals 10
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 433
    new-instance v7, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;

    invoke-direct {v7, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)V

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->gridAdapter:Landroid/widget/BaseAdapter;

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->context:Landroid/content/Context;

    .line 86
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->context:Landroid/content/Context;

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 87
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    .line 88
    const v7, 0x7f0e061b

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->screenBody:Landroid/view/View;

    .line 89
    const v7, 0x7f0e061c

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 91
    const v7, 0x7f0e062f

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->descriptionTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 92
    const v7, 0x7f0e0630

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->descriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 93
    const v7, 0x7f0e060b

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->developerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 94
    const v7, 0x7f0e060c

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->publisherTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 95
    const v7, 0x7f0e060d

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->ratingView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 96
    const v7, 0x7f0e060e

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 97
    const v7, 0x7f0e060f

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->releaseDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 98
    const v7, 0x7f0e0610

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->multiplayerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 99
    const v7, 0x7f0e0611

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->coopTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 100
    const v7, 0x7f0e062c

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchasedTextLayout:Landroid/widget/LinearLayout;

    .line 101
    const v7, 0x7f0e062e

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchasedDateTextView:Landroid/widget/TextView;

    .line 102
    const v7, 0x7f0e0967

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonIconView:Landroid/widget/TextView;

    .line 103
    const v7, 0x7f0e063a

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->ratingLevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    .line 104
    const v7, 0x7f0e063b

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->ratingPEGILevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

    .line 106
    const v7, 0x7f0e061d

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->gamepassDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 107
    const v7, 0x7f0e061f

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    .line 108
    const v7, 0x7f0e061e

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->installButton:Landroid/widget/RelativeLayout;

    .line 109
    const v7, 0x7f0e0629

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->helpButton:Landroid/widget/RelativeLayout;

    .line 110
    const v7, 0x7f0e0624

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->profileButton:Landroid/widget/RelativeLayout;

    .line 111
    const v7, 0x7f0e0626

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->companionButton:Landroid/widget/RelativeLayout;

    .line 112
    const v7, 0x7f0e0620

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    .line 113
    const v7, 0x7f0e0969

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonPrice:Landroid/view/View;

    .line 114
    const v7, 0x7f0e096b

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonText:Landroid/widget/TextView;

    .line 115
    const v7, 0x7f0e096a

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    .line 116
    const v7, 0x7f0e096c

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 117
    const v7, 0x7f0e096d

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    .line 118
    const v7, 0x7f0e0977

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 119
    const v7, 0x7f0e0978

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    .line 120
    const v7, 0x7f0e0968

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonLabel:Landroid/widget/TextView;

    .line 121
    const v7, 0x7f0e071d

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->installButtonSubscriptionLayout:Landroid/widget/LinearLayout;

    .line 123
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    if-eqz v7, :cond_0

    .line 124
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    sget-boolean v7, Lcom/microsoft/xbox/toolkit/Build;->IncludePurchaseFlow:Z

    if-eqz v7, :cond_2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPurchaseBlocked()Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v7, 0x0

    :goto_0
    invoke-virtual {v8, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 127
    :cond_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    if-eqz v7, :cond_1

    .line 128
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v8

    or-int/lit8 v8, v8, 0x10

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 131
    :cond_1
    const v7, 0x7f0e0621

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->filterLayout:Landroid/widget/LinearLayout;

    .line 132
    const v7, 0x7f0e0623

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 135
    .local v0, "filterSpinner":Landroid/widget/Spinner;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v5, "platformNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->getPlatformNameResources()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 137
    .local v2, "i":Ljava/lang/Integer;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->context:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 124
    .end local v0    # "filterSpinner":Landroid/widget/Spinner;
    .end local v2    # "i":Ljava/lang/Integer;
    .end local v5    # "platformNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    const/16 v7, 0x8

    goto :goto_0

    .line 140
    .restart local v0    # "filterSpinner":Landroid/widget/Spinner;
    .restart local v5    # "platformNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->context:Landroid/content/Context;

    const v8, 0x1090008

    invoke-direct {v1, v7, v8, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 143
    .local v1, "filterSpinnerAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v7, 0x7f03020a

    invoke-virtual {v1, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 144
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 145
    invoke-virtual {v0}, Landroid/widget/Spinner;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    const/high16 v8, -0x1000000

    sget-object v9, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v7, v8, v9}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 146
    new-instance v7, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$1;

    invoke-direct {v7, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)V

    invoke-virtual {v0, v7}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 162
    const v7, 0x7f0e0637

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->availableOnPlatforms:Landroid/view/View;

    .line 163
    const v7, 0x7f0e0639

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    .line 164
    .local v4, "platformIconGrid":Lcom/microsoft/xbox/toolkit/ui/XLEGridView;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->gridAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 165
    const v7, 0x7f0e0631

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->desktopSystemRequirements:Landroid/view/View;

    .line 167
    const v7, 0x7f0e0633

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->desktopRecRequirementsView:Landroid/widget/LinearLayout;

    .line 168
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->desktopRecRequirementsView:Landroid/widget/LinearLayout;

    const v8, 0x7f0e0634

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 169
    .local v6, "recRequirementTitle":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->context:Landroid/content/Context;

    const v8, 0x7f070c53

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    const v7, 0x7f0e0635

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->desktopMinRequirementsView:Landroid/widget/LinearLayout;

    .line 172
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->desktopMinRequirementsView:Landroid/widget/LinearLayout;

    const v8, 0x7f0e0636

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 173
    .local v3, "minRequirementTitle":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->context:Landroid/content/Context;

    const v8, 0x7f070c4e

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method private getDisplayPriceText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getIsFree()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070afd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDisplayPrice()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->launchGame()V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->launchHelp()V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->purchaseTitle()V

    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->installTitle()V

    return-void
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->launchGameProfile()V

    return-void
.end method

.method static synthetic lambda$onStart$5(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->launchCompanion()V

    return-void
.end method

.method private updateRequirements(Landroid/widget/LinearLayout;Ljava/util/List;)V
    .locals 8
    .param p1, "requirementsViewSection"    # Landroid/widget/LinearLayout;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 414
    .local p2, "requirements":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 415
    const/16 v4, 0x8

    invoke-virtual {p1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 431
    :cond_0
    return-void

    .line 417
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 418
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 419
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/util/Pair;

    .line 420
    .local v2, "requirement":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v2, :cond_2

    .line 421
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    const v6, 0x7f030110

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 422
    .local v1, "newLine":Landroid/view/View;
    const v4, 0x7f0e0602

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 423
    .local v0, "name":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v4, 0x7f0e0603

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 424
    .local v3, "value":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    iget-object v4, v2, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 425
    iget-object v4, v2, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 426
    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private updateSubscriptionDetails()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 363
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getSubscriptionText()Ljava/lang/String;

    move-result-object v1

    .line 364
    .local v1, "subscriptionName":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getSubscriptionImageResourceId()I

    move-result v0

    .line 366
    .local v0, "subscriptionImageResourceId":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 367
    if-lez v0, :cond_1

    .line 368
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageResource(I)V

    .line 369
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageResource(I)V

    .line 370
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 371
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 372
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 373
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 375
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 376
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 378
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 379
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 380
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0

    .line 382
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 383
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 384
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 385
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 386
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonIconView:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private updateXPADetails()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 396
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->isXPA()Z

    move-result v1

    .line 397
    .local v1, "isXPA":Z
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->filterLayout:Landroid/widget/LinearLayout;

    invoke-static {v5, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 398
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->availableOnPlatforms:Landroid/view/View;

    invoke-static {v5, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 400
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getPlatformType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->Desktop:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    if-ne v5, v6, :cond_0

    const/4 v0, 0x1

    .line 401
    .local v0, "isViewingPC":Z
    :goto_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getRecRequirements()Ljava/util/List;

    move-result-object v3

    .line 402
    .local v3, "recRequirements":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getMinRequirements()Ljava/util/List;

    move-result-object v2

    .line 404
    .local v2, "minRequirements":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz v0, :cond_1

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->hasDesktopSystemRequirements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 405
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->desktopSystemRequirements:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 406
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->desktopRecRequirementsView:Landroid/widget/LinearLayout;

    invoke-direct {p0, v4, v3}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->updateRequirements(Landroid/widget/LinearLayout;Ljava/util/List;)V

    .line 407
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->desktopMinRequirementsView:Landroid/widget/LinearLayout;

    invoke-direct {p0, v4, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->updateRequirements(Landroid/widget/LinearLayout;Ljava/util/List;)V

    .line 411
    :goto_1
    return-void

    .end local v0    # "isViewingPC":Z
    .end local v2    # "minRequirements":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v3    # "recRequirements":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_0
    move v0, v4

    .line 400
    goto :goto_0

    .line 409
    .restart local v0    # "isViewingPC":Z
    .restart local v2    # "minRequirements":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v3    # "recRequirements":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->desktopSystemRequirements:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 178
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->helpButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->helpButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->installButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->installButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->profileButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_4

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->profileButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->companionButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_5

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->companionButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    :cond_5
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 204
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->helpButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->helpButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->installButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->installButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    :cond_3
    return-void
.end method

.method public updateViewOverride()V
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 222
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->isBusy()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->updateLoadingIndicator(Z)V

    .line 224
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v7

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 225
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->updateXPADetails()V

    .line 227
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 228
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->descriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 229
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->descriptionTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 232
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPurchaseBlocked()Z

    move-result v1

    .line 233
    .local v1, "isPurchaseBlocked":Z
    const/16 v2, 0x8

    .line 234
    .local v2, "playButtonVisibility":I
    const/16 v0, 0x8

    .line 236
    .local v0, "installButtonVisibility":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getIsPlayable()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 237
    const/4 v2, 0x0

    .line 238
    const/4 v0, 0x0

    .line 241
    :cond_1
    sget-boolean v3, Lcom/microsoft/xbox/toolkit/Build;->IncludePurchaseFlow:Z

    if-eqz v3, :cond_13

    if-nez v1, :cond_13

    .line 242
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonLabel:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getPurchaseButtonLabelText()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 243
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_2

    .line 244
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->isBusy()Z

    move-result v3

    if-nez v3, :cond_9

    move v3, v4

    :goto_0
    invoke-virtual {v7, v3}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 247
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_3

    .line 248
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->isBusy()Z

    move-result v3

    if-nez v3, :cond_a

    move v3, v4

    :goto_1
    invoke-virtual {v7, v3}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 251
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->installButton:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_4

    .line 252
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->installButton:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->isBusy()Z

    move-result v3

    if-nez v3, :cond_b

    move v3, v4

    :goto_2
    invoke-virtual {v7, v3}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 255
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->gamepassDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getIsGamepassTitle()Z

    move-result v7

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 256
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->installButtonSubscriptionLayout:Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getIsGamepassTitle()Z

    move-result v7

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 258
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getHasInstallRights()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 259
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 260
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->installButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 261
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 262
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getIsPurchasable()Z

    move-result v3

    if-nez v3, :cond_c

    .line 264
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 265
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 266
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 267
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 278
    :cond_5
    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getIsPurchasable()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 279
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDisplayPrice()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_10

    .line 280
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 281
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonLabel:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getIsFree()Z

    move-result v3

    if-eqz v3, :cond_e

    move v3, v6

    :goto_4
    invoke-static {v7, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 282
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonText:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->getDisplayPriceText()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 284
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDisplayListPrice()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_f

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDisplayPrice()Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDisplayListPrice()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 286
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDisplayListPrice()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 287
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonPrice:Landroid/view/View;

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070c30

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDisplayListPrice()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v5

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDisplayPrice()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v4

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 293
    :goto_5
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->updateSubscriptionDetails()V

    .line 307
    :cond_6
    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getPurchaseDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_12

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getIsGamepassTitle()Z

    move-result v3

    if-nez v3, :cond_12

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getIsTrial()Z

    move-result v3

    if-nez v3, :cond_12

    .line 308
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchasedDateTextView:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getPurchaseDate()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 309
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchasedTextLayout:Landroid/widget/LinearLayout;

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 319
    :goto_7
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->profileButton:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->shouldShowGameHubAndHelpButton()Z

    move-result v3

    if-eqz v3, :cond_14

    move v3, v5

    :goto_8
    invoke-static {v7, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 320
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->helpButton:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->shouldShowGameHubAndHelpButton()Z

    move-result v3

    if-eqz v3, :cond_15

    move v3, v5

    :goto_9
    invoke-static {v7, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 321
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->companionButton:Landroid/widget/RelativeLayout;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->shouldShowCompanionButton()Z

    move-result v7

    if-eqz v7, :cond_7

    move v6, v5

    :cond_7
    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 323
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->developerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDeveloper()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_16

    move v3, v4

    :goto_a
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0703c0

    .line 324
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDeveloper()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 323
    invoke-static {v6, v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 326
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->publisherTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getPublisher()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_17

    move v3, v4

    :goto_b
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0703cc

    .line 327
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getPublisher()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 326
    invoke-static {v6, v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 329
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_18

    move v3, v4

    :goto_c
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0703c8

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 331
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->releaseDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getReleaseDate()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_19

    move v3, v4

    :goto_d
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0703ce

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    .line 332
    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getReleaseDate()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 331
    invoke-static {v6, v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 334
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->ratingView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1a

    move v3, v4

    :goto_e
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0703cd

    .line 335
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 334
    invoke-static {v6, v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 336
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->multiplayerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getMultiplayer()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1b

    move v3, v4

    :goto_f
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0703ca

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    .line 337
    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getMultiplayer()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 336
    invoke-static {v6, v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 339
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->coopTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getCoop()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1c

    :goto_10
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f0703be

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getCoop()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v4, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 344
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 345
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->getUseAllImage()Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 347
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->ratingPEGILevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 348
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->ratingPEGILevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v4

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getRatingDescriptors()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->setRatingLevelAndDescriptors(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;Ljava/util/List;)V

    .line 349
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->ratingPEGILevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->setVisibility(I)V

    .line 360
    :cond_8
    :goto_11
    return-void

    :cond_9
    move v3, v5

    .line 244
    goto/16 :goto_0

    :cond_a
    move v3, v5

    .line 248
    goto/16 :goto_1

    :cond_b
    move v3, v5

    .line 252
    goto/16 :goto_2

    .line 269
    :cond_c
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getIsAdult()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 270
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->updateSubscriptionDetails()V

    goto/16 :goto_3

    .line 274
    :cond_d
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 275
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->installButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_3

    :cond_e
    move v3, v5

    .line 281
    goto/16 :goto_4

    .line 290
    :cond_f
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_5

    .line 295
    :cond_10
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 296
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 297
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 298
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_6

    .line 300
    :cond_11
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDisplayListPrice()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 303
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 304
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_6

    .line 311
    :cond_12
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchasedTextLayout:Landroid/widget/LinearLayout;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_7

    .line 314
    :cond_13
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 315
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 316
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->purchasedDateTextView:Landroid/widget/TextView;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_7

    :cond_14
    move v3, v6

    .line 319
    goto/16 :goto_8

    :cond_15
    move v3, v6

    .line 320
    goto/16 :goto_9

    :cond_16
    move v3, v5

    .line 323
    goto/16 :goto_a

    :cond_17
    move v3, v5

    .line 326
    goto/16 :goto_b

    :cond_18
    move v3, v5

    .line 329
    goto/16 :goto_c

    :cond_19
    move v3, v5

    .line 331
    goto/16 :goto_d

    :cond_1a
    move v3, v5

    .line 334
    goto/16 :goto_e

    :cond_1b
    move v3, v5

    .line 336
    goto/16 :goto_f

    :cond_1c
    move v4, v5

    .line 339
    goto/16 :goto_10

    .line 352
    :cond_1d
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->ratingLevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 353
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->ratingLevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v4

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getRatingDescriptors()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->setRatingLevelAndDescriptors(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;Ljava/util/List;)V

    .line 354
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->ratingLevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->setVisibility(I)V

    goto/16 :goto_11
.end method
