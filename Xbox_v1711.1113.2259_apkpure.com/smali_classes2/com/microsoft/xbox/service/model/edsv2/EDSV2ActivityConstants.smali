.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityConstants;
.super Ljava/lang/Object;
.source "EDSV2ActivityConstants.java"


# static fields
.field public static final EDSV2ACTIVITYPURCHASESTATE_FREE:I = 0x1

.field public static final EDSV2ACTIVITYPURCHASESTATE_FREEREQUIRESGOLD:I = 0x2

.field public static final EDSV2ACTIVITYPURCHASESTATE_INVALID:I = 0x0

.field public static final EDSV2ACTIVITYPURCHASESTATE_PURCHASED:I = 0x3

.field public static final EDSV2ACTIVITYPURCHASESTATE_PURCHASEDREQUIRESGOLD:I = 0x4

.field public static final EDSV2ACTIVITYPURCHASESTATE_REQUIRESPURCHASE:I = 0x5

.field public static final EDSV2ACTIVITYPURCHASESTATE_REQUIRESPURCHASEANDGOLD:I = 0x6

.field public static final EDSV2ACTIVITYTYPE_DEEPLINKAPP:I = 0x2

.field public static final EDSV2ACTIVITYTYPE_HOSTEDAPP:I = 0x1

.field public static final EDSV2ACTIVITYTYPE_UNKNOWN:I = 0x0

.field public static final EDSV2SUBSCRIPTIONLEVEL_GOLD:I = 0x1

.field public static final EDSV2SUBSCRIPTIONLEVEL_SILVER:I = 0x2

.field public static final EDSV2SUBSCRIPTIONLEVEL_UNKNOWN:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
