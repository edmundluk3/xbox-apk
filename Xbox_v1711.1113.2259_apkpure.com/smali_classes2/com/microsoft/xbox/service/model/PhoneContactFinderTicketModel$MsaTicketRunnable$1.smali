.class Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable$1;
.super Ljava/lang/Object;
.source "PhoneContactFinderTicketModel.java"

# interfaces
.implements Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable$1;->this$1:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountAcquired(Lcom/microsoft/xbox/idp/jobs/MSAJob;Lcom/microsoft/onlineid/UserAccount;)V
    .locals 3
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;
    .param p2, "userAccount"    # Lcom/microsoft/onlineid/UserAccount;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable$1;->this$1:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;->this$0:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->access$200(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;Ljava/lang/String;Z)V

    .line 114
    return-void
.end method

.method public onFailure(Lcom/microsoft/xbox/idp/jobs/MSAJob;Ljava/lang/Exception;)V
    .locals 3
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable$1;->this$1:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;->this$0:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->access$200(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;Ljava/lang/String;Z)V

    .line 99
    return-void
.end method

.method public onSignedOut(Lcom/microsoft/xbox/idp/jobs/MSAJob;)V
    .locals 3
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable$1;->this$1:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;->this$0:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->access$200(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;Ljava/lang/String;Z)V

    .line 109
    return-void
.end method

.method public onTicketAcquired(Lcom/microsoft/xbox/idp/jobs/MSAJob;Lcom/microsoft/onlineid/Ticket;)V
    .locals 3
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;
    .param p2, "ticket"    # Lcom/microsoft/onlineid/Ticket;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable$1;->this$1:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;->this$0:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    invoke-virtual {p2}, Lcom/microsoft/onlineid/Ticket;->getValue()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->access$200(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;Ljava/lang/String;Z)V

    .line 119
    return-void
.end method

.method public onUiNeeded(Lcom/microsoft/xbox/idp/jobs/MSAJob;)V
    .locals 3
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable$1;->this$1:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;->this$0:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->access$200(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;Ljava/lang/String;Z)V

    .line 94
    return-void
.end method

.method public onUserCancel(Lcom/microsoft/xbox/idp/jobs/MSAJob;)V
    .locals 3
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable$1;->this$1:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;->this$0:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->access$200(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;Ljava/lang/String;Z)V

    .line 104
    return-void
.end method
