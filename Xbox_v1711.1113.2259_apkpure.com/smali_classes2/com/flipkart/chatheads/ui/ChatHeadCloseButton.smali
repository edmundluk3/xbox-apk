.class public Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;
.super Landroid/widget/ImageView;
.source "ChatHeadCloseButton.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;
    }
.end annotation


# static fields
.field private static final PERC_PARENT_HEIGHT:F = 0.05f

.field private static final PERC_PARENT_WIDTH:F = 0.1f


# instance fields
.field private centerX:I

.field private centerY:I

.field private chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

.field private disappeared:Z

.field private listener:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;

.field private mParentHeight:I

.field private mParentWidth:I

.field private scaleSpring:Lcom/facebook/rebound/Spring;

.field private xSpring:Lcom/facebook/rebound/Spring;

.field private ySpring:Lcom/facebook/rebound/Spring;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/flipkart/chatheads/ui/ChatHeadManager;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "manager"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .param p3, "maxHeight"    # I
    .param p4, "maxWidth"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-direct {p0, p2, p3, p4}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->init(Lcom/flipkart/chatheads/ui/ChatHeadManager;II)V

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;Lcom/facebook/rebound/Spring;)I
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;
    .param p1, "x1"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getXFromSpring(Lcom/facebook/rebound/Spring;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;Lcom/facebook/rebound/Spring;)I
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;
    .param p1, "x1"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getYFromSpring(Lcom/facebook/rebound/Spring;)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;)Lcom/facebook/rebound/Spring;
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    return-object v0
.end method

.method private getTranslationFromSpring(DFI)D
    .locals 11
    .param p1, "springValue"    # D
    .param p3, "percent"    # F
    .param p4, "fullValue"    # I

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 173
    int-to-float v0, p4

    mul-float v10, p3, v0

    .line 174
    .local v10, "widthToCover":F
    const-wide/16 v2, 0x0

    int-to-double v4, p4

    neg-float v0, v10

    div-float/2addr v0, v1

    float-to-double v6, v0

    div-float v0, v10, v1

    float-to-double v8, v0

    move-wide v0, p1

    invoke-static/range {v0 .. v9}, Lcom/facebook/rebound/SpringUtil;->mapValueFromRangeToRange(DDDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method private getXFromSpring(Lcom/facebook/rebound/Spring;)I
    .locals 4
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 87
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->centerX:I

    invoke-virtual {p1}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v2

    double-to-int v1, v2

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    return v0
.end method

.method private getYFromSpring(Lcom/facebook/rebound/Spring;)I
    .locals 4
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 83
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->centerY:I

    invoke-virtual {p1}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v2

    double-to-int v1, v2

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    return v0
.end method

.method private init(Lcom/flipkart/chatheads/ui/ChatHeadManager;II)V
    .locals 3
    .param p1, "manager"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .param p2, "maxHeight"    # I
    .param p3, "maxWidth"    # I

    .prologue
    .line 49
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 51
    sget v1, Lcom/flipkart/chatheads/R$drawable;->dismiss_big:I

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->setImageResource(I)V

    .line 52
    invoke-static {}, Lcom/facebook/rebound/SpringSystem;->create()Lcom/facebook/rebound/SpringSystem;

    move-result-object v0

    .line 53
    .local v0, "springSystem":Lcom/facebook/rebound/SpringSystem;
    invoke-virtual {v0}, Lcom/facebook/rebound/SpringSystem;->createSpring()Lcom/facebook/rebound/Spring;

    move-result-object v1

    iput-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->xSpring:Lcom/facebook/rebound/Spring;

    .line 54
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->xSpring:Lcom/facebook/rebound/Spring;

    new-instance v2, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$1;

    invoke-direct {v2, p0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$1;-><init>(Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;Lcom/flipkart/chatheads/ui/ChatHeadManager;)V

    invoke-virtual {v1, v2}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 62
    invoke-virtual {v0}, Lcom/facebook/rebound/SpringSystem;->createSpring()Lcom/facebook/rebound/Spring;

    move-result-object v1

    iput-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    .line 63
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    new-instance v2, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$2;

    invoke-direct {v2, p0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$2;-><init>(Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;Lcom/flipkart/chatheads/ui/ChatHeadManager;)V

    invoke-virtual {v1, v2}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 71
    invoke-virtual {v0}, Lcom/facebook/rebound/SpringSystem;->createSpring()Lcom/facebook/rebound/Spring;

    move-result-object v1

    iput-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->scaleSpring:Lcom/facebook/rebound/Spring;

    .line 72
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->scaleSpring:Lcom/facebook/rebound/Spring;

    new-instance v2, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$3;

    invoke-direct {v2, p0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$3;-><init>(Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;)V

    invoke-virtual {v1, v2}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 80
    return-void
.end method


# virtual methods
.method public appear()V
    .locals 6

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 92
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    sget-object v3, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v2, v3}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 93
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->xSpring:Lcom/facebook/rebound/Spring;

    sget-object v3, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v2, v3}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 94
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->scaleSpring:Lcom/facebook/rebound/Spring;

    const-wide v4, 0x3fe99999a0000000L    # 0.800000011920929

    invoke-virtual {v2, v4, v5}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 95
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 96
    .local v1, "parent":Landroid/view/ViewParent;
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    move-object v2, v1

    .line 97
    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 98
    .local v0, "i":I
    check-cast v1, Landroid/view/ViewGroup;

    .end local v1    # "parent":Landroid/view/ViewParent;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v0, v2, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->bringToFront()V

    .line 102
    .end local v0    # "i":I
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->disappeared:Z

    .line 105
    :cond_1
    return-void
.end method

.method public disappear(ZZ)V
    .locals 8
    .param p1, "immediate"    # Z
    .param p2, "animate"    # Z

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 116
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    iget v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->mParentHeight:I

    iget v2, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->centerY:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getCloseButtonHeight()I

    move-result v2

    add-int/2addr v1, v2

    int-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 117
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    sget-object v1, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v0, v1}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 118
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->xSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0, v6, v7}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 119
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    new-instance v1, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$4;

    invoke-direct {v1, p0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$4;-><init>(Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;)V

    invoke-virtual {v0, v1}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 126
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->scaleSpring:Lcom/facebook/rebound/Spring;

    const-wide v2, 0x3fb99999a0000000L    # 0.10000000149011612

    invoke-virtual {v0, v2, v3}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 127
    if-nez p2, :cond_0

    .line 128
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    iget v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->mParentHeight:I

    int-to-double v2, v1

    invoke-virtual {v0, v2, v3, v4}, Lcom/facebook/rebound/Spring;->setCurrentValue(DZ)Lcom/facebook/rebound/Spring;

    .line 129
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->xSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0, v6, v7, v4}, Lcom/facebook/rebound/Spring;->setCurrentValue(DZ)Lcom/facebook/rebound/Spring;

    .line 131
    :cond_0
    iput-boolean v4, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->disappeared:Z

    .line 132
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->listener:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->listener:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;->onCloseButtonDisappear()V

    .line 134
    :cond_1
    return-void
.end method

.method public getEndValueX()I
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->xSpring:Lcom/facebook/rebound/Spring;

    invoke-direct {p0, v0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getXFromSpring(Lcom/facebook/rebound/Spring;)I

    move-result v0

    return v0
.end method

.method public getEndValueY()I
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    invoke-direct {p0, v0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getYFromSpring(Lcom/facebook/rebound/Spring;)I

    move-result v0

    return v0
.end method

.method public isAtRest()Z
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->xSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->isAtRest()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->isAtRest()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDisappeared()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->disappeared:Z

    return v0
.end method

.method public onCapture()V
    .locals 4

    .prologue
    .line 108
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->scaleSpring:Lcom/facebook/rebound/Spring;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 109
    return-void
.end method

.method public onParentHeightRefreshed()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getMaxWidth()I

    move-result v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->mParentWidth:I

    .line 144
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getMaxHeight()I

    move-result v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->mParentHeight:I

    .line 145
    return-void
.end method

.method public onRelease()V
    .locals 4

    .prologue
    .line 112
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->scaleSpring:Lcom/facebook/rebound/Spring;

    const-wide v2, 0x3fe999999999999aL    # 0.8

    invoke-virtual {v0, v2, v3}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 113
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 138
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 139
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->disappear(ZZ)V

    .line 140
    return-void
.end method

.method public pointTo(FF)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 162
    float-to-double v4, p1

    const v6, 0x3dcccccd    # 0.1f

    iget v7, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->mParentWidth:I

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getTranslationFromSpring(DFI)D

    move-result-wide v0

    .line 163
    .local v0, "translationX":D
    float-to-double v4, p2

    const v6, 0x3d4ccccd    # 0.05f

    iget v7, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->mParentHeight:I

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getTranslationFromSpring(DFI)D

    move-result-wide v2

    .line 164
    .local v2, "translationY":D
    iget-boolean v4, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->disappeared:Z

    if-nez v4, :cond_0

    .line 165
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->xSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v4, v0, v1}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 166
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v4, v2, v3}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 167
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->listener:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->listener:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;

    invoke-interface {v4}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;->onCloseButtonAppear()V

    .line 170
    .end local v0    # "translationX":D
    .end local v2    # "translationY":D
    :cond_0
    return-void
.end method

.method public setCenter(II)V
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 148
    const/4 v0, 0x0

    .line 149
    .local v0, "changed":Z
    iget v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->centerX:I

    if-ne p1, v1, :cond_0

    iget v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->centerY:I

    if-eq p2, v1, :cond_1

    .line 150
    :cond_0
    const/4 v0, 0x1

    .line 152
    :cond_1
    if-eqz v0, :cond_2

    .line 153
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->centerX:I

    .line 154
    iput p2, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->centerY:I

    .line 155
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->xSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/rebound/Spring;->setCurrentValue(DZ)Lcom/facebook/rebound/Spring;

    .line 156
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->ySpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/rebound/Spring;->setCurrentValue(DZ)Lcom/facebook/rebound/Spring;

    .line 158
    :cond_2
    return-void
.end method

.method public setListener(Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->listener:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;

    .line 42
    return-void
.end method
