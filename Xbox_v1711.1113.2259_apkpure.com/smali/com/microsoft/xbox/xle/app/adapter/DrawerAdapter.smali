.class public Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "DrawerAdapter.java"

# interfaces
.implements Landroid/support/v4/widget/DrawerLayout$DrawerListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final achievementsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final clubsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final connectionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final disableableDrawerLabels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final disableableDrawerRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final drawerLayout:Landroid/support/v4/widget/DrawerLayout;

.field private final dvrTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final feedbackTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final guideTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final homeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final menuRowGuide:Landroid/widget/LinearLayout;

.field private final networkTestTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final pinsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final profileGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final profileGamerscoreIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final profileName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final profilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private final scrollView:Landroid/widget/ScrollView;

.field private final searchTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final settingsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final storeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final trendingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;)V
    .locals 19
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    .prologue
    .line 107
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 108
    sget-object v16, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)V

    .line 110
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    .line 112
    const v16, 0x7f0e0785

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/support/v4/widget/DrawerLayout;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 113
    const v16, 0x7f0e0524

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ScrollView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->scrollView:Landroid/widget/ScrollView;

    .line 115
    const v16, 0x7f0e0525

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/RelativeLayout;

    .line 116
    .local v15, "profileRow":Landroid/widget/RelativeLayout;
    const v16, 0x7f0e0527

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profileName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 117
    const v16, 0x7f0e0529

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profileGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 118
    const v16, 0x7f0e0528

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profileGamerscoreIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 119
    const v16, 0x7f0e0526

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 121
    const v16, 0x7f0e054b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    .line 122
    .local v10, "menuRowSearch":Landroid/widget/LinearLayout;
    const v16, 0x7f0e052a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 123
    .local v3, "menuRowActivityFeed":Landroid/widget/LinearLayout;
    const v16, 0x7f0e052d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 124
    .local v2, "menuRowAchievements":Landroid/widget/LinearLayout;
    const v16, 0x7f0e0545

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 125
    .local v9, "menuRowPins":Landroid/widget/LinearLayout;
    const v16, 0x7f0e053f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->menuRowGuide:Landroid/widget/LinearLayout;

    .line 126
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->menuRowGuide:Landroid/widget/LinearLayout;

    move-object/from16 v16, v0

    const/16 v17, 0x8

    invoke-virtual/range {v16 .. v17}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 128
    const v16, 0x7f0e054e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 129
    .local v8, "menuRowNetworkTest":Landroid/widget/LinearLayout;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getNetworkTestingEnabled()Z

    move-result v16

    if-eqz v16, :cond_0

    const/16 v16, 0x0

    :goto_0
    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 131
    const v16, 0x7f0e0530

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 133
    .local v6, "menuRowDVR":Landroid/widget/LinearLayout;
    const v16, 0x7f0e0539

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 135
    .local v12, "menuRowStore":Landroid/widget/LinearLayout;
    const v16, 0x7f0e052c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->homeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 136
    const v16, 0x7f0e052f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->achievementsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 137
    const v16, 0x7f0e0532

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->dvrTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 138
    const v16, 0x7f0e0535

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->clubsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 139
    const v16, 0x7f0e0538

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->trendingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 140
    const v16, 0x7f0e053b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->storeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 141
    const v16, 0x7f0e0541

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->guideTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 142
    const v16, 0x7f0e0544

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->connectionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 143
    const v16, 0x7f0e0547

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->pinsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 144
    const v16, 0x7f0e054a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->settingsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 145
    const v16, 0x7f0e054d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->searchTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 146
    const v16, 0x7f0e0550

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->networkTestTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 147
    const v16, 0x7f0e0553

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->feedbackTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 149
    const v16, 0x7f0e0542

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 151
    .local v5, "menuRowConnection":Landroid/widget/LinearLayout;
    const v16, 0x7f0e0548

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    .line 154
    .local v11, "menuRowSettings":Landroid/widget/LinearLayout;
    const v16, 0x7f0e0551

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 156
    .local v7, "menuRowFeedback":Landroid/widget/LinearLayout;
    const v16, 0x7f0e0533

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 157
    .local v4, "menuRowClubs":Landroid/widget/LinearLayout;
    const v16, 0x7f0e0536

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    .line 158
    .local v13, "menuRowTrending":Landroid/widget/LinearLayout;
    const v16, 0x7f0e053c

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/LinearLayout;

    .line 165
    .local v14, "menuRowTutorial":Landroid/widget/LinearLayout;
    const/16 v16, 0x8

    move/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getSettings()Lio/reactivex/Observable;

    move-result-object v17

    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Lio/reactivex/functions/Function;

    move-result-object v18

    .line 169
    invoke-virtual/range {v17 .. v18}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v17

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/widget/LinearLayout;)Lio/reactivex/functions/Consumer;

    move-result-object v18

    .line 170
    invoke-virtual/range {v17 .. v18}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v17

    .line 168
    invoke-virtual/range {v16 .. v17}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->addDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 186
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->menuRowGuide:Landroid/widget/LinearLayout;

    move-object/from16 v16, v0

    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 273
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$16;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    move-object/from16 v17, v0

    .line 327
    invoke-interface/range {v17 .. v17}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->prefs()Lio/reactivex/Observable;

    move-result-object v17

    .line 328
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v17

    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$17;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Lio/reactivex/functions/Consumer;

    move-result-object v18

    .line 329
    invoke-virtual/range {v17 .. v18}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v17

    .line 326
    invoke-virtual/range {v16 .. v17}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    move-object/from16 v17, v0

    .line 332
    invoke-interface/range {v17 .. v17}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->isTutorialExperienceEnabledObservable()Lio/reactivex/Observable;

    move-result-object v17

    .line 333
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v17

    .line 334
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v17

    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$18;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)Lio/reactivex/functions/Consumer;

    move-result-object v18

    .line 335
    invoke-virtual/range {v17 .. v18}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v17

    .line 331
    invoke-virtual/range {v16 .. v17}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 341
    const/16 v16, 0xe

    move/from16 v0, v16

    new-array v0, v0, [Landroid/view/View;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const v18, 0x7f0e0525

    .line 342
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    const v18, 0x7f0e052a

    .line 343
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x2

    const v18, 0x7f0e052d

    .line 344
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x3

    const v18, 0x7f0e0530

    .line 345
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x4

    const v18, 0x7f0e0533

    .line 346
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x5

    const v18, 0x7f0e0536

    .line 347
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x6

    const v18, 0x7f0e0539

    .line 348
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x7

    const v18, 0x7f0e053f

    .line 349
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x8

    const v18, 0x7f0e0542

    .line 350
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x9

    const v18, 0x7f0e0545

    .line 351
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0xa

    const v18, 0x7f0e0548

    .line 352
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0xb

    const v18, 0x7f0e054b

    .line 353
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0xc

    const v18, 0x7f0e054e

    .line 354
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0xd

    const v18, 0x7f0e0551

    .line 355
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v18

    aput-object v18, v16, v17

    .line 341
    invoke-static/range {v16 .. v16}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->disableableDrawerRows:Ljava/util/List;

    .line 358
    const/16 v16, 0x1d

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const v18, 0x7f0e0527

    .line 359
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    const v18, 0x7f0e0528

    .line 360
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x2

    const v18, 0x7f0e0529

    .line 361
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x3

    const v18, 0x7f0e052b

    .line 362
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x4

    const v18, 0x7f0e052c

    .line 363
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x5

    const v18, 0x7f0e052e

    .line 364
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x6

    const v18, 0x7f0e052f

    .line 365
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x7

    const v18, 0x7f0e0531

    .line 366
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x8

    const v18, 0x7f0e0532

    .line 367
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x9

    const v18, 0x7f0e0534

    .line 368
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0xa

    const v18, 0x7f0e0535

    .line 369
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0xb

    const v18, 0x7f0e0537

    .line 370
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0xc

    const v18, 0x7f0e0538

    .line 371
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0xd

    const v18, 0x7f0e053a

    .line 372
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0xe

    const v18, 0x7f0e053b

    .line 373
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0xf

    const v18, 0x7f0e0540

    .line 374
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x10

    const v18, 0x7f0e0541

    .line 375
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x11

    const v18, 0x7f0e0543

    .line 376
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x12

    const v18, 0x7f0e0544

    .line 377
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x13

    const v18, 0x7f0e0546

    .line 378
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x14

    const v18, 0x7f0e0547

    .line 379
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x15

    const v18, 0x7f0e0549

    .line 380
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x16

    const v18, 0x7f0e054a

    .line 381
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x17

    const v18, 0x7f0e054c

    .line 382
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x18

    const v18, 0x7f0e054d

    .line 383
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x19

    const v18, 0x7f0e054f

    .line 384
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1a

    const v18, 0x7f0e0550

    .line 385
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1b

    const v18, 0x7f0e0552

    .line 386
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1c

    const v18, 0x7f0e0553

    .line 387
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    .line 358
    invoke-static/range {v16 .. v16}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->disableableDrawerLabels:Ljava/util/List;

    .line 389
    return-void

    .line 129
    .end local v4    # "menuRowClubs":Landroid/widget/LinearLayout;
    .end local v5    # "menuRowConnection":Landroid/widget/LinearLayout;
    .end local v6    # "menuRowDVR":Landroid/widget/LinearLayout;
    .end local v7    # "menuRowFeedback":Landroid/widget/LinearLayout;
    .end local v11    # "menuRowSettings":Landroid/widget/LinearLayout;
    .end local v12    # "menuRowStore":Landroid/widget/LinearLayout;
    .end local v13    # "menuRowTrending":Landroid/widget/LinearLayout;
    .end local v14    # "menuRowTutorial":Landroid/widget/LinearLayout;
    :cond_0
    const/16 v16, 0x8

    goto/16 :goto_0
.end method

.method private hideKeyboard()V
    .locals 1

    .prologue
    .line 492
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 493
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 495
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Lcom/microsoft/xbox/xle/app/SmartglassSettings;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "settings"    # Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->tutorialExperienceEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/widget/LinearLayout;Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "menuRowTutorial"    # Landroid/widget/LinearLayout;
    .param p2, "tutorialScreenAllowed"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 171
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "is tutorial drawer button allowed to be visible? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->connectionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f070379

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 181
    :goto_0
    return-void

    .line 179
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic lambda$new$10(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 246
    const-string v0, "SmartGlass Settings"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateTo(Ljava/lang/Class;)V

    .line 248
    return-void
.end method

.method static synthetic lambda$new$11(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 251
    const-string v2, "Game DVR"

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 252
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 254
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 255
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 257
    if-eqz v0, :cond_0

    .line 258
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 259
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    const-class v4, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreen;

    invoke-virtual {v2, v3, v4, v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateToPivot(Ljava/lang/Class;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 261
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$12(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 264
    const-string v0, "Console Connection"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateTo(Ljava/lang/Class;)V

    .line 266
    return-void
.end method

.method static synthetic lambda$new$13(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 269
    const-string v0, "Clubs"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateTo(Ljava/lang/Class;)V

    .line 271
    return-void
.end method

.method static synthetic lambda$new$14(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 274
    const-string v0, "Trending"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->getActiveHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Trending:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    if-ne v0, v1, :cond_0

    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateToHome()V

    .line 280
    :goto_0
    return-void

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/trending/TrendingPivotScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateTo(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method static synthetic lambda$new$15(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 283
    const-string v0, "Tutorial"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->getIsFirstRun()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->TAG:Ljava/lang/String;

    const-string v1, "Succesfully ending the first run experience as expected."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->setAppToNormalState()V

    .line 295
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;

    if-eqz v0, :cond_1

    .line 296
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 300
    :goto_0
    return-void

    .line 298
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v1, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateTo(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method static synthetic lambda$new$17(Landroid/view/View;)V
    .locals 4
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 304
    const-string v1, "Send Feedback"

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 306
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 307
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_0

    .line 308
    const-string v1, "DRAWERFEEDBACK"

    const-string v2, "Feedback pressed. Close Drawer"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->closeDrawer()V

    .line 314
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter$$Lambda$19;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 322
    return-void
.end method

.method static synthetic lambda$new$18(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->relocalizeItems()V

    return-void
.end method

.method static synthetic lambda$new$19(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "shouldShowTutorialExperience"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 336
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "shouldShowTutorialExperience? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->setDrawerState(Z)V

    .line 338
    return-void

    .line 337
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 187
    const-string v0, "ActivityFeed"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->getActiveHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->ActivityFeed:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    if-ne v0, v1, :cond_0

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateToHome()V

    .line 193
    :goto_0
    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateTo(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 196
    const-string v0, "Pins"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/PinsScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateTo(Ljava/lang/Class;)V

    .line 198
    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 201
    const-string v0, "Profile"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateToProfile()V

    .line 203
    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 206
    const-string v2, "Achievements"

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 207
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    invoke-virtual {v2}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->getActiveHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Achievements:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    if-ne v2, v3, :cond_0

    .line 208
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateToHome()V

    .line 219
    :goto_0
    return-void

    .line 210
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 211
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 212
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 216
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 217
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    const-class v4, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreen;

    invoke-virtual {v2, v3, v4, v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateToPivot(Ljava/lang/Class;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 222
    const-string v0, "OneGuide"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateTo(Ljava/lang/Class;)V

    .line 224
    return-void
.end method

.method static synthetic lambda$new$7(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 227
    const-string v1, "Search"

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 228
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 229
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_0

    .line 230
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->closeDrawer()V

    .line 232
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateToSearchScreen()V

    .line 233
    return-void
.end method

.method static synthetic lambda$new$8(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 236
    const-string v0, "Network Test"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateTo(Ljava/lang/Class;)V

    .line 238
    return-void
.end method

.method static synthetic lambda$new$9(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 241
    const-string v0, "Store"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/StorePivotScreen;

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/FeaturedLandingScreen;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateToPivot(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 243
    return-void
.end method

.method static synthetic lambda$null$16()V
    .locals 2

    .prologue
    .line 316
    :try_start_0
    const-string v0, "DRAWERFEEDBACK"

    const-string v1, "Open feedback dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->createFeedbackDialog()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    :goto_0
    return-void

    .line 318
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private relocalizeItems()V
    .locals 3

    .prologue
    .line 498
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 500
    .local v0, "resources":Landroid/content/res/Resources;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profileGamerscoreIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070d57

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 501
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->homeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070438

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 502
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->achievementsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070437

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 503
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->dvrTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070439

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 504
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->clubsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f07032c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 505
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->trendingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070446

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 507
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPurchaseBlocked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 508
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->storeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070122

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 513
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->guideTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070445

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 514
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->connectionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f07043a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 515
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->pinsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070442

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 516
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->settingsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070444

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 517
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->searchTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070443

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 518
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->networkTestTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f0705fd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 519
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->feedbackTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070e8b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 520
    return-void

    .line 510
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->storeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f07043f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private scrollToTop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 486
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->scrollView:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 489
    :cond_0
    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 0
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 455
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->scrollToTop()V

    .line 456
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 1
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 460
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->refreshProfile()V

    .line 462
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->forceUpdateViewImmediately()V

    .line 464
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->scrollToTop()V

    .line 465
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->hideKeyboard()V

    .line 466
    const-string v0, "Drawer"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 467
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 0
    .param p1, "drawerView"    # Landroid/view/View;
    .param p2, "slideOffset"    # F

    .prologue
    .line 471
    return-void
.end method

.method public onDrawerStateChanged(I)V
    .locals 0
    .param p1, "newState"    # I

    .prologue
    .line 475
    packed-switch p1, :pswitch_data_0

    .line 483
    :goto_0
    return-void

    .line 477
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->hideKeyboard()V

    goto :goto_0

    .line 475
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setDrawerState(Z)V
    .locals 7
    .param p1, "enable"    # Z

    .prologue
    .line 423
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->disableableDrawerRows:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 424
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3, p1}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    .line 428
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->disableableDrawerLabels:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 429
    .local v2, "customTypeFaceTextViewId":I
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 430
    .local v1, "customTypeFaceTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz p1, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c018a

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 431
    .local v0, "color":I
    :goto_2
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    goto :goto_1

    .line 430
    .end local v0    # "color":I
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c014d

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_2

    .line 434
    .end local v1    # "customTypeFaceTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .end local v2    # "customTypeFaceTextViewId":I
    :cond_2
    if-eqz p1, :cond_4

    .line 436
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 437
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 441
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 451
    :goto_3
    return-void

    .line 444
    :cond_4
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 445
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(Landroid/view/View;)V

    .line 449
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_3
.end method

.method public updateViewOverride()V
    .locals 6

    .prologue
    const v5, 0x7f020125

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 393
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profileName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->getGamerTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->getGamerScore()Ljava/lang/String;

    move-result-object v0

    .line 396
    .local v0, "gamerScore":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 397
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profileGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 398
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profileGamerscoreIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 405
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->getProfilePicUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v5, v5}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 407
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPremiumLiveTVEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 408
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->menuRowGuide:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 411
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPurchaseBlocked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 412
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->storeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070122

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 416
    :goto_1
    return-void

    .line 400
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profileGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 401
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profileGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 402
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->profileGamerscoreIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0

    .line 414
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->storeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f07043f

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    goto :goto_1
.end method
