.class public Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "MovieOverviewScreenAdapter.java"


# instance fields
.field private activitiesData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;"
        }
    .end annotation
.end field

.field private activitiesLayout:Landroid/widget/LinearLayout;

.field private descriptionView:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

.field private durationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected playOnXboxButton:Landroid/widget/RelativeLayout;

.field private ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private releaseDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private resolutionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private staringListTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private studioTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field protected viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;)V
    .locals 1
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 44
    const v0, 0x7f0e07b8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->screenBody:Landroid/view/View;

    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    .line 47
    const v0, 0x7f0e07b9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 48
    const v0, 0x7f0e07c2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->descriptionView:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->content:Landroid/view/View;

    .line 51
    const v0, 0x7f0e07c3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->staringListTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 52
    const v0, 0x7f0e07c4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->studioTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 53
    const v0, 0x7f0e07c5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->durationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 54
    const v0, 0x7f0e07c6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 55
    const v0, 0x7f0e07c7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 56
    const v0, 0x7f0e07c8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->releaseDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 57
    const v0, 0x7f0e07c9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->resolutionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 58
    const v0, 0x7f0e07be

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    .line 60
    const v0, 0x7f0e07ba

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->playOnXboxButton:Landroid/widget/RelativeLayout;

    .line 61
    return-void
.end method

.method private addActivityItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 7
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 145
    if-nez p1, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_0

    .line 150
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 151
    .local v3, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f030168

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 153
    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 154
    const v4, 0x7f0e0791

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 155
    .local v0, "image":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    const v4, 0x7f0e0792

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 156
    .local v1, "label":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    invoke-virtual {v2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 158
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getDisplayTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getIcon2x1Url()Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->getDefaultImage()I

    move-result v6

    invoke-virtual {v0, v4, v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 162
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private getDefaultImage()I
    .locals 1

    .prologue
    .line 168
    const v0, 0x7f0201fa

    return v0
.end method

.method static synthetic lambda$addActivityItem$2(Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .param p2, "v1"    # Landroid/view/View;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->navigateToActivityDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 73
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    if-eqz v0, :cond_0

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->navigateToActivityDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 76
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 82
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getCurrentScreenData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showProviderPickerDialogScreen(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 4

    .prologue
    .line 65
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 68
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_1

    .line 69
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 70
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 71
    .local v0, "activityItem":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 72
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 81
    .end local v0    # "activityItem":Landroid/view/View;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->playOnXboxButton:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_2

    .line 82
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->playOnXboxButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    :cond_2
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 90
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->playOnXboxButton:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 96
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->playOnXboxButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    :cond_1
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 100
    return-void
.end method

.method public updateViewOverride()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->updateLoadingIndicator(Z)V

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 109
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->descriptionView:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->setText(Ljava/lang/String;)V

    .line 112
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->staringListTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getStars()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0703d5

    .line 113
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getStars()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 112
    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 114
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->studioTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getStudio()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0703d6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getStudio()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 115
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->durationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    .line 117
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getDurationInMinutes()I

    move-result v1

    if-lez v1, :cond_2

    move v1, v2

    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0703c2

    .line 118
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getDurationInMinutes()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f07061b

    .line 119
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 115
    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 121
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0703cd

    .line 122
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 121
    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 123
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0703c8

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 124
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->releaseDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getReleaseDate()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0703ce

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    .line 125
    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getReleaseDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 124
    invoke-static {v1, v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->resolutionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 130
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesData:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getActivityData()Ljava/util/ArrayList;

    move-result-object v2

    if-eq v1, v2, :cond_7

    .line 131
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getActivityData()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesData:Ljava/util/ArrayList;

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesData:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 136
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->addActivityItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    goto :goto_6

    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :cond_0
    move v1, v3

    .line 112
    goto/16 :goto_0

    :cond_1
    move v1, v3

    .line 114
    goto/16 :goto_1

    :cond_2
    move v1, v3

    .line 117
    goto/16 :goto_2

    :cond_3
    move v1, v3

    .line 121
    goto/16 :goto_3

    :cond_4
    move v1, v3

    .line 123
    goto/16 :goto_4

    :cond_5
    move v2, v3

    .line 124
    goto :goto_5

    .line 139
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 142
    :cond_7
    return-void
.end method
