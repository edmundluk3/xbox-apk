.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$1;
.super Ljava/lang/Object;
.source "UTCActivityFeed.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackLike(ZLjava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$feedType:Ljava/lang/String;

.field final synthetic val$isLiked:Z

.field final synthetic val$targetXuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 15
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$1;->val$targetXuid:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$1;->val$feedType:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$1;->val$isLiked:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 18
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 19
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TargetXuid"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$1;->val$targetXuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 20
    const-string v1, "ActivityType"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$1;->val$feedType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 21
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$1;->val$isLiked:Z

    if-eqz v1, :cond_0

    .line 22
    const-string v1, "Activity Feed Like"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 26
    :goto_0
    return-void

    .line 24
    :cond_0
    const-string v1, "Activity Feed Un-Like"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0
.end method
