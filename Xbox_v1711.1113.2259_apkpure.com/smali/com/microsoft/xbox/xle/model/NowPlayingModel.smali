.class public Lcom/microsoft/xbox/xle/model/NowPlayingModel;
.super Lcom/microsoft/xbox/toolkit/XLEObservable;
.source "NowPlayingModel.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;
.implements Lcom/microsoft/xbox/service/model/pins/LaunchableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;,
        Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;,
        Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEObservable",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;",
        "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;"
    }
.end annotation


# static fields
.field private static final COMMA_DELIMITER:Ljava/lang/String;

.field private static final MEDIA_META_DATA_KEY_SUBTITLE:Ljava/lang/String; = "subtitle"

.field private static final MEDIA_META_DATA_KEY_TITLE:Ljava/lang/String; = "title"

.field private static final MEDIA_META_DATA_UNDEFINED:Ljava/lang/String; = "undefined"

.field private static final PIPE_DELIMITER:Ljava/lang/String;

.field private static final UPDATE_DELAY_BETWEEN_TRACKS:I = 0xbb8

.field private static companionTable:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private TAG:Ljava/lang/String;

.field private final activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

.field private currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

.field private currentNowPlayingIdentifier:Ljava/lang/String;

.field private currentTitleId:I

.field private hasConsoleFocus:Z

.field private lastPlayedTitleModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

.field private loadAppDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;

.field private loadMediaDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;

.field private nowPlayingAppCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

.field private nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

.field private nowPlayingMediaCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

.field private nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

.field private nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

.field private timer:Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

.field private useMediaCompanion:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 69
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070e6e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->COMMA_DELIMITER:Ljava/lang/String;

    .line 70
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f071084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->PIPE_DELIMITER:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->companionTable:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Z)V
    .locals 5
    .param p1, "activeTitleLocation"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .param p2, "hasConsoleFocus"    # Z

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEObservable;-><init>()V

    .line 77
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Disconnected:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 105
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 106
    new-instance v0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->timer:Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    .line 107
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->hasConsoleFocus:Z

    .line 108
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "NowPlaying(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    .line 109
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/model/NowPlayingModel;JLcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .param p1, "x1"    # J
    .param p3, "x2"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    .param p4, "x3"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .param p5, "x4"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 52
    invoke-direct/range {p0 .. p5}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->onLoadAppDetailModelCompleted(JLcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/model/NowPlayingModel;Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;)Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Lcom/microsoft/xbox/smartglass/MediaState;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/model/NowPlayingModel;Lcom/microsoft/xbox/smartglass/MediaState;)Lcom/microsoft/xbox/smartglass/MediaState;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 52
    iget v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    return v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->updateNowPlayingModels()V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->updateNowPlayingState()V

    return-void
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/model/NowPlayingModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->useMediaCompanion:Z

    return p1
.end method

.method static synthetic access$700(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCompanionTableLookupKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800()Ljava/util/Hashtable;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->companionTable:Ljava/util/Hashtable;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/model/NowPlayingModel;JLjava/lang/String;Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    .param p5, "x4"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .param p6, "x5"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 52
    invoke-direct/range {p0 .. p6}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->onLoadMediaDetailCompleted(JLjava/lang/String;Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private createBufferingMediaState(Lcom/microsoft/xbox/smartglass/MediaState;)Lcom/microsoft/xbox/smartglass/SGMediaState;
    .locals 23
    .param p1, "original"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    .line 800
    new-instance v3, Lcom/microsoft/xbox/smartglass/SGMediaState;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget v4, v2, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-object v5, v2, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-object v6, v2, Lcom/microsoft/xbox/smartglass/MediaState;->aumId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/MediaState;->playbackType:Lcom/microsoft/xbox/smartglass/MediaType;

    .line 804
    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/MediaType;->getValue()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/MediaState;->soundLevel:Lcom/microsoft/xbox/smartglass/SoundLevel;

    .line 805
    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/SoundLevel;->getValue()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/MediaState;->enabledCommands:Ljava/util/EnumSet;

    .line 806
    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/SGMediaState;->getEnabledCommandsValue(Ljava/util/EnumSet;)I

    move-result v9

    sget-object v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Playing:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 809
    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->getValue()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget v11, v2, Lcom/microsoft/xbox/smartglass/MediaState;->playbackRate:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-wide v12, v2, Lcom/microsoft/xbox/smartglass/MediaState;->mediaEnd:J

    const-wide/32 v14, 0xf4240

    sub-long/2addr v12, v14

    const-wide/16 v14, 0x1

    sub-long/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-wide v14, v2, Lcom/microsoft/xbox/smartglass/MediaState;->mediaStart:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-wide v0, v2, Lcom/microsoft/xbox/smartglass/MediaState;->mediaEnd:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-wide v0, v2, Lcom/microsoft/xbox/smartglass/MediaState;->minSeekTicks:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-wide v0, v2, Lcom/microsoft/xbox/smartglass/MediaState;->maxSeekTicks:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/MediaState;->metaData:Ljava/util/List;

    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v0, v0, [Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    move-object/from16 v22, v0

    .line 817
    move-object/from16 v0, v22

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v22

    check-cast v22, [Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    invoke-direct/range {v3 .. v22}, Lcom/microsoft/xbox/smartglass/SGMediaState;-><init>(ILjava/lang/String;Ljava/lang/String;IIIIFJJJJJ[Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;)V

    .line 820
    .local v3, "mediaState":Lcom/microsoft/xbox/smartglass/SGMediaState;
    return-object v3
.end method

.method private static getCompanionTableLookupKey(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p0, "mediaItemId"    # Ljava/lang/String;
    .param p1, "providerTitleId"    # I

    .prologue
    .line 852
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s&%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDataFromMediaMetaData(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 893
    const/4 v1, 0x0

    .line 895
    .local v1, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    if-eqz v2, :cond_1

    .line 896
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/MediaState;->metaData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    .line 897
    .local v0, "item":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    iget-object v3, v0, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;->name:Ljava/lang/String;

    invoke-static {p1, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 898
    iget-object v1, v0, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;->value:Ljava/lang/String;

    .line 904
    .end local v0    # "item":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    :cond_1
    return-object v1
.end method

.method private getIsLoadingAppDetail()Z
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadAppDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadAppDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getIsLoadingMediaDetail()Z
    .locals 1

    .prologue
    .line 545
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadMediaDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadMediaDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1229
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-nez v4, :cond_1

    .line 1250
    :cond_0
    :goto_0
    return-object v3

    .line 1232
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v0

    .line 1233
    .local v0, "im":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<**>;"
    if-eqz v0, :cond_0

    .line 1236
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    .line 1237
    .local v2, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v2, :cond_0

    .line 1240
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImages()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1243
    const/4 v3, 0x0

    .line 1244
    .local v3, "uri":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImages()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    .line 1245
    .local v1, "img":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getPurpose()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getPurpose()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1246
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getUrl()Ljava/lang/String;

    move-result-object v3

    .line 1247
    goto :goto_0
.end method

.method private isMediaInProgress(Lcom/microsoft/xbox/smartglass/MediaState;)Z
    .locals 2
    .param p1, "mediaState"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    .line 883
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/MediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    sget-object v1, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Closed:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/MediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    sget-object v1, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Stopped:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isUndefinedMetaData(Ljava/lang/String;)Z
    .locals 1
    .param p1, "metaData"    # Ljava/lang/String;

    .prologue
    .line 908
    const-string/jumbo v0, "undefined"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isUnknownMediaInProgress()Z
    .locals 1

    .prologue
    .line 888
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isMediaInProgress(Lcom/microsoft/xbox/smartglass/MediaState;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    .line 889
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 888
    :goto_0
    return v0

    .line 889
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isValidId(Ljava/lang/String;)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 848
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isValidTitleIdForDetail(J)Z
    .locals 2
    .param p0, "titleId"    # J

    .prologue
    .line 844
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    const-wide v0, 0xfffe07d1L

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadAppDetailModel(ZI)V
    .locals 1
    .param p1, "forceLoad"    # Z
    .param p2, "titleId"    # I

    .prologue
    .line 604
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadAppDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;

    if-eqz v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadAppDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;->cancel()V

    .line 608
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;

    invoke-direct {v0, p0, p2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;-><init>(Lcom/microsoft/xbox/xle/model/NowPlayingModel;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadAppDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;

    .line 609
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadAppDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;->load(Z)V

    .line 610
    return-void
.end method

.method private loadMediaDetailModel(ZILjava/lang/String;)V
    .locals 6
    .param p1, "forceLoad"    # Z
    .param p2, "titleId"    # I
    .param p3, "partnerMediaId"    # Ljava/lang/String;

    .prologue
    .line 627
    const-string v0, "mtc"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "loading media detail, titleId=%d partnerMediaId=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadMediaDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;

    if-eqz v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadMediaDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->cancel()V

    .line 633
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;

    invoke-direct {v0, p0, p2, p3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;-><init>(Lcom/microsoft/xbox/xle/model/NowPlayingModel;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadMediaDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;

    .line 634
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadMediaDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->load(Z)V

    .line 635
    return-void
.end method

.method private static mediaHasChanged(Lcom/microsoft/xbox/smartglass/MediaState;Lcom/microsoft/xbox/smartglass/MediaState;)Z
    .locals 4
    .param p0, "oldMediaState"    # Lcom/microsoft/xbox/smartglass/MediaState;
    .param p1, "newMediaState"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 825
    if-ne p0, p1, :cond_1

    .line 832
    :cond_0
    :goto_0
    return v2

    .line 829
    :cond_1
    if-nez p0, :cond_2

    move-object v1, v0

    .line 830
    .local v1, "oldMediaAssetId":Ljava/lang/String;
    :goto_1
    if-nez p1, :cond_3

    .line 832
    .local v0, "newMediaAssetId":Ljava/lang/String;
    :goto_2
    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 829
    .end local v0    # "newMediaAssetId":Ljava/lang/String;
    .end local v1    # "oldMediaAssetId":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    goto :goto_1

    .line 830
    .restart local v1    # "oldMediaAssetId":Ljava/lang/String;
    :cond_3
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    goto :goto_2
.end method

.method private static mediaIsValid(Lcom/microsoft/xbox/smartglass/MediaState;)Z
    .locals 2
    .param p0, "mediaState"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    const/4 v0, 0x0

    .line 836
    if-nez p0, :cond_1

    .line 839
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private onLoadAppDetailModelCompleted(JLcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 5
    .param p1, "titleId"    # J
    .param p3, "appModel"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    .param p4, "companion"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .param p5, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 614
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "appdetail load completed with status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    iget v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    int-to-long v0, v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 616
    iput-object p3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 617
    iput-object p4, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 618
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->updateNowPlayingState()V

    .line 619
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "app detail model refreshed, title id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 624
    :goto_0
    return-void

    .line 622
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "title already changed, ignore this detail"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onLoadMediaDetailCompleted(JLjava/lang/String;Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "titleId"    # J
    .param p3, "partnerMediaId"    # Ljava/lang/String;
    .param p4, "mediaModel"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    .param p5, "companion"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .param p6, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 640
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 641
    iput-object p4, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 642
    iput-object p5, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 643
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->updateNowPlayingState()V

    .line 644
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 646
    :cond_0
    return-void
.end method

.method private resetAllNowPlayingData()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 857
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadAppDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;

    if-eqz v0, :cond_0

    .line 859
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadAppDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;->cancel()V

    .line 860
    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadAppDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadAppDetailTask;

    .line 863
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadMediaDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;

    if-eqz v0, :cond_1

    .line 864
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadMediaDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->cancel()V

    .line 865
    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadMediaDetailTask:Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;

    .line 868
    :cond_1
    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 869
    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 870
    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 871
    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 872
    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    .line 873
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    .line 877
    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentNowPlayingIdentifier:Ljava/lang/String;

    .line 878
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->timer:Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->stop()V

    .line 879
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->timer:Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->setOnPositionUpdatedRunnable(Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;)V

    .line 880
    return-void
.end method

.method private updateNowPlayingModels()V
    .locals 14

    .prologue
    .line 651
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 652
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->timer:Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->stop()V

    .line 794
    :cond_0
    :goto_0
    return-void

    .line 657
    :cond_1
    iget v11, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    .line 658
    .local v11, "newTitleId":I
    iget-object v10, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    .line 661
    .local v10, "newMediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getTitleId(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)I

    move-result v1

    iget v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    if-eq v1, v2, :cond_3

    .line 662
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "TitleId is updated. old=%d, new=%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v6

    iget-object v12, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v6, v12}, Lcom/microsoft/xbox/service/model/SessionModel;->getTitleId(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getTitleId(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)I

    move-result v11

    .line 665
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitleId()J

    move-result-wide v2

    int-to-long v4, v11

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 666
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string v2, "Now playing app detail model is reset."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 670
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    if-eqz v1, :cond_2

    .line 671
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string v2, "Now playing app companion is reset."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 678
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v2, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-nez v1, :cond_3

    int-to-long v2, v11

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isValidTitleIdForDetail(J)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 679
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string v2, "Running valid title. New now playing app detail loaded."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    int-to-long v2, v11

    const/4 v1, 0x0

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getModel(JLjava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 681
    const/4 v1, 0x0

    invoke-direct {p0, v1, v11}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadAppDetailModel(ZI)V

    .line 685
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1, v11}, Lcom/microsoft/xbox/service/model/SessionModel;->getMediaState(I)Lcom/microsoft/xbox/smartglass/MediaState;

    move-result-object v9

    .line 686
    .local v9, "ms":Lcom/microsoft/xbox/smartglass/MediaState;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    if-ne v1, v9, :cond_4

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    invoke-static {v1, v9}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->mediaHasChanged(Lcom/microsoft/xbox/smartglass/MediaState;Lcom/microsoft/xbox/smartglass/MediaState;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 687
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Media state is updated. old=%s, new=%s"

    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    if-nez v1, :cond_8

    const-string v1, "null"

    :goto_1
    aput-object v1, v5, v6

    const/4 v6, 0x1

    if-nez v9, :cond_9

    const-string v1, "null"

    :goto_2
    aput-object v1, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    move-object v10, v9

    .line 694
    const/4 v8, 0x0

    .line 696
    .local v8, "doBetweenTracks":Z
    if-eqz v8, :cond_5

    iget v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    if-ne v11, v1, :cond_5

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    sget-object v2, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingMusic:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    if-ne v1, v2, :cond_5

    if-eqz v10, :cond_5

    iget-object v1, v10, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    .line 697
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 698
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string v2, "Null asset id detected while playing music. Delay posting new media state"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    move-object v0, v10

    .line 700
    .local v0, "cachedMediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    move v7, v11

    .line 701
    .local v7, "cachedTitleId":I
    new-instance v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;

    invoke-direct {v1, p0, v7, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;-><init>(Lcom/microsoft/xbox/xle/model/NowPlayingModel;ILcom/microsoft/xbox/smartglass/MediaState;)V

    const-wide/16 v2, 0xbb8

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 742
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->createBufferingMediaState(Lcom/microsoft/xbox/smartglass/MediaState;)Lcom/microsoft/xbox/smartglass/SGMediaState;

    .line 746
    .end local v0    # "cachedMediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    .end local v7    # "cachedTitleId":I
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v2, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v1, v2, :cond_a

    iget v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    if-ne v11, v1, :cond_a

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    sget-object v2, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingMusic:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    if-ne v1, v2, :cond_a

    .line 747
    invoke-static {v10}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->mediaIsValid(Lcom/microsoft/xbox/smartglass/MediaState;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    invoke-static {v1, v10}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->mediaHasChanged(Lcom/microsoft/xbox/smartglass/MediaState;Lcom/microsoft/xbox/smartglass/MediaState;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 748
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string v2, "New asset id received for music. Delay updating media model"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    const/4 v1, 0x0

    iget-object v2, v10, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    invoke-direct {p0, v1, v11, v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadMediaDetailModel(ZILjava/lang/String;)V

    .line 790
    .end local v8    # "doBetweenTracks":Z
    :cond_6
    :goto_3
    iget v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    if-ne v11, v1, :cond_7

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    if-eq v10, v1, :cond_0

    .line 791
    :cond_7
    iput v11, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    .line 792
    iput-object v10, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    goto/16 :goto_0

    .line 687
    :cond_8
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    goto :goto_1

    :cond_9
    iget-object v1, v9, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    goto :goto_2

    .line 757
    .restart local v8    # "doBetweenTracks":Z
    :cond_a
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 758
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitleId()J

    move-result-wide v2

    int-to-long v4, v11

    cmp-long v1, v2, v4

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v2

    if-nez v10, :cond_e

    const/4 v1, 0x0

    :goto_4
    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 760
    :cond_b
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string v2, "Now playing media detail model is reset."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 764
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    if-eqz v1, :cond_c

    .line 765
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string v2, "Now playing media companion is reset."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 770
    :cond_c
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->timer:Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->stop()V

    .line 772
    if-eqz v10, :cond_6

    .line 773
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v2, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v1, v2, :cond_d

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-nez v1, :cond_d

    int-to-long v2, v11

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isValidTitleIdForDetail(J)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, v10, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isValidId(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 774
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string v2, "Running valid title and media. New now playing media detail loaded."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    int-to-long v2, v11

    iget-object v1, v10, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getModel(JLjava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 776
    const/4 v1, 0x0

    iget-object v2, v10, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    invoke-direct {p0, v1, v11, v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->loadMediaDetailModel(ZILjava/lang/String;)V

    .line 779
    :cond_d
    invoke-direct {p0, v10}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isMediaInProgress(Lcom/microsoft/xbox/smartglass/MediaState;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 780
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->timer:Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    iget-wide v2, v10, Lcom/microsoft/xbox/smartglass/MediaState;->position:J

    iget-wide v4, v10, Lcom/microsoft/xbox/smartglass/MediaState;->mediaEnd:J

    iget-wide v12, v10, Lcom/microsoft/xbox/smartglass/MediaState;->mediaStart:J

    sub-long/2addr v4, v12

    const-wide/16 v12, 0x1

    add-long/2addr v4, v12

    iget-object v6, v10, Lcom/microsoft/xbox/smartglass/MediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    sget-object v12, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Playing:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    if-ne v6, v12, :cond_f

    iget v6, v10, Lcom/microsoft/xbox/smartglass/MediaState;->playbackRate:F

    :goto_5
    invoke-virtual/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->update(JJF)V

    .line 782
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->timer:Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->start()V

    goto/16 :goto_3

    .line 758
    :cond_e
    iget-object v1, v10, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    goto :goto_4

    .line 780
    :cond_f
    const/4 v6, 0x0

    goto :goto_5

    .line 784
    :cond_10
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    goto/16 :goto_3
.end method

.method private updateNowPlayingState()V
    .locals 6

    .prologue
    .line 549
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v2

    .line 550
    .local v2, "sessionState":I
    const/4 v0, 0x0

    .line 552
    .local v0, "newNowPlayingIdentifier":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Disconnected:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 553
    .local v1, "newState":Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
    packed-switch v2, :pswitch_data_0

    .line 584
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/SessionModel;->getIsConnecting()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 585
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Connecting:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 590
    :cond_0
    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentNowPlayingIdentifier:Ljava/lang/String;

    .line 592
    sget-object v3, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Disconnected:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    if-ne v1, v3, :cond_1

    .line 593
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->resetAllNowPlayingData()V

    .line 596
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    if-eq v3, v1, :cond_2

    .line 597
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Old now playing state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 599
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "New now playing state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    :cond_2
    return-void

    .line 555
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Connecting:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 556
    goto :goto_0

    .line 558
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isMediaInProgress(Lcom/microsoft/xbox/smartglass/MediaState;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 559
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    .line 561
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaType()I

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/ActivityUtil;->isValidMediaTypeForActivity(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 562
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingMusic:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 565
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v3

    instance-of v3, v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    if-eqz v3, :cond_0

    .line 566
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getAlbumCanonicalId()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 569
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingVideo:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    goto/16 :goto_0

    .line 571
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 572
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaType()I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->getValue()I

    move-result v4

    if-ne v3, v4, :cond_5

    .line 573
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingApp:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 577
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 575
    :cond_5
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingGame:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    goto :goto_1

    .line 579
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "titleId failed to load, treated as app "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingApp:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 582
    goto/16 :goto_0

    .line 553
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public canUnsnap()Z
    .locals 2

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Snapped:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    return-object v0
.end method

.method public getAppBarNowPlayingImageUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 164
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 169
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getImageUri()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 167
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 164
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getAppCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAppIconSquareArtUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1221
    const-string v0, "AppIconSquareArt"

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    goto :goto_0
.end method

.method public getBoxArtBackgroundColor()I
    .locals 2

    .prologue
    .line 1195
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_1

    .line 1196
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 1197
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 1199
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :goto_0
    if-eqz v0, :cond_1

    .line 1200
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBoxArtBackgroundColor()I

    move-result v1

    .line 1204
    .end local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :goto_1
    return v1

    .line 1197
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 1198
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    goto :goto_0

    .line 1204
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getBoxArtUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1225
    const-string v0, "BoxArt"

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCanonicalId()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 336
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 347
    :cond_0
    :goto_0
    return-object v0

    .line 339
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 342
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 345
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->lastPlayedTitleModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->lastPlayedTitleModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 336
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public getContentType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1109
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getDetailModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    move-result-object v0

    .line 1110
    .local v0, "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaType()I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getCurrentDetailModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .locals 2

    .prologue
    .line 205
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "returning AppModel"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    :goto_0
    return-object v0

    .line 208
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "returning MediaModel"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    goto :goto_0

    .line 212
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "returning last played title model"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->lastPlayedTitleModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    goto :goto_0

    .line 205
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getCurrentMediaState()Lcom/microsoft/xbox/smartglass/MediaState;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    return-object v0
.end method

.method public getCurrentNowPlayingIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentNowPlayingIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentTitleId()I
    .locals 1

    .prologue
    .line 399
    iget v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    return v0
.end method

.method public getDefaultAspectRatio()[I
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 189
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 199
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    :goto_0
    return-object v0

    .line 191
    :pswitch_0
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    goto :goto_0

    .line 193
    :pswitch_1
    new-array v0, v2, [I

    fill-array-data v0, :array_2

    goto :goto_0

    .line 195
    :pswitch_2
    new-array v0, v2, [I

    fill-array-data v0, :array_3

    goto :goto_0

    .line 197
    :pswitch_3
    new-array v0, v2, [I

    fill-array-data v0, :array_4

    goto :goto_0

    .line 189
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 199
    :array_0
    .array-data 4
        0x4
        0x3
    .end array-data

    .line 191
    :array_1
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 193
    :array_2
    .array-data 4
        0x3
        0x2
    .end array-data

    .line 195
    :array_3
    .array-data 4
        0x4
        0x3
    .end array-data

    .line 197
    :array_4
    .array-data 4
        0x1
        0x1
    .end array-data
.end method

.method public getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->useMediaCompanion:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isMediaInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppCompanion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    goto :goto_0
.end method

.method public getDefaultResourceId()I
    .locals 2

    .prologue
    .line 174
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 184
    const v0, 0x7f0201fa

    :goto_0
    return v0

    .line 176
    :pswitch_0
    const v0, 0x7f020063

    goto :goto_0

    .line 178
    :pswitch_1
    const v0, 0x7f020123

    goto :goto_0

    .line 180
    :pswitch_2
    const v0, 0x7f020183

    goto :goto_0

    .line 182
    :pswitch_3
    const v0, 0x7f020189

    goto :goto_0

    .line 174
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getDetailModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    .locals 2

    .prologue
    .line 1162
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1170
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1165
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    goto :goto_0

    .line 1168
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    goto :goto_0

    .line 1162
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getHasConsoleFocus()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->hasConsoleFocus:Z

    return v0
.end method

.method public getHeader()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 226
    const/4 v0, 0x0

    .line 227
    .local v0, "result":Ljava/lang/String;
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getIsLoadingAppDetail()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 228
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07013a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 256
    :cond_0
    :goto_0
    return-object v0

    .line 230
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 252
    :cond_2
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 253
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0707c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 232
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isUnknownMediaInProgress()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 233
    const-string/jumbo v2, "title"

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getDataFromMediaMetaData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 234
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isUndefinedMetaData(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 240
    :cond_3
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v2, :cond_4

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 241
    :goto_2
    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 240
    goto :goto_2

    .line 244
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v2, :cond_5

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 245
    :goto_3
    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 244
    goto :goto_3

    .line 248
    :pswitch_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->lastPlayedTitleModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v2, :cond_6

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->lastPlayedTitleModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    :goto_4
    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_4

    .line 230
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public getImageUri()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 135
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 146
    :cond_0
    :goto_0
    return-object v0

    .line 138
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 141
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 144
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->lastPlayedTitleModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->lastPlayedTitleModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 135
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1115
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 1116
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getIsLoading()Z
    .locals 1

    .prologue
    .line 391
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getIsLoadingAppDetail()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getIsLoadingMediaDetail()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsMusicPlayList()Z
    .locals 1

    .prologue
    .line 1136
    const/4 v0, 0x0

    return v0
.end method

.method public getIsTVChannel()Z
    .locals 1

    .prologue
    .line 1146
    const/4 v0, 0x0

    return v0
.end method

.method public getIsWebLink()Z
    .locals 1

    .prologue
    .line 1141
    const/4 v0, 0x0

    return v0
.end method

.method public getItemId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1103
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 1104
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getLaunchUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1176
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaDurationInSeconds()J
    .locals 2

    .prologue
    .line 448
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->timer:Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->getDurationInSeconds()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 2

    .prologue
    .line 1150
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getDetailModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    move-result-object v0

    .line 1151
    .local v0, "dm":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    goto :goto_0
.end method

.method public getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 376
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 386
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 379
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    goto :goto_0

    .line 382
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    goto :goto_0

    .line 384
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->lastPlayedTitleModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->lastPlayedTitleModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    goto :goto_0

    .line 376
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getNowPlayingState()Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    return-object v0
.end method

.method public getPartnerMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v0, :cond_0

    .line 1215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v0

    .line 1217
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPosterImageUri()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 151
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 159
    :cond_0
    :goto_0
    return-object v0

    .line 154
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getPosterImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 157
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .locals 4

    .prologue
    .line 1076
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1092
    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    .line 1079
    :pswitch_0
    const/4 v0, 0x0

    .line 1080
    .local v0, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    .line 1081
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .end local v0    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;-><init>()V

    .line 1082
    .restart local v0    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitleId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setTitleId(J)V

    .line 1083
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setCanonicalId(Ljava/lang/String;)V

    .line 1084
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setName(Ljava/lang/String;)V

    .line 1085
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->ProviderMediaId:Ljava/lang/String;

    goto :goto_0

    .line 1076
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getProviderMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1070
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 357
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 371
    :cond_0
    :goto_0
    return-object v0

    .line 361
    :pswitch_0
    iget v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    int-to-long v2, v1

    const-wide/32 v4, 0x5848085b

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 362
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0710a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 364
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 367
    :pswitch_1
    iget v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    const v2, 0x18ffc9f4

    if-ne v1, v2, :cond_0

    .line 368
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0710a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 357
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getProviderTitleId()J
    .locals 2

    .prologue
    .line 1098
    iget v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getProviderTitleIdString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1065
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitleId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getShouldShowBackgroundColor()Z
    .locals 3

    .prologue
    .line 1181
    const/4 v1, 0x0

    .line 1182
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v2, :cond_0

    .line 1183
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 1184
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    iget-object v0, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 1186
    .local v0, "mediaType":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_0

    .line 1187
    const-string v2, "DApp"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 1190
    .end local v0    # "mediaType":Ljava/lang/String;
    :cond_0
    return v1

    .line 1184
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 1185
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    iget-object v0, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSubHeader()Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v8, 0x0

    .line 283
    sget-object v9, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v10, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    move-object v5, v8

    .line 329
    :cond_0
    :goto_0
    return-object v5

    .line 285
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isUnknownMediaInProgress()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 286
    const-string/jumbo v9, "subtitle"

    invoke-direct {p0, v9}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getDataFromMediaMetaData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 287
    .local v5, "result":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isUndefinedMetaData(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .end local v5    # "result":Ljava/lang/String;
    :cond_1
    move-object v5, v8

    .line 291
    goto :goto_0

    .line 293
    :pswitch_1
    iget-object v9, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v9, :cond_2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v9

    instance-of v9, v9, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    if-eqz v9, :cond_2

    .line 294
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    .line 295
    .local v2, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getPublisher()Ljava/lang/String;

    move-result-object v3

    .line 296
    .local v3, "publisher":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v9

    invoke-static {v9, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getYearString(Ljava/util/Date;Ljava/text/SimpleDateFormat;)Ljava/lang/String;

    move-result-object v4

    .line 297
    .local v4, "releaseYear":Ljava/lang/String;
    sget-object v9, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->COMMA_DELIMITER:Ljava/lang/String;

    invoke-static {v3, v4, v8, v9, v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .end local v2    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    .end local v3    # "publisher":Ljava/lang/String;
    .end local v4    # "releaseYear":Ljava/lang/String;
    :cond_2
    move-object v5, v8

    .line 299
    goto :goto_0

    .line 301
    :pswitch_2
    iget-object v9, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v9, :cond_5

    .line 302
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v9

    instance-of v9, v9, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    if-eqz v9, :cond_3

    .line 303
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    .line 304
    .local v2, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getMediaType()I

    move-result v9

    const/16 v10, 0x3ea

    if-eq v9, v10, :cond_5

    .line 305
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070612

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getSeasonNumber()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 306
    .local v6, "season":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f07060e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getEpisodeNumber()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 307
    .local v1, "episode":Ljava/lang/String;
    sget-object v9, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->COMMA_DELIMITER:Ljava/lang/String;

    invoke-static {v6, v1, v8, v9, v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 308
    .local v7, "temp":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getDuration()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->PIPE_DELIMITER:Ljava/lang/String;

    invoke-static {v7, v9, v8, v10, v13}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 310
    .end local v1    # "episode":Ljava/lang/String;
    .end local v2    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
    .end local v6    # "season":Ljava/lang/String;
    .end local v7    # "temp":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v9

    instance-of v9, v9, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    if-eqz v9, :cond_5

    .line 311
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    .line 312
    .local v2, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v8

    if-eqz v8, :cond_4

    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string/jumbo v9, "yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 313
    .restart local v4    # "releaseYear":Ljava/lang/String;
    :goto_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070625

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v10, v13, [Ljava/lang/Object;

    .line 314
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->getDuration()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->HHMMSSStringToMinutes(Ljava/lang/String;)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    .line 313
    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 315
    .local v0, "duration":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->getParentalRating()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->PIPE_DELIMITER:Ljava/lang/String;

    invoke-static {v4, v8, v0, v9, v13}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 312
    .end local v0    # "duration":Ljava/lang/String;
    .end local v4    # "releaseYear":Ljava/lang/String;
    :cond_4
    const-string v4, ""

    goto :goto_1

    .end local v2    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;
    :cond_5
    move-object v5, v8

    .line 318
    goto/16 :goto_0

    .line 320
    :pswitch_3
    iget-object v9, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v9, :cond_6

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v9

    instance-of v9, v9, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    if-eqz v9, :cond_6

    .line 321
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .line 322
    .local v2, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getAlbumName()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .end local v2    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    :cond_6
    move-object v5, v8

    .line 324
    goto/16 :goto_0

    :pswitch_4
    move-object v5, v8

    .line 327
    goto/16 :goto_0

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1059
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 1060
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public hasCompanion()Z
    .locals 1

    .prologue
    .line 1131
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDetails()Z
    .locals 1

    .prologue
    .line 1121
    const/4 v0, 0x1

    return v0
.end method

.method public hasProvider()Z
    .locals 1

    .prologue
    .line 1126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAppNowPlaying(J)Z
    .locals 3
    .param p1, "titleId"    # J

    .prologue
    .line 435
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getTitleId(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnectedToConsole()Z
    .locals 2

    .prologue
    .line 408
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDLC()Z
    .locals 1

    .prologue
    .line 1210
    const/4 v0, 0x0

    return v0
.end method

.method public isMediaInProgress()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 412
    sget-object v2, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 419
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 416
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    iget v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/SessionModel;->getMediaState(I)Lcom/microsoft/xbox/smartglass/MediaState;

    move-result-object v0

    .line 417
    .local v0, "mediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isMediaInProgress(Lcom/microsoft/xbox/smartglass/MediaState;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 412
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public isMediaItemNowPlaying(Ljava/lang/String;)Z
    .locals 1
    .param p1, "canonicalId"    # Ljava/lang/String;

    .prologue
    .line 429
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentNowPlayingIdentifier:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNullCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isMediaPaused()Z
    .locals 3

    .prologue
    .line 424
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getMediaState(I)Lcom/microsoft/xbox/smartglass/MediaState;

    move-result-object v0

    .line 425
    .local v0, "mediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/smartglass/MediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    sget-object v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Paused:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 4
    .param p1, "refreshSessionData"    # Z

    .prologue
    .line 467
    if-eqz p1, :cond_0

    .line 468
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 470
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 481
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 482
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->resetAllNowPlayingData()V

    .line 483
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Disconnected:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 484
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 473
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 477
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Connecting:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 478
    return-void
.end method

.method public setHasConsoleFocus(Z)V
    .locals 0
    .param p1, "hasConsoleFocus"    # Z

    .prologue
    .line 124
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->hasConsoleFocus:Z

    .line 125
    return-void
.end method

.method public shouldShowController()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 453
    const-string/jumbo v0, "shouldShowController: should not be here!"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 454
    const/4 v0, 0x0

    return v0
.end method

.method public stopMediaTimer()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->timer:Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->timer:Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->stop()V

    .line 131
    :cond_0
    return-void
.end method

.method public titleDetailsFailedToLoad()Z
    .locals 3

    .prologue
    .line 260
    const/4 v0, 0x0

    .line 262
    .local v0, "result":Z
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingState:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 278
    :cond_0
    :goto_0
    return v0

    .line 265
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getIsLoadingAppDetail()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingAppModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 266
    const/4 v0, 0x1

    goto :goto_0

    .line 272
    :pswitch_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getIsLoadingMediaDetail()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->nowPlayingMediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 273
    const/4 v0, 0x1

    goto :goto_0

    .line 262
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public unSnap()V
    .locals 1

    .prologue
    .line 537
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->unSnap()V

    .line 538
    return-void
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 488
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v3

    .line 489
    .local v3, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    .line 490
    .local v1, "isFinal":Z
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v0

    .line 495
    .local v0, "exception":Lcom/microsoft/xbox/toolkit/XLEException;
    if-eqz v1, :cond_1

    .line 496
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Received update: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/UpdateType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    sget-object v4, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 517
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ignores this update type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/UpdateType;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    :cond_0
    :goto_0
    return-void

    .line 500
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getSender()Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/microsoft/xbox/service/model/SessionModel;

    if-eqz v4, :cond_0

    .line 501
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/UpdateData;->getExtra()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/SessionModel;->getNowPlayingLocation(Landroid/os/Bundle;)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v2

    .line 502
    .local v2, "location":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v2, v4, :cond_0

    .line 503
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->updateNowPlayingModels()V

    .line 504
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->updateNowPlayingState()V

    .line 505
    new-instance v4, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v5, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v5, v6, v1}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v4, v5, p0, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0

    .line 512
    .end local v2    # "location":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    :pswitch_1
    iget v4, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->currentTitleId:I

    const v5, 0x3d8b930f

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v5, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v4, v5, :cond_0

    .line 513
    new-instance v4, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v5, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->InternetExplorerData:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v5, v6, v1}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v4, v5, p0, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0

    .line 522
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 524
    :pswitch_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->updateNowPlayingState()V

    .line 525
    new-instance v4, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v5, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v5, v6, v1}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v4, v5, p0, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0

    .line 497
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 522
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method
