.class public final enum Lcom/microsoft/xbox/domain/auth/AuthState;
.super Ljava/lang/Enum;
.source "AuthState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/domain/auth/AuthState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/domain/auth/AuthState;

.field public static final enum SignInError:Lcom/microsoft/xbox/domain/auth/AuthState;

.field public static final enum SignInInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

.field public static final enum SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

.field public static final enum SignInUiRequired:Lcom/microsoft/xbox/domain/auth/AuthState;

.field public static final enum SignInUserCancelled:Lcom/microsoft/xbox/domain/auth/AuthState;

.field public static final enum SignOutError:Lcom/microsoft/xbox/domain/auth/AuthState;

.field public static final enum SignOutInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

.field public static final enum SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

.field public static final enum Unknown:Lcom/microsoft/xbox/domain/auth/AuthState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/domain/auth/AuthState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->Unknown:Lcom/microsoft/xbox/domain/auth/AuthState;

    .line 6
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    const-string v1, "SignInInProgress"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/domain/auth/AuthState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

    .line 7
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    const-string v1, "SignInSuccess"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/domain/auth/AuthState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    .line 8
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    const-string v1, "SignInUserCancelled"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/domain/auth/AuthState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInUserCancelled:Lcom/microsoft/xbox/domain/auth/AuthState;

    .line 9
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    const-string v1, "SignInUiRequired"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/domain/auth/AuthState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInUiRequired:Lcom/microsoft/xbox/domain/auth/AuthState;

    .line 10
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    const-string v1, "SignInError"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/auth/AuthState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInError:Lcom/microsoft/xbox/domain/auth/AuthState;

    .line 12
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    const-string v1, "SignOutInProgress"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/auth/AuthState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

    .line 13
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    const-string v1, "SignOutSuccess"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/auth/AuthState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    const-string v1, "SignOutError"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/auth/AuthState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutError:Lcom/microsoft/xbox/domain/auth/AuthState;

    .line 3
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/microsoft/xbox/domain/auth/AuthState;

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->Unknown:Lcom/microsoft/xbox/domain/auth/AuthState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInUserCancelled:Lcom/microsoft/xbox/domain/auth/AuthState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInUiRequired:Lcom/microsoft/xbox/domain/auth/AuthState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInError:Lcom/microsoft/xbox/domain/auth/AuthState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutError:Lcom/microsoft/xbox/domain/auth/AuthState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->$VALUES:[Lcom/microsoft/xbox/domain/auth/AuthState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/auth/AuthState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/domain/auth/AuthState;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->$VALUES:[Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/domain/auth/AuthState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/domain/auth/AuthState;

    return-object v0
.end method


# virtual methods
.method public isTerminal()Z
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInUserCancelled:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInError:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutError:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInUiRequired:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->Unknown:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
