.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;

    .line 22
    const v0, 0x7f0e08fe

    const-string v1, "field \'leaderboardSummaryView\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;->leaderboardSummaryView:Landroid/view/View;

    .line 23
    const v0, 0x7f0e08ff

    const-string v1, "field \'leaderboardSpinner\'"

    const-class v2, Landroid/widget/Spinner;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;->leaderboardSpinner:Landroid/widget/Spinner;

    .line 24
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;

    .line 30
    .local v0, "target":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 31
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;

    .line 33
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;->leaderboardSummaryView:Landroid/view/View;

    .line 34
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;->leaderboardSpinner:Landroid/widget/Spinner;

    .line 35
    return-void
.end method
