.class public Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;
.super Ljava/lang/Object;
.source "ProfileColorServiceModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule$Names;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideMediaHubEndpoint()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "PROFILE_COLOR_SERVICE_ENDPOINT"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 29
    const-string v0, "https://dlassets-ssl.xboxlive.com"

    return-object v0
.end method

.method provideProfileColorRetrofit(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/converter/gson/GsonConverterFactory;)Lretrofit2/Retrofit;
    .locals 2
    .param p1, "baseUrl"    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "PROFILE_COLOR_SERVICE_ENDPOINT"
        .end annotation
    .end param
    .param p2, "client"    # Lokhttp3/OkHttpClient;
        .annotation runtime Ljavax/inject/Named;
            value = "DEFAULT_OK_HTTP"
        .end annotation
    .end param
    .param p3, "gsonConverterFactory"    # Lretrofit2/converter/gson/GsonConverterFactory;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "PROFILE_COLOR_SERVICE_RETROFIT"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lretrofit2/Retrofit$Builder;

    invoke-direct {v0}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 40
    invoke-virtual {v0, p1}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 41
    invoke-virtual {v0, p3}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 42
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 43
    invoke-virtual {v0, p2}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v0

    .line 39
    return-object v0
.end method

.method provideProfileColorService(Lretrofit2/Retrofit;)Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;
    .locals 1
    .param p1, "retrofit"    # Lretrofit2/Retrofit;
        .annotation runtime Ljavax/inject/Named;
            value = "PROFILE_COLOR_SERVICE_RETROFIT"
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 50
    const-class v0, Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;

    invoke-virtual {p1, v0}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;

    return-object v0
.end method
