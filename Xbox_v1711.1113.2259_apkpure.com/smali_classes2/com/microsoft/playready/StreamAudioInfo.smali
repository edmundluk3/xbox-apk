.class public final Lcom/microsoft/playready/StreamAudioInfo;
.super Ljava/lang/Object;
.source "StreamAudioInfo.java"

# interfaces
.implements Lcom/microsoft/playready/AudioInfo;


# instance fields
.field private mNativeClass:Lcom/microsoft/playready/Native_Class10;

.field private mStreamIndex:I


# direct methods
.method constructor <init>(Lcom/microsoft/playready/Native_Class10;I)V
    .locals 0
    .param p1, "nativeClass"    # Lcom/microsoft/playready/Native_Class10;
    .param p2, "streamIndex"    # I

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/microsoft/playready/StreamAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    .line 45
    iput p2, p0, Lcom/microsoft/playready/StreamAudioInfo;->mStreamIndex:I

    .line 46
    return-void
.end method


# virtual methods
.method public getAudioTag()I
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/playready/StreamAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamAudioInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_42(I)I

    move-result v0

    return v0
.end method

.method public getBitPerSample()I
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/playready/StreamAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamAudioInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_40(I)I

    move-result v0

    return v0
.end method

.method public getChannels()I
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/playready/StreamAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamAudioInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_39(I)I

    move-result v0

    return v0
.end method

.method public getPacketSize()I
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/playready/StreamAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamAudioInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_41(I)I

    move-result v0

    return v0
.end method

.method public getProfile()I
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/playready/StreamAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamAudioInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_43(I)I

    move-result v0

    return v0
.end method

.method public getSamplingRate()I
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/playready/StreamAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamAudioInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_38(I)I

    move-result v0

    return v0
.end method
