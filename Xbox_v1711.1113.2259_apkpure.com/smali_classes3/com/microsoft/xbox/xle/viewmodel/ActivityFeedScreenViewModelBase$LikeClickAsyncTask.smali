.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedScreenViewModelBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LikeClickAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field private final newLikeState:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

.field private final xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "xuid"    # Ljava/lang/String;

    .prologue
    .line 1546
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1547
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1548
    iget-object v0, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1549
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1551
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 1552
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->newLikeState:Z

    .line 1553
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->xuid:Ljava/lang/String;

    .line 1554
    return-void

    .line 1552
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1558
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1559
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 1574
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->newLikeState:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->xuid:Ljava/lang/String;

    .line 1575
    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->likeActivityFeedItem(ZLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 1574
    :goto_0
    return-object v0

    .line 1575
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1541
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1569
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1541
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 1564
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1565
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1585
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->newLikeState:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 1586
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1541
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1581
    return-void
.end method
