.class public Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "TitleLeaderboardScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;
    }
.end annotation


# instance fields
.field private isLoadingLeaderboard:Z

.field private leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

.field private leaderBoardName:Ljava/lang/String;

.field private loadLeaderboardTask:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;

.field private model:Lcom/microsoft/xbox/service/model/TitleModel;

.field private stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

.field private titleId:J

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 2
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 26
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 38
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 41
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getUserStats()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 42
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;Lcom/microsoft/xbox/service/model/TitleModel;)Lcom/microsoft/xbox/service/model/TitleModel;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/TitleModel;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->isLoadingLeaderboard:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->titleId:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;J)J
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;
    .param p1, "x1"    # J

    .prologue
    .line 24
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->titleId:J

    return-wide p1
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->userList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->userList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 131
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->isLoadingLeaderboard:Z

    if-eqz v0, :cond_1

    .line 132
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 139
    :goto_0
    return-void

    .line 134
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 137
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getLeaderBoardName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoardName:Ljava/lang/String;

    return-object v0
.end method

.method public getMyRank()Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->getMe()Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    move-result-object v0

    goto :goto_0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->titleId:J

    return-wide v0
.end method

.method public getTopPlayers()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->userList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public hasSeparateMyEntry()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->getMe()Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->isLoadingLeaderboard:Z

    return v0
.end method

.method public isTimeLeaderboard()Z
    .locals 3

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->getLeaderBoardName()Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "title":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07034c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->loadLeaderboardTask:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->loadLeaderboardTask:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->cancel()V

    .line 146
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->loadLeaderboardTask:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->loadLeaderboardTask:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->load(Z)V

    .line 148
    return-void
.end method

.method protected onLoadTitleLeaderboardCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 89
    const-string v0, "TitleLeaderboardScreenViewModel"

    const-string v1, "onLoadTitleLeaderboardCompleted Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->isLoadingLeaderboard:Z

    .line 92
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 125
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->updateAdapter()V

    .line 126
    return-void

    .line 96
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getLeaderBoard(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->leaderboardInfo:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$LeaderBoardInfo;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 102
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07034c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoardName:Ljava/lang/String;

    .line 116
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->GameProgress:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 104
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07034b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoardName:Ljava/lang/String;

    goto :goto_1

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->DisplayName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->DisplayName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoardName:Ljava/lang/String;

    goto :goto_1

    .line 110
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->leaderboardInfo:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$LeaderBoardInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$LeaderBoardInfo;->displayName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoardName:Ljava/lang/String;

    goto :goto_1

    .line 120
    :pswitch_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    .line 121
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->leaderBoard:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->loadLeaderboardTask:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->loadLeaderboardTask:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->cancel()V

    .line 65
    :cond_0
    return-void
.end method
