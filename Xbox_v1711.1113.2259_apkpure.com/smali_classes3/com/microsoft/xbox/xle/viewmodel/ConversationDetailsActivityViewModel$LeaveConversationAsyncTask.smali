.class Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ConversationDetailsActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LeaveConversationAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private conversationId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "conversationId"    # Ljava/lang/String;

    .prologue
    .line 957
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 958
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->conversationId:Ljava/lang/String;

    .line 959
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 963
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 964
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 992
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 993
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->leaveConversation(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 953
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 987
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 953
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 969
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 970
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 971
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 982
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 983
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 953
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 975
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 976
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$602(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Z)Z

    .line 977
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateAdapter()V

    .line 978
    return-void
.end method
