.class final Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;
.super Ljava/lang/Object;
.source "BehaviorRelay.java"

# interfaces
.implements Lio/reactivex/disposables/Disposable;
.implements Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList$NonThrowingPredicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jakewharton/rxrelay2/BehaviorRelay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "BehaviorDisposable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/disposables/Disposable;",
        "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList$NonThrowingPredicate",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final actual:Lio/reactivex/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observer",
            "<-TT;>;"
        }
    .end annotation
.end field

.field volatile cancelled:Z

.field emitting:Z

.field fastPath:Z

.field index:J

.field next:Z

.field queue:Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList",
            "<TT;>;"
        }
    .end annotation
.end field

.field final state:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/Observer;Lcom/jakewharton/rxrelay2/BehaviorRelay;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-TT;>;",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 272
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    .local p1, "actual":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-TT;>;"
    .local p2, "state":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    iput-object p1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->actual:Lio/reactivex/Observer;

    .line 274
    iput-object p2, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 275
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 279
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    iget-boolean v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->cancelled:Z

    if-nez v0, :cond_0

    .line 280
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->cancelled:Z

    .line 282
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->remove(Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;)V

    .line 284
    :cond_0
    return-void
.end method

.method emitFirst()V
    .locals 6

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    const/4 v3, 0x1

    .line 292
    iget-boolean v4, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->cancelled:Z

    if-eqz v4, :cond_1

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 296
    :cond_1
    monitor-enter p0

    .line 297
    :try_start_0
    iget-boolean v4, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->cancelled:Z

    if-eqz v4, :cond_2

    .line 298
    monitor-exit p0

    goto :goto_0

    .line 314
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 300
    :cond_2
    :try_start_1
    iget-boolean v4, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->next:Z

    if-eqz v4, :cond_3

    .line 301
    monitor-exit p0

    goto :goto_0

    .line 304
    :cond_3
    iget-object v2, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 305
    .local v2, "s":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    iget-object v0, v2, Lcom/jakewharton/rxrelay2/BehaviorRelay;->readLock:Ljava/util/concurrent/locks/Lock;

    .line 307
    .local v0, "lock":Ljava/util/concurrent/locks/Lock;
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 308
    iget-wide v4, v2, Lcom/jakewharton/rxrelay2/BehaviorRelay;->index:J

    iput-wide v4, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->index:J

    .line 309
    iget-object v4, v2, Lcom/jakewharton/rxrelay2/BehaviorRelay;->value:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    .line 310
    .local v1, "o":Ljava/lang/Object;, "TT;"
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 312
    if-eqz v1, :cond_4

    :goto_1
    iput-boolean v3, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->emitting:Z

    .line 313
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->next:Z

    .line 314
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    if-eqz v1, :cond_0

    .line 317
    invoke-virtual {p0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->test(Ljava/lang/Object;)Z

    .line 318
    invoke-virtual {p0}, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->emitLoop()V

    goto :goto_0

    .line 312
    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method emitLoop()V
    .locals 2

    .prologue
    .line 361
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    :goto_0
    iget-boolean v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->cancelled:Z

    if-eqz v1, :cond_0

    .line 369
    :goto_1
    return-void

    .line 365
    :cond_0
    monitor-enter p0

    .line 366
    :try_start_0
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->queue:Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;

    .line 367
    .local v0, "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    if-nez v0, :cond_1

    .line 368
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->emitting:Z

    .line 369
    monitor-exit p0

    goto :goto_1

    .line 372
    .end local v0    # "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 371
    .restart local v0    # "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    :cond_1
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->queue:Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;

    .line 372
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374
    invoke-virtual {v0, p0}, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->forEachWhile(Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList$NonThrowingPredicate;)V

    goto :goto_0
.end method

.method emitNext(Ljava/lang/Object;J)V
    .locals 6
    .param p2, "stateIndex"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;J)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    const/4 v4, 0x1

    .line 323
    iget-boolean v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->cancelled:Z

    if-eqz v1, :cond_0

    .line 349
    :goto_0
    return-void

    .line 326
    :cond_0
    iget-boolean v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->fastPath:Z

    if-nez v1, :cond_5

    .line 327
    monitor-enter p0

    .line 328
    :try_start_0
    iget-boolean v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->cancelled:Z

    if-eqz v1, :cond_1

    .line 329
    monitor-exit p0

    goto :goto_0

    .line 344
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 331
    :cond_1
    :try_start_1
    iget-wide v2, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->index:J

    cmp-long v1, v2, p2

    if-nez v1, :cond_2

    .line 332
    monitor-exit p0

    goto :goto_0

    .line 334
    :cond_2
    iget-boolean v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->emitting:Z

    if-eqz v1, :cond_4

    .line 335
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->queue:Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;

    .line 336
    .local v0, "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    if-nez v0, :cond_3

    .line 337
    new-instance v0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;

    .end local v0    # "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;-><init>(I)V

    .line 338
    .restart local v0    # "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    iput-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->queue:Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;

    .line 340
    :cond_3
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->add(Ljava/lang/Object;)V

    .line 341
    monitor-exit p0

    goto :goto_0

    .line 343
    .end local v0    # "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->next:Z

    .line 344
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345
    iput-boolean v4, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->fastPath:Z

    .line 348
    :cond_5
    invoke-virtual {p0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->test(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 288
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    iget-boolean v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->cancelled:Z

    return v0
.end method

.method public test(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 353
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    .local p1, "o":Ljava/lang/Object;, "TT;"
    iget-boolean v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->cancelled:Z

    if-nez v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->actual:Lio/reactivex/Observer;

    invoke-interface {v0, p1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 356
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
