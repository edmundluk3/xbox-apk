.class Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$6;
.super Ljava/lang/Object;
.source "FeedbackDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->negative:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->setFeedbackType(Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;)V

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setSelected(Z)V

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setSelected(Z)V

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->toggleSendButton()V

    .line 166
    return-void
.end method
