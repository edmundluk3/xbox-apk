.class public Lcom/microsoft/xbox/service/network/managers/SGFeaturedServiceManager;
.super Ljava/lang/Object;
.source "SGFeaturedServiceManager.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/ISGFeaturedServiceManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getProgrammingContentManifest()Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v2

    .line 22
    .local v2, "legalLocale":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getXboxOneFeaturedUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 25
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 26
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "User-Agent"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSmartGlassOverrideUserAgentString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "X-DeviceType"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSmartGlassOverrideXDeviceType()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 30
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    iget-object v4, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    .line 32
    .local v4, "stream":Ljava/io/InputStream;
    if-eqz v4, :cond_0

    .line 33
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XMLHelper;->instance()Lcom/microsoft/xbox/toolkit/XMLHelper;

    move-result-object v6

    const-class v7, Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;

    invoke-virtual {v6, v4, v7}, Lcom/microsoft/xbox/toolkit/XMLHelper;->load(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;

    .line 34
    .local v0, "data":Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 35
    return-object v0

    .line 37
    .end local v0    # "data":Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;
    :cond_0
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xfa1

    invoke-direct {v6, v8, v9}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v6
.end method
