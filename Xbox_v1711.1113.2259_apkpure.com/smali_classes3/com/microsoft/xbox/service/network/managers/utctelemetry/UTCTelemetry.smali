.class public Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;
.super Ljava/lang/Object;
.source "UTCTelemetry.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static log(LMicrosoft/Telemetry/Base;)V
    .locals 7
    .param p0, "event"    # LMicrosoft/Telemetry/Base;

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 19
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/Interop;->getCll()Lcom/microsoft/cll/android/Internal/AndroidInternalCll;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/microsoft/cll/android/EventSensitivity;

    invoke-virtual {v2, p0, v3}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->log(LMicrosoft/Telemetry/Base;[Lcom/microsoft/cll/android/EventSensitivity;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 30
    :goto_0
    return-void

    .line 20
    :catch_0
    move-exception v1

    .line 23
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    const-string v2, "Could not send %s.  Error: %s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 25
    .end local v1    # "oom":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v0

    .line 28
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Could not send %s.  Error: %s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
