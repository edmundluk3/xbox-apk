.class public abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystemSubscription;
.super Ljava/lang/Object;
.source "MultiplayerDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MultiplayerMemberPropertiesSystemSubscription"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystemSubscription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1403
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMemberPropertiesSystemSubscription$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMemberPropertiesSystemSubscription$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystemSubscription;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 1387
    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1389
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1390
    .local v0, "changeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "host"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1391
    const-string v1, "initialization"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1392
    const-string v1, "memberslist"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1393
    const-string v1, "membersstatus"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1394
    const-string v1, "customproperty"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1395
    const-string v1, "memberscustomproperty"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1396
    const-string v1, "joinability"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1397
    const-string v1, "cloudcompute"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1399
    new-instance v1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMemberPropertiesSystemSubscription;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMemberPropertiesSystemSubscription;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v1
.end method


# virtual methods
.method public abstract changeTypes()Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract id()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
