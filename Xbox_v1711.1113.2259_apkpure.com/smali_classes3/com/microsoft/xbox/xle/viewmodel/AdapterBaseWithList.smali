.class public abstract Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "AdapterBaseWithList.java"


# static fields
.field protected static final LIST_VIEW_ANIMATION_NAME:Ljava/lang/String; = "ListView"


# instance fields
.field protected listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 19
    return-void
.end method

.method protected constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 23
    return-void
.end method

.method private saveListPosition()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v3

    .line 47
    .local v3, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v4, :cond_0

    .line 48
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->getFirstVisiblePosition()I

    move-result v0

    .line 49
    .local v0, "index":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 50
    .local v2, "v":Landroid/view/View;
    if-nez v2, :cond_1

    .line 51
    .local v1, "offset":I
    :goto_0
    invoke-virtual {v3, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->setListPosition(II)V

    .line 54
    .end local v0    # "index":I
    .end local v1    # "offset":I
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    return-void

    .line 50
    .restart local v0    # "index":I
    .restart local v2    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method protected abstract getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
.end method

.method protected abstract getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 33
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 34
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->saveListPosition()V

    .line 40
    return-void
.end method

.method protected restoreListPosition()V
    .locals 4

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    .line 58
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListPosition()I

    move-result v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListOffset()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPositionWithOffset(Landroid/widget/ListView;II)V

    .line 62
    :cond_0
    return-void
.end method

.method protected setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V
    .locals 0
    .param p1, "listView"    # Lcom/microsoft/xbox/toolkit/ui/XLEListView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 27
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 29
    return-void
.end method

.method public setScreenState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 71
    :cond_0
    return-void
.end method
