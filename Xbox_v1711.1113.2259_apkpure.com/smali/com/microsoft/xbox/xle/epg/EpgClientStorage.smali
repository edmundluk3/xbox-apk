.class public Lcom/microsoft/xbox/xle/epg/EpgClientStorage;
.super Ljava/lang/Object;
.source "EpgClientStorage.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "EpgClientStorage"

.field private static final sAppChannelLastChannel:Ljava/lang/String; = "AppChannelLastChannel"

.field private static final sAppChannelSelectedChannel:Ljava/lang/String; = "AppChannelSelectedChannel"

.field private static final sDefaultLastChannel:I = 0x0

.field private static final sDefaultSelectedChannel:I = -0x1

.field private static final sFileName:Ljava/lang/String; = "xleepgdata"

.field private static sInstance:Lcom/microsoft/xbox/xle/epg/EpgClientStorage; = null

.field private static final sLastProvider:Ljava/lang/String; = "LastProvider"

.field private static final sLastScreen:Ljava/lang/String; = "LastScreen"

.field private static final sProviderList:Ljava/lang/String; = "ProviderList"

.field private static final sSessionDuration:Ljava/lang/String; = "SessionDuration"

.field private static final sSessionEndTime:Ljava/lang/String; = "SessionEndTime"

.field private static final sSessionID:Ljava/lang/String; = "SessionID"


# instance fields
.field private mAppChannelLastChannel:I

.field private mAppChannelSelectedChannel:I

.field private mContext:Landroid/content/Context;

.field private mLastProvider:Ljava/lang/String;

.field private mLastScreen:I

.field private mPreferences:Landroid/content/SharedPreferences;

.field private mProviderList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSessionDuration:J

.field private mSessionEndTime:J

.field private mSessionID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->sInstance:Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    iput v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mLastScreen:I

    .line 144
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    .line 145
    iput-object v2, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mLastProvider:Ljava/lang/String;

    .line 146
    iput v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mAppChannelLastChannel:I

    .line 147
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mAppChannelSelectedChannel:I

    .line 148
    iput-object v2, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionID:Ljava/lang/String;

    .line 149
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionEndTime:J

    .line 150
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionDuration:J

    .line 152
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mContext:Landroid/content/Context;

    .line 153
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->loadAllData()V

    .line 154
    return-void
.end method

.method public static connectToEpgModel()V
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->sInstance:Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->addListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;)V

    .line 186
    return-void
.end method

.method public static disconnectFromEpgModel()V
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->sInstance:Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->removeListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;)V

    .line 190
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;
    .locals 1

    .prologue
    .line 170
    sget-object v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->sInstance:Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    return-object v0
.end method

.method private loadAllData()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "xleepgdata"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "LastScreen"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mLastScreen:I

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "ProviderList"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->populateProviderInfo(Ljava/util/Set;)V

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "LastProvider"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mLastProvider:Ljava/lang/String;

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "AppChannelLastChannel"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mAppChannelLastChannel:I

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "AppChannelSelectedChannel"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mAppChannelSelectedChannel:I

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "SessionID"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionID:Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "SessionEndTime"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionEndTime:J

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "SessionDuration"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionDuration:J

    .line 167
    return-void
.end method

.method private populateProviderInfo(Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "str":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    :cond_0
    return-void

    .line 92
    :cond_1
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 93
    .local v1, "provider_string":Ljava/lang/String;
    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    move-result-object v0

    .line 94
    .local v0, "providerStore":Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    if-eqz v0, :cond_2

    .line 95
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private writeProviderInfo()V
    .locals 6

    .prologue
    .line 104
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 106
    .local v1, "providerSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    .line 107
    .local v2, "providerStore":Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    if-eqz v2, :cond_0

    .line 108
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->serialize()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 112
    .end local v2    # "providerStore":Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "xleepgdata"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    .line 113
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 114
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "ProviderList"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 115
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 116
    return-void
.end method


# virtual methods
.method public getAppChannelLastChannel()I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mAppChannelLastChannel:I

    return v0
.end method

.method public getAppChannelSelectedChannel()I
    .locals 1

    .prologue
    .line 331
    iget v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mAppChannelSelectedChannel:I

    return v0
.end method

.method public getFavoritesLastChannel(Ljava/lang/String;)I
    .locals 3
    .param p1, "headendID"    # Ljava/lang/String;

    .prologue
    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    const-string v0, "EpgClientStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFavoritesLastChannel: headend not found accessing storage ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const/4 v0, 0x0

    .line 247
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    iget v0, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->lastFavoritesChannel:I

    goto :goto_0
.end method

.method public getFavoritesSelectedChannel(Ljava/lang/String;)I
    .locals 3
    .param p1, "headendID"    # Ljava/lang/String;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    const-string v0, "EpgClientStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFavoritesSelectedChannel: headend not found accessing storage ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const/4 v0, -0x1

    .line 285
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    iget v0, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->selectedFavoritesChannel:I

    goto :goto_0
.end method

.method public getLastProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mLastProvider:Ljava/lang/String;

    return-object v0
.end method

.method public getLastScreen()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mLastScreen:I

    return v0
.end method

.method public getSessionDuration()J
    .locals 2

    .prologue
    .line 367
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionDuration:J

    return-wide v0
.end method

.method public getSessionEndTime()J
    .locals 2

    .prologue
    .line 355
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionEndTime:J

    return-wide v0
.end method

.method public getSessionID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionID:Ljava/lang/String;

    return-object v0
.end method

.method public getStoredProviders()[Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    return-object v0
.end method

.method public getTVLastChannel(Ljava/lang/String;)I
    .locals 3
    .param p1, "headendID"    # Ljava/lang/String;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    const-string v0, "EpgClientStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTVLastChannel: headend not found accessing storage ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const/4 v0, 0x0

    .line 228
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    iget v0, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->lastTVChannel:I

    goto :goto_0
.end method

.method public getTVSelectedChannel(Ljava/lang/String;)I
    .locals 3
    .param p1, "headendID"    # Ljava/lang/String;

    .prologue
    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 262
    const-string v0, "EpgClientStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTVSelectedChannel: headend not found accessing storage ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const/4 v0, -0x1

    .line 266
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    iget v0, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->selectedTVChannel:I

    goto :goto_0
.end method

.method public onActiveProviderChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
    .locals 1
    .param p1, "newProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    .line 376
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setLastProvider(Ljava/lang/String;)V

    .line 377
    return-void

    .line 376
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDataChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;II)V
    .locals 0
    .param p1, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .param p2, "startChannel"    # I
    .param p3, "endChannel"    # I

    .prologue
    .line 433
    return-void
.end method

.method public onFavoritesError(Lcom/microsoft/xbox/service/model/epg/EPGProvider;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
    .locals 0
    .param p1, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .param p2, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .prologue
    .line 438
    return-void
.end method

.method public onFetchingStatusChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
    .locals 0
    .param p1, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    .line 428
    return-void
.end method

.method public onProviderListChanged()V
    .locals 17

    .prologue
    .line 381
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v15

    .line 384
    .local v15, "providers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/microsoft/xbox/service/model/epg/EPGProvider;>;"
    invoke-virtual {v15}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 385
    const-string v1, "EpgClientStorage"

    const-string v2, "onProviderListChanged: provider list null, removing all providers"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 423
    :goto_0
    return-void

    .line 391
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    .line 392
    .local v13, "providerList":[Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    array-length v1, v13

    if-ge v11, v1, :cond_4

    .line 393
    aget-object v1, v13, v11

    iget-object v1, v1, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iget-object v9, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    .line 394
    .local v9, "existing_headend":Ljava/lang/String;
    const/4 v10, 0x0

    .line 395
    .local v10, "found":Z
    invoke-virtual {v15}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 396
    .local v12, "new_provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 397
    const/4 v10, 0x1

    .line 401
    .end local v12    # "new_provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_2
    if-nez v10, :cond_3

    .line 402
    const-string v1, "EpgClientStorage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onProviderListChanged: Removing provider "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v13, v11

    iget-object v3, v3, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v1, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 408
    .end local v9    # "existing_headend":Ljava/lang/String;
    .end local v10    # "found":Z
    :cond_4
    invoke-virtual {v15}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_5
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 409
    .restart local v12    # "new_provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 410
    const-string v1, "EpgClientStorage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onProviderListChanged: Adding provider "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    new-instance v14, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    invoke-direct {v14}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;-><init>()V

    .line 412
    .local v14, "providerStore":Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getTitleId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderSource()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    move-result-object v5

    .line 413
    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIsPreferred()Z

    move-result v6

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getFilter()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    move-result-object v7

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;ZLcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;Z)V

    iput-object v1, v14, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    .line 414
    const/4 v1, 0x0

    iput v1, v14, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->lastTVChannel:I

    .line 415
    const/4 v1, 0x0

    iput v1, v14, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->lastFavoritesChannel:I

    .line 416
    const/4 v1, -0x1

    iput v1, v14, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->selectedTVChannel:I

    .line 417
    const/4 v1, -0x1

    iput v1, v14, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->selectedFavoritesChannel:I

    .line 418
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    iget-object v2, v14, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v1, v2, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 422
    .end local v12    # "new_provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .end local v14    # "providerStore":Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->writeProviderInfo()V

    goto/16 :goto_0
.end method

.method public reset()V
    .locals 4

    .prologue
    .line 174
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "xleepgdata"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    .line 175
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 176
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 177
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 178
    return-void
.end method

.method public setAppChannelLastChannel(I)V
    .locals 4
    .param p1, "channel"    # I

    .prologue
    .line 311
    iput p1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mAppChannelLastChannel:I

    .line 312
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "xleepgdata"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    .line 313
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 314
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "AppChannelLastChannel"

    iget v2, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mAppChannelLastChannel:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 315
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 316
    return-void
.end method

.method public setAppChannelSelectedChannel(I)V
    .locals 4
    .param p1, "channel"    # I

    .prologue
    .line 323
    iput p1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mAppChannelSelectedChannel:I

    .line 324
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "xleepgdata"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    .line 325
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 326
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "AppChannelSelectedChannel"

    iget v2, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mAppChannelSelectedChannel:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 327
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 328
    return-void
.end method

.method public setFavoritesLastChannel(Ljava/lang/String;I)V
    .locals 3
    .param p1, "headendID"    # Ljava/lang/String;
    .param p2, "channel"    # I

    .prologue
    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    const-string v0, "EpgClientStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setFavoritesLastChannel: headend not found accessing storage ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    iput p2, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->lastFavoritesChannel:I

    .line 238
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->writeProviderInfo()V

    goto :goto_0
.end method

.method public setFavoritesSelectedChannel(Ljava/lang/String;I)V
    .locals 3
    .param p1, "headendID"    # Ljava/lang/String;
    .param p2, "channel"    # I

    .prologue
    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 271
    const-string v0, "EpgClientStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setFavoritesSelectedChannel: headend not found accessing storage ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :goto_0
    return-void

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    iput p2, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->selectedFavoritesChannel:I

    .line 276
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->writeProviderInfo()V

    goto :goto_0
.end method

.method public setLastProvider(Ljava/lang/String;)V
    .locals 4
    .param p1, "headendID"    # Ljava/lang/String;

    .prologue
    .line 299
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mLastProvider:Ljava/lang/String;

    .line 300
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "xleepgdata"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    .line 301
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 302
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "LastProvider"

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mLastProvider:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 303
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 304
    return-void
.end method

.method public setLastScreen(I)V
    .locals 4
    .param p1, "screen"    # I

    .prologue
    .line 193
    iput p1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mLastScreen:I

    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "xleepgdata"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    .line 195
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 196
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "LastScreen"

    iget v2, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mLastScreen:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 197
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 198
    return-void
.end method

.method public setLastScreen(Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    .prologue
    .line 201
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setLastScreen(I)V

    .line 202
    return-void
.end method

.method public setPreferredFilter(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;)V
    .locals 3
    .param p1, "headendID"    # Ljava/lang/String;
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    .prologue
    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    const-string v0, "EpgClientStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setPreferredFilter: headend not found accessing storage ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :goto_0
    return-void

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iput-object p2, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    .line 295
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->writeProviderInfo()V

    goto :goto_0
.end method

.method public setSessionDuration(J)V
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 359
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionDuration:J

    .line 360
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "xleepgdata"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    .line 361
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 362
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "SessionDuration"

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionDuration:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 363
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 364
    return-void
.end method

.method public setSessionEndTime(J)V
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 347
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionEndTime:J

    .line 348
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "xleepgdata"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    .line 349
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 350
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "SessionEndTime"

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionEndTime:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 351
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 352
    return-void
.end method

.method public setSessionID(Ljava/lang/String;)V
    .locals 4
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 335
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionID:Ljava/lang/String;

    .line 336
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "xleepgdata"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    .line 337
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 338
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "SessionID"

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mSessionID:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 339
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 340
    return-void
.end method

.method public setTVLastChannel(Ljava/lang/String;I)V
    .locals 3
    .param p1, "headendID"    # Ljava/lang/String;
    .param p2, "channel"    # I

    .prologue
    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    const-string v0, "EpgClientStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setTVLastChannel: headend not found accessing storage ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    iput p2, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->lastTVChannel:I

    .line 219
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->writeProviderInfo()V

    goto :goto_0
.end method

.method public setTVSelectedChannel(Ljava/lang/String;I)V
    .locals 3
    .param p1, "headendID"    # Ljava/lang/String;
    .param p2, "channel"    # I

    .prologue
    .line 251
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    const-string v0, "EpgClientStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setTVSelectedChannel: headend not found accessing storage ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->mProviderList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    iput p2, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->selectedTVChannel:I

    .line 257
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->writeProviderInfo()V

    goto :goto_0
.end method
