.class public final enum Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
.super Ljava/lang/Enum;
.source "StoreBrowseFilter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum ComingSoon:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum GamepassRTMList1:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum GamepassRTMList2:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum GamepassRTMList3:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum GamepassRTMList4:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum GamepassRTMList5:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum New:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum Popular:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum Recent:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum TopFree:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum TopPaid:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field public static final enum Undefined:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;


# instance fields
.field private final resId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 20
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "Popular"

    const v2, 0x7f070136

    const-string v3, "Popular"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Popular:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "Recent"

    const v2, 0x7f070137

    const-string v3, "Recent"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Recent:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "ComingSoon"

    const v2, 0x7f070124

    const-string v3, "ComingSoon"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->ComingSoon:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "New"

    const v2, 0x7f070135

    const-string v3, "New"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->New:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "TopFree"

    const v2, 0x7f070138

    const-string v3, "TopFree"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->TopFree:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "TopPaid"

    const/4 v2, 0x5

    const v3, 0x7f070139

    const-string v4, "TopPaid"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->TopPaid:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "GamepassRTMList1"

    const/4 v2, 0x6

    const v3, 0x7f070c43

    const-string v4, "GamePass - Featured"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList1:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 27
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "GamepassRTMList2"

    const/4 v2, 0x7

    const v3, 0x7f070c41

    const-string v4, "GamePass - All games A-Z"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList2:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "GamepassRTMList3"

    const/16 v2, 0x8

    const v3, 0x7f070c40

    const-string v4, "GamePass - Action, shooters, and RPGs"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList3:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "GamepassRTMList4"

    const/16 v2, 0x9

    const v3, 0x7f070c46

    const-string v4, "GamePass - Sports and Racing"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList4:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 30
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "GamepassRTMList5"

    const/16 v2, 0xa

    const v3, 0x7f070c44

    const-string v4, "GamePass - Awesome Indies from ID@Xbox"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList5:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const-string v1, "Undefined"

    const/16 v2, 0xb

    const-string v3, "NotUsed"

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Undefined:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 19
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Popular:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Recent:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->ComingSoon:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->New:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v1, v0, v8

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->TopFree:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->TopPaid:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList1:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList2:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList3:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList4:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList5:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Undefined:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "resId"    # I
    .param p4, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 40
    iput p3, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->resId:I

    .line 41
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->telemetryName:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public static getSpinnerArrayItemsByBrowseType(Lcom/microsoft/xbox/service/model/StoreBrowseType;)Ljava/util/List;
    .locals 3
    .param p0, "browseType"    # Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/StoreBrowseType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter$1;->$SwitchMap$com$microsoft$xbox$service$model$StoreBrowseType:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/StoreBrowseType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 82
    :goto_0
    return-object v0

    .line 61
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Popular:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Recent:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->ComingSoon:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->New:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->TopFree:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->TopPaid:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->New:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Popular:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    :pswitch_3
    const-string v1, "Unexpected: setting not set to using Arches RTM lists"

    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->useArchesRTMLists()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 76
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList1:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList2:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList3:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList4:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList5:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 53
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->resId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
