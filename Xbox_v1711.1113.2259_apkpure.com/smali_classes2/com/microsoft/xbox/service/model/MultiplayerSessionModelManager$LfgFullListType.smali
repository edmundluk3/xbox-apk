.class public final enum Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
.super Ljava/lang/Enum;
.source "MultiplayerSessionModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LfgFullListType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

.field public static final enum Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

.field public static final enum Following:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

.field public static final enum Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;


# instance fields
.field private final updateType:Lcom/microsoft/xbox/service/model/UpdateType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 72
    new-instance v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    const-string v1, "Upcoming"

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgSessionsUpcoming:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;-><init>(Ljava/lang/String;ILcom/microsoft/xbox/service/model/UpdateType;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .line 73
    new-instance v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    const-string v1, "Following"

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgSessionsFollowing:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;-><init>(Ljava/lang/String;ILcom/microsoft/xbox/service/model/UpdateType;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Following:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .line 74
    new-instance v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    const-string v1, "Clubs"

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgClubSessions:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;-><init>(Ljava/lang/String;ILcom/microsoft/xbox/service/model/UpdateType;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .line 71
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Following:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->$VALUES:[Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/microsoft/xbox/service/model/UpdateType;)V
    .locals 0
    .param p3, "updateType"    # Lcom/microsoft/xbox/service/model/UpdateType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/UpdateType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 79
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->updateType:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 80
    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Lcom/microsoft/xbox/service/model/UpdateType;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->updateType:Lcom/microsoft/xbox/service/model/UpdateType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 71
    const-class v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->$VALUES:[Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    return-object v0
.end method


# virtual methods
.method public getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->updateType:Lcom/microsoft/xbox/service/model/UpdateType;

    return-object v0
.end method
