.class public Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "FeaturedLandingScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;
    }
.end annotation


# instance fields
.field protected final cellMargin:I

.field private featuredList:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;

.field private redeemCodeButton:Landroid/view/View;

.field private searchButton:Landroid/view/View;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;

    .line 37
    const v0, 0x7f0e05a1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->featuredList:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .line 38
    const v0, 0x7f0e059c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->featuredList:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09034b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->cellMargin:I

    .line 40
    const v0, 0x7f0e059f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->redeemCodeButton:Landroid/view/View;

    .line 41
    const v0, 0x7f0e059e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->searchButton:Landroid/view/View;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;

    return-object v0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->featuredList:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->featuredList:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->redeemCodeButton:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->searchButton:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->featuredList:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->redeemCodeButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    return-void
.end method

.method public setSwitchPanelState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 86
    return-void
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->updateLoadingIndicator(Z)V

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->getDataList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->update(Ljava/util/List;)V

    .line 82
    return-void
.end method
