.class public Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "ProviderPickerDialogViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;,
        Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private detailTask:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;

.field private final errorRid:I

.field private isLoadingDetail:Z

.field private mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
            "<+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            "+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final msgRid:I

.field private final onClickListener:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;

.field private final selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 3
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 44
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$1;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$1;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    const v1, 0x7f070696

    const v2, 0x7f07068d

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;II)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;II)V
    .locals 1
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p2, "onClickListener"    # Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;
    .param p3, "msgRid"    # I
    .param p4, "errorRid"    # I

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 31
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 65
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 66
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->onClickListener:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;

    .line 67
    iput p3, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->msgRid:I

    .line 68
    iput p4, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->errorRid:I

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->isLoadingDetail:Z

    return p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->updateViewInternal()V

    return-void
.end method

.method private hasContent()Z
    .locals 2

    .prologue
    .line 173
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 174
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getProviders()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateViewInternal()V
    .locals 0

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->updateAdapter()V

    .line 179
    return-void
.end method


# virtual methods
.method public LaunchWithProviderInfo(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V
    .locals 3
    .param p1, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .param p2, "location"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v2

    invoke-static {p1, v0, v1, v2, p2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Ljava/lang/String;IILcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V

    .line 149
    return-void
.end method

.method public getAirings()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getAirings()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getErrorRid()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->errorRid:I

    return v0
.end method

.method public getMediaGroup()I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v0

    return v0
.end method

.method public getMessageRid()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->msgRid:I

    return v0
.end method

.method public getProviders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitleImagePlaceholderRid()I
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    return v0
.end method

.method public getTitleImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->isLoadingDetail:Z

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 102
    return-void
.end method

.method public onCloseTouched()V
    .locals 1

    .prologue
    .line 144
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 145
    return-void
.end method

.method public onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 152
    const-string v0, "ProviderPickerDialogViewModel"

    const-string v1, "onLoadDetail Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->isLoadingDetail:Z

    .line 155
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 169
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->updateViewInternal()V

    .line 170
    return-void

    .line 159
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->hasContent()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 163
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 164
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 155
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onStartOverride()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 76
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->load(Z)V

    .line 78
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 87
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->println()V

    .line 88
    return-void
.end method

.method public providerTapped(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V
    .locals 3
    .param p1, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .prologue
    .line 137
    if-eqz p1, :cond_0

    .line 138
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Provider Picker"

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->onClickListener:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;->onClick(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V

    .line 141
    return-void
.end method

.method protected shouldDismissTopNoFatalAlert()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method
