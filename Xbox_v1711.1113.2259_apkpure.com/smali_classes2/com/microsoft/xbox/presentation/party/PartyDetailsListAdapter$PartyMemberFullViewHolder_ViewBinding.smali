.class public Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "PartyDetailsListAdapter$PartyMemberFullViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;

    .line 24
    const v0, 0x7f0e0859

    const-string v1, "field \'detailContainer\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->detailContainer:Landroid/view/View;

    .line 25
    const v0, 0x7f0e0866

    const-string v1, "field \'squawkerView\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->squawkerView:Landroid/view/View;

    .line 26
    const v0, 0x7f0e0867

    const-string v1, "field \'gamerpic\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 27
    const v0, 0x7f0e085b

    const-string v1, "field \'gamertag\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->gamertag:Landroid/widget/TextView;

    .line 28
    const v0, 0x7f0e085c

    const-string v1, "field \'realname\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->realname:Landroid/widget/TextView;

    .line 29
    const v0, 0x7f0e085d

    const-string v1, "field \'leaderIcon\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->leaderIcon:Landroid/view/View;

    .line 30
    const v0, 0x7f0e085f

    const-string v1, "field \'broadcastingIcon\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->broadcastingIcon:Landroid/view/View;

    .line 31
    const v0, 0x7f0e085e

    const-string v1, "field \'expandIcon\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->expandIcon:Landroid/widget/TextView;

    .line 32
    const v0, 0x7f0e0860

    const-string v1, "field \'presence\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->presence:Landroid/widget/TextView;

    .line 33
    const v0, 0x7f0e0861

    const-string v1, "field \'optionsContainer\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->optionsContainer:Landroid/view/View;

    .line 34
    const v0, 0x7f0e0862

    const-string v1, "field \'muteButton\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->muteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 35
    const v0, 0x7f0e0863

    const-string v1, "field \'profileButton\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->profileButton:Landroid/view/View;

    .line 36
    const v0, 0x7f0e0864

    const-string v1, "field \'kickButton\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->kickButton:Landroid/view/View;

    .line 37
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;

    .line 43
    .local v0, "target":Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 44
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;

    .line 46
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->detailContainer:Landroid/view/View;

    .line 47
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->squawkerView:Landroid/view/View;

    .line 48
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 49
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->gamertag:Landroid/widget/TextView;

    .line 50
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->realname:Landroid/widget/TextView;

    .line 51
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->leaderIcon:Landroid/view/View;

    .line 52
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->broadcastingIcon:Landroid/view/View;

    .line 53
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->expandIcon:Landroid/widget/TextView;

    .line 54
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->presence:Landroid/widget/TextView;

    .line 55
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->optionsContainer:Landroid/view/View;

    .line 56
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->muteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 57
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->profileButton:Landroid/view/View;

    .line 58
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->kickButton:Landroid/view/View;

    .line 59
    return-void
.end method
