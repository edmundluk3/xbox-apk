.class public final Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents;
.super Ljava/lang/Object;
.source "OOBEViewIntents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$PingIntent;,
        Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;,
        Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;,
        Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$NextIntent;,
        Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$BackIntent;,
        Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoAppUpdatesChangedIntent;,
        Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoSystemUpdatesChangedIntent;,
        Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;,
        Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DSTOptionChangedIntent;,
        Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;,
        Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
