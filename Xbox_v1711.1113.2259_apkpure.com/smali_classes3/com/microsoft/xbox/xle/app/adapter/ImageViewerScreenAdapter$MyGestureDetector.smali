.class Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ImageViewerScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyGestureDetector"
.end annotation


# instance fields
.field private final saveImageToDeviceTask:Ljava/lang/Runnable;

.field private final shareImageIntentTask:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)V
    .locals 1

    .prologue
    .line 128
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 164
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->shareImageIntentTask:Ljava/lang/Runnable;

    .line 198
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->saveImageToDeviceTask:Ljava/lang/Runnable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->saveImageToDeviceTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->shareImageIntentTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method private getCurrentBitmap()Landroid/graphics/Bitmap;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 233
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;

    move-result-object v0

    .line 235
    .local v0, "iv":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    instance-of v1, v1, Lcom/bumptech/glide/load/resource/bitmap/GlideBitmapDrawable;

    if-eqz v1, :cond_0

    .line 236
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Lcom/bumptech/glide/load/resource/bitmap/GlideBitmapDrawable;

    invoke-virtual {v1}, Lcom/bumptech/glide/load/resource/bitmap/GlideBitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 239
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;)V
    .locals 13
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;

    .prologue
    const v12, 0x7f070b52

    const/4 v11, 0x1

    .line 165
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    .line 168
    .local v2, "context":Landroid/content/Context;
    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->getCurrentBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 170
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 171
    const-string v8, "temp_share_smartglass"

    const-string v9, ".png"

    invoke-virtual {v2}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v10

    invoke-static {v8, v9, v10}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v5

    .line 172
    .local v5, "file":Ljava/io/File;
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 173
    .local v4, "fOut":Ljava/io/FileOutputStream;
    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x64

    invoke-virtual {v0, v8, v9, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 174
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V

    .line 175
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 177
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 178
    .local v7, "sendIntent":Landroid/content/Intent;
    const-string v8, "android.intent.action.SEND"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 180
    .local v6, "screenshotUri":Landroid/net/Uri;
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v7, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 181
    const-string v8, "image/*"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    const-string v8, "Choose."

    invoke-static {v7, v8}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    .line 183
    .local v1, "chooser":Landroid/content/Intent;
    const/high16 v8, 0x10000000

    invoke-virtual {v1, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 185
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 186
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "chooser":Landroid/content/Intent;
    .end local v4    # "fOut":Ljava/io/FileOutputStream;
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "screenshotUri":Landroid/net/Uri;
    .end local v7    # "sendIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v8

    const-string v9, "Screenshot Share"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    return-void

    .line 189
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v8

    const v9, 0x7f070b52

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 191
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v3

    .line 192
    .local v3, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v8

    invoke-virtual {v8, v12, v11}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V

    goto :goto_0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;)V
    .locals 13
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;

    .prologue
    const v12, 0x7f070b51

    const/4 v11, 0x1

    .line 199
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 202
    .local v1, "context":Landroid/content/Context;
    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->getCurrentBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 204
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 205
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/SmartGlass"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 206
    .local v7, "path":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 207
    .local v2, "dir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 208
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 210
    :cond_0
    new-instance v5, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "IMG_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".png"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z

    .line 212
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 213
    .local v4, "fOut":Ljava/io/FileOutputStream;
    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x64

    invoke-virtual {v0, v8, v9, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 214
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V

    .line 215
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 217
    new-instance v6, Landroid/content/Intent;

    const-string v8, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 218
    .local v6, "mediaScanIntent":Landroid/content/Intent;
    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 219
    invoke-virtual {v1, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 220
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v8

    const v9, 0x7f070b50

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "dir":Ljava/io/File;
    .end local v4    # "fOut":Ljava/io/FileOutputStream;
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "mediaScanIntent":Landroid/content/Intent;
    .end local v7    # "path":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v8

    const-string v9, "Screenshot Save"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    return-void

    .line 222
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v8

    const v9, 0x7f070b51

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 224
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v3

    .line 225
    .local v3, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v8

    invoke-virtual {v8, v12, v11}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V

    goto :goto_0
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/high16 v4, 0x43480000    # 200.0f

    const/high16 v3, 0x42f00000    # 120.0f

    const/4 v2, 0x0

    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->getScaled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;->getImageCount()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 269
    :cond_0
    :goto_0
    return v2

    .line 253
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x437a0000    # 250.0f

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 256
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    .line 257
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$900(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/widget/ViewFlipper;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 258
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$900(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/widget/ViewFlipper;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$1000(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 259
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$900(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showPrevious()V

    goto :goto_0

    .line 265
    :catch_0
    move-exception v0

    goto :goto_0

    .line 260
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$900(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/widget/ViewFlipper;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$900(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/widget/ViewFlipper;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$1200(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$900(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 137
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 138
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f070b4b

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 139
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 140
    .local v1, "context":Landroid/content/Context;
    const/4 v4, 0x2

    new-array v3, v4, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const v5, 0x7f070b4c

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const v5, 0x7f070b4e

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 142
    .local v3, "options":[Ljava/lang/CharSequence;
    new-instance v4, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 160
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 161
    .local v2, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 162
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 244
    const/4 v0, 0x0

    return v0
.end method
