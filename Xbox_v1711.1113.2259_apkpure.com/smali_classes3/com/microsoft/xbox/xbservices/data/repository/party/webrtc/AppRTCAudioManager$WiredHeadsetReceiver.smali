.class Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$WiredHeadsetReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AppRTCAudioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WiredHeadsetReceiver"
.end annotation


# static fields
.field private static final HAS_MIC:I = 0x1

.field private static final HAS_NO_MIC:I = 0x0

.field private static final STATE_PLUGGED:I = 0x1

.field private static final STATE_UNPLUGGED:I


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$WiredHeadsetReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;
    .param p2, "x1"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$1;

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$WiredHeadsetReceiver;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 161
    const-string v3, "state"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 162
    .local v2, "state":I
    const-string v3, "microphone"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 163
    .local v0, "microphone":I
    const-string v3, "name"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "name":Ljava/lang/String;
    const-string v6, "AppRTCAudioManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WiredHeadsetReceiver.onReceive: a="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 165
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", s="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v2, :cond_0

    const-string v3, "unplugged"

    :goto_0
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", m="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-ne v0, v4, :cond_1

    const-string v3, "mic"

    :goto_1
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", n="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", sb="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 168
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$WiredHeadsetReceiver;->isInitialStickyBroadcast()Z

    move-result v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 164
    invoke-static {v6, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$WiredHeadsetReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    if-ne v2, v4, :cond_2

    move v3, v4

    :goto_2
    invoke-static {v6, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->access$002(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;Z)Z

    .line 170
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$WiredHeadsetReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->updateAudioDeviceState()V

    .line 171
    return-void

    .line 165
    :cond_0
    const-string v3, "plugged"

    goto :goto_0

    :cond_1
    const-string v3, "no mic"

    goto :goto_1

    :cond_2
    move v3, v5

    .line 169
    goto :goto_2
.end method
