.class public Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;
.super Ljava/lang/Object;
.source "UTCError.java"


# static fields
.field private static final CLIENTERRORVERSION:I = 0x1

.field private static final FAILURE:Ljava/lang/String; = "Client Error Type - Failure"

.field private static final MSACANCEL:Ljava/lang/String; = "Client Error Type - MSA canceled"

.field private static final SERVICEERRORVERSION:I = 0x1

.field private static final SIGNEDOUT:Ljava/lang/String; = "Client Error Type - Signed out"

.field private static final UINEEDEDERROR:Ljava/lang/String; = "Client Error Type - UI Needed"

.field private static final USERCANCEL:Ljava/lang/String; = "Client Error Type - User canceled"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackClose(Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;Ljava/lang/CharSequence;)V
    .locals 3
    .param p0, "errorScreen"    # Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;
    .param p1, "activityTitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 245
    :try_start_0
    const-string v1, "Errors - Close error screen"

    invoke-static {v1, p1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :goto_0
    return-void

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "UTCError.trackClose"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;->trackException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 248
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackException(Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 6
    .param p0, "ex"    # Ljava/lang/Exception;
    .param p1, "callingSource"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 195
    new-instance v0, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;-><init>()V

    .line 198
    .local v0, "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 200
    const-string v1, "%s:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 202
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setErrorName(Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setErrorText(Ljava/lang/String;)V

    .line 204
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setCallStack(Ljava/lang/String;)V

    .line 207
    invoke-static {}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageView;->getCurrentPage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setPageName(Ljava/lang/String;)V

    .line 210
    invoke-static {v0}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry;->LogEvent(LMicrosoft/Telemetry/Base;)V

    .line 212
    :cond_0
    return-void
.end method

.method public static trackFailure(Ljava/lang/String;ZLcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;J)V
    .locals 9
    .param p0, "jobName"    # Ljava/lang/String;
    .param p1, "isSilent"    # Z
    .param p2, "source"    # Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;
    .param p3, "errorCode"    # J

    .prologue
    const/4 v7, 0x0

    .line 175
    :try_start_0
    new-instance v2, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;

    invoke-direct {v2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;-><init>()V

    .line 176
    .local v2, "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    const-string v3, "isSilent"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 177
    const-string v3, "job"

    invoke-virtual {v2, v3, p0}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 178
    const-string v3, "source"

    invoke-virtual {v2, v3, p2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 180
    new-instance v1, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;

    invoke-direct {v1}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;-><init>()V

    .line 181
    .local v1, "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    const-string v3, "Client Error Type - Failure"

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setErrorName(Ljava/lang/String;)V

    .line 182
    const-string v3, "%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setErrorCode(Ljava/lang/String;)V

    .line 183
    invoke-static {}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageView;->getCurrentPage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setPageName(Ljava/lang/String;)V

    .line 184
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCCommonDataModel;->getCommonData(ILcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;)Lcom/microsoft/xbox/idp/telemetry/utc/CommonData;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 186
    const-string v3, "Error:%s, errorCode:%s, additionalInfo:%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "Client Error Type - Failure"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    invoke-static {v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry;->LogEvent(LMicrosoft/Telemetry/Base;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    .end local v1    # "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    .end local v2    # "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    :goto_0
    return-void

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "UTCError.trackFailure"

    invoke-static {v0, v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;->trackException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 190
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackFailure(Ljava/lang/String;ZLcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;Ljava/lang/Exception;)V
    .locals 10
    .param p0, "jobName"    # Ljava/lang/String;
    .param p1, "isSilent"    # Z
    .param p2, "source"    # Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    const/4 v9, 0x0

    .line 137
    :try_start_0
    new-instance v4, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;

    invoke-direct {v4}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;-><init>()V

    .line 138
    .local v4, "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    const-string v5, "isSilent"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 139
    const-string v5, "job"

    invoke-virtual {v4, v5, p0}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 140
    const-string v5, "source"

    invoke-virtual {v4, v5, p2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 142
    new-instance v1, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;

    invoke-direct {v1}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;-><init>()V

    .line 143
    .local v1, "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    const-string v5, "Client Error Type - Failure"

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setErrorName(Ljava/lang/String;)V

    .line 144
    invoke-static {}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageView;->getCurrentPage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setPageName(Ljava/lang/String;)V

    .line 146
    const-string v3, ""

    .line 147
    .local v3, "exceptionName":Ljava/lang/String;
    const-string v2, ""

    .line 148
    .local v2, "exceptionMessage":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 149
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    .line 150
    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 151
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setErrorName(Ljava/lang/String;)V

    .line 152
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setErrorText(Ljava/lang/String;)V

    .line 155
    :cond_0
    const/4 v5, 0x1

    invoke-static {v5, v4}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCCommonDataModel;->getCommonData(ILcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;)Lcom/microsoft/xbox/idp/telemetry/utc/CommonData;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 157
    const-string v5, "Error:%s, exception:%s, additionalInfo:%s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "Client Error Type - Failure"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    const/4 v7, 0x2

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    invoke-static {v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry;->LogEvent(LMicrosoft/Telemetry/Base;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    .end local v1    # "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    .end local v2    # "exceptionMessage":Ljava/lang/String;
    .end local v3    # "exceptionName":Ljava/lang/String;
    .end local v4    # "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    :goto_0
    return-void

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "UTCError.trackFailure"

    invoke-static {v0, v5}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;->trackException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 161
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackGoToEnforcement(Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;Ljava/lang/CharSequence;)V
    .locals 3
    .param p0, "errorScreen"    # Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;
    .param p1, "activityTitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 260
    :try_start_0
    const-string v1, "Errors - View enforcement site"

    invoke-static {v1, p1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :goto_0
    return-void

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "UTCError.trackGoToEnforcement"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;->trackException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 263
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackMSACancel(Ljava/lang/String;ZLcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;)V
    .locals 8
    .param p0, "MSAJobName"    # Ljava/lang/String;
    .param p1, "isSilent"    # Z
    .param p2, "source"    # Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;

    .prologue
    const/4 v7, 0x0

    .line 82
    :try_start_0
    new-instance v2, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;

    invoke-direct {v2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;-><init>()V

    .line 83
    .local v2, "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    const-string v3, "isSilent"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 84
    const-string v3, "job"

    invoke-virtual {v2, v3, p0}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 85
    const-string v3, "source"

    invoke-virtual {v2, v3, p2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 87
    new-instance v1, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;

    invoke-direct {v1}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;-><init>()V

    .line 88
    .local v1, "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    invoke-static {}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageView;->getCurrentPage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setPageName(Ljava/lang/String;)V

    .line 89
    const-string v3, "Client Error Type - MSA canceled"

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setErrorName(Ljava/lang/String;)V

    .line 90
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCCommonDataModel;->getCommonData(ILcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;)Lcom/microsoft/xbox/idp/telemetry/utc/CommonData;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 92
    const-string v3, "Error:%s, additionalInfo:%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "Client Error Type - MSA canceled"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    invoke-static {v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry;->LogEvent(LMicrosoft/Telemetry/Base;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    .end local v1    # "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    .end local v2    # "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "UTCError.trackUserCancel"

    invoke-static {v0, v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;->trackException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 96
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackPageView(Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;Ljava/lang/CharSequence;)V
    .locals 3
    .param p0, "errorScreen"    # Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;
    .param p1, "activityTitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 305
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry;->getErrorScreen(Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageView;->track(Ljava/lang/String;Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    :goto_0
    return-void

    .line 306
    :catch_0
    move-exception v0

    .line 307
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "UTCError.trackPageView"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;->trackException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 308
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackRightButton(Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;Ljava/lang/CharSequence;)V
    .locals 3
    .param p0, "errorScreen"    # Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;
    .param p1, "activityTitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 290
    :try_start_0
    const-string v1, "Errors - Ok"

    invoke-static {v1, p1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_0
    return-void

    .line 291
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "UTCError.trackRightButton"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;->trackException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 293
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackServiceFailure(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/idp/toolkit/HttpError;)V
    .locals 10
    .param p0, "errorName"    # Ljava/lang/String;
    .param p1, "pageName"    # Ljava/lang/String;
    .param p2, "httpError"    # Lcom/microsoft/xbox/idp/toolkit/HttpError;

    .prologue
    const/4 v9, 0x0

    .line 216
    :try_start_0
    new-instance v4, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;

    invoke-direct {v4}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;-><init>()V

    .line 217
    .local v4, "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    const-string v5, "pageName"

    invoke-virtual {v4, v5, p1}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 219
    const-string v3, "Error calling service"

    .line 220
    .local v3, "errorMessage":Ljava/lang/String;
    const-string v2, "0"

    .line 221
    .local v2, "errorCode":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 222
    const-string v5, "%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p2}, Lcom/microsoft/xbox/idp/toolkit/HttpError;->getErrorCode()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 225
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/idp/telemetry/utc/ServiceError;

    invoke-direct {v1}, Lcom/microsoft/xbox/idp/telemetry/utc/ServiceError;-><init>()V

    .line 226
    .local v1, "error":Lcom/microsoft/xbox/idp/telemetry/utc/ServiceError;
    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/idp/telemetry/utc/ServiceError;->setErrorName(Ljava/lang/String;)V

    .line 227
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ServiceError;->setErrorText(Ljava/lang/String;)V

    .line 228
    if-nez p1, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageView;->getCurrentPage()Ljava/lang/String;

    move-result-object p1

    .end local p1    # "pageName":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/idp/telemetry/utc/ServiceError;->setPageName(Ljava/lang/String;)V

    .line 229
    const-string v5, "%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/idp/telemetry/utc/ServiceError;->setErrorCode(Ljava/lang/String;)V

    .line 230
    const/4 v5, 0x1

    invoke-static {v5, v4}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCCommonDataModel;->getCommonData(ILcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;)Lcom/microsoft/xbox/idp/telemetry/utc/CommonData;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/idp/telemetry/utc/ServiceError;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 231
    invoke-static {v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry;->LogEvent(LMicrosoft/Telemetry/Base;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    .end local v1    # "error":Lcom/microsoft/xbox/idp/telemetry/utc/ServiceError;
    .end local v2    # "errorCode":Ljava/lang/String;
    .end local v3    # "errorMessage":Ljava/lang/String;
    .end local v4    # "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    :goto_0
    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackSignedOut(Ljava/lang/String;ZLcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;)V
    .locals 8
    .param p0, "MSAJobName"    # Ljava/lang/String;
    .param p1, "isSilent"    # Z
    .param p2, "source"    # Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;

    .prologue
    const/4 v7, 0x0

    .line 109
    :try_start_0
    new-instance v2, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;

    invoke-direct {v2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;-><init>()V

    .line 110
    .local v2, "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    const-string v3, "isSilent"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 111
    const-string v3, "job"

    invoke-virtual {v2, v3, p0}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    const-string v3, "source"

    invoke-virtual {v2, v3, p2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 114
    new-instance v1, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;

    invoke-direct {v1}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;-><init>()V

    .line 115
    .local v1, "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    invoke-static {}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageView;->getCurrentPage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setPageName(Ljava/lang/String;)V

    .line 116
    const-string v3, "Client Error Type - Signed out"

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setErrorName(Ljava/lang/String;)V

    .line 117
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCCommonDataModel;->getCommonData(ILcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;)Lcom/microsoft/xbox/idp/telemetry/utc/CommonData;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 119
    const-string v3, "Error:%s, additionalInfo:%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "Client Error Type - Signed out"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    invoke-static {v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry;->LogEvent(LMicrosoft/Telemetry/Base;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    .end local v1    # "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    .end local v2    # "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    :goto_0
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "UTCError.trackSignedOut"

    invoke-static {v0, v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;->trackException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 123
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackTryAgain(Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;Ljava/lang/CharSequence;)V
    .locals 3
    .param p0, "errorScreen"    # Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;
    .param p1, "activityTitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 275
    :try_start_0
    const-string v1, "Errors - Try again"

    invoke-static {v1, p1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    :goto_0
    return-void

    .line 276
    :catch_0
    move-exception v0

    .line 277
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "UTCError.trackTryAgain"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;->trackException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 278
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackUINeeded(Ljava/lang/String;ZLcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;)V
    .locals 8
    .param p0, "MSAJobName"    # Ljava/lang/String;
    .param p1, "isSilent"    # Z
    .param p2, "source"    # Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;

    .prologue
    const/4 v7, 0x0

    .line 35
    :try_start_0
    new-instance v2, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;

    invoke-direct {v2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;-><init>()V

    .line 36
    .local v2, "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    const-string v3, "isSilent"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 37
    const-string v3, "job"

    invoke-virtual {v2, v3, p0}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 38
    const-string v3, "source"

    invoke-virtual {v2, v3, p2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 40
    new-instance v1, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;

    invoke-direct {v1}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;-><init>()V

    .line 41
    .local v1, "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    invoke-static {}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageView;->getCurrentPage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setPageName(Ljava/lang/String;)V

    .line 42
    const-string v3, "Client Error Type - UI Needed"

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setErrorName(Ljava/lang/String;)V

    .line 43
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCCommonDataModel;->getCommonData(ILcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;)Lcom/microsoft/xbox/idp/telemetry/utc/CommonData;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 45
    const-string v3, "Error:%s, additionalInfo:%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "Client Error Type - UI Needed"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    invoke-static {v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry;->LogEvent(LMicrosoft/Telemetry/Base;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    .end local v1    # "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    .end local v2    # "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    :goto_0
    return-void

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "UTCError.trackUINeeded"

    invoke-static {v0, v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;->trackException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 49
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackUserCancel(Ljava/lang/String;ZLcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;)V
    .locals 8
    .param p0, "MSAJobName"    # Ljava/lang/String;
    .param p1, "isSilent"    # Z
    .param p2, "source"    # Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry$CallBackSources;

    .prologue
    const/4 v7, 0x0

    .line 62
    :try_start_0
    new-instance v2, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;

    invoke-direct {v2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;-><init>()V

    .line 63
    .local v2, "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    const-string v3, "isSilent"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    const-string v3, "job"

    invoke-virtual {v2, v3, p0}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    const-string v3, "source"

    invoke-virtual {v2, v3, p2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 67
    new-instance v1, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;

    invoke-direct {v1}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;-><init>()V

    .line 68
    .local v1, "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    invoke-static {}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageView;->getCurrentPage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setPageName(Ljava/lang/String;)V

    .line 69
    const-string v3, "Client Error Type - User canceled"

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setErrorName(Ljava/lang/String;)V

    .line 70
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCCommonDataModel;->getCommonData(ILcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;)Lcom/microsoft/xbox/idp/telemetry/utc/CommonData;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 72
    const-string v3, "Error:%s, additionalInfo:%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "Client Error Type - User canceled"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    invoke-static {v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCTelemetry;->LogEvent(LMicrosoft/Telemetry/Base;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    .end local v1    # "error":Lcom/microsoft/xbox/idp/telemetry/utc/ClientError;
    .end local v2    # "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "UTCError.trackUserCancel"

    invoke-static {v0, v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCError;->trackException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 76
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
