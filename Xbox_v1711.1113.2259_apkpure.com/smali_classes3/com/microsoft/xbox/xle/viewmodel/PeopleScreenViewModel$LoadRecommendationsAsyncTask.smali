.class Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PeopleScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadRecommendationsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V
    .locals 0

    .prologue
    .line 1043
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;

    .prologue
    .line 1043
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1046
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1047
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshPeopleHubRecommendationsProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 1075
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->forceLoad:Z

    .line 1076
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadPeopleHubRecommendations(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 1075
    :goto_0
    return-object v0

    .line 1076
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1043
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1069
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1043
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 1052
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1053
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->access$2400(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 1054
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->access$2400(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 1065
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1043
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 1058
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1059
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->access$2502(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Z)Z

    .line 1060
    return-void
.end method
