.class public Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "UploadCustomPicScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private errorDetails:Ljava/lang/String;

.field private isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

.field mediaHubService:Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

.field private selectedImageUri:Landroid/net/Uri;

.field private uploadedImageFile:Ljava/io/File;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 62
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;)V

    .line 64
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 67
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getUploadCustomPicScreenAdapter(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 71
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->onUploadSuccess()V

    return-void
.end method

.method private onUploadSuccess()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 145
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 146
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->updateAdapter()V

    .line 147
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->goBack()V

    .line 80
    return-void
.end method

.method public getAspectRatio()Landroid/util/Pair;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->access$000(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public getCropType()Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->access$100(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;

    move-result-object v0

    return-object v0
.end method

.method public getErrorDetails()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->errorDetails:Ljava/lang/String;

    return-object v0
.end method

.method public getIsUploading()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public getSelectedImageUri()Landroid/net/Uri;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->selectedImageUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 202
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 206
    invoke-super {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onActivityResult(IILandroid/content/Intent;)V

    .line 207
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    .line 208
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 209
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 210
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/theartofdev/edmodo/cropper/CropImage;->getPickImageResultUri(Landroid/content/Context;Landroid/content/Intent;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->selectedImageUri:Landroid/net/Uri;

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    if-nez p2, :cond_2

    .line 212
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->goBack()V

    goto :goto_0

    .line 214
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to select an image. ResultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 216
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070d2d

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->errorDetails:Ljava/lang/String;

    .line 217
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->updateAdapter()V

    goto :goto_0
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->close()V

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->access$400(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->access$400(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->uploadedImageFile:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 162
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 166
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getUploadCustomPicScreenAdapter(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 167
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 191
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->saveToBundle(Landroid/os/Bundle;)V

    .line 193
    return-void
.end method

.method protected onStartOverride()V
    .locals 5

    .prologue
    .line 171
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->selectedImageUri:Landroid/net/Uri;

    if-nez v2, :cond_0

    .line 172
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->access$500(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/microsoft/xbox/toolkit/ActivityResult;

    move-result-object v2

    if-nez v2, :cond_1

    .line 173
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 174
    .local v0, "activity":Landroid/app/Activity;
    const v2, 0x7f070c5c

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/theartofdev/edmodo/cropper/CropImage;->getPickImageChooserIntent(Landroid/content/Context;Ljava/lang/CharSequence;Z)Landroid/content/Intent;

    move-result-object v1

    .line 175
    .local v1, "selectImageIntent":Landroid/content/Intent;
    const/16 v2, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 183
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v1    # "selectImageIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .line 178
    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->access$500(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/microsoft/xbox/toolkit/ActivityResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ActivityResult;->requestCode()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .line 179
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->access$500(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/microsoft/xbox/toolkit/ActivityResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ActivityResult;->resultCode()I

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .line 180
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->access$500(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/microsoft/xbox/toolkit/ActivityResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ActivityResult;->data()Landroid/content/Intent;

    move-result-object v4

    .line 177
    invoke-virtual {p0, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 187
    return-void
.end method

.method protected onUploadFailed(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 150
    sget-object v0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Upload failed"

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 152
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 153
    const v0, 0x7f070d26

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->showError(I)V

    .line 154
    return-void
.end method

.method public showUploadConfirmDialog(Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "uploadRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 107
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070d29

    .line 108
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070d2a

    .line 109
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070d30

    .line 110
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070736

    .line 112
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    move-object v4, p1

    .line 107
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 114
    return-void
.end method

.method public uploadBitmap(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const v3, 0x7f070b6d

    const/4 v2, 0x0

    .line 117
    if-nez p1, :cond_1

    .line 118
    sget-object v0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Got a null bitmap from crop library"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->showError(I)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->createCacheFileForBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->uploadedImageFile:Ljava/io/File;

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->uploadedImageFile:Ljava/io/File;

    if-nez v0, :cond_2

    .line 125
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->showError(I)V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->isUploading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 128
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .line 130
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->access$200(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .line 131
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->access$300(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    move-result-object v1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->uploadedImageFile:Ljava/io/File;

    .line 132
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;->mediaHubService:Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    .line 129
    invoke-static {v2, v3, v1, v4, v5}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->with(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Ljava/lang/String;Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;)Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;

    move-result-object v1

    .line 134
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->upload()Lio/reactivex/Completable;

    move-result-object v1

    .line 135
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    .line 136
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;)Lio/reactivex/functions/Action;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 137
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 128
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0
.end method
