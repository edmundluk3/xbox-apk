.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyName$ActivityFeed;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyName;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActivityFeed"
.end annotation


# static fields
.field public static final ActivityFeedFilterChanges:Ljava/lang/String; = "ActivityFeedFilterChanges"

.field public static final ActivityFeedFilterFull:Ljava/lang/String; = "ActivityFeedFilterFull"

.field public static final Club:Ljava/lang/String; = "ActivityFeedClub"

.field public static final CommentCount:Ljava/lang/String; = "CommentCount"

.field public static final ImpressionContentId:Ljava/lang/String; = "ContentId"

.field public static final ImpressionContentRow:Ljava/lang/String; = "ContentIndex"

.field public static final ImpressionContentSource:Ljava/lang/String; = "ContentSource"

.field public static final ImpressionContentType:Ljava/lang/String; = "ContentType"

.field public static final IsRetry:Ljava/lang/String; = "Retry"

.field public static final LikeCount:Ljava/lang/String; = "LikeCount"

.field public static final ShareCount:Ljava/lang/String; = "ShareCount"

.field public static final Title:Ljava/lang/String; = "ActivityFeedTitle"

.field public static final Titles:Ljava/lang/String; = "ActivityFeedTitles"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
