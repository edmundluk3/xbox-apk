.class public Lcom/microsoft/cll/android/SerializedEvent;
.super Ljava/lang/Object;
.source "SerializedEvent.java"


# instance fields
.field private deviceId:Ljava/lang/String;

.field private latency:Lcom/microsoft/cll/android/Cll$EventLatency;

.field private persistence:Lcom/microsoft/cll/android/Cll$EventPersistence;

.field private sampleRate:D

.field private serializedData:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/cll/android/SerializedEvent;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getLatency()Lcom/microsoft/cll/android/Cll$EventLatency;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/cll/android/SerializedEvent;->latency:Lcom/microsoft/cll/android/Cll$EventLatency;

    return-object v0
.end method

.method public getPersistence()Lcom/microsoft/cll/android/Cll$EventPersistence;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/cll/android/SerializedEvent;->persistence:Lcom/microsoft/cll/android/Cll$EventPersistence;

    return-object v0
.end method

.method public getSampleRate()D
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/microsoft/cll/android/SerializedEvent;->sampleRate:D

    return-wide v0
.end method

.method public getSerializedData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/microsoft/cll/android/SerializedEvent;->serializedData:Ljava/lang/String;

    return-object v0
.end method

.method public setDeviceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/microsoft/cll/android/SerializedEvent;->deviceId:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setLatency(Lcom/microsoft/cll/android/Cll$EventLatency;)V
    .locals 0
    .param p1, "latency"    # Lcom/microsoft/cll/android/Cll$EventLatency;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/microsoft/cll/android/SerializedEvent;->latency:Lcom/microsoft/cll/android/Cll$EventLatency;

    .line 28
    return-void
.end method

.method public setPersistence(Lcom/microsoft/cll/android/Cll$EventPersistence;)V
    .locals 0
    .param p1, "persistence"    # Lcom/microsoft/cll/android/Cll$EventPersistence;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/microsoft/cll/android/SerializedEvent;->persistence:Lcom/microsoft/cll/android/Cll$EventPersistence;

    .line 36
    return-void
.end method

.method public setSampleRate(D)V
    .locals 1
    .param p1, "sampleRate"    # D

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/microsoft/cll/android/SerializedEvent;->sampleRate:D

    .line 44
    return-void
.end method

.method public setSerializedData(Ljava/lang/String;)V
    .locals 0
    .param p1, "serializedData"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/microsoft/cll/android/SerializedEvent;->serializedData:Ljava/lang/String;

    .line 20
    return-void
.end method
