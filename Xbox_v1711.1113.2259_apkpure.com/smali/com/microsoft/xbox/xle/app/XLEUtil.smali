.class public Lcom/microsoft/xbox/xle/app/XLEUtil;
.super Ljava/lang/Object;
.source "XLEUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/XLEUtil$TypefaceSpan;,
        Lcom/microsoft/xbox/xle/app/XLEUtil$BackgroundSetter;,
        Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;,
        Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;
    }
.end annotation


# static fields
.field public static final COMMON_TYPEFACE:Landroid/graphics/Typeface;

.field public static final DEFAULT_RESOURCE_MAX_COUNT:I = 0x6

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 92
    const-class v0, Lcom/microsoft/xbox/xle/app/XLEUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/XLEUtil;->TAG:Ljava/lang/String;

    .line 95
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts/SegoeWP.ttf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addTestHook(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "originalRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 995
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil$$Lambda$2;->lambdaFactory$(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method

.method public static applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;
    .locals 5
    .param p0, "string"    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1196
    if-eqz p0, :cond_0

    .line 1197
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1198
    .local v0, "spannableString":Landroid/text/SpannableString;
    new-instance v1, Lcom/microsoft/xbox/xle/app/XLEUtil$TypefaceSpan;

    sget-object v2, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil$TypefaceSpan;-><init>(Landroid/graphics/Typeface;)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1201
    .end local v0    # "spannableString":Landroid/text/SpannableString;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static calculateGridColumnCount(Landroid/content/Context;III)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "columnMargin"    # I
    .param p2, "columnWidth"    # I
    .param p3, "adjustment"    # I

    .prologue
    .line 1262
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1263
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 1264
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v3, v4

    int-to-float v4, p3

    sub-float v2, v3, v4

    .line 1265
    .local v2, "dpWidth":F
    invoke-static {p1, p0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->convertPixelsToDp(ILandroid/content/Context;)I

    move-result v0

    .line 1266
    .local v0, "colummMarginInDp":I
    add-int v3, p2, v0

    int-to-float v3, v3

    div-float v3, v2, v3

    float-to-int v3, v3

    return v3
.end method

.method public static convertFromMinuteToHours(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "minutes"    # Ljava/lang/String;

    .prologue
    const-wide/16 v12, 0x0

    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    const-wide/16 v8, 0x1

    .line 575
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 598
    :cond_0
    :goto_0
    return-object v4

    .line 579
    :cond_1
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 581
    .local v1, "mins":Ljava/lang/Long;
    :try_start_0
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    double-to-long v6, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 587
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v5, v6, v12

    if-lez v5, :cond_0

    .line 591
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v2

    .line 592
    .local v2, "hours":J
    cmp-long v4, v2, v8

    if-nez v4, :cond_2

    .line 593
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07062d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 582
    .end local v2    # "hours":J
    :catch_0
    move-exception v0

    .line 583
    .local v0, "ex":Ljava/lang/NumberFormatException;
    sget-object v5, Lcom/microsoft/xbox/xle/app/XLEUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "convertFromMinuteToHours: Cannot convert string "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 584
    const-string v5, "Bad number, check inputs"

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_1

    .line 594
    .end local v0    # "ex":Ljava/lang/NumberFormatException;
    .restart local v2    # "hours":J
    :cond_2
    cmp-long v4, v2, v8

    if-lez v4, :cond_3

    .line 595
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f07061e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 598
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-nez v4, :cond_4

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07062e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070625

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v11, [Ljava/lang/Object;

    aput-object v1, v6, v10

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public static convertPixelsToDp(ILandroid/content/Context;)I
    .locals 5
    .param p0, "px"    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Dimension;
        unit = 0x0
    .end annotation

    .prologue
    .line 1247
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1248
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1249
    .local v1, "resources":Landroid/content/res/Resources;
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1250
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    int-to-float v2, p0

    iget v3, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v3, v3

    const/high16 v4, 0x43200000    # 160.0f

    div-float/2addr v3, v4

    div-float/2addr v2, v3

    float-to-int v2, v2

    return v2
.end method

.method public static createCacheFileForBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)Ljava/io/File;
    .locals 7
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "filename"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1273
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "yyyy_MM_dd_HH_mm_ss_SSS"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "filename":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .line 1275
    .restart local p1    # "filename":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    .line 1276
    .local v2, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v2, :cond_0

    .line 1278
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-direct {v1, v4, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1280
    .local v1, "file":Ljava/io/File;
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1282
    .local v3, "outStream":Ljava/io/OutputStream;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1283
    invoke-virtual {v3}, Ljava/io/OutputStream;->flush()V

    .line 1284
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1295
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "outStream":Ljava/io/OutputStream;
    :goto_0
    return-object v1

    .line 1288
    :catch_0
    move-exception v0

    .line 1289
    .local v0, "e":Ljava/io/IOException;
    sget-object v4, Lcom/microsoft/xbox/xle/app/XLEUtil;->TAG:Ljava/lang/String;

    const-string v5, "Failed to create a cache file for bitmap"

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1295
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1292
    :cond_0
    sget-object v4, Lcom/microsoft/xbox/xle/app/XLEUtil;->TAG:Ljava/lang/String;

    const-string v5, "createCacheFileForBitmap: MainActivity is null"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static dateToDurationSinceNow(Ljava/util/Date;)Ljava/lang/String;
    .locals 1
    .param p0, "date"    # Ljava/util/Date;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 474
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToDurationSinceNow(Ljava/util/Date;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static dateToDurationSinceNow(Ljava/util/Date;Z)Ljava/lang/String;
    .locals 12
    .param p0, "date"    # Ljava/util/Date;
    .param p1, "forNarrator"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 448
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    sub-long v6, v8, v10

    .line 450
    .local v6, "timeDiff":J
    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-gez v1, :cond_0

    .line 451
    const-string v1, ""

    .line 468
    :goto_0
    return-object v1

    .line 452
    :cond_0
    const-wide/32 v8, 0x36ee80

    cmp-long v1, v6, v8

    if-gez v1, :cond_3

    .line 453
    const-wide/32 v8, 0xea60

    div-long v4, v6, v8

    .line 454
    .local v4, "minDiff":J
    const-wide/16 v8, 0x1

    cmp-long v1, v4, v8

    if-gtz v1, :cond_1

    .line 455
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070628

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 457
    :cond_1
    if-eqz p1, :cond_2

    .line 458
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f070d68

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v1, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 460
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f07062b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v1, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 463
    .end local v4    # "minDiff":J
    :cond_3
    const-wide/32 v8, 0x5265c00

    cmp-long v1, v6, v8

    if-gez v1, :cond_4

    .line 464
    const-wide/32 v8, 0x36ee80

    div-long v2, v6, v8

    .line 465
    .local v2, "hourDiff":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f07062a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v1, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 467
    .end local v2    # "hourDiff":J
    :cond_4
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 468
    .local v0, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public static dateToTimeRemaining(Ljava/util/Date;)Ljava/lang/String;
    .locals 12
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 603
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 604
    .local v0, "currentDate":Ljava/util/Date;
    invoke-virtual {p0, v0}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v1

    if-lez v1, :cond_0

    .line 605
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 606
    .local v2, "timeDiff":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v4, "%02d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v2, v3}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 607
    invoke-virtual {v7, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v8

    sget-object v7, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v2, v3}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v10

    invoke-virtual {v7, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v10

    sub-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 608
    invoke-virtual {v7, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v10

    invoke-virtual {v7, v10, v11}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v10

    sub-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    .line 606
    invoke-static {v1, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 610
    .end local v2    # "timeDiff":J
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f071092

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static dateToTimeRemainingShort(Ljava/util/Date;)Ljava/lang/String;
    .locals 14
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 615
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 616
    .local v0, "currentDate":Ljava/util/Date;
    invoke-virtual {p0, v0}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v1

    if-lez v1, :cond_3

    .line 617
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    sub-long v8, v10, v12

    .line 618
    .local v8, "timeDiff":J
    const-wide/32 v10, 0x36ee80

    cmp-long v1, v8, v10

    if-gez v1, :cond_0

    .line 619
    const-wide/32 v10, 0xea60

    div-long v6, v8, v10

    .line 620
    .local v6, "minDiff":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070625

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 633
    .end local v6    # "minDiff":J
    .end local v8    # "timeDiff":J
    :goto_0
    return-object v1

    .line 621
    .restart local v8    # "timeDiff":J
    :cond_0
    const-wide/32 v10, 0x5265c00

    cmp-long v1, v8, v10

    if-gez v1, :cond_1

    .line 622
    const-wide/32 v10, 0x36ee80

    div-long v4, v8, v10

    .line 623
    .local v4, "hourDiff":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f07061e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 625
    .end local v4    # "hourDiff":J
    :cond_1
    const-wide/32 v10, 0x5265c00

    div-long v2, v8, v10

    .line 626
    .local v2, "dayDiff":J
    const-wide/16 v10, 0x1

    cmp-long v1, v2, v10

    if-nez v1, :cond_2

    .line 627
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070142

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 629
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070140

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 633
    .end local v2    # "dayDiff":J
    .end local v8    # "timeDiff":J
    :cond_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070140

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static dismissKeyboard()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 939
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 940
    .local v0, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_0

    .line 941
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 945
    :goto_0
    return-void

    .line 943
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/XLEUtil;->TAG:Ljava/lang/String;

    const-string v2, "dismissKeyboard: MainActivity is null"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static dismissKeyboard(Landroid/view/View;)V
    .locals 3
    .param p0, "inputView"    # Landroid/view/View;

    .prologue
    .line 953
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 954
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 955
    return-void
.end method

.method public static findChildPosition(Landroid/view/View;)I
    .locals 6
    .param p0, "child"    # Landroid/view/View;

    .prologue
    .line 1063
    const/4 v3, -0x1

    .line 1064
    .local v3, "position":I
    if-eqz p0, :cond_0

    .line 1065
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 1066
    .local v2, "parent":Landroid/view/ViewParent;
    instance-of v5, v2, Landroid/view/ViewGroup;

    if-eqz v5, :cond_0

    move-object v0, v2

    .line 1067
    check-cast v0, Landroid/view/ViewGroup;

    .line 1068
    .local v0, "group":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 1069
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1070
    .local v4, "v":Landroid/view/View;
    if-ne v4, p0, :cond_1

    .line 1071
    move v3, v1

    .line 1077
    .end local v0    # "group":Landroid/view/ViewGroup;
    .end local v1    # "i":I
    .end local v2    # "parent":Landroid/view/ViewParent;
    .end local v4    # "v":Landroid/view/View;
    :cond_0
    return v3

    .line 1068
    .restart local v0    # "group":Landroid/view/ViewGroup;
    .restart local v1    # "i":I
    .restart local v2    # "parent":Landroid/view/ViewParent;
    .restart local v4    # "v":Landroid/view/View;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static forceUpdate()V
    .locals 6

    .prologue
    .line 1004
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070ca4

    .line 1005
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070ca3

    .line 1006
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070ca5

    .line 1007
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XLEUtil$$Lambda$3;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070d8b

    .line 1009
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XLEUtil$$Lambda$4;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v5

    .line 1004
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1011
    return-void
.end method

.method public static getCheckboxMePreferedColorStateList()Landroid/content/res/ColorStateList;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1308
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMePreferredColor()I

    move-result v0

    .line 1310
    .local v0, "mePreferredColor":I
    new-instance v1, Landroid/content/res/ColorStateList;

    new-array v2, v7, [[I

    new-array v3, v6, [I

    const v4, 0x10100a0

    aput v4, v3, v5

    aput-object v3, v2, v5

    new-array v3, v5, [I

    aput-object v3, v2, v6

    new-array v3, v7, [I

    aput v0, v3, v5

    const v4, -0x777778

    aput v4, v3, v6

    invoke-direct {v1, v2, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v1
.end method

.method public static getCircleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "src"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v9, 0x0

    .line 1340
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 1341
    .local v4, "width":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 1342
    .local v1, "height":I
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1344
    .local v2, "outputBitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 1345
    .local v3, "path":Landroid/graphics/Path;
    div-int/lit8 v5, v4, 0x2

    int-to-float v5, v5

    div-int/lit8 v6, v1, 0x2

    int-to-float v6, v6

    div-int/lit8 v7, v1, 0x2

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    int-to-float v7, v7

    sget-object v8, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 1347
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1348
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 1349
    const/4 v5, 0x0

    invoke-virtual {v0, p0, v9, v9, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1350
    return-object v2
.end method

.method public static getDateStringAsMonthDateYear(Ljava/util/Date;)Ljava/lang/String;
    .locals 3
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 704
    if-eqz p0, :cond_1

    .line 705
    invoke-virtual {p0}, Ljava/util/Date;->getYear()I

    move-result v1

    add-int/lit16 v1, v1, 0x76c

    const/16 v2, 0xaef

    if-lt v1, v2, :cond_0

    .line 706
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0703e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 712
    :goto_0
    return-object v1

    .line 708
    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    .line 709
    .local v0, "formatter":Ljava/text/DateFormat;
    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 712
    .end local v0    # "formatter":Ljava/text/DateFormat;
    :cond_1
    const-string v1, ""

    goto :goto_0
.end method

.method public static getDurationSinceNowUptoDayOrShortDate(Ljava/util/Date;)Ljava/lang/String;
    .locals 4
    .param p0, "date"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 643
    if-nez p0, :cond_0

    .line 644
    const-string v0, ""

    .line 648
    :goto_0
    return-object v0

    .line 645
    :cond_0
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 646
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToDurationSinceNow(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 648
    :cond_1
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getLocalizedDateString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getGameClipForOwnedActivityFeeedItemContent(Lcom/microsoft/xbox/service/model/entity/Entity;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .locals 3
    .param p0, "entity"    # Lcom/microsoft/xbox/service/model/entity/Entity;
    .param p1, "ownerXuid"    # Ljava/lang/String;

    .prologue
    .line 1092
    const/4 v0, 0x0

    .line 1093
    .local v0, "gameClip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getGameClipModelForOwnedActivityFeeedItemContent(Lcom/microsoft/xbox/service/model/entity/Entity;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    move-result-object v2

    .line 1094
    .local v2, "model":Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    if-eqz v2, :cond_0

    .line 1095
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->getGameClip()Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    move-result-object v1

    .line 1096
    .local v1, "gameClipResult":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    if-eqz v1, :cond_0

    .line 1097
    iget-object v0, v1, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 1100
    .end local v1    # "gameClipResult":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    :cond_0
    return-object v0
.end method

.method public static getGameClipModelForOwnedActivityFeeedItemContent(Lcom/microsoft/xbox/service/model/entity/Entity;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    .locals 6
    .param p0, "entity"    # Lcom/microsoft/xbox/service/model/entity/Entity;
    .param p1, "ownerXuid"    # Ljava/lang/String;

    .prologue
    .line 1081
    const/4 v1, 0x0

    .line 1082
    .local v1, "model":Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/entity/Entity;->getType()Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACTIVITY_FEED_ITEM:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    if-ne v2, v3, :cond_0

    .line 1083
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/entity/Entity;->getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    .line 1084
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->GameDVR:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1085
    new-instance v2, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getXuidForLoadingGameClip()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipScid:Ljava/lang/String;

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipId:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->getInstance(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    move-result-object v1

    .line 1088
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_0
    return-object v1
.end method

.method public static getLocalizedDateString(Ljava/util/Date;)Ljava/lang/String;
    .locals 5
    .param p0, "date"    # Ljava/util/Date;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 669
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const v4, 0x20010

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 672
    :goto_0
    return-object v1

    .line 670
    :catch_0
    move-exception v0

    .line 671
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "JavaUtil"

    const-string v2, "getLocalizedDateString: "

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 672
    const-string v1, ""

    goto :goto_0
.end method

.method public static getLocalizedDateStringValidated(Ljava/util/Date;)Ljava/lang/String;
    .locals 1
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 692
    sget-object v0, Lcom/microsoft/xbox/toolkit/JavaUtil;->MIN_DATE:Ljava/util/Date;

    invoke-virtual {p0, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {p0, v0}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 693
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getLocalizedDateString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 695
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getLocalizedTimeString(Ljava/util/Date;)Ljava/lang/String;
    .locals 5
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 684
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 687
    :goto_0
    return-object v1

    .line 685
    :catch_0
    move-exception v0

    .line 686
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "JavaUtil"

    const-string v2, "getLocalizedTimeString: "

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 687
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getMediaItemDefaultAspectXY(I)[I
    .locals 1
    .param p0, "mediaType"    # I

    .prologue
    const/4 v0, 0x2

    .line 353
    sparse-switch p0, :sswitch_data_0

    .line 410
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    :goto_0
    return-object v0

    .line 357
    :sswitch_0
    new-array v0, v0, [I

    fill-array-data v0, :array_1

    goto :goto_0

    .line 388
    :sswitch_1
    new-array v0, v0, [I

    fill-array-data v0, :array_2

    goto :goto_0

    .line 392
    :sswitch_2
    new-array v0, v0, [I

    fill-array-data v0, :array_3

    goto :goto_0

    .line 399
    :sswitch_3
    new-array v0, v0, [I

    fill-array-data v0, :array_4

    goto :goto_0

    .line 407
    :sswitch_4
    new-array v0, v0, [I

    fill-array-data v0, :array_5

    goto :goto_0

    .line 353
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_1
        0x12 -> :sswitch_1
        0x13 -> :sswitch_1
        0x14 -> :sswitch_1
        0x15 -> :sswitch_1
        0x16 -> :sswitch_1
        0x17 -> :sswitch_1
        0x18 -> :sswitch_1
        0x1e -> :sswitch_1
        0x22 -> :sswitch_1
        0x24 -> :sswitch_1
        0x25 -> :sswitch_1
        0x2e -> :sswitch_1
        0x2f -> :sswitch_1
        0x39 -> :sswitch_1
        0x3a -> :sswitch_1
        0x3b -> :sswitch_1
        0x3c -> :sswitch_1
        0x3d -> :sswitch_0
        0x3e -> :sswitch_1
        0x3f -> :sswitch_1
        0x40 -> :sswitch_1
        0x41 -> :sswitch_1
        0x42 -> :sswitch_1
        0x3e8 -> :sswitch_2
        0x3ea -> :sswitch_3
        0x3eb -> :sswitch_3
        0x3ec -> :sswitch_3
        0x3ed -> :sswitch_3
        0x3ee -> :sswitch_4
        0x3ef -> :sswitch_4
        0x3f0 -> :sswitch_4
        0x3f1 -> :sswitch_4
        0x2328 -> :sswitch_0
        0x2329 -> :sswitch_1
        0x232a -> :sswitch_1
        0x232b -> :sswitch_1
        0x232c -> :sswitch_1
    .end sparse-switch

    .line 410
    :array_0
    .array-data 4
        0x4
        0x3
    .end array-data

    .line 357
    :array_1
    .array-data 4
        0x64
        0x49
    .end array-data

    .line 388
    :array_2
    .array-data 4
        0x64
        0x43
    .end array-data

    .line 392
    :array_3
    .array-data 4
        0x64
        0x49
    .end array-data

    .line 399
    :array_4
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 407
    :array_5
    .array-data 4
        0x1
        0x1
    .end array-data
.end method

.method public static getMediaItemDefaultIconStringRid(I)I
    .locals 1
    .param p0, "mediaType"    # I

    .prologue
    const v0, 0x7f070eaf

    .line 168
    sparse-switch p0, :sswitch_data_0

    .line 225
    :goto_0
    :sswitch_0
    return v0

    .line 203
    :sswitch_1
    const v0, 0x7f070f46

    goto :goto_0

    .line 207
    :sswitch_2
    const v0, 0x7f070f93

    goto :goto_0

    .line 214
    :sswitch_3
    const v0, 0x7f071044

    goto :goto_0

    .line 222
    :sswitch_4
    const v0, 0x7f070f9b

    goto :goto_0

    .line 168
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_1
        0x12 -> :sswitch_1
        0x13 -> :sswitch_1
        0x14 -> :sswitch_1
        0x15 -> :sswitch_1
        0x16 -> :sswitch_1
        0x17 -> :sswitch_1
        0x18 -> :sswitch_1
        0x1e -> :sswitch_1
        0x22 -> :sswitch_1
        0x24 -> :sswitch_1
        0x25 -> :sswitch_1
        0x2e -> :sswitch_1
        0x2f -> :sswitch_1
        0x39 -> :sswitch_1
        0x3a -> :sswitch_1
        0x3b -> :sswitch_1
        0x3c -> :sswitch_1
        0x3d -> :sswitch_0
        0x3e -> :sswitch_1
        0x3f -> :sswitch_1
        0x40 -> :sswitch_1
        0x41 -> :sswitch_1
        0x42 -> :sswitch_1
        0x3e8 -> :sswitch_2
        0x3ea -> :sswitch_3
        0x3eb -> :sswitch_3
        0x3ec -> :sswitch_3
        0x3ed -> :sswitch_3
        0x3ee -> :sswitch_4
        0x3ef -> :sswitch_4
        0x3f0 -> :sswitch_4
        0x3f1 -> :sswitch_4
        0x2328 -> :sswitch_0
        0x2329 -> :sswitch_1
        0x232a -> :sswitch_1
        0x232b -> :sswitch_1
        0x232c -> :sswitch_1
    .end sparse-switch
.end method

.method public static getMediaItemDefaultRid(I)I
    .locals 1
    .param p0, "mediaType"    # I

    .prologue
    .line 102
    sparse-switch p0, :sswitch_data_0

    .line 159
    const v0, 0x7f0201fa

    :goto_0
    return v0

    .line 106
    :sswitch_0
    const v0, 0x7f020064

    goto :goto_0

    .line 137
    :sswitch_1
    const v0, 0x7f020124

    goto :goto_0

    .line 141
    :sswitch_2
    const v0, 0x7f020183

    goto :goto_0

    .line 148
    :sswitch_3
    const v0, 0x7f0201f7

    goto :goto_0

    .line 156
    :sswitch_4
    const v0, 0x7f020189

    goto :goto_0

    .line 102
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_1
        0x12 -> :sswitch_1
        0x13 -> :sswitch_1
        0x14 -> :sswitch_1
        0x15 -> :sswitch_1
        0x16 -> :sswitch_1
        0x17 -> :sswitch_1
        0x18 -> :sswitch_1
        0x1e -> :sswitch_1
        0x22 -> :sswitch_1
        0x24 -> :sswitch_1
        0x25 -> :sswitch_1
        0x2e -> :sswitch_1
        0x2f -> :sswitch_1
        0x39 -> :sswitch_1
        0x3a -> :sswitch_1
        0x3b -> :sswitch_1
        0x3c -> :sswitch_1
        0x3d -> :sswitch_0
        0x3e -> :sswitch_1
        0x3f -> :sswitch_1
        0x40 -> :sswitch_1
        0x41 -> :sswitch_1
        0x42 -> :sswitch_1
        0x3e8 -> :sswitch_2
        0x3ea -> :sswitch_3
        0x3eb -> :sswitch_3
        0x3ec -> :sswitch_3
        0x3ed -> :sswitch_3
        0x3ee -> :sswitch_4
        0x3ef -> :sswitch_4
        0x3f0 -> :sswitch_4
        0x3f1 -> :sswitch_4
        0x2328 -> :sswitch_0
        0x2329 -> :sswitch_1
        0x232a -> :sswitch_1
        0x232b -> :sswitch_1
        0x232c -> :sswitch_1
    .end sparse-switch
.end method

.method public static getMediaItemRelatedAspectXY(I)[I
    .locals 1
    .param p0, "mediaType"    # I

    .prologue
    const/4 v0, 0x2

    .line 313
    sparse-switch p0, :sswitch_data_0

    .line 344
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    :goto_0
    return-object v0

    .line 317
    :sswitch_0
    new-array v0, v0, [I

    fill-array-data v0, :array_1

    goto :goto_0

    .line 322
    :sswitch_1
    new-array v0, v0, [I

    fill-array-data v0, :array_2

    goto :goto_0

    .line 326
    :sswitch_2
    new-array v0, v0, [I

    fill-array-data v0, :array_3

    goto :goto_0

    .line 333
    :sswitch_3
    new-array v0, v0, [I

    fill-array-data v0, :array_4

    goto :goto_0

    .line 341
    :sswitch_4
    new-array v0, v0, [I

    fill-array-data v0, :array_5

    goto :goto_0

    .line 313
    :sswitch_data_0
    .sparse-switch
        0x3d -> :sswitch_0
        0x3e8 -> :sswitch_2
        0x3ea -> :sswitch_3
        0x3eb -> :sswitch_3
        0x3ec -> :sswitch_3
        0x3ed -> :sswitch_3
        0x3ee -> :sswitch_4
        0x3ef -> :sswitch_4
        0x3f0 -> :sswitch_4
        0x3f1 -> :sswitch_4
        0x2328 -> :sswitch_0
        0x2329 -> :sswitch_1
        0x232a -> :sswitch_1
    .end sparse-switch

    .line 344
    :array_0
    .array-data 4
        0x4
        0x3
    .end array-data

    .line 317
    :array_1
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 322
    :array_2
    .array-data 4
        0x64
        0x49
    .end array-data

    .line 326
    :array_3
    .array-data 4
        0x64
        0x49
    .end array-data

    .line 333
    :array_4
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 341
    :array_5
    .array-data 4
        0x1
        0x1
    .end array-data
.end method

.method public static getMediaItemTypeName(I)Ljava/lang/String;
    .locals 2
    .param p0, "mediaType"    # I

    .prologue
    .line 236
    sparse-switch p0, :sswitch_data_0

    .line 298
    const/4 v0, -0x1

    .line 301
    .local v0, "stringResource":I
    :goto_0
    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 302
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 304
    :goto_1
    return-object v1

    .line 240
    .end local v0    # "stringResource":I
    :sswitch_0
    const v0, 0x7f070407

    .line 241
    .restart local v0    # "stringResource":I
    goto :goto_0

    .line 272
    .end local v0    # "stringResource":I
    :sswitch_1
    const v0, 0x7f070411

    .line 273
    .restart local v0    # "stringResource":I
    goto :goto_0

    .line 277
    .end local v0    # "stringResource":I
    :sswitch_2
    const v0, 0x7f070413

    .line 278
    .restart local v0    # "stringResource":I
    goto :goto_0

    .line 285
    .end local v0    # "stringResource":I
    :sswitch_3
    const v0, 0x7f07041b

    .line 286
    .restart local v0    # "stringResource":I
    goto :goto_0

    .line 294
    .end local v0    # "stringResource":I
    :sswitch_4
    const v0, 0x7f070405

    .line 295
    .restart local v0    # "stringResource":I
    goto :goto_0

    .line 304
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 236
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_1
        0x12 -> :sswitch_1
        0x13 -> :sswitch_1
        0x14 -> :sswitch_1
        0x15 -> :sswitch_1
        0x16 -> :sswitch_1
        0x17 -> :sswitch_1
        0x18 -> :sswitch_1
        0x1e -> :sswitch_1
        0x22 -> :sswitch_1
        0x24 -> :sswitch_1
        0x25 -> :sswitch_1
        0x2e -> :sswitch_1
        0x2f -> :sswitch_1
        0x39 -> :sswitch_1
        0x3a -> :sswitch_1
        0x3b -> :sswitch_1
        0x3c -> :sswitch_1
        0x3d -> :sswitch_0
        0x3e -> :sswitch_1
        0x3f -> :sswitch_1
        0x40 -> :sswitch_1
        0x41 -> :sswitch_1
        0x42 -> :sswitch_1
        0x3e8 -> :sswitch_2
        0x3ea -> :sswitch_3
        0x3eb -> :sswitch_3
        0x3ec -> :sswitch_3
        0x3ed -> :sswitch_3
        0x3ee -> :sswitch_4
        0x3ef -> :sswitch_4
        0x3f0 -> :sswitch_4
        0x3f1 -> :sswitch_4
        0x2328 -> :sswitch_0
        0x2329 -> :sswitch_1
        0x232a -> :sswitch_1
        0x232b -> :sswitch_1
        0x232c -> :sswitch_1
    .end sparse-switch
.end method

.method public static getStringForLocale(ILjava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "resId"    # I
    .param p1, "locale"    # Ljava/lang/String;

    .prologue
    .line 416
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    .line 417
    .local v3, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v3, :cond_0

    .line 418
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 419
    .local v1, "conf":Landroid/content/res/Configuration;
    iget-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 420
    .local v0, "backupLocale":Ljava/util/Locale;
    new-instance v6, Ljava/util/Locale;

    invoke-direct {v6, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    iput-object v6, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 421
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    .line 422
    .local v4, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 423
    new-instance v5, Landroid/content/res/Resources;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v6

    invoke-direct {v5, v6, v4, v1}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    .line 424
    .local v5, "resources":Landroid/content/res/Resources;
    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 425
    .local v2, "localeString":Ljava/lang/String;
    iput-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 426
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v1, v4}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 431
    .end local v0    # "backupLocale":Ljava/util/Locale;
    .end local v1    # "conf":Landroid/content/res/Configuration;
    .end local v2    # "localeString":Ljava/lang/String;
    .end local v4    # "metrics":Landroid/util/DisplayMetrics;
    .end local v5    # "resources":Landroid/content/res/Resources;
    :goto_0
    return-object v2

    .line 430
    :cond_0
    sget-object v6, Lcom/microsoft/xbox/xle/app/XLEUtil;->TAG:Ljava/lang/String;

    const-string v7, "getStringForLocale: MainActivity is null"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    const-string v2, ""

    goto :goto_0
.end method

.method public static getTimeStringMMSS(J)Ljava/lang/String;
    .locals 2
    .param p0, "timeInSeconds"    # J

    .prologue
    .line 700
    invoke-static {p0, p1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getValidAchievementUnlockedDateString(Ljava/util/Date;)Ljava/lang/String;
    .locals 4
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 436
    const/4 v1, 0x0

    .line 439
    .local v1, "result":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/Date;->getYear()I

    move-result v2

    add-int/lit16 v2, v2, 0x76c

    const/16 v3, 0x7d0

    if-le v2, v3, :cond_0

    .line 440
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v2}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 441
    .local v0, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 443
    .end local v0    # "dateFormat":Ljava/text/DateFormat;
    :cond_0
    if-nez v1, :cond_1

    const-string v1, ""

    .end local v1    # "result":Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method public static getYearString(Ljava/util/Date;Ljava/text/SimpleDateFormat;)Ljava/lang/String;
    .locals 3
    .param p0, "date"    # Ljava/util/Date;
    .param p1, "yearFormatter"    # Ljava/text/SimpleDateFormat;

    .prologue
    .line 716
    if-eqz p0, :cond_2

    .line 717
    invoke-virtual {p0}, Ljava/util/Date;->getYear()I

    move-result v0

    add-int/lit16 v0, v0, 0x76c

    const/16 v1, 0xaef

    if-lt v0, v1, :cond_0

    .line 718
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0703e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 727
    :goto_0
    return-object v0

    .line 720
    :cond_0
    if-eqz p1, :cond_1

    .line 721
    invoke-virtual {p1, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 724
    :cond_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 727
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public static isActivityAlive(Landroid/app/Activity;)Z
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 1331
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isScrolledToBottom(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 3
    .param p0, "listView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 1180
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v0

    .line 1181
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 1182
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result v1

    .line 1183
    .local v1, "lastVisibleItemPosition":I
    add-int/lit8 v2, v0, -0x1

    if-ge v1, v2, :cond_0

    .line 1184
    const/4 v2, 0x0

    .line 1188
    .end local v1    # "lastVisibleItemPosition":I
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isServiceRunning(Landroid/content/Context;Ljava/lang/Class;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 1358
    .local p1, "serviceClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v2, "activity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1360
    .local v0, "manager":Landroid/app/ActivityManager;
    const v2, 0x7fffffff

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 1361
    .local v1, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1362
    const/4 v2, 0x1

    .line 1366
    .end local v1    # "service":Landroid/app/ActivityManager$RunningServiceInfo;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isTouchExplorationEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1299
    const-string v1, "accessibility"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 1300
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isTouchPointInsideView(FFLandroid/view/View;)Z
    .locals 7
    .param p0, "touchRawX"    # F
    .param p1, "touchRawY"    # F
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 653
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 654
    .local v0, "coordinates":[I
    invoke-virtual {p2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 656
    new-instance v1, Landroid/graphics/Rect;

    aget v2, v0, v4

    aget v3, v0, v6

    aget v4, v0, v4

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    aget v5, v0, v6

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 657
    .local v1, "rect":Landroid/graphics/Rect;
    float-to-int v2, p0

    float-to-int v3, p1

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    return v2
.end method

.method static synthetic lambda$addTestHook$1(Ljava/lang/Runnable;)V
    .locals 2
    .param p0, "originalRunnable"    # Ljava/lang/Runnable;

    .prologue
    const/4 v1, 0x0

    .line 996
    if-eqz p0, :cond_0

    .line 997
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 999
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-interface {v0, v1, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->setCurrentDialog(Ljava/lang/String;Landroid/app/Dialog;)V

    .line 1000
    return-void
.end method

.method static synthetic lambda$forceUpdate$2()V
    .locals 1

    .prologue
    .line 1010
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->killApp()V

    return-void
.end method

.method static synthetic lambda$showKeyboard$0(Landroid/view/View;)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 923
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 924
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 925
    new-instance v1, Lcom/microsoft/xbox/xle/app/XLEUtil$2;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil$2;-><init>()V

    invoke-static {v1}, Lcom/microsoft/xle/test/interop/TestInterop;->setDismissSoftKeyboard(Lcom/microsoft/xle/test/interop/delegates/Action;)V

    .line 931
    return-void
.end method

.method public static launchMarketplace()V
    .locals 6

    .prologue
    .line 1014
    sget-object v3, Lcom/microsoft/xbox/xle/app/XLEUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "go to marketplace for update "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getMarketUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getMarketUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1016
    .local v2, "gotoMarket":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1019
    :try_start_0
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/XLEApplication;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1034
    :goto_0
    return-void

    .line 1020
    :catch_0
    move-exception v1

    .line 1030
    .local v1, "ex":Landroid/content/ActivityNotFoundException;
    const-string v0, "Failed launch marketplace intent"

    .line 1031
    .local v0, "errorCode":Ljava/lang/String;
    const-string v3, "Failed launch marketplace intent"

    invoke-static {v3, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1032
    sget-object v3, Lcom/microsoft/xbox/xle/app/XLEUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static removeOnClickListenerIfNotNull(Landroid/view/View;)V
    .locals 1
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 1139
    if-eqz p0, :cond_0

    .line 1140
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1142
    :cond_0
    return-void
.end method

.method public static removeViewFromParent(Landroid/view/View;)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1233
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1235
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1237
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 1238
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "parent":Landroid/view/ViewParent;
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1242
    :cond_0
    return-void

    .line 1239
    .restart local v0    # "parent":Landroid/view/ViewParent;
    :cond_1
    if-eqz v0, :cond_0

    .line 1240
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "View parent was not an instance of ViewGroup"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static retrieveContentDescriptionIfNotNullAndVisible(Landroid/view/View;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 841
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 842
    invoke-virtual {p0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 844
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static retrieveTextIfNotNull(Landroid/widget/TextView;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 827
    if-eqz p0, :cond_0

    .line 828
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 830
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 834
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 835
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 837
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static scrollToPosition(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 2
    .param p0, "lv"    # Landroid/support/v7/widget/RecyclerView;
    .param p1, "position"    # I

    .prologue
    .line 1171
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    .line 1172
    .local v0, "lm":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    instance-of v1, v0, Landroid/support/v7/widget/GridLayoutManager;

    if-eqz v1, :cond_1

    .line 1173
    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->scrollToPosition(I)V

    .line 1177
    :cond_0
    :goto_0
    return-void

    .line 1174
    :cond_1
    instance-of v1, v0, Landroid/support/v7/widget/LinearLayoutManager;

    if-eqz v1, :cond_0

    .line 1175
    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->scrollToPosition(I)V

    goto :goto_0
.end method

.method public static scrollToPositionWithOffset(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2
    .param p0, "lv"    # Landroid/support/v7/widget/RecyclerView;
    .param p1, "position"    # I
    .param p2, "y"    # I

    .prologue
    .line 1162
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    .line 1163
    .local v0, "lm":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    instance-of v1, v0, Landroid/support/v7/widget/GridLayoutManager;

    if-eqz v1, :cond_1

    .line 1164
    check-cast v0, Landroid/support/v7/widget/GridLayoutManager;

    .end local v0    # "lm":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/GridLayoutManager;->scrollToPositionWithOffset(II)V

    .line 1168
    :cond_0
    :goto_0
    return-void

    .line 1165
    .restart local v0    # "lm":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    :cond_1
    instance-of v1, v0, Landroid/support/v7/widget/LinearLayoutManager;

    if-eqz v1, :cond_0

    .line 1166
    check-cast v0, Landroid/support/v7/widget/LinearLayoutManager;

    .end local v0    # "lm":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/LinearLayoutManager;->scrollToPositionWithOffset(II)V

    goto :goto_0
.end method

.method public static scrollToPositionWithOffset(Landroid/widget/ListView;II)V
    .locals 0
    .param p0, "lv"    # Landroid/widget/ListView;
    .param p1, "position"    # I
    .param p2, "y"    # I

    .prologue
    .line 1158
    invoke-virtual {p0, p1, p2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 1159
    return-void
.end method

.method public static setAttachmentError(Landroid/view/ViewGroup;I)V
    .locals 6
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "resId"    # I

    .prologue
    .line 1125
    if-eqz p0, :cond_0

    .line 1127
    :try_start_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1128
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030041

    const/4 v5, 0x0

    invoke-virtual {v1, v4, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 1129
    .local v3, "v":Landroid/view/View;
    const v4, 0x7f0e01eb

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1130
    .local v2, "tv":Landroid/widget/TextView;
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(I)V

    .line 1131
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1136
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "tv":Landroid/widget/TextView;
    .end local v3    # "v":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 1132
    :catch_0
    move-exception v0

    .line 1133
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    sget-object v4, Lcom/microsoft/xbox/xle/app/XLEUtil;->TAG:Ljava/lang/String;

    const-string v5, "Resource not found"

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static setBackgroundColorIfNotNull(Landroid/view/View;I)V
    .locals 0
    .param p0, "v"    # Landroid/view/View;
    .param p1, "backgroundColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 1152
    if-eqz p0, :cond_0

    .line 1153
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1155
    :cond_0
    return-void
.end method

.method public static setKeepScreenOn(Z)V
    .locals 3
    .param p0, "keepScreenOn"    # Z

    .prologue
    const/16 v2, 0x80

    .line 958
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 959
    .local v0, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_1

    .line 960
    if-eqz p0, :cond_0

    .line 961
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 968
    :goto_0
    return-void

    .line 963
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    .line 966
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/app/XLEUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "setKeepScreenOn: MainActivity is null"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setTextColorIfNotNull(Landroid/widget/TextView;I)V
    .locals 1
    .param p0, "v"    # Landroid/widget/TextView;
    .param p1, "colorResId"    # I

    .prologue
    .line 1145
    if-eqz p0, :cond_0

    .line 1147
    invoke-virtual {p0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1149
    :cond_0
    return-void
.end method

.method public static setVisibility(Landroid/view/View;Z)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "show"    # Z

    .prologue
    .line 913
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 914
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 915
    return-void

    .line 914
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static shortDateNarratorContentUptoMonthToDurationNow(Ljava/util/Date;)Ljava/lang/String;
    .locals 14
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 541
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    sub-long v8, v10, v12

    .line 543
    .local v8, "timeDiff":J
    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-gez v1, :cond_0

    .line 544
    const-string v1, ""

    .line 569
    :goto_0
    return-object v1

    .line 545
    :cond_0
    const-wide/32 v10, 0x36ee80

    cmp-long v1, v8, v10

    if-gez v1, :cond_3

    .line 546
    const-wide/32 v10, 0xea60

    div-long v6, v8, v10

    .line 547
    .local v6, "minDiff":J
    const-wide/16 v10, 0x1

    cmp-long v1, v6, v10

    if-gez v1, :cond_1

    .line 548
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070628

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 550
    :cond_1
    const-wide/16 v10, 0x1

    cmp-long v1, v6, v10

    if-nez v1, :cond_2

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070d6f

    .line 551
    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 552
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070d68

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 554
    .end local v6    # "minDiff":J
    :cond_3
    const-wide/32 v10, 0x5265c00

    cmp-long v1, v8, v10

    if-gez v1, :cond_5

    .line 555
    const-wide/32 v10, 0x36ee80

    div-long v4, v8, v10

    .line 556
    .local v4, "hourDiff":J
    const-wide/16 v10, 0x1

    cmp-long v1, v4, v10

    if-nez v1, :cond_4

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070d6e

    .line 557
    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 558
    :cond_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070d67

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 559
    .end local v4    # "hourDiff":J
    :cond_5
    const-wide v10, 0x9a7ec800L

    cmp-long v1, v8, v10

    if-gez v1, :cond_7

    .line 560
    const-wide/32 v10, 0x5265c00

    div-long v2, v8, v10

    .line 561
    .local v2, "dayDiff":J
    const-wide/16 v10, 0x1

    cmp-long v1, v2, v10

    if-nez v1, :cond_6

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070d6c

    .line 562
    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 563
    :cond_6
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070d66

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 565
    .end local v2    # "dayDiff":J
    :cond_7
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 566
    .local v0, "dateFormat":Ljava/text/DateFormat;
    sget-object v1, Lcom/microsoft/xbox/toolkit/JavaUtil;->MIN_DATE:Ljava/util/Date;

    invoke-virtual {p0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v1

    if-gtz v1, :cond_8

    .line 567
    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 569
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static shortDateUptoMonthToDurationNow(Ljava/util/Date;)Ljava/lang/String;
    .locals 1
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 506
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateUptoMonthToDurationNow(Ljava/util/Date;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static shortDateUptoMonthToDurationNow(Ljava/util/Date;Z)Ljava/lang/String;
    .locals 14
    .param p0, "date"    # Ljava/util/Date;
    .param p1, "useMaxDaysInMonth"    # Z

    .prologue
    .line 510
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    sub-long v8, v10, v12

    .line 512
    .local v8, "timeDiff":J
    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-gez v1, :cond_0

    .line 513
    const-string v1, ""

    .line 535
    :goto_0
    return-object v1

    .line 514
    :cond_0
    const-wide/32 v10, 0x36ee80

    cmp-long v1, v8, v10

    if-gez v1, :cond_2

    .line 515
    const-wide/32 v10, 0xea60

    div-long v6, v8, v10

    .line 516
    .local v6, "minDiff":J
    const-wide/16 v10, 0x1

    cmp-long v1, v6, v10

    if-gtz v1, :cond_1

    .line 517
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070628

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 519
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f07062b

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 521
    .end local v6    # "minDiff":J
    :cond_2
    const-wide/32 v10, 0x5265c00

    cmp-long v1, v8, v10

    if-gez v1, :cond_3

    .line 522
    const-wide/32 v10, 0x36ee80

    div-long v4, v8, v10

    .line 523
    .local v4, "hourDiff":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f07062a

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 524
    .end local v4    # "hourDiff":J
    :cond_3
    if-eqz p1, :cond_4

    const-wide v10, 0x9fa52400L

    cmp-long v1, v8, v10

    if-gez v1, :cond_4

    .line 525
    const-wide/32 v10, 0x5265c00

    div-long v2, v8, v10

    .line 526
    .local v2, "dayDiff":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070629

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 527
    .end local v2    # "dayDiff":J
    :cond_4
    const-wide v10, 0x9a7ec800L

    cmp-long v1, v8, v10

    if-gez v1, :cond_5

    .line 528
    const-wide/32 v10, 0x5265c00

    div-long v2, v8, v10

    .line 529
    .restart local v2    # "dayDiff":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070629

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 531
    .end local v2    # "dayDiff":J
    :cond_5
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 532
    .local v0, "dateFormat":Ljava/text/DateFormat;
    sget-object v1, Lcom/microsoft/xbox/toolkit/JavaUtil;->MIN_DATE:Ljava/util/Date;

    invoke-virtual {p0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v1

    if-gtz v1, :cond_6

    .line 533
    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 535
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static shortDateUptoWeekToDurationNow(Ljava/util/Date;)Ljava/lang/String;
    .locals 14
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 478
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    sub-long v8, v10, v12

    .line 480
    .local v8, "timeDiff":J
    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-gez v1, :cond_0

    .line 481
    const-string v1, ""

    .line 500
    :goto_0
    return-object v1

    .line 482
    :cond_0
    const-wide/32 v10, 0x36ee80

    cmp-long v1, v8, v10

    if-gez v1, :cond_2

    .line 483
    const-wide/32 v10, 0xea60

    div-long v6, v8, v10

    .line 484
    .local v6, "minDiff":J
    const-wide/16 v10, 0x1

    cmp-long v1, v6, v10

    if-gtz v1, :cond_1

    .line 485
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070628

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 487
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f07062b

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 489
    .end local v6    # "minDiff":J
    :cond_2
    const-wide/32 v10, 0x5265c00

    cmp-long v1, v8, v10

    if-gez v1, :cond_3

    .line 490
    const-wide/32 v10, 0x36ee80

    div-long v4, v8, v10

    .line 491
    .local v4, "hourDiff":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f07062a

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 492
    .end local v4    # "hourDiff":J
    :cond_3
    const-wide/32 v10, 0x240c8400

    cmp-long v1, v8, v10

    if-gez v1, :cond_4

    .line 493
    const-wide/32 v10, 0x5265c00

    div-long v2, v8, v10

    .line 494
    .local v2, "dayDiff":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070629

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 496
    .end local v2    # "dayDiff":J
    :cond_4
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 497
    .local v0, "dateFormat":Ljava/text/DateFormat;
    sget-object v1, Lcom/microsoft/xbox/toolkit/JavaUtil;->MIN_DATE:Ljava/util/Date;

    invoke-virtual {p0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v1

    if-gtz v1, :cond_5

    .line 498
    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 500
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static shouldReloadEntity(Lcom/microsoft/xbox/service/model/entity/EntityModel;Ljava/lang/String;)Z
    .locals 7
    .param p0, "entityModel"    # Lcom/microsoft/xbox/service/model/entity/EntityModel;
    .param p1, "ownerXuid"    # Ljava/lang/String;

    .prologue
    .line 1104
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->shouldRefresh()Z

    move-result v4

    .line 1105
    .local v4, "shouldExecute":Z
    if-nez v4, :cond_0

    .line 1106
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getGameClipModelForOwnedActivityFeeedItemContent(Lcom/microsoft/xbox/service/model/entity/Entity;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    move-result-object v1

    .line 1107
    .local v1, "gameClipModel":Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    if-eqz v1, :cond_0

    .line 1108
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->shouldRefresh()Z

    move-result v4

    .line 1111
    .end local v1    # "gameClipModel":Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    :cond_0
    if-nez v4, :cond_1

    .line 1112
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    .line 1113
    .local v0, "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    if-nez v0, :cond_2

    .line 1114
    const/4 v4, 0x1

    .line 1121
    .end local v0    # "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    :cond_1
    :goto_0
    return v4

    .line 1115
    .restart local v0    # "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    :cond_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/Entity;->getType()Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACTIVITY_FEED_ITEM:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    if-ne v5, v6, :cond_1

    .line 1116
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/Entity;->getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v2

    .line 1117
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    .line 1118
    .local v3, "profileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefresh()Z

    move-result v4

    goto :goto_0
.end method

.method public static showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 7
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "promptText"    # Ljava/lang/String;
    .param p2, "okText"    # Ljava/lang/String;
    .param p3, "okHandler"    # Ljava/lang/Runnable;
    .param p4, "cancelText"    # Ljava/lang/String;
    .param p5, "cancelHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 983
    const-string v0, "You must supply cancel text if this is not a must-act dialog."

    invoke-static {v0, p4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 985
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->onShowDialog(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 986
    invoke-static {p3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->addTestHook(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v4

    .line 987
    .local v4, "okRunnable":Ljava/lang/Runnable;
    invoke-static {p5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->addTestHook(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v6

    .line 989
    .local v6, "cancelRunnable":Ljava/lang/Runnable;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 990
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->getVisibleDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->setCurrentDialog(Ljava/lang/String;Landroid/app/Dialog;)V

    .line 992
    .end local v4    # "okRunnable":Ljava/lang/Runnable;
    .end local v6    # "cancelRunnable":Ljava/lang/Runnable;
    :cond_0
    return-void
.end method

.method public static showKeyboard(Landroid/view/View;I)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "delayMS"    # I

    .prologue
    .line 922
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil$$Lambda$1;->lambdaFactory$(Landroid/view/View;)Ljava/lang/Runnable;

    move-result-object v0

    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 932
    return-void
.end method

.method public static showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 7
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "promptText"    # Ljava/lang/String;
    .param p2, "okText"    # Ljava/lang/String;
    .param p3, "okHandler"    # Ljava/lang/Runnable;
    .param p4, "cancelText"    # Ljava/lang/String;
    .param p5, "cancelHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 971
    const-string v0, "You must supply cancel text if this is not a must-act dialog."

    invoke-static {v0, p4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 973
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->onShowDialog(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 974
    invoke-static {p3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->addTestHook(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v4

    .line 975
    .local v4, "okRunnable":Ljava/lang/Runnable;
    invoke-static {p5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->addTestHook(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v6

    .line 977
    .local v6, "cancelRunnable":Ljava/lang/Runnable;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 978
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->getVisibleDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->setCurrentDialog(Ljava/lang/String;Landroid/app/Dialog;)V

    .line 980
    .end local v4    # "okRunnable":Ljava/lang/Runnable;
    .end local v6    # "cancelRunnable":Ljava/lang/Runnable;
    :cond_0
    return-void
.end method

.method public static showViewIfNotNull(Landroid/view/View;Z)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "show"    # Z

    .prologue
    .line 918
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 919
    return-void

    .line 918
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static startCounter(JLjava/util/Date;Landroid/widget/TextView;IJLjava/util/Date;Landroid/widget/TextView;Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;)V
    .locals 18
    .param p0, "milliSecond"    # J
    .param p2, "date"    # Ljava/util/Date;
    .param p3, "dateTextView"    # Landroid/widget/TextView;
    .param p4, "textColor"    # I
    .param p5, "nextMilliSecond"    # J
    .param p7, "nextDate"    # Ljava/util/Date;
    .param p8, "timerLabel"    # Landroid/widget/TextView;
    .param p9, "helper"    # Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;

    .prologue
    .line 765
    const-wide/16 v4, 0x0

    cmp-long v2, p0, v4

    if-lez v2, :cond_0

    .line 766
    new-instance v3, Lcom/microsoft/xbox/xle/app/XLEUtil$1;

    const-wide/16 v6, 0x3e8

    move-wide/from16 v4, p0

    move-wide/from16 v8, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p3

    move-wide/from16 v13, p0

    move/from16 v15, p4

    move-object/from16 v16, p9

    move-object/from16 v17, p2

    invoke-direct/range {v3 .. v17}, Lcom/microsoft/xbox/xle/app/XLEUtil$1;-><init>(JJJLjava/util/Date;Landroid/widget/TextView;Landroid/widget/TextView;JILcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;Ljava/util/Date;)V

    .line 803
    .local v3, "counter":Landroid/os/CountDownTimer;
    invoke-virtual {v3}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 804
    move-object/from16 v0, p9

    invoke-interface {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;->onNewTimer(Landroid/os/CountDownTimer;)V

    .line 806
    .end local v3    # "counter":Landroid/os/CountDownTimer;
    :cond_0
    return-void
.end method

.method public static updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 872
    if-eqz p0, :cond_0

    .line 873
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 874
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 875
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 880
    :cond_0
    :goto_0
    return-void

    .line 877
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "view"    # Landroid/view/View;
    .param p1, "contentDescription"    # Ljava/lang/CharSequence;

    .prologue
    .line 848
    if-eqz p0, :cond_0

    .line 849
    invoke-virtual {p0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 851
    :cond_0
    return-void
.end method

.method public static updateEnabledIfNotNull(Landroid/view/View;Z)V
    .locals 0
    .param p0, "view"    # Landroid/view/View;
    .param p1, "enabled"    # Z

    .prologue
    .line 907
    if-eqz p0, :cond_0

    .line 908
    invoke-virtual {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 910
    :cond_0
    return-void
.end method

.method public static updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V
    .locals 0
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "visibility"    # I

    .prologue
    .line 894
    if-eqz p0, :cond_0

    .line 895
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 896
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 898
    :cond_0
    return-void
.end method

.method public static updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 854
    const/16 v0, 0x8

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 855
    return-void
.end method

.method public static updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "falseVisibility"    # I

    .prologue
    .line 858
    if-eqz p0, :cond_0

    .line 859
    if-eqz p1, :cond_1

    .line 860
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 861
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 866
    :cond_0
    :goto_0
    return-void

    .line 863
    :cond_1
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "condition"    # Z
    .param p2, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 883
    if-eqz p0, :cond_0

    .line 884
    if-eqz p1, :cond_1

    .line 885
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 886
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 891
    :cond_0
    :goto_0
    return-void

    .line 888
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static updateTextIfNotNull(Landroid/widget/TextView;I)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "textId"    # I

    .prologue
    .line 809
    if-eqz p0, :cond_0

    .line 810
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 812
    :cond_0
    return-void
.end method

.method public static updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 815
    if-eqz p0, :cond_0

    .line 816
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 818
    :cond_0
    return-void
.end method

.method public static updateTextIfNotNull(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;I)V
    .locals 1
    .param p0, "imageView"    # Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .param p1, "textId"    # I

    .prologue
    .line 821
    if-eqz p0, :cond_0

    .line 822
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(Ljava/lang/String;)V

    .line 824
    :cond_0
    return-void
.end method

.method public static updateVisibilityIfNotNull(Landroid/view/View;I)V
    .locals 0
    .param p0, "view"    # Landroid/view/View;
    .param p1, "visibility"    # I

    .prologue
    .line 901
    if-eqz p0, :cond_0

    .line 902
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 904
    :cond_0
    return-void
.end method
