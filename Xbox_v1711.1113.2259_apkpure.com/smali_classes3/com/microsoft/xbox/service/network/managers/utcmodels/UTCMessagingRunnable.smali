.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCMessagingRunnable;
.super Ljava/lang/Object;
.source "UTCMessagingRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

.field private attachmentId:Ljava/lang/String;

.field private message:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/entity/Entity;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 0
    .param p1, "attachment"    # Lcom/microsoft/xbox/service/model/entity/Entity;
    .param p2, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCMessagingRunnable;->attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

    .line 27
    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCMessagingRunnable;->message:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 28
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 17
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCMessagingRunnable;->message:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCMessagingRunnable;->attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCMessagingRunnable;->attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCMessagingRunnable;->message:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackShowAttachment(Lcom/microsoft/xbox/service/model/entity/Entity;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V

    .line 20
    :cond_0
    return-void
.end method
