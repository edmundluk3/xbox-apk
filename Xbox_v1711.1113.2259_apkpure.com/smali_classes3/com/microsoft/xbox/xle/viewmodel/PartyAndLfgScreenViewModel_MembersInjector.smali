.class public final Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;
.super Ljava/lang/Object;
.source "PartyAndLfgScreenViewModel_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final partyChatRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final systemSettingsModelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;"
        }
    .end annotation
.end field

.field private final telemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p1, "partyChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;>;"
    .local p2, "systemSettingsModelProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xle/model/SystemSettingsModel;>;"
    .local p3, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget-boolean v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 27
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    .line 28
    sget-boolean v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 29
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->systemSettingsModelProvider:Ljavax/inject/Provider;

    .line 30
    sget-boolean v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 31
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->telemetryServiceProvider:Ljavax/inject/Provider;

    .line 32
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;",
            ">;)",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "partyChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;>;"
    .local p1, "systemSettingsModelProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xle/model/SystemSettingsModel;>;"
    .local p2, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPartyChatRepository(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "partyChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    .line 56
    return-void
.end method

.method public static injectSystemSettingsModel(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "systemSettingsModelProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xle/model/SystemSettingsModel;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .line 62
    return-void
.end method

.method public static injectTelemetryService(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p1, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    .line 68
    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;)V
    .locals 2
    .param p1, "instance"    # Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    .prologue
    .line 44
    if-nez p1, :cond_0

    .line 45
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->systemSettingsModelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->telemetryServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    .line 50
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->injectMembers(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;)V

    return-void
.end method
