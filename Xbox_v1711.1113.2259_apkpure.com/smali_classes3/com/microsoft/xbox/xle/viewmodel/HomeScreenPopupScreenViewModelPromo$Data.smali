.class public Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;
.super Ljava/lang/Object;
.source "HomeScreenPopupScreenViewModelPromo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Data"
.end annotation


# instance fields
.field public final imageUri:Ljava/lang/String;

.field public final welcomeText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "imageUri"    # Ljava/lang/String;
    .param p2, "welcomeText"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;->imageUri:Ljava/lang/String;

    .line 88
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;->welcomeText:Ljava/lang/String;

    .line 89
    return-void
.end method
