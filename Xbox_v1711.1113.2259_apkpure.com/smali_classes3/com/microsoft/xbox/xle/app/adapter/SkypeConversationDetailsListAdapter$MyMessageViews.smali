.class Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;
.super Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;
.source "SkypeConversationDetailsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyMessageViews"
.end annotation


# instance fields
.field private attachmentContainer:Landroid/view/ViewGroup;

.field private messageContainer:Landroid/view/View;

.field private speechArrowGraphics:Landroid/widget/ImageView;

.field private titleView:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 321
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;-><init>()V

    .line 322
    return-void
.end method

.method static synthetic access$1000(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;
    .locals 1
    .param p0, "x0"    # Landroid/view/View;

    .prologue
    .line 315
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->create(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;

    .prologue
    .line 315
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->titleView:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;

    .prologue
    .line 315
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->messageContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;

    .prologue
    .line 315
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->speechArrowGraphics:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;

    .prologue
    .line 315
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->attachmentContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private static create(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;
    .locals 2
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 325
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;-><init>()V

    .line 327
    .local v0, "result":Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;
    const v1, 0x7f0e04a5

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->titleView:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    .line 328
    const v1, 0x7f0e04a4

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->messageContainer:Landroid/view/View;

    .line 329
    const v1, 0x7f0e04a1

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->attachmentContainer:Landroid/view/ViewGroup;

    .line 330
    const v1, 0x7f0e04a3

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->speechArrowGraphics:Landroid/widget/ImageView;

    .line 332
    return-object v0
.end method
