.class public Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;
.super Ljava/lang/Object;
.source "AppChannelData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/AppChannelData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShowExtra"
.end annotation


# instance fields
.field public contentType:Ljava/lang/String;

.field public deepLink:Ljava/lang/String;

.field public descritpion:Ljava/lang/String;

.field public duration:I

.field public episodeName:Ljava/lang/String;

.field public parentalRating:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->descritpion:Ljava/lang/String;

    .line 64
    const-string v0, "episodeName"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->episodeName:Ljava/lang/String;

    .line 65
    const-string v0, "parentalRating"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->parentalRating:Ljava/lang/String;

    .line 66
    const-string v0, "contentType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->contentType:Ljava/lang/String;

    .line 67
    const-string v0, "deepLink"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->deepLink:Ljava/lang/String;

    .line 68
    const-string v0, "duration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->duration:I

    .line 69
    return-void
.end method
