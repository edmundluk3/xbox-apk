.class public Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "PartyDetailsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PartyMemberFullViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;",
        ">;"
    }
.end annotation


# instance fields
.field broadcastingIcon:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e085f
    .end annotation
.end field

.field detailContainer:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0859
    .end annotation
.end field

.field expandIcon:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e085e
    .end annotation
.end field

.field gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0867
    .end annotation
.end field

.field gamertag:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e085b
    .end annotation
.end field

.field isMe:Z

.field kickButton:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0864
    .end annotation
.end field

.field leaderIcon:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e085d
    .end annotation
.end field

.field private memberColor:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private memberXuid:J

.field muteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0862
    .end annotation
.end field

.field optionsContainer:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0861
    .end annotation
.end field

.field private optionsVisible:Z

.field presence:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0860
    .end annotation
.end field

.field profileButton:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0863
    .end annotation
.end field

.field realname:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e085c
    .end annotation
.end field

.field squawkerView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0866
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->this$0:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    .line 163
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 164
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->detailContainer:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->muteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->profileButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->kickButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->onUpdate(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->optionsVisible:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->setOptionsVisibility(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->this$0:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->access$300(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->memberXuid:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$ToggleMemberMuteIntent;->with(J)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$ToggleMemberMuteIntent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->this$0:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->access$300(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->memberXuid:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$NavigateToUserProfileIntent;->with(J)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$NavigateToUserProfileIntent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->this$0:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->access$300(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->memberXuid:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$KickMemberIntent;->with(J)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$KickMemberIntent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method private onUpdate(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V
    .locals 9
    .param p1, "partyMember"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .prologue
    const v8, 0x7f070a20

    const v7, 0x7f070a1c

    const/4 v6, 0x1

    const v5, 0x7f070780

    const/4 v4, 0x0

    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->gamertag()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 196
    .local v0, "contentDescription":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isHost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070a34

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$1;->$SwitchMap$com$microsoft$xbox$xbservices$domain$party$PartyMember$MemberChatState:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->currentState()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 229
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isBroadcasting()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 230
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->broadcastingIcon:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 231
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070a17

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->detailContainer:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 237
    return-void

    .line 205
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->presence:Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 209
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->presence:Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 213
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->presence:Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->renderTalking(Z)V

    .line 215
    invoke-direct {p0, v6}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->renderMuted(Z)V

    goto :goto_0

    .line 218
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->presence:Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->renderTalking(Z)V

    .line 220
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->renderMuted(Z)V

    goto :goto_0

    .line 223
    :pswitch_4
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->presence:Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    invoke-direct {p0, v6}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->renderTalking(Z)V

    .line 225
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->renderMuted(Z)V

    goto/16 :goto_0

    .line 233
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->broadcastingIcon:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private renderMuted(Z)V
    .locals 5
    .param p1, "isMuted"    # Z

    .prologue
    .line 246
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v2, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v2}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 248
    .local v1, "squawkerRing":Landroid/graphics/drawable/ShapeDrawable;
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    .line 249
    .local v0, "color":I
    :goto_0
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 250
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->squawkerView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 252
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->muteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    if-eqz p1, :cond_1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f071052

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 253
    if-eqz p1, :cond_3

    .line 254
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->muteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-boolean v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->isMe:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070a47

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 258
    :goto_3
    return-void

    .line 248
    .end local v0    # "color":I
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->memberColor:I

    goto :goto_0

    .line 252
    .restart local v0    # "color":I
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07104f

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 254
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07066b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 256
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->muteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-boolean v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->isMe:Z

    if-eqz v2, :cond_4

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070a38

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_4
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_4
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07065c

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_4
.end method

.method private renderTalking(Z)V
    .locals 2
    .param p1, "isTalking"    # Z

    .prologue
    .line 240
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->squawkerView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 241
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->access$100()I

    move-result v1

    :goto_0
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 242
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->squawkerView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 243
    return-void

    .line 241
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->access$200()I

    move-result v1

    goto :goto_0
.end method

.method private setOptionsVisibility(Z)V
    .locals 2
    .param p1, "visibility"    # Z

    .prologue
    .line 261
    iput-boolean p1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->optionsVisible:Z

    .line 262
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->optionsContainer:Landroid/view/View;

    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->optionsVisible:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 263
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->expandIcon:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->optionsVisible:Z

    if-eqz v0, :cond_1

    const v0, 0x7f070ef6

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 264
    return-void

    .line 262
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 263
    :cond_1
    const v0, 0x7f070ef3

    goto :goto_1
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;)V
    .locals 9
    .param p1, "listItem"    # Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v8, 0x7f020125

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 174
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->partyMember()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v0

    .line 175
    .local v0, "user":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->userSummary()Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    move-result-object v1

    .line 176
    .local v1, "userSummary":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->isMe:Z

    .line 177
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->memberXuid:J

    .line 178
    invoke-virtual {v1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->colors()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v2

    const/high16 v5, -0x1000000

    or-int/2addr v2, v5

    iput v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->memberColor:I

    .line 180
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->displayPicUri()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 181
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->gamertag:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->gamertag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->realname:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->realName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->leaderIcon:Landroid/view/View;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isHost()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 185
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->canBeKicked()Z

    move-result v2

    if-nez v2, :cond_0

    .line 186
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->kickButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 189
    :cond_0
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->setOptionsVisibility(Z)V

    .line 190
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->onUpdate(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V

    .line 191
    return-void

    :cond_1
    move v2, v4

    .line 183
    goto :goto_0
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 115
    check-cast p1, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->onBind(Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;)V

    return-void
.end method
