.class public Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;
.super Ljava/lang/Object;
.source "NavigationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ItemNavigation"
.end annotation


# instance fields
.field public final navigationGeneric:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

.field public final navigationScreenshot:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

.field public final navigationSelf:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;)V
    .locals 0
    .param p1, "navigationSelf"    # Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;
    .param p2, "navigationScreenshot"    # Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;
    .param p3, "navigationGeneric"    # Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    .prologue
    .line 433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 434
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;->navigationSelf:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;

    .line 435
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;->navigationScreenshot:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    .line 436
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;->navigationGeneric:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    .line 437
    return-void
.end method
