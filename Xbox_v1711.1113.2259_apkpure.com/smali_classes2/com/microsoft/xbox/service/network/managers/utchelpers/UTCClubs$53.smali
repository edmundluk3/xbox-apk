.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;
.super Ljava/lang/Object;
.source "UTCClubs.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminReportsReporterMessage(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$clubId:J

.field final synthetic val$creatorXuid:Ljava/lang/String;

.field final synthetic val$itemId:Ljava/lang/String;

.field final synthetic val$reporterXuid:Ljava/lang/String;


# direct methods
.method constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1051
    iput-wide p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$clubId:J

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$itemId:Ljava/lang/String;

    iput-object p4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$creatorXuid:Ljava/lang/String;

    iput-object p5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$reporterXuid:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    .line 1054
    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$clubId:J

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 1055
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$itemId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1056
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$creatorXuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1057
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$reporterXuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1059
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 1060
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ClubId"

    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$clubId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1061
    const-string v1, "ReportItemId"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$itemId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1062
    const-string v1, "ReportCreatorXuid"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$creatorXuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1063
    const-string v1, "ReportReporterXuid"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;->val$reporterXuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1065
    const-string v1, "Clubs - Admin Reports Reporter Message"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 1066
    return-void
.end method
