.class public final Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;
.super Ljava/lang/Object;
.source "LeaderboardsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GamerscoreColumn"
.end annotation


# instance fields
.field private final gamerScore:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "GamerScore"
    .end annotation
.end field

.field private volatile transient hashCode:I


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "gamerScore"    # J

    .prologue
    .line 309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    iput-wide p1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;->gamerScore:J

    .line 311
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;

    .prologue
    .line 303
    iget-wide v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;->gamerScore:J

    return-wide v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 315
    if-ne p1, p0, :cond_1

    .line 321
    :cond_0
    :goto_0
    return v1

    .line 317
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;

    if-nez v3, :cond_2

    move v1, v2

    .line 318
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 320
    check-cast v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;

    .line 321
    .local v0, "other":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;->gamerScore:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;->gamerScore:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 327
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;->hashCode:I

    if-nez v0, :cond_0

    .line 328
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;->hashCode:I

    .line 329
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;->gamerScore:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;->hashCode:I

    .line 332
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 337
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
