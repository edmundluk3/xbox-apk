.class Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask$1;
.super Ljava/lang/Object;
.source "FriendsWhoPlayScreenViewModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->processData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/microsoft/xbox/service/model/FollowersData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask$1;->this$1:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/service/model/FollowersData;)I
    .locals 4
    .param p1, "f0"    # Lcom/microsoft/xbox/service/model/FollowersData;
    .param p2, "f1"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 225
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v3, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v2, v3, :cond_3

    .line 226
    iget-object v2, p2, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v3, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v2, v3, :cond_0

    .line 227
    iget-boolean v2, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-eqz v2, :cond_1

    .line 228
    iget-boolean v1, p2, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-eqz v1, :cond_0

    .line 229
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    .line 246
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    iget-boolean v0, p2, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 233
    goto :goto_0

    .line 235
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 240
    :cond_3
    iget-object v0, p2, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v2, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v0, v2, :cond_4

    move v0, v1

    .line 241
    goto :goto_0

    .line 243
    :cond_4
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getTimeStamp()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/FollowersData;->getTimeStamp()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 244
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/FollowersData;->getTimeStamp()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getTimeStamp()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    goto :goto_0

    .line 246
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 222
    check-cast p1, Lcom/microsoft/xbox/service/model/FollowersData;

    check-cast p2, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask$1;->compare(Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/service/model/FollowersData;)I

    move-result v0

    return v0
.end method
