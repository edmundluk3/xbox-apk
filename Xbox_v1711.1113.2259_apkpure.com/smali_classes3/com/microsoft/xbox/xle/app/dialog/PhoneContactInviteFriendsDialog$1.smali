.class Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$1;
.super Ljava/lang/Object;
.source "PhoneContactInviteFriendsDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 68
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;

    .line 69
    .local v0, "person":Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;
    iget-boolean v1, v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;->isSelected:Z

    if-eqz v1, :cond_0

    .line 71
    iput-boolean v2, v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;->isSelected:Z

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)Landroid/widget/AbsListView;

    move-result-object v1

    invoke-virtual {v1, p3, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 77
    :goto_0
    return-void

    .line 74
    :cond_0
    iput-boolean v3, v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;->isSelected:Z

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)Landroid/widget/AbsListView;

    move-result-object v1

    invoke-virtual {v1, p3, v3}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    goto :goto_0
.end method
