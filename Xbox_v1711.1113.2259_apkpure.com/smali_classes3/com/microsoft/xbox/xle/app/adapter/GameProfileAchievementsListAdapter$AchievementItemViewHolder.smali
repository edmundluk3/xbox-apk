.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "GameProfileAchievementsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AchievementItemViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;",
        ">;"
    }
.end annotation


# instance fields
.field achieveComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0696
    .end annotation
.end field

.field descriptionTextView:Landroid/widget/TextView;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0691
    .end annotation
.end field

.field gamerscoreIconView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0690
    .end annotation
.end field

.field gamerscoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0323
    .end annotation
.end field

.field imageContainer:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e068c
    .end annotation
.end field

.field imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e05eb
    .end annotation
.end field

.field lockedAchievementIconView:Landroid/widget/TextView;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0693
    .end annotation
.end field

.field progressTextView:Landroid/widget/TextView;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0695
    .end annotation
.end field

.field rareIconView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e068f
    .end annotation
.end field

.field rarityTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0692
    .end annotation
.end field

.field rewardIconView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e068e
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

.field timeRemainingTextView:Landroid/widget/TextView;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0698
    .end annotation
.end field

.field timerIconView:Landroid/widget/TextView;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0697
    .end annotation
.end field

.field titleView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e068d
    .end annotation
.end field

.field unlockedDetailsTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0694
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 372
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .line 373
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 374
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 376
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 381
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 377
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->getAdapterPosition()I

    move-result v1

    .line 378
    .local v1, "position":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->achievementItem()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    move-result-object v0

    .line 379
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->navigateToAchievementDetails(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    .line 380
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;)V
    .locals 9
    .param p1, "compositeItem"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 385
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->achievementItem()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    move-result-object v0

    .line 387
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->gamerscoreIconView:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 388
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timerIconView:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 389
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->rewardIconView:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 390
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->lockedAchievementIconView:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 391
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->gamerscoreTextView:Landroid/widget/TextView;

    const-string v5, ""

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 392
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timeRemainingTextView:Landroid/widget/TextView;

    const-string v5, ""

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 393
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->progressTextView:Landroid/widget/TextView;

    const-string v5, ""

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 394
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->unlockedDetailsTextView:Landroid/widget/TextView;

    const-string v5, ""

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 395
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->rareIconView:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 397
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v2, :cond_0

    .line 398
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getAchievementIconUri()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f020053

    const v7, 0x7f020055

    invoke-virtual {v2, v5, v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 400
    iget-boolean v2, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->isSecret:Z

    if-eqz v2, :cond_c

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v2

    sget-object v5, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-eq v2, v5, :cond_c

    .line 401
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 406
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->titleView:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 407
    iget-boolean v2, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->isSecret:Z

    if-eqz v2, :cond_1

    iget-boolean v2, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->isSecret:Z

    if-eqz v2, :cond_d

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v2

    sget-object v5, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v2, v5, :cond_d

    .line 408
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->titleView:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->name:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 409
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->titleView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 414
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->descriptionTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 415
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v2

    sget-object v5, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v2, v5, :cond_e

    .line 416
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->descriptionTextView:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->description:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 424
    :goto_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->descriptionTextView:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 427
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->rarityTextView:Landroid/widget/TextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    .line 429
    invoke-virtual {v5}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0705e1

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getAchievementRarity()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 427
    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 431
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->getItemViewType()I

    move-result v2

    const v5, 0x7f03012b

    if-ne v2, v5, :cond_4

    .line 432
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->lockedAchievementIconView:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 435
    :cond_4
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->imageContainer:Landroid/view/View;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v2

    if-eqz v2, :cond_10

    move v2, v3

    :goto_3
    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 438
    iget-boolean v2, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->isSecret:Z

    if-eqz v2, :cond_5

    iget-boolean v2, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->isSecret:Z

    if-eqz v2, :cond_a

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v2

    sget-object v5, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v2, v5, :cond_a

    .line 439
    :cond_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->gamerscoreTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getGamerscore()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 440
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->gamerscoreTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getGamerscore()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->gamerscoreIconView:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 444
    :cond_6
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->rareIconView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getIsRare()Z

    move-result v5

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 446
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->hasRewards()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 448
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->rewardIconView:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 451
    :cond_7
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v2

    sget-object v5, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v2, v5, :cond_8

    .line 452
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->unlockedDetailsTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getTimeUnlocked()Ljava/util/Date;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getValidAchievementUnlockedDateString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 455
    :cond_8
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timeRemainingTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timerIconView:Landroid/widget/TextView;

    if-eqz v2, :cond_9

    .line 456
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getRemainingTime()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v2

    sget-object v5, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-eq v2, v5, :cond_11

    .line 457
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timeRemainingTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getRemainingTime()Ljava/util/Date;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemainingShort(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 458
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timeRemainingTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 459
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timerIconView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 465
    :cond_9
    :goto_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->progressTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_a

    .line 466
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v1

    .line 467
    .local v1, "progress":I
    if-lez v1, :cond_12

    .line 468
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->progressTextView:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 469
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->lockedAchievementIconView:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 476
    .end local v1    # "progress":I
    :cond_a
    :goto_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->achieveComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    if-eqz v2, :cond_b

    .line 477
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->achieveComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v3

    rsub-int/lit8 v3, v3, 0x64

    int-to-long v6, v3

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setExactValues(JJ)V

    .line 478
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->achieveComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 479
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->achieveComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    .line 480
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->achieveComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarAlpha(F)V

    .line 482
    :cond_b
    return-void

    .line 403
    :cond_c
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 411
    :cond_d
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->titleView:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 418
    :cond_e
    iget-boolean v2, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->isSecret:Z

    if-eqz v2, :cond_f

    .line 419
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->descriptionTextView:Landroid/widget/TextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070083

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 421
    :cond_f
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->descriptionTextView:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->lockedDescription:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_10
    move v2, v4

    .line 435
    goto/16 :goto_3

    .line 461
    :cond_11
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timeRemainingTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 462
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timerIconView:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 471
    .restart local v1    # "progress":I
    :cond_12
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->lockedAchievementIconView:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_5
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 321
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;)V

    return-void
.end method
