.class final Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;
.super Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;
.source "AutoValue_OOBEInteractor_FinishAction.java"


# instance fields
.field private final email:Ljava/lang/String;

.field private final gamerpic:Ljava/lang/String;

.field private final gamertag:Ljava/lang/String;

.field private final isNewUser:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "email"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "gamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "gamerpic"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "isNewUser"    # Z

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->email:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamertag:Ljava/lang/String;

    .line 22
    iput-object p3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamerpic:Ljava/lang/String;

    .line 23
    iput-boolean p4, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->isNewUser:Z

    .line 24
    return-void
.end method


# virtual methods
.method public email()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->email:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    if-ne p1, p0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 64
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;

    if-eqz v3, :cond_6

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;

    .line 66
    .local v0, "that":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->email:Ljava/lang/String;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->email()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamertag:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 67
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->gamertag()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamerpic:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 68
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->gamerpic()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->isNewUser:Z

    .line 69
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->isNewUser()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 66
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->email:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->email()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 67
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamertag:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->gamertag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 68
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamerpic:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->gamerpic()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .end local v0    # "that":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;
    :cond_6
    move v1, v2

    .line 71
    goto :goto_0
.end method

.method public gamerpic()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamerpic:Ljava/lang/String;

    return-object v0
.end method

.method public gamertag()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamertag:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 76
    const/4 v0, 0x1

    .line 77
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->email:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 79
    mul-int/2addr v0, v3

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamertag:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 81
    mul-int/2addr v0, v3

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamerpic:Ljava/lang/String;

    if-nez v1, :cond_2

    :goto_2
    xor-int/2addr v0, v2

    .line 83
    mul-int/2addr v0, v3

    .line 84
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->isNewUser:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x4cf

    :goto_3
    xor-int/2addr v0, v1

    .line 85
    return v0

    .line 78
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->email:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamertag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 82
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamerpic:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 84
    :cond_3
    const/16 v1, 0x4d5

    goto :goto_3
.end method

.method public isNewUser()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->isNewUser:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FinishAction{email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gamertag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamertag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gamerpic="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->gamerpic:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isNewUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;->isNewUser:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
