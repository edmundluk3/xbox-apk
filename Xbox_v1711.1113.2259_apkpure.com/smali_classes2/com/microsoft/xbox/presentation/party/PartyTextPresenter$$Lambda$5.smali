.class final synthetic Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$5;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;

.field private final arg$2:Lcom/microsoft/xbox/domain/party/PartyInteractor;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;Lcom/microsoft/xbox/domain/party/PartyInteractor;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$5;->arg$1:Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$5;->arg$2:Lcom/microsoft/xbox/domain/party/PartyInteractor;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$5;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$5;-><init>(Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;Lcom/microsoft/xbox/domain/party/PartyInteractor;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$5;->arg$1:Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$5;->arg$2:Lcom/microsoft/xbox/domain/party/PartyInteractor;

    check-cast p1, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->lambda$null$3(Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
