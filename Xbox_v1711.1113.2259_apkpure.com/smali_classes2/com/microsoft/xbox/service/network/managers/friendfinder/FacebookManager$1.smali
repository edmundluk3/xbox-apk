.class Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;
.super Ljava/lang/Object;
.source "FacebookManager.java"

# interfaces
.implements Lcom/facebook/FacebookCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/FacebookCallback",
        "<",
        "Lcom/facebook/share/Sharer$Result;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 2

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration;->trackShareAction(Z)V

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->navigateToSuggestionList(Z)V

    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$002(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/facebook/share/widget/ShareDialog;)Lcom/facebook/share/widget/ShareDialog;

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$100(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->ShareCancel:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 119
    return-void
.end method

.method public onError(Lcom/facebook/FacebookException;)V
    .locals 2
    .param p1, "error"    # Lcom/facebook/FacebookException;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->navigateToSuggestionList(Z)V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$002(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/facebook/share/widget/ShareDialog;)Lcom/facebook/share/widget/ShareDialog;

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$100(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->ShareError:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 127
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->SHARE_TO_FACEBOOK:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyFriendFinderConfirmDialogAsyncTaskFailed(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V

    .line 128
    return-void
.end method

.method public onSuccess(Lcom/facebook/share/Sharer$Result;)V
    .locals 2
    .param p1, "result"    # Lcom/facebook/share/Sharer$Result;

    .prologue
    const/4 v1, 0x1

    .line 105
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration;->trackShareAction(Z)V

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->navigateToSuggestionList(Z)V

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$002(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/facebook/share/widget/ShareDialog;)Lcom/facebook/share/widget/ShareDialog;

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$100(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->ShareSuccess:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 109
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 100
    check-cast p1, Lcom/facebook/share/Sharer$Result;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;->onSuccess(Lcom/facebook/share/Sharer$Result;)V

    return-void
.end method
