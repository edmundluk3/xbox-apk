.class abstract Lcom/microsoft/xbox/smartglass/controls/Input;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "Input.java"


# static fields
.field public static final ComponentName:Ljava/lang/String; = "Input"

.field private static final SendMethod:Ljava/lang/String; = "Send"


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 1
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 25
    const-string v0, "Input"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 27
    const-string v0, "Send"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Input;->registerMethod(Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method private sendInput(Lcom/microsoft/xbox/smartglass/controls/CanvasKey;)V
    .locals 12
    .param p1, "key"    # Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .prologue
    const/4 v2, 0x0

    .line 59
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Clear:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 60
    .local v8, "buttons":Lcom/microsoft/xbox/smartglass/GamePadButtons;
    sget-object v10, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->None:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 63
    .local v10, "mediaCommand":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    sget-object v3, Lcom/microsoft/xbox/smartglass/controls/Input$1;->$SwitchMap$com$microsoft$xbox$smartglass$controls$CanvasKey:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 113
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Input button not supported."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 65
    :pswitch_0
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadA:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 116
    :goto_0
    sget-object v3, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Clear:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    if-eq v8, v3, :cond_1

    .line 117
    invoke-static {v8}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    .line 118
    .local v1, "buttonSet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/GamePadButtons;>;"
    new-instance v0, Lcom/microsoft/xbox/smartglass/GamePad;

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/smartglass/GamePad;-><init>(Ljava/util/EnumSet;FFFFFF)V

    .line 119
    .local v0, "gamePad":Lcom/microsoft/xbox/smartglass/GamePad;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendGamePad(Lcom/microsoft/xbox/smartglass/GamePad;Z)V

    .line 127
    .end local v0    # "gamePad":Lcom/microsoft/xbox/smartglass/GamePad;
    .end local v1    # "buttonSet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/GamePadButtons;>;"
    :cond_0
    :goto_1
    return-void

    .line 68
    :pswitch_1
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadB:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 69
    goto :goto_0

    .line 71
    :pswitch_2
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadX:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 72
    goto :goto_0

    .line 74
    :pswitch_3
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadY:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 75
    goto :goto_0

    .line 77
    :pswitch_4
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadUp:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 78
    goto :goto_0

    .line 80
    :pswitch_5
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadDown:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 81
    goto :goto_0

    .line 83
    :pswitch_6
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadLeft:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 84
    goto :goto_0

    .line 86
    :pswitch_7
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadRight:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 87
    goto :goto_0

    .line 89
    :pswitch_8
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Nexus:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 90
    goto :goto_0

    .line 92
    :pswitch_9
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->View:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 93
    goto :goto_0

    .line 95
    :pswitch_a
    sget-object v8, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Menu:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 96
    goto :goto_0

    .line 98
    :pswitch_b
    sget-object v10, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Play:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 99
    goto :goto_0

    .line 101
    :pswitch_c
    sget-object v10, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Pause:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 102
    goto :goto_0

    .line 104
    :pswitch_d
    sget-object v10, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Stop:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 105
    goto :goto_0

    .line 107
    :pswitch_e
    sget-object v10, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->FastForward:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 108
    goto :goto_0

    .line 110
    :pswitch_f
    sget-object v10, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Rewind:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 111
    goto :goto_0

    .line 120
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->None:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    if-eq v10, v2, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Input;->getDefaultTarget()Lcom/microsoft/xbox/smartglass/MessageTarget;

    move-result-object v11

    .line 122
    .local v11, "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    if-eqz v11, :cond_0

    .line 123
    new-instance v9, Lcom/microsoft/xbox/smartglass/MediaCommand;

    iget v2, v11, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    invoke-direct {v9, v2, v10}, Lcom/microsoft/xbox/smartglass/MediaCommand;-><init>(ILcom/microsoft/xbox/smartglass/MediaControlCommands;)V

    .line 124
    .local v9, "command":Lcom/microsoft/xbox/smartglass/MediaCommand;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendMediaCommand(Lcom/microsoft/xbox/smartglass/MediaCommand;)V

    goto :goto_1

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method


# virtual methods
.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/Input;->validateMethod(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/Input;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 39
    :goto_0
    return-void

    .line 38
    :cond_0
    invoke-virtual {p0, p1, p3}, Lcom/microsoft/xbox/smartglass/controls/Input;->send(ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected send(ILcom/microsoft/xbox/smartglass/controls/CanvasKey;)V
    .locals 4
    .param p1, "requestId"    # I
    .param p2, "key"    # Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .prologue
    .line 44
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getConnectionState()Lcom/microsoft/xbox/smartglass/ConnectionState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-eq v1, v2, :cond_0

    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Input;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "Session is disconnected."

    invoke-interface {v1, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 56
    :goto_0
    return-void

    .line 50
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v1

    const-string v2, "Send"

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/smartglass/controls/Input;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/Input;->sendInput(Lcom/microsoft/xbox/smartglass/controls/CanvasKey;)V

    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Input;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 53
    :catch_0
    move-exception v0

    .line 54
    .local v0, "ex":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Input;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to send input. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract send(ILjava/lang/String;)V
.end method
