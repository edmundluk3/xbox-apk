.class public Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ActivityFeedActionsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadCommentsItemsLikesInfoAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeCommentAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;


# instance fields
.field private actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

.field private actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

.field private actionsModel:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

.field private availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

.field private final completionHandler:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

.field private isDeletingComment:Z

.field private item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field private itemListState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private final itemLocator:Ljava/lang/String;

.field private likeUnlikeCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeCommentAsyncTask;

.field private likeUnlikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;

.field private likeUpdated:Z

.field private loadCommentsLikesInfoTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadCommentsItemsLikesInfoAsyncTask;

.field private loadFeedItemLikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;

.field private loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;

.field private loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

.field private final meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private message:Ljava/lang/String;

.field private oldInputMode:I

.field private final params:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

.field private postCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;

.field private prepareToScroll:[Z

.field private reloadActions:[Z

.field private scrollToBottom:[Z

.field private shouldScrollToBottomOnLayoutChange:Z

.field private socialActionInfoUpdated:Z

.field private tasks:[Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    .line 57
    sget-object v0, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 90
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 83
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->shouldScrollToBottomOnLayoutChange:Z

    .line 86
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->socialActionInfoUpdated:Z

    .line 842
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->completionHandler:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

    .line 91
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setItem(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->itemListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->itemLocator:Ljava/lang/String;

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 98
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->oldInputMode:I

    .line 99
    return-void

    .line 93
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    goto :goto_1
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionsModel:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->onFeedItemActionsComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->itemLocator:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/entity/EntityModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/model/entity/EntityModel;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->onFeedItemComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/entity/EntityModel;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p3, "x3"    # Z

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->onLikeUnlikeCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    .param p3, "x3"    # Z

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->onLikeUnlikeCommentCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->onDeleteCommentCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->message:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->onPostCommentComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;ZLcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->onLoadFeedItemLikeComplete(ZLcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$2202(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->socialActionInfoUpdated:Z

    return p1
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    .param p2, "x2"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->onGameClipLoaded(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
            "<",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286
    .local p1, "task":Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;, "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask<Lcom/microsoft/xbox/toolkit/AsyncActionStatus;>;"
    if-eqz p1, :cond_0

    .line 287
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;->cancel()V

    .line 289
    :cond_0
    return-void
.end method

.method private cancelGameClipTask()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->cancel()V

    .line 410
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    .line 412
    :cond_0
    return-void
.end method

.method private computeActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 3
    .param p1, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 516
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getData(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    move-result-object v1

    .line 517
    .local v1, "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    if-nez v1, :cond_0

    .line 518
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 524
    :goto_0
    return-object v2

    .line 520
    :cond_0
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->getActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Ljava/util/ArrayList;

    move-result-object v0

    .line 521
    .local v0, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 522
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 524
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method private createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1

    .prologue
    .line 480
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getActivityFeedActionsScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    return-object v0
.end method

.method private ensureActionType()V
    .locals 3

    .prologue
    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-nez v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 168
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adjusted action type to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_0
    return-void

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method private getSocialActionItems()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1219
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionsModel:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionsModel:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->getModel(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1220
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionsModel:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->getModel(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->getResultList()Ljava/util/ArrayList;

    move-result-object v0

    .line 1222
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLoadingFeedItemLike()Z
    .locals 1

    .prologue
    .line 1234
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemLikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemLikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLoadingGameClip()Z
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private varargs isReloadActions([Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z
    .locals 6
    .param p1, "types"    # [Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 459
    const/4 v1, 0x0

    .line 460
    .local v1, "ret":Z
    array-length v4, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, p1, v3

    .line 461
    .local v2, "type":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v0

    .line 462
    .local v0, "idx":I
    if-ltz v0, :cond_1

    .line 463
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->reloadActions:[Z

    aget-boolean v1, v5, v0

    .line 464
    if-eqz v1, :cond_1

    .line 469
    .end local v0    # "idx":I
    .end local v2    # "type":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    :cond_0
    return v1

    .line 460
    .restart local v0    # "idx":I
    .restart local v2    # "type":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private loadCommentsItemsLikesInfo(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 1213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadCommentsLikesInfoTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadCommentsItemsLikesInfoAsyncTask;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 1214
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadCommentsItemsLikesInfoAsyncTask;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getSocialActionItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadCommentsItemsLikesInfoAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadCommentsLikesInfoTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadCommentsItemsLikesInfoAsyncTask;

    .line 1215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadCommentsLikesInfoTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadCommentsItemsLikesInfoAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadCommentsItemsLikesInfoAsyncTask;->load(Z)V

    .line 1216
    return-void
.end method

.method private loadFeedItem(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 310
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->getIsBusy()Z

    move-result v0

    if-nez v0, :cond_1

    .line 311
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;

    .line 312
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->load(Z)V

    .line 314
    :cond_1
    return-void
.end method

.method private loadFeedItemActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V
    .locals 5
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .param p2, "forceRefresh"    # Z

    .prologue
    const/4 v4, 0x0

    .line 343
    const-string v1, "FeedItemActionsModel cannot be null "

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionsModel:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 344
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isActionTypeInRange(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 345
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->tasks:[Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v2

    aget-object v0, v1, v2

    .line 346
    .local v0, "t":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->getIsBusy()Z

    move-result v1

    if-nez v1, :cond_3

    .line 347
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->tasks:[Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v2

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;

    .end local v0    # "t":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;
    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$1;)V

    .restart local v0    # "t":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;
    aput-object v0, v1, v2

    .line 348
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->computeActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v1, v2, :cond_1

    .line 349
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    aput-object v3, v1, v2

    .line 351
    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object p1, v1, v4

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isReloadActions([Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 353
    const/4 p2, 0x1

    .line 354
    invoke-direct {p0, v4, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setReloadActions(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 356
    :cond_2
    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->load(Z)V

    .line 359
    .end local v0    # "t":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;
    :cond_3
    return-void
.end method

.method private loadFeedItemActions(Z)V
    .locals 8
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 326
    const/4 v0, 0x0

    .line 327
    .local v0, "currentLoaded":Z
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-eqz v4, :cond_2

    .line 328
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v1, v5, v4

    .line 329
    .local v1, "t":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    new-array v7, v2, [Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v1, v7, v3

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isReloadActions([Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 330
    invoke-direct {p0, v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    .line 331
    if-nez v0, :cond_0

    .line 332
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-ne v7, v1, :cond_1

    move v0, v2

    .line 328
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    move v0, v3

    .line 332
    goto :goto_1

    .line 337
    .end local v1    # "t":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    :cond_2
    if-nez v0, :cond_3

    .line 338
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-direct {p0, v2, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    .line 340
    :cond_3
    return-void
.end method

.method private loadFeedItemLike(Z)V
    .locals 2
    .param p1, "forcedRefresh"    # Z

    .prologue
    .line 1207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemLikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 1208
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemLikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;

    .line 1209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemLikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;->load(Z)V

    .line 1210
    return-void
.end method

.method private loadGameClip(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)V
    .locals 3
    .param p1, "key"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    .prologue
    .line 399
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelGameClipTask()V

    .line 400
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->completionHandler:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

    invoke-direct {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    .line 401
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->load(Z)V

    .line 402
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isLoadingGameClip()Z

    move-result v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->setBlocking(ZLjava/lang/String;)V

    .line 405
    :cond_0
    return-void
.end method

.method private onDeleteCommentCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 802
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isDeletingComment:Z

    .line 803
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateBlockingIndicator()V

    .line 805
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 815
    :goto_0
    :pswitch_0
    return-void

    .line 812
    :pswitch_1
    const v0, 0x7f0703dc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->showError(I)V

    goto :goto_0

    .line 805
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onFeedItemActionsComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    const/4 v3, 0x1

    .line 700
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 723
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateAdapter()V

    .line 724
    return-void

    .line 702
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateSocialCounts(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 704
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemLike(Z)V

    .line 707
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v1

    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->computeActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    aput-object v2, v0, v1

    .line 708
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-ne p2, v0, :cond_1

    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isPrepareToScroll(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 709
    invoke-virtual {p0, v3, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setScrollToBottom(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 710
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setPrepareToScroll(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 712
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getSocialActionItems()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 713
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadCommentsItemsLikesInfo(Z)V

    goto :goto_0

    .line 718
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->computeActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 719
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    aput-object v2, v0, v1

    goto :goto_0

    .line 700
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private onFeedItemComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/entity/EntityModel;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "model"    # Lcom/microsoft/xbox/service/model/entity/EntityModel;

    .prologue
    .line 727
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 751
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateAdapter()V

    .line 752
    return-void

    .line 731
    :pswitch_0
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    .line 732
    .local v0, "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    if-eqz v0, :cond_2

    .line 733
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/Entity;->getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setItem(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 734
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v1, :cond_2

    .line 735
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-nez v1, :cond_1

    .line 737
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    new-instance v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;-><init>()V

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 739
    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemActions(Z)V

    .line 742
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-nez v1, :cond_3

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->itemListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 746
    .end local v0    # "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->itemListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v1, v2, :cond_0

    .line 747
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->itemListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 727
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onGameClipLoaded(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "model"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    .param p2, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 818
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    .line 819
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-eqz v1, :cond_0

    .line 820
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isLoadingGameClip()Z

    move-result v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->setBlocking(ZLjava/lang/String;)V

    .line 822
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Loaded game clip with status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 840
    :goto_0
    return-void

    .line 827
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Loaded game clip"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->getGameClip()Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    move-result-object v0

    .line 829
    .local v0, "gameClip":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    if-eqz v1, :cond_1

    .line 830
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToGameclip(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)Z

    goto :goto_0

    .line 832
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Loaded empty game clip"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 837
    .end local v0    # "gameClip":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Failed to load game clip"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const v1, 0x7f070752

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->showError(I)V

    goto :goto_0

    .line 823
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLikeUnlikeCommentCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;Z)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "item"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    .param p3, "newLikeState"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 779
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 798
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateAdapter()V

    .line 799
    return-void

    .line 783
    :pswitch_0
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateItemLikeInfo(Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;)V

    .line 786
    :cond_0
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->socialActionInfoUpdated:Z

    goto :goto_0

    .line 790
    :pswitch_1
    const v0, 0x7f070b6d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->showError(I)V

    .line 791
    if-eqz p3, :cond_1

    .line 792
    const-string v0, "LikeUnlikeAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Like comment: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->path:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 794
    :cond_1
    const-string v0, "LikeUnlikeAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Unlike comment: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->path:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 779
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLikeUnlikeCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Z)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p3, "newLikeState"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 755
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 775
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateAdapter()V

    .line 776
    return-void

    .line 759
    :pswitch_0
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v0, :cond_0

    .line 760
    iget-object v0, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateItemLikeInfo(Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;)V

    .line 762
    :cond_0
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->socialActionInfoUpdated:Z

    .line 763
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-direct {p0, v3, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setReloadActions(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    goto :goto_0

    .line 767
    :pswitch_1
    const v0, 0x7f070b6d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->showError(I)V

    .line 768
    if-eqz p3, :cond_1

    .line 769
    const-string v0, "LikeUnlikeAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Like ActivityFeedItem: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 771
    :cond_1
    const-string v0, "LikeUnlikeAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Unlike ActivityFeedItem: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 755
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadFeedItemLikeComplete(ZLcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "isLiked"    # Z
    .param p2, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1055
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1065
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateAdapter()V

    .line 1066
    return-void

    .line 1057
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateLikeUnlike(Z)V

    goto :goto_0

    .line 1055
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onPostCommentComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1047
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1048
    const v0, 0x7f070b6e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->showError(I)V

    .line 1051
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateAdapter()V

    .line 1052
    return-void
.end method

.method private setItem(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 6
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 129
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 130
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 131
    :cond_0
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionsModel:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    .line 132
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 133
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->tasks:[Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;

    .line 134
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 135
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->reloadActions:[Z

    .line 136
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->prepareToScroll:[Z

    .line 137
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->scrollToBottom:[Z

    .line 159
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->getInstance(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionsModel:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    .line 140
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v1

    if-eqz v1, :cond_2

    new-array v1, v5, [Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v2, v1, v3

    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v2, v1, v4

    :goto_1
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 142
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    array-length v1, v1

    new-array v1, v1, [Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->tasks:[Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;

    .line 143
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    array-length v1, v1

    new-array v1, v1, [Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    array-length v1, v1

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->reloadActions:[Z

    .line 145
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    array-length v1, v1

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->prepareToScroll:[Z

    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    array-length v1, v1

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->scrollToBottom:[Z

    .line 147
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 148
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aget-object v2, v2, v0

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->computeActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    aput-object v2, v1, v0

    .line 149
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->reloadActions:[Z

    aput-boolean v3, v1, v0

    .line 150
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->prepareToScroll:[Z

    aput-boolean v3, v1, v0

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->scrollToBottom:[Z

    aput-boolean v3, v1, v0

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 140
    .end local v0    # "i":I
    :cond_2
    const/4 v1, 0x3

    new-array v1, v1, [Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v2, v1, v3

    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v2, v1, v4

    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v2, v1, v5

    goto :goto_1

    .line 153
    .restart local v0    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-nez v1, :cond_4

    .line 155
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    new-instance v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;-><init>()V

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 157
    :cond_4
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->ensureActionType()V

    goto/16 :goto_0
.end method

.method private setReloadActions(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 2
    .param p1, "reload"    # Z
    .param p2, "type"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 473
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v0

    .line 474
    .local v0, "idx":I
    if-ltz v0, :cond_0

    .line 475
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->reloadActions:[Z

    aput-boolean p1, v1, v0

    .line 477
    :cond_0
    return-void
.end method

.method private updateBlockingIndicator()V
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->updateBlockingIndicator()V

    .line 679
    return-void
.end method

.method private updateLikeUnlike(Z)V
    .locals 2
    .param p1, "isLiked"    # Z

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v0, :cond_1

    .line 1195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-nez v0, :cond_0

    .line 1196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    new-instance v1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;-><init>()V

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 1198
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setLikeUpdated(Z)V

    .line 1199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-eq v0, p1, :cond_1

    .line 1200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iput-boolean p1, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    .line 1201
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setLikeUpdated(Z)V

    .line 1204
    :cond_1
    return-void
.end method

.method private updateSocialCounts(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 3
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 686
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v1, :cond_1

    .line 687
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-nez v1, :cond_0

    .line 688
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    new-instance v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;-><init>()V

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 690
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getData(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    move-result-object v0

    .line 691
    .local v0, "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    if-eqz v0, :cond_1

    .line 692
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v2, v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->commentCount:I

    iput v2, v1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->commentCount:I

    .line 693
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v2, v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->likeCount:I

    iput v2, v1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 694
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v2, v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->shareCount:I

    iput v2, v1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    .line 697
    .end local v0    # "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    :cond_1
    return-void
.end method


# virtual methods
.method public clearShouldScrollToBottomOnLayoutChange()V
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->shouldScrollToBottomOnLayoutChange:Z

    .line 193
    return-void
.end method

.method public closeDialog()V
    .locals 0

    .prologue
    .line 557
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->goBack()V

    .line 558
    return-void
.end method

.method public deleteComment(Ljava/lang/String;)V
    .locals 3
    .param p1, "commentId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 665
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 667
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isDeletingComment:Z

    .line 668
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Ljava/lang/String;)V

    .line 669
    .local v0, "task":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;->load(Z)V

    .line 671
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateBlockingIndicator()V

    .line 673
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Activity Feed Delete Comment"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;)V

    .line 674
    return-void
.end method

.method public displayActionList(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 5
    .param p1, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 561
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "displayActionList: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 563
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-ne p1, v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v0

    if-nez v0, :cond_0

    .line 564
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "displayActionList no communication privilege"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07061d

    .line 566
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07061c

    .line 567
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 568
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 565
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 578
    :goto_0
    return-void

    .line 571
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 572
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->load(Z)V

    .line 573
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 576
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "actionType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of range"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 2
    .param p1, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 536
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isActionTypeInRange(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v1

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method public getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    return-object v0
.end method

.method public getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I
    .locals 2
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-eqz v1, :cond_1

    .line 107
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aget-object v1, v1, v0

    if-ne p1, v1, :cond_0

    .line 113
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 107
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    .end local v0    # "i":I
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getAvailableTypes()[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    return-object v0
.end method

.method public getData(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    .locals 1
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 544
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionsModel:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionsModel:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->getModel(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->getData()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    move-result-object v0

    goto :goto_0
.end method

.method public getIsCapture()Z
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->access$500(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Z

    move-result v0

    return v0
.end method

.method public getItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    return-object v0
.end method

.method public getItemListState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->itemListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->message:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->message:Ljava/lang/String;

    goto :goto_0
.end method

.method public getProfileRecentItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    return-object v0
.end method

.method public getShouldAutoFocus()Z
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Z

    move-result v0

    return v0
.end method

.method public isActionTypeInRange(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z
    .locals 2
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v0

    .line 125
    .local v0, "pos":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isLoadingFeedItemActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isLoadingFeedItem()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isLiking()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isLoadingGameClip()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isLoadingFeedItemLike()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDeletable(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)Z
    .locals 2
    .param p1, "feedItem"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .prologue
    .line 653
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-ne v0, v1, :cond_2

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->xuid:Ljava/lang/String;

    .line 654
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 653
    :goto_0
    return v0

    .line 654
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDeletingComment()Z
    .locals 1

    .prologue
    .line 682
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isDeletingComment:Z

    return v0
.end method

.method public isLikeUpdated()Z
    .locals 1

    .prologue
    .line 1226
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUpdated:Z

    return v0
.end method

.method public isLiking()Z
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUnlikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUnlikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoadingFeedItem()Z
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoadingFeedItemActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z
    .locals 3
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 548
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isActionTypeInRange(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->tasks:[Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v2

    aget-object v0, v1, v2

    .line 549
    .local v0, "t":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->getIsBusy()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 548
    .end local v0    # "t":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 549
    .restart local v0    # "t":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isPostingComment()Z
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->postCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->postCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPrepareToScroll(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z
    .locals 3
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 484
    const/4 v1, 0x0

    .line 485
    .local v1, "ret":Z
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v0

    .line 486
    .local v0, "idx":I
    if-ltz v0, :cond_0

    .line 487
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->prepareToScroll:[Z

    aget-boolean v1, v2, v0

    .line 489
    :cond_0
    return v1
.end method

.method public isReportable(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)Z
    .locals 2
    .param p1, "feedItem"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .prologue
    .line 660
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-ne v0, v1, :cond_2

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->xuid:Ljava/lang/String;

    .line 661
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 660
    :goto_0
    return v0

    .line 661
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScrollToBottom(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z
    .locals 3
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 500
    const/4 v1, 0x0

    .line 501
    .local v1, "ret":Z
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v0

    .line 502
    .local v0, "idx":I
    if-ltz v0, :cond_0

    .line 503
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->scrollToBottom:[Z

    aget-boolean v1, v2, v0

    .line 505
    :cond_0
    return v1
.end method

.method public isSocialActionInfoUpdated()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->socialActionInfoUpdated:Z

    return v0
.end method

.method public launchGameprofileIfApplicable(Ljava/lang/Object;)V
    .locals 0
    .param p1, "uncastItem"    # Ljava/lang/Object;

    .prologue
    .line 581
    invoke-static {p1, p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->launchGameProfileOrOpenLink(Ljava/lang/Object;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 582
    return-void
.end method

.method public likeComments(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V
    .locals 2
    .param p1, "comment"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .prologue
    .line 613
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v0, :cond_0

    .line 614
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUnlikeCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeCommentAsyncTask;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 615
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeCommentAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeCommentAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUnlikeCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeCommentAsyncTask;

    .line 616
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUnlikeCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeCommentAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeCommentAsyncTask;->load(Z)V

    .line 618
    :cond_0
    return-void
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 298
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->itemLocator:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-nez v0, :cond_1

    .line 300
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItem(Z)V

    .line 306
    :cond_0
    :goto_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->loadSystemTagsAsync(Z)V

    .line 307
    return-void

    .line 302
    :cond_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemActions(Z)V

    goto :goto_0
.end method

.method public navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p1, "feedbackContext"    # Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "evidenceId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "targetId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "displayName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "shouldReportToModerator"    # Z

    .prologue
    .line 593
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 594
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 595
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 596
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 598
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;-><init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 599
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->access$800(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->trySetActivityFeedContextFromScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 601
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 602
    return-void
.end method

.method public navigateToFeedItem()V
    .locals 5

    .prologue
    .line 362
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v1, :cond_2

    .line 365
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 366
    .local v0, "feedItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 389
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V

    .line 396
    .end local v0    # "feedItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :goto_0
    return-void

    .line 368
    .restart local v0    # "feedItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :pswitch_0
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 370
    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToAchievementDetails(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    goto :goto_0

    .line 372
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getAuthorInfoForAchievementComparison()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-result-object v1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->titleId:Ljava/lang/String;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->platform:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateAchievementToComparison(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 377
    :pswitch_1
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastProvider:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchBroadcastingVideo(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 381
    :pswitch_2
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipScid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 382
    const v1, 0x7f0704c7

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->showError(I)V

    goto :goto_0

    .line 384
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getXuidForLoadingGameClip()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipScid:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipId:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadGameClip(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)V

    goto :goto_0

    .line 394
    .end local v0    # "feedItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Cannot navigate to non-existent item"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 366
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public navigateToProfile(Ljava/lang/String;)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 585
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 586
    return-void
.end method

.method public onAnimateInCompleted()V
    .locals 0

    .prologue
    .line 118
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onAnimateInCompleted()V

    .line 119
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateAdapter()V

    .line 120
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 420
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onDestroy()V

    .line 421
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->tasks:[Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;

    if-eqz v1, :cond_1

    .line 422
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->tasks:[Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 423
    .local v0, "t":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;
    if-eqz v0, :cond_0

    .line 424
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->cancel()V

    .line 422
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 428
    .end local v0    # "t":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;
    :cond_1
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 263
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 251
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 258
    :cond_1
    return-void
.end method

.method protected onStopOverride()V
    .locals 6

    .prologue
    .line 267
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 268
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->tasks:[Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 269
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->availableTypes:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 270
    .local v0, "type":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->tasks:[Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v5

    aget-object v4, v4, v5

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 269
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 273
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadFeedItemLikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadFeedItemLikeAsyncTask;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 274
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadCommentsLikesInfoTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LoadCommentsItemsLikesInfoAsyncTask;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 275
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 276
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUnlikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 277
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUnlikeCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeCommentAsyncTask;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 278
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->postCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 280
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v1, :cond_1

    .line 281
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 283
    :cond_1
    return-void
.end method

.method public postComment()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 196
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->postCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->postCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->cancel()V

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackActivityFeedCommentPost(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;

    invoke-direct {v0, p0, v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->postCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;

    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->postCommentTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->load(Z)V

    .line 205
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->updateAdapter()V

    .line 218
    :goto_0
    return-void

    .line 207
    :cond_1
    const v0, 0x7f070b6e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->showError(I)V

    .line 208
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Cannot start comment flow without loaded item"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 211
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "postComment() no communication privilege"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    .line 213
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f07061d

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 214
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f07061c

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 215
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f0707c7

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 212
    invoke-interface {v0, v1, v2, v3, v5}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public resetSocialActionInfoUpdated()V
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->socialActionInfoUpdated:Z

    .line 238
    return-void
.end method

.method public restoreSoftInputAdjustMode()V
    .locals 2

    .prologue
    .line 179
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 180
    .local v0, "wnd":Landroid/view/Window;
    iget v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->oldInputMode:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 181
    return-void
.end method

.method public saveAndAdjustSoftInputAdjustMode()V
    .locals 2

    .prologue
    .line 173
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 174
    .local v0, "wnd":Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->oldInputMode:I

    .line 175
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 176
    return-void
.end method

.method public setLikeUpdated(Z)V
    .locals 0
    .param p1, "newVal"    # Z

    .prologue
    .line 1230
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUpdated:Z

    .line 1231
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->message:Ljava/lang/String;

    .line 222
    return-void
.end method

.method public setPrepareToScroll(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 2
    .param p1, "prepare"    # Z
    .param p2, "type"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 493
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v0

    .line 494
    .local v0, "idx":I
    if-ltz v0, :cond_0

    .line 495
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->prepareToScroll:[Z

    aput-boolean p1, v1, v0

    .line 497
    :cond_0
    return-void
.end method

.method public setScrollToBottom(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 2
    .param p1, "scroll"    # Z
    .param p2, "type"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 509
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v0

    .line 510
    .local v0, "idx":I
    if-ltz v0, :cond_0

    .line 511
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->scrollToBottom:[Z

    aput-boolean p1, v1, v0

    .line 513
    :cond_0
    return-void
.end method

.method public setShouldScrollToBottomOnLayoutChange()V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->shouldScrollToBottomOnLayoutChange:Z

    .line 185
    return-void
.end method

.method public shouldScrollToBottomOnLayoutChange()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->shouldScrollToBottomOnLayoutChange:Z

    return v0
.end method

.method public startCommentFlow()V
    .locals 6

    .prologue
    .line 621
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v1, :cond_1

    .line 622
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackActivityFeedComment(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 624
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setShouldScrollToBottomOnLayoutChange()V

    .line 625
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->shouldShowKeyboard(Z)V

    .line 635
    :goto_0
    return-void

    .line 627
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "User has no communication privileges"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 629
    .local v0, "r":Landroid/content/res/Resources;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    const v2, 0x7f07061d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f070110

    .line 630
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0707c7

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 629
    invoke-interface {v1, v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 633
    .end local v0    # "r":Landroid/content/res/Resources;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Cannot start comment flow without loaded item"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startLikeFlow(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 2
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 605
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUnlikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->cancelAsyncTask(Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;)V

    .line 607
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUnlikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;

    .line 608
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeUnlikeTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$LikeUnlikeAsyncTask;->load(Z)V

    .line 610
    :cond_0
    return-void
.end method

.method public startShareFlow()V
    .locals 6

    .prologue
    .line 638
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v1, :cond_1

    .line 639
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 640
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->shareRoot:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v3

    invoke-virtual {v1, p0, v2, v3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    .line 649
    :goto_0
    return-void

    .line 642
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 643
    .local v0, "r":Landroid/content/res/Resources;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    const v2, 0x7f07061d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f070110

    .line 644
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0707c7

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 643
    invoke-interface {v1, v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 647
    .end local v0    # "r":Landroid/content/res/Resources;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Cannot start share flow without loaded item"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 432
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 433
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 434
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 435
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 453
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isActive:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v2, v1, v4

    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v2, v1, v3

    const/4 v2, 0x2

    sget-object v3, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v3, v1, v2

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isReloadActions([Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 454
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->load(Z)V

    .line 456
    :cond_1
    return-void

    .line 437
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {p0, v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setPrepareToScroll(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 438
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->setCommentText(Ljava/lang/String;)V

    .line 440
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-direct {p0, v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setReloadActions(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    goto :goto_0

    .line 443
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {p0, v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setPrepareToScroll(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 445
    :pswitch_3
    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-direct {p0, v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setReloadActions(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    goto :goto_0

    .line 448
    :pswitch_4
    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-direct {p0, v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setReloadActions(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    goto :goto_0

    .line 435
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
