.class public final enum Lcom/microsoft/xbox/service/model/JMediaState;
.super Ljava/lang/Enum;
.source "JMediaState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/JMediaState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/JMediaState;

.field public static final enum MediaState_Buffering:Lcom/microsoft/xbox/service/model/JMediaState;

.field public static final enum MediaState_Invalid:Lcom/microsoft/xbox/service/model/JMediaState;

.field public static final enum MediaState_NoMedia:Lcom/microsoft/xbox/service/model/JMediaState;

.field public static final enum MediaState_Paused:Lcom/microsoft/xbox/service/model/JMediaState;

.field public static final enum MediaState_Playing:Lcom/microsoft/xbox/service/model/JMediaState;

.field public static final enum MediaState_Starting:Lcom/microsoft/xbox/service/model/JMediaState;

.field public static final enum MediaState_Stopped:Lcom/microsoft/xbox/service/model/JMediaState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5
    new-instance v0, Lcom/microsoft/xbox/service/model/JMediaState;

    const-string v1, "MediaState_NoMedia"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/model/JMediaState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_NoMedia:Lcom/microsoft/xbox/service/model/JMediaState;

    .line 6
    new-instance v0, Lcom/microsoft/xbox/service/model/JMediaState;

    const-string v1, "MediaState_Invalid"

    invoke-direct {v0, v1, v5, v4}, Lcom/microsoft/xbox/service/model/JMediaState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Invalid:Lcom/microsoft/xbox/service/model/JMediaState;

    .line 7
    new-instance v0, Lcom/microsoft/xbox/service/model/JMediaState;

    const-string v1, "MediaState_Stopped"

    invoke-direct {v0, v1, v6, v5}, Lcom/microsoft/xbox/service/model/JMediaState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Stopped:Lcom/microsoft/xbox/service/model/JMediaState;

    .line 8
    new-instance v0, Lcom/microsoft/xbox/service/model/JMediaState;

    const-string v1, "MediaState_Starting"

    invoke-direct {v0, v1, v7, v6}, Lcom/microsoft/xbox/service/model/JMediaState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Starting:Lcom/microsoft/xbox/service/model/JMediaState;

    .line 9
    new-instance v0, Lcom/microsoft/xbox/service/model/JMediaState;

    const-string v1, "MediaState_Playing"

    invoke-direct {v0, v1, v8, v7}, Lcom/microsoft/xbox/service/model/JMediaState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Playing:Lcom/microsoft/xbox/service/model/JMediaState;

    .line 10
    new-instance v0, Lcom/microsoft/xbox/service/model/JMediaState;

    const-string v1, "MediaState_Paused"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/microsoft/xbox/service/model/JMediaState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Paused:Lcom/microsoft/xbox/service/model/JMediaState;

    .line 11
    new-instance v0, Lcom/microsoft/xbox/service/model/JMediaState;

    const-string v1, "MediaState_Buffering"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/JMediaState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Buffering:Lcom/microsoft/xbox/service/model/JMediaState;

    .line 3
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/JMediaState;

    sget-object v1, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_NoMedia:Lcom/microsoft/xbox/service/model/JMediaState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Invalid:Lcom/microsoft/xbox/service/model/JMediaState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Stopped:Lcom/microsoft/xbox/service/model/JMediaState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Starting:Lcom/microsoft/xbox/service/model/JMediaState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Playing:Lcom/microsoft/xbox/service/model/JMediaState;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Paused:Lcom/microsoft/xbox/service/model/JMediaState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Buffering:Lcom/microsoft/xbox/service/model/JMediaState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/model/JMediaState;->$VALUES:[Lcom/microsoft/xbox/service/model/JMediaState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "v"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17
    iput p3, p0, Lcom/microsoft/xbox/service/model/JMediaState;->value:I

    .line 18
    return-void
.end method

.method public static fromOrdinal(I)Lcom/microsoft/xbox/service/model/JMediaState;
    .locals 5
    .param p0, "v"    # I

    .prologue
    .line 21
    invoke-static {}, Lcom/microsoft/xbox/service/model/JMediaState;->values()[Lcom/microsoft/xbox/service/model/JMediaState;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 22
    .local v0, "state":Lcom/microsoft/xbox/service/model/JMediaState;
    iget v4, v0, Lcom/microsoft/xbox/service/model/JMediaState;->value:I

    if-ne v4, p0, :cond_0

    .line 26
    .end local v0    # "state":Lcom/microsoft/xbox/service/model/JMediaState;
    :goto_1
    return-object v0

    .line 21
    .restart local v0    # "state":Lcom/microsoft/xbox/service/model/JMediaState;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 26
    .end local v0    # "state":Lcom/microsoft/xbox/service/model/JMediaState;
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/JMediaState;->MediaState_Invalid:Lcom/microsoft/xbox/service/model/JMediaState;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/JMediaState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/service/model/JMediaState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/JMediaState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/JMediaState;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/service/model/JMediaState;->$VALUES:[Lcom/microsoft/xbox/service/model/JMediaState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/JMediaState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/JMediaState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/microsoft/xbox/service/model/JMediaState;->value:I

    return v0
.end method
