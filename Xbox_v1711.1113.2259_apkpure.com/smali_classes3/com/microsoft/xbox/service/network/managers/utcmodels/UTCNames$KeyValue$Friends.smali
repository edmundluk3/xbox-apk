.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue$Friends;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Friends"
.end annotation


# static fields
.field public static final All:Ljava/lang/String; = "All"

.field public static final AllPeople:Ljava/lang/String; = "AllPeople"

.field public static final ClubsFirst:Ljava/lang/String; = "ClubsFirst"

.field public static final FacebookFriends:Ljava/lang/String; = "Facebook Friends"

.field public static final Favorites:Ljava/lang/String; = "Favorites"

.field public static final Followers:Ljava/lang/String; = "RecentPlayers"

.field public static final FriendsFirst:Ljava/lang/String; = "FriendsFirst"

.field public static final PeopleYouMayKnow:Ljava/lang/String; = "People you may know"

.field public static final PhoneFriends:Ljava/lang/String; = "Phone Friends"

.field public static final RecentPlayers:Ljava/lang/String; = "RecentPlayers"

.field public static final VIPs:Ljava/lang/String; = "VIPs"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
