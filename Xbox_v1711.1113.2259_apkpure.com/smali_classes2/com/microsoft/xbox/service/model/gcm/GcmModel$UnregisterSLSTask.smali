.class Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;
.super Landroid/os/AsyncTask;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gcm/GcmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UnregisterSLSTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/microsoft/xbox/toolkit/AsyncResult",
        "<",
        "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
        ">;>;"
    }
.end annotation


# instance fields
.field private curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

.field private final isLogin:Z

.field private lastXTokenString:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Z)V
    .locals 0
    .param p2, "isLogin"    # Z

    .prologue
    .line 1258
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1259
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->isLogin:Z

    .line 1260
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1273
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1400()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->endpointId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->lastXTokenString:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->disableNotifications(Ljava/lang/String;Ljava/lang/String;)V

    .line 1276
    new-instance v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget v4, v4, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->flags:I

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1281
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v1, v2, v3, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    :goto_0
    return-object v1

    .line 1277
    :catch_0
    move-exception v0

    .line 1278
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v1, v2, v3, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1252
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->doInBackground([Ljava/lang/Void;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1286
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;>;"
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1287
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1288
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1289
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->isSLSRegistered()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1290
    const-string v1, "endpoint.id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1291
    const-string v1, "endpoint.flags"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1292
    const-string v1, "last.sls.registration.time.04"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1293
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Successfully unregistered with SLS"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1303
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1305
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$2202(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 1306
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1308
    .end local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void

    .line 1295
    .restart local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_1
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->isLogin:Z

    if-nez v1, :cond_2

    .line 1299
    const-string v1, "user.flags"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->flags:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1301
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to unregister from SLS"

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1252
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 1264
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1265
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1200(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    .line 1266
    const-string v0, "https://xboxlive.com"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ESServiceManager;->getLastXTokenString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->lastXTokenString:Ljava/lang/String;

    .line 1267
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1268
    return-void
.end method
