.class public Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;
.super Ljava/lang/Object;
.source "IProfileShowcaseResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Thumbnail"
.end annotation


# instance fields
.field private imageUri:Ljava/lang/String;

.field public uri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getImageUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->imageUri:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->uri:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->uri:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->imageUri:Ljava/lang/String;

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->imageUri:Ljava/lang/String;

    return-object v0
.end method
