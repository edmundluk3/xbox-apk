.class Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "SettingsGeneralPageViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChangeUserShareIdentityAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private setting:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

.field private shareIdentityStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V
    .locals 1
    .param p2, "shareIdentityStatus"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .prologue
    .line 653
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 654
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->shareIdentityStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 655
    sget-object v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;->ShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->setting:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    .line 656
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;)V
    .locals 0
    .param p2, "shareIdentityStatus"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    .param p3, "setting"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    .prologue
    .line 658
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 659
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->shareIdentityStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 660
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->setting:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    .line 661
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 665
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->shareIdentityStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .line 666
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    .line 665
    :goto_0
    return v0

    .line 666
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 696
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 697
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 698
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->setting:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->shareIdentityStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->setUserShareIdentityStatus(ZLcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 699
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_1

    .line 705
    :cond_0
    :goto_0
    return-object v0

    .line 702
    :cond_1
    const-string v1, "SettingsActivityViewModel"

    const-string v2, "Failed to add user to share identity list"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 649
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 691
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 649
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 671
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$1000(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 672
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 683
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$1000(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 686
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->shareIdentityStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->setting:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing;->trackRealNameSharingToggle(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 649
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 676
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 677
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$1102(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Z)Z

    .line 678
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 679
    return-void
.end method
