.class public Lcom/microsoft/cll/android/SnapshotScheduler;
.super Lcom/microsoft/cll/android/ScheduledWorker;
.source "SnapshotScheduler.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

.field private final cll:Lcom/microsoft/cll/android/ICll;

.field private final logger:Lcom/microsoft/cll/android/ILogger;


# direct methods
.method public constructor <init>(Lcom/microsoft/cll/android/ClientTelemetry;Lcom/microsoft/cll/android/ILogger;Lcom/microsoft/cll/android/ICll;)V
    .locals 2
    .param p1, "clientTelemetry"    # Lcom/microsoft/cll/android/ClientTelemetry;
    .param p2, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p3, "cll"    # Lcom/microsoft/cll/android/ICll;

    .prologue
    .line 18
    sget-object v0, Lcom/microsoft/cll/android/SettingsStore$Settings;->SNAPSHOTSCHEDULEINTERVAL:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v0}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsLong(Lcom/microsoft/cll/android/SettingsStore$Settings;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/cll/android/ScheduledWorker;-><init>(J)V

    .line 12
    const-string v0, "SnapshotScheduler"

    iput-object v0, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->TAG:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->cll:Lcom/microsoft/cll/android/ICll;

    .line 20
    iput-object p1, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    .line 21
    iput-object p2, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->logger:Lcom/microsoft/cll/android/ILogger;

    .line 22
    return-void
.end method

.method private recordStatistics()V
    .locals 3

    .prologue
    .line 56
    iget-object v1, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    invoke-virtual {v1}, Lcom/microsoft/cll/android/ClientTelemetry;->GetEvent()LMicrosoft/Android/LoggingLibrary/Snapshot;

    move-result-object v0

    .line 57
    .local v0, "snapshot":LMicrosoft/Android/LoggingLibrary/Snapshot;
    iget-object v1, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->cll:Lcom/microsoft/cll/android/ICll;

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/microsoft/cll/android/EventSensitivity;

    invoke-interface {v1, v0, v2}, Lcom/microsoft/cll/android/ICll;->log(Lcom/microsoft/telemetry/Base;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 61
    iget-object v1, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    invoke-virtual {v1}, Lcom/microsoft/cll/android/ClientTelemetry;->Reset()V

    .line 62
    return-void
.end method


# virtual methods
.method public resume(Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 7
    .param p1, "executor"    # Ljava/util/concurrent/ScheduledExecutorService;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 36
    iget-wide v2, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->interval:J

    iget-wide v4, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->interval:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->nextExecution:Ljava/util/concurrent/ScheduledFuture;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->isPaused:Z

    .line 38
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v1, "SnapshotScheduler"

    const-string v2, "Uploading snapshot"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    iget-wide v0, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->interval:J

    sget-object v2, Lcom/microsoft/cll/android/SettingsStore$Settings;->SNAPSHOTSCHEDULEINTERVAL:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v2}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->nextExecution:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 47
    sget-object v0, Lcom/microsoft/cll/android/SettingsStore$Settings;->SNAPSHOTSCHEDULEINTERVAL:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v0}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->interval:J

    .line 48
    iget-object v0, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    iget-wide v2, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->interval:J

    iget-wide v4, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->interval:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->nextExecution:Ljava/util/concurrent/ScheduledFuture;

    .line 51
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/cll/android/SnapshotScheduler;->recordStatistics()V

    .line 52
    return-void
.end method

.method public start(Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 7
    .param p1, "executor"    # Ljava/util/concurrent/ScheduledExecutorService;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 30
    iget-wide v2, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->interval:J

    iget-wide v4, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->interval:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/cll/android/SnapshotScheduler;->nextExecution:Ljava/util/concurrent/ScheduledFuture;

    .line 31
    return-void
.end method
