.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;
.super Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;
.source "$AutoValue_ClubAccountsDataTypes_OwnedClub.java"


# instance fields
.field private final id:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final owner:Ljava/lang/String;

.field private final type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "owner"    # Ljava/lang/String;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "type"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;-><init>()V

    .line 20
    if-nez p1, :cond_0

    .line 21
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null name"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->name:Ljava/lang/String;

    .line 24
    if-nez p2, :cond_1

    .line 25
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null owner"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->owner:Ljava/lang/String;

    .line 28
    if-nez p3, :cond_2

    .line 29
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null id"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->id:Ljava/lang/String;

    .line 32
    if-nez p4, :cond_3

    .line 33
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 36
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 74
    if-ne p1, p0, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v1

    .line 77
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 78
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;

    .line 79
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->name:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->owner:Ljava/lang/String;

    .line 80
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;->owner()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->id:Ljava/lang/String;

    .line 81
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;->id()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 82
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;
    :cond_3
    move v1, v2

    .line 84
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 89
    const/4 v0, 0x1

    .line 90
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 92
    mul-int/2addr v0, v2

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->owner:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 94
    mul-int/2addr v0, v2

    .line 95
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 96
    mul-int/2addr v0, v2

    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 98
    return v0
.end method

.method public id()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->id:Ljava/lang/String;

    return-object v0
.end method

.method public name()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->name:Ljava/lang/String;

    return-object v0
.end method

.method public owner()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->owner:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OwnedClub{name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", owner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->owner:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_OwnedClub;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    return-object v0
.end method
