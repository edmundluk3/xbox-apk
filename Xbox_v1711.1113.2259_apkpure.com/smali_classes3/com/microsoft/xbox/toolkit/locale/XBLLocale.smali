.class public Lcom/microsoft/xbox/toolkit/locale/XBLLocale;
.super Ljava/lang/Object;
.source "XBLLocale.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/locale/XBLLocale$container;
    }
.end annotation


# static fields
.field private static final connector:Ljava/lang/String; = "-"

.field private static final defaultLocale:Ljava/lang/String; = "en-us"


# instance fields
.field private localeMap:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/locale/XBLLocale;->localeMap:Ljava/util/Hashtable;

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/locale/XBLLocale$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/locale/XBLLocale$1;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/locale/XBLLocale;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/toolkit/locale/XBLLocale;
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/microsoft/xbox/toolkit/locale/XBLLocale$container;->access$000()Lcom/microsoft/xbox/toolkit/locale/XBLLocale;

    move-result-object v0

    return-object v0
.end method

.method private getKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "countryRegion"    # Ljava/lang/String;
    .param p2, "language"    # Ljava/lang/String;

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 91
    .local v0, "key":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 97
    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 99
    return-object v0

    .line 94
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public Initialize(Lcom/microsoft/xbox/toolkit/locale/LocaleConfig;)V
    .locals 5
    .param p1, "config"    # Lcom/microsoft/xbox/toolkit/locale/LocaleConfig;

    .prologue
    .line 58
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 59
    if-nez p1, :cond_1

    .line 69
    :cond_0
    return-void

    .line 63
    :cond_1
    iget-object v2, p1, Lcom/microsoft/xbox/toolkit/locale/LocaleConfig;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/locale/LocaleConfigItem;

    .line 64
    .local v0, "item":Lcom/microsoft/xbox/toolkit/locale/LocaleConfigItem;
    iget-object v3, v0, Lcom/microsoft/xbox/toolkit/locale/LocaleConfigItem;->CountryRegion:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/toolkit/locale/LocaleConfigItem;->Language:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/microsoft/xbox/toolkit/locale/XBLLocale;->getKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "key":Ljava/lang/String;
    if-eqz v1, :cond_2

    iget-object v3, v0, Lcom/microsoft/xbox/toolkit/locale/LocaleConfigItem;->Locale:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 66
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/locale/XBLLocale;->localeMap:Ljava/util/Hashtable;

    iget-object v4, v0, Lcom/microsoft/xbox/toolkit/locale/LocaleConfigItem;->Locale:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public Initialize(Ljava/io/InputStream;)V
    .locals 4
    .param p1, "input"    # Ljava/io/InputStream;

    .prologue
    .line 45
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 46
    if-eqz p1, :cond_0

    .line 48
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XMLHelper;->instance()Lcom/microsoft/xbox/toolkit/XMLHelper;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/toolkit/locale/LocaleConfig;

    invoke-virtual {v2, p1, v3}, Lcom/microsoft/xbox/toolkit/XMLHelper;->load(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/locale/LocaleConfig;

    .line 49
    .local v0, "config":Lcom/microsoft/xbox/toolkit/locale/LocaleConfig;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/locale/XBLLocale;->Initialize(Lcom/microsoft/xbox/toolkit/locale/LocaleConfig;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    .end local v0    # "config":Lcom/microsoft/xbox/toolkit/locale/LocaleConfig;
    :cond_0
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v2, "XBLLocale"

    const-string v3, "Failed to deserialize the locale input"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getSupportedLocale(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "countryRegion"    # Ljava/lang/String;
    .param p2, "language"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/locale/XBLLocale;->getKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "key":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/locale/XBLLocale;->localeMap:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/locale/XBLLocale;->localeMap:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 85
    :goto_0
    return-object v1

    .line 80
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/toolkit/locale/XBLLocale;->getKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/locale/XBLLocale;->localeMap:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/locale/XBLLocale;->localeMap:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0

    .line 84
    :cond_1
    const-string v1, "XBLLocale"

    const-string v2, "Using default locale"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v1, "en-us"

    goto :goto_0
.end method
