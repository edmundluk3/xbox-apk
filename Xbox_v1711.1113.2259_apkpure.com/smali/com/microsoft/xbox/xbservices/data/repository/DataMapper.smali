.class public interface abstract Lcom/microsoft/xbox/xbservices/data/repository/DataMapper;
.super Ljava/lang/Object;
.source "DataMapper.java"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SOURCE_TYPE:",
        "Ljava/lang/Object;",
        "TARGET_TYPE:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function",
        "<TSOURCE_TYPE;",
        "Lio/reactivex/Observable",
        "<TTARGET_TYPE;>;>;"
    }
.end annotation

.annotation runtime Ljava/lang/FunctionalInterface;
.end annotation


# virtual methods
.method public abstract apply(Ljava/lang/Object;)Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TSOURCE_TYPE;)",
            "Lio/reactivex/Observable",
            "<TTARGET_TYPE;>;"
        }
    .end annotation
.end method
