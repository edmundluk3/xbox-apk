.class public abstract Lcom/microsoft/cll/android/AbstractSettings;
.super Ljava/lang/Object;
.source "AbstractSettings.java"


# instance fields
.field protected ETagSettingName:Lcom/microsoft/cll/android/SettingsStore$Settings;

.field protected TAG:Ljava/lang/String;

.field protected final clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

.field protected disableUploadOn404:Z

.field protected endpoint:Ljava/lang/String;

.field protected final logger:Lcom/microsoft/cll/android/ILogger;

.field private final partA:Lcom/microsoft/cll/android/PartA;


# direct methods
.method protected constructor <init>(Lcom/microsoft/cll/android/ClientTelemetry;Lcom/microsoft/cll/android/ILogger;Lcom/microsoft/cll/android/PartA;)V
    .locals 1
    .param p1, "clientTelemetry"    # Lcom/microsoft/cll/android/ClientTelemetry;
    .param p2, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p3, "partA"    # Lcom/microsoft/cll/android/PartA;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string v0, "AbstractSettings"

    iput-object v0, p0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/cll/android/AbstractSettings;->disableUploadOn404:Z

    .line 32
    iput-object p1, p0, Lcom/microsoft/cll/android/AbstractSettings;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    .line 33
    iput-object p2, p0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    .line 34
    iput-object p3, p0, Lcom/microsoft/cll/android/AbstractSettings;->partA:Lcom/microsoft/cll/android/PartA;

    .line 35
    return-void
.end method


# virtual methods
.method public abstract ParseSettings(Lorg/json/JSONObject;)V
.end method

.method protected getQueryParameters()Ljava/lang/String;
    .locals 2

    .prologue
    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 148
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 149
    const-string v1, "os="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    iget-object v1, p0, Lcom/microsoft/cll/android/AbstractSettings;->partA:Lcom/microsoft/cll/android/PartA;

    iget-object v1, v1, Lcom/microsoft/cll/android/PartA;->osName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    const-string v1, "&osVer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    iget-object v1, p0, Lcom/microsoft/cll/android/AbstractSettings;->partA:Lcom/microsoft/cll/android/PartA;

    iget-object v1, v1, Lcom/microsoft/cll/android/PartA;->osVer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const-string v1, "&deviceClass="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    iget-object v1, p0, Lcom/microsoft/cll/android/AbstractSettings;->partA:Lcom/microsoft/cll/android/PartA;

    iget-object v1, v1, Lcom/microsoft/cll/android/PartA;->deviceExt:Lcom/microsoft/telemetry/extensions/device;

    invoke-virtual {v1}, Lcom/microsoft/telemetry/extensions/device;->getDeviceClass()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    const-string v1, "&deviceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    iget-object v1, p0, Lcom/microsoft/cll/android/AbstractSettings;->partA:Lcom/microsoft/cll/android/PartA;

    iget-object v1, v1, Lcom/microsoft/cll/android/PartA;->deviceExt:Lcom/microsoft/telemetry/extensions/device;

    invoke-virtual {v1}, Lcom/microsoft/telemetry/extensions/device;->getLocalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getSettings()Lorg/json/JSONObject;
    .locals 20

    .prologue
    .line 41
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "Get Settings"

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :try_start_0
    new-instance v13, Ljava/net/URL;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->endpoint:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/cll/android/AbstractSettings;->getQueryParameters()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v13, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    .local v13, "url":Ljava/net/URL;
    const/4 v3, 0x0

    .line 54
    .local v3, "connection":Ljava/net/URLConnection;
    :try_start_1
    invoke-virtual {v13}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    .line 55
    instance-of v0, v3, Ljavax/net/ssl/HttpsURLConnection;

    move/from16 v16, v0

    if-eqz v16, :cond_a

    .line 56
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/cll/android/ClientTelemetry;->IncrementSettingsHttpAttempts()V

    .line 57
    move-object v0, v3

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    move-object v7, v0

    .line 58
    .local v7, "httpConnection":Ljavax/net/ssl/HttpsURLConnection;
    sget-object v16, Lcom/microsoft/cll/android/SettingsStore$Settings;->HTTPTIMEOUTINTERVAL:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static/range {v16 .. v16}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v7, v0}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    .line 59
    const-string v16, "GET"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 60
    const-string v16, "Accept"

    const-string v17, "application/json"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v16, "If-None-Match"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->ETagSettingName:Lcom/microsoft/cll/android/SettingsStore$Settings;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsString(Lcom/microsoft/cll/android/SettingsStore$Settings;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v16, "UTC"

    invoke-static/range {v16 .. v16}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v16

    sget-object v17, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static/range {v16 .. v17}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v14

    .line 64
    .local v14, "start":J
    invoke-virtual {v7}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 65
    const-string v16, "UTC"

    invoke-static/range {v16 .. v16}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v16

    sget-object v17, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static/range {v16 .. v17}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 66
    .local v8, "finish":J
    sub-long v4, v8, v14

    .line 67
    .local v4, "diff":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    move-object/from16 v16, v0

    long-to-int v0, v4

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/microsoft/cll/android/ClientTelemetry;->SetAvgSettingsLatencyMs(I)V

    .line 68
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    move-object/from16 v16, v0

    long-to-int v0, v4

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/microsoft/cll/android/ClientTelemetry;->SetMaxSettingsLatencyMs(I)V

    .line 70
    invoke-virtual {v7}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v16

    const/16 v17, 0x194

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->disableUploadOn404:Z

    move/from16 v16, v0

    if-eqz v16, :cond_4

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "Your iKey is invalid. Your events will not be sent!"

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    sget-object v16, Lcom/microsoft/cll/android/SettingsStore$Settings;->UPLOADENABLED:Lcom/microsoft/cll/android/SettingsStore$Settings;

    const-string v17, "false"

    invoke-static/range {v16 .. v17}, Lcom/microsoft/cll/android/SettingsStore;->updateCllSetting(Lcom/microsoft/cll/android/SettingsStore$Settings;Ljava/lang/String;)V

    .line 83
    :cond_0
    :goto_0
    invoke-virtual {v7}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v16

    const/16 v17, 0xc8

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_1

    invoke-virtual {v7}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v16

    const/16 v17, 0x130

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_6

    .line 84
    :cond_1
    const-string v16, "ETAG"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljavax/net/ssl/HttpsURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "ETag":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v16

    if-nez v16, :cond_2

    .line 86
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->ETagSettingName:Lcom/microsoft/cll/android/SettingsStore$Settings;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Lcom/microsoft/cll/android/SettingsStore;->updateCllSetting(Lcom/microsoft/cll/android/SettingsStore$Settings;Ljava/lang/String;)V

    .line 93
    .end local v2    # "ETag":Ljava/lang/String;
    :cond_2
    :goto_1
    invoke-virtual {v7}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v16

    const/16 v17, 0xc8

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_7

    .line 94
    invoke-virtual {v7}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    const/4 v7, 0x0

    .line 98
    const/4 v3, 0x0

    .line 99
    const/16 v16, 0x0

    .line 125
    if-eqz v3, :cond_3

    .line 129
    :try_start_2
    invoke-virtual {v3}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 139
    .end local v3    # "connection":Ljava/net/URLConnection;
    .end local v4    # "diff":J
    .end local v7    # "httpConnection":Ljavax/net/ssl/HttpsURLConnection;
    .end local v8    # "finish":J
    .end local v13    # "url":Ljava/net/URL;
    .end local v14    # "start":J
    :cond_3
    :goto_2
    return-object v16

    .line 46
    :catch_0
    move-exception v6

    .line 47
    .local v6, "e":Ljava/net/MalformedURLException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "Settings URL is invalid"

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const/16 v16, 0x0

    goto :goto_2

    .line 74
    .end local v6    # "e":Ljava/net/MalformedURLException;
    .restart local v3    # "connection":Ljava/net/URLConnection;
    .restart local v4    # "diff":J
    .restart local v7    # "httpConnection":Ljavax/net/ssl/HttpsURLConnection;
    .restart local v8    # "finish":J
    .restart local v13    # "url":Ljava/net/URL;
    .restart local v14    # "start":J
    :cond_4
    :try_start_3
    invoke-virtual {v7}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v16

    const/16 v17, 0x194

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->disableUploadOn404:Z

    move/from16 v16, v0

    if-eqz v16, :cond_0

    .line 78
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "Your iKey is valid."

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    sget-object v16, Lcom/microsoft/cll/android/SettingsStore$Settings;->UPLOADENABLED:Lcom/microsoft/cll/android/SettingsStore$Settings;

    const-string v17, "true"

    invoke-static/range {v16 .. v17}, Lcom/microsoft/cll/android/SettingsStore;->updateCllSetting(Lcom/microsoft/cll/android/SettingsStore$Settings;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 118
    .end local v4    # "diff":J
    .end local v7    # "httpConnection":Ljavax/net/ssl/HttpsURLConnection;
    .end local v8    # "finish":J
    .end local v14    # "start":J
    :catch_1
    move-exception v6

    .line 119
    .local v6, "e":Ljava/io/IOException;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    move-object/from16 v16, v0

    const/16 v17, -0x1

    invoke-virtual/range {v16 .. v17}, Lcom/microsoft/cll/android/ClientTelemetry;->IncrementSettingsHttpFailures(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 125
    if-eqz v3, :cond_5

    .line 129
    :try_start_5
    invoke-virtual {v3}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    .line 139
    .end local v6    # "e":Ljava/io/IOException;
    :cond_5
    :goto_3
    const/16 v16, 0x0

    goto :goto_2

    .line 89
    .restart local v4    # "diff":J
    .restart local v7    # "httpConnection":Ljavax/net/ssl/HttpsURLConnection;
    .restart local v8    # "finish":J
    .restart local v14    # "start":J
    :cond_6
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    move-object/from16 v16, v0

    invoke-virtual {v7}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Lcom/microsoft/cll/android/ClientTelemetry;->IncrementSettingsHttpFailures(I)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 121
    .end local v4    # "diff":J
    .end local v7    # "httpConnection":Ljavax/net/ssl/HttpsURLConnection;
    .end local v8    # "finish":J
    .end local v14    # "start":J
    :catch_2
    move-exception v6

    .line 122
    .local v6, "e":Lorg/json/JSONException;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 125
    if-eqz v3, :cond_5

    .line 129
    :try_start_8
    invoke-virtual {v3}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_3

    .line 131
    :catch_3
    move-exception v6

    .line 134
    .local v6, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 131
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v4    # "diff":J
    .restart local v7    # "httpConnection":Ljavax/net/ssl/HttpsURLConnection;
    .restart local v8    # "finish":J
    .restart local v14    # "start":J
    :catch_4
    move-exception v6

    .line 134
    .restart local v6    # "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-interface/range {v17 .. v19}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 102
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_7
    :try_start_9
    new-instance v10, Ljava/io/BufferedReader;

    new-instance v16, Ljava/io/InputStreamReader;

    invoke-virtual {v7}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 104
    .local v10, "input":Ljava/io/BufferedReader;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .local v12, "result":Ljava/lang/StringBuilder;
    :goto_4
    invoke-virtual {v10}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v11

    .local v11, "line":Ljava/lang/String;
    if-eqz v11, :cond_9

    .line 107
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_4

    .line 125
    .end local v4    # "diff":J
    .end local v7    # "httpConnection":Ljavax/net/ssl/HttpsURLConnection;
    .end local v8    # "finish":J
    .end local v10    # "input":Ljava/io/BufferedReader;
    .end local v11    # "line":Ljava/lang/String;
    .end local v12    # "result":Ljava/lang/StringBuilder;
    .end local v14    # "start":J
    :catchall_0
    move-exception v16

    if-eqz v3, :cond_8

    .line 129
    :try_start_a
    invoke-virtual {v3}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8

    .line 135
    :cond_8
    :goto_5
    throw v16

    .line 110
    .restart local v4    # "diff":J
    .restart local v7    # "httpConnection":Ljavax/net/ssl/HttpsURLConnection;
    .restart local v8    # "finish":J
    .restart local v10    # "input":Ljava/io/BufferedReader;
    .restart local v11    # "line":Ljava/lang/String;
    .restart local v12    # "result":Ljava/lang/StringBuilder;
    .restart local v14    # "start":J
    :cond_9
    :try_start_b
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V

    .line 111
    invoke-virtual {v7}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 112
    const/4 v7, 0x0

    .line 115
    const/4 v3, 0x0

    .line 116
    new-instance v16, Lorg/json/JSONObject;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 125
    if-eqz v3, :cond_3

    .line 129
    :try_start_c
    invoke-virtual {v3}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    goto/16 :goto_2

    .line 131
    :catch_5
    move-exception v6

    .line 134
    .restart local v6    # "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-interface/range {v17 .. v19}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 125
    .end local v4    # "diff":J
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v7    # "httpConnection":Ljavax/net/ssl/HttpsURLConnection;
    .end local v8    # "finish":J
    .end local v10    # "input":Ljava/io/BufferedReader;
    .end local v11    # "line":Ljava/lang/String;
    .end local v12    # "result":Ljava/lang/StringBuilder;
    .end local v14    # "start":J
    :cond_a
    if-eqz v3, :cond_5

    .line 129
    :try_start_d
    invoke-virtual {v3}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6

    goto/16 :goto_3

    .line 131
    :catch_6
    move-exception v6

    .line 134
    .restart local v6    # "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 131
    .local v6, "e":Ljava/io/IOException;
    :catch_7
    move-exception v6

    .line 134
    .local v6, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 131
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_8
    move-exception v6

    .line 134
    .restart local v6    # "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->logger:Lcom/microsoft/cll/android/ILogger;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/cll/android/AbstractSettings;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-interface/range {v17 .. v19}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method public setSettingsEndpoint(Ljava/lang/String;)V
    .locals 0
    .param p1, "endpoint"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/microsoft/cll/android/AbstractSettings;->endpoint:Ljava/lang/String;

    .line 165
    return-void
.end method
