.class Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$1;
.super Ljava/lang/Object;
.source "ClubAdminBannedScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPartialRosterChangeSuccess(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .local p2, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    const-string v0, "Should not be possible due to single xuid requests"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public onRosterChangeFailure(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p1, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    const v1, 0x7f070233

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;I)V

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)V

    .line 69
    return-void
.end method

.method public onRosterChangeSuccess(Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    return-void
.end method
