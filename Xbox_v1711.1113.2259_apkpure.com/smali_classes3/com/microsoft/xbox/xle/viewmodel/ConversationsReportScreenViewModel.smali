.class public Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ConversationsReportScreenViewModel.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 9
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 11
    return-void
.end method


# virtual methods
.method public confirm()V
    .locals 0

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;->goBack()V

    .line 29
    return-void
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 39
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 16
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 20
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method
