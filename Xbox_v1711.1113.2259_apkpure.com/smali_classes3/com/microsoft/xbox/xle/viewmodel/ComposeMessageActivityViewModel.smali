.class public Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "ComposeMessageActivityViewModel.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private autoFocus:Z

.field private busyText:Ljava/lang/String;

.field protected messageBody:Ljava/lang/String;

.field protected messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

.field protected recipients:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation
.end field

.field protected returnScreen:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation
.end field

.field private returnToConversationDetailsScreen:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 3
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 52
    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->TAG:Ljava/lang/String;

    .line 62
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 63
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07074e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->busyText:Ljava/lang/String;

    .line 64
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 65
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->autoFocus:Z

    .line 67
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 68
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getMessagesOriginatingScreen()Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->returnScreen:Ljava/lang/Class;

    .line 69
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getReturnToConversationDetailsScreen()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->returnToConversationDetailsScreen:Z

    .line 70
    return-void
.end method

.method private goBackToMessageScreen()Z
    .locals 3

    .prologue
    .line 257
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getPreviousActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 258
    .local v0, "originatingScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/FriendsSelectorScreen;

    if-ne v1, v2, :cond_0

    .line 259
    const/4 v1, 0x1

    .line 261
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancelSendClick()V
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->updateMessageBody()V

    .line 236
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->isDirty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 237
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->goBackToMessageScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ConversationsActivity;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->goToScreenWithPop(Ljava/lang/Class;)V

    .line 254
    :goto_0
    return-void

    .line 240
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->goBack()V

    goto :goto_0

    .line 243
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->goBackToMessageScreen()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->showDiscardChangeWithRunnable(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 251
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->showDiscardChangesGoBack()V

    goto :goto_0
.end method

.method public getBlockingStatusText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->busyText:Ljava/lang/String;

    return-object v0
.end method

.method public getIsRecipientNonEmpty()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->recipients:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMessageBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->messageBody:Ljava/lang/String;

    return-object v0
.end method

.method public getRecipients()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->recipients:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getShouldAutoShowKeyboard()Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->autoFocus:Z

    return v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return-object v0
.end method

.method public isBlockingBusy()Z
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->getIsSending()Z

    move-result v0

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method protected isDirty()Z
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->messageBody:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->getIsRecipientNonEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public leaveViewModel(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "closeHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->updateMessageBody()V

    .line 267
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->isDirty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 272
    :goto_0
    return-void

    .line 270
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->showDiscardChangeWithRunnable(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 200
    return-void
.end method

.method public navigateToFriendsPicker()V
    .locals 5

    .prologue
    .line 287
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->recipients:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 288
    new-instance v2, Lcom/microsoft/xbox/toolkit/MultiSelection;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/MultiSelection;-><init>()V

    .line 289
    .local v2, "recipients":Lcom/microsoft/xbox/toolkit/MultiSelection;, "Lcom/microsoft/xbox/toolkit/MultiSelection<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->recipients:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 290
    .local v0, "item":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/MultiSelection;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 292
    .end local v0    # "item":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedRecipients(Lcom/microsoft/xbox/toolkit/MultiSelection;)V

    .line 295
    .end local v2    # "recipients":Lcom/microsoft/xbox/toolkit/MultiSelection;, "Lcom/microsoft/xbox/toolkit/MultiSelection<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 296
    .local v1, "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putIsGroupConversation(Z)V

    .line 297
    const-class v3, Lcom/microsoft/xbox/xle/app/activity/FriendsSelectorScreen;

    invoke-virtual {p0, v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 298
    return-void
.end method

.method public onBackButtonPressed()V
    .locals 1

    .prologue
    .line 280
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->closeDrawer()V

    .line 282
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->cancelSendClick()V

    .line 284
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 205
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 81
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/MultiSelection;->toArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->recipients:Ljava/util/ArrayList;

    .line 82
    return-void
.end method

.method protected onStopOverride()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    .line 93
    .local v0, "composeAdapter":Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->updateMessageBody()V

    .line 94
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 95
    return-void
.end method

.method protected onUpdateFinished()V
    .locals 4

    .prologue
    .line 145
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageSend:Lcom/microsoft/xbox/service/model/UpdateType;

    const-wide/16 v2, 0xbec

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->checkErrorCode(Lcom/microsoft/xbox/service/model/UpdateType;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const v0, 0x7f07074c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->showError(I)V

    .line 149
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->onUpdateFinished()V

    .line 150
    return-void
.end method

.method public sendSkypeMessage()V
    .locals 7

    .prologue
    .line 208
    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->MessageSend:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->setUpdateTypesToCheck(Ljava/util/EnumSet;)V

    .line 210
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v4, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->updateMessageBody()V

    .line 213
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->recipients:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    .line 214
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 216
    .local v2, "recipientsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->recipients:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 217
    .local v1, "recipient":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    iget-object v5, v1, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->xuid:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 220
    .end local v1    # "recipient":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 221
    .local v0, "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    new-instance v3, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Text:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->name()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->messageBody:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-direct {v3, v5, v6, v4}, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    .local v3, "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_2

    .line 223
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v4, v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->sendSkypeGroupMessage(Ljava/util/List;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    .line 230
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v2    # "recipientsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    :goto_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->updateAdapter()V

    .line 231
    return-void

    .line 221
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    .restart local v2    # "recipientsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    const-string v4, ""

    goto :goto_1

    .line 225
    .restart local v3    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->sendSkypeMessage(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    goto :goto_2

    .line 228
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v2    # "recipientsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->TAG:Ljava/lang/String;

    const-string v5, "Tried to send message without any recipients"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public setMessageBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->messageBody:Ljava/lang/String;

    .line 183
    return-void
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel$2;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_0

    .line 135
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->updateAdapter()V

    .line 136
    :cond_1
    :goto_1
    return-void

    .line 101
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v6

    if-nez v6, :cond_4

    .line 103
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->returnScreen:Ljava/lang/Class;

    if-eqz v6, :cond_2

    .line 104
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->returnScreen:Ljava/lang/Class;

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->goToScreenWithPop(Ljava/lang/Class;)V

    goto :goto_1

    .line 105
    :cond_2
    iget-boolean v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->returnToConversationDetailsScreen:Z

    if-eqz v6, :cond_3

    .line 106
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/UpdateData;->getExtra()Landroid/os/Bundle;

    move-result-object v0

    .line 107
    .local v0, "b":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 108
    const-string v6, "CONVERSATION_ID_FOR_NEW_GROUP"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 109
    .local v2, "conversationId":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 110
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 111
    .local v3, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedConversationSummary(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 112
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSenderXuid(Ljava/lang/String;)V

    .line 113
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v6

    const-class v7, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_1

    .line 117
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v2    # "conversationId":Ljava/lang/String;
    .end local v3    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->goBack()V

    goto :goto_1

    .line 122
    :cond_4
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v4

    .line 123
    .local v4, "errorCode":J
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/XLEException;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    instance-of v6, v6, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_5

    .line 124
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/XLEException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/XLEException;

    .line 125
    .local v1, "cause":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v4

    .line 128
    .end local v1    # "cause":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_5
    const-wide/16 v6, 0x26e9

    cmp-long v6, v4, v6

    if-nez v6, :cond_0

    .line 129
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->showFixMessagingDialog()V

    goto :goto_0

    .line 99
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
