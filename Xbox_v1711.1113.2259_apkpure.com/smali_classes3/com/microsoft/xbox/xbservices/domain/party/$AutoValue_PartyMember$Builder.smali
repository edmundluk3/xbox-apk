.class final Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;
.super Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
.source "$AutoValue_PartyMember.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private isHost:Ljava/lang/Boolean;

.field private isSelfMuted:Ljava/lang/Boolean;

.field private isTalking:Ljava/lang/Boolean;

.field private multiplayerMember:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

.field private privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

.field private rosterKey:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;-><init>()V

    .line 135
    return-void
.end method

.method private constructor <init>(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;-><init>()V

    .line 137
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->rosterKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->rosterKey:Ljava/lang/String;

    .line 138
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isHost()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->isHost:Ljava/lang/Boolean;

    .line 139
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isTalking()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->isTalking:Ljava/lang/Boolean;

    .line 140
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->multiplayerMember()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->multiplayerMember:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    .line 141
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    .line 142
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isSelfMuted()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->isSelfMuted:Ljava/lang/Boolean;

    .line 143
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .param p2, "x1"    # Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$1;

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .locals 8

    .prologue
    .line 182
    const-string v7, ""

    .line 183
    .local v7, "missing":Ljava/lang/String;
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->rosterKey:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " rosterKey"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->isHost:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isHost"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->isTalking:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isTalking"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 192
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->multiplayerMember:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    if-nez v0, :cond_3

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " multiplayerMember"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 195
    :cond_3
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 196
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing required properties:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_4
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMember;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->rosterKey:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->isHost:Ljava/lang/Boolean;

    .line 200
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->isTalking:Ljava/lang/Boolean;

    .line 201
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->multiplayerMember:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->isSelfMuted:Ljava/lang/Boolean;

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMember;-><init>(Ljava/lang/String;ZZLcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;Ljava/lang/Boolean;)V

    .line 198
    return-object v0
.end method

.method public isHost(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
    .locals 1
    .param p1, "isHost"    # Z

    .prologue
    .line 154
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->isHost:Ljava/lang/Boolean;

    .line 155
    return-object p0
.end method

.method public isSelfMuted(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
    .locals 0
    .param p1, "isSelfMuted"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 177
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->isSelfMuted:Ljava/lang/Boolean;

    .line 178
    return-object p0
.end method

.method public isTalking(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
    .locals 1
    .param p1, "isTalking"    # Z

    .prologue
    .line 159
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->isTalking:Ljava/lang/Boolean;

    .line 160
    return-object p0
.end method

.method public multiplayerMember(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
    .locals 2
    .param p1, "multiplayerMember"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    .prologue
    .line 164
    if-nez p1, :cond_0

    .line 165
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null multiplayerMember"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->multiplayerMember:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    .line 168
    return-object p0
.end method

.method public privacyPermissions(Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
    .locals 0
    .param p1, "privacyPermissions"    # Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 172
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    .line 173
    return-object p0
.end method

.method public rosterKey(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
    .locals 2
    .param p1, "rosterKey"    # Ljava/lang/String;

    .prologue
    .line 146
    if-nez p1, :cond_0

    .line 147
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null rosterKey"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;->rosterKey:Ljava/lang/String;

    .line 150
    return-object p0
.end method
