.class Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;
.super Ljava/lang/Object;
.source "ServiceResponse.java"


# instance fields
.field public final headers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final response:Ljava/lang/String;

.field public final statusCode:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/util/Map;)V
    .locals 0
    .param p1, "statusCode"    # I
    .param p2, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p3, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;->statusCode:I

    .line 17
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;->response:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;->headers:Ljava/util/Map;

    .line 19
    return-void
.end method


# virtual methods
.method public isSuccess()Z
    .locals 2

    .prologue
    .line 22
    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;->statusCode:I

    const/16 v1, 0x12c

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
