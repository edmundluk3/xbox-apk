.class public final enum Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;
.super Ljava/lang/Enum;
.source "CanvasWebViewActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CanvasViewState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

.field public static final enum Splash:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

.field public static final enum Webview:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    const-string v1, "Splash"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->Splash:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    .line 60
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    const-string v1, "Webview"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->Webview:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    .line 57
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->Splash:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->Webview:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 57
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    return-object v0
.end method
