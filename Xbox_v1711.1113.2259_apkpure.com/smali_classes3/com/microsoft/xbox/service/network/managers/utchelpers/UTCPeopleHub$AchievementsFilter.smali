.class public final enum Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;
.super Ljava/lang/Enum;
.source "UTCPeopleHub.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AchievementsFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

.field public static final enum MostRecent:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

.field public static final enum Other:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

.field public static final enum XboxOne:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;->Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    .line 55
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    const-string v1, "MostRecent"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;->MostRecent:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    .line 56
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    const-string v1, "XboxOne"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;->XboxOne:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    .line 57
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    const-string v1, "Other"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;->Other:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    .line 53
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;->Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;->MostRecent:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;->XboxOne:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;->Other:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 53
    const-class v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;

    return-object v0
.end method
