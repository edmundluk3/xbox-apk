.class public Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PhoneContactInviteFriendsDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PhoneContactSelectorListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;Landroid/app/Activity;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "rowViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 246
    .local p4, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    .line 247
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 248
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;->notifyDataSetChanged()V

    .line 249
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 253
    move-object v4, p2

    .line 254
    .local v4, "v":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;

    if-eqz v6, :cond_2

    .line 255
    if-eqz v4, :cond_0

    instance-of v6, v4, Landroid/widget/LinearLayout;

    if-nez v6, :cond_1

    .line 256
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v9, "layout_inflater"

    invoke-virtual {v6, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 257
    .local v5, "vi":Landroid/view/LayoutInflater;
    const v6, 0x7f0301cf

    invoke-virtual {v5, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 260
    .end local v5    # "vi":Landroid/view/LayoutInflater;
    :cond_1
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;

    .line 261
    .local v3, "person":Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;
    if-eqz v3, :cond_2

    .line 262
    invoke-virtual {v4, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 263
    const v6, 0x7f0e095f

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 264
    .local v1, "name":Landroid/widget/TextView;
    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;->displayName:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    const v6, 0x7f0e0960

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 266
    .local v2, "onXbox":Landroid/widget/TextView;
    iget-boolean v6, v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;->isOnXbox:Z

    if-eqz v6, :cond_3

    move v6, v7

    :goto_0
    invoke-static {v2, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 267
    const v6, 0x7f0e05d7

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 268
    .local v0, "iconSelect":Landroid/widget/TextView;
    iget-boolean v6, v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;->isSelected:Z

    if-eqz v6, :cond_4

    :goto_1
    invoke-static {v0, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 269
    iget-boolean v6, v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;->isSelected:Z

    if-eqz v6, :cond_5

    .line 270
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 276
    .end local v0    # "iconSelect":Landroid/widget/TextView;
    .end local v1    # "name":Landroid/widget/TextView;
    .end local v2    # "onXbox":Landroid/widget/TextView;
    .end local v3    # "person":Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;
    :cond_2
    :goto_2
    return-object v4

    .restart local v1    # "name":Landroid/widget/TextView;
    .restart local v2    # "onXbox":Landroid/widget/TextView;
    .restart local v3    # "person":Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;
    :cond_3
    move v6, v8

    .line 266
    goto :goto_0

    .restart local v0    # "iconSelect":Landroid/widget/TextView;
    :cond_4
    move v7, v8

    .line 268
    goto :goto_1

    .line 272
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0145

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_2
.end method
