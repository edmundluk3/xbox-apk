.class public final enum Lcom/microsoft/xbox/service/model/entity/Entity$Type;
.super Ljava/lang/Enum;
.source "Entity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/entity/Entity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/entity/Entity$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/entity/Entity$Type;

.field public static final enum ACHIEVEMENT:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

.field public static final enum ACTIVITY_FEED_ITEM:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

.field public static final enum GAME_DVR:Lcom/microsoft/xbox/service/model/entity/Entity$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 74
    new-instance v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    const-string v1, "ACTIVITY_FEED_ITEM"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/entity/Entity$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACTIVITY_FEED_ITEM:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    .line 78
    new-instance v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    const-string v1, "GAME_DVR"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/entity/Entity$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->GAME_DVR:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    .line 82
    new-instance v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    const-string v1, "ACHIEVEMENT"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/entity/Entity$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACHIEVEMENT:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    .line 64
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    sget-object v1, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACTIVITY_FEED_ITEM:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->GAME_DVR:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACHIEVEMENT:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->$VALUES:[Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/entity/Entity$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    const-class v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/entity/Entity$Type;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->$VALUES:[Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/entity/Entity$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    return-object v0
.end method
