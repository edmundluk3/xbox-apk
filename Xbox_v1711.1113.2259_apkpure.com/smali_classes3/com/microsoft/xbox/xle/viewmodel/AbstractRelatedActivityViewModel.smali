.class public abstract Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "AbstractRelatedActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;",
        ">",
        "Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;"
    }
.end annotation


# instance fields
.field private isLoadingRelated:Z

.field private loadRelatedTask:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel",
            "<TT;>.",
            "LoadRelatedAsyncTask;"
        }
    .end annotation
.end field

.field protected mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 27
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>()V

    .line 19
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 29
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 30
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 31
    .local v1, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 32
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 33
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 34
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$1;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->loadRelatedTask:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;

    .line 35
    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 16
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->isLoadingRelated:Z

    return p1
.end method

.method private static getMediaItemRelatedAspectXY(I)[I
    .locals 1
    .param p0, "mediaType"    # I

    .prologue
    .line 101
    packed-switch p0, :pswitch_data_0

    .line 106
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultAspectXY(I)[I

    move-result-object v0

    :goto_0
    return-object v0

    .line 104
    :pswitch_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    goto :goto_0

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x2329
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 104
    :array_0
    .array-data 4
        0x1a8
        0x11b
    .end array-data
.end method


# virtual methods
.method public NavigateToRelatedItemDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 111
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 112
    return-void
.end method

.method public getAspectRatio()F
    .locals 3

    .prologue
    .line 96
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->getMediaItemRelatedAspectXY(I)[I

    move-result-object v0

    .line 97
    .local v0, "ar":[I
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    aget v2, v0, v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/4 v2, 0x1

    aget v2, v0, v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    return v1
.end method

.method public abstract getRelated()Ljava/lang/Object;
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 39
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public hasRelated()Z
    .locals 1

    .prologue
    .line 115
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getRelated()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getRelated()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 44
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->isLoadingRelated:Z

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 50
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->loadRelatedTask:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->load(Z)V

    .line 51
    return-void
.end method

.method public onLoadRelatedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 71
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    const-string v0, "RelatedViewModel"

    const-string v1, "onLoadRelated Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->isLoadingRelated:Z

    .line 74
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 88
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->updateAdapter()V

    .line 89
    return-void

    .line 78
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->hasRelated()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 82
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 83
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onLoadingStarted()V
    .locals 2

    .prologue
    .line 65
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 66
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 68
    :cond_0
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 56
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 61
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->loadRelatedTask:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->cancel()V

    .line 62
    return-void
.end method
