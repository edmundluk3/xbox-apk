.class final Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;
.super Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;
.source "AutoValue_RecyclerViewChildDetachEvent.java"


# instance fields
.field private final child:Landroid/view/View;

.field private final view:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "child"    # Landroid/view/View;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;-><init>()V

    .line 16
    if-nez p1, :cond_0

    .line 17
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null view"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_0
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;->view:Landroid/support/v7/widget/RecyclerView;

    .line 20
    if-nez p2, :cond_1

    .line 21
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null child"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_1
    iput-object p2, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;->child:Landroid/view/View;

    .line 24
    return-void
.end method


# virtual methods
.method public child()Landroid/view/View;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;->child:Landroid/view/View;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    if-ne p1, p0, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    instance-of v3, p1, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 52
    check-cast v0, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;

    .line 53
    .local v0, "that":Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;
    iget-object v3, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;->view:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;->view()Landroid/support/v7/widget/RecyclerView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;->child:Landroid/view/View;

    .line 54
    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;->child()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;
    :cond_3
    move v1, v2

    .line 56
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 61
    const/4 v0, 0x1

    .line 62
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 63
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;->view:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 64
    mul-int/2addr v0, v2

    .line 65
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;->child:Landroid/view/View;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 66
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RecyclerViewChildDetachEvent{view="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;->view:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", child="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;->child:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public view()Landroid/support/v7/widget/RecyclerView;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;->view:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method
