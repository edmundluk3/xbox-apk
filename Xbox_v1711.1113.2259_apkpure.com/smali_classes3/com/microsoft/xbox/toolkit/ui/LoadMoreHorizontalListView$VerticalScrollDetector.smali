.class Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$VerticalScrollDetector;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "LoadMoreHorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VerticalScrollDetector"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$VerticalScrollDetector;->this$0:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;
    .param p2, "x1"    # Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$1;

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$VerticalScrollDetector;-><init>(Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;)V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 126
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
