.class public Lcom/microsoft/xbox/xle/ui/IconFontButton;
.super Landroid/widget/LinearLayout;
.source "IconFontButton.java"


# instance fields
.field private iconTextView:Landroid/widget/TextView;

.field private labelTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method private initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x1

    const/4 v9, 0x0

    .line 37
    const-string v7, "layout_inflater"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 38
    .local v6, "vi":Landroid/view/LayoutInflater;
    const v7, 0x7f03014e

    invoke-virtual {v6, v7, p0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 39
    const v7, 0x7f0e070e

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->iconTextView:Landroid/widget/TextView;

    .line 40
    const v7, 0x7f0e070f

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->isInEditMode()Z

    move-result v7

    if-nez v7, :cond_2

    .line 43
    sget-object v7, Lcom/microsoft/xboxone/smartglass/R$styleable;->CustomTypeface:[I

    invoke-virtual {p1, p2, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 44
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 45
    .local v5, "typeface":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 47
    if-eqz v5, :cond_0

    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    if-eqz v7, :cond_0

    .line 48
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v7

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8, v5}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v4

    .line 49
    .local v4, "tf":Landroid/graphics/Typeface;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 52
    .end local v4    # "tf":Landroid/graphics/Typeface;
    :cond_0
    sget-object v7, Lcom/microsoft/xboxone/smartglass/R$styleable;->IconFontButton:[I

    invoke-virtual {p1, p2, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v11}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "icon":Ljava/lang/String;
    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "label":Ljava/lang/String;
    const/4 v7, 0x2

    invoke-virtual {v0, v7, v10}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 56
    .local v3, "size":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 58
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 59
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 61
    if-eq v3, v10, :cond_1

    .line 62
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->iconTextView:Landroid/widget/TextView;

    int-to-float v8, v3

    invoke-virtual {v7, v9, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 63
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    int-to-float v8, v3

    invoke-virtual {v7, v9, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 69
    .end local v0    # "a":Landroid/content/res/TypedArray;
    .end local v1    # "icon":Ljava/lang/String;
    .end local v2    # "label":Ljava/lang/String;
    .end local v3    # "size":I
    .end local v5    # "typeface":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 66
    :cond_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->iconTextView:Landroid/widget/TextView;

    const-string v8, "X"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    const-string v8, "Text"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public removeLabelLeftMargin()V
    .locals 5

    .prologue
    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 100
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v1, 0x0

    iget v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iget v4, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    .line 103
    return-void
.end method

.method public setIconFontColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->iconTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 84
    return-void
.end method

.method public setIconFontSize(I)V
    .locals 3
    .param p1, "size"    # I

    .prologue
    const/4 v2, 0x2

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->iconTextView:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 89
    return-void
.end method

.method public setIconText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "iconText"    # Ljava/lang/CharSequence;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->iconTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->iconTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->iconTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 79
    return-void

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLabelMargin(IIII)V
    .locals 2
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    const/4 v1, -0x2

    .line 92
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 93
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 94
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 95
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    .line 96
    return-void
.end method

.method public setLabelText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontButton;->labelTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 74
    return-void

    .line 73
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
