.class public abstract Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
.super Ljava/lang/Object;
.source "GameProfileAchievementsCompositeItem.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static achievementHeader()Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 32
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;

    const/4 v2, 0x1

    move v3, v1

    move v4, v1

    move v5, v1

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;-><init>(ZZZZZLcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    return-object v0
.end method

.method public static achievementItem(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
    .locals 8
    .param p0, "achievementItem"    # Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 41
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;

    const/4 v4, 0x1

    const/4 v6, 0x0

    move v2, v1

    move v3, v1

    move v5, v1

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;-><init>(ZZZZZLcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    return-object v0
.end method

.method public static emptyAchievementItems()Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 45
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;

    const/4 v5, 0x1

    move v2, v1

    move v3, v1

    move v4, v1

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;-><init>(ZZZZZLcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    return-object v0
.end method

.method public static leaderboardHeader()Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 28
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;

    const/4 v1, 0x1

    move v3, v2

    move v4, v2

    move v5, v2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;-><init>(ZZZZZLcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    return-object v0
.end method

.method public static leaderboardItem(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;)Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
    .locals 8
    .param p0, "leaderboardListItem"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 36
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;

    const/4 v3, 0x1

    const/4 v7, 0x0

    move v2, v1

    move v4, v1

    move v5, v1

    move-object v6, p0

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;-><init>(ZZZZZLcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    return-object v0
.end method


# virtual methods
.method public abstract achievementItem()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract isAchievementHeader()Z
.end method

.method public abstract isAchievementItem()Z
.end method

.method public abstract isEmptyAchievementItems()Z
.end method

.method public abstract isLeaderboardHeader()Z
.end method

.method public abstract isLeaderboardItem()Z
.end method

.method public abstract leaderboardItem()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
