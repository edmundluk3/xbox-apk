.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$4;
.super Ljava/lang/Object;
.source "UTCLfg.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackInterested(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

.field final synthetic val$isHost:Z


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Z)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$4;->val$handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    iput-boolean p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$4;->val$isHost:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 4

    .prologue
    .line 95
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$4;->val$handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 97
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$4;->val$isHost:Z

    if-eqz v3, :cond_1

    const-string v1, "LFG - Interested List"

    .line 99
    .local v1, "pageAction":Ljava/lang/String;
    :goto_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 101
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$4;->val$handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v2

    .line 102
    .local v2, "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    if-eqz v2, :cond_0

    .line 103
    const-string v3, "SessionRef"

    invoke-virtual {v0, v3, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 106
    :cond_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 107
    return-void

    .line 97
    .end local v0    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .end local v1    # "pageAction":Ljava/lang/String;
    .end local v2    # "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    :cond_1
    const-string v1, "LFG - Interested"

    goto :goto_0
.end method
