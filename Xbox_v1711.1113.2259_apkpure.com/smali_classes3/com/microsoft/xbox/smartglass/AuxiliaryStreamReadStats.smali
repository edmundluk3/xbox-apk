.class public Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;
.super Ljava/lang/Object;
.source "AuxiliaryStreamReadStats.java"


# instance fields
.field public final availableBytes:J

.field public final receivedBytes:J


# direct methods
.method constructor <init>(JJ)V
    .locals 1
    .param p1, "receivedBytes"    # J
    .param p3, "availableBytes"    # J

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-wide p1, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;->receivedBytes:J

    .line 18
    iput-wide p3, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;->availableBytes:J

    .line 19
    return-void
.end method
