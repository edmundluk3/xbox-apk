.class abstract Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBERequestBody;
.super Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;
.source "$AutoValue_OOBESessionDataType_OOBERequestBody.java"


# instance fields
.field private final consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;)V
    .locals 0
    .param p1, "consoleSettings"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBERequestBody;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    .line 16
    return-void
.end method


# virtual methods
.method public consoleSettings()Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleSettings"
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBERequestBody;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 34
    if-ne p1, p0, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v1

    .line 37
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 38
    check-cast v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;

    .line 39
    .local v0, "that":Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBERequestBody;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;->consoleSettings()Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    move-result-object v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBERequestBody;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;->consoleSettings()Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;
    :cond_3
    move v1, v2

    .line 41
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 46
    const/4 v0, 0x1

    .line 47
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBERequestBody;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 49
    return v0

    .line 48
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBERequestBody;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OOBERequestBody{consoleSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBERequestBody;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
