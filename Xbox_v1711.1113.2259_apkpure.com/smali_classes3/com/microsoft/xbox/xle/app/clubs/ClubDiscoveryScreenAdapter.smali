.class public Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ClubDiscoveryScreenAdapter.java"


# static fields
.field private static final MAX_CATEGORIES_FOR_TOUCH_EXPLORATION:I = 0x3


# instance fields
.field private final invitationHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final invitationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

.field private final invitationSection:Landroid/widget/RelativeLayout;

.field private invitationsHashCode:I

.field private final invitationsViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

.field private final recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

.field private recommendationsHashCode:I

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)V
    .locals 7
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 43
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 45
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 47
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    .line 48
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getInvitationsViewModel()Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationsViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    .line 50
    const v3, 0x7f0e0a95

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 51
    const v3, 0x7f0e0351

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    const v3, 0x7f0e0350

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    const v3, 0x7f0e0352

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    const v3, 0x7f0e0356

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 64
    .local v0, "invitationList":Landroid/support/v7/widget/RecyclerView;
    const v3, 0x7f0e0354

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 65
    const v3, 0x7f0e0353

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationSection:Landroid/widget/RelativeLayout;

    .line 66
    const v3, 0x7f0e0355

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 67
    .local v1, "invitationsSeeAllButton":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$$Lambda$4;->lambdaFactory$()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    new-instance v3, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-direct {v3, v4, v6, v6}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 71
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    .line 72
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    new-instance v5, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;)V

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    .line 91
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 93
    const v3, 0x7f0e0357

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    .line 94
    .local v2, "recommendationList":Landroid/support/v7/widget/RecyclerView;
    invoke-virtual {v2, v6}, Landroid/support/v7/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 96
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    .line 100
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationsViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->launchSearch()V

    .line 53
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->launchYourClubs()V

    .line 57
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->launchCreate()V

    .line 61
    return-void
.end method

.method static synthetic lambda$new$3(Landroid/view/View;)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 67
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreen;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;
    .param p1, "clubCardCategoryItem"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->criteria:Ljava/lang/String;

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->headerText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->launchRecommendations(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 105
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->isBusy()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationsViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isBusy()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    move v3, v5

    :goto_0
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->updateLoadingIndicator(Z)V

    .line 106
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 108
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    sget-object v6, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v3, v6, :cond_2

    .line 109
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationsViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->getInviteList()Ljava/util/List;

    move-result-object v0

    .line 110
    .local v0, "inviteList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;>;"
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v1

    .line 111
    .local v1, "newHash":I
    iget v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationsHashCode:I

    if-eq v1, v3, :cond_1

    .line 112
    iput v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationsHashCode:I

    .line 114
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->clear()V

    .line 116
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 117
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationSection:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 118
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->addAll(Ljava/util/Collection;)V

    .line 119
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070200

    new-array v5, v5, [Ljava/lang/Object;

    .line 121
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v4

    invoke-virtual {v6, v7, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 119
    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 128
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->notifyDataSetChanged()V

    .line 130
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getRecommendationCategoryList()Ljava/util/List;

    move-result-object v2

    .line 131
    .local v2, "recommendationList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;>;"
    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v1

    .line 132
    iget v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->recommendationsHashCode:I

    if-eq v1, v3, :cond_2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 133
    iput v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->recommendationsHashCode:I

    .line 135
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->clear()V

    .line 139
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 140
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x3

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-interface {v2, v4, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->addAll(Ljava/util/Collection;)V

    .line 146
    .end local v0    # "inviteList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;>;"
    .end local v1    # "newHash":I
    .end local v2    # "recommendationList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;>;"
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v3, v4

    .line 105
    goto/16 :goto_0

    .line 124
    .restart local v0    # "inviteList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;>;"
    .restart local v1    # "newHash":I
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->invitationSection:Landroid/widget/RelativeLayout;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    .line 142
    .restart local v2    # "recommendationList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;>;"
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->addAll(Ljava/util/Collection;)V

    goto :goto_2
.end method
