.class public Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "RecipientCountDrawable.java"


# instance fields
.field private maxRecipientsDisplayedOnScreen:I

.field private recipientCount:I

.field private recipientCountText:Ljava/lang/String;

.field private requiresAdjustmentTowardsCenter:Z

.field private final textBounds:Landroid/graphics/Rect;

.field private final textPaint:Landroid/graphics/Paint;

.field private final typeface:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 23
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->recipientCount:I

    .line 24
    const-string v0, "0"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->recipientCountText:Ljava/lang/String;

    .line 26
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->maxRecipientsDisplayedOnScreen:I

    .line 31
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->requiresAdjustmentTowardsCenter:Z

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v2, "fonts/SegoeWP.ttf"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->typeface:Landroid/graphics/Typeface;

    .line 36
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->textBounds:Landroid/graphics/Rect;

    .line 37
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->textPaint:Landroid/graphics/Paint;

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->textPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->typeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->textPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->textPaint:Landroid/graphics/Paint;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0903ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->textPaint:Landroid/graphics/Paint;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0c00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 43
    return-void
.end method

.method private drawLabel(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 52
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->recipientCountText:Ljava/lang/String;

    .line 54
    .local v2, "label":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 55
    .local v1, "drawableBounds":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->textPaint:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {v5, v2, v6, v7, v8}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 57
    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->requiresAdjustmentTowardsCenter:Z

    if-eqz v5, :cond_0

    const/high16 v5, 0x40800000    # 4.0f

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->DIPtoPixels(F)I

    move-result v5

    int-to-float v0, v5

    .line 59
    .local v0, "adjustmentLeftAmount":F
    :goto_0
    invoke-virtual {v1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v6

    sub-float/2addr v5, v6

    sub-float v3, v5, v0

    .line 60
    .local v3, "textPositionX":F
    invoke-virtual {v1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v6

    sub-float v4, v5, v6

    .line 62
    .local v4, "textPositionY":F
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 63
    return-void

    .line 57
    .end local v0    # "adjustmentLeftAmount":F
    .end local v3    # "textPositionX":F
    .end local v4    # "textPositionY":F
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateRecipientsCountText()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 97
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->recipientCount:I

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->maxRecipientsDisplayedOnScreen:I

    sub-int/2addr v1, v2

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 99
    .local v0, "additionalRecipients":I
    if-nez v0, :cond_0

    .line 100
    const-string v1, "+"

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->recipientCountText:Ljava/lang/String;

    .line 101
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->requiresAdjustmentTowardsCenter:Z

    .line 110
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->invalidateSelf()V

    .line 111
    return-void

    .line 102
    :cond_0
    const/16 v1, 0x63

    if-le v0, v1, :cond_1

    .line 103
    const-string v1, "+99"

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->recipientCountText:Ljava/lang/String;

    .line 104
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->requiresAdjustmentTowardsCenter:Z

    goto :goto_0

    .line 106
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->recipientCountText:Ljava/lang/String;

    .line 107
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->requiresAdjustmentTowardsCenter:Z

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 47
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c00e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 48
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->drawLabel(Landroid/graphics/Canvas;)V

    .line 49
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, -0x1

    return v0
.end method

.method public getRecipientCount()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->recipientCount:I

    return v0
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 72
    const-string v0, "Alpha not supported here yet.  Feel free to implement it if you need it ;-)"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 77
    const-string v0, "ColorFilters not supported here yet.  Feel free to implement it if you need it ;-)"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public setMaxRecipientsDisplayedOnScreenCount(I)V
    .locals 0
    .param p1, "maxRecipientsDisplayedOnScreen"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->maxRecipientsDisplayedOnScreen:I

    .line 93
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->updateRecipientsCountText()V

    .line 94
    return-void
.end method

.method public setRecipientCount(I)V
    .locals 0
    .param p1, "recipientCount"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->recipientCount:I

    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->updateRecipientsCountText()V

    .line 88
    return-void
.end method
