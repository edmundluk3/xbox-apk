.class final enum Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;
.super Ljava/lang/Enum;
.source "VerticalPanScrollContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TouchTarget"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

.field public static final enum BODY:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

.field public static final enum HEADER:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 379
    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->HEADER:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    const-string v1, "BODY"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->BODY:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    .line 378
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->HEADER:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->BODY:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->$VALUES:[Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 378
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 378
    const-class v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;
    .locals 1

    .prologue
    .line 378
    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->$VALUES:[Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    return-object v0
.end method
