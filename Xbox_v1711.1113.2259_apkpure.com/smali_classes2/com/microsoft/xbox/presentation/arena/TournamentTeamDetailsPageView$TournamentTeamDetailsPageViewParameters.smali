.class public Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;
.super Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
.source "TournamentTeamDetailsPageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TournamentTeamDetailsPageViewParameters"
.end annotation


# instance fields
.field public final id:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final isPendingSession:Z

.field public final organizer:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final teamId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "organizer"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "teamId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "isPendingSession"    # Z

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 75
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 76
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 78
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;->organizer:Ljava/lang/String;

    .line 79
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;->id:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;->teamId:Ljava/lang/String;

    .line 81
    iput-boolean p4, p0, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;->isPendingSession:Z

    .line 82
    return-void
.end method
