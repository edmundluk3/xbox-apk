.class Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder$1;
.super Ljava/lang/Object;
.source "GameProfileAchievementsListAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;

.field final synthetic val$this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;

    .prologue
    .line 283
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder$1;->val$this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 288
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    .line 289
    .local v0, "filter":Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getAchievementsFilter()Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 290
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->setAchievementsFilter(Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;)V

    .line 292
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 296
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
