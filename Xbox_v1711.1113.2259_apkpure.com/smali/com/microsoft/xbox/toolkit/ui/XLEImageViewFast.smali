.class public Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
.super Lcom/microsoft/xbox/toolkit/ui/XLEImageView;
.source "XLEImageViewFast.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private resourceIdForError:I

.field private resourceIdForLoading:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;-><init>(Landroid/content/Context;)V

    .line 22
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setSoundEffectsEnabled(Z)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->isInEditMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    :goto_0
    return-void

    .line 32
    :cond_0
    sget-object v2, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLEImageViewFast:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 33
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 35
    .local v1, "resourceId":I
    if-eq v1, v3, :cond_1

    .line 36
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageResource(I)V

    .line 39
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 41
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setSoundEffectsEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public setImageURI(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 58
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setImageURI2(Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 45
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->resourceIdForLoading:I

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->resourceIdForError:I

    invoke-static {p0, p1, v0, v1}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 46
    return-void
.end method

.method public setImageURI2(Ljava/lang/String;II)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "loadingResourceId"    # I
    .param p3, "errorResourceId"    # I

    .prologue
    .line 49
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->resourceIdForLoading:I

    .line 50
    iput p3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->resourceIdForError:I

    .line 52
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->resourceIdForLoading:I

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->resourceIdForError:I

    invoke-static {p0, p1, v0, v1}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 53
    return-void
.end method
