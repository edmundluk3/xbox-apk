.class public abstract Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
.super Ljava/lang/Object;
.source "TutorialViewState.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/logging/Loggable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static loadingInstance()Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;-><init>(ZLcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;Ljava/lang/Throwable;Z)V

    return-object v0
.end method

.method public static withContent(ZLcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
    .locals 3
    .param p0, "isLinkedToFacebook"    # Z
    .param p1, "welcomeCardCompletionStates"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .prologue
    .line 25
    new-instance v0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;-><init>(ZLcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;Ljava/lang/Throwable;Z)V

    return-object v0
.end method

.method public static withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
    .locals 3
    .param p0, "err"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 30
    new-instance v0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1, p0, v2}, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;-><init>(ZLcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;Ljava/lang/Throwable;Z)V

    return-object v0
.end method


# virtual methods
.method public abstract error()Ljava/lang/Throwable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public isContent()Z
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->welcomeCardCompletionStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->error()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->error()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract isLinkedToFacebook()Z
.end method

.method public abstract isLoading()Z
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract welcomeCardCompletionStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
