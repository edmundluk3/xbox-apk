.class public abstract Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;
.super Ljava/lang/Object;
.source "RxWebSocketDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "RxWebSocketByteStringEvent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lokio/ByteString;)Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;
    .locals 1
    .param p0, "bytes"    # Lokio/ByteString;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 32
    new-instance v0, Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketByteStringEvent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketByteStringEvent;-><init>(Lokio/ByteString;)V

    return-object v0
.end method


# virtual methods
.method public abstract bytes()Lokio/ByteString;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
