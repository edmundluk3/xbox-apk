.class public Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "SettingsPivotScreenViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 18
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 20
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getSettingsPivotScreenAdapter(Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 21
    return-void
.end method

.method private isPlayServiceInvalid()Z
    .locals 2

    .prologue
    .line 50
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v0

    .line 51
    .local v0, "model":Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getServiceCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isServiceInvalid(I)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public isBusy()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 47
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getSettingsPivotScreenAdapter(Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 34
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;->isPlayServiceInvalid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Play service not detected, removing Notification pivot"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const-class v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPage;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 29
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method
