.class public interface abstract Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer;
.super Ljava/lang/Object;
.source "SkypeConversationsSummaryContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;,
        Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;,
        Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;,
        Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;,
        Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;,
        Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;,
        Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;,
        Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    }
.end annotation
