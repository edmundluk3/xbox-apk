.class final synthetic Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$$Lambda$1;->arg$1:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;)Lio/reactivex/functions/Consumer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$$Lambda$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$$Lambda$1;-><init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;)V

    return-object v0
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$$Lambda$1;->arg$1:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    check-cast p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveStoredSkypeConversationMessageCacheInternal(Ljava/util/concurrent/ConcurrentHashMap;)Z

    return-void
.end method
