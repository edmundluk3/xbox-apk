.class public Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "GamerscoreLeaderboardScreenAdapter.java"


# instance fields
.field private final leaderGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private final leaderGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final leaderGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;)V
    .locals 7
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>()V

    .line 39
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 41
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;

    .line 42
    const v4, 0x7f0e0a95

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 44
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 46
    .local v0, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    new-instance v4, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;

    invoke-direct {v4, v5, v6}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    .line 47
    const v4, 0x7f0e06af

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    .line 48
    .local v1, "leaderboardList":Landroid/support/v7/widget/RecyclerView;
    new-instance v4, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;

    invoke-direct {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;-><init>(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;Landroid/support/v7/widget/LinearLayoutManager;)V

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView;->addOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 53
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 54
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 56
    const v4, 0x7f0e06ab

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    .line 57
    .local v2, "leaderboardSpinner":Landroid/widget/Spinner;
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f030202

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;

    .line 59
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->getFilterItems()Ljava/util/List;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 60
    .local v3, "leaderboardSpinnerAdapter":Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter<Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;>;"
    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 61
    const v4, 0x7f03020a

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 62
    new-instance v4, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v2, v4}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 64
    const v4, 0x7f0e06ac

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->leaderGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 65
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->leaderGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setLongClickable(Z)V

    .line 66
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->leaderGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;)Landroid/view/View$OnLongClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 91
    const v4, 0x7f0e06ad

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->leaderGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 92
    const v4, 0x7f0e06ae

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->leaderGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 93
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J

    .prologue
    .line 62
    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->setFilterIndex(I)V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;Landroid/view/View;)Z
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 67
    new-instance v2, Landroid/widget/PopupMenu;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 68
    .local v2, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v3

    const v4, 0x7f0f0007

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 69
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->getLeaderboardList()Ljava/util/List;

    move-result-object v1

    .line 70
    .local v1, "leaderboard":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;"
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getXuid()Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "friendXuid":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 72
    invoke-static {p1, v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;Ljava/lang/String;Ljava/util/List;)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 86
    invoke-virtual {v2}, Landroid/widget/PopupMenu;->show()V

    .line 89
    :cond_0
    const/4 v3, 0x1

    return v3
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;Ljava/lang/String;Ljava/util/List;Landroid/view/MenuItem;)Z
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "friendXuid"    # Ljava/lang/String;
    .param p2, "leaderboard"    # Ljava/util/List;
    .param p3, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 73
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 84
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 75
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->shareLeaderboardCompare(Ljava/lang/String;)V

    goto :goto_0

    .line 78
    :pswitch_1
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    goto :goto_0

    .line 81
    :pswitch_2
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getUser()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->navigateToSendMessage(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x7f0e0c2b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;

    return-object v0
.end method

.method protected updateViewOverride()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 107
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->isBusy()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->updateLoadingIndicator(Z)V

    .line 109
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    .line 110
    .local v2, "viewModelState":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 112
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->isBusy()Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_1

    .line 113
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;->getLeaderboardList()Ljava/util/List;

    move-result-object v1

    .line 115
    .local v1, "leaderboardList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 116
    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    .line 117
    .local v0, "leader":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->leaderGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getGamerpic()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 118
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->leaderGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070600

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getGamertag()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->leaderGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0705ff

    new-array v6, v8, [Ljava/lang/Object;

    iget-wide v8, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->monthlyGamerscore:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    .end local v0    # "leader":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->clear()V

    .line 123
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->addAll(Ljava/util/Collection;)V

    .line 124
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->notifyDataSetChanged()V

    .line 126
    .end local v1    # "leaderboardList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;"
    :cond_1
    return-void
.end method
