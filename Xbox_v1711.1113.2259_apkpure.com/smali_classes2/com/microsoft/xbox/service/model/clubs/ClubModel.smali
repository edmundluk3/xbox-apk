.class public Lcom/microsoft/xbox/service/model/clubs/ClubModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "ClubModel.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$ChangeNameTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubChangeAsyncTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field public static final MAX_ASSOCIATED_TITLES:I = 0x19

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

.field private final id:J

.field private final lastRefreshMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private localBackgroundImage:Ljava/lang/String;

.field private localDisplayImage:Ljava/lang/String;

.field private final requestFilterLoadingStatusMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;",
            ">;"
        }
    .end annotation
.end field

.field private rxData:Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation
.end field

.field private final socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

.field private final socialTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const-class v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(J)V
    .locals 7
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 129
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 131
    sget-object v1, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    .line 132
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->socialTags:Ljava/util/List;

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 137
    iput-wide p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->id:J

    .line 139
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->lastRefreshMap:Ljava/util/Map;

    .line 140
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->values()[Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 141
    .local v0, "requestFilter":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->lastRefreshMap:Ljava/util/Map;

    const/4 v5, 0x0

    invoke-interface {v4, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 144
    .end local v0    # "requestFilter":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;
    :cond_0
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->requestFilterLoadingStatusMap:Ljava/util/Map;

    .line 145
    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateAfterNameChange(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/Date;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .param p1, "x1"    # Ljava/util/Date;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateAfterSuccessfulSuspend(Ljava/util/Date;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateAfterSuccessfulKeep()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->id:J

    return-wide v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateDataAfterRosterChange(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateDataAfterSettingsChange(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateDataAfterClubChange(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    return-void
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateAfterOwnerTransfer(Ljava/lang/String;)V

    return-void
.end method

.method private doMeRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 1
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;
    .param p2, "role"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;
    .param p3, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    .prologue
    .line 352
    .line 354
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 352
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 357
    return-void
.end method

.method private doMeRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 1
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;
    .param p2, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    .prologue
    .line 348
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doMeRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 349
    return-void
.end method

.method private doRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 7
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;
    .param p3, "role"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;
    .param p4, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;",
            "Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 360
    .local p2, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;)V

    .line 366
    .local v0, "rosterChangeTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeAsyncTask;->load(Z)V

    .line 367
    return-void
.end method

.method private declared-synchronized getFiltersLoadingStatus(Ljava/util/List;)Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;"
        }
    .end annotation

    .prologue
    .line 856
    .local p1, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 857
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v0

    .line 859
    .local v0, "hashCode":I
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->requestFilterLoadingStatusMap:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 861
    .local v1, "loadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    if-nez v1, :cond_0

    .line 862
    new-instance v1, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .end local v1    # "loadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    .line 863
    .restart local v1    # "loadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->requestFilterLoadingStatusMap:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 866
    :cond_0
    monitor-exit p0

    return-object v1

    .line 856
    .end local v0    # "hashCode":I
    .end local v1    # "loadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic lambda$rxLoad$0(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "requestFilters"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 193
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubHubService()Lcom/microsoft/xbox/service/clubs/IClubHubService;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/microsoft/xbox/service/clubs/IClubHubService;->getClubs(Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$rxLoad$1(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 1
    .param p0, "clubs"    # Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 195
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    return-object v0
.end method

.method static synthetic lambda$rxLoad$2(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "requestFilters"    # Ljava/util/List;
    .param p2, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 199
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/util/List;)V

    return-void
.end method

.method private onSocialTagsChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 884
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 897
    :goto_0
    return-void

    .line 888
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateSocialTags()V

    .line 889
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ClubData:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0

    .line 894
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->TAG:Ljava/lang/String;

    const-string v1, "Social tags changed to invalid state. Using old data."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 884
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private overrideWithLocalImages(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 3
    .param p1, "newClub"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 968
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->localDisplayImage:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->localBackgroundImage:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 985
    .end local p1    # "newClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :goto_0
    return-object p1

    .line 971
    .restart local p1    # "newClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v0

    .line 973
    .local v0, "profileBuilder":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->localDisplayImage:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 974
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->localDisplayImage:Ljava/lang/String;

    .line 975
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v1

    .line 976
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    .line 974
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->displayImageUrl(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    .line 979
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->localBackgroundImage:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 980
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->backgroundImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->localBackgroundImage:Ljava/lang/String;

    .line 981
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v1

    .line 982
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    .line 980
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->backgroundImageUrl(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    .line 985
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 986
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 987
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object p1

    goto :goto_0
.end method

.method private setNewClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 5
    .param p1, "newClub"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 956
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->overrideWithLocalImages(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 958
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 959
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->invalidateData()V

    .line 962
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateSocialTags()V

    .line 964
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->ClubData:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v0, v2, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 965
    return-void

    :cond_1
    move-object v0, v1

    .line 956
    goto :goto_0
.end method

.method private updateAfterNameChange(Ljava/lang/String;)V
    .locals 3
    .param p1, "newName"    # Ljava/lang/String;

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1025
    :cond_0
    const-string v0, "Cannot update after club name change"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 1036
    :goto_0
    return-void

    .line 1029
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 1030
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 1031
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v2

    .line 1032
    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v2

    .line 1033
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v2

    .line 1031
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->name(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v1

    .line 1034
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    .line 1030
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 1035
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 1029
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setNewClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0
.end method

.method private updateAfterOwnerTransfer(Ljava/lang/String;)V
    .locals 5
    .param p1, "newOwner"    # Ljava/lang/String;

    .prologue
    .line 1001
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1002
    :cond_0
    const-string v3, "Cannot update after transfer"

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 1021
    :goto_0
    return-void

    .line 1006
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->ownerXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 1008
    .local v0, "builder":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v2

    .line 1009
    .local v2, "oldSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1010
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1011
    .local v1, "newViewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v1, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1013
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v3

    .line 1014
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;

    move-result-object v4

    .line 1015
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;->roles(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;

    move-result-object v4

    .line 1016
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v4

    .line 1014
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->viewerRoles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v3

    .line 1017
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    .line 1013
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    .line 1020
    .end local v1    # "newViewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    :cond_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setNewClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0
.end method

.method private updateAfterSuccessfulKeep()V
    .locals 3

    .prologue
    .line 1068
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-nez v1, :cond_0

    .line 1069
    const-string v1, "Data was nulled during a keep"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 1075
    :goto_0
    return-void

    .line 1073
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->None:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->state(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 1074
    .local v0, "builder":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setNewClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0
.end method

.method private updateAfterSuccessfulSuspend(Ljava/util/Date;)V
    .locals 3
    .param p1, "suspendedUntil"    # Ljava/util/Date;

    .prologue
    .line 1055
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-nez v1, :cond_0

    .line 1056
    const-string v1, "Data was nulled during a delete"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 1065
    :goto_0
    return-void

    .line 1060
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    .line 1061
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->state(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 1062
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->suspendedUntilUtc(Ljava/util/Date;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 1064
    .local v0, "builder":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setNewClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0
.end method

.method private updateChatNotificationRegistration(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)V
    .locals 4
    .param p1, "setting"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    .prologue
    .line 376
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;-><init>(J)V

    .line 377
    .local v0, "currentChannel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->setChatNotificationSetting(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)V

    .line 378
    return-void
.end method

.method private updateDataAfterClubChange(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 1
    .param p1, "updatedData"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-nez v0, :cond_0

    .line 1079
    const-string v0, "Data was nulled during roster change"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 1084
    :goto_0
    return-void

    .line 1083
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setNewClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0
.end method

.method private updateDataAfterRosterChange(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 944
    .local p1, "clubMembersResponse":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 953
    :cond_0
    :goto_0
    return-void

    .line 948
    :cond_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertFalse(Z)V

    .line 950
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 951
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->modifyWithClubMembers(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setNewClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0
.end method

.method private updateDataAfterSettingsChange(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 1
    .param p1, "newClub"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 992
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-nez v0, :cond_0

    .line 993
    const-string v0, "Data was nulled during roster change"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 998
    :goto_0
    return-void

    .line 997
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setNewClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0
.end method

.method private updateSocialTags()V
    .locals 4

    .prologue
    .line 930
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->socialTags:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 932
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 933
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tags()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->value()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 934
    .local v1, "tag":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getTag(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    move-result-object v0

    .line 936
    .local v0, "socialTag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    if-eqz v0, :cond_0

    .line 937
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->socialTags:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 941
    .end local v0    # "socialTag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    .end local v1    # "tag":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public acceptInvitation(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 266
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->ADD_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doMeRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 268
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->DirectMention:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateChatNotificationRegistration(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)V

    .line 269
    return-void
.end method

.method public ban(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 298
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 300
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->ADD_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    .line 302
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;->Banned:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    .line 300
    invoke-direct {p0, v0, v1, v2, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 305
    return-void
.end method

.method public cancelRequestToJoin(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 246
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doMeRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 247
    return-void
.end method

.method public deleteClub(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 2
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 831
    .local p1, "resultAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/toolkit/AsyncActionStatus;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 832
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->load(Z)V

    .line 833
    return-void
.end method

.method public demoteFromModerator(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 338
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 340
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    .line 342
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    .line 340
    invoke-direct {p0, v0, v1, v2, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 345
    return-void
.end method

.method public enableJoin(ZLcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V
    .locals 7
    .param p1, "enabled"    # Z
    .param p2, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 420
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 421
    const-string v3, "Can\'t update enableJoin setting."

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 442
    :goto_0
    return-void

    .line 425
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 426
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setRequestToJoinEnabled(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "requestToJoinEnabled"

    .line 427
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    .line 429
    .local v2, "request":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 430
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 431
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->requestToJoinEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 432
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 433
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 431
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->requestToJoinEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 434
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    .line 430
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    .line 435
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 437
    .local v1, "newData":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 441
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public enableLfg(ZLcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    .locals 8
    .param p1, "enabled"    # Z
    .param p2, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 446
    if-eqz p1, :cond_1

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 447
    .local v1, "clubHubSettingsRole":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    :goto_0
    if-eqz p1, :cond_2

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 449
    .local v2, "clubProfileRole":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 450
    :cond_0
    const-string v4, "Can\'t update settings."

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 471
    :goto_2
    return-void

    .line 446
    .end local v1    # "clubHubSettingsRole":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    .end local v2    # "clubProfileRole":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    goto :goto_0

    .line 447
    .restart local v1    # "clubHubSettingsRole":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    goto :goto_1

    .line 454
    .restart local v2    # "clubProfileRole":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 455
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 456
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 457
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v7

    .line 458
    invoke-virtual {v7, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v7

    .line 459
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v7

    .line 457
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->join(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v6

    .line 460
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v6

    .line 456
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->lfg(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v5

    .line 461
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    .line 455
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v4

    .line 462
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v3

    .line 464
    .local v3, "newClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;

    new-instance v4, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v5, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 465
    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWhoCanJoinLfg(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v5

    const-string v6, "whoCanJoinLfg"

    .line 466
    invoke-static {v6}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    invoke-direct {v0, p0, v4, v3, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V

    .line 470
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->load(Z)V

    goto :goto_2
.end method

.method public enableMatureContentEnabled(ZLcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V
    .locals 7
    .param p1, "enabled"    # Z
    .param p2, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 474
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 475
    const-string v3, "Can\'t update matureContentEnabled setting."

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 496
    :goto_0
    return-void

    .line 479
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 480
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setMatureContentEnabled(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "matureContentEnabled"

    .line 481
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    .line 483
    .local v2, "request":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 484
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 485
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->matureContentEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 486
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 487
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 485
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->matureContentEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 488
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    .line 484
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    .line 489
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 491
    .local v1, "newData":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 495
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public enableWatchClubTitlesOnly(ZLcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V
    .locals 7
    .param p1, "enabled"    # Z
    .param p2, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 499
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 500
    const-string v3, "Can\'t update watchClubTitlesOnly setting."

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 521
    :goto_0
    return-void

    .line 504
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 505
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWatchClubTitlesOnly(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "watchClubTitlesOnly"

    .line 506
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    .line 508
    .local v2, "request":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 509
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 510
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->watchClubTitlesOnly()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 511
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 512
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 510
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->watchClubTitlesOnly(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 513
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    .line 509
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    .line 514
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 516
    .local v1, "newData":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 520
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public followClub(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 258
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->ADD_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;->Follower:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doMeRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 259
    return-void
.end method

.method public getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 148
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->id:J

    return-wide v0
.end method

.method public getSocialTags()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->socialTags:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasValidData(Ljava/util/List;)Z
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 174
    .local p1, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 176
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    .line 177
    .local v0, "requestFilter":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->lastRefreshMap:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 178
    const/4 v1, 0x0

    .line 182
    .end local v0    # "requestFilter":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->hasValidData()Z

    move-result v1

    goto :goto_0
.end method

.method public ignoreInvites(Ljava/util/List;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 287
    .local p1, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 288
    .local v0, "xuidsCopy":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 290
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 295
    return-void
.end method

.method public load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)V

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public load(ZLjava/util/List;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    .local p2, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 212
    if-nez p1, :cond_0

    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->shouldRefresh(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v1, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;

    invoke-direct {v1, p0, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized loadAsync(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 216
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->isLoading:Z

    if-nez v0, :cond_0

    .line 217
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ClubData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v1, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    :cond_0
    monitor-exit p0

    return-void

    .line 216
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized loadAsync(ZLjava/util/List;)V
    .locals 7
    .param p1, "forceRefresh"    # Z
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 222
    .local p2, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 225
    .local v0, "requestFiltersCopy":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 227
    if-nez p1, :cond_0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->shouldRefresh(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 229
    .local v1, "forceLoad":Z
    :goto_0
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->lifetime:J

    if-eqz v1, :cond_2

    const/4 v4, 0x0

    .line 233
    :goto_1
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getFiltersLoadingStatus(Ljava/util/List;)Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    move-result-object v5

    new-instance v6, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;

    invoke-direct {v6, p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)V

    .line 229
    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    monitor-exit p0

    return-void

    .line 227
    .end local v1    # "forceLoad":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 229
    .restart local v1    # "forceLoad":Z
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->lastRefreshTime:Ljava/util/Date;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 222
    .end local v0    # "requestFiltersCopy":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    .end local v1    # "forceLoad":Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public loadSocialTagsAsync()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->loadSystemTagsAsync(Z)V

    .line 239
    return-void
.end method

.method public ownerKeepClub(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 2
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 836
    .local p1, "resultAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/toolkit/AsyncActionStatus;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 837
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;->load(Z)V

    .line 838
    return-void
.end method

.method public promoteToModerator(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 328
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 330
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->ADD_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    .line 332
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    .line 330
    invoke-direct {p0, v0, v1, v2, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 335
    return-void
.end method

.method public rejectInvitation(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 272
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doMeRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 273
    return-void
.end method

.method public remove(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 318
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 320
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    .line 322
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    .line 320
    invoke-direct {p0, v0, v1, v2, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 325
    return-void
.end method

.method public removeSelfFromClub(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 4
    .param p1, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 250
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doMeRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 252
    const-string v0, "Clubs - Home Resign from Club"

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V

    .line 254
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->None:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateChatNotificationRegistration(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)V

    .line 255
    return-void
.end method

.method public requestToJoin(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 242
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->ADD_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doMeRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 243
    return-void
.end method

.method public rxLoad(ZLjava/util/List;)Lio/reactivex/Single;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    .local p2, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 188
    if-nez p1, :cond_1

    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->shouldRefresh(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->rxData:Lio/reactivex/Single;

    if-nez v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->cache()Lio/reactivex/Single;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->rxData:Lio/reactivex/Single;

    .line 202
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->rxData:Lio/reactivex/Single;

    return-object v0

    .line 193
    :cond_1
    invoke-static {p0, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 194
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 195
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 196
    invoke-virtual {v0}, Lio/reactivex/Single;->cache()Lio/reactivex/Single;

    move-result-object v0

    .line 198
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 199
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->rxData:Lio/reactivex/Single;

    goto :goto_0
.end method

.method public sendInvites(Ljava/util/List;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 276
    .local p1, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 277
    .local v0, "xuidsCopy":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 279
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->ADD_USER:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 284
    return-void
.end method

.method public setAssociatedTitles(Ljava/util/Set;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V
    .locals 8
    .param p1    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "failedCallback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 524
    .local p1, "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 526
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 527
    const-string v4, "Can\'t update associated games."

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 549
    :goto_0
    return-void

    .line 531
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 532
    .local v3, "titleIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v4, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 533
    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setTitles(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v4

    const-string v5, "titles"

    .line 534
    invoke-static {v5}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    .line 536
    .local v2, "request":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 537
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 538
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->associatedTitles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;

    move-result-object v6

    .line 539
    invoke-static {v3}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;->value(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;

    move-result-object v6

    .line 540
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v6

    .line 538
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->associatedTitles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v5

    .line 541
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    .line 537
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v4

    .line 542
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 544
    .local v1, "newData":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 548
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public setClubBackgroundImage(Ljava/lang/String;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V
    .locals 6
    .param p1, "backgroundImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "failedCallback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 642
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 643
    const-string v3, "Can\'t update background image."

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 663
    :goto_0
    return-void

    .line 647
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 648
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setBackgroundImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "backgroundImageUrl"

    .line 649
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    .line 651
    .local v2, "request":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 652
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 653
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->backgroundImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 654
    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 655
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 653
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->backgroundImageUrl(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 656
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    .line 652
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    .line 657
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 659
    .local v1, "newData":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 662
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public setClubDescription(Ljava/lang/String;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V
    .locals 6
    .param p1, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "failedCallback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 579
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 581
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 582
    const-string v3, "Can\'t update description."

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 603
    :goto_0
    return-void

    .line 586
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 587
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setDescription(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "description"

    .line 588
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    .line 590
    .local v2, "request":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 591
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 592
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->description()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 593
    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 594
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 592
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->description(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 595
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    .line 591
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    .line 596
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 598
    .local v1, "newData":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 602
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public setClubDisplayImage(Ljava/lang/String;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V
    .locals 6
    .param p1, "displayImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "failedCallback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 606
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 608
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 609
    const-string v3, "Can\'t update display image."

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 629
    :goto_0
    return-void

    .line 613
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 614
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setDisplayImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "displayImageUrl"

    .line 615
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    .line 617
    .local v2, "request":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 618
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 619
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 620
    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 621
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 619
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->displayImageUrl(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 622
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    .line 618
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    .line 623
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 625
    .local v1, "newData":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 628
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public setClubName(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 4
    .param p1, "clubName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x4L
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 824
    .local p2, "resultAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/toolkit/AsyncActionStatus;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 825
    const-wide/16 v0, 0x4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 826
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 827
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ChangeNameTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ChangeNameTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ChangeNameTask;->load(Z)V

    .line 828
    return-void
.end method

.method public setClubPreferredColor(Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V
    .locals 8
    .param p1, "color"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "failedCallback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 676
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 678
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 679
    const-string v3, "Can\'t update background image."

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 707
    :goto_0
    return-void

    .line 683
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    new-instance v4, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;

    .line 685
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColorString()Ljava/lang/String;

    move-result-object v5

    .line 686
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getSecondaryColorString()Ljava/lang/String;

    move-result-object v6

    .line 687
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getTertiaryColorString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setPreferredColor(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    .line 687
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "preferredColor"

    .line 688
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    .line 690
    .local v2, "request":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 691
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 692
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->primaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 693
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColorString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 694
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 692
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->primaryColor(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 695
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->secondaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 696
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getSecondaryColorString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 697
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 695
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->secondaryColor(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 698
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tertiaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 699
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getTertiaryColorString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 700
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 698
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->tertiaryColor(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 701
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    .line 691
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    .line 702
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 704
    .local v1, "newData":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 706
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;->load(Z)V

    goto/16 :goto_0
.end method

.method public setLocalClubBackgroundImage(Ljava/lang/String;)V
    .locals 1
    .param p1, "displayImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 666
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 667
    const-string v0, "Can\'t update display image."

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 673
    :goto_0
    return-void

    .line 671
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->localBackgroundImage:Ljava/lang/String;

    .line 672
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateDataAfterClubChange(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0
.end method

.method public setLocalClubDisplayImage(Ljava/lang/String;)V
    .locals 1
    .param p1, "displayImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 632
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 633
    const-string v0, "Can\'t update display image."

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 639
    :goto_0
    return-void

    .line 637
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->localDisplayImage:Ljava/lang/String;

    .line 638
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateDataAfterClubChange(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0
.end method

.method public setSocialTags(Ljava/util/Set;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V
    .locals 7
    .param p1    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "failedCallback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 552
    .local p1, "tags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 554
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 555
    const-string v3, "Can\'t update social tags."

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 576
    :goto_0
    return-void

    .line 559
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 560
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setTags(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "tags"

    .line 561
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    .line 563
    .local v2, "request":Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 564
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 565
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tags()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;

    move-result-object v5

    .line 566
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;->value(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;

    move-result-object v5

    .line 567
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v5

    .line 565
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->tags(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 568
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    .line 564
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    .line 569
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 571
    .local v1, "newData":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 575
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public setToDefaultSettings(Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    .locals 9
    .param p1, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x1

    .line 381
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 382
    :cond_0
    const-string v3, "Can\'t update settings."

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 417
    :goto_0
    return-void

    .line 386
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 387
    .local v2, "oldClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-ne v3, v4, :cond_2

    .line 388
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->getOpenClubDefaultSettings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 393
    .local v1, "newClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :goto_1
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;

    new-instance v4, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v5, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 395
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->matureContentEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setMatureContentEnabled(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v5

    .line 396
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->watchClubTitlesOnly()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWatchClubTitlesOnly(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v5

    .line 397
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->requestToJoinEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setRequestToJoinEnabled(Z)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v5

    .line 398
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->write()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->fromClubHubSettingsRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWhoCanChat(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v5

    .line 399
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->fromClubHubSettingsRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWhoCanJoinLfg(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v5

    .line 400
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->fromClubHubSettingsRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWhoCanInvite(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v5

    .line 401
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->post()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->fromClubHubSettingsRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWhoCanPostToFeed(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v5

    .line 402
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->create()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->fromClubHubSettingsRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWhoCanCreateLfg(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    .line 403
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "matureContentEnabled"

    aput-object v7, v5, v6

    const-string v6, "watchClubTitlesOnly"

    aput-object v6, v5, v8

    const/4 v6, 0x2

    const-string v7, "requestToJoinEnabled"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "whoCanChat"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "whoCanJoinLfg"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "whoCanInvite"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "whoCanPostToFeed"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    const-string v7, "whoCanCreateLfg"

    aput-object v7, v5, v6

    .line 404
    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    invoke-direct {v0, p0, v4, v1, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V

    .line 416
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;
    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->load(Z)V

    goto/16 :goto_0

    .line 388
    .end local v0    # "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;
    .end local v1    # "newClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 389
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Closed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-ne v3, v4, :cond_3

    .line 390
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->getClosedClubDefaultSettings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    goto/16 :goto_1

    .line 391
    :cond_3
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->getSecretClubDefaultSettings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    goto/16 :goto_1
.end method

.method public setWhoCanChat(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    .locals 7
    .param p1, "role"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 791
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 793
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 794
    :cond_0
    const-string v2, "Can\'t update settings."

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 815
    :goto_0
    return-void

    .line 798
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 799
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 800
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 801
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->write()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 802
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->fromProfileRole(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 803
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 801
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->write(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v4

    .line 804
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v4

    .line 800
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->chat(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v3

    .line 805
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    .line 799
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 806
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 808
    .local v1, "newClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;

    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 809
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWhoCanChat(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "whoCanChat"

    .line 810
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V

    .line 814
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public setWhoCanCreateLfg(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    .locals 7
    .param p1, "role"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 764
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 766
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 767
    :cond_0
    const-string v2, "Can\'t update settings."

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 788
    :goto_0
    return-void

    .line 771
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 772
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 773
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 774
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->create()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 775
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->fromProfileRole(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 776
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 774
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->create(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v4

    .line 777
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v4

    .line 773
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->lfg(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v3

    .line 778
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    .line 772
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 779
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 781
    .local v1, "newClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;

    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 782
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWhoCanCreateLfg(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "whoCanCreateLfg"

    .line 783
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V

    .line 787
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public setWhoCanInvite(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    .locals 7
    .param p1, "role"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 710
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 712
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 713
    :cond_0
    const-string v2, "Can\'t update settings."

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 734
    :goto_0
    return-void

    .line 717
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 718
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 719
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 720
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 721
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->fromProfileRole(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 722
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 720
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->inviteOrAccept(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v4

    .line 723
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v4

    .line 719
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->roster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v3

    .line 724
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    .line 718
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 725
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 727
    .local v1, "newClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;

    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 728
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWhoCanInvite(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "whoCanInvite"

    .line 729
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V

    .line 733
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public setWhoCanPostToFeed(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    .locals 7
    .param p1, "role"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 737
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 739
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 740
    :cond_0
    const-string v2, "Can\'t update settings."

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 761
    :goto_0
    return-void

    .line 744
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 745
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 746
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 747
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->post()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 748
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->fromProfileRole(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 749
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 747
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->post(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v4

    .line 750
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v4

    .line 746
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->feed(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v3

    .line 751
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    .line 745
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 752
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 754
    .local v1, "newClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;

    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    new-instance v3, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;-><init>()V

    .line 755
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->setWhoCanPostToFeed(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    move-result-object v3

    const-string v4, "whoCanPostToFeed"

    .line 756
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V

    invoke-direct {v0, p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V

    .line 760
    .local v0, "changeAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public shouldRefresh(Ljava/util/List;)Z
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 164
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    .line 165
    .local v0, "requestFilter":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->lastRefreshMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Date;

    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->lifetime:J

    invoke-static {v1, v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    const/4 v1, 0x1

    .line 170
    .end local v0    # "requestFilter":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->shouldRefresh()Z

    move-result v1

    goto :goto_0
.end method

.method public transferOwnership(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 2
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 818
    .local p2, "resultAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/toolkit/AsyncActionStatus;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 819
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 820
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->load(Z)V

    .line 821
    return-void
.end method

.method public unban(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 308
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 310
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    .line 312
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;->Banned:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    .line 310
    invoke-direct {p0, v0, v1, v2, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 315
    return-void
.end method

.method public unfollowClub(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 262
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;->REMOVE_ROLE:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;->Follower:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->doMeRosterChange(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeType;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 263
    return-void
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 871
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    move-object v0, v2

    .line 872
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 873
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 875
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 881
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    :goto_1
    return-void

    .line 871
    .end local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 877
    .restart local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    .restart local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->onSocialTagsChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_1

    .line 875
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public updateAfterSuccessfulDelete()V
    .locals 4

    .prologue
    .line 1039
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1052
    :goto_0
    return-void

    .line 1043
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 1045
    .local v0, "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_1

    .line 1046
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    .line 1049
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->id:J

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->removeClub(J)V

    .line 1051
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setNewClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0
.end method

.method public updateChatPrivileges(ZZZZ)V
    .locals 6
    .param p1, "canRead"    # Z
    .param p2, "canWrite"    # Z
    .param p3, "canModerate"    # Z
    .param p4, "canSetMessageOfTheDay"    # Z

    .prologue
    .line 1090
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v2

    .line 1092
    .local v2, "oldSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1093
    :cond_0
    const-string v3, "Can\'t update setting, don\'t have the data"

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 1121
    :goto_0
    return-void

    .line 1097
    :cond_1
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v3

    .line 1098
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v4

    .line 1099
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->write()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 1100
    invoke-virtual {v5, p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 1101
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 1099
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->write(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v4

    .line 1102
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->setChatTopic()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 1103
    invoke-virtual {v5, p4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 1104
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 1102
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->setChatTopic(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v4

    .line 1105
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 1106
    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 1107
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 1105
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v4

    .line 1108
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v4

    .line 1098
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->chat(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v0

    .line 1110
    .local v0, "newSettingsBuilder":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1112
    .local v1, "newViewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    if-eqz p3, :cond_2

    .line 1113
    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1118
    :goto_1
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;->roles(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->viewerRoles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    .line 1120
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setNewClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto/16 :goto_0

    .line 1115
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v1, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public updatePresence(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 12
    .param p1, "clubPresenceState"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    const/4 v5, 0x0

    .line 842
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 844
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v8

    .line 845
    .local v8, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsMemberOf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 846
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v9

    .line 847
    .local v9, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v0

    cmp-long v0, v0, v10

    if-lez v0, :cond_0

    .line 848
    const/4 v7, 0x1

    new-instance v6, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v6}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v2

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;)V

    move v2, v7

    move-wide v3, v10

    move-object v7, v0

    invoke-static/range {v2 .. v7}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 852
    .end local v9    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1, v5, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0
.end method

.method updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/util/List;)V
    .locals 5
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 901
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    .local p2, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 903
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 904
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 905
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 910
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->overrideWithLocalImages(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 912
    if-eqz p2, :cond_2

    .line 913
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    .line 914
    .local v0, "requestFilter":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->lastRefreshMap:Ljava/util/Map;

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 907
    .end local v0    # "requestFilter":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->merge(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->data:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    goto :goto_0

    .line 917
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 918
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->TAG:Ljava/lang/String;

    const-string v2, "Failed to update club data"

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 921
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateSocialTags()V

    .line 923
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->ClubData:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v4, 0x1

    invoke-direct {v3, v1, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    if-eqz p1, :cond_3

    .line 926
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    :goto_2
    invoke-direct {v2, v3, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    .line 923
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 927
    return-void

    .line 926
    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method
