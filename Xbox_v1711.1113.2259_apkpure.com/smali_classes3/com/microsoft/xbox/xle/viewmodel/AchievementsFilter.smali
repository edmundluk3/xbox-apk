.class public final enum Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;
.super Ljava/lang/Enum;
.source "AchievementsFilter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

.field public static final enum ALL:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

.field public static final enum COMPLETED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

.field public static final enum LOCKED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;


# instance fields
.field private final resId:I

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    const-string v1, "ALL"

    const v2, 0x7f0705d9

    const-string v3, "Unknown"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->ALL:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    .line 13
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    const-string v1, "COMPLETED"

    const v2, 0x7f0705da

    const-string v3, "Unknown"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->COMPLETED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    const-string v1, "LOCKED"

    const v2, 0x7f0705db

    const-string v3, "Unknown"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->LOCKED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    .line 11
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->ALL:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->COMPLETED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->LOCKED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p2    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "resId"    # I
    .param p4, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 22
    iput p3, p0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->resId:I

    .line 23
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->telemetryName:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 35
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->resId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
