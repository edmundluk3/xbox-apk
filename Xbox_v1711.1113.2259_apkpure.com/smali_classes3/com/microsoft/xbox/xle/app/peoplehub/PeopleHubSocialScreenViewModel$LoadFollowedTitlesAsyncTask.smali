.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PeopleHubSocialScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadFollowedTitlesAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)V
    .locals 0

    .prologue
    .line 628
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$1;

    .prologue
    .line 628
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 631
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 632
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshFollowedTitles()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 660
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 661
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->forceLoad:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadFollowedTitles(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 663
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowedTitles()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 669
    .end local v0    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .local v1, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :goto_0
    return-object v1

    .line 666
    .end local v1    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .restart local v0    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 667
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->forceLoad:Z

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowedTitles()Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/model/TitleHubModel;->load(ZLjava/util/Set;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    :cond_1
    move-object v1, v0

    .line 669
    .end local v0    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .restart local v1    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 628
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 655
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 628
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 637
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 638
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->onLoadFollowedTitlesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 639
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 650
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->onLoadFollowedTitlesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 651
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 628
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 643
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 644
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$1102(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Z)Z

    .line 645
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowedTitlesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$1202(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Z)Z

    .line 646
    return-void
.end method
