.class public Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "GameProgressChallengesScreenAdapter.java"


# instance fields
.field private gameTitleTextView:Landroid/widget/TextView;

.field private limitedTimeChallenges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;)V
    .locals 3
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 30
    const v0, 0x7f0e069a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->screenBody:Landroid/view/View;

    .line 31
    const v0, 0x7f0e069b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    .line 34
    const v0, 0x7f0e069c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 36
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0300ff

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 3

    .prologue
    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->getShouldHideScreen()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->updateLoadingIndicator(Z)V

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->getData()Ljava/util/List;

    move-result-object v0

    .line 57
    .local v0, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->limitedTimeChallenges:Ljava/util/List;

    if-eq v1, v0, :cond_0

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;->clear()V

    .line 60
    if-eqz v0, :cond_2

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;->addAll(Ljava/util/Collection;)V

    .line 63
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 64
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->limitedTimeChallenges:Ljava/util/List;

    goto :goto_0
.end method
