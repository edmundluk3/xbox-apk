.class public abstract Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;
.super Ljava/lang/Object;
.source "ClubModerationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubDeleteReportRequest"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(JLcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;
    .locals 2
    .param p0, "reportId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "reason"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 118
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 119
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 121
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;-><init>(JLcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;)V

    return-object v0
.end method


# virtual methods
.method public abstract reason()Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract reportId()J
.end method
