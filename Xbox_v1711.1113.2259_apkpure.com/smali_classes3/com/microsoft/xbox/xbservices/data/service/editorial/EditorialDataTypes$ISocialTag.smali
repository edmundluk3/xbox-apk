.class public interface abstract Lcom/microsoft/xbox/xbservices/data/service/editorial/EditorialDataTypes$ISocialTag;
.super Ljava/lang/Object;
.source "EditorialDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/editorial/EditorialDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ISocialTag"
.end annotation


# virtual methods
.method public abstract getDisplayText()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract getId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getSymbol()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
