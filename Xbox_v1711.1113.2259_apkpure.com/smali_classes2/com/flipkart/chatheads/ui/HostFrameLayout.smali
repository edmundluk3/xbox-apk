.class public Lcom/flipkart/chatheads/ui/HostFrameLayout;
.super Landroid/widget/FrameLayout;
.source "HostFrameLayout.java"


# instance fields
.field private final container:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

.field private final manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/flipkart/chatheads/ui/ChatHeadContainer;Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chatHeadContainer"    # Lcom/flipkart/chatheads/ui/ChatHeadContainer;
    .param p3, "manager"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 16
    iput-object p3, p0, Lcom/flipkart/chatheads/ui/HostFrameLayout;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 17
    iput-object p2, p0, Lcom/flipkart/chatheads/ui/HostFrameLayout;->container:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    .line 18
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 34
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 35
    .local v0, "handled":Z
    if-nez v0, :cond_0

    .line 36
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/HostFrameLayout;->minimize()V

    move v0, v1

    .line 41
    .end local v0    # "handled":Z
    :cond_0
    return v0
.end method

.method public minimize()V
    .locals 4

    .prologue
    .line 45
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/HostFrameLayout;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getActiveArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v0

    instance-of v0, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/HostFrameLayout;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    const-class v1, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->setArrangement(Ljava/lang/Class;Landroid/os/Bundle;Z)V

    .line 48
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 22
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 23
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/HostFrameLayout;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/HostFrameLayout;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/HostFrameLayout;->getMeasuredWidth()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->onMeasure(II)V

    .line 24
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 29
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/HostFrameLayout;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->onSizeChanged(IIII)V

    .line 30
    return-void
.end method
