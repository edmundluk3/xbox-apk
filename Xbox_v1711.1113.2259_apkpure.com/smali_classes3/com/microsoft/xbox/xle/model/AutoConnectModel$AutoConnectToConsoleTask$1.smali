.class Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask$1;
.super Ljava/lang/Object;
.source "AutoConnectModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;

    .prologue
    .line 345
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask$1;->this$1:Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask$1;->this$1:Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->access$600(Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;)Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isAutoConnect()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getInstance()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask$1;->this$1:Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->access$600(Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;)Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->setLastConnectedConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 353
    :cond_0
    return-void
.end method
