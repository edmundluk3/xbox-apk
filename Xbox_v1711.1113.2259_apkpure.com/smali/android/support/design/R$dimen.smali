.class public final Landroid/support/design/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/design/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f09012d

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f09012e

.field public static final abc_action_bar_default_height_material:I = 0x7f090001

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f09012f

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f090130

.field public static final abc_action_bar_elevation_material:I = 0x7f090155

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f090156

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f090157

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f090158

.field public static final abc_action_bar_progress_bar_size:I = 0x7f090002

.field public static final abc_action_bar_stacked_max_height:I = 0x7f090159

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f09015a

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f09015b

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f09015c

.field public static final abc_action_button_min_height_material:I = 0x7f09015d

.field public static final abc_action_button_min_width_material:I = 0x7f09015e

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f09015f

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f090000

.field public static final abc_button_inset_horizontal_material:I = 0x7f090160

.field public static final abc_button_inset_vertical_material:I = 0x7f090161

.field public static final abc_button_padding_horizontal_material:I = 0x7f090162

.field public static final abc_button_padding_vertical_material:I = 0x7f090163

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f090164

.field public static final abc_config_prefDialogWidth:I = 0x7f090005

.field public static final abc_control_corner_material:I = 0x7f090165

.field public static final abc_control_inset_material:I = 0x7f090166

.field public static final abc_control_padding_material:I = 0x7f090167

.field public static final abc_dialog_fixed_height_major:I = 0x7f090006

.field public static final abc_dialog_fixed_height_minor:I = 0x7f090007

.field public static final abc_dialog_fixed_width_major:I = 0x7f090008

.field public static final abc_dialog_fixed_width_minor:I = 0x7f090009

.field public static final abc_dialog_min_width_major:I = 0x7f09000a

.field public static final abc_dialog_min_width_minor:I = 0x7f09000b

.field public static final abc_dialog_padding_material:I = 0x7f09016a

.field public static final abc_dialog_padding_top_material:I = 0x7f09016b

.field public static final abc_disabled_alpha_material_dark:I = 0x7f09016d

.field public static final abc_disabled_alpha_material_light:I = 0x7f09016e

.field public static final abc_dropdownitem_icon_width:I = 0x7f09016f

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f090170

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f090171

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f090172

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f090173

.field public static final abc_edit_text_inset_top_material:I = 0x7f090174

.field public static final abc_floating_window_z:I = 0x7f090175

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f090176

.field public static final abc_panel_menu_list_width:I = 0x7f090177

.field public static final abc_progress_bar_height_material:I = 0x7f090178

.field public static final abc_search_view_preferred_height:I = 0x7f090179

.field public static final abc_search_view_preferred_width:I = 0x7f09017a

.field public static final abc_seekbar_track_background_height_material:I = 0x7f09017b

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f09017c

.field public static final abc_select_dialog_padding_start_material:I = 0x7f09017d

.field public static final abc_switch_padding:I = 0x7f090148

.field public static final abc_text_size_body_1_material:I = 0x7f09017e

.field public static final abc_text_size_body_2_material:I = 0x7f09017f

.field public static final abc_text_size_button_material:I = 0x7f090180

.field public static final abc_text_size_caption_material:I = 0x7f090181

.field public static final abc_text_size_display_1_material:I = 0x7f090182

.field public static final abc_text_size_display_2_material:I = 0x7f090183

.field public static final abc_text_size_display_3_material:I = 0x7f090184

.field public static final abc_text_size_display_4_material:I = 0x7f090185

.field public static final abc_text_size_headline_material:I = 0x7f090186

.field public static final abc_text_size_large_material:I = 0x7f090187

.field public static final abc_text_size_medium_material:I = 0x7f090188

.field public static final abc_text_size_menu_header_material:I = 0x7f090189

.field public static final abc_text_size_menu_material:I = 0x7f09018a

.field public static final abc_text_size_small_material:I = 0x7f09018b

.field public static final abc_text_size_subhead_material:I = 0x7f09018c

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f090003

.field public static final abc_text_size_title_material:I = 0x7f09018d

.field public static final abc_text_size_title_material_toolbar:I = 0x7f090004

.field public static final design_appbar_elevation:I = 0x7f090233

.field public static final design_bottom_navigation_active_item_max_width:I = 0x7f090234

.field public static final design_bottom_navigation_active_text_size:I = 0x7f090235

.field public static final design_bottom_navigation_height:I = 0x7f090236

.field public static final design_bottom_navigation_item_max_width:I = 0x7f090237

.field public static final design_bottom_navigation_margin:I = 0x7f090238

.field public static final design_bottom_navigation_text_size:I = 0x7f090239

.field public static final design_bottom_sheet_modal_elevation:I = 0x7f09023a

.field public static final design_bottom_sheet_peek_height_min:I = 0x7f09023b

.field public static final design_fab_border_width:I = 0x7f09023c

.field public static final design_fab_elevation:I = 0x7f09023d

.field public static final design_fab_image_size:I = 0x7f09023e

.field public static final design_fab_size_mini:I = 0x7f09023f

.field public static final design_fab_size_normal:I = 0x7f090240

.field public static final design_fab_translation_z_pressed:I = 0x7f090241

.field public static final design_navigation_elevation:I = 0x7f090242

.field public static final design_navigation_icon_padding:I = 0x7f090243

.field public static final design_navigation_icon_size:I = 0x7f090244

.field public static final design_navigation_max_width:I = 0x7f090131

.field public static final design_navigation_padding_bottom:I = 0x7f090245

.field public static final design_navigation_separator_vertical_padding:I = 0x7f090246

.field public static final design_snackbar_action_inline_max_width:I = 0x7f090132

.field public static final design_snackbar_background_corner_radius:I = 0x7f090133

.field public static final design_snackbar_elevation:I = 0x7f090247

.field public static final design_snackbar_extra_spacing_horizontal:I = 0x7f090134

.field public static final design_snackbar_max_width:I = 0x7f090135

.field public static final design_snackbar_min_width:I = 0x7f090136

.field public static final design_snackbar_padding_horizontal:I = 0x7f090248

.field public static final design_snackbar_padding_vertical:I = 0x7f090249

.field public static final design_snackbar_padding_vertical_2lines:I = 0x7f090137

.field public static final design_snackbar_text_size:I = 0x7f09024a

.field public static final design_tab_max_width:I = 0x7f09024b

.field public static final design_tab_scrollable_min_width:I = 0x7f090138

.field public static final design_tab_text_size:I = 0x7f09024c

.field public static final design_tab_text_size_2line:I = 0x7f09024d

.field public static final disabled_alpha_material_dark:I = 0x7f09026a

.field public static final disabled_alpha_material_light:I = 0x7f09026b

.field public static final highlight_alpha_material_colored:I = 0x7f090325

.field public static final highlight_alpha_material_dark:I = 0x7f090326

.field public static final highlight_alpha_material_light:I = 0x7f090327

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x7f09035c

.field public static final item_touch_helper_swipe_escape_max_velocity:I = 0x7f09035d

.field public static final item_touch_helper_swipe_escape_velocity:I = 0x7f09035e

.field public static final notification_large_icon_height:I = 0x7f0903e8

.field public static final notification_large_icon_width:I = 0x7f0903e9

.field public static final notification_subtext_size:I = 0x7f0903ed


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
