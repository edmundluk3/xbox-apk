.class Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$1;
.super Ljava/lang/Object;
.source "SearchScreenViewModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->navigateToSearchResults(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 166
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/activity/SearchResultsActivity;

    if-eqz v1, :cond_0

    .line 169
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/SearchResultsActivity;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 182
    :goto_0
    return-void

    .line 175
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "navigate to a new search results page"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/SearchResultsActivity;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 178
    :catch_0
    move-exception v0

    .line 179
    .local v0, "ex":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "can\'t navigate to results screen"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
