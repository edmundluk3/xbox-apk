.class public Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;
.super Ljava/lang/Object;
.source "FriendsWhoPlayListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FriendsWhoPlayViewHolder"
.end annotation


# static fields
.field static gameTitleId:J


# instance fields
.field private favoriteIconView:Landroid/widget/TextView;

.field private gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private gamertagTextView:Landroid/widget/TextView;

.field private presenceTextView:Landroid/widget/TextView;

.field private pressenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private realNameTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 61
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gameTitleId:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inflateView(Landroid/content/Context;)Landroid/view/View;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 65
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f03010b

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 67
    .local v1, "v":Landroid/view/View;
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;-><init>()V

    .line 68
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;
    const v3, 0x7f0e05d9

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 69
    const v3, 0x7f0e05dd

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gamertagTextView:Landroid/widget/TextView;

    .line 70
    const v3, 0x7f0e05df

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->presenceTextView:Landroid/widget/TextView;

    .line 71
    const v3, 0x7f0e05da

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->favoriteIconView:Landroid/widget/TextView;

    .line 72
    const v3, 0x7f0e05de

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->realNameTextView:Landroid/widget/TextView;

    .line 73
    const v3, 0x7f0e05db

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->pressenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 74
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 76
    return-object v1
.end method


# virtual methods
.method public setDarkStyle()V
    .locals 3

    .prologue
    .line 100
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0c0141

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 101
    .local v0, "textColor":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gamertagTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gamertagTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->presenceTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->presenceTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 107
    :cond_1
    return-void
.end method

.method public showFavoriteIcon(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->favoriteIconView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 111
    return-void

    .line 110
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showPressenceIcon(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->pressenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 115
    return-void

    .line 114
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public updateUI(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 8
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 80
    if-eqz p1, :cond_1

    .line 81
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v3, :cond_0

    .line 82
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 85
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gamertagTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 86
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v4, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v3, v4, :cond_2

    iget-wide v4, p1, Lcom/microsoft/xbox/service/model/FollowersData;->titleId:J

    sget-wide v6, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gameTitleId:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 87
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->presenceTextView:Landroid/widget/TextView;

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 92
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->realNameTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 94
    iget-boolean v3, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->showFavoriteIcon(Z)V

    .line 95
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v4, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v3, v4, :cond_3

    :goto_1
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->showPressenceIcon(Z)V

    .line 97
    :cond_1
    return-void

    .line 89
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070adb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getTimeStamp()Ljava/util/Date;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateUptoWeekToDurationNow(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "lastPlayedText":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->presenceTextView:Landroid/widget/TextView;

    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0

    .end local v0    # "lastPlayedText":Ljava/lang/String;
    :cond_3
    move v1, v2

    .line 95
    goto :goto_1
.end method
