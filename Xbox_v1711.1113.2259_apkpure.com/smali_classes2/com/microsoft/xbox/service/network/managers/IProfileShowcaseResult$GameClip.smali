.class public Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
.super Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
.source "IProfileShowcaseResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameClip"
.end annotation


# instance fields
.field public clipName:Ljava/lang/String;

.field public dateRecorded:Ljava/lang/String;

.field public durationInSeconds:Ljava/lang/String;

.field public gameClipId:Ljava/lang/String;

.field public gameClipUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;",
            ">;"
        }
    .end annotation
.end field

.field public lastModified:Ljava/lang/String;

.field public rating:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;-><init>()V

    return-void
.end method


# virtual methods
.method public getClipName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->clipName:Ljava/lang/String;

    return-object v0
.end method

.method public getDurationInSeconds()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->durationInSeconds:Ljava/lang/String;

    return-object v0
.end method

.method public getIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipId:Ljava/lang/String;

    return-object v0
.end method

.method public getIsScreenshot()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getItemRoot()Ljava/lang/String;
    .locals 5

    .prologue
    .line 127
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getGameClipUriFormat()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->scid:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserCaption()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->userCaption:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->userCaption:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->clipName:Ljava/lang/String;

    goto :goto_0
.end method

.method public setGamerTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "gamertag"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gamertag:Ljava/lang/String;

    .line 124
    return-void
.end method
