.class public final Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;
.super Ljava/lang/Object;
.source "OOBEInteractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingAction;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$BackResult;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$OkResult;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;,
        Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final autoAppUpdatesChangedTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation
.end field

.field private final autoSystemUpdatesChangedTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation
.end field

.field private final dstOptionChangedTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation
.end field

.field private final enterCodeTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;",
            ">;"
        }
    .end annotation
.end field

.field private final finishTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;",
            ">;"
        }
    .end annotation
.end field

.field private final initialLoadTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation
.end field

.field private final instantOnChangedTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation
.end field

.field private final pingTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;",
            ">;"
        }
    .end annotation
.end field

.field private final timeZoneChangedTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/service/oobe/OOBEService;)V
    .locals 1
    .param p1, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p3, "oobeService"    # Lcom/microsoft/xbox/data/service/oobe/OOBEService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    .line 56
    invoke-static {p3, p2, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->enterCodeTransformer:Lio/reactivex/ObservableTransformer;

    .line 99
    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->timeZoneChangedTransformer:Lio/reactivex/ObservableTransformer;

    .line 106
    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->dstOptionChangedTransformer:Lio/reactivex/ObservableTransformer;

    .line 113
    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->instantOnChangedTransformer:Lio/reactivex/ObservableTransformer;

    .line 120
    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->autoSystemUpdatesChangedTransformer:Lio/reactivex/ObservableTransformer;

    .line 127
    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->autoAppUpdatesChangedTransformer:Lio/reactivex/ObservableTransformer;

    .line 134
    invoke-static {p1, p3, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->finishTransformer:Lio/reactivex/ObservableTransformer;

    .line 160
    invoke-static {p3, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->pingTransformer:Lio/reactivex/ObservableTransformer;

    .line 181
    return-void
.end method

.method static synthetic lambda$new$11(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "oobeService"    # Lcom/microsoft/xbox/data/service/oobe/OOBEService;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p3, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 56
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$40;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 57
    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 56
    return-object v0
.end method

.method static synthetic lambda$new$15(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 99
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$36;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 100
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 99
    return-object v0
.end method

.method static synthetic lambda$new$19(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 106
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$32;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 107
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 106
    return-object v0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 50
    invoke-static {p0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$49;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 51
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method static synthetic lambda$new$23(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 113
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$28;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 114
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 113
    return-object v0
.end method

.method static synthetic lambda$new$27(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 120
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$24;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 121
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 120
    return-object v0
.end method

.method static synthetic lambda$new$31(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 127
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$20;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 128
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 127
    return-object v0
.end method

.method static synthetic lambda$new$36(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "oobeService"    # Lcom/microsoft/xbox/data/service/oobe/OOBEService;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p3, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 134
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 135
    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 134
    return-object v0
.end method

.method static synthetic lambda$new$41(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "oobeService"    # Lcom/microsoft/xbox/data/service/oobe/OOBEService;
    .param p1, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 160
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 161
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 160
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 52
    invoke-interface {p0}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->getSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;->with(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$50;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 53
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 52
    return-object v0
.end method

.method static synthetic lambda$null$10(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "oobeService"    # Lcom/microsoft/xbox/data/service/oobe/OOBEService;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p3, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 58
    invoke-static {p3}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p3, p0, p1, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p3}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$42;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method static synthetic lambda$null$12(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 101
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;->timeZone()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->setTimeZone(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;->with(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$13(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)V
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 103
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$39;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$14(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 101
    invoke-static {p0, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$37;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 102
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$38;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 103
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 101
    return-object v0
.end method

.method static synthetic lambda$null$16(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;->enabled()Z

    move-result v0

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->setAutoDST(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;->with(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$17(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)V
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 110
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$35;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$18(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 108
    invoke-static {p0, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$33;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 109
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$34;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 110
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 108
    return-object v0
.end method

.method static synthetic lambda$null$20(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;->enabled()Z

    move-result v0

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->setInstantOn(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;->with(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$21(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)V
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 117
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$31;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$22(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 115
    invoke-static {p0, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$29;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 116
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$30;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 117
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 115
    return-object v0
.end method

.method static synthetic lambda$null$24(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 122
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;->enabled()Z

    move-result v0

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->setAutoUpdateSystem(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;->with(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$25(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)V
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$27;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$26(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 122
    invoke-static {p0, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$25;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 123
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$26;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 124
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 122
    return-object v0
.end method

.method static synthetic lambda$null$28(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 129
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;->enabled()Z

    move-result v0

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->setAutoUpdateApps(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;->with(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$29(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)V
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 131
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$23;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;)Lio/reactivex/SingleSource;
    .locals 5
    .param p0, "oobeService"    # Lcom/microsoft/xbox/data/service/oobe/OOBEService;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;
    .param p2, "ignored"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    .line 64
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->code()Ljava/lang/String;

    move-result-object v0

    .line 66
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->email()Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->gamertag()Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->gamerpic()Ljava/lang/String;

    move-result-object v3

    .line 69
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->isNewUser()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 65
    invoke-static {v1, v2, v3, v4}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->startWith(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;->with(Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;

    move-result-object v1

    .line 63
    invoke-interface {p0, v0, v1}, Lcom/microsoft/xbox/data/service/oobe/OOBEService;->createOrUpdateOOBESession(Ljava/lang/String;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$30(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 129
    invoke-static {p0, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$21;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 130
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$22;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 131
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 129
    return-object v0
.end method

.method static synthetic lambda$null$32(Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;
    .locals 1
    .param p0, "response"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 152
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;->OK:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;

    return-object v0
.end method

.method static synthetic lambda$null$33(Ljava/lang/Throwable;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;
    .locals 2
    .param p0, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 154
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    const-string v1, "Error writing settings to service"

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 155
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;->SERVICE_ERROR:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;

    return-object v0
.end method

.method static synthetic lambda$null$34(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;)V
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 157
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$19;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$35(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;)Lio/reactivex/ObservableSource;
    .locals 11
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "oobeService"    # Lcom/microsoft/xbox/data/service/oobe/OOBEService;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p3, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 136
    invoke-interface {p0}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->getSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v9

    .line 138
    .local v9, "oobeSettings":Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    invoke-interface {p0}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->getSessionCode()Ljava/lang/String;

    move-result-object v10

    .line 140
    invoke-virtual {v9}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoUpdateApps()Z

    move-result v0

    .line 141
    invoke-virtual {v9}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoUpdateSystem()Z

    move-result v1

    .line 142
    invoke-virtual {v9}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->timeZone()Ljava/lang/String;

    move-result-object v2

    .line 143
    invoke-virtual {v9}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoDST()Z

    move-result v3

    .line 144
    invoke-virtual {v9}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->instantOn()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 145
    invoke-virtual {p3}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->email()Ljava/lang/String;

    move-result-object v5

    .line 146
    invoke-virtual {p3}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->gamertag()Ljava/lang/String;

    move-result-object v6

    .line 147
    invoke-virtual {p3}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->gamerpic()Ljava/lang/String;

    move-result-object v7

    .line 148
    invoke-virtual {p3}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->isNewUser()Z

    move-result v8

    .line 139
    invoke-static/range {v0 .. v8}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->with(ZZLjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;->with(Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;

    move-result-object v0

    .line 137
    invoke-interface {p1, v10, v0}, Lcom/microsoft/xbox/data/service/oobe/OOBEService;->createOrUpdateOOBESession(Ljava/lang/String;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;)Lio/reactivex/Single;

    move-result-object v0

    .line 149
    invoke-interface {p2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 150
    invoke-interface {p2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$16;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 152
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$17;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 153
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p3}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$18;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 157
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 137
    return-object v0
.end method

.method static synthetic lambda$null$37(Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;
    .locals 5
    .param p0, "response"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 164
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleUpdateStep()Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 165
    .local v1, "consoleUpdateStep":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleUpdateProgress()Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 166
    .local v0, "consoleUpdateProgress":I
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    const/16 v2, 0x64

    if-ne v0, v2, :cond_0

    .line 168
    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;->CONSOLE_COMPLETE:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;

    .line 170
    :goto_0
    return-object v2

    :cond_0
    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;->OK:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;

    goto :goto_0
.end method

.method static synthetic lambda$null$38(Ljava/lang/Throwable;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;
    .locals 2
    .param p0, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 176
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    const-string v1, "Error pinging service"

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 177
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;->OK:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;

    return-object v0
.end method

.method static synthetic lambda$null$39(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingAction;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;)V
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 179
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;)V
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;
    .param p2, "ignored"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->code()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->setSessionCode(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$null$40(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "oobeService"    # Lcom/microsoft/xbox/data/service/oobe/OOBEService;
    .param p1, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 162
    invoke-interface {p1}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->getSessionCode()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/service/oobe/OOBEService;->getOOBESession(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 163
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$12;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 174
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 179
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 162
    return-object v0
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;)V
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p1, "response"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 74
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->setConsoleId(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;
    .locals 1
    .param p0, "response"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->OK:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    return-object v0
.end method

.method static synthetic lambda$null$7(Ljava/lang/Throwable;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;
    .locals 3
    .param p0, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    instance-of v1, p0, Lretrofit2/HttpException;

    if-eqz v1, :cond_0

    move-object v0, p0

    .line 80
    check-cast v0, Lretrofit2/HttpException;

    .line 81
    .local v0, "httpException":Lretrofit2/HttpException;
    invoke-virtual {v0}, Lretrofit2/HttpException;->code()I

    move-result v1

    const/16 v2, 0x194

    if-ne v1, v2, :cond_0

    .line 82
    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->BAD_INPUT:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    .line 88
    .end local v0    # "httpException":Lretrofit2/HttpException;
    :goto_0
    return-object v1

    .line 87
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    const-string v2, "Error retrieving session from service"

    invoke-static {v1, v2, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 88
    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->SERVICE_ERROR:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    goto :goto_0
.end method

.method static synthetic lambda$null$8(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;
    .param p1, "oobeService"    # Lcom/microsoft/xbox/data/service/oobe/OOBEService;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p3, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .param p4, "i"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->code()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[A-Z0-9]{6}"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->code()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/microsoft/xbox/data/service/oobe/OOBEService;->getOOBESession(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 62
    invoke-interface {p2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p1, p0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$44;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 70
    invoke-interface {p2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p3, p0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$45;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 73
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p3}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$46;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 74
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$47;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$48;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 76
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->BAD_INPUT:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;)V
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$43;->lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method


# virtual methods
.method public getAutoAppUpdatesChangedTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->autoAppUpdatesChangedTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getAutoSystemUpdatesChangedTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->autoSystemUpdatesChangedTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getDstOptionChangedTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->dstOptionChangedTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getEnterCodeTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->enterCodeTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getFinishTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->finishTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getInitialLoadTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getInstantOnChangedTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->instantOnChangedTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getPingTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->pingTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getTimeZoneChangedTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->timeZoneChangedTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method
