.class public Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;
.super Ljava/lang/Object;
.source "EDS32ServiceManager.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;


# static fields
.field private static final ALBUM_DETAILS_FIELDS:Ljava/lang/String; = "AllTimeAverageRating.ReleaseDate.Genres.Images.ParentItems.Duration.IsExplicit.PrimaryArtist.Label.LabelOwner.TrackCount.ZuneId"

.field private static final APP_DETAILS_FIELDS:Ljava/lang/String; = "Description.TitleId.ReleaseDate.Images.PublisherName.AllTimeAverageRating.Genres.HasCompanion.isbundle.BundlePrimaryItemId.IsPartOfAnyBundle"

.field private static final BROWSE_DESIRED_FIELDS_PARAMS:Ljava/lang/String; = "&desired=id.name.description.releasedate.images.primaryartist.alltimeratingcount.alltimeaveragerating.HasCompanion.titleid.tracknumber.duration.SeasonNumber"

.field private static final BROWSE_QUERY_FORMAT_BASE:Ljava/lang/String; = "/media/%s/browse?orderBy=%s&maxItems=10&skipItems=%d&"

.field private static final BROWSE_QUERY_FORMAT_DESIRED:Ljava/lang/String; = "desiredMediaItemTypes=%s&subscriptionLevel=%s"

.field private static final BROWSE_QUERY_ID_FORMAT:Ljava/lang/String; = "id=%s&MediaItemType=%s&"

.field private static final BRWOSE_QUERY_GENRE_FORMAT:Ljava/lang/String; = "genre=%s&"

.field private static final BUNDLES_OF_QUERY_FORMAT:Ljava/lang/String; = "/media/%s/browse?orderBy=releaseDate&id=%s&desiredMediaItemTypes=%s&mediaItemType=%s&relationship=BundledWith&targetDevices=XboxOne"

.field private static final BUNDLE_DESIRED_FIELDS_PARAMS:Ljava/lang/String; = "&desired=Images.AllTimeAverageRating.AllTimeRatingCount.releasedate.isbundle.BundlePrimaryItemId.IsPartOfAnyBundle"

.field private static final BUNDLE_FIELDS_PARAMS:Ljava/lang/String; = ".isbundle.BundlePrimaryItemId.IsPartOfAnyBundle"

.field private static final CONTINUATINO_TOKEN_PARAMTER:Ljava/lang/String; = "&continuationToken="

.field private static final DESIRED_FIELDS_PARAMS:Ljava/lang/String; = "&desired="

.field private static final DETAILS_QUERY_FORMAT:Ljava/lang/String; = "/media/%s/details?ids=%s&%s"

.field private static final DETAILS_QUERY_FORMAT_WITHOUT_IDTYPE:Ljava/lang/String; = "/media/%s/details?ids=%s"

.field private static final DETAIL_MEDIA_GROUP_QUERY:Ljava/lang/String; = "&mediaGroup=%s"

.field private static final DOMAIN_FIELDS_PARAMS:Ljava/lang/String; = "&domain=Modern"

.field private static final EDS_BROWSE_POPULAR_GAME:Ljava/lang/String; = "/media/%s/browse?orderBy=MostPopular&maxItems=3&skipItems=0&desiredMediaItemTypes=DGame&desired=Images"

.field private static final EDS_BROWSE_POPULAR_MOVIE:Ljava/lang/String; = "/media/%s/browse?orderBy=MostPopular&maxItems=5&skipItems=0&desiredMediaItemTypes=Movie&desired=Images"

.field private static final EDS_BROWSE_SNAPPABLE_APP:Ljava/lang/String; = "/media/%s/browse?orderBy=releaseDate&maxItems=10&skipItems=%d&fields=Name.TitleId.DeveloperName.PublisherName&capabilities=IsSnappable&desiredMediaItemTypes=DApp&subscriptionLevel=%s"

.field private static final EDS_BROWSE_SNAPPABLE_GAME:Ljava/lang/String; = "/media/%s/browse?orderBy=releaseDate&maxItems=10&skipItems=%d&fields=Name.TitleId.DeveloperName.PublisherName&capabilities=IsSnappable&desiredMediaItemTypes=DGame.DGameDemo&subscriptionLevel=%s"

.field private static final EDS_CANONICAL_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=Canonical"

.field private static final EDS_FILTERSTRING_COMPANION:Ljava/lang/String; = "EnhancedContentType"

.field private static final EDS_FILTERSTRING_MOVIES:Ljava/lang/String; = "MovieType"

.field private static final EDS_FILTERSTRING_MUSIC:Ljava/lang/String; = "Album.MusicArtistType"

.field private static final EDS_FILTERSTRING_MUSICALBUM:Ljava/lang/String; = "Album"

.field private static final EDS_FILTERSTRING_MUSICALL:Ljava/lang/String; = "Album.Track.MusicArtistType"

.field private static final EDS_FILTERSTRING_MUSICARTIST:Ljava/lang/String; = "MusicArtistType"

.field private static final EDS_FILTERSTRING_MUSICTRACK:Ljava/lang/String; = "Track"

.field private static final EDS_FILTERSTRING_SEPARATOR:Ljava/lang/String; = "."

.field private static final EDS_FILTERSTRING_TV:Ljava/lang/String; = "TvType"

.field private static final EDS_PARTNER_IDTYPE_QUERY_STRING:Ljava/lang/String; = "ScopeIdType=Title&ScopeId=%08X&idType=ScopedMediaId&desiredMediaItemTypes=Movie.TVShow.TVEpisode.TVSeries.TVSeason"

.field private static final EDS_STORE_SEARCH_ADDON_FILTER:Ljava/lang/String; = "DDurable.DConsumable"

.field private static final EDS_STORE_SEARCH_APP_FILTER:Ljava/lang/String; = "DApp"

.field private static final EDS_STORE_SEARCH_GAME_FILTER:Ljava/lang/String; = "DGame"

.field private static final EDS_STORE_SEARCH_POPULAR_ORDER:Ljava/lang/String; = "MostPopular"

.field private static final EDS_STORE_SEARCH_RECENT_ORDER:Ljava/lang/String; = "ReleaseDate"

.field private static final EDS_STORE_SEARCH_RELEASE_DATE_ASCENDING:Ljava/lang/String; = "ReleaseDateAscending"

.field private static final EDS_TITLE_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=XboxHexTitle"

.field private static final EDS_XBOXONE_FILTERSTRING_ACTIVITY:Ljava/lang/String; = "DActivity"

.field private static final EDS_XBOXONE_FILTERSTRING_APP:Ljava/lang/String; = "DApp"

.field private static final EDS_XBOXONE_FILTERSTRING_GAMES:Ljava/lang/String; = "DGame.DGameDemo.DConsumable.DDurable"

.field private static final EDS_XBOXONE_ID_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=Canonical&desiredMediaItemTypes=DGame.DGameDemo.DApp"

.field private static final EDS_XBOXONE_MUSIC_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=ZuneCatalog&desiredMediaItemTypes=MusicVideo.MusicArtist.Track.Album"

.field private static final EDS_XBOXONE_SEASON_TO_SERIES_ID_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=Canonical&desiredMediaItemTypes=TvSeason"

.field private static final EDS_XBOXONE_TITLE_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=XboxHexTitle&desiredMediaItemTypes=DGame.DGameDemo.DApp"

.field private static final EDS_XBOXONE_VIDEO_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=ZuneCatalog&desiredMediaItemTypes=Movie.TvShow.TvSeries.TvEpisode.TvSeason"

.field private static final EDS_ZUNE_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=ZuneCatalog"

.field private static final ENFORCE_AVAILABILITY_PARAMETER:Ljava/lang/String; = "&EnforceAvailability=true&conditionsets=action_browse"

.field private static final FIELD_TOKEN_QUERY:Ljava/lang/String; = "/media/%s/fields?desired=id.name.description.releasedate.images.primaryartist.alltimeratingcount.alltimeaveragerating.HasCompanion.titleid.tracknumber.duration"

.field private static final FUTURE_SHOWTIMES_QUERY_FORMAT:Ljava/lang/String; = "/media/%s/details?idType=Canonical&ids=%s&targetDevices=XboxOne&desiredMediaItemTypes=TvEpisode.TvShow.TvSeries.TvSeason.Movie&desired=Airings"

.field private static final GAME_DETAILS_FIELDS:Ljava/lang/String; = "Description.TitleId.ReleaseDate.Images.DeveloperName.PublisherName.AllTimeAverageRating.ParentalRatings.UserRatingCount.RatingDescriptors.SlideShows.Genres.Capabilities.Availabilities.HasCompanion.RelatedMedia.isbundle.BundlePrimaryItemId.IsPartOfAnyBundle"

.field private static final INCLUDED_CONTENT_QUERY_FORMAT:Ljava/lang/String; = "/media/%s/browse?orderBy=releaseDate&id=%s&desiredMediaItemTypes=%s&mediaItemType=%s&relationship=BundledProducts&targetDevices=XboxOne&maxItems=10&skipItems=%d"

.field private static final LINEUP_ID_PARAMETER:Ljava/lang/String; = "&channellineupID=%s"

.field private static final MAX_COUNT:I = 0xa

.field private static final MAX_COUNT_FOR_DETAILS:I = 0xa

.field private static final MOVIE_DETAILS_FIELDS:Ljava/lang/String; = "Description.ReleaseDate.Genres.Images.RatingId.Duration.ParentalRating.Providers.Contributors.ZuneId.Studios.ReviewSources.Airings.AllTimeAverageRating.HasCompanion"

.field private static final MUSICARTIST_DETAILS_FIELDS:Ljava/lang/String; = "AllTimeAverageRating.Description.Genres.Images.RelatedMedia.ZuneId"

.field private static final RECOMMEND_QUERY_FORMAT:Ljava/lang/String; = "/media/%s/recommendations?desiredMediaItemType=%s"

.field private static final RELATED_QUERY_FORMAT:Ljava/lang/String; = "/media/%s/related?id=%s&desiredMediaItemTypes=%s&mediaItemType=%s&targetDevices=XboxOne"

.field private static final SEARCH_DESIRED_FIELDS_PARAMS:Ljava/lang/String; = "&desired=Images.HasCompanion.releasedate.primaryartist.isbundle.BundlePrimaryItemId.IsPartOfAnyBundle"

.field private static final SEARCH_QUERY_FORMAT:Ljava/lang/String; = "/media/%s/crossMediaGroupSearch?maxItems=10&q=%s&DesiredMediaItemTypes=%s&subscriptionLevel=%s&targetDevices=XboxOne&domain=Modern"

.field private static final SEARCH_QUERY_SINGLE_FORMAT:Ljava/lang/String; = "/media/%s/singleMediaGroupSearch?maxItems=10&q=%s&DesiredMediaItemTypes=%s&subscriptionLevel=%s"

.field private static final STORE_SEARCH_FORMAT:Ljava/lang/String; = "/media/%s/browse?orderBy=%s&desiredMediaItemTypes=%s&maxItems=10&skipItems=%d&desired=id.name.ReleaseDate.Images.Providers.AllTimeAverageRating.AllTimeRatingCount"

.field private static final TARGET_DEVICE_PARAMETER:Ljava/lang/String; = "&targetDevices=xboxOne"

.field private static final TITLE_IMAGE_FIELDS:Ljava/lang/String; = "&desired=Images.TitleId"

.field private static final TV_DETAILS_FIELDS:Ljava/lang/String; = "Description.ReleaseDate.Genres.Images.ParentalRating.HasSeasons.Contributors.ZuneId.LatestEpisode.ReviewSources.Duration.AllTimeAverageRating.HasCompanion.EpisodeCount.SeasonCount.Airings.SeasonNumber.EpisodeNumber.ParentSeason.Networks.ParentSeries.Providers"

.field private static final TV_SEASON_TO_SERIES_FIELDS:Ljava/lang/String; = "&desired=ParentSeries"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static TitleIdToHex(J)Ljava/lang/String;
    .locals 6
    .param p0, "titleId"    # J

    .prologue
    .line 896
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%08X"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private browseMediaItemListInternal(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 8
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaTypeString"    # Ljava/lang/String;
    .param p3, "mediaItemType"    # I
    .param p4, "impressionGuid"    # Ljava/lang/String;
    .param p5, "orderBy"    # Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .param p6, "genre"    # Ljava/lang/String;
    .param p7, "skipItems"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 564
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 565
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 566
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 568
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v0

    .line 569
    .local v0, "membership":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "/media/%s/browse?orderBy=%s&maxItems=10&skipItems=%d&"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 570
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p5}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getOrderByString(Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 571
    .local v1, "searchUrl":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 572
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "id=%s&MediaItemType=%s&"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    invoke-static {p3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 575
    :cond_0
    if-eqz p6, :cond_1

    invoke-virtual {p6}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 576
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "genre=%s&"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p6, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 579
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "desiredMediaItemTypes=%s&subscriptionLevel=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    const/4 v6, 0x1

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 581
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&desired=id.name.description.releasedate.images.primaryartist.alltimeratingcount.alltimeaveragerating.HasCompanion.titleid.tracknumber.duration.SeasonNumber"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 582
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&targetDevices=xboxOne"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 584
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v2

    return-object v2
.end method

.method private browseSnappableAppListInternal(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 8
    .param p1, "skipItems"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 537
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 538
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 539
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 541
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v0

    .line 542
    .local v0, "membership":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "/media/%s/browse?orderBy=releaseDate&maxItems=10&skipItems=%d&fields=Name.TitleId.DeveloperName.PublisherName&capabilities=IsSnappable&desiredMediaItemTypes=DApp&subscriptionLevel=%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 543
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 545
    .local v1, "searchUrl":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v2

    return-object v2
.end method

.method private browseSnappableGameListInternal(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 8
    .param p1, "skipItems"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 550
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 551
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 552
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 554
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v0

    .line 555
    .local v0, "membership":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "/media/%s/browse?orderBy=releaseDate&maxItems=10&skipItems=%d&fields=Name.TitleId.DeveloperName.PublisherName&capabilities=IsSnappable&desiredMediaItemTypes=DGame.DGameDemo&subscriptionLevel=%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 556
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 558
    .local v1, "searchUrl":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v2

    return-object v2
.end method

.method private doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 8
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 767
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->getEDSHeader()Ljava/util/ArrayList;

    move-result-object v1

    .line 768
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    const/4 v2, 0x0

    .line 769
    .local v2, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    const/4 v3, 0x0

    .line 770
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const-string v4, "EDSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requesting "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    :try_start_0
    invoke-static {p1, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 775
    iget v4, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v5, 0xc8

    if-ne v4, v5, :cond_1

    .line 776
    iget-object v4, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    const-class v6, Ljava/util/Date;

    new-instance v7, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;

    invoke-direct {v7}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;-><init>()V

    invoke-static {v4, v5, v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 797
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v3, :cond_0

    iget-object v4, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_0

    .line 799
    :try_start_1
    iget-object v4, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 808
    :cond_0
    :goto_0
    return-object v2

    .line 779
    :cond_1
    :try_start_2
    const-string v4, "EDSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "search failed for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xfa6

    invoke-direct {v4, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 784
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :catch_0
    move-exception v0

    .line 785
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v4, "EDSServiceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v4, :cond_2

    .line 788
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 789
    :catch_1
    move-exception v4

    .line 794
    :cond_2
    :try_start_5
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xfa6

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 797
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v3, :cond_3

    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 799
    :try_start_6
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .line 802
    :cond_3
    :goto_1
    throw v4

    .line 800
    :catch_2
    move-exception v5

    goto :goto_1

    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :catch_3
    move-exception v4

    goto :goto_0
.end method

.method private getDetailFields(I)Ljava/lang/String;
    .locals 1
    .param p1, "mediaGroup"    # I

    .prologue
    .line 873
    packed-switch p1, :pswitch_data_0

    .line 889
    const-string v0, "&fields=all"

    :goto_0
    return-object v0

    .line 875
    :pswitch_0
    const-string v0, "&fields=all"

    goto :goto_0

    .line 877
    :pswitch_1
    const-string v0, "&desired=Description.TitleId.ReleaseDate.Images.DeveloperName.PublisherName.AllTimeAverageRating.ParentalRatings.UserRatingCount.RatingDescriptors.SlideShows.Genres.Capabilities.Availabilities.HasCompanion.RelatedMedia.isbundle.BundlePrimaryItemId.IsPartOfAnyBundle"

    goto :goto_0

    .line 879
    :pswitch_2
    const-string v0, "&desired=Description.TitleId.ReleaseDate.Images.PublisherName.AllTimeAverageRating.Genres.HasCompanion.isbundle.BundlePrimaryItemId.IsPartOfAnyBundle"

    goto :goto_0

    .line 881
    :pswitch_3
    const-string v0, "&desired=Description.ReleaseDate.Genres.Images.RatingId.Duration.ParentalRating.Providers.Contributors.ZuneId.Studios.ReviewSources.Airings.AllTimeAverageRating.HasCompanion"

    goto :goto_0

    .line 883
    :pswitch_4
    const-string v0, "&desired=Description.ReleaseDate.Genres.Images.ParentalRating.HasSeasons.Contributors.ZuneId.LatestEpisode.ReviewSources.Duration.AllTimeAverageRating.HasCompanion.EpisodeCount.SeasonCount.Airings.SeasonNumber.EpisodeNumber.ParentSeason.Networks.ParentSeries.Providers"

    goto :goto_0

    .line 885
    :pswitch_5
    const-string v0, "&desired=AllTimeAverageRating.ReleaseDate.Genres.Images.ParentItems.Duration.IsExplicit.PrimaryArtist.Label.LabelOwner.TrackCount.ZuneId"

    goto :goto_0

    .line 887
    :pswitch_6
    const-string v0, "&desired=AllTimeAverageRating.Description.Genres.Images.RelatedMedia.ZuneId"

    goto :goto_0

    .line 873
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private getEDS32RequestHeader()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 850
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 852
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Cache-Control"

    const-string v4, "no-store, no-cache"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 853
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Pragma"

    const-string v4, "no-cache"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 854
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Accept"

    const-string v4, "application/json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 857
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "x-xbl-build-version"

    const-string v4, "current"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 858
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "x-xbl-client-type"

    const-string v4, "Companion"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 859
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "x-xbl-device-type"

    const-string v4, "AndroidPhone"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 860
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "x-xbl-contract-version"

    const-string v4, "3.2"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 861
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "x-xbl-client-version"

    const-string v4, "2.0"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 863
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getContentRestrictions()Ljava/lang/String;

    move-result-object v0

    .line 864
    .local v0, "contentRestrictions":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 865
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "x-xbl-contentrestrictions"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 869
    :cond_0
    return-object v1
.end method

.method private getEDSFilterString(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)Ljava/lang/String;
    .locals 3
    .param p1, "filter"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .prologue
    .line 900
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager$1;->$SwitchMap$com$microsoft$xbox$service$model$edsv2$EDSV2SearchFilterType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 932
    const-string v0, "DApp.DGame.DGameDemo.DConsumable.DDurable.DActivity"

    .line 933
    .local v0, "filterStr":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMusicBlocked()Z

    move-result v1

    if-nez v1, :cond_0

    .line 934
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".Album.MusicArtistType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 937
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isVideoBlocked()Z

    move-result v1

    if-nez v1, :cond_1

    .line 938
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".TvType.MovieType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v1, v0

    .line 941
    .end local v0    # "filterStr":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 902
    :pswitch_0
    const-string v1, "DApp"

    goto :goto_0

    .line 905
    :pswitch_1
    const-string v1, "DGame.DGameDemo.DConsumable.DDurable"

    goto :goto_0

    .line 908
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMusicBlocked()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "Album.MusicArtistType"

    goto :goto_0

    :cond_2
    const-string v1, ""

    goto :goto_0

    .line 911
    :pswitch_3
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isVideoBlocked()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "TvType"

    goto :goto_0

    :cond_3
    const-string v1, ""

    goto :goto_0

    .line 914
    :pswitch_4
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isVideoBlocked()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "MovieType"

    goto :goto_0

    :cond_4
    const-string v1, ""

    goto :goto_0

    .line 917
    :pswitch_5
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMusicBlocked()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "MusicArtistType"

    goto :goto_0

    :cond_5
    const-string v1, ""

    goto :goto_0

    .line 920
    :pswitch_6
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMusicBlocked()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "Album"

    goto :goto_0

    :cond_6
    const-string v1, ""

    goto :goto_0

    .line 923
    :pswitch_7
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMusicBlocked()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "Track"

    goto :goto_0

    :cond_7
    const-string v1, ""

    goto :goto_0

    .line 926
    :pswitch_8
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMusicBlocked()Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "Album.Track.MusicArtistType"

    goto :goto_0

    :cond_8
    const-string v1, ""

    goto :goto_0

    .line 928
    :pswitch_9
    const-string v1, "EnhancedContentType"

    goto :goto_0

    .line 900
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private getEDSHeader()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 846
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->getEDS32RequestHeader()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private getTitleImageInternal(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "titleIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 294
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 295
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 296
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v8

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 297
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 298
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/16 v11, 0xa

    if-gt v8, v11, :cond_1

    move v8, v9

    :goto_0
    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 300
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_2

    .line 301
    :cond_0
    const/4 v8, 0x0

    .line 332
    :goto_1
    return-object v8

    :cond_1
    move v8, v10

    .line 298
    goto :goto_0

    .line 303
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v3

    .line 304
    .local v3, "locale":Ljava/lang/String;
    const/4 v5, 0x0

    .line 305
    .local v5, "searchUrl":Ljava/lang/String;
    const/4 v2, 0x0

    .line 306
    .local v2, "idTypeString":Ljava/lang/String;
    const/4 v0, 0x0

    .line 307
    .local v0, "id":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 309
    .local v1, "idList":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 310
    .local v6, "titleId":J
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_4

    .line 311
    const-string v8, "."

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    :cond_4
    const-wide/16 v12, 0x0

    cmp-long v8, v6, v12

    if-eqz v8, :cond_3

    .line 314
    invoke-static {v6, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->TitleIdToHex(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 318
    .end local v6    # "titleId":J
    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 320
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 321
    const-string v2, "idType=XboxHexTitle&desiredMediaItemTypes=DGame.DGameDemo.DApp"

    .line 322
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v12, "/media/%s/details?ids=%s&%s"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    aput-object v3, v13, v10

    aput-object v0, v13, v9

    const/4 v9, 0x2

    aput-object v2, v13, v9

    invoke-static {v11, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 328
    :goto_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "&desired=Images.TitleId"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 329
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "&targetDevices=xboxOne&domain=Modern"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 330
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v4

    .line 331
    .local v4, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_7

    .line 332
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    goto/16 :goto_1

    .line 325
    .end local v4    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_6
    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    goto :goto_3

    .line 335
    .restart local v4    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_7
    new-instance v8, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v10, 0xfae

    invoke-direct {v8, v10, v11}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v8
.end method


# virtual methods
.method public browseMediaItemList(Ljava/lang/String;IILjava/lang/String;I)Ljava/util/ArrayList;
    .locals 9
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaType"    # I
    .param p3, "mediaItemType"    # I
    .param p4, "impressionGuid"    # Ljava/lang/String;
    .param p5, "maxvalue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 422
    sget-object v5, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_RELEASEDATE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v8, p5

    invoke-virtual/range {v0 .. v8}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->browseMediaItemList(Ljava/lang/String;IILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public browseMediaItemList(Ljava/lang/String;IILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 9
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaType"    # I
    .param p3, "mediaItemType"    # I
    .param p4, "impressionGuid"    # Ljava/lang/String;
    .param p5, "orderBy"    # Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .param p6, "genre"    # Ljava/lang/String;
    .param p7, "skipItems"    # I
    .param p8, "maxvalue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 428
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v2

    .local v2, "mediaTypeString":Ljava/lang/String;
    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    .line 429
    invoke-virtual/range {v0 .. v8}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->browseMediaItemList(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public browseMediaItemList(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 13
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaTypeString"    # Ljava/lang/String;
    .param p3, "mediaItemType"    # I
    .param p4, "impressionGuid"    # Ljava/lang/String;
    .param p5, "orderBy"    # Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .param p6, "genre"    # Ljava/lang/String;
    .param p7, "skipItems"    # I
    .param p8, "maxvalue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 436
    const/4 v11, 0x0

    .line 437
    .local v11, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    const/4 v9, 0x0

    .line 438
    .local v9, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    move/from16 v8, p7

    .line 439
    .local v8, "skip":I
    const/4 v12, 0x0

    .line 441
    .local v12, "total":I
    :goto_0
    move/from16 v0, p8

    if-ge v12, v0, :cond_2

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    .line 443
    invoke-direct/range {v1 .. v8}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->browseMediaItemListInternal(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v11

    .line 444
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 445
    if-nez v9, :cond_0

    .line 446
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 449
    .restart local v9    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    :cond_0
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v10, v1, :cond_1

    .line 450
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 454
    .end local v10    # "i":I
    :cond_1
    if-eqz v11, :cond_2

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_3

    .line 462
    :cond_2
    return-object v9

    .line 458
    :cond_3
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v12, v1

    .line 459
    add-int/lit8 v8, v8, 0xa

    goto :goto_0
.end method

.method public browseSnappableAppList()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">()",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/16 v8, 0xa

    .line 466
    const/4 v4, 0x0

    .line 467
    .local v4, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    const/4 v0, 0x0

    .line 468
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    const/4 v5, 0x0

    .line 469
    .local v5, "skip":I
    const/4 v6, 0x0

    .line 471
    .local v6, "total":I
    const v3, 0x7fffffff

    .line 472
    .local v3, "maxvalue":I
    :goto_0
    if-ge v6, v3, :cond_2

    .line 474
    const/4 v4, 0x0

    .line 476
    :try_start_0
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->browseSnappableAppListInternal(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 482
    :goto_1
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 483
    if-nez v0, :cond_0

    .line 484
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 487
    .restart local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 488
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 487
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 477
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 479
    .local v1, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    const/4 v3, -0x1

    goto :goto_1

    .line 492
    .end local v1    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v7, v8, :cond_4

    .line 501
    :cond_2
    const v3, 0x7fffffff

    .line 502
    const/4 v6, 0x0

    .line 503
    const/4 v5, 0x0

    .line 504
    :goto_3
    if-ge v6, v3, :cond_6

    .line 506
    const/4 v4, 0x0

    .line 508
    :try_start_1
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->browseSnappableGameListInternal(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 514
    :goto_4
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 515
    if-nez v0, :cond_3

    .line 516
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 519
    .restart local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_5
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_5

    .line 520
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 519
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 496
    .end local v2    # "i":I
    :cond_4
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/2addr v6, v7

    .line 497
    add-int/lit8 v5, v5, 0xa

    goto/16 :goto_0

    .line 509
    :catch_1
    move-exception v1

    .line 511
    .restart local v1    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    const/4 v3, -0x1

    goto :goto_4

    .line 524
    .end local v1    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_5
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v7, v8, :cond_7

    .line 532
    :cond_6
    return-object v0

    .line 528
    :cond_7
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/2addr v6, v7

    .line 529
    add-int/lit8 v5, v5, 0xa

    goto :goto_3
.end method

.method public getBundles(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 11
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "mediaItemType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 707
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v5

    if-nez v5, :cond_1

    .line 719
    :cond_0
    :goto_0
    return-object v4

    .line 711
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    .line 712
    .local v1, "locale":Ljava/lang/String;
    const/16 v5, 0x2328

    if-ne p2, v5, :cond_2

    const-string v0, "DApp"

    .line 713
    .local v0, "desiredMediaItemTypes":Ljava/lang/String;
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "/media/%s/browse?orderBy=releaseDate&id=%s&desiredMediaItemTypes=%s&mediaItemType=%s&relationship=BundledWith&targetDevices=XboxOne"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v1, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    .line 714
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 716
    .local v3, "searchUrl":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&desired=Images.AllTimeAverageRating.AllTimeRatingCount.releasedate.isbundle.BundlePrimaryItemId.IsPartOfAnyBundle"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 718
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v2

    .line 719
    .local v2, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    goto :goto_0

    .line 712
    .end local v0    # "desiredMediaItemTypes":Ljava/lang/String;
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .end local v3    # "searchUrl":Ljava/lang/String;
    :cond_2
    const-string v0, "DGame.DGameDemo.DConsumable.DDurable"

    goto :goto_1
.end method

.method public getCombinedContentRating()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 755
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    .line 756
    .local v1, "legalLocale":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v2

    .line 757
    .local v2, "subscriptionLevel":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getAllowExplicitContent()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    .line 759
    .local v0, "isAdultAccount":Z
    :goto_0
    invoke-virtual {p0, v1, v2, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->getCombinedContentRating(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 757
    .end local v0    # "isAdultAccount":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCombinedContentRating(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p1, "legalLocale"    # Ljava/lang/String;
    .param p2, "subscriptionLevel"    # Ljava/lang/String;
    .param p3, "isAdultAccount"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 763
    const-string v0, ""

    return-object v0
.end method

.method public getFutureShowtimes(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 15
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "headendIdList"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 723
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v8

    if-nez v8, :cond_1

    .line 724
    const/4 v6, 0x0

    .line 747
    :cond_0
    return-object v6

    .line 727
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v4

    .line 728
    .local v4, "locale":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "/media/%s/details?idType=Canonical&ids=%s&targetDevices=XboxOne&desiredMediaItemTypes=TvEpisode.TvShow.TvSeries.TvSeason.Movie&desired=Airings"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    const/4 v12, 0x1

    aput-object p1, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 729
    .local v7, "searchUrl":Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 733
    .local v6, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    move-object/from16 v0, p2

    array-length v9, v0

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v2, p2, v8

    .line 734
    .local v2, "headendId":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v12, "&channellineupID=%s"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    invoke-static {v11, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v5

    .line 737
    .local v5, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v5, :cond_3

    .line 738
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 739
    .local v3, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v11, v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Airings:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    .line 740
    .local v1, "airing":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
    iput-object v2, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->HeadendId:Ljava/lang/String;

    .line 741
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 733
    .end local v1    # "airing":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
    .end local v3    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public getGameDlc()Ljava/lang/String;
    .locals 9

    .prologue
    .line 948
    const-string v5, "https://eds.dnet.xboxlive.com/media/en-us/browse?orderBy=releaseDate&desiredMediaItemTypes=DDurable.DConsumable&fields=all&mediaItemType=DGame&id=6c5402e4-3cd5-4b29-a9c4-bec7d2c7514a"

    .line 950
    .local v5, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 951
    .local v2, "result":Ljava/lang/String;
    const/4 v4, 0x0

    .line 953
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->getEDS32RequestHeader()Ljava/util/ArrayList;

    move-result-object v1

    .line 954
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    const-string v6, "https://eds.dnet.xboxlive.com/media/en-us/browse?orderBy=releaseDate&desiredMediaItemTypes=DDurable.DConsumable&fields=all&mediaItemType=DGame&id=6c5402e4-3cd5-4b29-a9c4-bec7d2c7514a"

    invoke-static {v6, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 955
    iget v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_1

    .line 956
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 957
    const-string v6, "Game DLC from EDS"

    invoke-static {v6, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 966
    :goto_0
    if-eqz v4, :cond_0

    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_0

    .line 968
    :try_start_1
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :goto_1
    move-object v3, v2

    .line 972
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Ljava/lang/String;
    .local v3, "result":Ljava/lang/String;
    :goto_2
    return-object v3

    .line 959
    .end local v3    # "result":Ljava/lang/String;
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Ljava/lang/String;
    :cond_1
    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusLine:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\nDid you log in with the right account?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 960
    const-string v6, "Game DLC from EDS"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 963
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    :catch_0
    move-exception v0

    .line 964
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v6, "edsGetGameDlc"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 966
    if-eqz v4, :cond_2

    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_2

    .line 968
    :try_start_4
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :cond_2
    :goto_3
    move-object v3, v2

    .line 972
    .end local v2    # "result":Ljava/lang/String;
    .restart local v3    # "result":Ljava/lang/String;
    goto :goto_2

    .line 966
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "result":Ljava/lang/String;
    .restart local v2    # "result":Ljava/lang/String;
    :catchall_0
    move-exception v6

    if-eqz v4, :cond_3

    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_3

    .line 968
    :try_start_5
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :cond_3
    :goto_4
    move-object v3, v2

    .line 972
    .end local v2    # "result":Ljava/lang/String;
    .restart local v3    # "result":Ljava/lang/String;
    goto :goto_2

    .line 969
    .end local v3    # "result":Ljava/lang/String;
    .restart local v2    # "result":Ljava/lang/String;
    :catch_1
    move-exception v6

    goto :goto_4

    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v6

    goto :goto_3

    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    :catch_3
    move-exception v6

    goto :goto_1
.end method

.method public getGenreList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "mediaType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 842
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIncludedContent(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 16
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "mediaItemType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 667
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v10

    if-nez v10, :cond_1

    .line 668
    const/4 v1, 0x0

    .line 703
    :cond_0
    :goto_0
    return-object v1

    .line 671
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v4

    .line 672
    .local v4, "locale":Ljava/lang/String;
    const/16 v10, 0x2328

    move/from16 v0, p2

    if-ne v0, v10, :cond_4

    const-string v2, "DApp"

    .line 673
    .local v2, "desiredMediaItemTypes":Ljava/lang/String;
    :goto_1
    invoke-static/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v5

    .line 675
    .local v5, "mediaItemTypeString":Ljava/lang/String;
    const/4 v9, -0x1

    .line 676
    .local v9, "totalOnServer":I
    const/4 v8, 0x0

    .line 677
    .local v8, "skipItems":I
    const/4 v3, 0x0

    .line 678
    .local v3, "i":I
    const/4 v7, 0x0

    .line 679
    .local v7, "searchUrl":Ljava/lang/String;
    const/4 v1, 0x0

    .line 680
    .local v1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    const/4 v6, 0x0

    .line 683
    .local v6, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v12, "/media/%s/browse?orderBy=releaseDate&id=%s&desiredMediaItemTypes=%s&mediaItemType=%s&relationship=BundledProducts&targetDevices=XboxOne&maxItems=10&skipItems=%d"

    const/4 v13, 0x5

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v13, v14

    const/4 v14, 0x1

    aput-object p1, v13, v14

    const/4 v14, 0x2

    aput-object v2, v13, v14

    const/4 v14, 0x3

    aput-object v5, v13, v14

    const/4 v14, 0x4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v11, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "&desired=Images.AllTimeAverageRating.AllTimeRatingCount.releasedate.isbundle.BundlePrimaryItemId.IsPartOfAnyBundle"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 686
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v6

    .line 687
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 688
    if-nez v1, :cond_5

    .line 689
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v10

    invoke-direct {v1, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 694
    .restart local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :goto_2
    sget-object v10, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_ALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {v6, v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getFilterTypeCount(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)I

    move-result v9

    .line 695
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-le v9, v10, :cond_3

    .line 697
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 701
    :cond_3
    if-eqz v6, :cond_0

    if-eqz v1, :cond_6

    const/4 v10, 0x1

    :goto_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v11, v9, :cond_7

    const/4 v11, 0x1

    :goto_4
    and-int/2addr v10, v11

    if-eqz v10, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/16 v11, 0xc8

    if-ge v10, v11, :cond_0

    add-int/lit8 v3, v3, 0x1

    const/16 v10, 0x8

    if-lt v3, v10, :cond_2

    goto/16 :goto_0

    .line 672
    .end local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .end local v2    # "desiredMediaItemTypes":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v5    # "mediaItemTypeString":Ljava/lang/String;
    .end local v6    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .end local v7    # "searchUrl":Ljava/lang/String;
    .end local v8    # "skipItems":I
    .end local v9    # "totalOnServer":I
    :cond_4
    const-string v2, "DGame.DGameDemo.DConsumable.DDurable"

    goto/16 :goto_1

    .line 691
    .restart local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .restart local v2    # "desiredMediaItemTypes":Ljava/lang/String;
    .restart local v3    # "i":I
    .restart local v5    # "mediaItemTypeString":Ljava/lang/String;
    .restart local v6    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .restart local v7    # "searchUrl":Ljava/lang/String;
    .restart local v8    # "skipItems":I
    .restart local v9    # "totalOnServer":I
    :cond_5
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 701
    :cond_6
    const/4 v10, 0x0

    goto :goto_3

    :cond_7
    const/4 v11, 0x0

    goto :goto_4
.end method

.method public getMediaItemDetail(Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 19
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "partnerMediaId"    # Ljava/lang/String;
    .param p3, "titleId"    # J
    .param p5, "mediaGroup"    # I
    .param p6, "impressionGuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JI",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 340
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 341
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 342
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v12

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 344
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v8

    .line 345
    .local v8, "locale":Ljava/lang/String;
    const/4 v11, 0x0

    .line 346
    .local v11, "searchUrl":Ljava/lang/String;
    const/4 v6, 0x0

    .line 347
    .local v6, "idTypeString":Ljava/lang/String;
    const/4 v5, 0x0

    .line 348
    .local v5, "id":Ljava/lang/String;
    const/4 v9, 0x0

    .line 349
    .local v9, "mediaGroupString":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v4

    .line 351
    .local v4, "headendId":Ljava/lang/String;
    :goto_0
    if-eqz p1, :cond_4

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_4

    .line 353
    move-object/from16 v5, p1

    .line 354
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "&mediaGroup=%s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static/range {p5 .. p5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaGroup;->getGroupStringName(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 355
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "/media/%s/details?ids=%s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v8, v15, v16

    const/16 v16, 0x1

    aput-object v5, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 357
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_0

    .line 358
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "&channellineupID=%s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 396
    :cond_0
    :goto_1
    if-eqz v9, :cond_1

    .line 397
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 400
    :cond_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->getDetailFields(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 401
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "&targetDevices=xboxOne&domain=Modern"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 402
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v10

    .line 403
    .local v10, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v10, :cond_e

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v12

    if-eqz v12, :cond_e

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-lez v12, :cond_e

    .line 405
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_d

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 406
    .local v7, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getAirings()Ljava/util/ArrayList;

    move-result-object v3

    .line 407
    .local v3, "airings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    if-eqz v3, :cond_2

    .line 408
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    .line 409
    .local v2, "airing":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
    iput-object v4, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->HeadendId:Ljava/lang/String;

    goto :goto_2

    .line 349
    .end local v2    # "airing":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
    .end local v3    # "airings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    .end local v4    # "headendId":Ljava/lang/String;
    .end local v7    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v10    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 361
    .restart local v4    # "headendId":Ljava/lang/String;
    :cond_4
    if-eqz p2, :cond_8

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_8

    .line 363
    move-object/from16 v5, p2

    .line 365
    const-wide/32 v12, 0x5848085b

    cmp-long v12, p3, v12

    if-nez v12, :cond_5

    .line 366
    const-string v6, "idType=ZuneCatalog"

    .line 374
    :goto_3
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "/media/%s/details?ids=%s&%s"

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v8, v15, v16

    const/16 v16, 0x1

    aput-object v5, v15, v16

    const/16 v16, 0x2

    aput-object v6, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    .line 367
    :cond_5
    const-wide/32 v12, 0x18ffc9f4

    cmp-long v12, p3, v12

    if-nez v12, :cond_6

    .line 368
    const-string v6, "idType=ZuneCatalog&desiredMediaItemTypes=MusicVideo.MusicArtist.Track.Album"

    goto :goto_3

    .line 369
    :cond_6
    const-wide/32 v12, 0x3d705025

    cmp-long v12, p3, v12

    if-nez v12, :cond_7

    .line 370
    const-string v6, "idType=ZuneCatalog&desiredMediaItemTypes=Movie.TvShow.TvSeries.TvEpisode.TvSeason"

    goto :goto_3

    .line 372
    :cond_7
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "ScopeIdType=Title&ScopeId=%08X&idType=ScopedMediaId&desiredMediaItemTypes=Movie.TVShow.TVEpisode.TVSeries.TVSeason"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 375
    :cond_8
    const-wide/16 v12, 0x0

    cmp-long v12, p3, v12

    if-eqz v12, :cond_c

    .line 378
    const-wide/32 v12, 0x3d705025

    cmp-long v12, p3, v12

    if-nez v12, :cond_9

    .line 379
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "/media/%s/details?ids=%s&%s"

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v8, v15, v16

    const/16 v16, 0x1

    const-string v17, "a489d977-8a87-4983-8df6-facea1ad6d93"

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const-string v17, "&desiredMediaItemTypes=DApp"

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    .line 380
    :cond_9
    const-wide/32 v12, 0x18ffc9f4

    cmp-long v12, p3, v12

    if-nez v12, :cond_a

    .line 381
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "/media/%s/details?ids=%s&%s"

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v8, v15, v16

    const/16 v16, 0x1

    const-string v17, "6D96DEDC-F3C9-43F8-89E3-0C95BF76AD2A"

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const-string v17, "&desiredMediaItemTypes=DApp"

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    .line 382
    :cond_a
    const-wide/32 v12, 0x3d8b930f

    cmp-long v12, p3, v12

    if-nez v12, :cond_b

    .line 383
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "/media/%s/details?ids=%s&%s"

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v8, v15, v16

    const/16 v16, 0x1

    const-string v17, "9c7e0f20-78fb-4ea7-a8bd-cf9d78059a08"

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const-string v17, "&desiredMediaItemTypes=DApp"

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    .line 387
    :cond_b
    invoke-static/range {p3 .. p4}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->TitleIdToHex(J)Ljava/lang/String;

    move-result-object v5

    .line 388
    const-string v6, "idType=XboxHexTitle&desiredMediaItemTypes=DGame.DGameDemo.DApp"

    .line 389
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "/media/%s/details?ids=%s&%s"

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v8, v15, v16

    const/16 v16, 0x1

    aput-object v5, v15, v16

    const/16 v16, 0x2

    aput-object v6, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    .line 393
    :cond_c
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    goto/16 :goto_1

    .line 413
    .restart local v10    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_d
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v12

    .line 416
    :cond_e
    new-instance v12, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v14, 0xfae

    invoke-direct {v12, v14, v15}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v12
.end method

.method public getProgrammingItems2()Lcom/microsoft/xbox/service/model/edsv2/EDSV2DiscoverData;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 138
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 139
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getCombinedContentRating()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-object v1

    .line 144
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v3

    .line 145
    .local v3, "locale":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "/media/%s/browse?orderBy=MostPopular&maxItems=3&skipItems=0&desiredMediaItemTypes=DGame&desired=Images"

    new-array v9, v11, [Ljava/lang/Object;

    aput-object v3, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 146
    .local v5, "searchUrl":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v4

    .line 149
    .local v4, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 150
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 151
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 155
    .end local v2    # "i":I
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "/media/%s/browse?orderBy=MostPopular&maxItems=5&skipItems=0&desiredMediaItemTypes=Movie&desired=Images"

    new-array v9, v11, [Ljava/lang/Object;

    aput-object v3, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 156
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v4

    .line 157
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 158
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 159
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 163
    .end local v2    # "i":I
    :cond_3
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 164
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2DiscoverData;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2DiscoverData;-><init>()V

    .line 165
    .local v1, "discoverResult":Lcom/microsoft/xbox/service/model/edsv2/EDSV2DiscoverData;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2DiscoverData;->setBrowseItems(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method public getRelated(Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 11
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaType"    # I
    .param p3, "mediaItemType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 645
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v5

    if-nez v5, :cond_1

    .line 646
    const/4 v0, 0x0

    .line 663
    :cond_0
    return-object v0

    .line 649
    :cond_1
    const/4 v0, 0x0

    .line 650
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v2

    .line 651
    .local v2, "locale":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "/media/%s/related?id=%s&desiredMediaItemTypes=%s&mediaItemType=%s&targetDevices=XboxOne"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    .line 652
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    invoke-static {p3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 654
    .local v4, "searchUrl":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&desired=Images.HasCompanion.releasedate.primaryartist.isbundle.BundlePrimaryItemId.IsPartOfAnyBundle"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 656
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v3

    .line 657
    .local v3, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 658
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 659
    .restart local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 660
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 659
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getSeriesFromSeaonId(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "Ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x0

    .line 175
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 176
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 177
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 178
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 180
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 181
    :cond_0
    const/4 v7, 0x0

    .line 211
    :goto_0
    return-object v7

    .line 184
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v4

    .line 185
    .local v4, "locale":Ljava/lang/String;
    const/4 v6, 0x0

    .line 186
    .local v6, "searchUrl":Ljava/lang/String;
    const/4 v3, 0x0

    .line 187
    .local v3, "idTypeString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 188
    .local v1, "id":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    .local v2, "idList":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 191
    .local v0, "canonicalId":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_2

    .line 192
    const-string v8, "."

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 198
    .end local v0    # "canonicalId":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 200
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 201
    const-string v3, "idType=Canonical&desiredMediaItemTypes=TvSeason"

    .line 202
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "/media/%s/details?ids=%s&%s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v4, v10, v11

    const/4 v11, 0x1

    aput-object v1, v10, v11

    const/4 v11, 0x2

    aput-object v3, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 208
    :goto_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&targetDevices=xboxOne&domain=Modern&desired=ParentSeries"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 209
    invoke-direct {p0, v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v5

    .line 210
    .local v5, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_5

    .line 211
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    goto/16 :goto_0

    .line 205
    .end local v5    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_4
    invoke-static {v11}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    goto :goto_2

    .line 214
    .restart local v5    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_5
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xfae

    invoke-direct {v7, v8, v9}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v7
.end method

.method public getSmartDJ(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SmartDJResult;
    .locals 1
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 837
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStoreItems(Lcom/microsoft/xbox/service/model/StoreBrowseType;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 17
    .param p1, "searchType"    # Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .param p3, "skipItems"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 589
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v9

    .line 590
    .local v9, "searchUrl":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    .line 593
    .local v7, "locale":Ljava/lang/String;
    sget-object v11, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager$1;->$SwitchMap$com$microsoft$xbox$service$model$StoreBrowseType:[I

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/model/StoreBrowseType;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    .line 605
    const-string v10, ""

    .line 610
    .local v10, "type":Ljava/lang/String;
    :goto_0
    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Popular:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-object/from16 v0, p2

    if-ne v0, v11, :cond_1

    .line 611
    const-string v8, "MostPopular"

    .line 618
    .local v8, "order":Ljava/lang/String;
    :goto_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "/media/%s/browse?orderBy=%s&desiredMediaItemTypes=%s&maxItems=10&skipItems=%d&desired=id.name.ReleaseDate.Images.Providers.AllTimeAverageRating.AllTimeRatingCount"

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v7, v14, v15

    const/4 v15, 0x1

    aput-object v8, v14, v15

    const/4 v15, 0x2

    aput-object v10, v14, v15

    const/4 v15, 0x3

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 620
    sget-object v11, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Games:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    move-object/from16 v0, p1

    if-ne v0, v11, :cond_0

    .line 621
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v11, "yyyy-MM-dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v12

    invoke-direct {v6, v11, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 622
    .local v6, "formater":Ljava/text/SimpleDateFormat;
    const-string v11, "GMT"

    invoke-static {v11}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 626
    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->ComingSoon:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-object/from16 v0, p2

    if-ne v0, v11, :cond_3

    .line 627
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    const-wide/32 v14, 0x5265c00

    add-long/2addr v12, v14

    invoke-direct {v4, v12, v13}, Ljava/util/Date;-><init>(J)V

    .line 628
    .local v4, "dateStart":Ljava/util/Date;
    invoke-virtual {v6, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 629
    .local v5, "dateStartStr":Ljava/lang/String;
    const-string v3, "2799-12-31"

    .line 636
    .end local v4    # "dateStart":Ljava/util/Date;
    .local v3, "dateEndStr":Ljava/lang/String;
    :goto_2
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "&releaseDateStart=%s&releaseDateEnd=%s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v5, v14, v15

    const/4 v15, 0x1

    aput-object v3, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 639
    .end local v3    # "dateEndStr":Ljava/lang/String;
    .end local v5    # "dateStartStr":Ljava/lang/String;
    .end local v6    # "formater":Ljava/text/SimpleDateFormat;
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v11

    return-object v11

    .line 595
    .end local v8    # "order":Ljava/lang/String;
    .end local v10    # "type":Ljava/lang/String;
    :pswitch_0
    const-string v10, "DGame"

    .line 596
    .restart local v10    # "type":Ljava/lang/String;
    goto/16 :goto_0

    .line 598
    .end local v10    # "type":Ljava/lang/String;
    :pswitch_1
    const-string v10, "DDurable.DConsumable"

    .line 599
    .restart local v10    # "type":Ljava/lang/String;
    goto/16 :goto_0

    .line 601
    .end local v10    # "type":Ljava/lang/String;
    :pswitch_2
    const-string v10, "DApp"

    .line 602
    .restart local v10    # "type":Ljava/lang/String;
    goto/16 :goto_0

    .line 612
    :cond_1
    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Recent:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-object/from16 v0, p2

    if-ne v0, v11, :cond_2

    .line 613
    const-string v8, "ReleaseDate"

    .restart local v8    # "order":Ljava/lang/String;
    goto/16 :goto_1

    .line 615
    .end local v8    # "order":Ljava/lang/String;
    :cond_2
    const-string v8, "ReleaseDateAscending"

    .restart local v8    # "order":Ljava/lang/String;
    goto/16 :goto_1

    .line 631
    .restart local v6    # "formater":Ljava/text/SimpleDateFormat;
    :cond_3
    const-string v5, "2013-11-12"

    .line 632
    .restart local v5    # "dateStartStr":Ljava/lang/String;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 633
    .local v2, "dateEnd":Ljava/util/Date;
    invoke-virtual {v6, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "dateEndStr":Ljava/lang/String;
    goto :goto_2

    .line 593
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getTitleImage(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 264
    .local p1, "titleIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 266
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/16 v6, 0xa

    if-gt v5, v6, :cond_1

    .line 267
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->getTitleImageInternal(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 290
    :cond_0
    return-object v2

    .line 272
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 273
    .local v2, "results":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 275
    .local v3, "tempIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 276
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    add-int/lit8 v5, v0, 0x1

    rem-int/lit8 v5, v5, 0xa

    if-eqz v5, :cond_2

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ne v5, v6, :cond_4

    .line 280
    :cond_2
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->getTitleImageInternal(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    .line 281
    .local v4, "tempResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    if-eqz v4, :cond_3

    .line 282
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 283
    .local v1, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;, "TT;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 286
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;, "TT;"
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 275
    .end local v4    # "tempResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getTitleImageFromId(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "Ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x0

    .line 219
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 220
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 221
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 222
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 224
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 225
    :cond_0
    const/4 v7, 0x0

    .line 256
    :goto_0
    return-object v7

    .line 228
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v4

    .line 229
    .local v4, "locale":Ljava/lang/String;
    const/4 v6, 0x0

    .line 230
    .local v6, "searchUrl":Ljava/lang/String;
    const/4 v3, 0x0

    .line 231
    .local v3, "idTypeString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 232
    .local v1, "id":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 234
    .local v2, "idList":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 235
    .local v0, "canonicalId":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_2

    .line 236
    const-string v8, "."

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 242
    .end local v0    # "canonicalId":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 244
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 245
    const-string v3, "idType=Canonical&desiredMediaItemTypes=DGame.DGameDemo.DApp"

    .line 246
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "/media/%s/details?ids=%s&%s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v4, v10, v11

    const/4 v11, 0x1

    aput-object v1, v10, v11

    const/4 v11, 0x2

    aput-object v3, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 252
    :goto_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&desired=Images.TitleId"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 253
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&targetDevices=xboxOne&domain=Modern&EnforceAvailability=true&conditionsets=action_browse"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 254
    invoke-direct {p0, v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v5

    .line 255
    .local v5, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_5

    .line 256
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    goto/16 :goto_0

    .line 249
    .end local v5    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_4
    invoke-static {v11}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    goto :goto_2

    .line 259
    .restart local v5    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_5
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xfae

    invoke-direct {v7, v8, v9}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v7
.end method

.method public searchMediaItems(Ljava/lang/String;ILjava/lang/String;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 10
    .param p1, "searchTerm"    # Ljava/lang/String;
    .param p2, "filter"    # I
    .param p3, "continuationToken"    # Ljava/lang/String;
    .param p4, "resultsPerPage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 812
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 813
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 815
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    .line 816
    .local v1, "locale":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v2

    .line 817
    .local v2, "membership":Ljava/lang/String;
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->forValue(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    move-result-object v0

    .line 820
    .local v0, "filterType":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    const-string v3, ""

    .line 821
    .local v3, "searchUrl":Ljava/lang/String;
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_COMPANION:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    if-ne v0, v4, :cond_1

    .line 822
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "/media/%s/singleMediaGroupSearch?maxItems=10&q=%s&DesiredMediaItemTypes=%s&subscriptionLevel=%s"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v8, 0x1

    aput-object p1, v7, v8

    const/4 v8, 0x2

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->getEDSFilterString(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 827
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&desired=Images.HasCompanion.releasedate.primaryartist.isbundle.BundlePrimaryItemId.IsPartOfAnyBundle"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 828
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 829
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&continuationToken="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 832
    :cond_0
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v4

    return-object v4

    .line 825
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "/media/%s/crossMediaGroupSearch?maxItems=10&q=%s&DesiredMediaItemTypes=%s&subscriptionLevel=%s&targetDevices=XboxOne&domain=Modern"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v8, 0x1

    aput-object p1, v7, v8

    const/4 v8, 0x2

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;->getEDSFilterString(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public setCombinedContentRating(Ljava/lang/String;)V
    .locals 0
    .param p1, "combinedContentRating"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 751
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 752
    return-void
.end method
