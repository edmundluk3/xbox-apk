.class public Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "TVSeriesSeasonsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "LoadSelectedSeasonEpisodesTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private seasonItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

.field private seasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;
    .param p2, "selectedSeasonItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 164
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->seasonItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->seasonItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->seasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    .line 166
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->seasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;->shouldRefreshChild()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->seasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;->loadListDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 175
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 177
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 189
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 158
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;Z)Z

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;Z)Z

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel$LoadSelectedSeasonEpisodesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->updateAdapter()V

    .line 184
    return-void
.end method
