.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;
.super Ljava/lang/Object;
.source "UTCMessaging.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackShowAttachment(Lcom/microsoft/xbox/service/model/entity/Entity;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

.field final synthetic val$skypeMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/entity/Entity;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$skypeMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    .line 342
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 343
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$skypeMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 345
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$skypeMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->hasAttachment()Z

    move-result v4

    if-nez v4, :cond_0

    .line 401
    :goto_0
    return-void

    .line 349
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$skypeMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSkypeAttachmentMessageContent(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;

    move-result-object v1

    .line 351
    .local v1, "attachmentContent":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 352
    .local v3, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$skypeMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 353
    const-string v4, "ConversationId"

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$skypeMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 355
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$skypeMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 356
    const-string v4, "MessageId"

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$skypeMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 358
    :cond_2
    if-eqz v1, :cond_3

    iget-object v4, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;->locator:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 359
    const-string v4, "AttachmentId"

    iget-object v5, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;->locator:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 362
    :cond_3
    const-string v0, "Messaging - Show Achievement"

    .line 364
    .local v0, "actionName":Ljava/lang/String;
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$21;->$SwitchMap$com$microsoft$xbox$service$model$entity$Entity$Type:[I

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/entity/Entity;->getType()Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 394
    const-string v4, "Invalid attachment type"

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 366
    :pswitch_0
    const-string v0, "Messaging - Show Achievement"

    .line 400
    :goto_1
    invoke-static {v0, v3}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0

    .line 370
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$15;->val$attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/entity/Entity;->getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v2

    .line 371
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$21;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType:[I

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 386
    const-string v0, "Messaging - Show Shared Item"

    .line 387
    goto :goto_1

    .line 374
    :pswitch_2
    const-string v0, "Messaging - Show Achievement"

    .line 375
    goto :goto_1

    .line 377
    :pswitch_3
    const-string v0, "Messaging - Show Screenshot"

    .line 378
    goto :goto_1

    .line 380
    :pswitch_4
    const-string v0, "Messaging - Play Clip"

    .line 381
    goto :goto_1

    .line 383
    :pswitch_5
    const-string v0, "Messaging - Play Broadcast"

    .line 384
    goto :goto_1

    .line 391
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :pswitch_6
    const-string v0, "Messaging - Play Clip"

    .line 392
    goto :goto_1

    .line 364
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_6
    .end packed-switch

    .line 371
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
