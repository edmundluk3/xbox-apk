.class Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;
.super Ljava/lang/Object;
.source "PinsScreenAdapterPhone.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config",
        "<",
        "Lcom/microsoft/xbox/service/model/pins/PinItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDrag(I)V
    .locals 2
    .param p1, "startPosition"    # I

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->getPins()Ljava/util/List;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->getPins()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$002(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;Ljava/util/List;)Ljava/util/List;

    .line 87
    :cond_0
    return-void
.end method

.method public onDrop(I)V
    .locals 3
    .param p1, "endPosition"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$200(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->getStartPosition()I

    move-result v0

    if-eq p1, v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/pins/PinItem;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$200(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->getStartPosition()I

    move-result v2

    invoke-virtual {v1, v0, v2, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->movePin(Lcom/microsoft/xbox/service/model/pins/PinItem;II)V

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$300(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)V

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->getPins()Ljava/util/List;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->updateViewOverride()V

    goto :goto_0
.end method
