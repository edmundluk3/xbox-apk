.class public abstract Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;
.super Ljava/lang/Object;
.source "ClubHomeScreenPlayersListAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$PlayerListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AndMoreItem"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(I)Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;
    .locals 1
    .param p0, "count"    # I

    .prologue
    .line 65
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/AutoValue_ClubHomeScreenPlayersListAdapter_AndMoreItem;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/AutoValue_ClubHomeScreenPlayersListAdapter_AndMoreItem;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method abstract count()I
.end method
