.class Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;
.super Ljava/lang/Object;
.source "ClubCreateConfirmationStep.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;


# instance fields
.field private backBtn:Landroid/view/View;

.field private clubTypeDescriptionView:Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;

.field private nextBtn:Landroid/view/View;

.field private titleTextView:Landroid/widget/TextView;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    .line 31
    return-void
.end method

.method private enableControls(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->backBtn:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->nextBtn:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 71
    return-void
.end method

.method static synthetic lambda$onCreateContentView$0(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->previousPage()V

    .line 41
    return-void
.end method

.method static synthetic lambda$onCreateContentView$1(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->createClub()V

    .line 46
    return-void
.end method


# virtual methods
.method public onCreateContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 36
    const v1, 0x7f030073

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 38
    .local v0, "page":Landroid/view/View;
    const v1, 0x7f0e0307

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->backBtn:Landroid/view/View;

    .line 39
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->backBtn:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    const v1, 0x7f0e0308

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->nextBtn:Landroid/view/View;

    .line 44
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->nextBtn:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    const v1, 0x7f0e0304

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->titleTextView:Landroid/widget/TextView;

    .line 49
    const v1, 0x7f0e0305

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->clubTypeDescriptionView:Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;

    .line 51
    return-object v0
.end method

.method public update()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 56
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_1

    .line 57
    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->enableControls(Z)V

    .line 58
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getClubType()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v0

    .line 59
    .local v0, "clubType":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07019f

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 60
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->getLocalizedString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getClubName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 59
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "titleText":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->clubTypeDescriptionView:Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/ui/ClubTypeDescriptionView;->setClubType(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V

    .line 66
    .end local v0    # "clubType":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .end local v1    # "titleText":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_0

    .line 64
    invoke-direct {p0, v6}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;->enableControls(Z)V

    goto :goto_0
.end method
