.class public Lcom/microsoft/xbox/service/network/managers/ESServiceManagerStub;
.super Ljava/lang/Object;
.source "ESServiceManagerStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/IESServiceManager;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/microsoft/xbox/service/network/managers/ESServiceManagerStub;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ESServiceManagerStub;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public varargs add([Lcom/microsoft/xbox/service/model/pins/PinItem;)Z
    .locals 1
    .param p1, "pinItems"    # [Lcom/microsoft/xbox/service/model/pins/PinItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public createSkypeEndpointForNotifications(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "edfRegId"    # Ljava/lang/String;
    .param p2, "putBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 93
    const/4 v0, 0x0

    return v0
.end method

.method public varargs delete([Lcom/microsoft/xbox/service/model/pins/PinItem;)Z
    .locals 1
    .param p1, "pinItems"    # [Lcom/microsoft/xbox/service/model/pins/PinItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public deleteEDFRegistrationForSkypeNotifications(Ljava/lang/String;)Z
    .locals 1
    .param p1, "edfRegId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public deleteWithSkypeChatServiceForNotifications(Ljava/lang/String;)Z
    .locals 1
    .param p1, "edfRegId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public disableNotifications(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "endpointId"    # Ljava/lang/String;
    .param p2, "lastXTokenString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 79
    return-void
.end method

.method public edfRegistrationForSkypeNotifications(Ljava/lang/String;)Z
    .locals 1
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public enableNotifications(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Ljava/lang/String;
    .locals 1
    .param p1, "registrationId"    # Ljava/lang/String;
    .param p2, "systemId"    # Ljava/lang/String;
    .param p3, "lastXTokenString"    # Ljava/lang/String;
    .param p4, "flags"    # I
    .param p5, "isLogin"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 74
    const-string v0, "dummyEndpointId"

    return-object v0
.end method

.method public getEDFRegistrationForSkypeNotifications()Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$EdfRegistrationResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPins()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/Pin;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 22
    const/4 v2, 0x0

    .line 23
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/pins/Pin;>;"
    const/4 v3, 0x0

    .line 25
    .local v3, "stream":Ljava/io/InputStream;
    :try_start_0
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v6, "stubdata/CurrentPins.json"

    invoke-virtual {v5, v6}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 26
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    .line 27
    .local v1, "response":Ljava/lang/String;
    const-class v5, Lcom/microsoft/xbox/service/model/pins/PinResponse;

    invoke-static {v1, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/pins/PinResponse;

    .line 28
    .local v4, "tempResponse":Lcom/microsoft/xbox/service/model/pins/PinResponse;
    if-eqz v4, :cond_1

    .line 29
    iget-object v2, v4, Lcom/microsoft/xbox/service/model/pins/PinResponse;->ListItems:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    :goto_0
    if-eqz v3, :cond_0

    .line 39
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 46
    .end local v1    # "response":Ljava/lang/String;
    .end local v4    # "tempResponse":Lcom/microsoft/xbox/service/model/pins/PinResponse;
    :cond_0
    :goto_1
    return-object v2

    .line 31
    .restart local v1    # "response":Ljava/lang/String;
    .restart local v4    # "tempResponse":Lcom/microsoft/xbox/service/model/pins/PinResponse;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 34
    .end local v1    # "response":Ljava/lang/String;
    .end local v4    # "tempResponse":Lcom/microsoft/xbox/service/model/pins/PinResponse;
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v5, Lcom/microsoft/xbox/service/network/managers/ESServiceManagerStub;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 37
    if-eqz v3, :cond_0

    .line 39
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 40
    :catch_1
    move-exception v5

    goto :goto_1

    .line 37
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v3, :cond_2

    .line 39
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 42
    :cond_2
    :goto_2
    throw v5

    .line 40
    .restart local v1    # "response":Ljava/lang/String;
    .restart local v4    # "tempResponse":Lcom/microsoft/xbox/service/model/pins/PinResponse;
    :catch_2
    move-exception v5

    goto :goto_1

    .end local v1    # "response":Ljava/lang/String;
    .end local v4    # "tempResponse":Lcom/microsoft/xbox/service/model/pins/PinResponse;
    :catch_3
    move-exception v6

    goto :goto_2
.end method

.method public getRecents()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 69
    const/4 v0, 0x0

    const v1, 0x7fffffff

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ESServiceManager;->appendStubRecents(Ljava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public move(Lcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)Z
    .locals 1
    .param p1, "srcItem"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p2, "dstItem"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p3, "beforeDstItem"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public subscribeWithSkypeChatServiceForNotifications(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "edfRegId"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method
