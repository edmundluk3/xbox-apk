.class public Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;
.super Landroid/widget/LinearLayout;
.source "SwitchPaneWithRefreshView.java"


# instance fields
.field private text:Ljava/lang/String;

.field private textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private typefaceSource:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 36
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v12, -0x1

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    new-array v4, v11, [I

    const v8, 0x101014f

    aput v8, v4, v10

    .line 42
    .local v4, "textAttr":[I
    const/4 v2, 0x0

    .line 43
    .local v2, "indexOfTextAttr":I
    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 44
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->text:Ljava/lang/String;

    .line 45
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 48
    new-array v6, v11, [I

    const v8, 0x1010098

    aput v8, v6, v10

    .line 49
    .local v6, "textColorAttr":[I
    invoke-virtual {p1, p2, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 50
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0c0142

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 51
    .local v1, "backupTextColor":I
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    .line 52
    .local v5, "textColor":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 55
    sget-object v8, Lcom/microsoft/xboxone/smartglass/R$styleable;->CustomTypeface:[I

    invoke-virtual {p1, p2, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 56
    invoke-virtual {v0, v10}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->typefaceSource:Ljava/lang/String;

    .line 57
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    const-string v8, "layout_inflater"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    .line 60
    .local v7, "vi":Landroid/view/LayoutInflater;
    const v8, 0x7f030222

    invoke-virtual {v7, v8, p0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 62
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v12, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 63
    .local v3, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    const v8, 0x7f0e0a94

    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v8, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 66
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v8, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    .line 68
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 76
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->typefaceSource:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->typefaceSource:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 80
    .local v0, "tf":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 83
    .end local v0    # "tf":Landroid/graphics/Typeface;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->text:Ljava/lang/String;

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    return-void
.end method

.method public setTextColor(I)V
    .locals 1
    .param p1, "textColor"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    .line 72
    return-void
.end method
