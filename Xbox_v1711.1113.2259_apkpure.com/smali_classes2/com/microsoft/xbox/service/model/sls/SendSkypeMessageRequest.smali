.class public Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
.super Ljava/lang/Object;
.source "SendSkypeMessageRequest.java"


# static fields
.field private static final transient dateFormatter:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ss.SSS"


# instance fields
.field public clientmessageid:Ljava/lang/String;

.field public composetime:Ljava/lang/String;

.field public content:Ljava/lang/String;

.field public imdisplayname:Ljava/lang/String;

.field public messagetype:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;Ljava/lang/String;)V
    .locals 1
    .param p1, "messageType"    # Ljava/lang/String;
    .param p2, "content"    # Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;
    .param p3, "gamertag"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;->getSkypeContentAttachmentFormatJson(Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "messageType"    # Ljava/lang/String;
    .param p2, "gamertag"    # Ljava/lang/String;

    .prologue
    .line 36
    const-string v0, ""

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "messageType"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .param p3, "gamertag"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;->messagetype:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;->content:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 29
    .local v0, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 30
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Z"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;->composetime:Ljava/lang/String;

    .line 31
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;->clientmessageid:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;->imdisplayname:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public static getSendSkypeMessageRequestBody(Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)Ljava/lang/String;
    .locals 4
    .param p0, "sendMessageRequest"    # Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    .prologue
    .line 45
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/gson/JsonIOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 49
    :goto_0
    return-object v1

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Lcom/google/gson/JsonIOException;
    const-string v1, "SendMessageRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in serialzation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/gson/JsonIOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const/4 v1, 0x0

    goto :goto_0
.end method
