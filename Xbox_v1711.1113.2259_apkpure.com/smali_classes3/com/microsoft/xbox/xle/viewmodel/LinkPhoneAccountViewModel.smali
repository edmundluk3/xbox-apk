.class public Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "LinkPhoneAccountViewModel.java"


# instance fields
.field private friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 17
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getResult()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    .line 18
    return-void
.end method

.method private createAdapter()V
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 120
    :cond_0
    return-void
.end method


# virtual methods
.method public isBusy()Z
    .locals 2

    .prologue
    .line 37
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->isLoading()Z

    move-result v0

    .line 38
    .local v0, "isBusy":Z
    return v0
.end method

.method public isPhoneNumberLinked()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 88
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    if-eqz v2, :cond_0

    .line 89
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getPhoneAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v0

    .line 90
    .local v0, "optedInStatus":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    .line 93
    .end local v0    # "optedInStatus":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    :cond_0
    return v1
.end method

.method public linkUnlink()V
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->linkUnlink()V

    .line 64
    return-void
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 43
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 44
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->loadAsync(Z)V

    .line 45
    return-void
.end method

.method public notInterested()V
    .locals 1

    .prologue
    .line 67
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->notInterested()V

    .line 68
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->createAdapter()V

    .line 28
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->createAdapter()V

    .line 23
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 33
    return-void
.end method

.method public showLinkPhoneAccountButtonInSettings()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToAddFriend()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    if-eqz v2, :cond_1

    .line 78
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getPhoneAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v0

    .line 79
    .local v0, "optedInStatus":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 84
    .end local v0    # "optedInStatus":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    :cond_1
    return v1
.end method

.method public showLinkPhoneAccountButtonInSuggestions()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToAddFriend()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    if-eqz v2, :cond_1

    .line 99
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getPhoneAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v0

    .line 100
    .local v0, "optedInStatus":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->DontShow:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 103
    .end local v0    # "optedInStatus":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    :cond_1
    return v1
.end method

.method public showNotInterestedButton()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToAddFriend()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    if-eqz v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getPhoneAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v0

    .line 110
    .local v0, "optedInStatus":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    .line 113
    .end local v0    # "optedInStatus":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    :cond_0
    return v1
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->FriendFinder:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 50
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_1

    .line 51
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getResult()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v0

    .line 52
    .local v0, "newResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    if-eq v1, v0, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->isPhoneStateChanged(Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 53
    :cond_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->updateView()V

    .line 55
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    if-eqz v1, :cond_1

    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecommendations(Z)V

    .line 60
    .end local v0    # "newResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :cond_1
    return-void
.end method
