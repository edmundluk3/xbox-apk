.class public final enum Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
.super Ljava/lang/Enum;
.source "NetworkStats.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/network/NetworkStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NetworkQuality"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

.field public static final enum EXCELLENT:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

.field public static final enum GOOD:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

.field public static final enum MODERATE:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

.field public static final enum POOR:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

.field public static final enum UNKNOWN:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;


# instance fields
.field private final description:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    const-string v1, "UNKNOWN"

    const-string v2, "Unknown"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->UNKNOWN:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    const-string v1, "POOR"

    const-string v2, "< 150kbps"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->POOR:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    const-string v1, "MODERATE"

    const-string v2, ">= 150kbps & <= 550kbps"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->MODERATE:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    const-string v1, "GOOD"

    const-string v2, ">= 550kbps & <= 2000kbps"

    invoke-direct {v0, v1, v6, v2}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->GOOD:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    const-string v1, "EXCELLENT"

    const-string v2, ">= 2000kbps"

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->EXCELLENT:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    .line 17
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->UNKNOWN:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->POOR:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->MODERATE:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->GOOD:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->EXCELLENT:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    aput-object v1, v0, v7

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->$VALUES:[Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "description"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput-object p3, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->description:Ljava/lang/String;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/facebook/network/connectionclass/ConnectionQuality;)Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
    .locals 1
    .param p0, "x0"    # Lcom/facebook/network/connectionclass/ConnectionQuality;

    .prologue
    .line 17
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->fromConnectionQuality(Lcom/facebook/network/connectionclass/ConnectionQuality;)Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    move-result-object v0

    return-object v0
.end method

.method private static fromConnectionQuality(Lcom/facebook/network/connectionclass/ConnectionQuality;)Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
    .locals 2
    .param p0, "connectionQuality"    # Lcom/facebook/network/connectionclass/ConnectionQuality;

    .prologue
    .line 46
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats$1;->$SwitchMap$com$facebook$network$connectionclass$ConnectionQuality:[I

    sget-object v0, Lcom/facebook/network/connectionclass/ConnectionQuality;->UNKNOWN:Lcom/facebook/network/connectionclass/ConnectionQuality;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/network/connectionclass/ConnectionQuality;

    invoke-virtual {v0}, Lcom/facebook/network/connectionclass/ConnectionQuality;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 57
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->UNKNOWN:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    :goto_0
    return-object v0

    .line 48
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->POOR:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    goto :goto_0

    .line 50
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->MODERATE:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    goto :goto_0

    .line 52
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->GOOD:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    goto :goto_0

    .line 54
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->EXCELLENT:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->$VALUES:[Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    return-object v0
.end method


# virtual methods
.method public isGreaterThan(Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 36
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 37
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->ordinal()I

    move-result v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLessThan(Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->ordinal()I

    move-result v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
