.class Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;
.super Lorg/json/JSONObject;
.source "JsonTouchPoint.java"


# instance fields
.field public final action:Lcom/microsoft/xbox/smartglass/TouchAction;

.field public final id:S

.field public final x:D

.field public final y:D


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 19
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    iput-short v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;->id:S

    .line 20
    const-string v0, "action"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/TouchAction;->fromInt(I)Lcom/microsoft/xbox/smartglass/TouchAction;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;->action:Lcom/microsoft/xbox/smartglass/TouchAction;

    .line 21
    const-string v0, "x"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;->x:D

    .line 22
    const-string v0, "y"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;->y:D

    .line 23
    return-void
.end method
