.class final Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties;
.super Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties;
.source "AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "clientType"    # Ljava/lang/Integer;
    .param p2, "protocolVersion"    # Ljava/lang/Integer;
    .param p3, "simpleConnectionState"    # Ljava/lang/Integer;
    .param p4, "deviceId"    # Ljava/lang/String;
    .param p5, "allowedInBroadcast"    # Ljava/lang/Boolean;
    .param p6, "isBroadcasting"    # Ljava/lang/Boolean;
    .param p7, "webRtcDtlsCertificateAlgorithm"    # Ljava/lang/String;
    .param p8, "webRtcDtlsCertificateThumbprint"    # Ljava/lang/String;
    .param p9, "webRtcIceUfrag"    # Ljava/lang/String;
    .param p10, "webRtcIcePwd"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct/range {p0 .. p10}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    return-void
.end method
