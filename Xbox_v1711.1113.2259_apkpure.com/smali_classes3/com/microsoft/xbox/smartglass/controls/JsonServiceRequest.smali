.class Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;
.super Ljava/lang/Object;
.source "JsonServiceRequest.java"


# instance fields
.field public final audienceUri:Ljava/lang/String;

.field public final contentType:Ljava/lang/String;

.field public final data:Ljava/lang/String;

.field public final headers:Lorg/json/JSONObject;

.field public final method:Ljava/lang/String;

.field public final sendUserToken:Ljava/lang/Boolean;

.field public final timeout:I

.field public final url:Ljava/lang/String;

.field public final withCredentials:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 7
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 26
    .local v3, "jsonObject":Lorg/json/JSONObject;
    const-string v4, "url"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->url:Ljava/lang/String;

    .line 27
    const-string v4, "method"

    const-string v5, "POST"

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->method:Ljava/lang/String;

    .line 28
    const-string v4, "contentType"

    const-string v5, "application/json"

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->contentType:Ljava/lang/String;

    .line 29
    const-string v4, "data"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    :goto_0
    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->data:Ljava/lang/String;

    .line 32
    const-string v4, "headers"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->headers:Lorg/json/JSONObject;

    .line 33
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->headers:Lorg/json/JSONObject;

    if-eqz v4, :cond_2

    .line 34
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->headers:Lorg/json/JSONObject;

    invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "fields":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 35
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 36
    .local v1, "field":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->headers:Lorg/json/JSONObject;

    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 37
    .local v0, "array":Lorg/json/JSONArray;
    if-eqz v0, :cond_0

    .line 38
    new-instance v4, Lorg/json/JSONException;

    const-string v5, "Arrays are not supported as header values."

    invoke-direct {v4, v5}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 29
    .end local v0    # "array":Lorg/json/JSONArray;
    .end local v1    # "field":Ljava/lang/String;
    .end local v2    # "fields":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :cond_1
    const-string v4, "data"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 43
    :cond_2
    const-string v4, "sendUserToken"

    invoke-virtual {v3, v4, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->sendUserToken:Ljava/lang/Boolean;

    .line 44
    const-string v4, "audienceUri"

    const-string v5, "http://xboxlive.com"

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->audienceUri:Ljava/lang/String;

    .line 45
    const-string v4, "withCredentials"

    invoke-virtual {v3, v4, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->withCredentials:Ljava/lang/Boolean;

    .line 46
    const-string v4, "timeout"

    const/16 v5, 0x3a98

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->timeout:I

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "sendUserToken"    # Z
    .param p4, "audienceUri"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->method:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->url:Ljava/lang/String;

    .line 52
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->sendUserToken:Ljava/lang/Boolean;

    .line 53
    iput-object p4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->audienceUri:Ljava/lang/String;

    .line 56
    const-string v0, "application/json"

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->contentType:Ljava/lang/String;

    .line 57
    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->data:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->headers:Lorg/json/JSONObject;

    .line 59
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->withCredentials:Ljava/lang/Boolean;

    .line 60
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->timeout:I

    .line 61
    return-void
.end method
