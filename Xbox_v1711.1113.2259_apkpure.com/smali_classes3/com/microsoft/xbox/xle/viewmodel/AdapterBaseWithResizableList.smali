.class public abstract Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "AdapterBaseWithResizableList.java"


# instance fields
.field protected displayedListItemCount:F

.field protected listItemAspectRatio:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 14
    const v0, 0x3fe38e39

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->listItemAspectRatio:F

    .line 17
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->displayedListItemCount:F

    return-void
.end method


# virtual methods
.method protected calculateListSize()V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 26
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v8, :cond_0

    iget v8, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->displayedListItemCount:F

    cmpl-float v8, v8, v7

    if-lez v8, :cond_0

    .line 27
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v8

    const v9, 0x7f0e0786

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/xle/app/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 28
    .local v2, "contentFrame":Landroid/view/View;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 30
    iget v8, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->displayedListItemCount:F

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v8, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v8

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f090257

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    mul-float v4, v8, v9

    .line 32
    .local v4, "itemSpacingInPX":F
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f090231

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0901b8

    .line 33
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    add-int/2addr v8, v9

    int-to-float v1, v8

    .line 35
    .local v1, "bottomMargin":F
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v7/app/ActionBar;->isShowing()Z

    move-result v8

    if-eqz v8, :cond_1

    move v0, v7

    .line 38
    .local v0, "actionBarHeight":F
    :goto_0
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v8

    int-to-float v8, v8

    add-float v9, v4, v1

    add-float/2addr v9, v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->getTopMargin()F

    move-result v10

    add-float/2addr v9, v10

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->displayedListItemCount:F

    div-float v3, v8, v9

    .line 39
    .local v3, "itemHeight":F
    cmpl-float v7, v3, v7

    if-lez v7, :cond_0

    .line 40
    iget v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->listItemAspectRatio:F

    mul-float v5, v3, v7

    .line 43
    .local v5, "itemWidth":F
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 44
    .local v6, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    const/high16 v7, 0x3f000000    # 0.5f

    add-float/2addr v7, v5

    float-to-int v7, v7

    iput v7, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 45
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v7, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 47
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->requestLayout()V

    .line 50
    .end local v0    # "actionBarHeight":F
    .end local v1    # "bottomMargin":F
    .end local v2    # "contentFrame":Landroid/view/View;
    .end local v3    # "itemHeight":F
    .end local v4    # "itemSpacingInPX":F
    .end local v5    # "itemWidth":F
    .end local v6    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    return-void

    .line 35
    .restart local v1    # "bottomMargin":F
    .restart local v2    # "contentFrame":Landroid/view/View;
    .restart local v4    # "itemSpacingInPX":F
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v7/app/ActionBar;->getHeight()I

    move-result v8

    int-to-float v0, v8

    goto :goto_0
.end method

.method protected getTopMargin()F
    .locals 3

    .prologue
    .line 21
    const/4 v0, 0x1

    const/high16 v1, 0x42e60000    # 115.0f

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method
