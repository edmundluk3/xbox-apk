.class Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$1;
.super Ljava/lang/Object;
.source "TagPickerDialog.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)Z
    .locals 1
    .param p1, "socialTag"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 130
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V
    .locals 1
    .param p1, "socialTag"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 117
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$400(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    .line 126
    return-void
.end method
