.class public final enum Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;
.super Ljava/lang/Enum;
.source "CircularDrawableFactory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;


# instance fields
.field final cache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->INSTANCE:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    .line 13
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    sget-object v1, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->INSTANCE:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->$VALUES:[Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 16
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->cache:Landroid/util/SparseArray;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->$VALUES:[Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    return-object v0
.end method


# virtual methods
.method public get(I)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "resId"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 19
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->cache:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    .line 21
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v1, :cond_0

    .line 22
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-static {v3, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 24
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-static {v3, v0}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawableFactory;->create(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;

    move-result-object v2

    .line 25
    .local v2, "roundedDrawable":Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;->setCircular(Z)V

    .line 27
    move-object v1, v2

    .line 28
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->cache:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 31
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "roundedDrawable":Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;
    :cond_0
    return-object v1
.end method
