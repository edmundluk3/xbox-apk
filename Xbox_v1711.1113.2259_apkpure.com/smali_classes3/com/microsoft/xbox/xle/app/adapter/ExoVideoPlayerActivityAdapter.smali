.class public Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ExoVideoPlayerActivityAdapter.java"


# instance fields
.field private bandwidthMeter:Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;

.field private eventHandler:Landroid/os/Handler;

.field private eventLogger:Lcom/microsoft/xbox/toolkit/ExoVideoPlayerEventLogger;

.field private player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

.field private final playerView:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

.field private resumePosition:J

.field private resumeWindow:I

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;

    .line 41
    const v0, 0x7f0e0593

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->playerView:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    .line 42
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->clearResumePosition()V

    .line 43
    return-void
.end method

.method private clearResumePosition()V
    .locals 2

    .prologue
    .line 108
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->resumeWindow:I

    .line 109
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->resumePosition:J

    .line 110
    return-void
.end method

.method private initializePlayer()V
    .locals 14

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 72
    new-instance v4, Lcom/microsoft/xbox/toolkit/ExoVideoPlayerEventLogger;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->getDataSource()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/toolkit/ExoVideoPlayerEventLogger;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventLogger:Lcom/microsoft/xbox/toolkit/ExoVideoPlayerEventLogger;

    .line 73
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventHandler:Landroid/os/Handler;

    .line 74
    new-instance v4, Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventHandler:Landroid/os/Handler;

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventLogger:Lcom/microsoft/xbox/toolkit/ExoVideoPlayerEventLogger;

    invoke-direct {v4, v5, v11}, Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;-><init>(Landroid/os/Handler;Lcom/google/android/exoplayer2/upstream/BandwidthMeter$EventListener;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->bandwidthMeter:Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;

    .line 75
    new-instance v8, Lcom/google/android/exoplayer2/trackselection/AdaptiveVideoTrackSelection$Factory;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->bandwidthMeter:Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;

    invoke-direct {v8, v4}, Lcom/google/android/exoplayer2/trackselection/AdaptiveVideoTrackSelection$Factory;-><init>(Lcom/google/android/exoplayer2/upstream/BandwidthMeter;)V

    .line 76
    .local v8, "videoTrackSelectionFactory":Lcom/google/android/exoplayer2/trackselection/TrackSelection$Factory;
    new-instance v7, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;

    invoke-direct {v7, v8}, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;-><init>(Lcom/google/android/exoplayer2/trackselection/TrackSelection$Factory;)V

    .line 79
    .local v7, "trackSelector":Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    new-instance v5, Lcom/google/android/exoplayer2/DefaultLoadControl;

    invoke-direct {v5}, Lcom/google/android/exoplayer2/DefaultLoadControl;-><init>()V

    invoke-static {v4, v7, v5}, Lcom/google/android/exoplayer2/ExoPlayerFactory;->newSimpleInstance(Landroid/content/Context;Lcom/google/android/exoplayer2/trackselection/TrackSelector;Lcom/google/android/exoplayer2/LoadControl;)Lcom/google/android/exoplayer2/SimpleExoPlayer;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    .line 80
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->playerView:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->setPlayer(Lcom/google/android/exoplayer2/SimpleExoPlayer;)V

    .line 81
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v4, v9}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->setPlayWhenReady(Z)V

    .line 82
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->addListener(Lcom/google/android/exoplayer2/ExoPlayer$EventListener;)V

    .line 84
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventLogger:Lcom/microsoft/xbox/toolkit/ExoVideoPlayerEventLogger;

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->addListener(Lcom/google/android/exoplayer2/ExoPlayer$EventListener;)V

    .line 85
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventLogger:Lcom/microsoft/xbox/toolkit/ExoVideoPlayerEventLogger;

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->setVideoDebugListener(Lcom/google/android/exoplayer2/video/VideoRendererEventListener;)V

    .line 86
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventLogger:Lcom/microsoft/xbox/toolkit/ExoVideoPlayerEventLogger;

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->setAudioDebugListener(Lcom/google/android/exoplayer2/audio/AudioRendererEventListener;)V

    .line 87
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventLogger:Lcom/microsoft/xbox/toolkit/ExoVideoPlayerEventLogger;

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->setMetadataOutput(Lcom/google/android/exoplayer2/metadata/MetadataRenderer$Output;)V

    .line 89
    new-instance v2, Lcom/google/android/exoplayer2/upstream/DefaultDataSourceFactory;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    const-string v11, "XboxApp"

    invoke-static {v5, v11}, Lcom/google/android/exoplayer2/util/Util;->getUserAgent(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/google/android/exoplayer2/upstream/DefaultDataSourceFactory;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 90
    .local v2, "dataSourceFactory":Lcom/google/android/exoplayer2/upstream/DataSource$Factory;
    new-instance v3, Lcom/google/android/exoplayer2/extractor/DefaultExtractorsFactory;

    invoke-direct {v3}, Lcom/google/android/exoplayer2/extractor/DefaultExtractorsFactory;-><init>()V

    .line 91
    .local v3, "extractorsFactory":Lcom/google/android/exoplayer2/extractor/ExtractorsFactory;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->getDataSource()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 92
    .local v1, "videoUri":Landroid/net/Uri;
    new-instance v0, Lcom/google/android/exoplayer2/source/ExtractorMediaSource;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventLogger:Lcom/microsoft/xbox/toolkit/ExoVideoPlayerEventLogger;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/ExtractorMediaSource;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/DataSource$Factory;Lcom/google/android/exoplayer2/extractor/ExtractorsFactory;Landroid/os/Handler;Lcom/google/android/exoplayer2/source/ExtractorMediaSource$EventListener;)V

    .line 94
    .local v0, "videoSource":Lcom/google/android/exoplayer2/source/MediaSource;
    iget v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->resumeWindow:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    move v6, v9

    .line 95
    .local v6, "haveResumePosition":Z
    :goto_0
    if-eqz v6, :cond_0

    .line 96
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    iget v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->resumeWindow:I

    iget-wide v12, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->resumePosition:J

    invoke-virtual {v4, v5, v12, v13}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->seekTo(IJ)V

    .line 98
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-nez v6, :cond_2

    move v4, v9

    :goto_1
    invoke-virtual {v5, v0, v4, v10}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->prepare(Lcom/google/android/exoplayer2/source/MediaSource;ZZ)V

    .line 99
    return-void

    .end local v6    # "haveResumePosition":Z
    :cond_1
    move v6, v10

    .line 94
    goto :goto_0

    .restart local v6    # "haveResumePosition":Z
    :cond_2
    move v4, v10

    .line 98
    goto :goto_1
.end method

.method private updateResumePosition()V
    .locals 4

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getCurrentWindowIndex()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->resumeWindow:I

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->isCurrentWindowSeekable()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getCurrentPosition()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->resumePosition:J

    .line 105
    return-void

    .line 103
    :cond_0
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_0
.end method


# virtual methods
.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v0, :cond_0

    .line 61
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->updateResumePosition()V

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->release()V

    .line 63
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    .line 64
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->bandwidthMeter:Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;

    .line 65
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventHandler:Landroid/os/Handler;

    .line 66
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->eventLogger:Lcom/microsoft/xbox/toolkit/ExoVideoPlayerEventLogger;

    .line 68
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onPause()V

    .line 69
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onResume()V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-nez v0, :cond_0

    .line 54
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->initializePlayer()V

    .line 56
    :cond_0
    return-void
.end method

.method protected updateViewOverride()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ExoVideoPlayerActivityAdapter;->updateLoadingIndicator(Z)V

    .line 48
    return-void
.end method
