.class public Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;
.super Ljava/lang/Object;
.source "OOBETelemetryService.java"


# instance fields
.field private repository:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->repository:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    .line 25
    return-void
.end method

.method private getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .locals 4

    .prologue
    .line 88
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 89
    .local v1, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->repository:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    invoke-interface {v3}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->getSessionCode()Ljava/lang/String;

    move-result-object v2

    .line 90
    .local v2, "sessionCode":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->repository:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    invoke-interface {v3}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;->getConsoleId()Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "consoleId":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 93
    const-string v3, "SetupCode"

    invoke-virtual {v1, v3, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 96
    :cond_0
    if-eqz v0, :cond_1

    .line 97
    const-string v3, "SetupConsoleId"

    invoke-virtual {v1, v3, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 100
    :cond_1
    return-object v1
.end method

.method static synthetic lambda$trackAppUpdateAction$10(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;Z)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;
    .param p1, "enabled"    # Z

    .prologue
    .line 68
    if-eqz p1, :cond_0

    const-string v0, "OOBE - Update Settings Keep Content Up To Date Enabled"

    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void

    :cond_0
    const-string v0, "OOBE - Update Settings Keep Content Up To Date Disabled"

    goto :goto_0
.end method

.method static synthetic lambda$trackBackButtonAction$6(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    .prologue
    .line 52
    const-string v0, "OOBE - Back"

    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void
.end method

.method static synthetic lambda$trackCloseButtonAction$7(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    .prologue
    .line 56
    const-string v0, "OOBE - Close"

    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void
.end method

.method static synthetic lambda$trackConsoleUpdateAction$9(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;Z)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;
    .param p1, "enabled"    # Z

    .prologue
    .line 64
    if-eqz p1, :cond_0

    const-string v0, "OOBE - Update Settings Keep Console Up To Date Enabled"

    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void

    :cond_0
    const-string v0, "OOBE - Update Settings Keep Console Up To Date Disabled"

    goto :goto_0
.end method

.method static synthetic lambda$trackDSTAction$11(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;Z)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;
    .param p1, "enabled"    # Z

    .prologue
    .line 72
    if-eqz p1, :cond_0

    const-string v0, "OOBE - Time Zone AutoDST Enabled"

    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void

    :cond_0
    const-string v0, "OOBE - Time Zone AutoDST Disabled"

    goto :goto_0
.end method

.method static synthetic lambda$trackInstantOnAction$8(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;Z)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;
    .param p1, "enabled"    # Z

    .prologue
    .line 60
    if-eqz p1, :cond_0

    const-string v0, "OOBE - Power Settings Instant On"

    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void

    :cond_0
    const-string v0, "OOBE - Power Settings Energy Saving"

    goto :goto_0
.end method

.method static synthetic lambda$trackInvalidSetupCode$13()V
    .locals 3

    .prologue
    .line 80
    const-string v0, "OOBE - Invalid setup code"

    const-string v1, "90015"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$trackNextButtonAction$5(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    .prologue
    .line 48
    const-string v0, "OOBE - Next"

    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void
.end method

.method static synthetic lambda$trackSaveSettingsFailure$14()V
    .locals 3

    .prologue
    .line 84
    const-string v0, "OOBE - Could not save settings"

    const-string v1, "90016"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$trackShowCompletionPage$4(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    .prologue
    .line 44
    const-string v0, "OOBE - Completion View"

    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void
.end method

.method static synthetic lambda$trackShowEnterCodePage$0(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    .prologue
    .line 28
    const-string v0, "OOBE - Welcome View"

    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void
.end method

.method static synthetic lambda$trackShowPowerSettingsPage$2(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    .prologue
    .line 36
    const-string v0, "OOBE - Power Settings View"

    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void
.end method

.method static synthetic lambda$trackShowTimeZonePage$1(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    .prologue
    .line 32
    const-string v0, "OOBE - Time Zone View"

    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void
.end method

.method static synthetic lambda$trackShowUpdateSettingsPage$3(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    .prologue
    .line 40
    const-string v0, "OOBE - Update Settings View"

    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void
.end method

.method static synthetic lambda$trackTimeZoneAction$12(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    .prologue
    .line 76
    const-string v0, "OOBE - Time Zone"

    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->getSessionInfo()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    return-void
.end method


# virtual methods
.method public trackAppUpdateAction(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 68
    invoke-static {p0, p1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;Z)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 69
    return-void
.end method

.method public trackBackButtonAction()V
    .locals 1

    .prologue
    .line 52
    invoke-static {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 53
    return-void
.end method

.method public trackCloseButtonAction()V
    .locals 1

    .prologue
    .line 56
    invoke-static {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 57
    return-void
.end method

.method public trackConsoleUpdateAction(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 64
    invoke-static {p0, p1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;Z)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 65
    return-void
.end method

.method public trackDSTAction(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;Z)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 73
    return-void
.end method

.method public trackInstantOnAction(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 60
    invoke-static {p0, p1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;Z)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 61
    return-void
.end method

.method public trackInvalidSetupCode()V
    .locals 1

    .prologue
    .line 80
    invoke-static {}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$14;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 81
    return-void
.end method

.method public trackNextButtonAction()V
    .locals 1

    .prologue
    .line 48
    invoke-static {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 49
    return-void
.end method

.method public trackSaveSettingsFailure()V
    .locals 1

    .prologue
    .line 84
    invoke-static {}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$15;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 85
    return-void
.end method

.method public trackShowCompletionPage()V
    .locals 1

    .prologue
    .line 44
    invoke-static {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 45
    return-void
.end method

.method public trackShowEnterCodePage()V
    .locals 1

    .prologue
    .line 28
    invoke-static {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 29
    return-void
.end method

.method public trackShowPowerSettingsPage()V
    .locals 1

    .prologue
    .line 36
    invoke-static {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 37
    return-void
.end method

.method public trackShowTimeZonePage()V
    .locals 1

    .prologue
    .line 32
    invoke-static {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 33
    return-void
.end method

.method public trackShowUpdateSettingsPage()V
    .locals 1

    .prologue
    .line 40
    invoke-static {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 41
    return-void
.end method

.method public trackTimeZoneAction()V
    .locals 1

    .prologue
    .line 76
    invoke-static {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 77
    return-void
.end method
