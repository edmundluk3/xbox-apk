.class public Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;
.super Ljava/lang/Object;
.source "SkypeEndpointLongPollBody.java"


# instance fields
.field public final channelType:Ljava/lang/String;

.field public interestedResources:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final template:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-string v0, "httpLongPoll"

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;->channelType:Ljava/lang/String;

    .line 10
    const-string v0, "raw"

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;->template:Ljava/lang/String;

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;->interestedResources:Ljava/util/ArrayList;

    .line 14
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;->interestedResources:Ljava/util/ArrayList;

    const-string v1, "/v1/users/ME/contacts/ALL"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 15
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;->interestedResources:Ljava/util/ArrayList;

    const-string v1, "/v1/users/ME/conversations/ALL/properties"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;->interestedResources:Ljava/util/ArrayList;

    const-string v1, "/v1/threads/ALL"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;->interestedResources:Ljava/util/ArrayList;

    const-string v1, "/v1/users/ME/conversations/ALL/messages"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 18
    return-void
.end method

.method public static getSkypeEndpointLongPollBody(Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;)Ljava/lang/String;
    .locals 4
    .param p0, "skypeEndpointLongPollBody"    # Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;

    .prologue
    .line 22
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 27
    :goto_0
    return-object v1

    .line 23
    :catch_0
    move-exception v0

    .line 24
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SkypeEndpointLongPollBody"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in serialization "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const-string v1, ""

    goto :goto_0
.end method
