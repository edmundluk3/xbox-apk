.class final Lcom/f2prateek/rx/preferences2/RealPreference;
.super Ljava/lang/Object;
.source "RealPreference.java"

# interfaces
.implements Lcom/f2prateek/rx/preferences2/Preference;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/f2prateek/rx/preferences2/Preference",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final adapter:Lcom/f2prateek/rx/preferences2/Preference$Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference$Adapter",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final defaultValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final key:Ljava/lang/String;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final values:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Adapter;Lio/reactivex/Observable;)V
    .locals 2
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/String;",
            "TT;",
            "Lcom/f2prateek/rx/preferences2/Preference$Adapter",
            "<TT;>;",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "this":Lcom/f2prateek/rx/preferences2/RealPreference;, "Lcom/f2prateek/rx/preferences2/RealPreference<TT;>;"
    .local p3, "defaultValue":Ljava/lang/Object;, "TT;"
    .local p4, "adapter":Lcom/f2prateek/rx/preferences2/Preference$Adapter;, "Lcom/f2prateek/rx/preferences2/Preference$Adapter<TT;>;"
    .local p5, "keyChanges":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->preferences:Landroid/content/SharedPreferences;

    .line 22
    iput-object p2, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->defaultValue:Ljava/lang/Object;

    .line 24
    iput-object p4, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->adapter:Lcom/f2prateek/rx/preferences2/Preference$Adapter;

    .line 25
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference$2;

    invoke-direct {v0, p0, p2}, Lcom/f2prateek/rx/preferences2/RealPreference$2;-><init>(Lcom/f2prateek/rx/preferences2/RealPreference;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p5, v0}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "<init>"

    .line 31
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/f2prateek/rx/preferences2/RealPreference$1;

    invoke-direct {v1, p0}, Lcom/f2prateek/rx/preferences2/RealPreference$1;-><init>(Lcom/f2prateek/rx/preferences2/RealPreference;)V

    .line 32
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->values:Lio/reactivex/Observable;

    .line 37
    return-void
.end method


# virtual methods
.method public asConsumer()Lio/reactivex/functions/Consumer;
    .locals 1
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/functions/Consumer",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lcom/f2prateek/rx/preferences2/RealPreference;, "Lcom/f2prateek/rx/preferences2/RealPreference<TT;>;"
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference$3;

    invoke-direct {v0, p0}, Lcom/f2prateek/rx/preferences2/RealPreference$3;-><init>(Lcom/f2prateek/rx/preferences2/RealPreference;)V

    return-object v0
.end method

.method public asObservable()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "this":Lcom/f2prateek/rx/preferences2/RealPreference;, "Lcom/f2prateek/rx/preferences2/RealPreference<TT;>;"
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->values:Lio/reactivex/Observable;

    return-object v0
.end method

.method public defaultValue()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "this":Lcom/f2prateek/rx/preferences2/RealPreference;, "Lcom/f2prateek/rx/preferences2/RealPreference<TT;>;"
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->defaultValue:Ljava/lang/Object;

    return-object v0
.end method

.method public delete()V
    .locals 1

    .prologue
    .line 69
    .local p0, "this":Lcom/f2prateek/rx/preferences2/RealPreference;, "Lcom/f2prateek/rx/preferences2/RealPreference<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/f2prateek/rx/preferences2/RealPreference;->set(Ljava/lang/Object;)V

    .line 70
    return-void
.end method

.method public get()Ljava/lang/Object;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lcom/f2prateek/rx/preferences2/RealPreference;, "Lcom/f2prateek/rx/preferences2/RealPreference<TT;>;"
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->defaultValue:Ljava/lang/Object;

    .line 51
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->adapter:Lcom/f2prateek/rx/preferences2/Preference$Adapter;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, v1, v2}, Lcom/f2prateek/rx/preferences2/Preference$Adapter;->get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public isSet()Z
    .locals 2

    .prologue
    .line 65
    .local p0, "this":Lcom/f2prateek/rx/preferences2/RealPreference;, "Lcom/f2prateek/rx/preferences2/RealPreference<TT;>;"
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public key()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 40
    .local p0, "this":Lcom/f2prateek/rx/preferences2/RealPreference;, "Lcom/f2prateek/rx/preferences2/RealPreference<TT;>;"
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    return-object v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lcom/f2prateek/rx/preferences2/RealPreference;, "Lcom/f2prateek/rx/preferences2/RealPreference<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 56
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-nez p1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 61
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 62
    return-void

    .line 59
    :cond_0
    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->adapter:Lcom/f2prateek/rx/preferences2/Preference$Adapter;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    invoke-interface {v1, v2, p1, v0}, Lcom/f2prateek/rx/preferences2/Preference$Adapter;->set(Ljava/lang/String;Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method
