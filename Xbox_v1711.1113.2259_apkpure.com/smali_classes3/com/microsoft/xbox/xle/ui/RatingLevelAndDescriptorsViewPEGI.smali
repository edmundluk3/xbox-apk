.class public Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;
.super Landroid/widget/LinearLayout;
.source "RatingLevelAndDescriptorsViewPEGI.java"


# instance fields
.field imageOnly:Z

.field private ratingDescriptorsContainer:Landroid/widget/GridLayout;

.field private ratingDisclaimersContainer:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->imageOnly:Z

    .line 31
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->init(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method private addRatingDescriptor(Ljava/lang/String;)V
    .locals 5
    .param p1, "imageUrl"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 89
    .local v1, "vi":Landroid/view/LayoutInflater;
    const v2, 0x7f0301a8

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDescriptorsContainer:Landroid/widget/GridLayout;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 90
    .local v0, "imageView":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    sget v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v3, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-virtual {v0, p1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 91
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDescriptorsContainer:Landroid/widget/GridLayout;

    invoke-virtual {v2, v0}, Landroid/widget/GridLayout;->addView(Landroid/view/View;)V

    .line 93
    .end local v0    # "imageView":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .end local v1    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    return-void
.end method

.method private addRatingDisclaimer(Ljava/lang/String;)V
    .locals 5
    .param p1, "disclaimerText"    # Ljava/lang/String;

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 97
    .local v1, "vi":Landroid/view/LayoutInflater;
    const v2, 0x7f0300fb

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 98
    .local v0, "textView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 100
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 36
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f0301e1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 37
    const v1, 0x7f0e09a0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    .line 38
    const v1, 0x7f0e099f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDescriptorsContainer:Landroid/widget/GridLayout;

    .line 39
    return-void
.end method


# virtual methods
.method public setRatingLevelAndDescriptors(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;Ljava/util/List;)V
    .locals 6
    .param p1, "rating"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "ratingdescriptors":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;>;"
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 49
    if-eqz p1, :cond_0

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    if-nez v2, :cond_2

    .line 50
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDescriptorsContainer:Landroid/widget/GridLayout;

    if-eqz v2, :cond_1

    .line 51
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDescriptorsContainer:Landroid/widget/GridLayout;

    invoke-virtual {v2, v3}, Landroid/widget/GridLayout;->setVisibility(I)V

    .line 84
    :cond_1
    return-void

    .line 54
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDescriptorsContainer:Landroid/widget/GridLayout;

    if-eqz v2, :cond_3

    .line 55
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDescriptorsContainer:Landroid/widget/GridLayout;

    invoke-virtual {v2}, Landroid/widget/GridLayout;->removeAllViews()V

    .line 57
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_4

    .line 58
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->imageOnly:Z

    if-eqz v2, :cond_5

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 59
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 61
    :cond_4
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->RatingImages:Ljava/util/List;

    if-eqz v2, :cond_8

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->RatingImages:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_8

    .line 63
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->RatingImages:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;

    .line 64
    .local v0, "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;->Url:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->addRatingDescriptor(Ljava/lang/String;)V

    goto :goto_1

    .end local v0    # "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;
    :cond_5
    move v2, v4

    .line 58
    goto :goto_0

    .line 66
    :cond_6
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDescriptorsContainer:Landroid/widget/GridLayout;

    invoke-virtual {v2, v4}, Landroid/widget/GridLayout;->setVisibility(I)V

    .line 72
    :goto_2
    if-eqz p2, :cond_1

    .line 73
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDescriptorsContainer:Landroid/widget/GridLayout;

    if-eqz v2, :cond_1

    .line 74
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;

    .line 75
    .local v1, "ratingDescriptor":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;
    iget-object v3, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->Image:Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;

    if-eqz v3, :cond_7

    .line 76
    iget-object v3, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->Image:Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;->Url:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->addRatingDescriptor(Ljava/lang/String;)V

    goto :goto_3

    .line 68
    .end local v1    # "ratingDescriptor":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;
    :cond_8
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDescriptorsContainer:Landroid/widget/GridLayout;

    invoke-virtual {v2, v3}, Landroid/widget/GridLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method public setShowImageOnly(Z)V
    .locals 2
    .param p1, "onlyImage"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->imageOnly:Z

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 44
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->imageOnly:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 46
    :cond_0
    return-void

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
