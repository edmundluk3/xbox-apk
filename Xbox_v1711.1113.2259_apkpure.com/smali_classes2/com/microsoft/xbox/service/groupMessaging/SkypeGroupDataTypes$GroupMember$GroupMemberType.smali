.class public final enum Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;
.super Ljava/lang/Enum;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GroupMemberType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

.field public static final enum Dummy:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

.field public static final enum Member:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 92
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    const-string v1, "Member"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;->Member:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    .line 93
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    const-string v1, "Dummy"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;->Dummy:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    .line 91
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;->Member:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;->Dummy:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;->$VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 91
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;->$VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    return-object v0
.end method
