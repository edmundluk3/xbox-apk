.class abstract Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_ContinuationUris;
.super Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;
.source "$AutoValue_TournamentNotificationDataTypes_ContinuationUris.java"


# instance fields
.field private final uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

.field private final uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;)V
    .locals 2
    .param p1, "uwpDesktop"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;
    .param p2, "uwpXboxOne"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;-><init>()V

    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null uwpDesktop"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_ContinuationUris;->uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    .line 21
    if-nez p2, :cond_1

    .line 22
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null uwpXboxOne"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_ContinuationUris;->uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 55
    check-cast v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    .line 56
    .local v0, "that":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_ContinuationUris;->uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;->uwpDesktop()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_ContinuationUris;->uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    .line 57
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;->uwpXboxOne()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;
    :cond_3
    move v1, v2

    .line 59
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 64
    const/4 v0, 0x1

    .line 65
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_ContinuationUris;->uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 67
    mul-int/2addr v0, v2

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_ContinuationUris;->uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 69
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ContinuationUris{uwpDesktop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_ContinuationUris;->uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uwpXboxOne="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_ContinuationUris;->uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public uwpDesktop()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "uwp_desktop"
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_ContinuationUris;->uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    return-object v0
.end method

.method public uwpXboxOne()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "uwp_xboxone"
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_ContinuationUris;->uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;

    return-object v0
.end method
