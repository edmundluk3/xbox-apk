.class Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "ImageViewerScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$ScaleListener;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;

    .prologue
    .line 274
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$ScaleListener;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 282
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    .line 283
    .local v0, "factor":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    .line 284
    .local v1, "focusX":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v2

    .line 285
    .local v2, "focusY":F
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$ScaleListener;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->handleScale(FFF)Z

    .line 286
    const/4 v3, 0x1

    return v3
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 277
    const/4 v0, 0x1

    return v0
.end method
