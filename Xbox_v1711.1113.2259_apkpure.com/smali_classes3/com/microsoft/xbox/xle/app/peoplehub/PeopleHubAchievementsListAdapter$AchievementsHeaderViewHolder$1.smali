.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder$1;
.super Ljava/lang/Object;
.source "PeopleHubAchievementsListAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;

.field final synthetic val$peopleHubAchievementsScreenViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

.field final synthetic val$this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder$1;->this$1:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder$1;->val$this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder$1;->val$peopleHubAchievementsScreenViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 227
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;->values()[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    move-result-object v1

    .line 228
    .local v1, "values":[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;
    if-ltz p3, :cond_1

    array-length v2, v1

    if-ge p3, v2, :cond_1

    .line 229
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;->values()[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    move-result-object v2

    aget-object v0, v2, p3

    .line 231
    .local v0, "filter":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder$1;->val$peopleHubAchievementsScreenViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getAchievementsFilter()Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 234
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder$1;->val$peopleHubAchievementsScreenViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->setAchievementsFilter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;)V

    .line 237
    :cond_0
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackAchievementsFilterAction(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;)V

    .line 243
    .end local v0    # "filter":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;
    :cond_1
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 247
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
