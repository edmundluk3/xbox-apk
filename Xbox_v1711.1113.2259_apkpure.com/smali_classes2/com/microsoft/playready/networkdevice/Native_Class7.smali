.class Lcom/microsoft/playready/networkdevice/Native_Class7;
.super Ljava/lang/Object;
.source "Native_Class7.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/networkdevice/Native_Class7$1;,
        Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;,
        Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass4;,
        Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;,
        Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;,
        Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass1;,
        Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;
    }
.end annotation


# instance fields
.field protected mField1:J

.field private final mField10:Ljava/lang/String;

.field private final mField11:Ljava/lang/Object;

.field private mField12:J

.field private mField13:I

.field protected final mField2:J

.field protected mField3:Ljava/lang/String;

.field protected mField4:Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass1;

.field protected mField5:Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;

.field protected mField6:Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass4;

.field protected mField7:Ljava/lang/String;

.field protected mField8:Ljava/lang/String;

.field private mField9:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

.field private mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 15
    invoke-static {}, Lcom/microsoft/playready/networkdevice/NativeLoadLibrary;->loadLibrary()V

    .line 16
    return-void
.end method

.method constructor <init>(JLjava/lang/String;)V
    .locals 5
    .param p1, "arg1"    # J
    .param p3, "arg2"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;

    .line 117
    iput-wide v2, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField1:J

    .line 123
    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField3:Ljava/lang/String;

    .line 126
    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField4:Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass1;

    .line 129
    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField5:Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;

    .line 132
    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField6:Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass4;

    .line 135
    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField7:Ljava/lang/String;

    .line 138
    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField8:Ljava/lang/String;

    .line 141
    sget-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->Unknown:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField9:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 147
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField11:Ljava/lang/Object;

    .line 150
    iput-wide v2, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    .line 153
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField13:I

    .line 158
    iput-wide p1, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField2:J

    .line 159
    iput-object p3, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField10:Ljava/lang/String;

    .line 162
    invoke-direct {p0}, Lcom/microsoft/playready/networkdevice/Native_Class7;->_method_1()V

    .line 163
    return-void
.end method

.method private native _method_1()V
.end method

.method private native _method_10(Ljava/lang/String;)J
.end method

.method private native _method_2()V
.end method

.method private native _method_3(J)V
.end method

.method private native _method_4(Ljava/lang/String;)J
.end method

.method private native _method_5(Ljava/lang/String;I)J
.end method

.method private native _method_6(Ljava/lang/String;)J
.end method

.method private native _method_7(Ljava/lang/String;Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;)J
.end method

.method private native _method_8(Ljava/lang/String;)J
.end method

.method private native _method_9(Ljava/lang/String;)J
.end method


# virtual methods
.method protected finalize()V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/microsoft/playready/networkdevice/Native_Class7;->_method_2()V

    .line 171
    return-void
.end method

.method public getResult(I)Ljava/lang/Object;
    .locals 8
    .param p1, "timeoutInMs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 180
    const/4 v0, 0x0

    .line 182
    .local v0, "result":Ljava/lang/Object;
    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField11:Ljava/lang/Object;

    monitor-enter v2

    .line 184
    :try_start_0
    iget-wide v4, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 186
    if-ltz p1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField11:Ljava/lang/Object;

    int-to-long v4, p1

    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 194
    iget-wide v4, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 197
    invoke-virtual {p0}, Lcom/microsoft/playready/networkdevice/Native_Class7;->method_3()V

    .line 198
    const/4 v0, 0x0

    monitor-exit v2

    .line 237
    .end local v0    # "result":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 204
    .restart local v0    # "result":Ljava/lang/Object;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField11:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 207
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    sget-object v1, Lcom/microsoft/playready/networkdevice/Native_Class7$1;->$SwitchMap$com$microsoft$playready$networkdevice$Native_Class7$Enum1:[I

    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField9:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    invoke-virtual {v2}, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 212
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField3:Ljava/lang/String;

    .line 213
    .local v0, "result":Ljava/lang/String;
    goto :goto_0

    .line 207
    .end local v0    # "result":Ljava/lang/String;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 215
    .local v0, "result":Ljava/lang/Object;
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField4:Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass1;

    .line 216
    .local v0, "result":Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass1;
    goto :goto_0

    .line 218
    .local v0, "result":Ljava/lang/Object;
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField5:Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;

    .line 219
    .local v0, "result":Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;
    goto :goto_0

    .line 221
    .local v0, "result":Ljava/lang/Object;
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField6:Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass4;

    .line 222
    .local v0, "result":Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass4;
    goto :goto_0

    .line 224
    .local v0, "result":Ljava/lang/Object;
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField7:Ljava/lang/String;

    .line 225
    .local v0, "result":Ljava/lang/String;
    goto :goto_0

    .line 227
    .local v0, "result":Ljava/lang/Object;
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField8:Ljava/lang/String;

    .line 228
    .local v0, "result":Ljava/lang/String;
    goto :goto_0

    .line 230
    .local v0, "result":Ljava/lang/Object;
    :pswitch_6
    iget v1, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField13:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 231
    .local v0, "result":Ljava/lang/Integer;
    goto :goto_0

    .line 209
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public declared-synchronized method_10()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 343
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 345
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Async operation in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 343
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 348
    :cond_0
    :try_start_1
    sget-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->SystemUpdateID:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField9:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 349
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField10:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/microsoft/playready/networkdevice/Native_Class7;->_method_10(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    .line 351
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized method_3()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 243
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 245
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    invoke-direct {p0, v0, v1}, Lcom/microsoft/playready/networkdevice/Native_Class7;->_method_3(J)V

    .line 248
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField11:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 250
    const-wide/16 v2, 0x0

    :try_start_1
    iput-wide v2, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    .line 251
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField11:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 252
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254
    :cond_0
    monitor-exit p0

    return-void

    .line 252
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 243
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized method_4()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 259
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 261
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Async operation in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 264
    :cond_0
    :try_start_1
    sget-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->ConnectionIDs:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField9:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 265
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField10:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/microsoft/playready/networkdevice/Native_Class7;->_method_4(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    .line 267
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized method_5(I)Z
    .locals 4
    .param p1, "arg1"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 273
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 275
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Async operation in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 278
    :cond_0
    :try_start_1
    sget-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->ConnectionInfo:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField9:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 279
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField10:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/playready/networkdevice/Native_Class7;->_method_5(Ljava/lang/String;I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    .line 281
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized method_6()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 287
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 289
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Async operation in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 292
    :cond_0
    :try_start_1
    sget-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->ProtocolInfo:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField9:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 293
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField10:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/microsoft/playready/networkdevice/Native_Class7;->_method_6(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    .line 295
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized method_7(Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;)Z
    .locals 4
    .param p1, "arg1"    # Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;

    .prologue
    const-wide/16 v2, 0x0

    .line 301
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 303
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Async operation in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 301
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 306
    :cond_0
    :try_start_1
    sget-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->Browse:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField9:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 307
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField10:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/playready/networkdevice/Native_Class7;->_method_7(Ljava/lang/String;Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    .line 309
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized method_8()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 315
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 317
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Async operation in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 320
    :cond_0
    :try_start_1
    sget-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->SearchCaps:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField9:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 321
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField10:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/microsoft/playready/networkdevice/Native_Class7;->_method_8(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    .line 323
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized method_9()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 329
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 331
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Async operation in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 334
    :cond_0
    :try_start_1
    sget-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->SortCaps:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField9:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 335
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField10:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/microsoft/playready/networkdevice/Native_Class7;->_method_9(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    .line 337
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method r_method_1(I)V
    .locals 4
    .param p1, "arg1"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 395
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField11:Ljava/lang/Object;

    monitor-enter v1

    .line 397
    const-wide/16 v2, 0x0

    :try_start_0
    iput-wide v2, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField12:J

    .line 398
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mField11:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 399
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;

    invoke-interface {v0, p1}, Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;->call1(I)V

    .line 405
    :cond_0
    return-void

    .line 399
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setHandler1(Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;)V
    .locals 0
    .param p1, "handler"    # Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/Native_Class7;->mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class7$IHandler_1;

    .line 176
    return-void
.end method
