.class public final enum Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;
.super Ljava/lang/Enum;
.source "ClubModerationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ClubReportContentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

.field public static final enum ActivityFeedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

.field public static final enum Chat:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

.field public static final enum Comment:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    const-string v1, "ActivityFeedItem"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->ActivityFeedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    const-string v1, "Comment"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->Comment:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    const-string v1, "Chat"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->Chat:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->ActivityFeedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->Comment:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->Chat:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    return-object v0
.end method
