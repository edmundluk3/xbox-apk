.class public Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;
.super Ljava/lang/Object;
.source "ServiceManagerFactory.java"


# static fields
.field private static final instance:Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;


# instance fields
.field private companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

.field private edsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

.field private experimentServiceManager:Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;

.field private slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->instance:Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->instance:Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    return-object v0
.end method


# virtual methods
.method public getActivitiesServiceManager()Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;
    .locals 1

    .prologue
    .line 189
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;-><init>()V

    return-object v0
.end method

.method public getActivityFeedService()Lcom/microsoft/xbox/service/activityFeed/IActivityFeedsService;
    .locals 1

    .prologue
    .line 345
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;

    .line 348
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    goto :goto_0
.end method

.method public getActivityHubService()Lcom/microsoft/xbox/service/activityHub/IActivityHubService;
    .locals 1

    .prologue
    .line 201
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    sget-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

    .line 204
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->INSTANCE:Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

    goto :goto_0
.end method

.method public getChatService()Lcom/microsoft/xbox/service/chat/IChatService;
    .locals 1

    .prologue
    .line 257
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/chat/ChatServiceStub;

    .line 260
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatService;->INSTANCE:Lcom/microsoft/xbox/service/chat/ChatService;

    goto :goto_0
.end method

.method public getClubAccountsService()Lcom/microsoft/xbox/service/clubs/IClubAccountsService;
    .locals 1

    .prologue
    .line 265
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubAccountsServiceStub;

    .line 268
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

    goto :goto_0
.end method

.method public getClubChatManagementService()Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;
    .locals 1

    .prologue
    .line 249
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;

    .line 252
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubChatManagementService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubChatManagementService;

    goto :goto_0
.end method

.method public getClubHubService()Lcom/microsoft/xbox/service/clubs/IClubHubService;
    .locals 1

    .prologue
    .line 209
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubHubServiceStub;

    .line 212
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubHubService;

    goto :goto_0
.end method

.method public getClubModerationService()Lcom/microsoft/xbox/service/clubs/IClubModerationService;
    .locals 1

    .prologue
    .line 233
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubModerationServiceStub;

    .line 236
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubModerationService;

    goto :goto_0
.end method

.method public getClubPresenceService()Lcom/microsoft/xbox/service/clubs/IClubPresenceService;
    .locals 1

    .prologue
    .line 225
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;

    .line 228
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubPresenceService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubPresenceService;

    goto :goto_0
.end method

.method public getClubProfileService()Lcom/microsoft/xbox/service/clubs/IClubProfileService;
    .locals 1

    .prologue
    .line 241
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubProfileServiceStub;

    .line 244
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubProfileService;

    goto :goto_0
.end method

.method public getClubRosterService()Lcom/microsoft/xbox/service/clubs/IClubRosterService;
    .locals 1

    .prologue
    .line 217
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubRosterServiceStub;

    .line 220
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubRosterService;

    goto :goto_0
.end method

.method public getClubSearchService()Lcom/microsoft/xbox/service/clubs/IClubSearchService;
    .locals 1

    .prologue
    .line 329
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubSearchServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubSearchServiceStub;

    .line 332
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubSearchService;

    goto :goto_0
.end method

.method public getCommentService()Lcom/microsoft/xbox/service/comments/ICommentsService;
    .locals 1

    .prologue
    .line 337
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    sget-object v0, Lcom/microsoft/xbox/service/comments/CommentsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/comments/CommentsServiceStub;

    .line 340
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/comments/CommentsService;->INSTANCE:Lcom/microsoft/xbox/service/comments/CommentsService;

    goto :goto_0
.end method

.method public getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    .line 185
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    move-result-object v0

    goto :goto_0
.end method

.method public getDLAssetsService()Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;
    .locals 2

    .prologue
    .line 361
    sget-object v0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    const-class v1, Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;

    return-object v0
.end method

.method public getEDSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->edsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->edsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    .line 148
    :goto_0
    return-object v0

    .line 142
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerStub;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerStub;-><init>()V

    goto :goto_0

    .line 145
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 146
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDS32ServiceManager;-><init>()V

    goto :goto_0

    .line 148
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;-><init>()V

    goto :goto_0
.end method

.method public getESServiceManager()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ESServiceManagerStub;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/ESServiceManagerStub;-><init>()V

    .line 196
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ESServiceManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/ESServiceManager;-><init>()V

    goto :goto_0
.end method

.method public getEnforcementService()Lcom/microsoft/xbox/service/enforcement/IEnforcementService;
    .locals 1

    .prologue
    .line 305
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    sget-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

    .line 308
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->INSTANCE:Lcom/microsoft/xbox/service/enforcement/EnforcementService;

    goto :goto_0
.end method

.method public getExperimentManager()Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->experimentServiceManager:Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->experimentServiceManager:Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;

    .line 176
    :goto_0
    return-object v0

    .line 175
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->experimentServiceManager:Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->experimentServiceManager:Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;

    goto :goto_0
.end method

.method public getLeaderboardsService()Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;
    .locals 1

    .prologue
    .line 353
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    sget-object v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsServiceStub;

    .line 356
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsService;->INSTANCE:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsService;

    goto :goto_0
.end method

.method public getMediaHubService()Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    .locals 2

    .prologue
    .line 365
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    sget-object v0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    const-class v1, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->getStub(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    .line 368
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    const-class v1, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->get(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    goto :goto_0
.end method

.method public getProgrammingServiceManager()Lcom/microsoft/xbox/service/network/managers/ISGFeaturedServiceManager;
    .locals 1

    .prologue
    .line 130
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/SGFeaturedServiceManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/SGFeaturedServiceManager;-><init>()V

    .line 133
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/SGFeaturedServiceManagerStub;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/SGFeaturedServiceManagerStub;-><init>()V

    goto :goto_0
.end method

.method public getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    .line 161
    :goto_0
    return-object v0

    .line 158
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManagerStub;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManagerStub;-><init>()V

    goto :goto_0

    .line 161
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;-><init>()V

    goto :goto_0
.end method

.method public getSkypeGroupMessageService()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;
    .locals 1

    .prologue
    .line 273
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupServiceStub;

    .line 276
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->INSTANCE:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

    goto :goto_0
.end method

.method public getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;
    .locals 1

    .prologue
    .line 289
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceStub;

    .line 292
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreService;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreService;

    goto :goto_0
.end method

.method public getSystemSettingsService()Lcom/microsoft/xbox/service/systemSettings/ISystemSettingsService;
    .locals 1

    .prologue
    .line 297
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    sget-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;

    .line 300
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;->INSTANCE:Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

    goto :goto_0
.end method

.method public getTitleHubService()Lcom/microsoft/xbox/service/titleHub/ITitleHubService;
    .locals 1

    .prologue
    .line 281
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    sget-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

    .line 284
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->INSTANCE:Lcom/microsoft/xbox/service/titleHub/TitleHubService;

    goto :goto_0
.end method

.method public getUserPostsService()Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;
    .locals 1

    .prologue
    .line 313
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/UserPostsServiceStub;

    .line 316
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

    goto :goto_0
.end method

.method public getUserSearchService()Lcom/microsoft/xbox/service/userSearch/IUserSearchService;
    .locals 1

    .prologue
    .line 321
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    sget-object v0, Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

    .line 324
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/userSearch/UserSearchService;->INSTANCE:Lcom/microsoft/xbox/service/userSearch/UserSearchService;

    goto :goto_0
.end method

.method public setCompanionSession(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;)V
    .locals 0
    .param p1, "companionSession"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    .line 123
    return-void
.end method

.method public setEDSServiceManager(Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;)V
    .locals 0
    .param p1, "serviceManager"    # Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->edsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    .line 119
    return-void
.end method

.method public setSLSServiceManager(Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;)V
    .locals 0
    .param p1, "serviceManager"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    .line 127
    return-void
.end method
