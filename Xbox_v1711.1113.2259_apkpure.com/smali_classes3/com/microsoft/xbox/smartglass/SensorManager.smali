.class public Lcom/microsoft/xbox/smartglass/SensorManager;
.super Ljava/lang/Object;
.source "SensorManager.java"


# instance fields
.field private _accelerometer:Lcom/microsoft/xbox/smartglass/Accelerometer;

.field private _gyrometer:Lcom/microsoft/xbox/smartglass/Gyrometer;

.field private final _lock:Ljava/lang/Object;

.field private final _manager:Landroid/hardware/SensorManager;

.field private final _pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method constructor <init>(JLandroid/content/Context;)V
    .locals 5
    .param p1, "pPlatform"    # J
    .param p3, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_lock:Ljava/lang/Object;

    .line 20
    const-string v0, "sensor"

    invoke-virtual {p3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_manager:Landroid/hardware/SensorManager;

    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_manager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/smartglass/Accelerometer;->isSupported:Z

    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_manager:Landroid/hardware/SensorManager;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_1
    sput-boolean v1, Lcom/microsoft/xbox/smartglass/Gyrometer;->isSupported:Z

    .line 26
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/SensorManager;->initialize(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 27
    return-void

    :cond_0
    move v0, v2

    .line 23
    goto :goto_0

    :cond_1
    move v1, v2

    .line 24
    goto :goto_1
.end method

.method private encodeTouchPointData(Lcom/microsoft/xbox/smartglass/TouchFrame;)[I
    .locals 6
    .param p1, "touchFrame"    # Lcom/microsoft/xbox/smartglass/TouchFrame;

    .prologue
    .line 125
    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x4

    new-array v0, v4, [I

    .line 126
    .local v0, "data":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 127
    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/smartglass/TouchPoint;

    .line 128
    .local v3, "touchPoint":Lcom/microsoft/xbox/smartglass/TouchPoint;
    mul-int/lit8 v2, v1, 0x4

    .line 129
    .local v2, "j":I
    iget-short v4, v3, Lcom/microsoft/xbox/smartglass/TouchPoint;->id:S

    aput v4, v0, v2

    .line 130
    add-int/lit8 v4, v2, 0x1

    iget-object v5, v3, Lcom/microsoft/xbox/smartglass/TouchPoint;->action:Lcom/microsoft/xbox/smartglass/TouchAction;

    invoke-virtual {v5}, Lcom/microsoft/xbox/smartglass/TouchAction;->getValue()I

    move-result v5

    aput v5, v0, v4

    .line 131
    add-int/lit8 v4, v2, 0x2

    iget v5, v3, Lcom/microsoft/xbox/smartglass/TouchPoint;->x:I

    aput v5, v0, v4

    .line 132
    add-int/lit8 v4, v2, 0x3

    iget v5, v3, Lcom/microsoft/xbox/smartglass/TouchPoint;->y:I

    aput v5, v0, v4

    .line 126
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 135
    .end local v2    # "j":I
    .end local v3    # "touchPoint":Lcom/microsoft/xbox/smartglass/TouchPoint;
    :cond_0
    return-object v0
.end method

.method private native initialize(J)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native sendAccelerometerReading(JJDDDII)V
.end method

.method private native sendGyrometerReading(JJDDDII)V
.end method

.method private native sendTouchFrame(JI[III)V
.end method


# virtual methods
.method public getAccelerometer()Lcom/microsoft/xbox/smartglass/Accelerometer;
    .locals 3

    .prologue
    .line 34
    invoke-static {}, Lcom/microsoft/xbox/smartglass/Accelerometer;->isSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 58
    :goto_0
    return-object v0

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_accelerometer:Lcom/microsoft/xbox/smartglass/Accelerometer;

    if-nez v0, :cond_2

    .line 39
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_lock:Ljava/lang/Object;

    monitor-enter v1

    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_accelerometer:Lcom/microsoft/xbox/smartglass/Accelerometer;

    if-nez v0, :cond_1

    .line 41
    new-instance v0, Lcom/microsoft/xbox/smartglass/Accelerometer;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_manager:Landroid/hardware/SensorManager;

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/smartglass/Accelerometer;-><init>(Landroid/hardware/SensorManager;)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_accelerometer:Lcom/microsoft/xbox/smartglass/Accelerometer;

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_accelerometer:Lcom/microsoft/xbox/smartglass/Accelerometer;

    new-instance v2, Lcom/microsoft/xbox/smartglass/SensorManager$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/smartglass/SensorManager$1;-><init>(Lcom/microsoft/xbox/smartglass/SensorManager;)V

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/smartglass/Accelerometer;->addListener(Ljava/lang/Object;)V

    .line 55
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_accelerometer:Lcom/microsoft/xbox/smartglass/Accelerometer;

    goto :goto_0

    .line 55
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getGyrometer()Lcom/microsoft/xbox/smartglass/Gyrometer;
    .locals 3

    .prologue
    .line 66
    invoke-static {}, Lcom/microsoft/xbox/smartglass/Gyrometer;->isSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    const/4 v0, 0x0

    .line 90
    :goto_0
    return-object v0

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_gyrometer:Lcom/microsoft/xbox/smartglass/Gyrometer;

    if-nez v0, :cond_2

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_lock:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_gyrometer:Lcom/microsoft/xbox/smartglass/Gyrometer;

    if-nez v0, :cond_1

    .line 73
    new-instance v0, Lcom/microsoft/xbox/smartglass/Gyrometer;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_manager:Landroid/hardware/SensorManager;

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/smartglass/Gyrometer;-><init>(Landroid/hardware/SensorManager;)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_gyrometer:Lcom/microsoft/xbox/smartglass/Gyrometer;

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_gyrometer:Lcom/microsoft/xbox/smartglass/Gyrometer;

    new-instance v2, Lcom/microsoft/xbox/smartglass/SensorManager$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/smartglass/SensorManager$2;-><init>(Lcom/microsoft/xbox/smartglass/SensorManager;)V

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/smartglass/Gyrometer;->addListener(Ljava/lang/Object;)V

    .line 87
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_gyrometer:Lcom/microsoft/xbox/smartglass/Gyrometer;

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public sendAccelerometerReading(Lcom/microsoft/xbox/smartglass/AccelerometerReading;Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    .locals 16
    .param p1, "reading"    # Lcom/microsoft/xbox/smartglass/AccelerometerReading;
    .param p2, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/SensorManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v4

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->timeStamp:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->x:D

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->y:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->z:D

    move-object/from16 v0, p2

    iget v14, v0, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/ServiceChannel;->getValue()I

    move-result v15

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v15}, Lcom/microsoft/xbox/smartglass/SensorManager;->sendAccelerometerReading(JJDDDII)V

    .line 111
    return-void
.end method

.method public sendGyrometerReading(Lcom/microsoft/xbox/smartglass/GyrometerReading;Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    .locals 16
    .param p1, "reading"    # Lcom/microsoft/xbox/smartglass/GyrometerReading;
    .param p2, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/SensorManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v4

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/microsoft/xbox/smartglass/GyrometerReading;->timeStamp:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/microsoft/xbox/smartglass/GyrometerReading;->x:D

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/microsoft/xbox/smartglass/GyrometerReading;->y:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/microsoft/xbox/smartglass/GyrometerReading;->z:D

    move-object/from16 v0, p2

    iget v14, v0, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/ServiceChannel;->getValue()I

    move-result v15

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v15}, Lcom/microsoft/xbox/smartglass/SensorManager;->sendGyrometerReading(JJDDDII)V

    .line 121
    return-void
.end method

.method public sendTouchFrame(Lcom/microsoft/xbox/smartglass/TouchFrame;Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    .locals 8
    .param p1, "frame"    # Lcom/microsoft/xbox/smartglass/TouchFrame;
    .param p2, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget v4, p1, Lcom/microsoft/xbox/smartglass/TouchFrame;->timeStamp:I

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/SensorManager;->encodeTouchPointData(Lcom/microsoft/xbox/smartglass/TouchFrame;)[I

    move-result-object v5

    iget v6, p2, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    iget-object v0, p2, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/ServiceChannel;->getValue()I

    move-result v7

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/smartglass/SensorManager;->sendTouchFrame(JI[III)V

    .line 101
    return-void
.end method
