.class public Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;
.super Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;
.source "FriendsWhoPlayDetailScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$FriendsWhoPlayDetailAdapterProvider;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;-><init>()V

    .line 18
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$FriendsWhoPlayDetailAdapterProvider;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$FriendsWhoPlayDetailAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$FriendsWhoPlayDetailAdapterProvider;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$FriendsWhoPlayDetailAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .line 24
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;->onCreate()V

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->setInGameDetailScreenFlag()V

    .line 31
    return-void
.end method
