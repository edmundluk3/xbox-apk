.class public abstract Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAddedEvent;
.super Ljava/lang/Object;
.source "ChatHeadManagerEventDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ChatHeadAddedEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAddedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAddedEvent<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/io/Serializable;)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAddedEvent;
    .locals 1
    .param p0    # Ljava/io/Serializable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(TT;)",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAddedEvent",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "key":Ljava/io/Serializable;, "TT;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 27
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadAddedEvent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadAddedEvent;-><init>(Ljava/io/Serializable;)V

    return-object v0
.end method


# virtual methods
.method public abstract key()Ljava/io/Serializable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method
