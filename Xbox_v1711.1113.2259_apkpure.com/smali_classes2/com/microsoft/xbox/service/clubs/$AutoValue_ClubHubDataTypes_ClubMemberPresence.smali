.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
.source "$AutoValue_ClubHubDataTypes_ClubMemberPresence.java"


# instance fields
.field private final lastSeenState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field private final lastSeenTimestamp:Ljava/lang/String;

.field private final xuid:J


# direct methods
.method constructor <init>(JLjava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V
    .locals 3
    .param p1, "xuid"    # J
    .param p3, "lastSeenTimestamp"    # Ljava/lang/String;
    .param p4, "lastSeenState"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;-><init>()V

    .line 18
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->xuid:J

    .line 19
    if-nez p3, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null lastSeenTimestamp"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->lastSeenTimestamp:Ljava/lang/String;

    .line 23
    if-nez p4, :cond_1

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null lastSeenState"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_1
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->lastSeenState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    if-ne p1, p0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 61
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    .line 62
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->xuid:J

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->lastSeenTimestamp:Ljava/lang/String;

    .line 63
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenTimestamp()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->lastSeenState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 64
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenState()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    :cond_3
    move v1, v2

    .line 66
    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const v8, 0xf4243

    .line 71
    const/4 v0, 0x1

    .line 72
    .local v0, "h":I
    mul-int/2addr v0, v8

    .line 73
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->xuid:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->xuid:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 74
    mul-int/2addr v0, v8

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->lastSeenTimestamp:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 76
    mul-int/2addr v0, v8

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->lastSeenState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 78
    return v0
.end method

.method public lastSeenState()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->lastSeenState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    return-object v0
.end method

.method public lastSeenTimestamp()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->lastSeenTimestamp:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubMemberPresence{xuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->xuid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastSeenTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->lastSeenTimestamp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastSeenState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->lastSeenState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public xuid()J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubMemberPresence;->xuid:J

    return-wide v0
.end method
