.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubPivotScreen;
.super Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.source "PeopleHubPivotScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    const-string v0, "Friends"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 12
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onCreate()V

    .line 14
    const v0, 0x7f0301c7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubPivotScreen;->setContentView(I)V

    .line 16
    const v0, 0x7f0e0932

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubPivotScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubPivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    .line 17
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubPivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 18
    return-void
.end method

.method public onCreateContentView()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onStart()V

    .line 27
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 31
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onStop()V

    .line 32
    return-void
.end method
