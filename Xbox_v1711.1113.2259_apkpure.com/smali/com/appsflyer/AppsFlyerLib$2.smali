.class Lcom/appsflyer/AppsFlyerLib$2;
.super Landroid/os/AsyncTask;
.source "AppsFlyerLib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/appsflyer/AppsFlyerLib;->registerOnGCM(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/appsflyer/GcmToken;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/appsflyer/AppsFlyerLib;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$gcmProjectNumber:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/appsflyer/AppsFlyerLib;

    .prologue
    .line 246
    iput-object p1, p0, Lcom/appsflyer/AppsFlyerLib$2;->this$0:Lcom/appsflyer/AppsFlyerLib;

    iput-object p2, p0, Lcom/appsflyer/AppsFlyerLib$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/appsflyer/AppsFlyerLib$2;->val$gcmProjectNumber:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/appsflyer/GcmToken;
    .locals 10
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v6, 0x0

    .line 250
    :try_start_0
    const-string v5, "com.google.android.gms.iid.InstanceID"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 251
    iget-object v5, p0, Lcom/appsflyer/AppsFlyerLib$2;->val$context:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/gms/iid/InstanceID;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/iid/InstanceID;

    move-result-object v1

    .line 252
    .local v1, "instanceID":Lcom/google/android/gms/iid/InstanceID;
    iget-object v5, p0, Lcom/appsflyer/AppsFlyerLib$2;->val$gcmProjectNumber:Ljava/lang/String;

    const-string v7, "GCM"

    const/4 v8, 0x0

    invoke-virtual {v1, v5, v7, v8}, Lcom/google/android/gms/iid/InstanceID;->getToken(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    .line 253
    .local v4, "token":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/google/android/gms/iid/InstanceID;->getId()Ljava/lang/String;

    move-result-object v2

    .line 254
    .local v2, "sInstanceId":Ljava/lang/String;
    new-instance v5, Lcom/appsflyer/GcmToken;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v5, v8, v9, v4, v2}, Lcom/appsflyer/GcmToken;-><init>(JLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 263
    .end local v1    # "instanceID":Lcom/google/android/gms/iid/InstanceID;
    .end local v2    # "sInstanceId":Ljava/lang/String;
    .end local v4    # "token":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 256
    :catch_0
    move-exception v0

    .line 257
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v5, "Please integrate Google Play Services in order to support uninstall feature"

    invoke-static {v5}, Lcom/appsflyer/AFLogger;->afLog(Ljava/lang/String;)V

    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :goto_1
    move-object v5, v6

    .line 263
    goto :goto_0

    .line 258
    :catch_1
    move-exception v0

    .line 259
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "Could not load registration ID"

    invoke-static {v5}, Lcom/appsflyer/AFLogger;->afLog(Ljava/lang/String;)V

    goto :goto_1

    .line 260
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 261
    .local v3, "t":Ljava/lang/Throwable;
    const-string v5, "Error registering for uninstall feature"

    invoke-static {v5}, Lcom/appsflyer/AFLogger;->afLog(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 246
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/appsflyer/AppsFlyerLib$2;->doInBackground([Ljava/lang/Void;)Lcom/appsflyer/GcmToken;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/appsflyer/GcmToken;)V
    .locals 6
    .param p1, "newGcmToken"    # Lcom/appsflyer/GcmToken;

    .prologue
    .line 268
    if-eqz p1, :cond_0

    .line 269
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v4

    const-string v5, "gcmToken"

    invoke-virtual {v4, v5}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 270
    .local v2, "token":Ljava/lang/String;
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v4

    const-string v5, "gcmInstanceId"

    invoke-virtual {v4, v5}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 271
    .local v1, "instanceId":Ljava/lang/String;
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v4

    const-string v5, "gcmTokenTimestamp"

    invoke-virtual {v4, v5}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 272
    .local v3, "tokenTimestamp":Ljava/lang/String;
    new-instance v0, Lcom/appsflyer/GcmToken;

    invoke-direct {v0, v3, v2, v1}, Lcom/appsflyer/GcmToken;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    .local v0, "existingGcmToken":Lcom/appsflyer/GcmToken;
    invoke-virtual {v0, p1}, Lcom/appsflyer/GcmToken;->update(Lcom/appsflyer/GcmToken;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 275
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "token="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/appsflyer/GcmToken;->getToken()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/appsflyer/AFLogger;->afLog(Ljava/lang/String;)V

    .line 276
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "instance id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/appsflyer/GcmToken;->getInstanceId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/appsflyer/AFLogger;->afLog(Ljava/lang/String;)V

    .line 277
    iget-object v4, p0, Lcom/appsflyer/AppsFlyerLib$2;->this$0:Lcom/appsflyer/AppsFlyerLib;

    iget-object v5, p0, Lcom/appsflyer/AppsFlyerLib$2;->val$context:Landroid/content/Context;

    invoke-virtual {v4, v0, v5}, Lcom/appsflyer/AppsFlyerLib;->updateServerGcmToken(Lcom/appsflyer/GcmToken;Landroid/content/Context;)V

    .line 280
    .end local v0    # "existingGcmToken":Lcom/appsflyer/GcmToken;
    .end local v1    # "instanceId":Ljava/lang/String;
    .end local v2    # "token":Ljava/lang/String;
    .end local v3    # "tokenTimestamp":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 246
    check-cast p1, Lcom/appsflyer/GcmToken;

    invoke-virtual {p0, p1}, Lcom/appsflyer/AppsFlyerLib$2;->onPostExecute(Lcom/appsflyer/GcmToken;)V

    return-void
.end method
