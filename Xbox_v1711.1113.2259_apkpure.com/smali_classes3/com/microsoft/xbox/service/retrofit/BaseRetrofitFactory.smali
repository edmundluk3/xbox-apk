.class public abstract Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory;
.super Ljava/lang/Object;
.source "BaseRetrofitFactory.java"

# interfaces
.implements Lcom/microsoft/xbox/service/retrofit/RetrofitFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/service/retrofit/RetrofitFactory",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    .local p0, "this":Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory;, "Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected getDefaultRetrofitBuilder(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;
    .locals 2
    .param p1, "baseUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 23
    .local p0, "this":Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory;, "Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory<TT;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 25
    new-instance v0, Lretrofit2/Retrofit$Builder;

    invoke-direct {v0}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 26
    invoke-virtual {v0, p1}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory;->getGsonConvertorFactory()Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 25
    return-object v0
.end method

.method protected getGsonConvertorFactory()Lretrofit2/converter/gson/GsonConverterFactory;
    .locals 5

    .prologue
    .line 31
    .local p0, "this":Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory;, "Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory<TT;>;"
    new-instance v1, Lcom/google/gson/GsonBuilder;

    invoke-direct {v1}, Lcom/google/gson/GsonBuilder;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0x80

    aput v4, v2, v3

    .line 32
    invoke-virtual {v1, v2}, Lcom/google/gson/GsonBuilder;->excludeFieldsWithModifiers([I)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    .line 33
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/AutoValueGsonAdapterFactory;->create()Lcom/google/gson/TypeAdapterFactory;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson;-><init>()V

    .line 34
    invoke-virtual {v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lcom/google/common/collect/ImmutableList;

    new-instance v3, Lcom/microsoft/xbox/toolkit/gson/ImmutableListDeserializer;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/gson/ImmutableListDeserializer;-><init>()V

    .line 35
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    .line 36
    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 38
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-static {v0}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/Gson;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v1

    return-object v1
.end method
