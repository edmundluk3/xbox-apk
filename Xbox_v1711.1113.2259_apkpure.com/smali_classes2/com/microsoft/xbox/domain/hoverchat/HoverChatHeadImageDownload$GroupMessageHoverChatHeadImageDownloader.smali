.class Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;
.super Ljava/lang/Object;
.source "HoverChatHeadImageDownload.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GroupMessageHoverChatHeadImageDownloader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# instance fields
.field private imageDownloadSubject:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

.field private final model:Lcom/microsoft/xbox/service/model/MessageModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)V
    .locals 1
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    .line 156
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->model:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 157
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
    .param p2, "x1"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$1;

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;-><init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)V

    return-void
.end method

.method private getUrlsFromGroup(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;)",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    .local p1, "groupMembers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 175
    .local v1, "urls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 176
    .local v0, "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    iget-object v3, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->gamerPicUrl:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 179
    .end local v0    # "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    :cond_0
    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    return-object v2
.end method

.method private onMessageDataUpdate(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 197
    sget-object v1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 201
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->model:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->conversationId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationMembers(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    .line 203
    .local v0, "groupMembers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 204
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->model:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 205
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->imageDownloadSubject:Lio/reactivex/subjects/PublishSubject;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->getUrlsFromGroup(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 206
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->imageDownloadSubject:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v1}, Lio/reactivex/subjects/PublishSubject;->onComplete()V

    goto :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getImagesUrls()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 161
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->model:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->conversationId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationMembers(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    .line 163
    .local v0, "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->imageDownloadSubject:Lio/reactivex/subjects/PublishSubject;

    .line 165
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->model:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 166
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->imageDownloadSubject:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v1}, Lio/reactivex/subjects/PublishSubject;->singleOrError()Lio/reactivex/Single;

    move-result-object v1

    .line 168
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->getUrlsFromGroup(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 184
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    move-object v0, v2

    .line 185
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 188
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 194
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    :goto_1
    return-void

    .line 184
    .end local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 190
    .restart local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    .restart local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;->onMessageDataUpdate(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_1

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
