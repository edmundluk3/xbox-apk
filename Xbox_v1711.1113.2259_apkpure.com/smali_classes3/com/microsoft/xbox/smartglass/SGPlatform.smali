.class public Lcom/microsoft/xbox/smartglass/SGPlatform;
.super Ljava/lang/Object;
.source "SGPlatform.java"


# static fields
.field private static s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;


# instance fields
.field private _discoveryManager:Lcom/microsoft/xbox/smartglass/DiscoveryManager;

.field private _environmentManager:Lcom/microsoft/xbox/smartglass/EnvironmentManager;

.field private _gameDvr:Lcom/microsoft/xbox/smartglass/GameDvr;

.field private _metricsManager:Lcom/microsoft/xbox/smartglass/MetricsManager;

.field private _pPlatform:J

.field private _sensorManager:Lcom/microsoft/xbox/smartglass/SensorManager;

.field private _serviceManager:Lcom/microsoft/xbox/smartglass/ServiceManager;

.field private _sessionManager:Lcom/microsoft/xbox/smartglass/SessionManager;

.field private _textManager:Lcom/microsoft/xbox/smartglass/TextManager;

.field private _tokenManager:Lcom/microsoft/xbox/smartglass/TokenManager;

.field private _traceLog:Lcom/microsoft/xbox/smartglass/TraceLog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "SmartGlassCore"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 27
    const-string v0, "SmartGlassSDK"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    .line 145
    return-void
.end method

.method private native createSGPlatform(Landroid/content/Context;)J
.end method

.method private native destroySGPlatform(J)V
.end method

.method static getCurrent()Lcom/microsoft/xbox/smartglass/SGPlatform;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    return-object v0
.end method

.method public static getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_discoveryManager:Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    return-object v0
.end method

.method public static getEnvironmentManager()Lcom/microsoft/xbox/smartglass/EnvironmentManager;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_environmentManager:Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    return-object v0
.end method

.method public static getGameDvr()Lcom/microsoft/xbox/smartglass/GameDvr;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_gameDvr:Lcom/microsoft/xbox/smartglass/GameDvr;

    return-object v0
.end method

.method public static getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_metricsManager:Lcom/microsoft/xbox/smartglass/MetricsManager;

    return-object v0
.end method

.method public static getSensorManager()Lcom/microsoft/xbox/smartglass/SensorManager;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_sensorManager:Lcom/microsoft/xbox/smartglass/SensorManager;

    return-object v0
.end method

.method public static getServiceManager()Lcom/microsoft/xbox/smartglass/ServiceManager;
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_serviceManager:Lcom/microsoft/xbox/smartglass/ServiceManager;

    return-object v0
.end method

.method public static getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_sessionManager:Lcom/microsoft/xbox/smartglass/SessionManager;

    return-object v0
.end method

.method public static getTextManager()Lcom/microsoft/xbox/smartglass/TextManager;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_textManager:Lcom/microsoft/xbox/smartglass/TextManager;

    return-object v0
.end method

.method public static getTokenManager()Lcom/microsoft/xbox/smartglass/TokenManager;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_tokenManager:Lcom/microsoft/xbox/smartglass/TokenManager;

    return-object v0
.end method

.method public static getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_traceLog:Lcom/microsoft/xbox/smartglass/TraceLog;

    return-object v0
.end method

.method public static initialize(Lcom/microsoft/xbox/smartglass/ClientInformation;Landroid/content/Context;)V
    .locals 1
    .param p0, "clientInfo"    # Lcom/microsoft/xbox/smartglass/ClientInformation;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 37
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/microsoft/xbox/smartglass/SGPlatform;

    invoke-direct {v0}, Lcom/microsoft/xbox/smartglass/SGPlatform;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    .line 39
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/smartglass/SGPlatform;->initializeInternal(Lcom/microsoft/xbox/smartglass/ClientInformation;Landroid/content/Context;)V

    .line 41
    :cond_0
    return-void
.end method

.method private initializeInternal(Lcom/microsoft/xbox/smartglass/ClientInformation;Landroid/content/Context;)V
    .locals 4
    .param p1, "clientInfo"    # Lcom/microsoft/xbox/smartglass/ClientInformation;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 155
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/smartglass/SGPlatform;->createSGPlatform(Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    .line 157
    iget-wide v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/ClientInformation;->configuration:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/smartglass/ClientInformation;->appKey:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/smartglass/SGPlatform;->loadConfiguration(JLjava/lang/String;Ljava/lang/String;)V

    .line 159
    new-instance v0, Lcom/microsoft/xbox/smartglass/TraceLog;

    invoke-direct {v0}, Lcom/microsoft/xbox/smartglass/TraceLog;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_traceLog:Lcom/microsoft/xbox/smartglass/TraceLog;

    .line 160
    new-instance v0, Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    iget-wide v2, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_environmentManager:Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    .line 161
    new-instance v0, Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    iget-wide v2, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_discoveryManager:Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    .line 162
    new-instance v0, Lcom/microsoft/xbox/smartglass/TokenManager;

    iget-wide v2, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    sget-object v1, Lcom/microsoft/xbox/smartglass/TokenType;->Xsts3:Lcom/microsoft/xbox/smartglass/TokenType;

    invoke-direct {v0, v2, v3, v1}, Lcom/microsoft/xbox/smartglass/TokenManager;-><init>(JLcom/microsoft/xbox/smartglass/TokenType;)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_tokenManager:Lcom/microsoft/xbox/smartglass/TokenManager;

    .line 163
    new-instance v0, Lcom/microsoft/xbox/smartglass/SensorManager;

    iget-wide v2, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    invoke-direct {v0, v2, v3, p2}, Lcom/microsoft/xbox/smartglass/SensorManager;-><init>(JLandroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_sensorManager:Lcom/microsoft/xbox/smartglass/SensorManager;

    .line 164
    new-instance v0, Lcom/microsoft/xbox/smartglass/SessionManager;

    iget-wide v2, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    invoke-direct {v0, v2, v3, p1}, Lcom/microsoft/xbox/smartglass/SessionManager;-><init>(JLcom/microsoft/xbox/smartglass/ClientInformation;)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_sessionManager:Lcom/microsoft/xbox/smartglass/SessionManager;

    .line 165
    new-instance v0, Lcom/microsoft/xbox/smartglass/TextManager;

    iget-wide v2, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/TextManager;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_textManager:Lcom/microsoft/xbox/smartglass/TextManager;

    .line 166
    new-instance v0, Lcom/microsoft/xbox/smartglass/MetricsManager;

    iget-wide v2, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/MetricsManager;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_metricsManager:Lcom/microsoft/xbox/smartglass/MetricsManager;

    .line 167
    new-instance v0, Lcom/microsoft/xbox/smartglass/ServiceManager;

    iget-wide v2, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/ServiceManager;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_serviceManager:Lcom/microsoft/xbox/smartglass/ServiceManager;

    .line 168
    new-instance v0, Lcom/microsoft/xbox/smartglass/GameDvr;

    iget-wide v2, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/GameDvr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_gameDvr:Lcom/microsoft/xbox/smartglass/GameDvr;

    .line 169
    return-void
.end method

.method private native loadConfiguration(JLjava/lang/String;Ljava/lang/String;)V
.end method

.method public static uninitialize()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/smartglass/SGPlatform;->s_instance:Lcom/microsoft/xbox/smartglass/SGPlatform;

    .line 52
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 53
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 148
    iget-wide v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/SGPlatform;->destroySGPlatform(J)V

    .line 149
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    .line 151
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 152
    return-void
.end method

.method getPlatform()J
    .locals 2

    .prologue
    .line 140
    iget-wide v0, p0, Lcom/microsoft/xbox/smartglass/SGPlatform;->_pPlatform:J

    return-wide v0
.end method
