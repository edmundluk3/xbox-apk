.class Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$7;
.super Ljava/lang/Object;
.source "ChangeGamertagDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$7;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$7;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamertagReserved()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$7;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->changeGamertag()V

    .line 179
    :cond_0
    return-void
.end method
