.class final Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
.source "$AutoValue_ClubHubDataTypes_ClubProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private associatedTitles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private backgroundImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private description:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private displayImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private isRecommendable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private isSearchable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private leaveEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private matureContentEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private name:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private preferredLocale:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private primaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private requestToJoinEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private secondaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private tags:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private tertiaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private transferOwnershipEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private watchClubTitlesOnly:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;-><init>()V

    .line 282
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    .prologue
    .line 283
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;-><init>()V

    .line 284
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->associatedTitles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->associatedTitles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 285
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->backgroundImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->backgroundImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 286
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->primaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->primaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 287
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->secondaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->secondaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 288
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tertiaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->tertiaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 289
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->description()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->description:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 290
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->displayImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 291
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->name:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 292
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->preferredLocale()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->preferredLocale:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 293
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tags()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->tags:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 294
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isRecommendable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->isRecommendable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 295
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isSearchable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->isSearchable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 296
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->leaveEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->leaveEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 297
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->matureContentEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->matureContentEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 298
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->requestToJoinEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->requestToJoinEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 299
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->transferOwnershipEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->transferOwnershipEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 300
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->watchClubTitlesOnly()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->watchClubTitlesOnly:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 301
    return-void
.end method


# virtual methods
.method public associatedTitles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 304
    .local p1, "associatedTitles":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->associatedTitles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 305
    return-object p0
.end method

.method public backgroundImageUrl(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 309
    .local p1, "backgroundImageUrl":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->backgroundImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 310
    return-object p0
.end method

.method public build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
    .locals 19

    .prologue
    .line 389
    new-instance v1, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->associatedTitles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->backgroundImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->primaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->secondaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->tertiaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->description:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->displayImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->name:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->preferredLocale:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->tags:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->isRecommendable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->isSearchable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->leaveEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->matureContentEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->requestToJoinEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->transferOwnershipEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->watchClubTitlesOnly:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v18, v0

    invoke-direct/range {v1 .. v18}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)V

    return-object v1
.end method

.method public description(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 329
    .local p1, "description":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->description:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 330
    return-object p0
.end method

.method public displayImageUrl(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 334
    .local p1, "displayImageUrl":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->displayImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 335
    return-object p0
.end method

.method public isRecommendable(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 354
    .local p1, "isRecommendable":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->isRecommendable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 355
    return-object p0
.end method

.method public isSearchable(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 359
    .local p1, "isSearchable":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->isSearchable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 360
    return-object p0
.end method

.method public leaveEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 364
    .local p1, "leaveEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->leaveEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 365
    return-object p0
.end method

.method public matureContentEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 369
    .local p1, "matureContentEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->matureContentEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 370
    return-object p0
.end method

.method public name(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 339
    .local p1, "name":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->name:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 340
    return-object p0
.end method

.method public preferredLocale(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 344
    .local p1, "preferredLocale":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->preferredLocale:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 345
    return-object p0
.end method

.method public primaryColor(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 314
    .local p1, "primaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->primaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 315
    return-object p0
.end method

.method public requestToJoinEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 374
    .local p1, "requestToJoinEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->requestToJoinEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 375
    return-object p0
.end method

.method public secondaryColor(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 319
    .local p1, "secondaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->secondaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 320
    return-object p0
.end method

.method public tags(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 349
    .local p1, "tags":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->tags:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 350
    return-object p0
.end method

.method public tertiaryColor(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 324
    .local p1, "tertiaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->tertiaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 325
    return-object p0
.end method

.method public transferOwnershipEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 379
    .local p1, "transferOwnershipEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->transferOwnershipEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 380
    return-object p0
.end method

.method public watchClubTitlesOnly(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;"
        }
    .end annotation

    .prologue
    .line 384
    .local p1, "watchClubTitlesOnly":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;->watchClubTitlesOnly:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 385
    return-object p0
.end method
