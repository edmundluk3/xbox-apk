.class public final enum Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;
.super Ljava/lang/Enum;
.source "TutorialTelemetryService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TutorialCard"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

.field public static final enum clubs:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

.field public static final enum facebook:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

.field public static final enum redeem:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

.field public static final enum setupConsole:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

.field public static final enum shop:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

.field public static final enum suggestedFriends:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;


# instance fields
.field private telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 17
    new-instance v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    const-string v1, "setupConsole"

    const-string v2, "Tutorial - Set up a console"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->setupConsole:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    const-string v1, "redeem"

    const-string v2, "Tutorial - Redeem a code"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->redeem:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    const-string v1, "shop"

    const-string v2, "Tutorial - Shop for games"

    invoke-direct {v0, v1, v6, v2}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->shop:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    const-string v1, "clubs"

    const-string v2, "Tutorial - Join a club"

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->clubs:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    const-string v1, "facebook"

    const-string v2, "Tutorial - Find Facebook friends"

    invoke-direct {v0, v1, v8, v2}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->facebook:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    const-string v1, "suggestedFriends"

    const/4 v2, 0x5

    const-string v3, "Tutorial - Friend Suggestions"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->suggestedFriends:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    .line 16
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    sget-object v1, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->setupConsole:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->redeem:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->shop:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->clubs:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->facebook:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->suggestedFriends:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->$VALUES:[Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->telemetryName:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->telemetryName:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->$VALUES:[Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    return-object v0
.end method


# virtual methods
.method public getTelemetryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
