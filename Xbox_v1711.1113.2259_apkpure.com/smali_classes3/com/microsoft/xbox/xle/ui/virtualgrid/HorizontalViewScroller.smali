.class public Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
.super Landroid/view/ViewGroup;
.source "HorizontalViewScroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;,
        Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;,
        Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IViewPortAwareness;,
        Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;,
        Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static compatibilityInitialized:Z

.field private static mPostOnAnimation:Ljava/lang/reflect/Method;


# instance fields
.field private mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

.field private mAutoScrollRunnable:Ljava/lang/Runnable;

.field private mAutoScrollVelocity:I

.field private mContentOffset:I

.field private mContentWidth:I

.field private mLastMotionEventX:F

.field private mListener:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field public mRequestDisallowInterceptTouchEventonTouchDown:Z

.field private mScrollRunnable:Ljava/lang/Runnable;

.field private mScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

.field private mScroller:Landroid/widget/Scroller;

.field private mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "HorizontalViewScroller"

    sput-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->TAG:Ljava/lang/String;

    .line 728
    const/4 v0, 0x0

    sput-boolean v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->compatibilityInitialized:Z

    .line 729
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mPostOnAnimation:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 107
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    .line 99
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mRequestDisallowInterceptTouchEventonTouchDown:Z

    .line 434
    new-instance v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$1;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScrollRunnable:Ljava/lang/Runnable;

    .line 826
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAutoScrollVelocity:I

    .line 828
    new-instance v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAutoScrollRunnable:Ljava/lang/Runnable;

    .line 109
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setClickable(Z)V

    .line 111
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 112
    .local v0, "cfg":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mTouchSlop:I

    .line 113
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mMinimumVelocity:I

    .line 114
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mMaximumVelocity:I

    .line 116
    new-instance v1, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScroller:Landroid/widget/Scroller;

    .line 119
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)Landroid/widget/Scroller;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScroller:Landroid/widget/Scroller;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->postScrollRunnable()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->hasScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    .prologue
    .line 39
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAutoScrollVelocity:I

    return v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
    .param p1, "x1"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAutoScrollVelocity:I

    return p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAutoScrollRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Landroid/view/View;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 39
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method private addVelocityTrackerMovement(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 541
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 542
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 545
    return-void
.end method

.method private clear(Z)V
    .locals 6
    .param p1, "removeAll"    # Z

    .prologue
    .line 696
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v0

    .line 698
    .local v0, "count":I
    if-eqz p1, :cond_2

    .line 700
    const/4 v1, 0x0

    .local v1, "idx":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 701
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;->reclaimView(Landroid/view/View;)V

    .line 700
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 704
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->removeAllViews()V

    .line 718
    :cond_1
    return-void

    .line 706
    .end local v1    # "idx":I
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 708
    .local v2, "removeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v1, 0x0

    .restart local v1    # "idx":I
    :goto_1
    if-ge v1, v0, :cond_4

    .line 709
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 710
    .local v3, "view":Landroid/view/View;
    invoke-static {v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->isChildViewTemporary(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 711
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 708
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 714
    .end local v3    # "view":Landroid/view/View;
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 715
    .restart local v3    # "view":Landroid/view/View;
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->removeView(Landroid/view/View;)V

    goto :goto_2
.end method

.method private getViewPortWidth()I
    .locals 2

    .prologue
    .line 526
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private hasScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z
    .locals 1
    .param p1, "state"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    .prologue
    .line 522
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getScrollState()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static initializeCompatibility()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 737
    sget-boolean v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->compatibilityInitialized:Z

    if-eqz v0, :cond_0

    .line 748
    :goto_0
    return-void

    .line 742
    :cond_0
    :try_start_0
    const-class v0, Landroid/view/View;

    const-string v1, "postOnAnimation"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/Runnable;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mPostOnAnimation:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 747
    :goto_1
    sput-boolean v5, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->compatibilityInitialized:Z

    goto :goto_0

    .line 743
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private insertChild(Landroid/view/View;Z)V
    .locals 7
    .param p1, "child"    # Landroid/view/View;
    .param p2, "append"    # Z

    .prologue
    const/4 v2, 0x0

    .line 644
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v6

    .line 645
    .local v6, "index":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v6, v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 647
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getHeight()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 648
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->layoutChild(Landroid/view/View;)V

    .line 650
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 652
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 654
    :cond_0
    return-void

    .end local v6    # "index":I
    :cond_1
    move v6, v2

    .line 644
    goto :goto_0
.end method

.method public static isChildViewTemporary(Landroid/view/View;)Z
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 819
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    .line 820
    .local v0, "mlp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    iget-boolean v1, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->temporary:Z

    return v1
.end method

.method private layoutChild(Landroid/view/View;)V
    .locals 8
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 658
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_0

    .line 668
    :goto_0
    return-void

    .line 661
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    .line 663
    .local v1, "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingLeft()I

    move-result v5

    iget v6, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->start:I

    iget v7, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->leftMargin:I

    add-int/2addr v6, v7

    add-int/2addr v5, v6

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentOffset()I

    move-result v6

    sub-int v3, v5, v6

    .line 664
    .local v3, "x":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingTop()I

    move-result v5

    iget v6, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->topMargin:I

    add-int v4, v5, v6

    .line 665
    .local v4, "y":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 666
    .local v2, "w":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 667
    .local v0, "h":I
    add-int v5, v3, v2

    add-int v6, v4, v0

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method private maxContentOffset()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 531
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentWidth()I

    move-result v0

    .line 532
    .local v0, "width":I
    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentWidth()I

    move-result v2

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getViewPortWidth()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_0
    return v1
.end method

.method private offsetChild(Landroid/view/View;I)Z
    .locals 1
    .param p1, "child"    # Landroid/view/View;
    .param p2, "offset"    # I

    .prologue
    .line 685
    invoke-virtual {p1, p2}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 687
    const/4 v0, 0x1

    return v0
.end method

.method private populateChildren()V
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 549
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

    if-nez v8, :cond_1

    .line 640
    :cond_0
    :goto_0
    return-void

    .line 552
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentOffset()I

    move-result v5

    .line 553
    .local v5, "rangeMin":I
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getViewPortWidth()I

    move-result v8

    add-int v4, v5, v8

    .line 556
    .local v4, "rangeMax":I
    const/4 v1, 0x0

    .line 557
    .local v1, "invalidate":Z
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v8

    if-lez v8, :cond_2

    .line 559
    invoke-virtual {p0, v10}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 560
    .local v7, "view":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    .line 561
    .local v3, "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->end()I

    move-result v8

    if-le v8, v5, :cond_8

    .line 568
    .end local v3    # "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    .end local v7    # "view":Landroid/view/View;
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v8

    if-lez v8, :cond_3

    .line 570
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 571
    .restart local v7    # "view":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    .line 572
    .restart local v3    # "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    iget v8, v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->start:I

    if-ge v8, v4, :cond_9

    .line 582
    .end local v3    # "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    .end local v7    # "view":Landroid/view/View;
    :cond_3
    move v0, v5

    .line 583
    .local v0, "end":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v8

    if-lez v8, :cond_4

    .line 584
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->end()I

    move-result v0

    .line 587
    :cond_4
    if-ge v0, v4, :cond_5

    .line 588
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

    invoke-interface {v8, v0, v9}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;->getViewIterator(IZ)Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;

    move-result-object v2

    .line 589
    .local v2, "iterator":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;
    if-eqz v2, :cond_5

    .line 590
    :goto_3
    if-ge v0, v4, :cond_5

    .line 592
    invoke-interface {v2, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;->getNext(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 593
    .restart local v7    # "view":Landroid/view/View;
    if-nez v7, :cond_a

    .line 610
    .end local v2    # "iterator":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;
    .end local v7    # "view":Landroid/view/View;
    :cond_5
    :goto_4
    move v6, v4

    .line 611
    .local v6, "start":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v8

    if-lez v8, :cond_6

    .line 612
    invoke-virtual {p0, v10}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    iget v6, v8, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->start:I

    .line 616
    :cond_6
    if-le v6, v5, :cond_7

    .line 617
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

    invoke-interface {v8, v6, v10}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;->getViewIterator(IZ)Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;

    move-result-object v2

    .line 618
    .restart local v2    # "iterator":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;
    if-eqz v2, :cond_7

    .line 619
    :goto_5
    if-le v6, v5, :cond_7

    .line 621
    invoke-interface {v2, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;->getNext(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 622
    .restart local v7    # "view":Landroid/view/View;
    if-nez v7, :cond_c

    .line 638
    .end local v2    # "iterator":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;
    .end local v7    # "view":Landroid/view/View;
    :cond_7
    :goto_6
    if-eqz v1, :cond_0

    .line 639
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->invalidate()V

    goto/16 :goto_0

    .line 564
    .end local v0    # "end":I
    .end local v6    # "start":I
    .restart local v3    # "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    .restart local v7    # "view":Landroid/view/View;
    :cond_8
    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->removeChild(Landroid/view/View;)V

    .line 565
    const/4 v1, 0x1

    .line 566
    goto/16 :goto_1

    .line 575
    :cond_9
    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->removeChild(Landroid/view/View;)V

    .line 576
    const/4 v1, 0x1

    .line 577
    goto :goto_2

    .line 596
    .end local v3    # "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    .restart local v0    # "end":I
    .restart local v2    # "iterator":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;
    :cond_a
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    .line 597
    .restart local v3    # "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->end()I

    move-result v8

    if-ge v8, v0, :cond_b

    .line 598
    sget-object v8, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->TAG:Ljava/lang/String;

    const-string v9, "IViewIterator returned view with the end position short of specified end"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

    invoke-interface {v8, v7}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;->reclaimView(Landroid/view/View;)V

    goto :goto_4

    .line 602
    :cond_b
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->end()I

    move-result v0

    .line 603
    invoke-direct {p0, v7, v9}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->insertChild(Landroid/view/View;Z)V

    goto :goto_3

    .line 625
    .end local v3    # "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    .restart local v6    # "start":I
    :cond_c
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    .line 626
    .restart local v3    # "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    iget v8, v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->start:I

    if-le v8, v6, :cond_d

    .line 627
    sget-object v8, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->TAG:Ljava/lang/String;

    const-string v9, "IViewIterator returned view with the start position which is greater than specified start"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

    invoke-interface {v8, v7}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;->reclaimView(Landroid/view/View;)V

    goto :goto_6

    .line 631
    :cond_d
    iget v6, v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->start:I

    .line 632
    invoke-direct {p0, v7, v10}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->insertChild(Landroid/view/View;Z)V

    goto :goto_5
.end method

.method private static postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 752
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->initializeCompatibility()V

    .line 754
    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mPostOnAnimation:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 756
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mPostOnAnimation:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 769
    :goto_0
    return-void

    .line 758
    :catch_0
    move-exception v0

    .line 759
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 768
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    :goto_1
    const-wide/16 v2, 0xa

    invoke-virtual {p0, p1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 760
    :catch_1
    move-exception v0

    .line 761
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 762
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v0

    .line 763
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method

.method private postScrollRunnable()V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScrollRunnable:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 449
    return-void
.end method

.method private removeChild(Landroid/view/View;)V
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 672
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->removeViewInLayout(Landroid/view/View;)V

    .line 674
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;->reclaimView(Landroid/view/View;)V

    .line 675
    return-void
.end method

.method public static setChildViewLayoutParams(Landroid/view/View;IIZ)Landroid/view/View;
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "start"    # I
    .param p2, "width"    # I
    .param p3, "temporary"    # Z

    .prologue
    .line 799
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 800
    .local v0, "llp":Landroid/view/ViewGroup$LayoutParams;
    instance-of v2, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 801
    check-cast v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    .line 810
    .end local v0    # "llp":Landroid/view/ViewGroup$LayoutParams;
    .local v1, "mlp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    :goto_0
    iput p1, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->start:I

    .line 811
    iget v2, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->leftMargin:I

    iget v3, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->rightMargin:I

    add-int/2addr v2, v3

    sub-int v2, p2, v2

    iput v2, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->width:I

    .line 812
    iput-boolean p3, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->temporary:Z

    .line 814
    return-object p0

    .line 802
    .end local v1    # "mlp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    .restart local v0    # "llp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    instance-of v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v2, :cond_1

    .line 803
    new-instance v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .end local v0    # "llp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v1, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 804
    .restart local v1    # "mlp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    invoke-virtual {p0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 806
    .end local v1    # "mlp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    .restart local v0    # "llp":Landroid/view/ViewGroup$LayoutParams;
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 807
    .restart local v1    # "mlp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    invoke-virtual {p0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V
    .locals 1
    .param p1, "state"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    if-ne v0, p1, :cond_0

    .line 165
    :goto_0
    return-void

    .line 162
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->onScrollStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    goto :goto_0
.end method

.method private validateContentOffset(I)I
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 536
    if-gez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->maxContentOffset()I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 500
    instance-of v0, p1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    return v0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 515
    const/4 v0, 0x0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 510
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 505
    new-instance v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getAdapter()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

    return-object v0
.end method

.method public getAutoScroll()I
    .locals 1

    .prologue
    .line 864
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAutoScrollVelocity:I

    return v0
.end method

.method public getContentOffset()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentOffset:I

    return v0
.end method

.method public getContentWidth()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentWidth:I

    return v0
.end method

.method public getFlingStop()I
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalX()I

    move-result v0

    return v0
.end method

.method public getListener()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mListener:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;

    return-object v0
.end method

.method public getScrollState()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScrollState:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    return-object v0
.end method

.method public isChildVisible(Landroid/view/View;)Z
    .locals 2
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 691
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getWidth()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public measureChild(Landroid/view/View;)V
    .locals 6
    .param p1, "child"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 722
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getHeight()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 723
    return-void
.end method

.method public notifyChildrenAboutViewPort()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 254
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getViewPortWidth()I

    move-result v5

    .line 255
    .local v5, "width":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v1

    .line 256
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 257
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 258
    .local v0, "child":Landroid/view/View;
    instance-of v6, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IViewPortAwareness;

    if-eqz v6, :cond_0

    .line 259
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v6

    neg-int v6, v6

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 260
    .local v3, "left":I
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v6

    sub-int/2addr v6, v5

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 261
    .local v4, "right":I
    check-cast v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IViewPortAwareness;

    .end local v0    # "child":Landroid/view/View;
    invoke-interface {v0, v3, v4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IViewPortAwareness;->viewPortUpdated(II)V

    .line 256
    .end local v3    # "left":I
    .end local v4    # "right":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 264
    :cond_1
    return-void
.end method

.method protected onContentOffsetChanged(I)V
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getListener()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;

    move-result-object v0

    .line 305
    .local v0, "listener":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;
    if-eqz v0, :cond_0

    .line 306
    invoke-interface {v0, p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;->contentOffsetChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;I)V

    .line 307
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentWidth()I

    move-result v2

    if-gtz v2, :cond_0

    .line 321
    const/4 v2, 0x0

    .line 361
    :goto_0
    return v2

    .line 323
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v0, v2, 0xff

    .line 324
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 361
    :cond_1
    :goto_1
    sget-object v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->hasScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z

    move-result v2

    goto :goto_0

    .line 328
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mLastMotionEventX:F

    .line 331
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_2

    .line 332
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    .line 333
    :cond_2
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->addVelocityTrackerMovement(Landroid/view/MotionEvent;)V

    .line 336
    sget-object v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->hasScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    :goto_2
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    .line 339
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScroller:Landroid/widget/Scroller;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Scroller;->forceFinished(Z)V

    goto :goto_1

    .line 336
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DOWN:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    goto :goto_2

    .line 344
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DOWN:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->hasScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 346
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mLastMotionEventX:F

    sub-float v1, v2, v3

    .line 347
    .local v1, "delta":F
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mTouchSlop:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 348
    sget-object v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    .line 349
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->addVelocityTrackerMovement(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 357
    .end local v1    # "delta":F
    :pswitch_2
    sget-object v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    goto :goto_1

    .line 324
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 486
    if-eqz p1, :cond_0

    .line 487
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentOffset()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setContentOffset(I)V

    .line 488
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->populateChildren()V

    .line 492
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v0

    .line 493
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 494
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->layoutChild(Landroid/view/View;)V

    .line 493
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 496
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 454
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->measureChildren(II)V

    .line 456
    const/4 v10, 0x0

    .line 457
    .local v10, "maxHeight":I
    const/4 v11, 0x0

    .line 459
    .local v11, "maxWidth":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v7

    .line 460
    .local v7, "count":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v7, :cond_1

    .line 461
    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 462
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_0

    .line 464
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingRight()I

    move-result v2

    add-int v3, v0, v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingBottom()I

    move-result v2

    add-int v5, v0, v2

    move-object v0, p0

    move v2, p1

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 466
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;

    .line 467
    .local v9, "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget v2, v9, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->topMargin:I

    add-int/2addr v0, v2

    iget v2, v9, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->bottomMargin:I

    add-int v6, v0, v2

    .line 468
    .local v6, "childHeight":I
    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 460
    .end local v6    # "childHeight":I
    .end local v9    # "lp":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 472
    .end local v1    # "child":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v10, v0

    .line 473
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 474
    invoke-static {v10, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->resolveSize(II)I

    move-result v10

    .line 476
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getPaddingRight()I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v11, v0

    .line 477
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 478
    invoke-static {v11, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->resolveSize(II)I

    move-result v11

    .line 480
    invoke-virtual {p0, v11, v10}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setMeasuredDimension(II)V

    .line 481
    return-void
.end method

.method protected onScrollStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V
    .locals 3
    .param p1, "state"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    .prologue
    const/4 v2, 0x1

    .line 277
    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DOWN:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    if-ne p1, v1, :cond_2

    .line 279
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mRequestDisallowInterceptTouchEventonTouchDown:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 280
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 296
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getListener()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;

    move-result-object v0

    .line 297
    .local v0, "listener":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;
    if-eqz v0, :cond_1

    .line 298
    invoke-interface {v0, p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;->scrollStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    .line 299
    :cond_1
    return-void

    .line 281
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    if-ne p1, v1, :cond_3

    .line 283
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 285
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    if-ne p1, v1, :cond_0

    .line 287
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_4

    .line 288
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 289
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 292
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 367
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentWidth()I

    move-result v0

    if-gtz v0, :cond_0

    .line 431
    :goto_0
    return v2

    .line 370
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->addVelocityTrackerMovement(Landroid/view/MotionEvent;)V

    .line 372
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v9, v0, 0xff

    .line 373
    .local v9, "action":I
    packed-switch v9, :pswitch_data_0

    :cond_1
    :goto_1
    move v2, v12

    .line 431
    goto :goto_0

    .line 377
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mLastMotionEventX:F

    .line 380
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->hasScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    :goto_2
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    .line 383
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0, v12}, Landroid/widget/Scroller;->forceFinished(Z)V

    goto :goto_1

    .line 380
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DOWN:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    goto :goto_2

    .line 388
    :pswitch_1
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mLastMotionEventX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float v10, v0, v1

    .line 389
    .local v10, "delta":F
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DOWN:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->hasScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 392
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mTouchSlop:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 393
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    .line 395
    const/4 v0, 0x0

    cmpl-float v0, v10, v0

    if-lez v0, :cond_4

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mTouchSlop:I

    neg-int v0, v0

    int-to-float v0, v0

    :goto_3
    add-float/2addr v10, v0

    .line 399
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->hasScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 400
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentOffset()I

    move-result v0

    float-to-int v1, v10

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setContentOffset(I)V

    .line 401
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mLastMotionEventX:F

    goto :goto_1

    .line 395
    :cond_4
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mTouchSlop:I

    int-to-float v0, v0

    goto :goto_3

    .line 406
    .end local v10    # "delta":F
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->hasScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 408
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mMaximumVelocity:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 409
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    float-to-int v11, v0

    .line 410
    .local v11, "velocity":I
    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mMinimumVelocity:I

    if-le v0, v1, :cond_5

    .line 412
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentOffset()I

    move-result v1

    neg-int v3, v11

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->maxContentOffset()I

    move-result v6

    move v4, v2

    move v5, v2

    move v7, v2

    move v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 413
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->postScrollRunnable()V

    .line 415
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    goto/16 :goto_1

    .line 417
    :cond_5
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    goto/16 :goto_1

    .line 419
    .end local v11    # "velocity":I
    :cond_6
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->DOWN:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->hasScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 421
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    goto/16 :goto_1

    .line 427
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    goto/16 :goto_1

    .line 373
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public refresh(Z)V
    .locals 0
    .param p1, "all"    # Z

    .prologue
    .line 247
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->clear(Z)V

    .line 248
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->populateChildren()V

    .line 249
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->notifyChildrenAboutViewPort()V

    .line 250
    return-void
.end method

.method public reset(I)V
    .locals 1
    .param p1, "width"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentWidth:I

    .line 124
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentOffset:I

    .line 125
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->refresh(Z)V

    .line 126
    return-void
.end method

.method public setAdapter(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

    if-ne v0, p1, :cond_0

    .line 139
    :goto_0
    return-void

    .line 134
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->clear(Z)V

    .line 136
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;

    .line 138
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentOffset:I

    goto :goto_0
.end method

.method public setAutoScroll(I)V
    .locals 1
    .param p1, "velocity"    # I

    .prologue
    .line 848
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAutoScrollVelocity:I

    if-ne v0, p1, :cond_0

    .line 861
    :goto_0
    return-void

    .line 851
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAutoScrollRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 853
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAutoScrollVelocity:I

    .line 855
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAutoScrollVelocity:I

    if-eqz v0, :cond_1

    .line 856
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mAutoScrollRunnable:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 857
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    goto :goto_0

    .line 859
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    goto :goto_0
.end method

.method public setContentOffset(I)V
    .locals 5
    .param p1, "offset"    # I

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->validateContentOffset(I)I

    move-result p1

    .line 192
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentOffset:I

    if-ne v4, p1, :cond_0

    .line 215
    :goto_0
    return-void

    .line 195
    :cond_0
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentOffset:I

    sub-int v3, v4, p1

    .line 196
    .local v3, "shift":I
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentOffset:I

    .line 199
    const/4 v2, 0x0

    .line 200
    .local v2, "needInvalidate":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v0

    .line 201
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 202
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-direct {p0, v4, v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->offsetChild(Landroid/view/View;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 203
    const/4 v2, 0x1

    .line 201
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 207
    :cond_2
    if-eqz v2, :cond_3

    .line 208
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->invalidate()V

    .line 211
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->populateChildren()V

    .line 214
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentOffset:I

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->onContentOffsetChanged(I)V

    goto :goto_0
.end method

.method public setContentWidth(I)V
    .locals 1
    .param p1, "width"    # I

    .prologue
    .line 177
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentWidth:I

    if-ne v0, p1, :cond_0

    .line 183
    :goto_0
    return-void

    .line 180
    :cond_0
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentWidth:I

    .line 182
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentOffset()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setContentOffset(I)V

    goto :goto_0
.end method

.method public setFlingStop(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScroller:Landroid/widget/Scroller;

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->validateContentOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->setFinalX(I)V

    .line 243
    return-void
.end method

.method public setListener(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mListener:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;

    .line 147
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 155
    return-void
.end method

.method public shiftContent(I)V
    .locals 3
    .param p1, "shift"    # I

    .prologue
    const/4 v2, 0x1

    .line 224
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentOffset:I

    add-int/2addr v1, p1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->validateContentOffset(I)I

    move-result v0

    .line 225
    .local v0, "offset":I
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentOffset:I

    if-ne v1, v0, :cond_0

    .line 226
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->refresh(Z)V

    .line 235
    :goto_0
    return-void

    .line 228
    :cond_0
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentOffset:I

    .line 230
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->refresh(Z)V

    .line 233
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mContentOffset:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->onContentOffsetChanged(I)V

    goto :goto_0
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x1

    return v0
.end method

.method public smoothScrollTo(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 219
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setContentOffset(I)V

    .line 220
    return-void
.end method

.method public stopScrolling()V
    .locals 2

    .prologue
    .line 268
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    .line 269
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mScroller:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 270
    return-void
.end method
