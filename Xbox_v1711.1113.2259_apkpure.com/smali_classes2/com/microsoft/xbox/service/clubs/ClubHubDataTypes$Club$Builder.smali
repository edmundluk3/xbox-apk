.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bannerImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
.end method

.method public abstract clubPresence(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;"
        }
    .end annotation
.end method

.method public abstract clubPresenceCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract clubPresenceInGameCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract clubPresenceTodayCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract clubType(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract creationDateUtc(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract followersCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract founderXuid(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract glyphImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract id(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract maxMembersInGame(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract maxMembersPerClub(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract membersCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract moderatorsCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract ownerXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract recommendation(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract recommendedCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract reportCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract reportedItemsCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract requestedToJoinCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract roster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract shortName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract state(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract suspendedUntilUtc(Ljava/util/Date;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract targetRoles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public abstract titleDeeplinks(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method
