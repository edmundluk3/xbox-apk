.class final synthetic Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$2;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$2;->arg$1:Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$2;-><init>(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$2;->arg$1:Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;

    check-cast p1, Lcom/microsoft/xbox/service/network/managers/ProfileColorList$ProfileColorItem;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->lambda$getAllProfileColorsFromService$1(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;Lcom/microsoft/xbox/service/network/managers/ProfileColorList$ProfileColorItem;)Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v0

    return-object v0
.end method
