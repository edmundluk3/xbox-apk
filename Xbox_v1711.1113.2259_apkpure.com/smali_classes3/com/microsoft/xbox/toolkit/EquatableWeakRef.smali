.class public final Lcom/microsoft/xbox/toolkit/EquatableWeakRef;
.super Ljava/lang/ref/WeakReference;
.source "EquatableWeakRef.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/WeakReference",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<TT;>;"
    .local p1, "r":Ljava/lang/Object;, "TT;"
    invoke-direct {p0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 15
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 19
    .local p0, "this":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<TT;>;"
    if-ne p1, p0, :cond_0

    .line 20
    const/4 v1, 0x1

    .line 25
    :goto_0
    return v1

    .line 21
    :cond_0
    instance-of v1, p1, Ljava/lang/ref/Reference;

    if-nez v1, :cond_1

    .line 22
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 24
    check-cast v0, Ljava/lang/ref/Reference;

    .line 25
    .local v0, "other":Ljava/lang/ref/Reference;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 31
    .local p0, "this":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    .local p0, "this":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": hashCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 37
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", referent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 38
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 36
    return-object v0
.end method
