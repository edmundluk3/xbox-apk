.class interface abstract Lorg/simpleframework/xml/core/Parameter;
.super Ljava/lang/Object;
.source "Parameter.java"


# virtual methods
.method public abstract getAnnotation()Ljava/lang/annotation/Annotation;
.end method

.method public abstract getIndex()I
.end method

.method public abstract getName()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract getName(Lorg/simpleframework/xml/core/Context;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract getType()Ljava/lang/Class;
.end method

.method public abstract isPrimitive()Z
.end method

.method public abstract isRequired()Z
.end method

.method public abstract toString()Ljava/lang/String;
.end method
