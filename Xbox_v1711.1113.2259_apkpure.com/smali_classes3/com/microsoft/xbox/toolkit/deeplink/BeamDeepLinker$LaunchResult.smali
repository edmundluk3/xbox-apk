.class public final enum Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;
.super Ljava/lang/Enum;
.source "BeamDeepLinker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LaunchResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

.field public static final enum App:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

.field public static final enum Failed:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

.field public static final enum Store:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

.field public static final enum Web:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    const-string v1, "Failed"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->Failed:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    new-instance v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    const-string v1, "App"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->App:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    new-instance v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    const-string v1, "Store"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->Store:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    new-instance v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    const-string v1, "Web"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->Web:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    sget-object v1, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->Failed:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->App:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->Store:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->Web:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->$VALUES:[Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->$VALUES:[Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    return-object v0
.end method
