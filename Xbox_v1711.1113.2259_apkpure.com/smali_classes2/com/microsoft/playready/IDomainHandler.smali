.class public interface abstract Lcom/microsoft/playready/IDomainHandler;
.super Ljava/lang/Object;
.source "IDomainHandler.java"


# virtual methods
.method public abstract getDomainHandlingPlugin()Lcom/microsoft/playready/IDomainHandlingPlugin;
.end method

.method public abstract joinDomain(Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;)Lcom/microsoft/playready/IDomainHandlingTask;
.end method

.method public abstract leaveDomain(Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;)Lcom/microsoft/playready/IDomainHandlingTask;
.end method

.method public abstract setDomainHandlingPlugin(Lcom/microsoft/playready/IDomainHandlingPlugin;)V
.end method
