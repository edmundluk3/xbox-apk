.class public Lcom/microsoft/xbox/xle/app/Feedback;
.super Ljava/lang/Object;
.source "Feedback.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/Feedback$Encoding;,
        Lcom/microsoft/xbox/xle/app/Feedback$LogType;
    }
.end annotation


# static fields
.field public static final MAXLAUNCH:I = 0xa

.field private static final MAXLOG:I = 0xbb8

.field private static final MAXVIDEOLOG:I = 0x3e8

.field private static final TAG:Ljava/lang/String;

.field private static alias:Ljava/lang/String; = null

.field private static appLog:Ljava/util/concurrent/ConcurrentLinkedQueue; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static applicationPaused:Ljava/lang/Boolean; = null

.field private static final baseLogName:Ljava/lang/String; = "SmartGlass"

.field private static body:Ljava/lang/String;

.field private static bugArea:Ljava/lang/String;

.field private static bugTitle:Ljava/lang/String;

.field private static description:Ljava/lang/String;

.field private static final feedbackFolder:Ljava/lang/String;

.field private static feedbackMenu:Landroid/view/MenuItem;

.field private static feedbackSaved:Ljava/lang/Boolean;

.field private static feedbackSource:Ljava/lang/String;

.field private static handlingShake:Ljava/lang/Boolean;

.field private static initialized:Lcom/microsoft/xbox/toolkit/Ready;

.field private static isCrash:Ljava/lang/Boolean;

.field private static final logDateFormat:Ljava/text/SimpleDateFormat;

.field private static pageName:Ljava/lang/String;

.field private static screenshot:Ljava/lang/String;

.field private static sentEmailIgnoreInitializeOnce:Ljava/lang/Boolean;

.field private static shakeRunnable:Ljava/lang/Runnable;

.field private static smartGlassLog:Ljava/lang/String;

.field private static smartGlassVideoLog:Ljava/lang/String;

.field private static statusCode:I

.field private static syncObject:Ljava/lang/Object;

.field private static final utcTimeZone:Ljava/util/TimeZone;

.field private static veyronLog:Ljava/lang/String;

.field private static videoLog:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 65
    const-class v0, Lcom/microsoft/xbox/xle/app/Feedback;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->TAG:Ljava/lang/String;

    .line 66
    sput-object v2, Lcom/microsoft/xbox/xle/app/Feedback;->body:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->appLog:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 68
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->videoLog:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 69
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->syncObject:Ljava/lang/Object;

    .line 70
    new-instance v0, Lcom/microsoft/xbox/toolkit/Ready;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/Ready;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->initialized:Lcom/microsoft/xbox/toolkit/Ready;

    .line 71
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->feedbackSaved:Ljava/lang/Boolean;

    .line 74
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->feedbackFolder:Ljava/lang/String;

    .line 75
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->isCrash:Ljava/lang/Boolean;

    .line 77
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->applicationPaused:Ljava/lang/Boolean;

    .line 78
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->sentEmailIgnoreInitializeOnce:Ljava/lang/Boolean;

    .line 79
    const-string v0, "UNKNOWN"

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->pageName:Ljava/lang/String;

    .line 82
    sput-object v2, Lcom/microsoft/xbox/xle/app/Feedback;->shakeRunnable:Ljava/lang/Runnable;

    .line 83
    sput-object v2, Lcom/microsoft/xbox/xle/app/Feedback;->feedbackMenu:Landroid/view/MenuItem;

    .line 84
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->handlingShake:Ljava/lang/Boolean;

    .line 86
    const-string v0, "title.txt"

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->bugTitle:Ljava/lang/String;

    .line 87
    const-string v0, "area.txt"

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->bugArea:Ljava/lang/String;

    .line 88
    const-string v0, "description.txt"

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->description:Ljava/lang/String;

    .line 89
    const-string v0, "screenshot.png"

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->screenshot:Ljava/lang/String;

    .line 90
    const-string v0, "feedbacksource.txt"

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->feedbackSource:Ljava/lang/String;

    .line 91
    const-string v0, "SGTraces.log"

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->veyronLog:Ljava/lang/String;

    .line 92
    const-string v0, "XboxLiveApp.log"

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->smartGlassLog:Ljava/lang/String;

    .line 93
    const-string v0, "SGVideo.log"

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->smartGlassVideoLog:Ljava/lang/String;

    .line 94
    const-string v0, "alias.txt"

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->alias:Ljava/lang/String;

    .line 95
    sput-object v2, Lcom/microsoft/xbox/xle/app/Feedback;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    .line 96
    sput v1, Lcom/microsoft/xbox/xle/app/Feedback;->statusCode:I

    .line 102
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->utcTimeZone:Ljava/util/TimeZone;

    .line 103
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd-MM HH:mm:ss:SSS zzz"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->logDateFormat:Ljava/text/SimpleDateFormat;

    .line 104
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->logDateFormat:Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback;->utcTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 105
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static appendFeedbackData(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xle/app/Feedback$Encoding;)V
    .locals 1
    .param p0, "stream"    # Ljava/io/ByteArrayOutputStream;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "encoding"    # Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    .prologue
    .line 799
    invoke-static {p2, p3}, Lcom/microsoft/xbox/xle/app/Feedback;->toByteArray(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/Feedback$Encoding;)[B

    move-result-object v0

    .line 800
    .local v0, "valueBytes":[B
    if-nez v0, :cond_0

    .line 804
    :goto_0
    return-void

    .line 803
    :cond_0
    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/Feedback;->appendFeedbackData(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method private static appendFeedbackData(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;[B)V
    .locals 9
    .param p0, "stream"    # Ljava/io/ByteArrayOutputStream;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "valueBytes"    # [B

    .prologue
    const/4 v8, 0x0

    .line 807
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    int-to-long v2, v7

    .line 808
    .local v2, "keyLength":J
    array-length v7, p2

    int-to-long v4, v7

    .line 809
    .local v4, "valueLength":J
    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/Feedback;->toByteArray(J)[B

    move-result-object v1

    .line 810
    .local v1, "keyLengthBytes":[B
    sget-object v7, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->UTF16LE:Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    invoke-static {p1, v7}, Lcom/microsoft/xbox/xle/app/Feedback;->toByteArray(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/Feedback$Encoding;)[B

    move-result-object v0

    .line 811
    .local v0, "keyBytes":[B
    if-nez v0, :cond_0

    .line 820
    :goto_0
    return-void

    .line 814
    :cond_0
    array-length v7, v1

    invoke-virtual {p0, v1, v8, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 815
    array-length v7, v0

    invoke-virtual {p0, v0, v8, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 817
    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/Feedback;->toByteArray(J)[B

    move-result-object v6

    .line 818
    .local v6, "valueLengthBytes":[B
    array-length v7, v6

    invoke-virtual {p0, v6, v8, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 819
    array-length v7, p2

    invoke-virtual {p0, p2, v8, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0
.end method

.method public static crashFileExists()Ljava/lang/Boolean;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 935
    const/16 v2, 0x400

    .line 936
    .local v2, "minCrashSize":I
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s/%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    aput-object v8, v7, v4

    sget-object v8, Lcom/microsoft/xbox/toolkit/XLELog;->crashFileName:Ljava/lang/String;

    aput-object v8, v7, v3

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 937
    .local v1, "crashLog":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 938
    .local v0, "crashFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 939
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 941
    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x400

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    :goto_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1
.end method

.method public static createCrashDialog()V
    .locals 0

    .prologue
    .line 673
    return-void
.end method

.method private static createDialog(Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "showCrashDialog"    # Ljava/lang/Boolean;

    .prologue
    .line 618
    return-void
.end method

.method public static createFeedbackDialog()V
    .locals 0

    .prologue
    .line 679
    return-void
.end method

.method public static createSentDialog()V
    .locals 7

    .prologue
    .line 682
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 683
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030102

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 684
    .local v3, "view":Landroid/view/View;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 686
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0704f2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 688
    .local v2, "sentText":Ljava/lang/String;
    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 689
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0704da

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 690
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070152

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback$$Lambda$1;->lambdaFactory$()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 692
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback$$Lambda$2;->lambdaFactory$()Landroid/content/DialogInterface$OnCancelListener;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 694
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 695
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 696
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 697
    return-void
.end method

.method private static createShaker()V
    .locals 0

    .prologue
    .line 380
    return-void
.end method

.method public static deleteLogFile(Ljava/lang/String;)V
    .locals 6
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 140
    new-instance v0, Ljava/io/File;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s/%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/xle/app/Feedback;->feedbackFolder:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 141
    .local v0, "target":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 144
    :cond_0
    return-void
.end method

.method public static deleteOldCrashLogs()V
    .locals 6

    .prologue
    .line 148
    const-string v0, "xboxonesmartglass"

    .line 149
    .local v0, "crashLogPrefix":Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 151
    .local v1, "dir":Ljava/io/File;
    new-instance v4, Lcom/microsoft/xbox/xle/app/Feedback$1;

    invoke-direct {v4}, Lcom/microsoft/xbox/xle/app/Feedback$1;-><init>()V

    invoke-virtual {v1, v4}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v3

    .line 159
    .local v3, "matches":[Ljava/io/File;
    array-length v5, v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v3, v4

    .line 160
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 159
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 162
    .end local v2    # "file":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private static generateLogData(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 991
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->logDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 992
    const-string v0, "%s\t%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/xle/app/Feedback;->logDateFormat:Ljava/text/SimpleDateFormat;

    sget-object v4, Lcom/microsoft/xbox/xle/app/Feedback;->logDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v4}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getActivityName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 522
    const-string v2, "UNKNOWN"

    .line 524
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .line 525
    .local v0, "base":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    if-eqz v0, :cond_0

    .line 526
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getActivityNameForFeedback()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 532
    .end local v0    # "base":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    .line 533
    const-string v2, "UNKNOWN"

    .line 536
    :cond_1
    return-object v2

    .line 528
    :catch_0
    move-exception v1

    .line 529
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Error getting activity name"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getApplicationPaused()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 352
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->applicationPaused:Ljava/lang/Boolean;

    return-object v0
.end method

.method private static getBytesFromFile(Ljava/lang/String;)[B
    .locals 11
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 485
    if-nez p0, :cond_0

    move-object v7, v8

    .line 518
    :goto_0
    return-object v7

    .line 488
    :cond_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 489
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_1

    move-object v7, v8

    .line 490
    goto :goto_0

    .line 494
    :cond_1
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 499
    .local v5, "inputStream":Ljava/io/FileInputStream;
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 500
    .local v6, "outputStream":Ljava/io/ByteArrayOutputStream;
    const/16 v9, 0x400

    new-array v0, v9, [B

    .line 502
    .local v0, "buffer":[B
    const/4 v9, 0x0

    :try_start_1
    array-length v10, v0

    invoke-virtual {v5, v0, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    move-result v1

    .line 503
    .local v1, "count":I
    :goto_1
    if-lez v1, :cond_2

    .line 504
    const/4 v9, 0x0

    invoke-virtual {v6, v0, v9, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 505
    const/4 v9, 0x0

    array-length v10, v0

    invoke-virtual {v5, v0, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    move-result v1

    goto :goto_1

    .line 495
    .end local v0    # "buffer":[B
    .end local v1    # "count":I
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .end local v6    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v3

    .local v3, "e1":Ljava/io/FileNotFoundException;
    move-object v7, v8

    .line 496
    goto :goto_0

    .line 507
    .end local v3    # "e1":Ljava/io/FileNotFoundException;
    .restart local v0    # "buffer":[B
    .restart local v1    # "count":I
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :cond_2
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 512
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    .line 514
    .local v7, "result":[B
    :try_start_2
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 515
    :catch_1
    move-exception v2

    .local v2, "e":Ljava/io/IOException;
    move-object v7, v8

    .line 516
    goto :goto_0

    .line 508
    .end local v1    # "count":I
    .end local v2    # "e":Ljava/io/IOException;
    .end local v7    # "result":[B
    :catch_2
    move-exception v2

    .local v2, "e":Ljava/lang/Exception;
    move-object v7, v8

    .line 509
    goto :goto_0
.end method

.method public static getIsHandlingShake()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private static getReady()Lcom/microsoft/xbox/toolkit/Ready;
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    return-object v0
.end method

.method private static getServiceInfo()[B
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    return-object v0
.end method

.method private static getServiceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x0

    return-object v0
.end method

.method public static handleCrash()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 958
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->crashFileExists()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 959
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;-><init>()V

    .line 960
    .local v0, "temp":Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->crash:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->setFeedbackType(Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;)V

    .line 961
    const-string v1, "00000000-0000-0000-0000-000000000000"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->setReferenceId(Ljava/lang/String;)V

    .line 962
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s/%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/xle/app/Feedback;->feedbackFolder:Ljava/lang/String;

    aput-object v5, v3, v4

    sget-object v4, Lcom/microsoft/xbox/toolkit/XLELog;->crashFileName:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->setCrashLogFile(Ljava/lang/String;)V

    .line 963
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getCrashData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->setFeedbackMessage(Ljava/lang/String;)V

    .line 964
    const/4 v1, 0x3

    const-string v2, "Crash"

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getReferenceId()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getFeedbackMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;->trackFeedback(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->deleteOldCrashLogs()V

    .line 968
    :cond_0
    return-void
.end method

.method public static initialize()V
    .locals 2

    .prologue
    .line 266
    const-string v0, "FEEDBACK"

    const-string v1, "Initialize"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    return-void
.end method

.method static synthetic lambda$createDialog$1(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p0, "dialog"    # Landroid/content/DialogInterface;
    .param p1, "which"    # I

    .prologue
    .line 568
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->crash:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->setFeedbackType(Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;)V

    .line 569
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->saveData()V

    .line 570
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->setIsProblemReport(Z)V

    .line 571
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFeedbackDialog(Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;)V

    .line 572
    return-void
.end method

.method static synthetic lambda$createDialog$2(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p0, "dialog"    # Landroid/content/DialogInterface;
    .param p1, "which"    # I

    .prologue
    .line 574
    invoke-interface {p0}, Landroid/content/DialogInterface;->cancel()V

    .line 575
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->isCrash:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578
    sget-object v0, Lcom/microsoft/xbox/toolkit/XLELog;->crashFileName:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/Feedback;->deleteLogFile(Ljava/lang/String;)V

    .line 582
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->initialize()V

    .line 583
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/Feedback;->setIsHandlingShake(Ljava/lang/Boolean;)V

    .line 584
    return-void
.end method

.method static synthetic lambda$createDialog$3(Landroid/content/DialogInterface;)V
    .locals 1
    .param p0, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 585
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/Feedback;->setIsHandlingShake(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic lambda$createDialog$4(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p0, "dialog"    # Landroid/content/DialogInterface;
    .param p1, "which"    # I

    .prologue
    .line 597
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFeedbackDialog(Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;)V

    .line 598
    return-void
.end method

.method static synthetic lambda$createDialog$5(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p0, "dialog"    # Landroid/content/DialogInterface;
    .param p1, "which"    # I

    .prologue
    .line 600
    invoke-interface {p0}, Landroid/content/DialogInterface;->cancel()V

    .line 601
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback;->isCrash:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    sget-object v0, Lcom/microsoft/xbox/toolkit/XLELog;->crashFileName:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/Feedback;->deleteLogFile(Ljava/lang/String;)V

    .line 608
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->initialize()V

    .line 609
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/Feedback;->setIsHandlingShake(Ljava/lang/Boolean;)V

    .line 610
    return-void
.end method

.method static synthetic lambda$createDialog$6(Landroid/content/DialogInterface;)V
    .locals 1
    .param p0, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 611
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/Feedback;->setIsHandlingShake(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic lambda$createSentDialog$7(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p0, "dialog"    # Landroid/content/DialogInterface;
    .param p1, "which"    # I

    .prologue
    .line 690
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/Feedback;->setIsHandlingShake(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic lambda$createSentDialog$8(Landroid/content/DialogInterface;)V
    .locals 1
    .param p0, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 692
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/Feedback;->setIsHandlingShake(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic lambda$createShaker$0()V
    .locals 2

    .prologue
    .line 370
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->getIsHandlingShake()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 371
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/Feedback;->setIsHandlingShake(Ljava/lang/Boolean;)V

    .line 372
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 374
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 375
    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback;->feedbackMenu:Landroid/view/MenuItem;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 378
    :cond_0
    return-void
.end method

.method static synthetic lambda$sendFeedbackData$9([B)V
    .locals 10
    .param p0, "postBody"    # [B

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 781
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 782
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "Authorization"

    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->getServiceInfo()[B

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 783
    const/4 v2, 0x0

    .line 785
    .local v2, "requestStatus":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->getServiceUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1, p0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStreamWithStatus(Ljava/lang/String;Ljava/util/List;[B)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 786
    iget v3, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    sput v3, Lcom/microsoft/xbox/xle/app/Feedback;->statusCode:I
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 790
    const-string v3, "Feedback"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Status code return value: %s"

    new-array v6, v7, [Ljava/lang/Object;

    sget v7, Lcom/microsoft/xbox/xle/app/Feedback;->statusCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    if-eqz v2, :cond_0

    .line 792
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 795
    :cond_0
    :goto_0
    return-void

    .line 787
    :catch_0
    move-exception v0

    .line 788
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_1
    const-string v3, "Feedback"

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 790
    const-string v3, "Feedback"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Status code return value: %s"

    new-array v6, v7, [Ljava/lang/Object;

    sget v7, Lcom/microsoft/xbox/xle/app/Feedback;->statusCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    if-eqz v2, :cond_0

    .line 792
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    goto :goto_0

    .line 790
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v3

    const-string v4, "Feedback"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Status code return value: %s"

    new-array v7, v7, [Ljava/lang/Object;

    sget v8, Lcom/microsoft/xbox/xle/app/Feedback;->statusCode:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    if-eqz v2, :cond_1

    .line 792
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_1
    throw v3
.end method

.method static synthetic lambda$showRateMeDialog$10(Landroid/content/DialogInterface;)V
    .locals 2
    .param p0, "dialogInterface"    # Landroid/content/DialogInterface;

    .prologue
    .line 1066
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;->trackRemindMeLater()V

    .line 1067
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppLaunchCount(I)V

    .line 1068
    return-void
.end method

.method static synthetic lambda$showRateMeDialog$11(Landroid/app/AlertDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "dialog"    # Landroid/app/AlertDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1079
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;->trackRateApp()V

    .line 1080
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppLaunchCount(I)V

    .line 1081
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchRateApp()V

    .line 1082
    invoke-virtual {p0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1083
    return-void
.end method

.method static synthetic lambda$showRateMeDialog$12(Landroid/app/AlertDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "dialog"    # Landroid/app/AlertDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1086
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;->trackRemindMeLater()V

    .line 1087
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppLaunchCount(I)V

    .line 1088
    invoke-virtual {p0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1089
    return-void
.end method

.method static synthetic lambda$showRateMeDialog$13(Landroid/app/AlertDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "dialog"    # Landroid/app/AlertDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1092
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;->trackDontAsk()V

    .line 1093
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppLaunchCount(I)V

    .line 1094
    invoke-virtual {p0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1095
    return-void
.end method

.method public static pauseShakeDetect()V
    .locals 0

    .prologue
    .line 338
    return-void
.end method

.method private static removeShaker()V
    .locals 0

    .prologue
    .line 386
    return-void
.end method

.method public static resumeShakeDetect()V
    .locals 0

    .prologue
    .line 349
    return-void
.end method

.method private static saveAppLog()Ljava/lang/String;
    .locals 2

    .prologue
    .line 165
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->APP:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->getFilename()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback;->appLog:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/Feedback;->saveLogInternal(Ljava/lang/String;Ljava/util/concurrent/ConcurrentLinkedQueue;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized saveCrashLog(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 239
    const-class v0, Lcom/microsoft/xbox/xle/app/Feedback;

    monitor-enter v0

    const/4 v1, 0x0

    monitor-exit v0

    return-object v1
.end method

.method private static saveLogInternal(Ljava/lang/String;Ljava/util/concurrent/ConcurrentLinkedQueue;)Ljava/lang/String;
    .locals 1
    .param p0, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, "log":Ljava/util/concurrent/ConcurrentLinkedQueue;, "Ljava/util/concurrent/ConcurrentLinkedQueue<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method private static declared-synchronized savePlatformLog()Ljava/lang/String;
    .locals 2

    .prologue
    .line 666
    const-class v0, Lcom/microsoft/xbox/xle/app/Feedback;

    monitor-enter v0

    const/4 v1, 0x0

    monitor-exit v0

    return-object v1
.end method

.method private static saveVideoLog()Ljava/lang/String;
    .locals 2

    .prologue
    .line 169
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->VIDEO:Lcom/microsoft/xbox/xle/app/Feedback$LogType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/Feedback$LogType;->getFilename()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback;->videoLog:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/Feedback;->saveLogInternal(Ljava/lang/String;Ljava/util/concurrent/ConcurrentLinkedQueue;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static sendEmail()V
    .locals 0

    .prologue
    .line 910
    return-void
.end method

.method public static sendFeedback(ZLcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;)V
    .locals 0
    .param p0, "isCrash"    # Z
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    .prologue
    .line 774
    return-void
.end method

.method private static sendFeedbackData([B)V
    .locals 2
    .param p0, "postBody"    # [B

    .prologue
    .line 777
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 796
    :cond_0
    :goto_0
    return-void

    .line 780
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/Feedback$$Lambda$3;->lambdaFactory$([B)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static setApplicationPaused(Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "paused"    # Ljava/lang/Boolean;

    .prologue
    .line 329
    return-void
.end method

.method public static setFeedbackMenu(Landroid/view/MenuItem;)V
    .locals 0
    .param p0, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 323
    return-void
.end method

.method public static setIsHandlingShake(Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "isHandling"    # Ljava/lang/Boolean;

    .prologue
    .line 310
    return-void
.end method

.method public static showRateMeDialog()V
    .locals 16

    .prologue
    const/16 v14, 0xa

    .line 1049
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getRateAppLaunchCount()I

    move-result v1

    .line 1050
    .local v1, "count":I
    const-string v10, "Launch count:%s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1052
    if-eq v1, v14, :cond_0

    .line 1053
    sget-object v10, Lcom/microsoft/xbox/xle/app/Feedback;->TAG:Ljava/lang/String;

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v12, "Do not show Rate App dialog.  Launch count: %s != %s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const/16 v15, 0xa

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v11, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 1104
    :goto_0
    return-void

    .line 1057
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v10

    invoke-direct {v0, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1058
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v10

    const-string v11, "layout_inflater"

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 1059
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const v10, 0x7f03022c

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v5, v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 1061
    .local v9, "view":Landroid/view/View;
    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070b14

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1062
    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070b16

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1063
    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1064
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1065
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback$$Lambda$4;->lambdaFactory$()Landroid/content/DialogInterface$OnCancelListener;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1070
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 1071
    .local v2, "dialog":Landroid/app/AlertDialog;
    const v10, 0x7f0e0ac1

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1072
    .local v8, "positive":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    const v10, 0x7f0e0ac2

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1073
    .local v7, "neutral":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    const v10, 0x7f0e0ac3

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1075
    .local v6, "negative":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    const v10, 0x7f07006a

    invoke-virtual {v8, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 1076
    const v10, 0x7f070b15

    invoke-virtual {v7, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 1077
    const v10, 0x7f070b13

    invoke-virtual {v6, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 1078
    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/Feedback$$Lambda$5;->lambdaFactory$(Landroid/app/AlertDialog;)Landroid/view/View$OnClickListener;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1085
    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/Feedback$$Lambda$6;->lambdaFactory$(Landroid/app/AlertDialog;)Landroid/view/View$OnClickListener;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1091
    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/Feedback$$Lambda$7;->lambdaFactory$(Landroid/app/AlertDialog;)Landroid/view/View$OnClickListener;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1097
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;->trackRateAppPageView()V

    .line 1098
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1099
    .end local v0    # "alert":Landroid/app/AlertDialog$Builder;
    .end local v2    # "dialog":Landroid/app/AlertDialog;
    .end local v5    # "inflater":Landroid/view/LayoutInflater;
    .end local v6    # "negative":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .end local v7    # "neutral":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .end local v8    # "positive":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .end local v9    # "view":Landroid/view/View;
    :catch_0
    move-exception v3

    .line 1100
    .local v3, "e":Ljava/lang/Exception;
    sget-object v10, Lcom/microsoft/xbox/xle/app/Feedback;->TAG:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    const-string v4, "Failed to create rate me dialog"

    .line 1102
    .local v4, "errorCode":Ljava/lang/String;
    const-string v10, "Failed to create rate me dialog"

    invoke-static {v10, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method public static storeCrashData()V
    .locals 0

    .prologue
    .line 361
    return-void
.end method

.method private static storeData()V
    .locals 0

    .prologue
    .line 412
    return-void
.end method

.method public static takeScreenshot(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 481
    const/4 v0, 0x0

    return-object v0
.end method

.method private static toByteArray(J)[B
    .locals 4
    .param p0, "value"    # J

    .prologue
    .line 832
    const/16 v2, 0x8

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 833
    .local v0, "bufferLE":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, p0, p1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 834
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 835
    .local v1, "resultLE":[B
    return-object v1
.end method

.method private static toByteArray(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/Feedback$Encoding;)[B
    .locals 2
    .param p0, "value"    # Ljava/lang/String;
    .param p1, "encoding"    # Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    .prologue
    .line 823
    const/4 v0, 0x0

    .line 825
    .local v0, "result":[B
    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 828
    :goto_0
    return-object v0

    .line 826
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static trackLaunchCount()V
    .locals 11

    .prologue
    const v10, 0xa32fa28

    .line 1005
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->ensureLaunchRateApp()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1036
    .local v0, "count":I
    :goto_0
    return-void

    .line 1010
    .end local v0    # "count":I
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getRateAppLaunchCount()I

    move-result v0

    .line 1011
    .restart local v0    # "count":I
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getRateAppVersion()I

    move-result v3

    .line 1012
    .local v3, "version":I
    sget-object v4, Lcom/microsoft/xbox/xle/app/Feedback;->TAG:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Launch count:%s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    sget-object v4, Lcom/microsoft/xbox/xle/app/Feedback;->TAG:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Launch version:%s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    if-le v10, v3, :cond_1

    .line 1018
    sget-object v4, Lcom/microsoft/xbox/xle/app/Feedback;->TAG:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Launch version:%s is > previous version:%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const v9, 0xa32fa28

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppLaunchCount(I)V

    .line 1021
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v4

    const v5, 0xa32fa28

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppVersion(I)V

    .line 1023
    sget-object v4, Lcom/microsoft/xbox/xle/app/Feedback;->TAG:Ljava/lang/String;

    const-string v5, "Reset version and launch count"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1031
    .end local v3    # "version":I
    :catch_0
    move-exception v1

    .line 1032
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/microsoft/xbox/xle/app/Feedback;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    const-string v2, "failed to track launch count"

    .line 1034
    .local v2, "errorCode":Ljava/lang/String;
    const-string v4, "failed to track launch count"

    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 1028
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "errorCode":Ljava/lang/String;
    .restart local v3    # "version":I
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v4

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppLaunchCount(I)V

    .line 1030
    sget-object v4, Lcom/microsoft/xbox/xle/app/Feedback;->TAG:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Incremented the launch count:%s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public static write(Ljava/lang/String;)V
    .locals 0
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 978
    return-void
.end method

.method public static writeVideoLog(Ljava/lang/String;)V
    .locals 0
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 988
    return-void
.end method
