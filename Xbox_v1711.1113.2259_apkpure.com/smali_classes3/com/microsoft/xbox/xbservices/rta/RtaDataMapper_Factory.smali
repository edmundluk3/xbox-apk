.class public final Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper_Factory;
.super Ljava/lang/Object;
.source "RtaDataMapper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;",
        ">;"
    }
.end annotation


# static fields
.field private static final INSTANCE:Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper_Factory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper_Factory;

    invoke-direct {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper_Factory;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper_Factory;->INSTANCE:Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper_Factory;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    sget-object v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper_Factory;->INSTANCE:Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper_Factory;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;

    invoke-direct {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;-><init>()V

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper_Factory;->get()Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;

    move-result-object v0

    return-object v0
.end method
