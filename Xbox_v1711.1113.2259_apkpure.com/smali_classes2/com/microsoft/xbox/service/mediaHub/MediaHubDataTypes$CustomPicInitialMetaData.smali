.class public abstract Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;
.super Ljava/lang/Object;
.source "MediaHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CustomPicInitialMetaData"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 519
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;J)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;
    .locals 3
    .param p0, "customPicType"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "associationId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 512
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 513
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 515
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData;-><init>(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;J)V

    return-object v0
.end method


# virtual methods
.method public abstract associationId()J
.end method

.method public abstract customPicType()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
