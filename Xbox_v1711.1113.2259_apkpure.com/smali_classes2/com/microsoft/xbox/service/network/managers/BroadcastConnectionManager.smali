.class public Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;
.super Lcom/microsoft/xbox/smartglass/SessionManagerListener;
.source "BroadcastConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;,
        Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;,
        Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;,
        Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;
    }
.end annotation


# static fields
.field private static final JSON_ENABLED:Ljava/lang/String; = "enabled"

.field private static final JSON_ERROR_TYPE:Ljava/lang/String; = "errorType"

.field private static final JSON_ERROR_VALUE:Ljava/lang/String; = "errorValue"

.field private static final JSON_STATE:Ljava/lang/String; = "state"

.field private static final JSON_TYPE:Ljava/lang/String; = "type"

.field private static final TAG:Ljava/lang/String; = "BroadcastConnectionManager"

.field private static final mServiceChannel:Lcom/microsoft/xbox/smartglass/ServiceChannel;

.field private static sInstance:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;


# instance fields
.field private mGameStreamState:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

.field private mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mIsConnected:Z

.field private mIsGameStreamEnabled:Z

.field private final mLock:Ljava/lang/Object;

.field private mStartChannelCalled:Z

.field private final mTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/microsoft/xbox/smartglass/ServiceChannel;->SystemBroadcast:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mServiceChannel:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;-><init>()V

    .line 64
    new-instance v0, Lcom/microsoft/xbox/smartglass/MessageTarget;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mServiceChannel:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(Lcom/microsoft/xbox/smartglass/ServiceChannel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mLock:Ljava/lang/Object;

    .line 84
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/smartglass/SessionManager;->addListener(Ljava/lang/Object;)V

    .line 87
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 88
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/SessionManager;->getConnectionState()Lcom/microsoft/xbox/smartglass/ConnectionState;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mStartChannelCalled:Z

    if-nez v0, :cond_0

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/SessionManager;->startChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;I)V

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mStartChannelCalled:Z

    .line 92
    :cond_0
    monitor-exit v1

    .line 93
    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->sInstance:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->sInstance:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    .line 80
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->sInstance:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    return-object v0
.end method

.method private native nativeChangeQuality(IIII)V
.end method

.method private native nativeConnectToStream(Ljava/lang/String;)Z
.end method

.method private native nativeDisconnectFromStream()V
.end method

.method private native nativeStartNetworkTest()Z
.end method

.method public static onDestroy()V
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->sInstance:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    if-eqz v0, :cond_0

    .line 97
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->sInstance:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->onDestroyInternal()V

    .line 99
    :cond_0
    return-void
.end method

.method private onDestroyInternal()V
    .locals 3

    .prologue
    .line 102
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/smartglass/SessionManager;->removeListener(Ljava/lang/Object;)V

    .line 104
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/SessionManager;->stopChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->sInstance:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    .line 109
    return-void

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "BroadcastConnectionManager"

    const-string v2, "Failed to stop communication channel"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private reset()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 320
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;

    .line 321
    .local v0, "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsGameStreamEnabled:Z

    if-eqz v2, :cond_1

    .line 322
    invoke-interface {v0, v4}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;->onGameStreamEnabledChanged(Z)V

    .line 324
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamState:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Unknown:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    if-eq v2, v3, :cond_2

    .line 325
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Unknown:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;->onGameStreamStateChanged(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;)V

    .line 327
    :cond_2
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsConnected:Z

    if-eqz v2, :cond_0

    .line 328
    invoke-interface {v0, v4}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;->onBroadcastChannelConnectionStateChanged(Z)V

    goto :goto_0

    .line 332
    .end local v0    # "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    :cond_3
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsConnected:Z

    .line 333
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mStartChannelCalled:Z

    .line 334
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsGameStreamEnabled:Z

    .line 335
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Unknown:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamState:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    .line 336
    return-void
.end method

.method private sendMessage(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;)Z
    .locals 6
    .param p1, "type"    # Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;

    .prologue
    const/4 v2, 0x0

    .line 304
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsConnected:Z

    if-eqz v3, :cond_0

    .line 306
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 307
    .local v1, "json":Lorg/json/JSONObject;
    const-string v3, "type"

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;->ordinal()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 308
    const-string v3, "BroadcastConnectionManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "send message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v3

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendTitleMessage(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    const/4 v2, 0x1

    .line 316
    .end local v1    # "json":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return v2

    .line 311
    :catch_0
    move-exception v0

    .line 312
    .local v0, "e":Lorg/json/JSONException;
    const-string v3, "BroadcastConnectionManager"

    const-string v4, "Failed to create json message."

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public addGameStreamStateListener(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;

    .prologue
    .line 112
    if-eqz p1, :cond_0

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_0
    return-void
.end method

.method public changeQuality(IIII)V
    .locals 1
    .param p1, "bitrate"    # I
    .param p2, "resolutionIndex"    # I
    .param p3, "frameRateNum"    # I
    .param p4, "frameRateDen"    # I

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->getIsGameStreaming()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->nativeChangeQuality(IIII)V

    .line 174
    :cond_0
    return-void
.end method

.method public connectToStream(Ljava/lang/String;)Z
    .locals 3
    .param p1, "serverAddress"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 144
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    const-string v1, "BroadcastConnectionManager"

    const-string v2, "Server address is empty"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :goto_0
    return v0

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->getIsGameStreaming()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 149
    const-string v0, "BroadcastConnectionManager"

    const-string v1, "Connect To Stream"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->nativeConnectToStream(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 152
    :cond_1
    const-string v1, "BroadcastConnectionManager"

    const-string v2, "Failed to connect to stream. Game streaming has not started on the console."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public disconnectFromStream()V
    .locals 2

    .prologue
    .line 157
    const-string v0, "BroadcastConnectionManager"

    const-string v1, "Disconnect From Stream"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->nativeDisconnectFromStream()V

    .line 159
    return-void
.end method

.method public getIsConnected()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsConnected:Z

    return v0
.end method

.method public getIsGameStreamEnabled()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsGameStreamEnabled:Z

    return v0
.end method

.method public getIsGameStreaming()Z
    .locals 2

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsConnected:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsGameStreamEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamState:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Unknown:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamState:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Stopped:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onChannelEstablished(Lcom/microsoft/xbox/smartglass/MessageTarget;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 8
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    const/4 v7, 0x1

    .line 288
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/smartglass/MessageTarget;->equals(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 289
    invoke-virtual {p2}, Lcom/microsoft/xbox/smartglass/SGResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 290
    const-string v1, "BroadcastConnectionManager"

    const-string v2, "onChannelEstablished"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iput-boolean v7, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsConnected:Z

    .line 292
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;

    .line 293
    .local v0, "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsConnected:Z

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;->onBroadcastChannelConnectionStateChanged(Z)V

    goto :goto_0

    .line 295
    .end local v0    # "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;->GameStreamEnabled:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->sendMessage(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;)Z

    .line 301
    :cond_1
    :goto_1
    return-void

    .line 297
    :cond_2
    const-string v1, "BroadcastConnectionManager"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Failed to establish broadcast channel: %s InnerError: %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p2, Lcom/microsoft/xbox/smartglass/SGResult;->error:Lcom/microsoft/xbox/smartglass/SGError;

    invoke-virtual {v6}, Lcom/microsoft/xbox/smartglass/SGError;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    iget v5, p2, Lcom/microsoft/xbox/smartglass/SGResult;->innerError:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 4
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/ConnectionState;
    .param p2, "exception"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 206
    const-string v0, "BroadcastConnectionManager"

    const-string v1, "onConnectionStateChanged"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 208
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mStartChannelCalled:Z

    if-nez v0, :cond_0

    .line 209
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/SessionManager;->startChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;I)V

    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mStartChannelCalled:Z

    .line 214
    :goto_0
    monitor-exit v1

    .line 215
    return-void

    .line 212
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->reset()V

    goto :goto_0

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onMessageReceived(Lcom/microsoft/xbox/smartglass/Message;)V
    .locals 18
    .param p1, "message"    # Lcom/microsoft/xbox/smartglass/Message;

    .prologue
    .line 227
    const-string v12, "BroadcastConnectionManager"

    const-string v13, "onMessageReceived"

    invoke-static {v12, v13}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/microsoft/xbox/smartglass/Message;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/smartglass/MessageTarget;->equals(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v12

    if-eqz v12, :cond_0

    move-object/from16 v5, p1

    .line 230
    check-cast v5, Lcom/microsoft/xbox/smartglass/JsonMessage;

    .line 231
    .local v5, "jsonMessage":Lcom/microsoft/xbox/smartglass/JsonMessage;
    if-eqz v5, :cond_0

    .line 233
    :try_start_0
    new-instance v8, Lorg/json/JSONObject;

    iget-object v12, v5, Lcom/microsoft/xbox/smartglass/JsonMessage;->text:Ljava/lang/String;

    invoke-direct {v8, v12}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 234
    .local v8, "jsonObject":Lorg/json/JSONObject;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;->values()[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;

    move-result-object v12

    const-string v13, "type"

    const/4 v14, 0x0

    invoke-virtual {v8, v13, v14}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v13

    aget-object v11, v12, v13

    .line 236
    .local v11, "type":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;
    sget-object v12, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$BroadcastConnectionManager$BroadcastMessageType:[I

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_0

    .line 276
    const-string v12, "BroadcastConnectionManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Received an unknown message type. Message = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v5, Lcom/microsoft/xbox/smartglass/JsonMessage;->text:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    .end local v5    # "jsonMessage":Lcom/microsoft/xbox/smartglass/JsonMessage;
    .end local v8    # "jsonObject":Lorg/json/JSONObject;
    .end local v11    # "type":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;
    :cond_0
    :goto_0
    return-void

    .line 238
    .restart local v5    # "jsonMessage":Lcom/microsoft/xbox/smartglass/JsonMessage;
    .restart local v8    # "jsonObject":Lorg/json/JSONObject;
    .restart local v11    # "type":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;
    :pswitch_0
    const-string v12, "state"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 239
    .local v10, "stateIndex":I
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->values()[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    move-result-object v12

    array-length v12, v12

    if-lt v10, v12, :cond_1

    .line 240
    const-string v12, "BroadcastConnectionManager"

    const-string v13, "Unkonwn state index. Default to 0."

    invoke-static {v12, v13}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const/4 v10, 0x0

    .line 243
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->values()[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    move-result-object v12

    aget-object v12, v12, v10

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamState:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    .line 245
    const-string v12, "BroadcastConnectionManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "OnGameStreamStateChanged: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamState:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    invoke-virtual {v14}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->name()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v12}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;

    .line 248
    .local v9, "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamState:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    invoke-interface {v9, v13}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;->onGameStreamStateChanged(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 279
    .end local v8    # "jsonObject":Lorg/json/JSONObject;
    .end local v9    # "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    .end local v10    # "stateIndex":I
    .end local v11    # "type":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;
    :catch_0
    move-exception v2

    .line 280
    .local v2, "e":Lorg/json/JSONException;
    const-string v12, "BroadcastConnectionManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Failed to parse jsonMessage. Message = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v5, Lcom/microsoft/xbox/smartglass/JsonMessage;->text:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 252
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v8    # "jsonObject":Lorg/json/JSONObject;
    .restart local v11    # "type":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;
    :pswitch_1
    :try_start_1
    const-string v12, "enabled"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsGameStreamEnabled:Z

    .line 254
    const-string v12, "BroadcastConnectionManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "OnGameStreamEnabled: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsGameStreamEnabled:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v12}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;

    .line 257
    .restart local v9    # "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mIsGameStreamEnabled:Z

    invoke-interface {v9, v13}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;->onGameStreamEnabledChanged(Z)V

    goto :goto_2

    .line 261
    .end local v9    # "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    :pswitch_2
    const-string v12, "errorType"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 262
    .local v4, "errorTypeIndex":I
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->values()[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    move-result-object v12

    array-length v12, v12

    if-lt v4, v12, :cond_2

    .line 263
    const-string v12, "BroadcastConnectionManager"

    const-string v13, "Unkonwn error type index. Default to 0."

    invoke-static {v12, v13}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const/4 v4, 0x0

    .line 266
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->values()[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    move-result-object v12

    aget-object v3, v12, v4

    .line 267
    .local v3, "errorType":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;
    const-string v12, "errorValue"

    const-wide/16 v14, 0x0

    invoke-virtual {v8, v12, v14, v15}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 269
    .local v6, "errorValue":J
    const-string v12, "BroadcastConnectionManager"

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "GameStreamError: %s Value: %d"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->name()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v12}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;

    .line 272
    .restart local v9    # "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    invoke-interface {v9, v3}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;->onGameStreamError(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNetworkTestCompleted(ZLjava/lang/String;)V
    .locals 4
    .param p1, "succeeded"    # Z
    .param p2, "jsonResult"    # Ljava/lang/String;

    .prologue
    .line 178
    const-string v1, "BroadcastConnectionManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OnNetworkTestCompleted: success = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;

    .line 180
    .local v0, "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    invoke-interface {v0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;->onNetworkTestCompleted(ZLjava/lang/String;)V

    goto :goto_0

    .line 182
    .end local v0    # "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    :cond_0
    return-void
.end method

.method public onPairedIdentityStateChanged(Lcom/microsoft/xbox/smartglass/PairedIdentityState;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 3
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/PairedIdentityState;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 219
    const-string v1, "BroadcastConnectionManager"

    const-string v2, "onPairedIdentityStateChanged"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;

    .line 221
    .local v0, "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    invoke-interface {v0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;->onPairedIdentityStateChanged(Lcom/microsoft/xbox/smartglass/PairedIdentityState;Lcom/microsoft/xbox/smartglass/SGResult;)V

    goto :goto_0

    .line 223
    .end local v0    # "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    :cond_0
    return-void
.end method

.method public onStreamConnectionStateChanged(Z)V
    .locals 4
    .param p1, "isConnected"    # Z

    .prologue
    .line 185
    const-string v1, "BroadcastConnectionManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OnStreamConnectionStateChanged: isConnected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    if-nez p1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->disconnectFromStream()V

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;

    .line 191
    .local v0, "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;->onStreamConnectionStateChanged(Z)V

    goto :goto_0

    .line 193
    .end local v0    # "listener":Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
    :cond_1
    return-void
.end method

.method public removeGameStreamStateListener(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;

    .prologue
    .line 118
    if-eqz p1, :cond_0

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->mGameStreamStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 121
    :cond_0
    return-void
.end method

.method public startGameStream()Z
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;->StartGameStream:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->sendMessage(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;)Z

    move-result v0

    return v0
.end method

.method public startNetworkTest()Z
    .locals 2

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->getIsGameStreaming()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    const-string v0, "BroadcastConnectionManager"

    const-string v1, "Start Network Test"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->nativeStartNetworkTest()Z

    move-result v0

    .line 167
    :goto_0
    return v0

    .line 166
    :cond_0
    const-string v0, "BroadcastConnectionManager"

    const-string v1, "Failed to start network test. Game streaming has not started on the console."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopGameStream()Z
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;->StopGameStream:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->sendMessage(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$BroadcastMessageType;)Z

    move-result v0

    return v0
.end method
