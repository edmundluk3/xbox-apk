.class public Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HoverChatBroadcastReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;

.field repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getConverstationId(Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;)Ljava/lang/String;
    .locals 5
    .param p1, "message"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    .prologue
    .line 136
    iget-object v4, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->conversationLink:Ljava/lang/String;

    invoke-static {v4}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getSkypeIdFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "id":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeGroupMessage(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    .line 139
    .local v2, "isGroupConversation":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 144
    .end local v1    # "id":Ljava/lang/String;
    :goto_1
    return-object v1

    .line 137
    .end local v2    # "isGroupConversation":Z
    .restart local v1    # "id":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 142
    .restart local v2    # "isGroupConversation":Z
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->from:Ljava/lang/String;

    .line 143
    .local v0, "from":Ljava/lang/String;
    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 144
    .local v3, "parts":[Ljava/lang/String;
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    aget-object v1, v3, v4

    goto :goto_1
.end method

.method private isClubChatNotification(Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 65
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 66
    const-string/jumbo v1, "type"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->fromType(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    move-result-object v0

    .line 67
    .local v0, "notificationType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isMessageNotification(Landroid/os/Bundle;)Z
    .locals 9
    .param p1, "bundle"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 93
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 95
    const-string v8, "rawPayload"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 97
    .local v5, "rawPayload":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 99
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 100
    .local v2, "jsonPayload":Lorg/json/JSONObject;
    const-string/jumbo v8, "type"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->fromSkypeType(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    move-result-object v4

    .line 101
    .local v4, "notificationType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    const-string v8, "messagetype"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v3

    .line 103
    .local v3, "messageType":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    sget-object v8, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Text:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    if-eq v3, v8, :cond_0

    sget-object v8, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Attachment:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    if-eq v3, v8, :cond_0

    sget-object v8, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Activity:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    if-ne v3, v8, :cond_1

    :cond_0
    move v1, v6

    .line 107
    .local v1, "isRelevantSkypeType":Z
    :goto_0
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v4, v8, :cond_2

    if-eqz v1, :cond_2

    .line 114
    .end local v1    # "isRelevantSkypeType":Z
    .end local v2    # "jsonPayload":Lorg/json/JSONObject;
    .end local v3    # "messageType":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    .end local v4    # "notificationType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :goto_1
    return v6

    .restart local v2    # "jsonPayload":Lorg/json/JSONObject;
    .restart local v3    # "messageType":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    .restart local v4    # "notificationType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :cond_1
    move v1, v7

    .line 103
    goto :goto_0

    .restart local v1    # "isRelevantSkypeType":Z
    :cond_2
    move v6, v7

    .line 107
    goto :goto_1

    .line 109
    .end local v1    # "isRelevantSkypeType":Z
    .end local v2    # "jsonPayload":Lorg/json/JSONObject;
    .end local v3    # "messageType":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    .end local v4    # "notificationType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Lorg/json/JSONException;
    sget-object v6, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v8, "Exp thrown when creating JSONObject from rawPayload"

    invoke-static {v6, v8, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v0    # "e":Lorg/json/JSONException;
    :cond_3
    move v6, v7

    .line 114
    goto :goto_1
.end method

.method private onClubChatNotification(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 71
    const-string/jumbo v5, "xbl"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 73
    .local v1, "detailsPayload":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 75
    :try_start_0
    const-class v5, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;

    invoke-static {v1, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;

    .line 76
    .local v0, "chatNotification":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;->getDetails()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->getChatChannel()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 78
    .local v2, "clubId":J
    iget-object v5, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->with(J)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->isEnabledFromNotificationForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 79
    iget-object v5, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v5, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->clubNotificationTriggered(J)V

    .line 80
    iget-object v5, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->context:Landroid/content/Context;

    invoke-static {v5, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->addClubInstance(Landroid/content/Context;J)V

    .line 90
    .end local v0    # "chatNotification":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;
    .end local v2    # "clubId":J
    :goto_0
    return-void

    .line 82
    .restart local v0    # "chatNotification":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;
    .restart local v2    # "clubId":J
    :cond_0
    sget-object v5, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Received club chat notification, but bubble is disabled. ClubId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 84
    .end local v0    # "chatNotification":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;
    .end local v2    # "clubId":J
    :catch_0
    move-exception v4

    .line 85
    .local v4, "ex":Ljava/lang/Exception;
    sget-object v5, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to get club details from payload: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 88
    .end local v4    # "ex":Ljava/lang/Exception;
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not find club chat payload: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onMessageNotification(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 118
    const-string v3, "rawPayload"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "rawPayload":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 121
    const-class v3, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    .line 122
    .local v2, "skypeMessage":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->getConverstationId(Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;)Ljava/lang/String;

    move-result-object v0

    .line 124
    .local v0, "conversationId":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->with(Ljava/lang/String;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->isEnabledFromNotificationForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 125
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->messageNotificationTriggered(Ljava/lang/String;)V

    .line 126
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->context:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->addMessageInstance(Landroid/content/Context;Ljava/lang/String;)V

    .line 133
    .end local v0    # "conversationId":Ljava/lang/String;
    .end local v2    # "skypeMessage":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    :goto_0
    return-void

    .line 128
    .restart local v0    # "conversationId":Ljava/lang/String;
    .restart local v2    # "skypeMessage":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    :cond_0
    sget-object v3, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received message notification, but bubble is disabled. ConversationId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    .end local v0    # "conversationId":Ljava/lang/String;
    .end local v2    # "skypeMessage":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not find message payload: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->context:Landroid/content/Context;

    .line 44
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 46
    .local v0, "extras":Landroid/os/Bundle;
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v2}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v2

    invoke-interface {v2, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;)V

    .line 48
    iget-object v2, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->isEnabledGlobally()Z

    move-result v1

    .line 50
    .local v1, "isEnabled":Z
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 51
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Ignoring. isEnabled: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " extras: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :goto_0
    return-void

    .line 55
    :cond_1
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->isClubChatNotification(Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 56
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->onClubChatNotification(Landroid/os/Bundle;)V

    goto :goto_0

    .line 57
    :cond_2
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->isMessageNotification(Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 58
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->onMessageNotification(Landroid/os/Bundle;)V

    goto :goto_0

    .line 60
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not a club or message notification: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
