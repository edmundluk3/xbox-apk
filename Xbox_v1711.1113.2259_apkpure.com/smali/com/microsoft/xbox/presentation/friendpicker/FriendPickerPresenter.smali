.class public Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;
.super Lcom/microsoft/xbox/presentation/base/MviPresenter;
.source "FriendPickerPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/presentation/base/MviPresenter",
        "<",
        "Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerView;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        "Lcom/microsoft/xbox/presentation/lce/LceViewState",
        "<",
        "Lcom/google/common/collect/ImmutableList",
        "<",
        "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
        ">;>;>;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final intentToStateTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 1
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;-><init>()V

    .line 38
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 40
    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 57
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;Lcom/microsoft/xbox/presentation/lce/LceViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->stateReducer(Lcom/microsoft/xbox/presentation/lce/LceViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$initialSetup$7(Lcom/microsoft/xbox/presentation/lce/LceViewState;)V
    .locals 2
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    sget-object v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->TAG:Ljava/lang/String;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/lce/LceViewState;)Lcom/microsoft/xbox/toolkit/logging/Loggable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logRender(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/logging/Loggable;)V

    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;
    .param p2, "intents"    # Lio/reactivex/Observable;

    .prologue
    .line 41
    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 56
    invoke-static {}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;)Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 41
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .locals 1
    .param p0, "initialLoadIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    return-object v0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$OkIntent;)Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;
    .locals 1
    .param p0, "okIntent"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$OkIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    sget-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;->INSTANCE:Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$CancelIntent;)Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;
    .locals 1
    .param p0, "cancelIntent"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$CancelIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    sget-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;->INSTANCE:Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "interactor"    # Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;
    .param p1, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    const-class v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 44
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 46
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->getInitialLoadTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$OkIntent;

    .line 48
    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter$$Lambda$10;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 49
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 50
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->getOkTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$CancelIntent;

    .line 52
    invoke-virtual {p1, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 53
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->getCancelTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 43
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;
    .param p2, "intent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    invoke-static {p2}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 42
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 41
    return-object v0
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/presentation/lce/LceViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/lce/LceViewState;

    .prologue
    .line 66
    invoke-static {p0}, Lcom/microsoft/xbox/presentation/lce/DefaultLceViewStateLogger;->toLogString(Lcom/microsoft/xbox/presentation/lce/LceViewState;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private stateReducer(Lcom/microsoft/xbox/presentation/lce/LceViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .locals 2
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;>;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;",
            ")",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "previousState":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;>;"
    instance-of v1, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 77
    check-cast v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    .line 79
    .local v0, "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;>;"
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 80
    iget-object v1, v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->content:Ljava/lang/Object;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->withContent(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object p1

    .line 88
    .end local v0    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;>;"
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;>;"
    :cond_0
    :goto_0
    return-object p1

    .line 81
    .restart local v0    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;>;"
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;>;"
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->isFailure()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 82
    iget-object v1, v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->error:Ljava/lang/Throwable;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object p1

    goto :goto_0

    .line 84
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method protected initialSetup()V
    .locals 3

    .prologue
    .line 61
    invoke-static {}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    move-result-object v1

    .line 62
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->bindViewIntents(Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 63
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 64
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 65
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 66
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 67
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 69
    .local v0, "viewStates":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;>;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->bindViewState(Lio/reactivex/Observable;Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;)V

    .line 70
    return-void
.end method
