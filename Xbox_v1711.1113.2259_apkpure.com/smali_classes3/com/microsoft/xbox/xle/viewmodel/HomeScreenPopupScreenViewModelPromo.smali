.class public Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;
.super Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;
.source "HomeScreenPopupScreenViewModelPromo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;",
        ">;"
    }
.end annotation


# instance fields
.field private final data:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;

.field private final headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;",
            ">;"
        }
    .end annotation
.end field

.field private final launchUri:Ljava/net/URI;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;Ljava/net/URI;)V
    .locals 2
    .param p1, "data"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;
    .param p2, "launchUri"    # Ljava/net/URI;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;-><init>()V

    .line 63
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;->headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;->data:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;

    .line 22
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;->launchUri:Ljava/net/URI;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 24
    .local v0, "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_XBOX_ONE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_LOCAL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;->commands:Ljava/util/ArrayList;

    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;->createScreenAdapter()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;->updateView()V

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;->data:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;

    return-object v0
.end method

.method private updateView()V
    .locals 0

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;->updateAdapter()V

    .line 61
    return-void
.end method


# virtual methods
.method public createScreenAdapter()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->createPromoScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderData()Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;->headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    return-object v0
.end method

.method public handleCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V
    .locals 4
    .param p1, "cmd"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .prologue
    const/4 v3, 0x0

    .line 38
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v0

    .line 39
    .local v0, "popup":Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$2;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$HomeScreenPopupScreenViewModelBase$Command:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 52
    :goto_0
    return-void

    .line 41
    :pswitch_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->hide()V

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    const-string v2, "TODO: hookup PLAY_TO_XBOX_ONE"

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(Ljava/lang/String;I)V

    goto :goto_0

    .line 45
    :pswitch_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->hide()V

    .line 46
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    const-string v2, "TODO: hookup PLAY_LOCAL"

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(Ljava/lang/String;I)V

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
