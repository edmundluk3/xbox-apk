.class public final Lcom/microsoft/xbox/toolkit/Build;
.super Ljava/lang/Object;
.source "Build.java"


# static fields
.field public static final AllowFeedback:Z = false

.field public static final AllowSwitchEnv:Z = true

.field public static final DisplayOwnerContent:Z = false

.field public static final EnableEPG:Z = true

.field public static final EnablePhoneInvitation:Z = false

.field public static EnableSkypeLongPoll:Z = false

.field public static final EnableTV:Z = true

.field public static final EnableURC:Z = true

.field public static final EnableViewServer:Z = true

.field public static final FileCacheEnabled:Z = true

.field public static IncludePurchaseFlow:Z = false

.field public static InjectRandomDelay:Z = false

.field public static final IsDebug:Z = false

.field public static final OptimizePivotLoad:Z = false

.field public static final ShowLikes:Z = true

.field public static final ShowMediaProgressTimerData:Z = false

.field public static final ShowTouchFrames:Z = false

.field public static TestNetworkDisconnectivity:Z = false

.field public static final TestTextInputWithDSGTitleTester:Z = false

.field public static final TrackPerf:Z = false

.field public static final UseDefaultTVSetting:Z = true

.field public static final UseDefaultURCSetting:Z = true

.field public static final UseSDForFileCache:Z = false

.field public static final defaultDebugSandbox:Ljava/lang/String; = "RETAIL"

.field public static final isBeta:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 20
    const-string v0, "beta"

    const-string v3, "retail"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "internalbeta"

    const-string v3, "retail"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    .line 41
    sput-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->InjectRandomDelay:Z

    .line 43
    sput-boolean v2, Lcom/microsoft/xbox/toolkit/Build;->IncludePurchaseFlow:Z

    .line 45
    sput-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->EnableSkypeLongPoll:Z

    .line 49
    sput-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->TestNetworkDisconnectivity:Z

    return-void

    :cond_1
    move v0, v1

    .line 20
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
