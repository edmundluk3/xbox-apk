.class Lcom/microsoft/playready/networkdevice/PrndDrmProxy$1;
.super Ljava/lang/Object;
.source "PrndDrmProxy.java"

# interfaces
.implements Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/playready/networkdevice/PrndDrmProxy;-><init>(Lcom/microsoft/playready/networkdevice/IPRNDFactory;Lcom/microsoft/playready/networkdevice/IReceiverSession;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;


# direct methods
.method constructor <init>(Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy$1;->this$0:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;)Z
    .locals 17
    .param p1, "arg1"    # Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;

    .prologue
    .line 56
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy$1;->this$0:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-static {v2}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->access$000(Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)Lcom/microsoft/playready/networkdevice/IReceiverSession;

    move-result-object v2

    invoke-interface {v2}, Lcom/microsoft/playready/networkdevice/IReceiverSession;->getReceiverPlugin()Lcom/microsoft/playready/networkdevice/IReceiverPlugin;

    move-result-object v16

    .line 58
    .local v16, "plugin":Lcom/microsoft/playready/networkdevice/IReceiverPlugin;
    if-eqz v16, :cond_3

    .line 60
    const/4 v14, 0x0

    .line 61
    .local v14, "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    const/4 v1, 0x0

    .line 62
    .local v1, "cert":Lcom/microsoft/playready/networkdevice/TransmitterCertificate;
    const/4 v13, 0x0

    .line 64
    .local v13, "authData":Lcom/microsoft/playready/networkdevice/TransmitterAuthenticationData;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    if-eqz v2, :cond_1

    .line 66
    const/4 v7, 0x0

    .line 68
    .local v7, "supportedFeatures":[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget-object v2, v2, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField4:[I

    if-eqz v2, :cond_0

    .line 70
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget-object v2, v2, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField4:[I

    array-length v2, v2

    new-array v7, v2, [Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    .line 72
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    array-length v2, v7

    if-ge v15, v2, :cond_0

    .line 74
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget-object v2, v2, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField4:[I

    aget v2, v2, v15

    invoke-static {v2}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->fromValue(I)Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    move-result-object v2

    aput-object v2, v7, v15

    .line 72
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 78
    .end local v15    # "i":I
    :cond_0
    new-instance v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;

    .end local v1    # "cert":Lcom/microsoft/playready/networkdevice/TransmitterCertificate;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget v2, v2, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField1:I

    invoke-static {v2}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->fromValue(I)Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget v3, v3, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField2:I

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget v4, v4, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField3:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget v5, v5, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField10:I

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget v6, v6, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField11:I

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget-object v8, v8, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField5:[B

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget-object v9, v9, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField6:[B

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget-object v10, v10, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField7:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget-object v11, v11, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField8:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField1:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;

    iget-object v12, v12, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;->mField9:Ljava/lang/String;

    invoke-direct/range {v1 .. v12}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;IIII[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;[B[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .end local v7    # "supportedFeatures":[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;
    .restart local v1    # "cert":Lcom/microsoft/playready/networkdevice/TransmitterCertificate;
    :cond_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField2:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;

    if-eqz v2, :cond_2

    .line 94
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField2:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;

    iget-object v2, v2, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;->mField1:[B

    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField2:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;

    iget-object v2, v2, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;->mField2:[B

    if-eqz v2, :cond_2

    .line 96
    new-instance v14, Lcom/microsoft/playready/networkdevice/CustomData;

    .end local v14    # "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField2:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;

    iget-object v2, v2, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;->mField1:[B

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;->mField2:Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;

    iget-object v3, v3, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;->mField2:[B

    invoke-direct {v14, v2, v3}, Lcom/microsoft/playready/networkdevice/CustomData;-><init>([B[B)V

    .line 100
    .restart local v14    # "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    :cond_2
    new-instance v13, Lcom/microsoft/playready/networkdevice/TransmitterAuthenticationData;

    .end local v13    # "authData":Lcom/microsoft/playready/networkdevice/TransmitterAuthenticationData;
    invoke-direct {v13, v1, v14}, Lcom/microsoft/playready/networkdevice/TransmitterAuthenticationData;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterCertificate;Lcom/microsoft/playready/networkdevice/CustomData;)V

    .line 102
    .restart local v13    # "authData":Lcom/microsoft/playready/networkdevice/TransmitterAuthenticationData;
    move-object/from16 v0, v16

    invoke-interface {v0, v13}, Lcom/microsoft/playready/networkdevice/IReceiverPlugin;->onAuthenticateTransmitter(Lcom/microsoft/playready/networkdevice/TransmitterAuthenticationData;)Z

    move-result v2

    .line 106
    .end local v1    # "cert":Lcom/microsoft/playready/networkdevice/TransmitterCertificate;
    .end local v13    # "authData":Lcom/microsoft/playready/networkdevice/TransmitterAuthenticationData;
    .end local v14    # "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    :goto_1
    return v2

    :cond_3
    const/4 v2, 0x1

    goto :goto_1
.end method
