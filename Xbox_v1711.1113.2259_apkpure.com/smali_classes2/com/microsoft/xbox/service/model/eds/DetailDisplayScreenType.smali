.class public final enum Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;
.super Ljava/lang/Enum;
.source "DetailDisplayScreenType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum ActivityDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum AlbumDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum AlbumDetailsFromTrack:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum AppDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum GameContentDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum GameDemoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum GameDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum MusicArtistDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum MusicVideoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum TvEpisodeDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum TvSeasonDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum TvSeriesDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum TvShowDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

.field public static final enum VideoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "GameDetails"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->GameDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 6
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "GameDemoDetails"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->GameDemoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 7
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "GameContentDetails"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->GameContentDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 8
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "VideoDetails"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->VideoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 9
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "TvEpisodeDetails"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvEpisodeDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 10
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "TvShowDetails"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvShowDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 11
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "AlbumDetails"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->AlbumDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 12
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "AlbumDetailsFromTrack"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->AlbumDetailsFromTrack:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 13
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "MusicArtistDetails"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->MusicArtistDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "MusicVideoDetails"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->MusicVideoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "AppDetails"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->AppDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "TvSeriesDetails"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvSeriesDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "TvSeasonDetails"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvSeasonDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "ActivityDetails"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->ActivityDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    const-string v1, "Unknown"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->Unknown:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 4
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    sget-object v1, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->GameDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->GameDemoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->GameContentDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->VideoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvEpisodeDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvShowDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->AlbumDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->AlbumDetailsFromTrack:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->MusicArtistDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->MusicVideoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->AppDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvSeriesDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvSeasonDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->ActivityDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->Unknown:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->$VALUES:[Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4
    const-class v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->$VALUES:[Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    return-object v0
.end method
