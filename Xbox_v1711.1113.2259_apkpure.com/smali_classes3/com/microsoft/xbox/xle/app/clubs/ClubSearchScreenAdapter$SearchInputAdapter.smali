.class Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ClubSearchScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchInputAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final filter:Landroid/widget/Filter;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 391
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 393
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->setNotifyOnChange(Z)V

    .line 396
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter$1;

    invoke-direct {v0, p0, p3}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;Ljava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->filter:Landroid/widget/Filter;

    .line 432
    return-void
.end method


# virtual methods
.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->filter:Landroid/widget/Filter;

    return-object v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 436
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 437
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->setNotifyOnChange(Z)V

    .line 438
    return-void
.end method
