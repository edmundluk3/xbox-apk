.class final Lcom/microsoft/xbox/xle/app/LaunchUtils$11;
.super Ljava/lang/Object;
.source "LaunchUtils.java"

# interfaces
.implements Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/LaunchUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountAcquired(Lcom/microsoft/xbox/idp/jobs/MSAJob;Lcom/microsoft/onlineid/UserAccount;)V
    .locals 2
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;
    .param p2, "userAccount"    # Lcom/microsoft/onlineid/UserAccount;

    .prologue
    .line 654
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "silentSignInCallbacks.onUiNeeded - Acceptable NO-OP."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    return-void
.end method

.method public onFailure(Lcom/microsoft/xbox/idp/jobs/MSAJob;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 629
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "silentSignInCallbacks.onFailure - We should never be here."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$300(Ljava/lang/String;)V

    .line 633
    return-void
.end method

.method public onSignedOut(Lcom/microsoft/xbox/idp/jobs/MSAJob;)V
    .locals 2
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;

    .prologue
    .line 645
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "silentSignInCallbacks.onSignedOut - We should never be here."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$300(Ljava/lang/String;)V

    .line 649
    return-void
.end method

.method public onTicketAcquired(Lcom/microsoft/xbox/idp/jobs/MSAJob;Lcom/microsoft/onlineid/Ticket;)V
    .locals 2
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;
    .param p2, "ticket"    # Lcom/microsoft/onlineid/Ticket;

    .prologue
    .line 659
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "silentSignInCallbacks.onTicketAcquired"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    invoke-virtual {p2}, Lcom/microsoft/onlineid/Ticket;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$300(Ljava/lang/String;)V

    .line 661
    return-void
.end method

.method public onUiNeeded(Lcom/microsoft/xbox/idp/jobs/MSAJob;)V
    .locals 2
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;

    .prologue
    .line 621
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "silentSignInCallbacks.onUiNeeded - We should never be here."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$300(Ljava/lang/String;)V

    .line 625
    return-void
.end method

.method public onUserCancel(Lcom/microsoft/xbox/idp/jobs/MSAJob;)V
    .locals 2
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;

    .prologue
    .line 637
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "silentSignInCallbacks.onUserCancel - We should never be here."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$300(Ljava/lang/String;)V

    .line 641
    return-void
.end method
