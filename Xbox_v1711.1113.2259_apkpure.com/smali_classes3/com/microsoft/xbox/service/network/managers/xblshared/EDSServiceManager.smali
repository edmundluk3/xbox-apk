.class public Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;
.super Ljava/lang/Object;
.source "EDSServiceManager.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;


# static fields
.field private static final BROWSE_QUERY_FORMAT_BASE:Ljava/lang/String; = "/media/en-US/browse?orderBy=%s&maxItems=25&skipItems=%d&"

.field private static final BROWSE_QUERY_FORMAT_DESIRED:Ljava/lang/String; = "desiredMediaItemTypes=%s&subscriptionLevel=%s"

.field private static final BROWSE_QUERY_ID_FORMAT:Ljava/lang/String; = "id=%s&MediaItemType=%s&"

.field private static final BRWOSE_QUERY_GENRE_FORMAT:Ljava/lang/String; = "genre=%s&"

.field private static final CONTINUATINO_TOKEN_PARAMTER:Ljava/lang/String; = "&continuationToken="

.field private static final DETAILS_QUERY_FORMAT:Ljava/lang/String; = "/media/%s/details?ids=%s&%s&fields=all"

.field private static final DETAILS_QUERY_FORMAT_WITHOUT_IDTYPE:Ljava/lang/String; = "/media/%s/details?ids=%s&fields=all"

.field private static final DETAIL_MEDIA_GROUP_QUERY:Ljava/lang/String; = "&mediaGroup=%s"

.field private static final EDS_CANONICAL_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=Canonical"

.field private static final EDS_FILTERSTRING_ALL:Ljava/lang/String; = "XboxApp.Xbox360Game.XboxGameTrial.Xbox360GameContent.Xbox360GameDemo.XboxTheme.XboxOriginalGame.XboxGamerTile.XboxArcadeGame.XboxGameConsumable.XboxGameVideo.XboxGameTrailer.XboxXnaCommunityGame.Album.MusicVideo.MusicArtist.TVShow.TVSeries.Movie"

.field private static final EDS_FILTERSTRING_APP:Ljava/lang/String; = "XboxApp"

.field private static final EDS_FILTERSTRING_GAMES:Ljava/lang/String; = "Xbox360Game.XboxGameTrial.Xbox360GameContent.Xbox360GameDemo.XboxTheme.XboxOriginalGame.XboxGamerTile.XboxArcadeGame.XboxGameConsumable.XboxGameVideo.XboxGameTrailer.XboxXnaCommunityGame"

.field private static final EDS_FILTERSTRING_MOVIES:Ljava/lang/String; = "Movie"

.field private static final EDS_FILTERSTRING_MUSIC:Ljava/lang/String; = "Album.MusicVideo.MusicArtist"

.field private static final EDS_FILTERSTRING_MUSICALBUM:Ljava/lang/String; = "Album"

.field private static final EDS_FILTERSTRING_MUSICALL:Ljava/lang/String; = "Album.Track.MusicArtist"

.field private static final EDS_FILTERSTRING_MUSICARTIST:Ljava/lang/String; = "MusicArtist"

.field private static final EDS_FILTERSTRING_MUSICTRACK:Ljava/lang/String; = "Track"

.field private static final EDS_FILTERSTRING_SEPARATOR:Ljava/lang/String; = "."

.field private static final EDS_FILTERSTRING_TV:Ljava/lang/String; = "TVShow.TVSeries"

.field private static final EDS_PARTNER_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=ProviderContentId"

.field private static final EDS_TITLE_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=XboxHexTitle"

.field private static final EDS_XBOXONE_FILTERSTRING_ACTIVITY:Ljava/lang/String; = "DActivity"

.field private static final EDS_XBOXONE_FILTERSTRING_ALL:Ljava/lang/String; = "DApp.DNativeApp.DGame.DConsumable.DDurable.DActivity.Album.MusicVideo.MusicArtist.TVShow.TVSeries.Movie"

.field private static final EDS_XBOXONE_FILTERSTRING_APP:Ljava/lang/String; = "DApp.DNativeApp"

.field private static final EDS_XBOXONE_FILTERSTRING_GAMES:Ljava/lang/String; = "DGame.DConsumable.DDurable"

.field private static final EDS_XBOXONE_MUSIC_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=ZuneCatalog&desiredMediaItemTypes=MusicVideo.MusicArtist.Track.Album"

.field private static final EDS_XBOXONE_TITLE_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=XboxHexTitle&desiredMediaItemTypes=DGame.DGameDemo.DApp"

.field private static final EDS_XBOXONE_VIDEO_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=ZuneCatalog&desiredMediaItemTypes=Movie.TvShow.TvSeries.TvEpisode.TvSeason"

.field private static final EDS_ZUNE_IDTYPE_QUERY_STRING:Ljava/lang/String; = "idType=ZuneCatalog"

.field private static final FIELD_TOKEN_QUERY:Ljava/lang/String; = "/media/%s/fields?desired=id.name.description.releasedate.images.primaryartist.alltimeratingcount.alltimeaveragerating.hasactivities.titleid.tracknumber.duration"

.field private static final MAX_COUNT:I = 0x19

.field private static final RECOMMEND_QUERY_FORMAT:Ljava/lang/String; = "/media/%s/recommendations?desiredMediaItemType=%s"

.field private static final RELATED_QUERY_FORMAT:Ljava/lang/String; = "/media/%s/related?id=%s&desiredMediaItemTypes=%s&mediaItemType=%s&firstPartyOnly=%s"

.field private static final SEARCH_FIELDS_PARAMS:Ljava/lang/String; = "&fields="

.field private static final SEARCH_QUERY_FORMAT:Ljava/lang/String; = "/media/%s/crossMediaGroupSearch?maxItems=25&q=%s&DesiredMediaItemTypes=%s&subscriptionLevel=%s"

.field private static final XSTS_AUDIENCE:Ljava/lang/String; = "http://xboxlive.com"

.field private static searchFieldToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->searchFieldToken:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private browseMediaItemListInternal(Ljava/lang/String;IILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 9
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaType"    # I
    .param p3, "mediaItemType"    # I
    .param p4, "impressionGuid"    # Ljava/lang/String;
    .param p5, "orderBy"    # Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .param p6, "genre"    # Ljava/lang/String;
    .param p7, "skipItems"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 293
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 294
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 295
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 297
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "membership":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "/media/en-US/browse?orderBy=%s&maxItems=25&skipItems=%d&"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p5}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getOrderByString(Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 299
    .local v2, "searchUrl":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 300
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "id=%s&MediaItemType=%s&"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    invoke-static {p3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 303
    :cond_0
    if-eqz p6, :cond_1

    invoke-virtual {p6}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 304
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "genre=%s&"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p6, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 307
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "desiredMediaItemTypes=%s&subscriptionLevel=%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 309
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->getSearchFieldToken()Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "fieldsToken":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 311
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&fields="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 314
    :cond_2
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v3

    return-object v3
.end method

.method public static buildEDSImageUrl(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p0, "baseimageurl"    # Ljava/lang/String;
    .param p1, "dimenRValue"    # I

    .prologue
    .line 445
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 446
    .local v0, "px":I
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s?width=%d&height=%d&format=png"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static clearSearchFieldToken()V
    .locals 1

    .prologue
    .line 114
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 115
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->searchFieldToken:Ljava/lang/String;

    .line 116
    return-void
.end method

.method private doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 8
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 379
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->getEDSHeader()Ljava/util/ArrayList;

    move-result-object v1

    .line 380
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    const/4 v2, 0x0

    .line 381
    .local v2, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    const/4 v3, 0x0

    .line 382
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const-string v4, "EDSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requesting "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    :try_start_0
    invoke-static {p1, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 387
    iget v4, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v5, 0xc8

    if-ne v4, v5, :cond_1

    .line 388
    iget-object v4, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v3, :cond_0

    iget-object v4, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_0

    .line 410
    :try_start_1
    iget-object v4, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 419
    :cond_0
    :goto_0
    return-object v2

    .line 390
    :cond_1
    :try_start_2
    const-string v4, "EDSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "search failed for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xfa6

    invoke-direct {v4, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 395
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :catch_0
    move-exception v0

    .line 396
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v4, "EDSServiceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v4, :cond_2

    .line 399
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 400
    :catch_1
    move-exception v4

    .line 405
    :cond_2
    :try_start_5
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xfa6

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 408
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v3, :cond_3

    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 410
    :try_start_6
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .line 413
    :cond_3
    :goto_1
    throw v4

    .line 411
    :catch_2
    move-exception v5

    goto :goto_1

    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :catch_3
    move-exception v4

    goto :goto_0
.end method

.method private getEDS2bHeader()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 468
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 470
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Cache-Control"

    const-string v3, "no-store, no-cache"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Pragma"

    const-string v3, "no-cache"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Accept"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 474
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "x-xbl-build-version"

    const-string v3, "current"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "x-xbl-client-type"

    const-string v3, "Companion"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 476
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "x-xbl-device-type"

    const-string v3, "AndroidPhone"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 477
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "x-xbl-contract-version"

    const-string v3, "2.b"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 478
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "x-xbl-client-version"

    const-string v3, "2.0"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 481
    return-object v0
.end method

.method private getEDS32RequestHeader()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 485
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 487
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Cache-Control"

    const-string v3, "no-store, no-cache"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 488
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Pragma"

    const-string v3, "no-cache"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 489
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Accept"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 492
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "x-xbl-build-version"

    const-string v3, "current"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "x-xbl-client-type"

    const-string v3, "Companion"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 494
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "x-xbl-device-type"

    const-string v3, "AndroidPhone"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 495
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "x-xbl-contract-version"

    const-string v3, "3.2"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "x-xbl-client-version"

    const-string v3, "2.0"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 498
    return-object v0
.end method

.method private getEDSFilterString(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)Ljava/lang/String;
    .locals 2
    .param p1, "filter"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .prologue
    .line 502
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager$3;->$SwitchMap$com$microsoft$xbox$service$model$edsv2$EDSV2SearchFilterType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 532
    const-string v0, "XboxApp.Xbox360Game.XboxGameTrial.Xbox360GameContent.Xbox360GameDemo.XboxTheme.XboxOriginalGame.XboxGamerTile.XboxArcadeGame.XboxGameConsumable.XboxGameVideo.XboxGameTrailer.XboxXnaCommunityGame.Album.MusicVideo.MusicArtist.TVShow.TVSeries.Movie"

    :goto_0
    return-object v0

    .line 504
    :pswitch_0
    const-string v0, "XboxApp"

    goto :goto_0

    .line 507
    :pswitch_1
    const-string v0, "Xbox360Game.XboxGameTrial.Xbox360GameContent.Xbox360GameDemo.XboxTheme.XboxOriginalGame.XboxGamerTile.XboxArcadeGame.XboxGameConsumable.XboxGameVideo.XboxGameTrailer.XboxXnaCommunityGame"

    goto :goto_0

    .line 510
    :pswitch_2
    const-string v0, "Album.MusicVideo.MusicArtist"

    goto :goto_0

    .line 513
    :pswitch_3
    const-string v0, "TVShow.TVSeries"

    goto :goto_0

    .line 516
    :pswitch_4
    const-string v0, "Movie"

    goto :goto_0

    .line 519
    :pswitch_5
    const-string v0, "MusicArtist"

    goto :goto_0

    .line 522
    :pswitch_6
    const-string v0, "Album"

    goto :goto_0

    .line 525
    :pswitch_7
    const-string v0, "Track"

    goto :goto_0

    .line 528
    :pswitch_8
    const-string v0, "Album.Track.MusicArtist"

    goto :goto_0

    .line 502
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private getEDSHeader()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 460
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getIsForXboxOne()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->getEDS32RequestHeader()Ljava/util/ArrayList;

    move-result-object v0

    .line 463
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->getEDS2bHeader()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public static prefetchSearchFieldsToken()V
    .locals 3

    .prologue
    .line 119
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 120
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager$2;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager$1;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager$1;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager$2;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 130
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    .line 132
    return-void
.end method


# virtual methods
.method public browseMediaItemList(Ljava/lang/String;IILjava/lang/String;I)Ljava/util/ArrayList;
    .locals 9
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaType"    # I
    .param p3, "mediaItemType"    # I
    .param p4, "impressionGuid"    # Ljava/lang/String;
    .param p5, "maxvalue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 255
    sget-object v5, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_RELEASEDATE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v8, p5

    invoke-virtual/range {v0 .. v8}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->browseMediaItemList(Ljava/lang/String;IILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public browseMediaItemList(Ljava/lang/String;IILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 13
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaType"    # I
    .param p3, "mediaItemType"    # I
    .param p4, "impressionGuid"    # Ljava/lang/String;
    .param p5, "orderBy"    # Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .param p6, "genre"    # Ljava/lang/String;
    .param p7, "skipItems"    # I
    .param p8, "maxvalue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 262
    const/4 v11, 0x0

    .line 263
    .local v11, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    const/4 v9, 0x0

    .line 264
    .local v9, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    move/from16 v8, p7

    .line 265
    .local v8, "skip":I
    const/4 v12, 0x0

    .line 267
    .local v12, "total":I
    :goto_0
    move/from16 v0, p8

    if-ge v12, v0, :cond_2

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    .line 269
    invoke-direct/range {v1 .. v8}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->browseMediaItemListInternal(Ljava/lang/String;IILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v11

    .line 270
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 271
    if-nez v9, :cond_0

    .line 272
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 275
    .restart local v9    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    :cond_0
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v10, v1, :cond_1

    .line 276
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 280
    .end local v10    # "i":I
    :cond_1
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/16 v2, 0x19

    if-ge v1, v2, :cond_3

    .line 288
    :cond_2
    return-object v9

    .line 284
    :cond_3
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v12, v1

    .line 285
    add-int/lit8 v8, v8, 0x19

    goto :goto_0
.end method

.method public browseMediaItemList(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaTypeString"    # Ljava/lang/String;
    .param p3, "mediaItemType"    # I
    .param p4, "impressionGuid"    # Ljava/lang/String;
    .param p5, "orderBy"    # Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .param p6, "genre"    # Ljava/lang/String;
    .param p7, "skipItems"    # I
    .param p8, "maxvalue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 589
    const/4 v0, 0x0

    return-object v0
.end method

.method public browseSnappableAppList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">()",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 576
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBundles(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "mediaItemType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 355
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCombinedContentRating()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 367
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    .line 368
    .local v1, "legalLocale":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v2

    .line 369
    .local v2, "subscriptionLevel":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getAllowExplicitContent()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    .line 371
    .local v0, "isAdultAccount":Z
    :goto_0
    invoke-virtual {p0, v1, v2, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->getCombinedContentRating(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 369
    .end local v0    # "isAdultAccount":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCombinedContentRating(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p1, "legalLocale"    # Ljava/lang/String;
    .param p2, "subscriptionLevel"    # Ljava/lang/String;
    .param p3, "isAdultAccount"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 375
    const-string v0, ""

    return-object v0
.end method

.method public getFutureShowtimes(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "headendIdList"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 359
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGameDlc()Ljava/lang/String;
    .locals 9

    .prologue
    .line 539
    const-string v5, "https://eds.dnet.xboxlive.com/media/en-us/browse?orderBy=releaseDate&desiredMediaItemTypes=DDurable.DConsumable&fields=all&mediaItemType=DGame&id=6c5402e4-3cd5-4b29-a9c4-bec7d2c7514a"

    .line 541
    .local v5, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 542
    .local v2, "result":Ljava/lang/String;
    const/4 v4, 0x0

    .line 544
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->getEDS32RequestHeader()Ljava/util/ArrayList;

    move-result-object v1

    .line 545
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    const-string v6, "https://eds.dnet.xboxlive.com/media/en-us/browse?orderBy=releaseDate&desiredMediaItemTypes=DDurable.DConsumable&fields=all&mediaItemType=DGame&id=6c5402e4-3cd5-4b29-a9c4-bec7d2c7514a"

    invoke-static {v6, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 546
    iget v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_1

    .line 547
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 548
    const-string v6, "Game DLC from EDS"

    invoke-static {v6, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 557
    :goto_0
    if-eqz v4, :cond_0

    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_0

    .line 559
    :try_start_1
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :goto_1
    move-object v3, v2

    .line 563
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Ljava/lang/String;
    .local v3, "result":Ljava/lang/String;
    :goto_2
    return-object v3

    .line 550
    .end local v3    # "result":Ljava/lang/String;
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Ljava/lang/String;
    :cond_1
    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusLine:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\nDid you log in with the right account?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 551
    const-string v6, "Game DLC from EDS"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 554
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    :catch_0
    move-exception v0

    .line 555
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v6, "edsGetGameDlc"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 557
    if-eqz v4, :cond_2

    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_2

    .line 559
    :try_start_4
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :cond_2
    :goto_3
    move-object v3, v2

    .line 563
    .end local v2    # "result":Ljava/lang/String;
    .restart local v3    # "result":Ljava/lang/String;
    goto :goto_2

    .line 557
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "result":Ljava/lang/String;
    .restart local v2    # "result":Ljava/lang/String;
    :catchall_0
    move-exception v6

    if-eqz v4, :cond_3

    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_3

    .line 559
    :try_start_5
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :cond_3
    :goto_4
    move-object v3, v2

    .line 563
    .end local v2    # "result":Ljava/lang/String;
    .restart local v3    # "result":Ljava/lang/String;
    goto :goto_2

    .line 560
    .end local v3    # "result":Ljava/lang/String;
    .restart local v2    # "result":Ljava/lang/String;
    :catch_1
    move-exception v6

    goto :goto_4

    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v6

    goto :goto_3

    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    :catch_3
    move-exception v6

    goto :goto_1
.end method

.method public getGenreList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "mediaType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 456
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIncludedContent(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "mediaItemType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 351
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItemDetail(Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 11
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "partnerMediaId"    # Ljava/lang/String;
    .param p3, "titleId"    # J
    .param p5, "mediaGroup"    # I
    .param p6, "impressionGuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JI",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 195
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 196
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 197
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v6

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 199
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v2

    .line 200
    .local v2, "locale":Ljava/lang/String;
    const/4 v5, 0x0

    .line 201
    .local v5, "searchUrl":Ljava/lang/String;
    const/4 v1, 0x0

    .line 202
    .local v1, "idTypeString":Ljava/lang/String;
    const/4 v0, 0x0

    .line 203
    .local v0, "id":Ljava/lang/String;
    const/4 v3, 0x0

    .line 205
    .local v3, "mediaGroupString":Ljava/lang/String;
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 206
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v6

    if-nez v6, :cond_0

    .line 207
    const-string v1, "idType=Canonical"

    .line 209
    :cond_0
    move-object v0, p1

    .line 210
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "&mediaGroup=%s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static/range {p5 .. p5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaGroup;->getGroupStringName(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 235
    :goto_0
    if-nez v1, :cond_9

    .line 236
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "/media/%s/details?ids=%s&fields=all"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v2, v9, v10

    const/4 v10, 0x1

    aput-object v0, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 240
    :goto_1
    if-eqz v3, :cond_1

    .line 241
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 244
    :cond_1
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v4

    .line 245
    .local v4, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v4, :cond_a

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_a

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_a

    .line 246
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v6

    .line 211
    .end local v4    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_2
    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_6

    .line 212
    sget-object v6, Lcom/microsoft/xbox/toolkit/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {p2, v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->ensureUrlEncoding(Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    .line 214
    const-wide/32 v6, 0x5848085b

    cmp-long v6, p3, v6

    if-nez v6, :cond_3

    .line 215
    const-string v1, "idType=ZuneCatalog"

    goto :goto_0

    .line 216
    :cond_3
    const-wide/32 v6, 0x18ffc9f4

    cmp-long v6, p3, v6

    if-nez v6, :cond_4

    .line 217
    const-string v1, "idType=ZuneCatalog&desiredMediaItemTypes=MusicVideo.MusicArtist.Track.Album"

    goto/16 :goto_0

    .line 218
    :cond_4
    const-wide/32 v6, 0x3d705025

    cmp-long v6, p3, v6

    if-nez v6, :cond_5

    .line 219
    const-string v1, "idType=ZuneCatalog&desiredMediaItemTypes=Movie.TvShow.TvSeries.TvEpisode.TvSeason"

    goto/16 :goto_0

    .line 221
    :cond_5
    const-string v1, "idType=ProviderContentId"

    goto/16 :goto_0

    .line 223
    :cond_6
    const-wide/16 v6, 0x0

    cmp-long v6, p3, v6

    if-eqz v6, :cond_8

    .line 225
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p3, p4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 227
    const-string v1, "idType=XboxHexTitle&desiredMediaItemTypes=DGame.DGameDemo.DApp"

    goto/16 :goto_0

    .line 229
    :cond_7
    const-string v1, "idType=XboxHexTitle"

    goto/16 :goto_0

    .line 232
    :cond_8
    const/4 v6, 0x0

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    goto/16 :goto_0

    .line 238
    :cond_9
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "/media/%s/details?ids=%s&%s&fields=all"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v2, v9, v10

    const/4 v10, 0x1

    aput-object v0, v9, v10

    const/4 v10, 0x2

    aput-object v1, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 249
    .restart local v4    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_a
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xfae

    invoke-direct {v6, v8, v9}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v6
.end method

.method public getProgrammingItems2()Lcom/microsoft/xbox/service/model/edsv2/EDSV2DiscoverData;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 183
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 184
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getCombinedContentRating()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 189
    :cond_0
    return-object v1
.end method

.method public getRelated(Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 14
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaType"    # I
    .param p3, "mediaItemType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 320
    const/4 v3, 0x0

    .line 321
    .local v3, "firstPartyOnly":Z
    const/4 v8, 0x1

    move/from16 v0, p3

    if-eq v0, v8, :cond_0

    const/16 v8, 0x17

    move/from16 v0, p3

    if-ne v0, v8, :cond_1

    .line 322
    :cond_0
    const/4 v3, 0x1

    .line 325
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v8

    if-nez v8, :cond_3

    .line 326
    const/4 v1, 0x0

    .line 347
    :cond_2
    return-object v1

    .line 329
    :cond_3
    const/4 v1, 0x0

    .line 330
    .local v1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    .line 331
    .local v5, "locale":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "/media/%s/related?id=%s&desiredMediaItemTypes=%s&mediaItemType=%s&firstPartyOnly=%s"

    const/4 v11, 0x5

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v5, v11, v12

    const/4 v12, 0x1

    aput-object p1, v11, v12

    const/4 v12, 0x2

    .line 332
    invoke-static/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x3

    invoke-static/range {p3 .. p3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x4

    .line 333
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    .line 332
    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 335
    .local v7, "searchUrl":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->getSearchFieldToken()Ljava/lang/String;

    move-result-object v2

    .line 336
    .local v2, "fieldsToken":Ljava/lang/String;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_4

    .line 337
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "&fields="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 340
    :cond_4
    invoke-direct {p0, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v6

    .line 341
    .local v6, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 342
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 343
    .restart local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v4, v8, :cond_2

    .line 344
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getSearchFieldToken()Ljava/lang/String;
    .locals 12

    .prologue
    .line 136
    sget-object v7, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->searchFieldToken:Ljava/lang/String;

    if-eqz v7, :cond_0

    sget-object v7, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->searchFieldToken:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 137
    sget-object v7, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->searchFieldToken:Ljava/lang/String;

    .line 179
    :goto_0
    return-object v7

    .line 140
    :cond_0
    const-string v7, "EDSServiceManager"

    const-string v8, "search fields token does not exist, refresh. "

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v3

    .line 142
    .local v3, "locale":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "/media/%s/fields?desired=id.name.description.releasedate.images.primaryartist.alltimeratingcount.alltimeaveragerating.hasactivities.titleid.tracknumber.duration"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v3, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 144
    .local v6, "url":Ljava/lang/String;
    const/4 v4, 0x0

    .line 145
    .local v4, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2FieldsToken;
    const/4 v5, 0x0

    .line 147
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->getEDSHeader()Ljava/util/ArrayList;

    move-result-object v2

    .line 149
    .local v2, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {v6, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v5

    .line 150
    iget v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v8, 0xc8

    if-ne v7, v8, :cond_3

    .line 151
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v8, Lcom/microsoft/xbox/service/model/edsv2/EDSV2FieldsToken;

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2FieldsToken;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    :goto_1
    if-eqz v5, :cond_1

    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_1

    .line 169
    :try_start_1
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 176
    .end local v2    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    :cond_1
    :goto_2
    if-eqz v4, :cond_2

    iget-object v7, v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2FieldsToken;->Fields:Ljava/lang/String;

    if-eqz v7, :cond_2

    .line 177
    iget-object v7, v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2FieldsToken;->Fields:Ljava/lang/String;

    sput-object v7, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->searchFieldToken:Ljava/lang/String;

    .line 179
    :cond_2
    sget-object v7, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->searchFieldToken:Ljava/lang/String;

    goto :goto_0

    .line 153
    .restart local v2    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    :cond_3
    :try_start_2
    const-string v7, "EDSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to get field token. "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 156
    .end local v2    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    :catch_0
    move-exception v1

    .line 157
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v7, "EDSServiceManager"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    instance-of v7, v1, Lcom/microsoft/xbox/toolkit/XLEException;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v7, :cond_4

    .line 160
    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 161
    :catch_1
    move-exception v7

    .line 167
    :cond_4
    if-eqz v5, :cond_1

    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_1

    .line 169
    :try_start_5
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 170
    :catch_2
    move-exception v7

    goto :goto_2

    .line 167
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v5, :cond_5

    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v8, :cond_5

    .line 169
    :try_start_6
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 172
    :cond_5
    :goto_3
    throw v7

    .line 170
    :catch_3
    move-exception v8

    goto :goto_3

    .restart local v2    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    :catch_4
    move-exception v7

    goto :goto_2
.end method

.method public getSeriesFromSeaonId(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 595
    .local p1, "Ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSmartDJ(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SmartDJResult;
    .locals 1
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 451
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStoreItems(Lcom/microsoft/xbox/service/model/StoreBrowseType;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 1
    .param p1, "searchType"    # Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .param p3, "skipItems"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 600
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitleImage(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 570
    .local p1, "titleId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitleImageFromId(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 582
    .local p1, "Ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public initializeServiceManager()V
    .locals 0

    .prologue
    .line 108
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->clearSearchFieldToken()V

    .line 109
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->prefetchSearchFieldsToken()V

    .line 111
    return-void
.end method

.method public searchMediaItems(Ljava/lang/String;ILjava/lang/String;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 11
    .param p1, "searchTerm"    # Ljava/lang/String;
    .param p2, "filter"    # I
    .param p3, "continuationToken"    # Ljava/lang/String;
    .param p4, "resultsPerPage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 423
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 424
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 426
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v2

    .line 427
    .local v2, "locale":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v3

    .line 428
    .local v3, "membership":Ljava/lang/String;
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->forValue(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    move-result-object v1

    .line 431
    .local v1, "filterType":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEDSUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "/media/%s/crossMediaGroupSearch?maxItems=25&q=%s&DesiredMediaItemTypes=%s&subscriptionLevel=%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->getEDSFilterString(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 432
    .local v4, "searchUrl":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->getSearchFieldToken()Ljava/lang/String;

    move-result-object v0

    .line 433
    .local v0, "fieldsToken":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 434
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&fields="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 436
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 437
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&continuationToken="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 440
    :cond_1
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManager;->doSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v5

    return-object v5
.end method

.method public setCombinedContentRating(Ljava/lang/String;)V
    .locals 0
    .param p1, "combinedContentRating"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 363
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 364
    return-void
.end method
