.class public final enum Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;
.super Ljava/lang/Enum;
.source "UTCPeopleHub.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FriendsListItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

.field public static final enum Block:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

.field public static final enum BlockedUser:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

.field public static final enum Follower:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

.field public static final enum Friend:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

.field public static final enum Game:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 77
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    .line 78
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    const-string v1, "Friend"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->Friend:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    .line 79
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    const-string v1, "Follower"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->Follower:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    .line 80
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    const-string v1, "Game"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->Game:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    .line 81
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    const-string v1, "BlockedUser"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->BlockedUser:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    .line 82
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    const-string v1, "Block"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->Block:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    .line 76
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->Friend:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->Follower:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->Game:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->BlockedUser:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->Block:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 76
    const-class v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;

    return-object v0
.end method
