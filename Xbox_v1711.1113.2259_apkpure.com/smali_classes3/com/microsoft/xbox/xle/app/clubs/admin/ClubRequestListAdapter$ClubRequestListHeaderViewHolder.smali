.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "ClubRequestListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ClubRequestListHeaderViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private ignoreAll:Landroid/widget/Button;

.field private sendAllInvites:Landroid/widget/Button;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    .line 92
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 94
    const v0, 0x7f0e028f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->sendAllInvites:Landroid/widget/Button;

    .line 95
    const v0, 0x7f0e0290

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->ignoreAll:Landroid/widget/Button;

    .line 96
    return-void
.end method

.method static synthetic lambda$onBind$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->removeAllDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$onBind$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->removeAllDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method private removeAllDecorator(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 105
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->clear()V

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->notifyDataSetChanged()V

    .line 108
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;)V
    .locals 2
    .param p1, "dataObject"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->sendAllInvites:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->ignoreAll:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 87
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;)V

    return-void
.end method
