.class public Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;
.super Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;
.source "HomeScreenPopupScreenViewModelPins.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent",
        "<",
        "Lcom/microsoft/xbox/service/model/pins/PinItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation
.end field

.field private final pin:Lcom/microsoft/xbox/service/model/pins/PinItem;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    .line 32
    const/4 v1, 0x0

    new-array v1, v1, [Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;[Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 14
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;->headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    .line 33
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    .line 34
    .local v0, "gd":Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedPin()Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;)Lcom/microsoft/xbox/service/model/pins/PinItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;

    .prologue
    .line 10
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    return-object v0
.end method


# virtual methods
.method public createScreenAdapter()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->createPinScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderData()Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;->headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    return-object v0
.end method

.method protected getLaunchableItem()Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    return-object v0
.end method

.method protected getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/pins/PinItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaItemForDetails()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getIsTVSeason()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getParentSeriesItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    goto :goto_0
.end method
