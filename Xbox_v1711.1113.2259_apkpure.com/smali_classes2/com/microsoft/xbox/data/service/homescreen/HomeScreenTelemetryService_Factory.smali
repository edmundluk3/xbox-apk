.class public final Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService_Factory;
.super Ljava/lang/Object;
.source "HomeScreenTelemetryService_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;",
        ">;"
    }
.end annotation


# static fields
.field private static final INSTANCE:Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService_Factory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService_Factory;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService_Factory;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService_Factory;->INSTANCE:Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService_Factory;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    sget-object v0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService_Factory;->INSTANCE:Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService_Factory;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;-><init>()V

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService_Factory;->get()Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;

    move-result-object v0

    return-object v0
.end method
