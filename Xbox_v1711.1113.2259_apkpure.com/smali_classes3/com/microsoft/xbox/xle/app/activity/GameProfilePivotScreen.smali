.class public Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;
.super Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.source "GameProfilePivotScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string v0, "Game Hub"

    return-object v0
.end method

.method public onCreate()V
    .locals 8

    .prologue
    .line 19
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onCreate()V

    .line 21
    const v3, 0x7f030127

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;->setContentView(I)V

    .line 24
    const v3, 0x7f0e067b

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .local v1, "pivotWithTabs":Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    .line 25
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackHubPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    sget-object v6, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->TAB_NAMES:[Ljava/lang/String;

    sget-object v7, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->ACTION_NAMES:[Ljava/lang/String;

    invoke-virtual {v1, v3, v6, v7}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTelemetryTabSelectCallback(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 26
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackHubPageResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    sget-object v6, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->TAB_NAMES:[Ljava/lang/String;

    invoke-virtual {v1, v3, v6}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTelemetrySetActiveTabCallback(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;)V

    .line 27
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0c0149

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderBackgroundColor(I)V

    .line 28
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setSelectedTabIndicatorColorToProfileColor()V

    .line 30
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 32
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 35
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    .line 36
    .local v2, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v4

    .line 37
    .local v4, "titleId":J
    :goto_0
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 40
    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->setGameTitleId(J)V

    .line 43
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedScid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 45
    const-class v3, Lcom/microsoft/xbox/xle/app/activity/GameProfileLfgScreen;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;->setActivePivotPane(Ljava/lang/Class;)V

    .line 47
    :cond_1
    return-void

    .line 36
    .end local v4    # "titleId":J
    :cond_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v4

    goto :goto_0
.end method

.method public onCreateContentView()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onStart()V

    .line 56
    return-void
.end method
