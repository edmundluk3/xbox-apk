.class Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;
.super Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;
.source "SkypeConversationDetailsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OtherMessageViews"
.end annotation


# instance fields
.field private attachmentContainer:Landroid/view/ViewGroup;

.field private messageContainer:Landroid/view/View;

.field private otherNameHolder:Landroid/widget/LinearLayout;

.field private senderRealName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private senderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private sentTimeSizerView:Landroid/widget/TextView;

.field private sentTimeView:Landroid/widget/TextView;

.field private speechArrowGraphicsOther:Landroid/widget/ImageView;

.field private titleView:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;-><init>()V

    .line 295
    return-void
.end method

.method static synthetic access$000(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;
    .locals 1
    .param p0, "x0"    # Landroid/view/View;

    .prologue
    .line 283
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->create(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->titleView:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->sentTimeView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->sentTimeSizerView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->messageContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->senderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->otherNameHolder:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->senderRealName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->attachmentContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->speechArrowGraphicsOther:Landroid/widget/ImageView;

    return-object v0
.end method

.method private static create(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;
    .locals 2
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 299
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;-><init>()V

    .line 301
    .local v0, "result":Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;
    const v1, 0x7f0e04a0

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->titleView:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    .line 302
    const v1, 0x7f0e049c

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->senderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 303
    const v1, 0x7f0e049d

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->senderRealName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 304
    const v1, 0x7f0e04a2

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->sentTimeView:Landroid/widget/TextView;

    .line 305
    const v1, 0x7f0e0499

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->sentTimeSizerView:Landroid/widget/TextView;

    .line 306
    const v1, 0x7f0e049f

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->messageContainer:Landroid/view/View;

    .line 307
    const v1, 0x7f0e04a1

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->attachmentContainer:Landroid/view/ViewGroup;

    .line 308
    const v1, 0x7f0e049b

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->otherNameHolder:Landroid/widget/LinearLayout;

    .line 309
    const v1, 0x7f0e049e

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->speechArrowGraphicsOther:Landroid/widget/ImageView;

    .line 311
    return-object v0
.end method
