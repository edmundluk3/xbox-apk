.class Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadConversationUpdateFromNotification"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final isGroupConversation:Z

.field private final senderXuid:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Z)V
    .locals 0
    .param p2, "senderXuid"    # Ljava/lang/String;
    .param p3, "isGroupConversation"    # Z

    .prologue
    .line 2389
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 2390
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->senderXuid:Ljava/lang/String;

    .line 2391
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->isGroupConversation:Z

    .line 2392
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 2396
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 2397
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 2425
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 2426
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->senderXuid:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->isGroupConversation:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->loadSkypeConversationMessages(Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2383
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 2420
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 2383
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 2402
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 2403
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 5
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 2412
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne p1, v0, :cond_0

    .line 2413
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->MessageDetailsData:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v3, 0x0

    invoke-direct {v1, v2, p0, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 2415
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2383
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 2407
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 2408
    return-void
.end method
