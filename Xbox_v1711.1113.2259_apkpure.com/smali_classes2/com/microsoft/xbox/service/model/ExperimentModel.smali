.class public Lcom/microsoft/xbox/service/model/ExperimentModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "ExperimentModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/ExperimentModel$GetExperiementDataRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/microsoft/xbox/service/model/ExperimentModel;


# instance fields
.field private isDebug:Z

.field private settings:Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

.field private targetXuid:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/microsoft/xbox/service/model/ExperimentModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/ExperimentModel;->TAG:Ljava/lang/String;

    .line 20
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/model/ExperimentModel;->instance:Lcom/microsoft/xbox/service/model/ExperimentModel;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/ExperimentModel;->isDebug:Z

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/ExperimentModel;->settings:Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/microsoft/xbox/service/model/ExperimentModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/ExperimentModel;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/service/model/ExperimentModel;->instance:Lcom/microsoft/xbox/service/model/ExperimentModel;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/model/ExperimentModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/ExperimentModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/ExperimentModel;->instance:Lcom/microsoft/xbox/service/model/ExperimentModel;

    .line 34
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/ExperimentModel;->instance:Lcom/microsoft/xbox/service/model/ExperimentModel;

    return-object v0
.end method


# virtual methods
.method public getIsDebug()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/ExperimentModel;->isDebug:Z

    return v0
.end method

.method public getTreatments()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    .line 108
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method

.method public getXuid()J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/ExperimentModel;->targetXuid:J

    return-wide v0
.end method

.method public load(J)V
    .locals 5
    .param p1, "xuid"    # J

    .prologue
    .line 125
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/model/ExperimentModel;->setXuid(J)V

    .line 127
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getExperimentSettings()Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "savedSettings":Ljava/lang/String;
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/ExperimentModel;->settings:Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    .line 130
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ExperimentModel;->settings:Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->getXuid()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-eqz v2, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ExperimentModel;->reset()V

    .line 134
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/model/ExperimentModel$GetExperiementDataRunner;

    invoke-direct {v0, p0, p0}, Lcom/microsoft/xbox/service/model/ExperimentModel$GetExperiementDataRunner;-><init>(Lcom/microsoft/xbox/service/model/ExperimentModel;Lcom/microsoft/xbox/service/model/ExperimentModel;)V

    .line 135
    .local v0, "runner":Lcom/microsoft/xbox/service/model/ExperimentModel$GetExperiementDataRunner;
    const/4 v2, 0x1

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->ExperimentData:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-virtual {p0, v2, v3, v0}, Lcom/microsoft/xbox/service/model/ExperimentModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 136
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 115
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->saveExperimentSettings(Ljava/lang/String;)V

    .line 116
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/ExperimentModel;->settings:Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    .line 117
    return-void
.end method

.method public setIsDebug(Z)V
    .locals 0
    .param p1, "debug"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/ExperimentModel;->isDebug:Z

    .line 71
    return-void
.end method

.method public setXuid(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/microsoft/xbox/service/model/ExperimentModel;->targetXuid:J

    .line 53
    return-void
.end method

.method public treatmentExists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "treatmentName"    # Ljava/lang/String;

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;>;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 145
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 147
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/ExperimentModel;->settings:Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    .line 150
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ExperimentModel;->settings:Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->saveExperimentSettings(Ljava/lang/String;)V

    .line 155
    :goto_1
    return-void

    .line 144
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 152
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/ExperimentModel;->TAG:Ljava/lang/String;

    const-string v1, "There was an error from the experimentation service.  Will use persisted data for now."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
