.class final synthetic Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$13;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$13;->arg$1:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$13;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$13;-><init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$13;->arg$1:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;

    check-cast p1, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;->apply(Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
