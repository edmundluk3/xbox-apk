.class public Lcom/microsoft/xbox/xle/app/activity/GameProfileFriendsScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "GameProfileFriendsScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string v0, "GameProfileFriends"

    return-object v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/GameProfileFriendsScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0705bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 25
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/GameProfileFriendsScreen;->onCreateContentView()V

    .line 29
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    const/4 v2, 0x0

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;-><init>([Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 35
    .local v0, "friendsScreenVM":Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->setAsPivotPane()V

    .line 37
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameProfileFriendsScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 38
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 42
    const v0, 0x7f03011f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameProfileFriendsScreen;->setContentView(I)V

    .line 43
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method
