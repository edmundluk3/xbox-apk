.class public Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;
.super Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;
.source "URCGeneratedGroupView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;,
        Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;-><init>(Landroid/content/Context;)V

    .line 21
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;)Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getURCViewModel()Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v0

    return-object v0
.end method

.method private generateContent(Landroid/content/Context;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;Z)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "factory"    # Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;
    .param p4, "callViewModel"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;",
            ">;",
            "Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 147
    .local p2, "buttons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->removeAllViews()V

    .line 149
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;

    .line 150
    .local v0, "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    .line 152
    .local v2, "id":Ljava/lang/String;
    :goto_1
    invoke-interface {p3, p1, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;->create(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;

    move-result-object v3

    .line 154
    .local v3, "v":Landroid/view/View;
    if-eqz v0, :cond_5

    .line 155
    if-nez v2, :cond_1

    .line 156
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 188
    :goto_2
    invoke-direct {p0, v3, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->setNarratorContent(Landroid/view/View;Ljava/lang/String;)V

    .line 189
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 150
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "v":Landroid/view/View;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 158
    .restart local v2    # "id":Ljava/lang/String;
    .restart local v3    # "v":Landroid/view/View;
    :cond_1
    instance-of v5, v3, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;

    if-eqz v5, :cond_4

    move-object v1, v3

    .line 159
    check-cast v1, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;

    .line 160
    .local v1, "button":Lcom/microsoft/xbox/xle/urc/ui/BranchButton;
    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->labelStr:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 161
    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->labelStr:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;->setText(Ljava/lang/CharSequence;)V

    .line 171
    .end local v1    # "button":Lcom/microsoft/xbox/xle/urc/ui/BranchButton;
    :cond_2
    :goto_3
    new-instance v5, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$4;

    invoke-direct {v5, p0, p4, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$4;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;ZLjava/lang/String;)V

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 163
    .restart local v1    # "button":Lcom/microsoft/xbox/xle/urc/ui/BranchButton;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v5

    iget v6, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->label:I

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 165
    .end local v1    # "button":Lcom/microsoft/xbox/xle/urc/ui/BranchButton;
    :cond_4
    instance-of v5, v3, Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;

    if-eqz v5, :cond_2

    move-object v1, v3

    .line 166
    check-cast v1, Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;

    .line 167
    .local v1, "button":Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;
    iget v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->image:I

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;->setImageResource(I)V

    .line 168
    sget-object v5, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_3

    .line 185
    .end local v1    # "button":Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 191
    .end local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "v":Landroid/view/View;
    :cond_6
    return-void
.end method

.method private getURCViewModel()Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getInstance(Landroid/content/Context;)Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v0

    return-object v0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->context:Landroid/content/Context;

    .line 65
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->updateButtons()V

    .line 66
    return-void
.end method

.method private setNarratorContent(Landroid/view/View;Ljava/lang/String;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 194
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getInstance(Landroid/content/Context;)Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getNarratorContentIdFromButtonId(Ljava/lang/String;)I

    move-result v0

    .line 198
    .local v0, "stringId":I
    if-eqz v0, :cond_0

    .line 199
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getURCViewModel()Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->addView(Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;)V

    .line 71
    invoke-super {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->onAttachedToWindow()V

    .line 72
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->onDetachedFromWindow()V

    .line 77
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getURCViewModel()Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->removeView(Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;)V

    .line 78
    return-void
.end method

.method public updateButtons()V
    .locals 5

    .prologue
    const/4 v0, 0x4

    const/4 v4, 0x0

    .line 81
    iget v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->groupType:I

    packed-switch v1, :pswitch_data_0

    .line 83
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected grouptype"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getURCViewModel()Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupColor:Ljava/util/ArrayList;

    new-instance v3, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$1;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$1;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;)V

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->generateContent(Landroid/content/Context;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;Z)V

    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getChildCount()I

    move-result v1

    if-ne v1, v0, :cond_1

    :goto_0
    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->cols_count:I

    .line 144
    :cond_0
    :goto_1
    return-void

    .line 96
    :cond_1
    const/4 v0, 0x3

    goto :goto_0

    .line 99
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getURCViewModel()Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupAction:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;

    sget v3, Lcom/microsoft/xbox/xle/urc/R$layout;->urc_action_button:I

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;-><init>(I)V

    invoke-direct {p0, v0, v1, v2, v4}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->generateContent(Landroid/content/Context;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;Z)V

    goto :goto_1

    .line 102
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getURCViewModel()Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNumpad:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$2;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;)V

    invoke-direct {p0, v0, v1, v2, v4}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->generateContent(Landroid/content/Context;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;Z)V

    goto :goto_1

    .line 117
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getURCViewModel()Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlayback:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$3;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$3;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;)V

    invoke-direct {p0, v0, v1, v2, v4}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->generateContent(Landroid/content/Context;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;Z)V

    goto :goto_1

    .line 138
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getURCViewModel()Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;

    sget v3, Lcom/microsoft/xbox/xle/urc/R$layout;->urc_action_button:I

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;-><init>(I)V

    invoke-direct {p0, v0, v1, v2, v4}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->generateContent(Landroid/content/Context;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;Z)V

    goto :goto_1

    .line 141
    :pswitch_6
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getURCViewModel()Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreFull:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;

    sget v3, Lcom/microsoft/xbox/xle/urc/R$layout;->urc_action_button:I

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;-><init>(I)V

    const/4 v3, 0x1

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->generateContent(Landroid/content/Context;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;Z)V

    goto :goto_1

    .line 81
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
