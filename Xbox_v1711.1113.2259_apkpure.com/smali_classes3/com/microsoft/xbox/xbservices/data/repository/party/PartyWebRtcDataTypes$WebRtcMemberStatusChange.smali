.class public abstract Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;
.super Ljava/lang/Object;
.source "PartyWebRtcDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$PartyWebRtcEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "WebRtcMemberStatusChange"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/String;Z)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;
    .locals 1
    .param p0, "userSsrc"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "isTalking"    # Z

    .prologue
    .line 27
    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 28
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcMemberStatusChange;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcMemberStatusChange;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public abstract isTalking()Z
.end method

.method public abstract userSsrc()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
