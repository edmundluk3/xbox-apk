.class public Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
.super Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
.source "VirtualGridRow.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;


# static fields
.field private static final TAG:Ljava/lang/String; = "VirtualGridRow"


# instance fields
.field private mScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V
    .locals 4
    .param p1, "grid"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V

    .line 44
    new-instance v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;-><init>(Landroid/content/Context;)V

    .line 45
    .local v0, "view":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 46
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setAdapter(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;)V

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setListener(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;)V

    .line 48
    new-instance v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow$1;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow$1;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->setTopView(Landroid/view/View;)V

    .line 57
    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    .line 58
    return-void
.end method


# virtual methods
.method public bind(I)V
    .locals 2
    .param p1, "row"    # I

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->bind(I)V

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    iget v1, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualWidth:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setContentWidth(I)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    iget v1, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mVirtualOffset:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setContentOffset(I)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->refresh(Z)V

    .line 79
    return-void
.end method

.method public getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    return-object v0
.end method

.method public getViewIterator(IZ)Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;
    .locals 3
    .param p1, "pos"    # I
    .param p2, "forward"    # Z

    .prologue
    const/4 v0, 0x0

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    if-nez v1, :cond_0

    .line 92
    const-string v1, "VirtualGridRow"

    const-string v2, "getViewIterator: ViewGrid null!"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :goto_0
    return-object v0

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    if-nez v1, :cond_1

    .line 95
    const-string v1, "VirtualGridRow"

    const-string v2, "getViewIterator: Adapter null!"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mRow:I

    invoke-interface {v0, v1, p1, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;->getViewIterator(IIZ)Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;

    move-result-object v0

    goto :goto_0
.end method

.method public reclaimView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;->reclaimView(Landroid/view/View;)V

    .line 104
    return-void
.end method

.method public refresh(Z)V
    .locals 1
    .param p1, "clear"    # Z

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->mScroller:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->refresh(Z)V

    .line 84
    return-void
.end method
