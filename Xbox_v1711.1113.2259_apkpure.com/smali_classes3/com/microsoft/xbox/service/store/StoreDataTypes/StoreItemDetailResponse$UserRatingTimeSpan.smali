.class public final enum Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;
.super Ljava/lang/Enum;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UserRatingTimeSpan"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

.field public static final enum AllTime:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

.field public static final enum SevenDays:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

.field public static final enum ThirtyDays:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;


# instance fields
.field private final timeSpanString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 109
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    const-string v1, "AllTime"

    const-string v2, "alltime"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->AllTime:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    .line 110
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    const-string v1, "SevenDays"

    const-string v2, "7days"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->SevenDays:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    .line 111
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    const-string v1, "ThirtyDays"

    const-string v2, "30days"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->ThirtyDays:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    .line 108
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->AllTime:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->SevenDays:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->ThirtyDays:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "timeSpanString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 116
    iput-object p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->timeSpanString:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 108
    const-class v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->timeSpanString:Ljava/lang/String;

    return-object v0
.end method
