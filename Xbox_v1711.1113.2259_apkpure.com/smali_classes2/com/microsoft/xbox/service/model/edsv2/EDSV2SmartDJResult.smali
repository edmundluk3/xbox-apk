.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2SmartDJResult;
.super Ljava/lang/Object;
.source "EDSV2SmartDJResult.java"


# instance fields
.field private continuationToken:Ljava/lang/String;

.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContinuationToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SmartDJResult;->continuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public getItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 10
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SmartDJResult;->items:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setContinuationToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SmartDJResult;->continuationToken:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setItems(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SmartDJResult;->items:Ljava/util/ArrayList;

    .line 19
    return-void
.end method
