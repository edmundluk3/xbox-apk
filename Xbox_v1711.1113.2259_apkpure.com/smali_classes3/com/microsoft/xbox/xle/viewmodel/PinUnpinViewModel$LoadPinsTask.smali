.class Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PinUnpinViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadPinsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$1;

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 238
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->shouldRefresh()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->shouldRefresh()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 253
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->forceLoad:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 254
    .local v1, "statusLoadPins":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->forceLoad:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 255
    .local v0, "statusLoadModel":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->merge(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;[Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    return-object v2
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 248
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 244
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 265
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 235
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 260
    return-void
.end method
