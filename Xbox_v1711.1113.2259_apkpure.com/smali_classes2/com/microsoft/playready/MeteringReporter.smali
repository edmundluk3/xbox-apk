.class Lcom/microsoft/playready/MeteringReporter;
.super Ljava/lang/Object;
.source "MeteringReporter.java"

# interfaces
.implements Lcom/microsoft/playready/IMeteringReporter;


# instance fields
.field private final mExecService:Ljava/util/concurrent/ExecutorService;

.field private mMRPlugin:Lcom/microsoft/playready/IMeteringReportingPlugin;

.field private mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;


# direct methods
.method private constructor <init>(Lcom/microsoft/playready/PlayReadyFactory;)V
    .locals 1
    .param p1, "prFactory"    # Lcom/microsoft/playready/PlayReadyFactory;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/MeteringReporter;->mExecService:Ljava/util/concurrent/ExecutorService;

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/MeteringReporter;->mMRPlugin:Lcom/microsoft/playready/IMeteringReportingPlugin;

    .line 39
    iput-object p1, p0, Lcom/microsoft/playready/MeteringReporter;->mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 40
    return-void
.end method

.method static createMeteringReporter(Lcom/microsoft/playready/PlayReadyFactory;)Lcom/microsoft/playready/IMeteringReporter;
    .locals 1
    .param p0, "prFactory"    # Lcom/microsoft/playready/PlayReadyFactory;

    .prologue
    .line 32
    new-instance v0, Lcom/microsoft/playready/MeteringReporter;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/MeteringReporter;-><init>(Lcom/microsoft/playready/PlayReadyFactory;)V

    return-object v0
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/MeteringReporter;->mExecService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 54
    return-void

    .line 51
    :catchall_0
    move-exception v0

    .line 52
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 53
    throw v0
.end method

.method public getMeteringReportingPlugin()Lcom/microsoft/playready/IMeteringReportingPlugin;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/playready/MeteringReporter;->mMRPlugin:Lcom/microsoft/playready/IMeteringReportingPlugin;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Lcom/microsoft/playready/SimpleMeteringReportingPlugin;

    invoke-direct {v0}, Lcom/microsoft/playready/SimpleMeteringReportingPlugin;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MeteringReporter;->mMRPlugin:Lcom/microsoft/playready/IMeteringReportingPlugin;

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/microsoft/playready/MeteringReporter;->mMRPlugin:Lcom/microsoft/playready/IMeteringReportingPlugin;

    return-object v0
.end method

.method public reportMetering(Ljava/lang/String;)Lcom/microsoft/playready/IMeteringReportingTask;
    .locals 2
    .param p1, "b64EncodedMeteringCert"    # Ljava/lang/String;

    .prologue
    .line 98
    const/4 v1, 0x0

    invoke-static {p1, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 100
    .local v0, "meteringCert":[B
    invoke-virtual {p0, v0}, Lcom/microsoft/playready/MeteringReporter;->reportMetering([B)Lcom/microsoft/playready/IMeteringReportingTask;

    move-result-object v1

    return-object v1
.end method

.method public reportMetering([B)Lcom/microsoft/playready/IMeteringReportingTask;
    .locals 4
    .param p1, "meteringCert"    # [B

    .prologue
    .line 77
    if-nez p1, :cond_0

    .line 79
    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 80
    const-string v3, "Invalid argument to reportMetering, metering cert must not be null"

    .line 79
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 84
    :cond_0
    new-instance v1, Lcom/microsoft/playready/MeteringReportingWorker;

    .line 85
    iget-object v2, p0, Lcom/microsoft/playready/MeteringReporter;->mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 86
    invoke-virtual {p0}, Lcom/microsoft/playready/MeteringReporter;->getMeteringReportingPlugin()Lcom/microsoft/playready/IMeteringReportingPlugin;

    move-result-object v3

    .line 84
    invoke-direct {v1, v2, v3, p1}, Lcom/microsoft/playready/MeteringReportingWorker;-><init>(Lcom/microsoft/playready/PlayReadyFactory;Lcom/microsoft/playready/IMeteringReportingPlugin;[B)V

    .line 89
    .local v1, "worker":Lcom/microsoft/playready/MeteringReportingWorker;
    new-instance v0, Lcom/microsoft/playready/MeteringReportingTask;

    invoke-direct {v0, v1}, Lcom/microsoft/playready/MeteringReportingTask;-><init>(Lcom/microsoft/playready/MeteringReportingWorker;)V

    .line 90
    .local v0, "task":Lcom/microsoft/playready/MeteringReportingTask;
    iget-object v2, p0, Lcom/microsoft/playready/MeteringReporter;->mExecService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v2, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 92
    return-object v0
.end method

.method public setMeteringReportingPlugin(Lcom/microsoft/playready/IMeteringReportingPlugin;)V
    .locals 0
    .param p1, "mrPlugin"    # Lcom/microsoft/playready/IMeteringReportingPlugin;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/microsoft/playready/MeteringReporter;->mMRPlugin:Lcom/microsoft/playready/IMeteringReportingPlugin;

    .line 61
    return-void
.end method
