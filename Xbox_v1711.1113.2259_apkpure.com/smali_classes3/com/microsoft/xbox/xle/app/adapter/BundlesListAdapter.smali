.class public Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "BundlesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 20
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 26
    if-nez p2, :cond_2

    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 28
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f030046

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 30
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;

    invoke-direct {v0, v5}, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$1;)V

    .line 31
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;
    const v3, 0x7f0e0213

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;->access$102(Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;)Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 32
    const v3, 0x7f0e0214

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;->access$202(Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 34
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 39
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 40
    .local v1, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_1

    .line 41
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 42
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getIcon2x1Url()Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    const v6, 0x7f0201fb

    invoke-virtual {v3, v4, v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 45
    :cond_0
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getDisplayTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 48
    :cond_1
    return-object p2

    .line 36
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/BundlesListAdapter$ViewHolder;
    goto :goto_0
.end method
