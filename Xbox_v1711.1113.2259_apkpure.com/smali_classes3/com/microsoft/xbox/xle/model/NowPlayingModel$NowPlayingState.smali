.class public final enum Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
.super Ljava/lang/Enum;
.source "NowPlayingModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/model/NowPlayingModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NowPlayingState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

.field public static final enum ConnectedPlayingApp:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

.field public static final enum ConnectedPlayingGame:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

.field public static final enum ConnectedPlayingMusic:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

.field public static final enum ConnectedPlayingVideo:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

.field public static final enum Connecting:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

.field public static final enum Disconnected:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 59
    new-instance v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    const-string v1, "Disconnected"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Disconnected:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 60
    new-instance v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    const-string v1, "Connecting"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Connecting:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 61
    new-instance v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    const-string v1, "ConnectedPlayingVideo"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingVideo:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 62
    new-instance v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    const-string v1, "ConnectedPlayingMusic"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingMusic:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 63
    new-instance v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    const-string v1, "ConnectedPlayingApp"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingApp:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 64
    new-instance v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    const-string v1, "ConnectedPlayingGame"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingGame:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 57
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Disconnected:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Connecting:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingVideo:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingMusic:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingApp:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingGame:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->$VALUES:[Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 57
    const-class v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->$VALUES:[Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    return-object v0
.end method
