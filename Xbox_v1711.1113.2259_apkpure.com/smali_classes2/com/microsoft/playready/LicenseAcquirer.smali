.class Lcom/microsoft/playready/LicenseAcquirer;
.super Ljava/lang/Object;
.source "LicenseAcquirer.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static createLicenseAcquirer(Lcom/microsoft/playready/PlayReadyFactory;)Lcom/microsoft/playready/ILicenseAcquirer;
    .locals 1
    .param p0, "prFactory"    # Lcom/microsoft/playready/PlayReadyFactory;

    .prologue
    .line 73
    new-instance v0, Lcom/microsoft/playready/ProactiveLicenseAcquirer;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/ProactiveLicenseAcquirer;-><init>(Lcom/microsoft/playready/PlayReadyFactory;)V

    return-object v0
.end method

.method static createReactiveLicenseAcquirer()Lcom/microsoft/playready/IReactiveLicenseAcquirer;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/microsoft/playready/ReactiveLicenseAcquirer;

    invoke-direct {v0}, Lcom/microsoft/playready/ReactiveLicenseAcquirer;-><init>()V

    return-object v0
.end method
