.class Lcom/microsoft/xbox/smartglass/controls/JsonInformationPrivileges;
.super Lorg/json/JSONObject;
.source "JsonInformationPrivileges.java"


# direct methods
.method public constructor <init>([I)V
    .locals 6
    .param p1, "privileges"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 17
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 18
    .local v1, "privilegeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget v0, p1, v2

    .line 19
    .local v0, "i":I
    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/controls/Json;->unsignedInt(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 18
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 21
    .end local v0    # "i":I
    :cond_0
    const-string v2, "privileges"

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v1}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationPrivileges;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 22
    return-void
.end method
