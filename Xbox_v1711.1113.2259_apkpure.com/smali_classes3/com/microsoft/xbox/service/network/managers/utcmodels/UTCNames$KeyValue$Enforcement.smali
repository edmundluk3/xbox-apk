.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue$Enforcement;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Enforcement"
.end annotation


# static fields
.field public static final CommsAbusiveVoice:Ljava/lang/String; = "CommsAbusiveVoice"

.field public static final CommsLfgApplication:Ljava/lang/String; = "CommsLfgApplication"

.field public static final CommsSpam:Ljava/lang/String; = "CommsSpam"

.field public static final CommsTextMessage:Ljava/lang/String; = "CommsTextMessage"

.field public static final CommsTextMessageBreaksClubRules:Ljava/lang/String; = "CommsTextMessageBreaksClubRules"

.field public static final FairPlayCheater:Ljava/lang/String; = "FairPlayCheater"

.field public static final FairPlayQuitter:Ljava/lang/String; = "FairPlayQuitter"

.field public static final FairplayUnsporting:Ljava/lang/String; = "FairplayUnsporting"

.field public static final UserContentActivityFeed:Ljava/lang/String; = "UserContentActivityFeed"

.field public static final UserContentActivityFeedBreaksClubRules:Ljava/lang/String; = "UserContentActivityFeedBreaksClubRules"

.field public static final UserContentBreaksClubRules:Ljava/lang/String; = "UserContentBreaksClubRules"

.field public static final UserContentClubBackground:Ljava/lang/String; = "UserContentClubBackground"

.field public static final UserContentClubChat:Ljava/lang/String; = "UserContentClubChat"

.field public static final UserContentClubDescription:Ljava/lang/String; = "UserContentClubDescription"

.field public static final UserContentClubLogo:Ljava/lang/String; = "UserContentClubLogo"

.field public static final UserContentClubName:Ljava/lang/String; = "UserContentClubName"

.field public static final UserContentComment:Ljava/lang/String; = "UserContentComment"

.field public static final UserContentCommentBreaksClubRules:Ljava/lang/String; = "UserContentCommentBreaksClubRules"

.field public static final UserContentGameDVR:Ljava/lang/String; = "UserContentGameDVR"

.field public static final UserContentGamerpic:Ljava/lang/String; = "UserContentGamerpic"

.field public static final UserContentGamertag:Ljava/lang/String; = "UserContentGamertag"

.field public static final UserContentInappropriateLFG:Ljava/lang/String; = "UserContentInappropriateLFG"

.field public static final UserContentLFGApplication:Ljava/lang/String; = "UserContentLFGApplication"

.field public static final UserContentPersonalInfo:Ljava/lang/String; = "UserContentPersonalInfo"

.field public static final UserContentRealName:Ljava/lang/String; = "UserContentRealName"

.field public static final UserContentScreenshot:Ljava/lang/String; = "UserContentScreenshot"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
