.class Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PinUnpinViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PinUnpinTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final pinItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

.field private final pinning:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)V
    .locals 0
    .param p2, "pinItem"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p3, "pinning"    # Z

    .prologue
    .line 327
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 328
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 329
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->pinning:Z

    .line 330
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->pinItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 331
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x1

    return v0
.end method

.method public isPinning()Z
    .locals 1

    .prologue
    .line 334
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->pinning:Z

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 354
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    .line 355
    .local v0, "pm":Lcom/microsoft/xbox/service/model/pins/PinsModel;
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->pinning:Z

    if-eqz v1, :cond_0

    new-array v1, v2, [Lcom/microsoft/xbox/service/model/pins/PinItem;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->pinItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->add([Lcom/microsoft/xbox/service/model/pins/PinItem;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    new-array v1, v2, [Lcom/microsoft/xbox/service/model/pins/PinItem;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->pinItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->remove([Lcom/microsoft/xbox/service/model/pins/PinItem;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 349
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 345
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 365
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 366
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 320
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->access$402(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/toolkit/network/ListState;)Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 361
    return-void
.end method
