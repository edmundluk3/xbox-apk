.class public Lcom/microsoft/xbox/smartglass/internal/HttpClient;
.super Ljava/lang/Object;
.source "HttpClient.java"


# static fields
.field private static final ConnectionPerRoute:I = 0x10

.field private static final HttpSocketBufferSize:I = 0x1000

.field private static final MaxConnectionCount:I = 0x20

.field private static _client:Lorg/apache/http/impl/client/DefaultHttpClient;

.field private static _params:Lorg/apache/http/params/HttpParams;


# instance fields
.field private _response:Lorg/apache/http/HttpResponse;

.field private _url:Lorg/apache/http/client/methods/HttpUriRequest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method private static initializeParams(I)V
    .locals 5
    .param p0, "timeoutInMilliseconds"    # I

    .prologue
    .line 63
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    sput-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    .line 66
    sget-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    sget-object v2, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-static {v1, v2}, Lorg/apache/http/params/HttpProtocolParams;->setVersion(Lorg/apache/http/params/HttpParams;Lorg/apache/http/ProtocolVersion;)V

    .line 67
    sget-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Lorg/apache/http/params/HttpProtocolParams;->setContentCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 68
    sget-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lorg/apache/http/params/HttpProtocolParams;->setUseExpectContinue(Lorg/apache/http/params/HttpParams;Z)V

    .line 70
    sget-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 76
    sget-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    invoke-static {v1, p0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 77
    sget-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    invoke-static {v1, p0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 78
    sget-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    const/16 v2, 0x1000

    invoke-static {v1, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 79
    sget-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    new-instance v2, Lorg/apache/http/conn/params/ConnPerRouteBean;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    invoke-static {v1, v2}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    .line 80
    sget-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    const/16 v2, 0x20

    invoke-static {v1, v2}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxTotalConnections(Lorg/apache/http/params/HttpParams;I)V

    .line 82
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    sget-object v2, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    invoke-direct {v1, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    sput-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_client:Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 84
    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v0

    .line 85
    .local v0, "socketFactory":Lorg/apache/http/conn/ssl/SSLSocketFactory;
    sget-object v1, Lorg/apache/http/conn/ssl/SSLSocketFactory;->ALLOW_ALL_HOSTNAME_VERIFIER:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 86
    sget-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_client:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 87
    sget-object v1, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_client:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v1

    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "https"

    const/16 v4, 0x1bb

    invoke-direct {v2, v3, v0, v4}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 89
    :cond_0
    return-void
.end method


# virtual methods
.method public addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "headerName"    # Ljava/lang/String;
    .param p2, "headerValue"    # Ljava/lang/String;

    .prologue
    .line 110
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_url:Lorg/apache/http/client/methods/HttpUriRequest;

    new-instance v2, Lorg/apache/http/message/BasicHeader;

    invoke-direct {v2, p1, p2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Lorg/apache/http/Header;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public cancelRequest()Ljava/lang/String;
    .locals 3

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 130
    .local v0, "errorMsg":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_url:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :goto_0
    return-object v0

    .line 131
    :catch_0
    move-exception v1

    .line 132
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public execute()Ljava/lang/String;
    .locals 4

    .prologue
    .line 118
    const/4 v0, 0x0

    .line 120
    .local v0, "errorMsg":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_client:Lorg/apache/http/impl/client/DefaultHttpClient;

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_url:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-virtual {v2, v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_response:Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    return-object v0

    .line 121
    :catch_0
    move-exception v1

    .line 122
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getResponseBody([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "responseBody"    # [Ljava/lang/String;

    .prologue
    .line 174
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_response:Lorg/apache/http/HttpResponse;

    if-eqz v2, :cond_0

    .line 175
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_response:Lorg/apache/http/HttpResponse;

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 176
    .local v0, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v0, :cond_0

    .line 178
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    .end local v0    # "entity":Lorg/apache/http/HttpEntity;
    :cond_0
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 179
    .restart local v0    # "entity":Lorg/apache/http/HttpEntity;
    :catch_0
    move-exception v1

    .line 180
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getResponseHeaderArray([Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "headerFields"    # [Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 149
    const/4 v4, 0x0

    .line 150
    .local v4, "headers":[Lorg/apache/http/Header;
    :try_start_0
    iget-object v9, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_response:Lorg/apache/http/HttpResponse;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_response:Lorg/apache/http/HttpResponse;

    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    if-eqz v4, :cond_0

    array-length v9, v4

    if-nez v9, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-object v8

    .line 154
    :cond_1
    const/4 v5, 0x0

    .line 155
    .local v5, "i":I
    move-object v0, v4

    .local v0, "arr$":[Lorg/apache/http/Header;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v7, :cond_0

    aget-object v2, v0, v6

    .line 156
    .local v2, "header":Lorg/apache/http/Header;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 157
    .local v3, "headerFieldValue":Ljava/lang/String;
    aput-object v3, p1, v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    add-int/lit8 v5, v5, 0x1

    .line 155
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 160
    .end local v0    # "arr$":[Lorg/apache/http/Header;
    .end local v2    # "header":Lorg/apache/http/Header;
    .end local v3    # "headerFieldValue":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    :catch_0
    move-exception v1

    .line 161
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method

.method public getResponseHeaderCount()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 138
    const/4 v1, 0x0

    .line 140
    .local v1, "headers":[Lorg/apache/http/Header;
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_response:Lorg/apache/http/HttpResponse;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_response:Lorg/apache/http/HttpResponse;

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v1

    if-nez v1, :cond_1

    .line 142
    :cond_0
    :goto_0
    return v2

    .line 140
    :cond_1
    array-length v2, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "ex":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getStatusCode()I
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_response:Lorg/apache/http/HttpResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_response:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    if-nez v0, :cond_1

    .line 168
    :cond_0
    const/4 v0, 0x0

    .line 170
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_response:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    goto :goto_0
.end method

.method public initializeClient(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p1, "urlAddress"    # Ljava/lang/String;
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "timeoutInMilliseconds"    # I

    .prologue
    .line 43
    const/4 v0, 0x0

    .line 45
    .local v0, "errorMsg":Ljava/lang/String;
    :try_start_0
    const-string v2, "GET"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 46
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_url:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 53
    :goto_0
    sget-object v2, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_client:Lorg/apache/http/impl/client/DefaultHttpClient;

    if-nez v2, :cond_1

    .line 54
    :cond_0
    invoke-static {p3}, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->initializeParams(I)V

    .line 59
    :cond_1
    :goto_1
    return-object v0

    .line 47
    :cond_2
    const-string v2, "POST"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 48
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_url:Lorg/apache/http/client/methods/HttpUriRequest;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 56
    :catch_0
    move-exception v1

    .line 57
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 50
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    const-string v0, "HTTP request method not supported."
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setRequestBody(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 92
    const/4 v1, 0x0

    .line 93
    .local v1, "errorMsg":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_url:Lorg/apache/http/client/methods/HttpUriRequest;

    instance-of v4, v4, Lorg/apache/http/client/methods/HttpPost;

    if-eqz v4, :cond_0

    .line 94
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/internal/HttpClient;->_url:Lorg/apache/http/client/methods/HttpUriRequest;

    check-cast v3, Lorg/apache/http/client/methods/HttpPost;

    .line 97
    .local v3, "post":Lorg/apache/http/client/methods/HttpPost;
    :try_start_0
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v0, p1}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 98
    .local v0, "bodyEntity":Lorg/apache/http/entity/StringEntity;
    invoke-virtual {v3, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .end local v0    # "bodyEntity":Lorg/apache/http/entity/StringEntity;
    .end local v3    # "post":Lorg/apache/http/client/methods/HttpPost;
    :goto_0
    return-object v1

    .line 99
    .restart local v3    # "post":Lorg/apache/http/client/methods/HttpPost;
    :catch_0
    move-exception v2

    .line 100
    .local v2, "ex":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 103
    .end local v2    # "ex":Ljava/lang/Exception;
    .end local v3    # "post":Lorg/apache/http/client/methods/HttpPost;
    :cond_0
    const-string v1, "Set body is only supported for HTTP POST requests"

    goto :goto_0
.end method
