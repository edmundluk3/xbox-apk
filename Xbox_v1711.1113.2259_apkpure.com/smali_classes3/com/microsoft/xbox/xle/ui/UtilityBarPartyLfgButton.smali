.class public Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;
.super Landroid/widget/LinearLayout;
.source "UtilityBarPartyLfgButton.java"


# instance fields
.field private systemSettingsDisposable:Lio/reactivex/disposables/Disposable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 34
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f03026f

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 36
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton$$Lambda$1;->lambdaFactory$()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-void
.end method

.method static synthetic lambda$new$0(Landroid/view/View;)V
    .locals 3
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 38
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 39
    .local v0, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->closeDrawer()V

    .line 43
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCHomeActionBar;->trackLFGAction()V

    .line 44
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/PartyAndLfgScreen;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    .end local v0    # "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    :goto_0
    return-void

    .line 45
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic lambda$onAttachedToWindow$1(Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;Lcom/microsoft/xbox/xle/app/SmartglassSettings;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;
    .param p1, "ignored"    # Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->isPartyChatEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    const v1, 0x7f0e0bc2

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 58
    .local v0, "icon":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070f0b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701f2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 61
    .end local v0    # "icon":Landroid/widget/TextView;
    :cond_0
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 55
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getSettings()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;->systemSettingsDisposable:Lio/reactivex/disposables/Disposable;

    .line 62
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;->systemSettingsDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;->systemSettingsDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 70
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 71
    return-void
.end method
