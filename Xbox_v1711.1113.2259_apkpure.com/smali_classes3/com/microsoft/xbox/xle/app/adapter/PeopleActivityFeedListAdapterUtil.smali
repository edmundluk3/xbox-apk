.class public Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;
.super Ljava/lang/Object;
.source "PeopleActivityFeedListAdapterUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static attachSocialBarListeners(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)V
    .locals 18
    .param p0, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "authorInfo"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .param p3, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p4, "isTrending"    # Z

    .prologue
    .line 247
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    if-eqz v2, :cond_0

    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_1

    .line 258
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 266
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_3

    .line 267
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 269
    if-eqz p1, :cond_c

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 270
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    const-string v8, "Club"

    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 271
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    const-string v8, "Club/"

    const/4 v14, 0x1

    const-string v15, "0"

    invoke-static {v2, v8, v14, v15}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    .line 272
    .local v11, "clubId":Ljava/lang/Long;
    if-eqz v11, :cond_b

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v2, v14, v16

    if-lez v2, :cond_b

    .line 273
    sget-object v2, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual {v2, v14, v15}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v12

    .line 274
    .local v12, "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v2

    sget-object v8, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-eq v2, v8, :cond_2

    .line 275
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v8, 0x8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 285
    .end local v11    # "clubId":Ljava/lang/Long;
    .end local v12    # "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_5

    .line 294
    move-object/from16 v0, p3

    instance-of v4, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModel;

    .line 295
    .local v4, "isHomeFeed":Z
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->getAuthorType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    move-result-object v8

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {v2, v8, v14}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canReportFeedItem(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;Ljava/lang/String;)Z

    move-result v5

    .line 296
    .local v5, "canReport":Z
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canDeleteFeedItem(Ljava/lang/String;)Z

    move-result v6

    .line 297
    .local v6, "canDelete":Z
    if-nez p4, :cond_d

    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->canHidePost()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v2

    sget-object v8, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Container:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-eq v2, v8, :cond_d

    const/4 v3, 0x1

    .line 298
    .local v3, "canHide":Z
    :goto_1
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes;->canUserPinPost(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Z

    move-result v10

    .line 299
    .local v10, "canPinItem":Z
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->pinned:Z

    if-nez v2, :cond_e

    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->canPinPosts()Z

    move-result v2

    if-eqz v2, :cond_e

    if-eqz v10, :cond_e

    const/4 v7, 0x1

    .line 300
    .local v7, "canPin":Z
    :goto_2
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->pinned:Z

    if-eqz v2, :cond_f

    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->canPinPosts()Z

    move-result v2

    if-eqz v2, :cond_f

    if-eqz v10, :cond_f

    const/4 v9, 0x1

    .line 301
    .local v9, "canUnpin":Z
    :goto_3
    if-nez v5, :cond_4

    if-nez v6, :cond_4

    if-nez v3, :cond_4

    if-nez v7, :cond_4

    if-eqz v9, :cond_10

    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    if-eqz v2, :cond_10

    const/4 v13, 0x1

    .line 303
    .local v13, "hasMoreAction":Z
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v2, v13}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 304
    if-eqz v13, :cond_5

    .line 305
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v2, p1

    move-object/from16 v8, p3

    invoke-static/range {v2 .. v9}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;ZZZZZLcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v14, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 434
    .end local v3    # "canHide":Z
    .end local v4    # "isHomeFeed":Z
    .end local v5    # "canReport":Z
    .end local v6    # "canDelete":Z
    .end local v7    # "canPin":Z
    .end local v9    # "canUnpin":Z
    .end local v10    # "canPinItem":Z
    .end local v13    # "hasMoreAction":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v2, :cond_6

    .line 435
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 443
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v2, :cond_7

    .line 444
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 452
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v2, :cond_8

    .line 453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 461
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->undoButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_9

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 462
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->undoButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 465
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->dismissButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_a

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 466
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->dismissButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 468
    :cond_a
    return-void

    .line 278
    .restart local v11    # "clubId":Ljava/lang/Long;
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v8, 0x8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 282
    .end local v11    # "clubId":Ljava/lang/Long;
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v8, 0x8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 297
    .restart local v4    # "isHomeFeed":Z
    .restart local v5    # "canReport":Z
    .restart local v6    # "canDelete":Z
    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 299
    .restart local v3    # "canHide":Z
    .restart local v10    # "canPinItem":Z
    :cond_e
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 300
    .restart local v7    # "canPin":Z
    :cond_f
    const/4 v9, 0x0

    goto/16 :goto_3

    .line 301
    .restart local v9    # "canUnpin":Z
    :cond_10
    const/4 v13, 0x0

    goto/16 :goto_4
.end method

.method public static bindSocialRecommendationView(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V
    .locals 12
    .param p0, "viewHolder"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 471
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    check-cast v4, Landroid/widget/LinearLayout;

    .line 472
    .local v4, "linearView":Landroid/widget/LinearLayout;
    iget-object v7, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    if-eqz v7, :cond_0

    iget-object v7, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iget-object v7, v7, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v7

    if-nez v7, :cond_0

    move v1, v8

    .line 473
    .local v1, "hasRecommendation":Z
    :goto_0
    if-eqz v4, :cond_1

    .line 474
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    .line 475
    .local v0, "childViewCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_1

    .line 477
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-static {v7, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 475
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0    # "childViewCount":I
    .end local v1    # "hasRecommendation":Z
    .end local v2    # "i":I
    :cond_0
    move v1, v9

    .line 472
    goto :goto_0

    .line 480
    .restart local v1    # "hasRecommendation":Z
    :cond_1
    if-nez v1, :cond_3

    .line 555
    :cond_2
    :goto_2
    return-void

    .line 485
    :cond_3
    iget-object v7, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iget-object v6, v7, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    .line 486
    .local v6, "socialRecommendations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    if-eqz p0, :cond_2

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationPersonView:Landroid/view/View;

    if-eqz v7, :cond_2

    .line 487
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->seeAllRecommendations:Landroid/widget/RelativeLayout;

    if-eqz v7, :cond_4

    .line 488
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->seeAllRecommendations:Landroid/widget/RelativeLayout;

    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Landroid/view/View$OnClickListener;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 494
    :cond_4
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 495
    .local v5, "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationProfileView:Landroid/view/View;

    if-eqz v7, :cond_5

    .line 496
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationProfileView:Landroid/view/View;

    invoke-static {v5, p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Landroid/view/View$OnClickListener;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 504
    :cond_5
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationRemoveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v7, :cond_6

    .line 505
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationRemoveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v5, v6, p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Landroid/view/View$OnClickListener;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 515
    :cond_6
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->addFriendButton:Landroid/widget/RelativeLayout;

    if-eqz v7, :cond_7

    .line 516
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->addFriendButton:Landroid/widget/RelativeLayout;

    invoke-static {v5, p2, v6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/ArrayList;)Landroid/view/View$OnClickListener;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 539
    :cond_7
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationProfileImg:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v10, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    invoke-virtual {v7, v10}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 540
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v10, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->gamertag:Ljava/lang/String;

    invoke-static {v7, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 541
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationRealname:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v10, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->realName:Ljava/lang/String;

    invoke-static {v7, v10, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 543
    iget-object v7, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recommendation:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;

    if-eqz v7, :cond_2

    .line 544
    iget-object v7, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recommendation:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;

    iget-object v7, v7, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;->Reasons:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 545
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationReason:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v7, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recommendation:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;

    iget-object v7, v7, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;->Reasons:Ljava/util/ArrayList;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-static {v10, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 547
    :cond_8
    iget-object v7, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recommendation:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;

    iget-object v7, v7, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;->Type:Ljava/lang/String;

    sget-object v10, Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;->SMALL:Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;

    invoke-static {v7, v10}, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->getIconBySize(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;)Ljava/lang/String;

    move-result-object v3

    .line 548
    .local v3, "icon":Ljava/lang/String;
    if-eqz v3, :cond_9

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v7, :cond_9

    .line 549
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v7, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 551
    :cond_9
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v3, :cond_a

    move v7, v8

    :goto_3
    invoke-static {v10, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 552
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationIconText:Landroid/view/View;

    iget-object v10, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recommendation:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;->getRecommendationType()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->PhoneContact:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    if-ne v10, v11, :cond_b

    :goto_4
    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    goto/16 :goto_2

    :cond_a
    move v7, v9

    .line 551
    goto :goto_3

    :cond_b
    move v8, v9

    .line 552
    goto :goto_4
.end method

.method public static bindTrendingView(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V
    .locals 12
    .param p0, "viewHolder"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    .prologue
    const/4 v11, 0x1

    const/4 v0, 0x0

    .line 558
    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItems:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    move v8, v11

    .line 559
    .local v8, "hasTrendingData":Z
    :goto_0
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    check-cast v10, Landroid/widget/LinearLayout;

    .line 560
    .local v10, "linearView":Landroid/widget/LinearLayout;
    if-eqz v10, :cond_1

    .line 561
    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    .line 562
    .local v7, "childViewCount":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-ge v9, v7, :cond_1

    .line 564
    invoke-virtual {v10, v9}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 562
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .end local v7    # "childViewCount":I
    .end local v8    # "hasTrendingData":Z
    .end local v9    # "i":I
    .end local v10    # "linearView":Landroid/widget/LinearLayout;
    :cond_0
    move v8, v0

    .line 558
    goto :goto_0

    .line 567
    .restart local v8    # "hasTrendingData":Z
    .restart local v10    # "linearView":Landroid/widget/LinearLayout;
    :cond_1
    if-nez v8, :cond_2

    .line 591
    :goto_2
    return-void

    .line 571
    :cond_2
    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 572
    .local v2, "trendingItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->seeAllTrending:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 573
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->seeAllTrending:Landroid/widget/RelativeLayout;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$14;->lambdaFactory$()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 579
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->trendingDescription:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v0, :cond_4

    .line 580
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->trendingDescription:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 583
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    if-nez v0, :cond_5

    .line 584
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createTrendingHolder(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 587
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 588
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->detachItemListeners(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)V

    .line 589
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->ACTIVITY_FEED:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->NAV_FEED:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    move-object v0, p2

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V

    .line 590
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    iget-object v1, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-static {v0, v2, v1, p2, v11}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->attachSocialBarListeners(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)V

    goto :goto_2
.end method

.method public static bindView(Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 1
    .param p0, "v"    # Landroid/view/View;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 610
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 611
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 612
    return-void
.end method

.method private static bindView(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)V
    .locals 32
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p1, "authorInfo"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .param p2, "shortDescription"    # Ljava/lang/String;
    .param p3, "authorStatus"    # Lcom/microsoft/xbox/service/model/UserStatus;
    .param p4, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .prologue
    .line 615
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v26

    sget-object v27, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Container:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v26

    sget-object v27, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->SocialRecommendation:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    .line 1183
    :cond_0
    :goto_0
    return-void

    .line 619
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v13

    .line 622
    .local v13, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    sget-object v26, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$AuthorInfo$AuthorType:[I

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->getAuthorType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->ordinal()I

    move-result v27

    aget v26, v26, v27

    packed-switch v26, :pswitch_data_0

    .line 642
    :goto_1
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2

    .line 643
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    if-eqz v26, :cond_a

    .line 644
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 646
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->imageUrl:Ljava/lang/String;

    move-object/from16 v27, v0

    const v28, 0x7f020125

    const v29, 0x7f020125

    invoke-virtual/range {v26 .. v29}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 656
    :cond_2
    :goto_2
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_3

    .line 657
    if-nez p3, :cond_b

    .line 658
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-virtual/range {v26 .. v30}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 670
    :cond_3
    :goto_3
    sget-object v26, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$AuthorInfo$AuthorType:[I

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->getAuthorType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->ordinal()I

    move-result v27

    aget v26, v26, v27

    packed-switch v26, :pswitch_data_1

    .line 692
    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->getActivtyDescription(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 693
    .local v3, "actionText":Ljava/lang/String;
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->actionView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_f

    const/16 v26, 0x1

    :goto_5
    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-static {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 694
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->dateView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getIsHidden()Z

    move-result v26

    if-eqz v26, :cond_10

    const/16 v26, 0x8

    :goto_6
    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 695
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->dateView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->date:Ljava/util/Date;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateUptoMonthToDurationNow(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v26

    const-string v28, ""

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/CharSequence;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 696
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->dateView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->date:Ljava/util/Date;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateNarratorContentUptoMonthToDurationNow(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 697
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->realnameView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->secondName:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_11

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->getAuthorType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    move-result-object v26

    sget-object v28, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->User:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_11

    const/16 v26, 0x1

    :goto_7
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->secondName:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 698
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->ugcCaption:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->ugcCaption:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_12

    const/16 v26, 0x1

    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->ugcCaption:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 700
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->contentTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_4

    .line 701
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->contentTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_13

    const/16 v26, 0x1

    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 705
    :cond_4
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    move-object/from16 v26, v0

    if-eqz v26, :cond_5

    .line 706
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v26, v0

    if-eqz v26, :cond_14

    .line 707
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/xle/ui/LikeControl;->enable()V

    .line 708
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setLikeState(Z)V

    .line 715
    :cond_5
    :goto_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->pinned:Z

    move/from16 v26, v0

    if-eqz v26, :cond_15

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->pinIcon:Landroid/widget/TextView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_15

    .line 716
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->dateView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x8

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 717
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->pinIcon:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 722
    :goto_b
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 723
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v27, v0

    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v26

    if-eqz v26, :cond_16

    const v26, 0x7f0c0132

    :goto_c
    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setTextColorIfNotNull(Landroid/widget/TextView;I)V

    .line 724
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v27, v0

    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v26

    if-eqz v26, :cond_17

    const v26, 0x7f0c0132

    :goto_d
    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setTextColorIfNotNull(Landroid/widget/TextView;I)V

    .line 726
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v26, v0

    if-nez v26, :cond_18

    const/4 v12, 0x0

    .line 727
    .local v12, "likeCount":I
    :goto_e
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v0, v12}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setSocialBarValueIfNotNull(Lcom/microsoft/xbox/xle/ui/SocialBarValue;I)V

    .line 728
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v27, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    if-le v12, v0, :cond_19

    sget-object v26, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v28, 0x7f0700d9

    .line 729
    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    .line 728
    :goto_f
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 731
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v26, v0

    if-nez v26, :cond_1a

    const/4 v7, 0x0

    .line 732
    .local v7, "commentCount":I
    :goto_10
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v0, v7}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setSocialBarValueIfNotNull(Lcom/microsoft/xbox/xle/ui/SocialBarValue;I)V

    .line 733
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v27, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    if-le v7, v0, :cond_1b

    sget-object v26, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v28, 0x7f0700b8

    .line 734
    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    .line 733
    :goto_11
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 736
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v26, v0

    if-nez v26, :cond_1c

    const/16 v19, 0x0

    .line 737
    .local v19, "shareCount":I
    :goto_12
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setSocialBarValueIfNotNull(Lcom/microsoft/xbox/xle/ui/SocialBarValue;I)V

    .line 738
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v27, v0

    const/16 v26, 0x1

    move/from16 v0, v19

    move/from16 v1, v26

    if-le v0, v1, :cond_1d

    sget-object v26, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v28, 0x7f0700e9

    .line 739
    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    .line 738
    :goto_13
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 743
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v26, v0

    if-eqz v26, :cond_6

    .line 744
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v26, v0

    if-eqz v26, :cond_1f

    .line 745
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v26

    if-nez v26, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    move/from16 v26, v0

    if-lez v26, :cond_1e

    .line 746
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setVisibility(I)V

    .line 755
    :cond_6
    :goto_14
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValuesContainer:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v27, v0

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v28, v0

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v29, v0

    invoke-static/range {v26 .. v29}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->updateSocialBarValueContainerVisibility(Landroid/view/View;Lcom/microsoft/xbox/xle/ui/SocialBarValue;Lcom/microsoft/xbox/xle/ui/SocialBarValue;Lcom/microsoft/xbox/xle/ui/SocialBarValue;)V

    .line 759
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 761
    sget-object v26, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType:[I

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->ordinal()I

    move-result v27

    aget v26, v26, v27

    packed-switch v26, :pswitch_data_2

    .line 1109
    sget-object v26, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->TAG:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Encountered unknown ActivityItemType: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1110
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1111
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_50

    const/16 v26, 0x1

    :goto_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 1112
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1113
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1114
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1115
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1116
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1117
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1118
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1119
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1120
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1121
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1122
    const/4 v10, 0x1

    .line 1127
    .local v10, "hasBackgroundColor":Z
    :goto_16
    if-eqz v13, :cond_51

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v6

    .line 1129
    .local v6, "color":I
    :goto_17
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_8

    .line 1130
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v25

    check-cast v25, Landroid/view/View;

    .line 1131
    .local v25, "viewParent":Landroid/view/View;
    sget-object v26, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual/range {v26 .. v26}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v26

    move-object/from16 v0, v26

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v17, v0

    .line 1133
    .local v17, "scale":F
    const/high16 v26, 0x41200000    # 10.0f

    mul-float v26, v26, v17

    const/high16 v27, 0x3f000000    # 0.5f

    add-float v26, v26, v27

    move/from16 v0, v26

    float-to-int v15, v0

    .line 1134
    .local v15, "padding":I
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x13

    invoke-virtual/range {v26 .. v27}, Landroid/widget/TextView;->setGravity(I)V

    .line 1135
    const/high16 v26, 0x42c80000    # 100.0f

    mul-float v26, v26, v17

    const/high16 v27, 0x3f000000    # 0.5f

    add-float v26, v26, v27

    move/from16 v0, v26

    float-to-int v14, v0

    .line 1136
    .local v14, "minHeight":I
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setMinHeight(I)V

    .line 1138
    if-eqz v25, :cond_8

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getId()I

    move-result v26

    const v27, 0x7f0e0889

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_8

    .line 1139
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getVisibility()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->setVisibility(I)V

    .line 1140
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_53

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->getVisibility()I

    move-result v26

    if-nez v26, :cond_53

    .line 1141
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Landroid/widget/TextView;->setGravity(I)V

    .line 1142
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/widget/TextView;->setMinHeight(I)V

    .line 1143
    const/high16 v26, 0x41c80000    # 25.0f

    mul-float v26, v26, v17

    const/high16 v27, 0x3f000000    # 0.5f

    add-float v26, v26, v27

    move/from16 v0, v26

    float-to-int v15, v0

    .line 1144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v26, v0

    if-eqz v26, :cond_7

    .line 1145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v24

    .line 1146
    .local v24, "targetUserColor":I
    if-nez v24, :cond_52

    sget-object v26, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v27, 0x7f0c009c

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 1156
    .end local v24    # "targetUserColor":I
    :cond_7
    :goto_18
    move-object/from16 v0, v25

    invoke-virtual {v0, v15, v15, v15, v15}, Landroid/view/View;->setPadding(IIII)V

    .line 1157
    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1158
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v26

    if-eqz v26, :cond_8

    .line 1159
    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1164
    .end local v14    # "minHeight":I
    .end local v15    # "padding":I
    .end local v17    # "scale":F
    .end local v25    # "viewParent":Landroid/view/View;
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getIsHidden()Z

    move-result v26

    if-eqz v26, :cond_56

    .line 1165
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    move-object/from16 v26, v0

    if-eqz v26, :cond_9

    .line 1166
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    move-object/from16 v26, v0

    sget-object v27, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v28, 0x7f0c001a

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getColor(I)I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1168
    :cond_9
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->metadataLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    if-eqz v26, :cond_0

    .line 1169
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->metadataLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/high16 v27, 0x3f000000    # 0.5f

    invoke-virtual/range {v26 .. v27}, Landroid/view/ViewGroup;->setAlpha(F)V

    goto/16 :goto_0

    .line 624
    .end local v3    # "actionText":Ljava/lang/String;
    .end local v6    # "color":I
    .end local v7    # "commentCount":I
    .end local v10    # "hasBackgroundColor":Z
    .end local v12    # "likeCount":I
    .end local v19    # "shareCount":I
    :pswitch_0
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->titleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->titleImage:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setProfileImage(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;)V

    .line 625
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->titleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 626
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->userImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x8

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_1

    .line 629
    :pswitch_1
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->titleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->pageImage:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setProfileImage(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;)V

    .line 630
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->titleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 631
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->userImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x8

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_1

    .line 636
    :pswitch_2
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->userImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->imageUrl:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setProfileImage(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;)V

    .line 637
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->titleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x8

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 638
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->userImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_1

    .line 651
    :cond_a
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x8

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 660
    :cond_b
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    move-object/from16 v26, v0

    if-eqz v26, :cond_3

    .line 661
    sget-object v26, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    move-object/from16 v0, p3

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_c

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->getAuthorType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    move-result-object v26

    sget-object v27, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->User:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-ne v0, v1, :cond_c

    .line 662
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    goto/16 :goto_3

    .line 664
    :cond_c
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    move-object/from16 v26, v0

    const/16 v27, 0x8

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    goto/16 :goto_3

    .line 672
    :pswitch_3
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->titleName:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 675
    :pswitch_4
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->pageName:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 680
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->timeline:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;

    move-object/from16 v26, v0

    if-eqz v26, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->timeline:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;->timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-object/from16 v26, v0

    sget-object v27, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-ne v0, v1, :cond_d

    .line 682
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v26

    sget-object v27, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->TextPost:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-ne v0, v1, :cond_d

    const/16 v20, 0x1

    .line 684
    .local v20, "shouldClubFormatGamertag":Z
    :goto_19
    if-eqz v20, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->timeline:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;

    move-object/from16 v26, v0

    .line 685
    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;->getAuthorRoles()Ljava/util/List;

    move-result-object v26

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->name:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->getClubFormattedGamertag(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 688
    .local v9, "gamertag":Ljava/lang/String;
    :goto_1a
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v0, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 682
    .end local v9    # "gamertag":Ljava/lang/String;
    .end local v20    # "shouldClubFormatGamertag":Z
    :cond_d
    const/16 v20, 0x0

    goto :goto_19

    .line 685
    .restart local v20    # "shouldClubFormatGamertag":Z
    :cond_e
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->name:Ljava/lang/String;

    goto :goto_1a

    .line 693
    .end local v20    # "shouldClubFormatGamertag":Z
    .restart local v3    # "actionText":Ljava/lang/String;
    :cond_f
    const/16 v26, 0x0

    goto/16 :goto_5

    .line 694
    :cond_10
    const/16 v26, 0x0

    goto/16 :goto_6

    .line 697
    :cond_11
    const/16 v26, 0x0

    goto/16 :goto_7

    .line 698
    :cond_12
    const/16 v26, 0x0

    goto/16 :goto_8

    .line 701
    :cond_13
    const/16 v26, 0x0

    goto/16 :goto_9

    .line 710
    :cond_14
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/xle/ui/LikeControl;->disable()V

    goto/16 :goto_a

    .line 719
    :cond_15
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->pinIcon:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x8

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_b

    .line 723
    :cond_16
    const v26, 0x7f0c0131

    goto/16 :goto_c

    .line 724
    :cond_17
    const v26, 0x7f0c0131

    goto/16 :goto_d

    .line 726
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v12, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    goto/16 :goto_e

    .line 729
    .restart local v12    # "likeCount":I
    :cond_19
    sget-object v26, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v28, 0x7f0700b6

    .line 730
    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_f

    .line 731
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v7, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->commentCount:I

    goto/16 :goto_10

    .line 734
    .restart local v7    # "commentCount":I
    :cond_1b
    sget-object v26, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v28, 0x7f0700b5

    .line 735
    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_11

    .line 736
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    move/from16 v19, v0

    goto/16 :goto_12

    .line 739
    .restart local v19    # "shareCount":I
    :cond_1d
    sget-object v26, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v28, 0x7f0700b7

    .line 740
    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_13

    .line 748
    :cond_1e
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v26, v0

    const/16 v27, 0x8

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setVisibility(I)V

    goto/16 :goto_14

    .line 751
    :cond_1f
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    move-object/from16 v26, v0

    const/16 v27, 0x8

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setVisibility(I)V

    goto/16 :goto_14

    .line 763
    :pswitch_6
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 765
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentType:Ljava/lang/String;

    move-object/from16 v26, v0

    const-string v27, "game"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-nez v26, :cond_20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamerscore:I

    move/from16 v26, v0

    if-lez v26, :cond_22

    .line 766
    :cond_20
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, ""

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamerscore:I

    move/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    const/16 v28, 0x0

    invoke-static/range {v26 .. v28}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 767
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 768
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isRareAchievement()Z

    move-result v27

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 775
    :goto_1b
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 776
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_21

    .line 777
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementIcon:Ljava/lang/String;

    move-object/from16 v27, v0

    const v28, 0x7f020053

    const v29, 0x7f020055

    invoke-static/range {v26 .. v29}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V

    .line 779
    :cond_21
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_23

    const/16 v26, 0x1

    :goto_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 780
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 781
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 782
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 783
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 784
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 785
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 786
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 787
    const/4 v10, 0x0

    .line 788
    .restart local v10    # "hasBackgroundColor":Z
    goto/16 :goto_16

    .line 770
    .end local v10    # "hasBackgroundColor":Z
    :cond_22
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 771
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 772
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    goto/16 :goto_1b

    .line 779
    :cond_23
    const/16 v26, 0x0

    goto/16 :goto_1c

    .line 791
    :pswitch_7
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 792
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentType:Ljava/lang/String;

    move-object/from16 v26, v0

    const-string v27, "game"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-nez v26, :cond_24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamerscore:I

    move/from16 v26, v0

    if-lez v26, :cond_27

    .line 795
    :cond_24
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, ""

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamerscore:I

    move/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    const/16 v28, 0x0

    invoke-static/range {v26 .. v28}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 796
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 797
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isRareAchievement()Z

    move-result v27

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 804
    :goto_1d
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 805
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_25

    .line 806
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->backgroundImage:Ljava/lang/String;

    move-object/from16 v27, v0

    const v28, 0x7f020053

    const v29, 0x7f020055

    invoke-static/range {v26 .. v29}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V

    .line 808
    :cond_25
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_28

    const/16 v26, 0x1

    :goto_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 809
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 810
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 811
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 812
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 813
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 814
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 815
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_26

    .line 816
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 818
    :cond_26
    const/4 v10, 0x0

    .line 819
    .restart local v10    # "hasBackgroundColor":Z
    goto/16 :goto_16

    .line 799
    .end local v10    # "hasBackgroundColor":Z
    :cond_27
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 800
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 801
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    goto/16 :goto_1d

    .line 808
    :cond_28
    const/16 v26, 0x0

    goto/16 :goto_1e

    .line 822
    :pswitch_8
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 823
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_29

    .line 824
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const v27, 0x7f070fb8

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    .line 826
    :cond_29
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 827
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 828
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 829
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 830
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 831
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 832
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2a

    .line 833
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipThumbnail:Ljava/lang/String;

    move-object/from16 v27, v0

    const v28, 0x7f02017f

    const v29, 0x7f02017f

    invoke-static/range {v26 .. v29}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V

    .line 835
    :cond_2a
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_2b

    const/16 v26, 0x1

    :goto_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 836
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 837
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 838
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 839
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 840
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 841
    const/4 v10, 0x0

    .line 842
    .restart local v10    # "hasBackgroundColor":Z
    goto/16 :goto_16

    .line 835
    .end local v10    # "hasBackgroundColor":Z
    :cond_2b
    const/16 v26, 0x0

    goto :goto_1f

    .line 845
    :pswitch_9
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 846
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 847
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 848
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 849
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 850
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 851
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2c

    .line 852
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotThumbnail:Ljava/lang/String;

    move-object/from16 v27, v0

    const v28, 0x7f020127

    const v29, 0x7f020127

    invoke-static/range {v26 .. v29}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V

    .line 854
    :cond_2c
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_2d

    const/16 v26, 0x1

    :goto_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 855
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 856
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 857
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 858
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 859
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 860
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 861
    const/4 v10, 0x0

    .line 862
    .restart local v10    # "hasBackgroundColor":Z
    goto/16 :goto_16

    .line 854
    .end local v10    # "hasBackgroundColor":Z
    :cond_2d
    const/16 v26, 0x0

    goto :goto_20

    .line 865
    :pswitch_a
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 866
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2e

    .line 867
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const v27, 0x7f070ed2

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    .line 869
    :cond_2e
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 870
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 871
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 872
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 873
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2f

    .line 874
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentImageUri:Ljava/lang/String;

    move-object/from16 v27, v0

    const v28, 0x7f02017f

    const v29, 0x7f02017f

    invoke-static/range {v26 .. v29}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V

    .line 876
    :cond_2f
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 877
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 878
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 879
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 880
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 881
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 882
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 883
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 884
    const/4 v10, 0x0

    .line 885
    .restart local v10    # "hasBackgroundColor":Z
    goto/16 :goto_16

    .line 888
    .end local v10    # "hasBackgroundColor":Z
    :pswitch_b
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 889
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_30

    const/16 v26, 0x1

    :goto_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 890
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 891
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 892
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 893
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 894
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 895
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 896
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 897
    const/4 v10, 0x0

    .line 898
    .restart local v10    # "hasBackgroundColor":Z
    goto/16 :goto_16

    .line 889
    .end local v10    # "hasBackgroundColor":Z
    :cond_30
    const/16 v26, 0x0

    goto :goto_21

    .line 901
    :pswitch_c
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 902
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 903
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 904
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 905
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_31

    const/16 v26, 0x1

    :goto_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 906
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 907
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 908
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 909
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 910
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 911
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 912
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 913
    const/4 v10, 0x1

    .line 914
    .restart local v10    # "hasBackgroundColor":Z
    goto/16 :goto_16

    .line 905
    .end local v10    # "hasBackgroundColor":Z
    :cond_31
    const/16 v26, 0x0

    goto :goto_22

    .line 917
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_35

    const/4 v11, 0x1

    .line 918
    .local v11, "hasItemImage":Z
    :goto_23
    if-eqz v11, :cond_32

    .line 919
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    move-object/from16 v27, v0

    const v28, 0x7f020127

    const v29, 0x7f020127

    invoke-static/range {v26 .. v29}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V

    .line 921
    :cond_32
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v0, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 922
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v0, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 923
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v27, v0

    if-nez v11, :cond_33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->postDetails:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    move-object/from16 v26, v0

    if-eqz v26, :cond_36

    :cond_33
    const/16 v26, 0x1

    :goto_24
    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 924
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->ugcCaption:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->ugcCaption:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_37

    const/16 v26, 0x1

    :goto_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 925
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 926
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 927
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 928
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 929
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 930
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 931
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->postDetails:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    move-object/from16 v26, v0

    if-eqz v26, :cond_34

    .line 932
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->postDetails:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->title:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_38

    const/16 v26, 0x1

    :goto_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->postDetails:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->title:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 933
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->postDetails:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->displayLink:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_39

    const/16 v26, 0x1

    :goto_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->postDetails:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->displayLink:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 935
    :cond_34
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 936
    if-nez v11, :cond_3a

    const/4 v10, 0x1

    .line 937
    .restart local v10    # "hasBackgroundColor":Z
    :goto_28
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 938
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 939
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    goto/16 :goto_16

    .line 917
    .end local v10    # "hasBackgroundColor":Z
    .end local v11    # "hasItemImage":Z
    :cond_35
    const/4 v11, 0x0

    goto/16 :goto_23

    .line 923
    .restart local v11    # "hasItemImage":Z
    :cond_36
    const/16 v26, 0x0

    goto/16 :goto_24

    .line 924
    :cond_37
    const/16 v26, 0x0

    goto/16 :goto_25

    .line 932
    :cond_38
    const/16 v26, 0x0

    goto/16 :goto_26

    .line 933
    :cond_39
    const/16 v26, 0x0

    goto :goto_27

    .line 936
    :cond_3a
    const/4 v10, 0x0

    goto :goto_28

    .line 943
    .end local v11    # "hasItemImage":Z
    :pswitch_e
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 944
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_3b

    .line 945
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const v27, 0x7f070ed2

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    .line 947
    :cond_3b
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 948
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 949
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 950
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 951
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_3c

    .line 952
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentImageUri:Ljava/lang/String;

    move-object/from16 v27, v0

    const v28, 0x7f02017f

    const v29, 0x7f02017f

    invoke-static/range {v26 .. v29}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V

    .line 954
    :cond_3c
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_3d

    const/16 v26, 0x1

    :goto_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 955
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 956
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 957
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 958
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 959
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 960
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 961
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 962
    const/4 v10, 0x0

    .line 963
    .restart local v10    # "hasBackgroundColor":Z
    goto/16 :goto_16

    .line 954
    .end local v10    # "hasBackgroundColor":Z
    :cond_3d
    const/16 v26, 0x0

    goto :goto_29

    .line 965
    :pswitch_f
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 966
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 967
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 968
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 970
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->data:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 971
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->data:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;

    move-object/from16 v26, v0

    if-eqz v26, :cond_44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->data:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;->leaderboardEntries:Ljava/util/List;

    move-object/from16 v26, v0

    .line 972
    invoke-static/range {v26 .. v26}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v26

    if-nez v26, :cond_44

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    if-eqz v26, :cond_44

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_44

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardSubtitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_44

    .line 976
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->data:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;->leaderboardEntries:Ljava/util/List;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-interface/range {v26 .. v27}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;

    .line 977
    .local v16, "primaryEntry":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 978
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    if-eqz v26, :cond_41

    .line 979
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v26, v0

    if-eqz v26, :cond_3e

    .line 980
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerContainer:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Landroid/view/View;->setBackgroundColor(I)V

    .line 982
    :cond_3e
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->imageUrl:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 986
    :goto_2a
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v27

    const v28, 0x7f070600

    const/16 v29, 0x2

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->rank:I

    move/from16 v31, v0

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v29, v30

    const/16 v30, 0x1

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->name:Ljava/lang/String;

    move-object/from16 v31, v0

    aput-object v31, v29, v30

    invoke-virtual/range {v27 .. v29}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 987
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v27

    const v28, 0x7f0705ff

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->value:I

    move/from16 v31, v0

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v29, v30

    invoke-virtual/range {v27 .. v29}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 988
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerContainer:Landroid/view/View;

    move-object/from16 v26, v0

    if-eqz v26, :cond_40

    .line 989
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->data:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;->leaderboardEntries:Ljava/util/List;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-le v0, v1, :cond_43

    .line 990
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/view/View;->setVisibility(I)V

    .line 991
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->data:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;->leaderboardEntries:Ljava/util/List;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-interface/range {v26 .. v27}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;

    .line 992
    .local v18, "secondaryEntry":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 993
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    if-eqz v26, :cond_42

    .line 994
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v26, v0

    if-eqz v26, :cond_3f

    .line 995
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerContainer:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Landroid/view/View;->setBackgroundColor(I)V

    .line 997
    :cond_3f
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->imageUrl:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 1001
    :goto_2b
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v27

    const v28, 0x7f070600

    const/16 v29, 0x2

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->rank:I

    move/from16 v31, v0

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v29, v30

    const/16 v30, 0x1

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->name:Ljava/lang/String;

    move-object/from16 v31, v0

    aput-object v31, v29, v30

    invoke-virtual/range {v27 .. v29}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1002
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v27

    const v28, 0x7f0705ff

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->value:I

    move/from16 v31, v0

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v29, v30

    invoke-virtual/range {v27 .. v29}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1007
    .end local v18    # "secondaryEntry":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;
    :cond_40
    :goto_2c
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->leaderboardDescription:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1008
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardSubtitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1012
    .end local v16    # "primaryEntry":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;
    :goto_2d
    const/4 v10, 0x0

    .line 1013
    .restart local v10    # "hasBackgroundColor":Z
    goto/16 :goto_16

    .line 984
    .end local v10    # "hasBackgroundColor":Z
    .restart local v16    # "primaryEntry":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;
    :cond_41
    sget-object v26, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->TAG:Ljava/lang/String;

    const-string v27, "Primary user info is null"

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2a

    .line 999
    .restart local v18    # "secondaryEntry":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;
    :cond_42
    sget-object v26, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->TAG:Ljava/lang/String;

    const-string v27, "Secondary user info is null"

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2b

    .line 1004
    .end local v18    # "secondaryEntry":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;
    :cond_43
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x8

    invoke-virtual/range {v26 .. v27}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2c

    .line 1010
    .end local v16    # "primaryEntry":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;
    :cond_44
    sget-object v26, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->TAG:Ljava/lang/String;

    const-string v27, "Leaderboard data is null"

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2d

    .line 1015
    :pswitch_10
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1016
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1017
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1018
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1019
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1020
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1021
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1022
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1023
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1024
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1025
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1026
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1031
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->metadataLayout:Landroid/view/ViewGroup;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    if-eqz v26, :cond_45

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->name:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->name:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_48

    :cond_45
    const/16 v26, 0x1

    :goto_2e
    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1033
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    if-eqz v26, :cond_46

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgHostInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v26, v0

    if-eqz v26, :cond_46

    .line 1034
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgHostInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1037
    :cond_46
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 1039
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgStartTime:Ljava/util/Date;

    move-object/from16 v26, v0

    if-eqz v26, :cond_49

    .line 1040
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgPostedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgStartTime:Ljava/util/Date;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemainingShort(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 1044
    :goto_2f
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgDescription:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgDescription:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 1046
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTitleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_47

    .line 1047
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTitleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentImageUri:Ljava/lang/String;

    move-object/from16 v27, v0

    const v28, 0x7f02017f

    const v29, 0x7f02017f

    invoke-static/range {v26 .. v29}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V

    .line 1050
    :cond_47
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 1051
    .local v21, "systemTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1053
    .local v8, "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgTags:Ljava/util/List;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v26

    if-nez v26, :cond_4b

    .line 1054
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgTags:Ljava/util/List;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :goto_30
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_4b

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 1055
    .local v23, "tagId":Ljava/lang/String;
    sget-object v27, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getTag(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    move-result-object v22

    .line 1057
    .local v22, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    if-eqz v22, :cond_4a

    .line 1058
    invoke-interface/range {v21 .. v22}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_30

    .line 1031
    .end local v8    # "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    .end local v21    # "systemTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    .end local v22    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    .end local v23    # "tagId":Ljava/lang/String;
    :cond_48
    const/16 v26, 0x0

    goto/16 :goto_2e

    .line 1042
    :cond_49
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgPostedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgPostedTime:Ljava/util/Date;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getDurationSinceNowUptoDayOrShortDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_2f

    .line 1060
    .restart local v8    # "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    .restart local v21    # "systemTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    .restart local v22    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    .restart local v23    # "tagId":Ljava/lang/String;
    :cond_4a
    invoke-static/range {v23 .. v23}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_30

    .line 1065
    .end local v22    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    .end local v23    # "tagId":Ljava/lang/String;
    :cond_4b
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v26, v0

    if-eqz v26, :cond_4d

    .line 1066
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgHostInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getSecondaryColor()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->setBackgroundColor(I)V

    .line 1067
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->clear()V

    .line 1069
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgAchievementTags:Ljava/util/List;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v26

    if-nez v26, :cond_4c

    .line 1070
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgAchievementTags:Ljava/util/List;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :goto_31
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_4c

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LfgAchievementTag;

    .line 1071
    .local v22, "tag":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LfgAchievementTag;
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v27, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LfgAchievementTag;->Id:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LfgAchievementTag;->Name:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LfgAchievementTag;->Gamerscore:I

    move/from16 v30, v0

    .line 1074
    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v30

    .line 1071
    invoke-static/range {v28 .. v30}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_31

    .line 1078
    .end local v22    # "tag":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LfgAchievementTag;
    :cond_4c
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 1079
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 1080
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->notifyDataSetChanged()V

    .line 1083
    :cond_4d
    const/4 v10, 0x1

    .line 1084
    .restart local v10    # "hasBackgroundColor":Z
    goto/16 :goto_16

    .line 1086
    .end local v8    # "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    .end local v10    # "hasBackgroundColor":Z
    .end local v21    # "systemTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    :pswitch_11
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1087
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const v27, 0x7f07102e

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;I)V

    .line 1088
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1089
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1090
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1091
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1092
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1093
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1095
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_4e

    .line 1096
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentSharingArt:Ljava/lang/String;

    move-object/from16 v27, v0

    const v28, 0x7f0201fb

    const v29, 0x7f0201fb

    invoke-static/range {v26 .. v29}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V

    .line 1099
    :cond_4e
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_4f

    const/16 v26, 0x1

    :goto_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemText:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move/from16 v1, v26

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 1100
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1101
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1102
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1103
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1104
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 1105
    const/4 v10, 0x0

    .line 1106
    .restart local v10    # "hasBackgroundColor":Z
    goto/16 :goto_16

    .line 1099
    .end local v10    # "hasBackgroundColor":Z
    :cond_4f
    const/16 v26, 0x0

    goto :goto_32

    .line 1111
    :cond_50
    const/16 v26, 0x0

    goto/16 :goto_15

    .line 1127
    .restart local v10    # "hasBackgroundColor":Z
    :cond_51
    const/4 v6, 0x0

    goto/16 :goto_17

    .restart local v6    # "color":I
    .restart local v14    # "minHeight":I
    .restart local v15    # "padding":I
    .restart local v17    # "scale":F
    .restart local v24    # "targetUserColor":I
    .restart local v25    # "viewParent":Landroid/view/View;
    :cond_52
    move/from16 v6, v24

    .line 1146
    goto/16 :goto_18

    .line 1149
    .end local v24    # "targetUserColor":I
    :cond_53
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v26, v0

    if-eqz v26, :cond_54

    .line 1150
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v4

    .line 1151
    .local v4, "authorInfoColor":I
    if-nez v4, :cond_55

    sget-object v26, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v27, 0x7f0c009c

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 1154
    .end local v4    # "authorInfoColor":I
    :cond_54
    :goto_33
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/16 v27, 0x13

    invoke-virtual/range {v26 .. v27}, Landroid/widget/TextView;->setGravity(I)V

    goto/16 :goto_18

    .restart local v4    # "authorInfoColor":I
    :cond_55
    move v6, v4

    .line 1151
    goto :goto_33

    .line 1171
    .end local v4    # "authorInfoColor":I
    .end local v14    # "minHeight":I
    .end local v15    # "padding":I
    .end local v17    # "scale":F
    .end local v25    # "viewParent":Landroid/view/View;
    :cond_56
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v26

    if-eqz v26, :cond_0

    .line 1172
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    if-eqz v26, :cond_57

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v26, v0

    if-eqz v26, :cond_57

    .line 1173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v6

    .line 1175
    :cond_57
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    if-eqz v26, :cond_58

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v26, v0

    if-eqz v26, :cond_58

    .line 1176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v6

    .line 1178
    :cond_58
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    move-object/from16 v26, v0

    if-eqz v26, :cond_0

    .line 1179
    if-eqz v10, :cond_59

    move v5, v6

    .line 1180
    .local v5, "backColor":I
    :goto_34
    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_0

    .line 1179
    .end local v5    # "backColor":I
    :cond_59
    const/4 v5, 0x0

    goto :goto_34

    .line 622
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 670
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch

    .line 761
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public static bindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 7
    .param p0, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 594
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->getItemViewType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    move-result-object v2

    .line 595
    .local v2, "viewType":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Leaderboard:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v3, v4, :cond_2

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->description:Ljava/lang/String;

    .line 596
    .local v1, "description":Ljava/lang/String;
    :goto_0
    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getProfileStatus()Lcom/microsoft/xbox/service/model/UserStatus;

    move-result-object v4

    invoke-static {p1, v3, v1, v4, p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindView(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)V

    .line 597
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Shared:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    if-ne v2, v3, :cond_1

    .line 598
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v4, 0x7f0e08ba

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 599
    .local v0, "attachmentView":Landroid/view/View;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    if-nez v3, :cond_0

    if-eqz v0, :cond_0

    .line 600
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createAttachmentHolder(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 603
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    if-eqz v3, :cond_1

    .line 604
    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalShortDescription:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    invoke-static {p1, v3, v4, v5, v6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindView(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)V

    .line 607
    .end local v0    # "attachmentView":Landroid/view/View;
    :cond_1
    return-void

    .line 595
    .end local v1    # "description":Ljava/lang/String;
    :cond_2
    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->shortDescription:Ljava/lang/String;

    goto :goto_0
.end method

.method private static createAttachmentHolder(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .locals 2
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 149
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    const v1, 0x7f0e08ba

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;-><init>(Landroid/view/View;)V

    .line 150
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->initAsAttachement()V

    .line 151
    return-object v0
.end method

.method public static createSocialRecommendationViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .locals 3
    .param p0, "resId"    # I
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 128
    const/4 v2, 0x0

    invoke-virtual {p2, p0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 129
    .local v1, "v":Landroid/view/View;
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;-><init>(Landroid/view/View;)V

    .line 130
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->initAsSocialRecommendation()V

    .line 131
    return-object v0
.end method

.method private static createTrendingHolder(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .locals 2
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 155
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    const v1, 0x7f0e06d4

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;-><init>(Landroid/view/View;)V

    .line 156
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->initAsTrendingContent()V

    .line 157
    return-object v0
.end method

.method public static createTrendingViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .locals 3
    .param p0, "resId"    # I
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 135
    const/4 v2, 0x0

    invoke-virtual {p2, p0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 136
    .local v1, "v":Landroid/view/View;
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;-><init>(Landroid/view/View;)V

    .line 137
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->init()V

    .line 138
    const v2, 0x7f0e06d4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createTrendingHolder(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 139
    return-object v0
.end method

.method public static createView(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;Z)Landroid/view/View;
    .locals 2
    .param p0, "resId"    # I
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "bindShared"    # Z

    .prologue
    .line 143
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;Z)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v0

    .line 144
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 145
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    return-object v1
.end method

.method public static createView(Landroid/view/ViewGroup;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 2
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "viewType"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 110
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createViewHolder(Landroid/view/ViewGroup;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;Landroid/view/LayoutInflater;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v0

    .line 111
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 112
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    return-object v1
.end method

.method public static createViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;Z)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .locals 4
    .param p0, "resId"    # I
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "bindShared"    # Z

    .prologue
    .line 116
    const/4 v2, 0x0

    invoke-virtual {p2, p0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 117
    .local v1, "v":Landroid/view/View;
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;-><init>(Landroid/view/View;)V

    .line 118
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->init()V

    .line 120
    if-eqz p3, :cond_0

    .line 121
    const v2, 0x7f0e08ba

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createAttachmentHolder(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 122
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    iput-object v3, v2, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->parent:Landroid/view/View;

    .line 124
    :cond_0
    return-object v0
.end method

.method public static createViewHolder(Landroid/view/ViewGroup;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;Landroid/view/LayoutInflater;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .locals 3
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "viewType"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    const/4 v2, 0x0

    .line 86
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$1;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$PeopleActivityFeedListAdapterUtil$ViewType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 105
    const v0, 0x7f0301ab

    invoke-static {v0, p0, p2, v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;Z)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v0

    :goto_0
    return-object v0

    .line 88
    :pswitch_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    const v1, 0x7f0301ad

    invoke-virtual {p2, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 90
    :pswitch_1
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    const v1, 0x7f0301b9

    invoke-virtual {p2, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 92
    :pswitch_2
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    const v1, 0x7f03013c

    invoke-virtual {p2, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 94
    :pswitch_3
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    const v1, 0x7f030138

    invoke-virtual {p2, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 96
    :pswitch_4
    const v0, 0x7f0301ae

    invoke-static {v0, p0, p2, v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;Z)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v0

    goto :goto_0

    .line 98
    :pswitch_5
    const v0, 0x7f0301b0

    const/4 v1, 0x1

    invoke-static {v0, p0, p2, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;Z)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v0

    goto :goto_0

    .line 100
    :pswitch_6
    const v0, 0x7f03013a

    invoke-static {v0, p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createSocialRecommendationViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v0

    goto :goto_0

    .line 102
    :pswitch_7
    const v0, 0x7f03013b

    invoke-static {v0, p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createTrendingViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v0

    goto :goto_0

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static getActivtyDescription(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p1, "shortDescription"    # Ljava/lang/String;

    .prologue
    .line 1186
    const/4 v0, 0x0

    .line 1187
    .local v0, "result":Ljava/lang/String;
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 1188
    move-object v0, p1

    .line 1189
    const-string v1, "Xbox360"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->platform:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1190
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070580

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1193
    :cond_0
    return-object v0
.end method

.method private static getAttachmentNarratorContent(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)Ljava/lang/String;
    .locals 3
    .param p0, "attachmentHolder"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .prologue
    const/16 v2, 0x2c

    .line 206
    if-nez p0, :cond_0

    .line 207
    const-string v1, ""

    .line 223
    :goto_0
    return-object v1

    .line 208
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 209
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 210
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->realnameView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 211
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->actionView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 213
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 215
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 216
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 217
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 219
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 221
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 223
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getClubFormattedGamertag(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "gamertag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p0, "roles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    const v7, 0x7f0701e5

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1503
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {p0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1504
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f07030b

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1505
    .local v1, "owner":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v4

    aput-object p1, v3, v5

    invoke-virtual {v2, v7, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 1510
    .end local v1    # "owner":Ljava/lang/String;
    .end local p1    # "gamertag":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 1506
    .restart local p1    # "gamertag":Ljava/lang/String;
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {p0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1507
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f070309

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1508
    .local v0, "moderator":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v4

    aput-object p1, v3, v5

    invoke-virtual {v2, v7, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static getItemViewType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;
    .locals 1
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 71
    if-eqz p0, :cond_3

    .line 72
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getIsHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Hidden:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    .line 82
    :goto_0
    return-object v0

    .line 74
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Shared:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isRecommendation()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 77
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Recommendations:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isTrending()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 79
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Trending:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0

    .line 82
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Normal:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0
.end method

.method private static getTrendingNarratorContent(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)Ljava/lang/String;
    .locals 3
    .param p0, "trendingHolder"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .prologue
    const/16 v2, 0x2c

    .line 227
    if-nez p0, :cond_0

    .line 228
    const-string v1, ""

    .line 243
    :goto_0
    return-object v1

    .line 229
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 230
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->ugcCaption:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 231
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 232
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 233
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 234
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 236
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 237
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 239
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 240
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->realnameView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 241
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->contentTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 243
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method static synthetic lambda$attachSocialBarListeners$0(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Landroid/view/View;)V
    .locals 4
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 249
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 250
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz p0, :cond_0

    if-eqz v0, :cond_0

    .line 251
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v1, v1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackLike(ZLjava/lang/String;Ljava/lang/String;)V

    .line 252
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->likeClick(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    :cond_0
    return-void

    .line 251
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$attachSocialBarListeners$1(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Landroid/view/View;)V
    .locals 2
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 259
    if-eqz p0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackActivityFeedComment(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->navigateToPostCommentScreen(Ljava/lang/String;)V

    .line 263
    :cond_0
    return-void
.end method

.method static synthetic lambda$attachSocialBarListeners$10(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Landroid/view/View;)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 466
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->removeActivityFeedItem(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$attachSocialBarListeners$2(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Landroid/view/View;)V
    .locals 0
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 286
    if-eqz p0, :cond_0

    .line 287
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackShare(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 288
    invoke-virtual {p1, p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->navigateToShareScreen(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 290
    :cond_0
    return-void
.end method

.method static synthetic lambda$attachSocialBarListeners$5(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;ZZZZZLcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;ZLandroid/view/View;)V
    .locals 17
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p1, "canHide"    # Z
    .param p2, "isHomeFeed"    # Z
    .param p3, "canReport"    # Z
    .param p4, "canDelete"    # Z
    .param p5, "canPin"    # Z
    .param p6, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p7, "canUnpin"    # Z
    .param p8, "v1"    # Landroid/view/View;

    .prologue
    .line 306
    new-instance v12, Landroid/widget/PopupMenu;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-direct {v12, v2, v0}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 307
    .local v12, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f0f0001

    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v14

    invoke-virtual {v2, v3, v14}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->timeline:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->timeline:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;->timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    sget-object v3, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    if-ne v2, v3, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->timeline:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;

    iget-wide v4, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;->timelineId:J

    .line 309
    .local v4, "clubId":J
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-wide v6, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->titleId:J

    .line 310
    .local v6, "titleId":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->getUnfollowableTitles()Ljava/util/List;

    move-result-object v13

    .line 311
    .local v13, "unfollowableTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$UnfollowableTitle;>;"
    if-nez p1, :cond_0

    .line 312
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c16

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 314
    :cond_0
    if-eqz p2, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-nez v2, :cond_2

    .line 315
    :cond_1
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c17

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 317
    :cond_2
    if-eqz p2, :cond_3

    const-wide/16 v2, 0x0

    cmp-long v2, v6, v2

    if-eqz v2, :cond_3

    invoke-static {v13}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 318
    :cond_3
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c18

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 320
    :cond_4
    const/4 v10, 0x0

    .line 321
    .local v10, "isClubAdmin":Z
    const/4 v8, 0x0

    .line 323
    .local v8, "canReportToClubAdmin":Z
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-lez v2, :cond_5

    .line 324
    sget-object v2, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v9

    .line 325
    .local v9, "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsMemberOf()Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v8, 0x1

    .line 326
    :goto_1
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsAdminOf()Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v10, 0x1

    .line 328
    .end local v9    # "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    :cond_5
    :goto_2
    if-eqz p3, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 329
    :cond_6
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c19

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 330
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c1a

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 331
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c1b

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 341
    :goto_3
    if-nez p4, :cond_7

    if-nez v10, :cond_7

    .line 342
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c1e

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 345
    :cond_7
    if-eqz p5, :cond_e

    .line 347
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c1c

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v11

    .line 348
    .local v11, "pinItem":Landroid/view/MenuItem;
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$1;->$SwitchMap$com$microsoft$xbox$service$activityFeed$UserPostsDataTypes$TimelineType:[I

    invoke-virtual/range {p6 .. p6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 362
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->TAG:Ljava/lang/String;

    const-string v3, "Unexpected timeline type when modifying pin menu item: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual/range {p6 .. p6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v3, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    .end local v11    # "pinItem":Landroid/view/MenuItem;
    :goto_4
    if-nez p7, :cond_8

    .line 370
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c1d

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    :cond_8
    move-object/from16 v2, p6

    move-object/from16 v3, p0

    .line 373
    invoke-static/range {v2 .. v7}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;JJ)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 429
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->show()V

    .line 430
    return-void

    .line 308
    .end local v4    # "clubId":J
    .end local v6    # "titleId":J
    .end local v8    # "canReportToClubAdmin":Z
    .end local v10    # "isClubAdmin":Z
    .end local v13    # "unfollowableTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$UnfollowableTitle;>;"
    :cond_9
    const-wide/16 v4, 0x0

    goto/16 :goto_0

    .line 325
    .restart local v4    # "clubId":J
    .restart local v6    # "titleId":J
    .restart local v8    # "canReportToClubAdmin":Z
    .restart local v9    # "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .restart local v10    # "isClubAdmin":Z
    .restart local v13    # "unfollowableTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$UnfollowableTitle;>;"
    :cond_a
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 326
    :cond_b
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 333
    .end local v9    # "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    :cond_c
    if-eqz v8, :cond_d

    .line 334
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c19

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_3

    .line 336
    :cond_d
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c1a

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 337
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c1b

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    goto/16 :goto_3

    .line 350
    .restart local v11    # "pinItem":Landroid/view/MenuItem;
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_4

    .line 353
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_4

    .line 356
    :pswitch_2
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700e3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_4

    .line 359
    :pswitch_3
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700e2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_4

    .line 366
    .end local v11    # "pinItem":Landroid/view/MenuItem;
    :cond_e
    invoke-virtual {v12}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c1c

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    goto/16 :goto_4

    .line 348
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic lambda$attachSocialBarListeners$6(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Landroid/view/View;)V
    .locals 2
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 436
    if-eqz p0, :cond_0

    .line 437
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackSocial(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Ljava/lang/String;)V

    .line 438
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {p1, p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->navigateToActionsScreen(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 440
    :cond_0
    return-void
.end method

.method static synthetic lambda$attachSocialBarListeners$7(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Landroid/view/View;)V
    .locals 2
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 445
    if-eqz p0, :cond_0

    .line 446
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackSocial(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Ljava/lang/String;)V

    .line 447
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {p1, p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->navigateToActionsScreen(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 449
    :cond_0
    return-void
.end method

.method static synthetic lambda$attachSocialBarListeners$8(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Landroid/view/View;)V
    .locals 2
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 454
    if-eqz p0, :cond_0

    .line 455
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackSocial(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Ljava/lang/String;)V

    .line 456
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {p1, p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->navigateToActionsScreen(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 458
    :cond_0
    return-void
.end method

.method static synthetic lambda$attachSocialBarListeners$9(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Landroid/view/View;)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 462
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->unhideActivityFeedItem(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$bindSocialRecommendationView$11(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "vw"    # Landroid/view/View;

    .prologue
    .line 489
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackSuggestedFriendSeeAll()V

    .line 490
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->navigateToSuggestedPeopleScreen()V

    .line 491
    return-void
.end method

.method static synthetic lambda$bindSocialRecommendationView$12(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Landroid/view/View;)V
    .locals 1
    .param p0, "person"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p2, "vw"    # Landroid/view/View;

    .prologue
    .line 497
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackSuggestedFriendViewProfile(Ljava/lang/String;)V

    .line 499
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->navigateToUserProfile(Ljava/lang/String;)V

    .line 501
    :cond_0
    return-void
.end method

.method static synthetic lambda$bindSocialRecommendationView$13(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Landroid/view/View;)V
    .locals 1
    .param p0, "person"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .param p1, "socialRecommendations"    # Ljava/util/ArrayList;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p3, "vw"    # Landroid/view/View;

    .prologue
    .line 506
    if-eqz p0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackSuggestedFriendRemove(Ljava/lang/String;)V

    .line 509
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 510
    invoke-virtual {p2, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->deleteSocialRecommendationItem(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/util/List;)V

    .line 512
    :cond_1
    return-void
.end method

.method static synthetic lambda$bindSocialRecommendationView$14(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/ArrayList;Landroid/view/View;)V
    .locals 4
    .param p0, "person"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p2, "socialRecommendations"    # Ljava/util/ArrayList;
    .param p3, "vw"    # Landroid/view/View;

    .prologue
    .line 517
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackSuggestedFriendAdd(Ljava/lang/String;)V

    .line 519
    instance-of v2, p1, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 520
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 522
    .local v1, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v1, :cond_3

    .line 523
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recommendation:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;->getRecommendationType()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->FacebookFriend:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    if-ne v2, v3, :cond_1

    const/4 v0, 0x1

    .line 525
    .local v0, "isFacebookFriend":Z
    :goto_0
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->shouldAutoAddFollowing(ZLcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 526
    invoke-virtual {p1, p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addFollowingUser(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/util/List;)V

    .line 527
    invoke-virtual {p1, p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToShareIdentityList(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 528
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->trackChangeRelationshipDoneAutoAdd(Ljava/lang/String;Z)V

    .line 536
    .end local v0    # "isFacebookFriend":Z
    .end local p1    # "viewModel":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    :cond_0
    :goto_1
    return-void

    .line 523
    .restart local p1    # "viewModel":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 530
    .restart local v0    # "isFacebookFriend":Z
    :cond_2
    instance-of v2, p1, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;

    if-eqz v2, :cond_0

    .line 531
    check-cast p1, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;

    .end local p1    # "viewModel":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    invoke-virtual {p1, p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->showChangeFriendshipDialog(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    goto :goto_1

    .line 534
    .end local v0    # "isFacebookFriend":Z
    .restart local p1    # "viewModel":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    :cond_3
    const-string v2, "Unexpected null meProfile"

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic lambda$bindTrendingView$15(Landroid/view/View;)V
    .locals 3
    .param p0, "vw"    # Landroid/view/View;

    .prologue
    .line 574
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackTrendingSeeAll()V

    .line 575
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/trending/TrendingPivotScreen;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 576
    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V
    .locals 4
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    .prologue
    .line 390
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 391
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    .line 393
    .local v0, "locatorForDeletion":Ljava/lang/String;
    :goto_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->pinned:Z

    if-eqz v1, :cond_3

    .line 395
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2, v3, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->unpinActivityFeedItem(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 400
    .end local v0    # "locatorForDeletion":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 391
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    goto :goto_0

    .line 397
    .restart local v0    # "locatorForDeletion":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->deleteActivityFeedItem(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;JJLandroid/view/MenuItem;)Z
    .locals 10
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "clubId"    # J
    .param p4, "titleId"    # J
    .param p6, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 374
    invoke-interface/range {p6 .. p6}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 427
    :cond_0
    :goto_0
    return v8

    .line 376
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->SocialBar:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 379
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsFeed:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 382
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsFeed:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move v5, v8

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 385
    :pswitch_3
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0700bc

    .line 386
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700bb

    .line 387
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070738

    .line 388
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$16;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070736

    .line 401
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 385
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 404
    :pswitch_4
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->hideActivityFeedItem(Ljava/lang/String;)V

    goto :goto_0

    .line 409
    :pswitch_5
    invoke-virtual {p0, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->hideActivitiesFromClub(J)V

    goto :goto_0

    .line 412
    :pswitch_6
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->getUnfollowableTitles()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p4, p5, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->hideActivitiesFromTitle(JLjava/util/List;)V

    goto :goto_0

    .line 415
    :pswitch_7
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 416
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->checkAndPinActivityFeedItem(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 420
    :pswitch_8
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 421
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-virtual {p0, v0, v5, v6, v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->unpinActivityFeedItem(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 374
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0c16
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_7
        :pswitch_8
        :pswitch_3
    .end packed-switch
.end method

.method public static setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V
    .locals 0
    .param p0, "img"    # Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "loadingResourceId"    # I
    .param p3, "errorResourceId"    # I

    .prologue
    .line 1488
    if-eqz p0, :cond_0

    .line 1489
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 1491
    :cond_0
    return-void
.end method

.method public static setNarratorContent(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;)V
    .locals 4
    .param p0, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .param p1, "viewType"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    .prologue
    const/16 v3, 0x2c

    .line 161
    if-eqz p0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    if-nez v1, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$1;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$PeopleActivityFeedListAdapterUtil$ViewType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 202
    :goto_1
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 168
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 169
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->realnameView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 170
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->dateView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveContentDescriptionIfNotNullAndVisible(Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->pinIcon:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveContentDescriptionIfNotNullAndVisible(Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 172
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->actionView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 174
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->ugcCaption:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 176
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 178
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 179
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 182
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 184
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 187
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->realnameView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 189
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->dateView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveContentDescriptionIfNotNullAndVisible(Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 190
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->actionView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 192
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->ugcCaption:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 193
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->getAttachmentNarratorContent(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 196
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->trendingDescription:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 197
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->getTrendingNarratorContent(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 166
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private static setProfileImage(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;)V
    .locals 1
    .param p0, "imageView"    # Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const v0, 0x7f020125

    .line 1494
    if-eqz p0, :cond_0

    .line 1495
    invoke-virtual {p0, p1, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 1500
    :cond_0
    return-void
.end method
