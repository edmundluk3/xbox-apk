.class public Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ConnectDialog.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/IViewUpdate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;
    }
.end annotation


# static fields
.field private static final STATE_LOADED:I = 0x0

.field private static final STATE_LOADING:I = 0x2

.field private static final STATE_WIFI_ERROR:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final adp:Landroid/widget/BaseAdapter;

.field private final closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private connectionFaqHandler:Landroid/view/View$OnClickListener;

.field private consoles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;"
        }
    .end annotation
.end field

.field private final container:Landroid/widget/ListView;

.field private displayErrorOnDisconnect:Z

.field private final enterIpListener:Landroid/view/View$OnClickListener;

.field private faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private footerFaqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final helpForWifiNotConnectedButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final inflater:Landroid/view/LayoutInflater;

.field private noConsolesFound:Z

.field private postOOBE:Z

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 86
    const v1, 0x7f080238

    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 72
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->displayErrorOnDisconnect:Z

    .line 78
    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->connectionFaqHandler:Landroid/view/View$OnClickListener;

    .line 342
    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$5;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$5;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->enterIpListener:Landroid/view/View$OnClickListener;

    .line 351
    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->adp:Landroid/widget/BaseAdapter;

    .line 87
    const v1, 0x7f0300ac

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setContentView(I)V

    .line 88
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setCanceledOnTouchOutside(Z)V

    .line 89
    const v1, 0x7f0e0423

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 90
    const v1, 0x7f0e042b

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->container:Landroid/widget/ListView;

    .line 91
    const v1, 0x7f0e0422

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 92
    const v1, 0x7f0e0424

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->helpForWifiNotConnectedButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 93
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->inflater:Landroid/view/LayoutInflater;

    .line 95
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->isConnectedToWifi()Z

    move-result v0

    .line 96
    .local v0, "wifiConnected":Z
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 98
    if-eqz v0, :cond_3

    .line 99
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;-><init>(Lcom/microsoft/xbox/toolkit/IViewUpdate;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    .line 100
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->getDiscoveredConsoles()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->consoles:Ljava/util/List;

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->container:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->adp:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 110
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->isAmazonDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->forceKindleRespectDimOptions()V

    .line 113
    :cond_0
    return-void

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->consoles:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 103
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->close()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->consoles:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->clearErrors()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->inflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->noConsolesFound:Z

    return v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->noConsolesFound:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->enterIpListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->footerFaqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;Lcom/microsoft/xbox/toolkit/ui/XLEButton;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->footerFaqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setUpHelpFaqButton(Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Landroid/widget/BaseAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->adp:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method private bindResult()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 228
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->getTryingConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    .line 229
    .local v0, "currentConsole":Lcom/microsoft/xbox/xle/model/ConsoleData;
    if-eqz v0, :cond_0

    .line 230
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    .line 231
    .local v3, "lastResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->getConsoleState(Lcom/microsoft/xbox/xle/model/ConsoleData;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    move-result-object v4

    .line 232
    .local v4, "state":Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    sget-object v5, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Console state: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    sget-object v5, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$7;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$ConnectDialogViewModel$ConsoleState:[I

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 259
    .end local v3    # "lastResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    .end local v4    # "state":Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    :cond_0
    :goto_0
    return-void

    .line 235
    .restart local v3    # "lastResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    .restart local v4    # "state":Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    :pswitch_0
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->displayErrorOnDisconnect:Z

    .line 237
    :pswitch_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->clearErrors()V

    goto :goto_0

    .line 240
    :pswitch_2
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    .line 241
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    if-eqz v1, :cond_1

    .line 242
    sget-object v5, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->TAG:Ljava/lang/String;

    const-string v6, "Got error"

    invoke-static {v5, v6, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 244
    :cond_1
    iput-boolean v8, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->displayErrorOnDisconnect:Z

    .line 245
    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setError(Lcom/microsoft/xbox/xle/model/ConsoleData;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0

    .line 248
    .end local v1    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :pswitch_3
    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->displayErrorOnDisconnect:Z

    if-eqz v5, :cond_0

    .line 249
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    .line 250
    .local v2, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    iput-boolean v8, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->displayErrorOnDisconnect:Z

    .line 251
    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setError(Lcom/microsoft/xbox/xle/model/ConsoleData;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0

    .line 233
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private clearErrors()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    .line 326
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->container:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 327
    .local v1, "first":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->container:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v3

    .line 328
    .local v3, "last":I
    move v2, v1

    .local v2, "i":I
    :goto_0
    if-gt v2, v3, :cond_1

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->consoles:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 329
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->container:Landroid/widget/ListView;

    sub-int v6, v2, v1

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 330
    .local v4, "v":Landroid/view/View;
    const v5, 0x7f0e0434

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 331
    .local v0, "errMsg":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v5

    if-eq v5, v7, :cond_0

    .line 332
    const-string v5, ""

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 328
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 337
    .end local v0    # "errMsg":Landroid/widget/TextView;
    .end local v4    # "v":Landroid/view/View;
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v5, :cond_2

    .line 338
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 340
    :cond_2
    return-void
.end method

.method private close()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->close()V

    .line 145
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissConnectDialog()V

    goto :goto_0
.end method

.method private getDiscoveredConsoles()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->getDiscoveredConsoles()Ljava/util/List;

    move-result-object v0

    .line 117
    .local v0, "modelConsoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/model/ConsoleData;>;"
    if-nez v0, :cond_0

    .line 118
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 120
    :cond_0
    return-object v0
.end method

.method private isConnectedToWifi()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 262
    const/4 v1, 0x0

    .line 263
    .local v1, "isConnected":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 265
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 266
    .local v2, "wifiNetwork":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v3

    .line 268
    :goto_0
    return v1

    .line 266
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setError(Lcom/microsoft/xbox/xle/model/ConsoleData;Lcom/microsoft/xbox/toolkit/XLEException;)V
    .locals 12
    .param p1, "currentConsole"    # Lcom/microsoft/xbox/xle/model/ConsoleData;
    .param p2, "e"    # Lcom/microsoft/xbox/toolkit/XLEException;

    .prologue
    const/4 v11, 0x0

    .line 277
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->consoles:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    .line 278
    .local v5, "idx":I
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->container:Landroid/widget/ListView;

    invoke-virtual {v8}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v4

    .line 279
    .local v4, "first":I
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->container:Landroid/widget/ListView;

    invoke-virtual {v8}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v6

    .line 280
    .local v6, "last":I
    const-wide/16 v2, 0x0

    .line 281
    .local v2, "errorCode":J
    if-eqz p2, :cond_0

    .line 282
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/XLEException;->getUserObject()Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_4

    const-wide/16 v2, 0x0

    .line 284
    :cond_0
    :goto_0
    if-gt v4, v5, :cond_3

    if-gt v5, v6, :cond_3

    .line 285
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->container:Landroid/widget/ListView;

    sub-int v9, v5, v4

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 286
    .local v7, "v":Landroid/view/View;
    const v8, 0x7f0e0434

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 287
    .local v0, "errMsg":Landroid/widget/TextView;
    if-eqz v0, :cond_1

    .line 288
    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->getConnectionErrorStringFromException(Lcom/microsoft/xbox/toolkit/XLEException;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 291
    :cond_1
    const-string v8, "Manual Console Connect: errorCode %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 292
    .local v1, "errorDescription":Ljava/lang/String;
    invoke-static {v1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 294
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-nez v8, :cond_2

    .line 295
    const v8, 0x7f0e0435

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v8, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 297
    :cond_2
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v8, :cond_3

    .line 298
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-direct {p0, v8}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setUpHelpFaqButton(Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V

    .line 299
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v8, v11}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 302
    .end local v0    # "errMsg":Landroid/widget/TextView;
    .end local v1    # "errorDescription":Ljava/lang/String;
    .end local v7    # "v":Landroid/view/View;
    :cond_3
    return-void

    .line 282
    :cond_4
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/XLEException;->getUserObject()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_0
.end method

.method private setUpHelpFaqButton(Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V
    .locals 1
    .param p1, "button"    # Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .prologue
    .line 272
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getPaddingBottom()I

    move-result v0

    or-int/lit8 v0, v0, 0x8

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setPaintFlags(I)V

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->connectionFaqHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    return-void
.end method

.method public static setVisible(Landroid/view/View;Z)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "visible"    # Z

    .prologue
    .line 590
    invoke-virtual {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 591
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 592
    return-void

    .line 591
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->close()V

    .line 134
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 176
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPostOOBEStatus()Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->postOOBE:Z

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->onResume()V

    .line 189
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->helpForWifiNotConnectedButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setUpHelpFaqButton(Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V

    .line 197
    return-void

    .line 180
    :cond_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->postOOBE:Z

    if-nez v0, :cond_0

    .line 181
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->onPause()V

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->helpForWifiNotConnectedButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->helpForWifiNotConnectedButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->footerFaqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_3

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->footerFaqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    :cond_3
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->postOOBE:Z

    if-nez v0, :cond_4

    .line 218
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$4;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)V

    const-wide/16 v2, 0x320

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 225
    :cond_4
    return-void
.end method

.method public setErrorMessage(Lcom/microsoft/xbox/xle/model/ConsoleData;Ljava/lang/String;)V
    .locals 8
    .param p1, "currentConsole"    # Lcom/microsoft/xbox/xle/model/ConsoleData;
    .param p2, "errorMsg"    # Ljava/lang/String;

    .prologue
    .line 305
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->consoles:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 306
    .local v2, "idx":I
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->container:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 307
    .local v1, "first":I
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->container:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v3

    .line 308
    .local v3, "last":I
    if-gt v1, v2, :cond_1

    if-gt v2, v3, :cond_1

    .line 309
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->clearErrors()V

    .line 311
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->container:Landroid/widget/ListView;

    sub-int v7, v2, v1

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 312
    .local v5, "v":Landroid/view/View;
    const v6, 0x7f0e0434

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 313
    .local v0, "errMsg":Landroid/widget/TextView;
    const v6, 0x7f0e0431

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 315
    .local v4, "powerOnOffConnectView":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 316
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 319
    :cond_0
    if-eqz v4, :cond_1

    .line 320
    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 323
    .end local v0    # "errMsg":Landroid/widget/TextView;
    .end local v4    # "powerOnOffConnectView":Landroid/widget/LinearLayout;
    .end local v5    # "v":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public setOnConnectHandler(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "onConnectHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->setOnConnectHandler(Ljava/lang/Runnable;)V

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->TAG:Ljava/lang/String;

    const-string v1, "Cannot set onConnectHandler because there is no view model"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateView(Ljava/lang/Object;)V
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->getDiscoveredConsoles()Ljava/util/List;

    move-result-object v1

    .line 151
    .local v1, "modelConsoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/model/ConsoleData;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 153
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 154
    .local v0, "d":Lcom/microsoft/xbox/service/model/UpdateData;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->ConsoleDiscovery:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 156
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 160
    .end local v0    # "d":Lcom/microsoft/xbox/service/model/UpdateData;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v2

    if-nez v2, :cond_2

    .line 161
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->consoles:Ljava/util/List;

    if-eq v2, v1, :cond_1

    .line 162
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->consoles:Ljava/util/List;

    .line 165
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->consoles:Ljava/util/List;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->consoles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 166
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->bindResult()V

    .line 171
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->adp:Landroid/widget/BaseAdapter;

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 172
    return-void

    .line 167
    :cond_3
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->postOOBE:Z

    if-nez v2, :cond_2

    .line 168
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissConnectDialog()V

    goto :goto_0
.end method
