.class public final Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;
.super Ljava/lang/Object;
.source "UploadCustomPicTask.java"


# static fields
.field private static final BLOCK_SIZE:J = 0x80000L

.field private static final WRITE_TIMEOUT_MINS:I = 0xa


# instance fields
.field private final associatedId:J

.field private final filePath:Ljava/lang/String;

.field private final mediaHubService:Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

.field private final picType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

.field private final publishClient:Lokhttp3/OkHttpClient;

.field private final uploadFileClient:Lokhttp3/OkHttpClient;


# direct methods
.method private constructor <init>(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Ljava/lang/String;Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;)V
    .locals 5
    .param p1, "associatedId"    # J
    .param p3, "picType"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
    .param p4, "filePath"    # Ljava/lang/String;
    .param p5, "mediaHubService"    # Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->associatedId:J

    .line 65
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->picType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 66
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->filePath:Ljava/lang/String;

    .line 67
    iput-object p5, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->mediaHubService:Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    .line 69
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory;->getDefaultLoggingInterceptor()Lokhttp3/Interceptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    const-wide/16 v2, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    .line 71
    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->uploadFileClient:Lokhttp3/OkHttpClient;

    .line 74
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory;->getXTokenOkHttpBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 75
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory;->getDefaultLoggingInterceptor()Lokhttp3/Interceptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;-><init>()V

    .line 76
    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->publishClient:Lokhttp3/OkHttpClient;

    .line 78
    return-void
.end method

.method private getEncodedBlockId(J)Ljava/lang/String;
    .locals 7
    .param p1, "blockNumber"    # J
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 144
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BlockId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%07d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "blockId":Ljava/lang/String;
    invoke-static {}, Lcom/google/common/io/BaseEncoding;->base64()Lcom/google/common/io/BaseEncoding;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/common/io/BaseEncoding;->encode([B)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method static synthetic lambda$publish$5(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;Ljava/lang/String;)Ljava/lang/Object;
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;
    .param p1, "publishUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 150
    new-instance v2, Lokhttp3/Request$Builder;

    invoke-direct {v2}, Lokhttp3/Request$Builder;-><init>()V

    .line 151
    invoke-virtual {v2, p1}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [B

    .line 152
    invoke-static {v3, v4}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;[B)Lokhttp3/RequestBody;

    move-result-object v3

    invoke-virtual {v2, v3}, Lokhttp3/Request$Builder;->post(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v2

    .line 153
    invoke-virtual {v2}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    .line 155
    .local v0, "publishRequest":Lokhttp3/Request;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->publishClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v2, v0}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v2

    invoke-interface {v2}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object v1

    .line 157
    .local v1, "publishResponse":Lokhttp3/Response;
    invoke-virtual {v1}, Lokhttp3/Response;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 158
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    return-object v2

    .line 160
    :cond_0
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to publish: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lokhttp3/Response;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method static synthetic lambda$splitFileIntoChunks$4(Ljava/io/File;Lio/reactivex/ObservableEmitter;)V
    .locals 10
    .param p0, "file"    # Ljava/io/File;
    .param p1, "e"    # Lio/reactivex/ObservableEmitter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/32 v8, 0x80000

    .line 108
    invoke-static {p0}, Lcom/google/common/io/Files;->asByteSource(Ljava/io/File;)Lcom/google/common/io/ByteSource;

    move-result-object v1

    .line 109
    .local v1, "byteSource":Lcom/google/common/io/ByteSource;
    invoke-virtual {v1}, Lcom/google/common/io/ByteSource;->size()J

    move-result-wide v4

    .line 110
    .local v4, "fileSize":J
    const-wide/16 v6, 0x0

    .line 112
    .local v6, "offset":J
    :goto_0
    cmp-long v3, v6, v4

    if-gez v3, :cond_0

    .line 113
    invoke-virtual {v1, v6, v7, v8, v9}, Lcom/google/common/io/ByteSource;->slice(JJ)Lcom/google/common/io/ByteSource;

    move-result-object v2

    .line 114
    .local v2, "currentBlock":Lcom/google/common/io/ByteSource;
    invoke-virtual {v2}, Lcom/google/common/io/ByteSource;->read()[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/primitives/Bytes;->asList([B)Ljava/util/List;

    move-result-object v0

    .line 116
    .local v0, "blockBytes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Byte;>;"
    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    .line 118
    add-long/2addr v6, v8

    .line 119
    goto :goto_0

    .line 121
    .end local v0    # "blockBytes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Byte;>;"
    .end local v2    # "currentBlock":Lcom/google/common/io/ByteSource;
    :cond_0
    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->onComplete()V

    .line 122
    return-void
.end method

.method static synthetic lambda$upload$0(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;Ljava/io/File;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;)Lio/reactivex/SingleSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;
    .param p1, "fileToUpload"    # Ljava/io/File;
    .param p2, "response"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p2, p1}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->uploadFile(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;Ljava/io/File;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$upload$1(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;)Lio/reactivex/CompletableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;
    .param p1, "response"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;->publishUri()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->publish(Ljava/lang/String;)Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$uploadFile$2(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;Ljava/lang/Integer;Ljava/util/List;)Ljava/lang/Integer;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;
    .param p1, "response"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    .param p2, "blockCount"    # Ljava/lang/Integer;
    .param p3, "blockBytes"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 99
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;->contentUploadUri()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p3}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->uploadBlock(Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$uploadFile$3(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    .locals 0
    .param p0, "response"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    .param p1, "ignore"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 103
    return-object p0
.end method

.method private publish(Ljava/lang/String;)Lio/reactivex/Completable;
    .locals 1
    .param p1, "publishUrl"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Completable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method private splitFileIntoChunks(Ljava/io/File;)Lio/reactivex/Observable;
    .locals 1
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 107
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask$$Lambda$5;->lambdaFactory$(Ljava/io/File;)Lio/reactivex/ObservableOnSubscribe;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private uploadBlock(Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;)Ljava/lang/Integer;
    .locals 8
    .param p1, "blockNumber"    # Ljava/lang/Integer;
    .param p2, "baseUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    .local p3, "bytes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Byte;>;"
    const-string v3, "%s&comp=block&blockId=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-long v6, v6

    invoke-direct {p0, v6, v7}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->getEncodedBlockId(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "blockUrl":Ljava/lang/String;
    new-instance v3, Lokhttp3/Request$Builder;

    invoke-direct {v3}, Lokhttp3/Request$Builder;-><init>()V

    .line 129
    invoke-virtual {v3, v0}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v3

    const/4 v4, 0x0

    .line 130
    invoke-static {p3}, Lcom/google/common/primitives/Bytes;->toArray(Ljava/util/Collection;)[B

    move-result-object v5

    invoke-static {v4, v5}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;[B)Lokhttp3/RequestBody;

    move-result-object v4

    invoke-virtual {v3, v4}, Lokhttp3/Request$Builder;->put(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v3

    .line 131
    invoke-virtual {v3}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v1

    .line 133
    .local v1, "uploadBlockRequest":Lokhttp3/Request;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->uploadFileClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v3, v1}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v3

    invoke-interface {v3}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object v2

    .line 135
    .local v2, "uploadBlockResponse":Lokhttp3/Response;
    invoke-virtual {v2}, Lokhttp3/Response;->isSuccessful()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 136
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    return-object v3

    .line 138
    :cond_0
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to upload block: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lokhttp3/Response;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private uploadFile(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;Ljava/io/File;)Lio/reactivex/Single;
    .locals 3
    .param p1, "response"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    .param p2, "fileToUpload"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;",
            "Ljava/io/File;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->splitFileIntoChunks(Ljava/io/File;)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 99
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;)Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->reduce(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 103
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 96
    return-object v0
.end method

.method public static with(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Ljava/lang/String;Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;)Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;
    .locals 8
    .param p0, "associatedId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "picType"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "filePath"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "mediaHubService"    # Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 51
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 52
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 53
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 54
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 56
    new-instance v1, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;-><init>(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Ljava/lang/String;Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;)V

    return-object v1
.end method


# virtual methods
.method public upload()Lio/reactivex/Completable;
    .locals 12

    .prologue
    .line 81
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->filePath:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 82
    .local v1, "fileToUpload":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 83
    .local v2, "fileSize":J
    long-to-double v6, v2

    const-wide/high16 v8, 0x4120000000000000L    # 524288.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    .line 85
    .local v4, "totalBlocks":D
    double-to-long v6, v4

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->picType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    iget-wide v10, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->associatedId:J

    .line 88
    invoke-static {v8, v10, v11}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;->with(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;J)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    move-result-object v8

    .line 85
    invoke-static {v6, v7, v2, v3, v8}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;->with(JJLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;

    move-result-object v0

    .line 90
    .local v0, "createRequest":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;->mediaHubService:Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-interface {v6, v0}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->createCustomPicRx(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;)Lio/reactivex/Single;

    move-result-object v6

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;Ljava/io/File;)Lio/reactivex/functions/Function;

    move-result-object v7

    .line 91
    invoke-virtual {v6, v7}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v6

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicTask;)Lio/reactivex/functions/Function;

    move-result-object v7

    .line 92
    invoke-virtual {v6, v7}, Lio/reactivex/Single;->flatMapCompletable(Lio/reactivex/functions/Function;)Lio/reactivex/Completable;

    move-result-object v6

    .line 90
    return-object v6
.end method
