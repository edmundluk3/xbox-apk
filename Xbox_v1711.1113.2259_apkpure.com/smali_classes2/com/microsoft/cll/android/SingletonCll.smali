.class public Lcom/microsoft/cll/android/SingletonCll;
.super Ljava/lang/Object;
.source "SingletonCll.java"

# interfaces
.implements Lcom/microsoft/cll/android/ICll;
.implements Lcom/microsoft/telemetry/IChannel;


# static fields
.field private static Instance:Lcom/microsoft/cll/android/SingletonCll;

.field private static InstanceLock:Ljava/lang/Object;


# instance fields
.field protected final TAG:Ljava/lang/String;

.field protected final clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

.field protected final cllEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/ICllEvents;",
            ">;"
        }
    .end annotation
.end field

.field public correlationVector:Lcom/microsoft/cll/android/CorrelationVector;

.field protected eventHandler:Lcom/microsoft/cll/android/EventHandler;

.field private executor:Ljava/util/concurrent/ScheduledExecutorService;

.field private final isChanging:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final isPaused:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final isStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected logger:Lcom/microsoft/cll/android/ILogger;

.field protected partA:Lcom/microsoft/cll/android/PartA;

.field protected settingsSync:Lcom/microsoft/cll/android/SettingsSync;

.field protected snapshotScheduler:Lcom/microsoft/cll/android/SnapshotScheduler;

.field private ticketCallback:Lcom/microsoft/cll/android/ITicketCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/cll/android/SingletonCll;->InstanceLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/PartA;Lcom/microsoft/cll/android/CorrelationVector;)V
    .locals 4
    .param p1, "iKey"    # Ljava/lang/String;
    .param p2, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p3, "eventDir"    # Ljava/lang/String;
    .param p4, "partA"    # Lcom/microsoft/cll/android/PartA;
    .param p5, "correlationVector"    # Lcom/microsoft/cll/android/CorrelationVector;

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "Cll"

    iput-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->TAG:Ljava/lang/String;

    .line 54
    if-eqz p1, :cond_0

    const-string v0, ""

    if-ne p1, v0, :cond_1

    .line 55
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "iKey cannot be null or \"\""

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_1
    sget-object v0, Lcom/microsoft/cll/android/Verbosity;->NONE:Lcom/microsoft/cll/android/Verbosity;

    invoke-interface {p2, v0}, Lcom/microsoft/cll/android/ILogger;->setVerbosity(Lcom/microsoft/cll/android/Verbosity;)V

    .line 60
    iput-object p5, p0, Lcom/microsoft/cll/android/SingletonCll;->correlationVector:Lcom/microsoft/cll/android/CorrelationVector;

    .line 61
    iput-object p2, p0, Lcom/microsoft/cll/android/SingletonCll;->logger:Lcom/microsoft/cll/android/ILogger;

    .line 62
    iput-object p4, p0, Lcom/microsoft/cll/android/SingletonCll;->partA:Lcom/microsoft/cll/android/PartA;

    .line 63
    new-instance v0, Lcom/microsoft/cll/android/ClientTelemetry;

    invoke-direct {v0}, Lcom/microsoft/cll/android/ClientTelemetry;-><init>()V

    iput-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->cllEvents:Ljava/util/List;

    .line 65
    new-instance v0, Lcom/microsoft/cll/android/EventHandler;

    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    iget-object v2, p0, Lcom/microsoft/cll/android/SingletonCll;->cllEvents:Ljava/util/List;

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/microsoft/cll/android/EventHandler;-><init>(Lcom/microsoft/cll/android/ClientTelemetry;Ljava/util/List;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->eventHandler:Lcom/microsoft/cll/android/EventHandler;

    .line 66
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isChanging:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 67
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isPaused:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 69
    new-instance v0, Lcom/microsoft/cll/android/SettingsSync;

    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    invoke-direct {v0, v1, p2, p1, p4}, Lcom/microsoft/cll/android/SettingsSync;-><init>(Lcom/microsoft/cll/android/ClientTelemetry;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/PartA;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->settingsSync:Lcom/microsoft/cll/android/SettingsSync;

    .line 70
    new-instance v0, Lcom/microsoft/cll/android/SnapshotScheduler;

    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    invoke-direct {v0, v1, p2, p0}, Lcom/microsoft/cll/android/SnapshotScheduler;-><init>(Lcom/microsoft/cll/android/ClientTelemetry;Lcom/microsoft/cll/android/ILogger;Lcom/microsoft/cll/android/ICll;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->snapshotScheduler:Lcom/microsoft/cll/android/SnapshotScheduler;

    .line 72
    sget-object v0, Lcom/microsoft/cll/android/SettingsStore$Settings;->VORTEXPRODURL:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v0}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsString(Lcom/microsoft/cll/android/SettingsStore$Settings;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/cll/android/SingletonCll;->setEndpointUrl(Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public static getInstance(Ljava/lang/String;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/PartA;Lcom/microsoft/cll/android/CorrelationVector;)Lcom/microsoft/cll/android/ICll;
    .locals 7
    .param p0, "iKey"    # Ljava/lang/String;
    .param p1, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p2, "eventDir"    # Ljava/lang/String;
    .param p3, "partA"    # Lcom/microsoft/cll/android/PartA;
    .param p4, "correlationVector"    # Lcom/microsoft/cll/android/CorrelationVector;

    .prologue
    .line 39
    sget-object v0, Lcom/microsoft/cll/android/SingletonCll;->Instance:Lcom/microsoft/cll/android/SingletonCll;

    if-nez v0, :cond_1

    .line 40
    sget-object v6, Lcom/microsoft/cll/android/SingletonCll;->InstanceLock:Ljava/lang/Object;

    monitor-enter v6

    .line 41
    :try_start_0
    sget-object v0, Lcom/microsoft/cll/android/SingletonCll;->Instance:Lcom/microsoft/cll/android/SingletonCll;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/microsoft/cll/android/SingletonCll;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/cll/android/SingletonCll;-><init>(Ljava/lang/String;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/PartA;Lcom/microsoft/cll/android/CorrelationVector;)V

    sput-object v0, Lcom/microsoft/cll/android/SingletonCll;->Instance:Lcom/microsoft/cll/android/SingletonCll;

    .line 44
    :cond_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    :cond_1
    sget-object v0, Lcom/microsoft/cll/android/SingletonCll;->Instance:Lcom/microsoft/cll/android/SingletonCll;

    return-object v0

    .line 44
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public SubscribeCllEvents(Lcom/microsoft/cll/android/ICllEvents;)V
    .locals 0
    .param p1, "cllEvents"    # Lcom/microsoft/cll/android/ICllEvents;

    .prologue
    .line 271
    return-void
.end method

.method public getAppUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->partA:Lcom/microsoft/cll/android/PartA;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/PartA;->getAppUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public varargs log(Lcom/microsoft/cll/android/PreSerializedEvent;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 5
    .param p1, "event"    # Lcom/microsoft/cll/android/PreSerializedEvent;
    .param p3, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/cll/android/PreSerializedEvent;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 185
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/microsoft/cll/android/PreSerializedJsonSerializable;

    iget-object v1, p1, Lcom/microsoft/cll/android/PreSerializedEvent;->data:Ljava/lang/String;

    iget-object v2, p1, Lcom/microsoft/cll/android/PreSerializedEvent;->partCName:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/cll/android/PreSerializedEvent;->partBName:Ljava/lang/String;

    iget-object v4, p1, Lcom/microsoft/cll/android/PreSerializedEvent;->attributes:Ljava/util/Map;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/cll/android/PreSerializedJsonSerializable;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 186
    .local v0, "preSerializedJsonSerializable":Lcom/microsoft/cll/android/PreSerializedJsonSerializable;
    invoke-virtual {p0, v0, p2, p3}, Lcom/microsoft/cll/android/SingletonCll;->log(Lcom/microsoft/telemetry/Base;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 187
    return-void
.end method

.method public varargs log(Lcom/microsoft/telemetry/Base;Ljava/util/List;Ljava/util/Map;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 4
    .param p1, "event"    # Lcom/microsoft/telemetry/Base;
    .param p4, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/telemetry/Base;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 210
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "tags":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->isStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    .line 212
    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "Cll"

    const-string v3, "Cll must be started before logging events"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :goto_0
    return-void

    .line 214
    :cond_0
    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->ticketCallback:Lcom/microsoft/cll/android/ITicketCallback;

    if-nez v1, :cond_1

    .line 215
    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "Cll"

    const-string v3, "You must set the ticket callback if you want to log ids with your events"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 219
    :cond_1
    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->partA:Lcom/microsoft/cll/android/PartA;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/microsoft/cll/android/PartA;->populate(Lcom/microsoft/telemetry/Base;Ljava/util/List;Ljava/util/Map;[Lcom/microsoft/cll/android/EventSensitivity;)Lcom/microsoft/cll/android/SerializedEvent;

    move-result-object v0

    .line 220
    .local v0, "serializedEvent":Lcom/microsoft/cll/android/SerializedEvent;
    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->eventHandler:Lcom/microsoft/cll/android/EventHandler;

    invoke-virtual {v1, v0, p2}, Lcom/microsoft/cll/android/EventHandler;->log(Lcom/microsoft/cll/android/SerializedEvent;Ljava/util/List;)Z

    goto :goto_0
.end method

.method public varargs log(Lcom/microsoft/telemetry/Base;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 1
    .param p1, "event"    # Lcom/microsoft/telemetry/Base;
    .param p3, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/telemetry/Base;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 202
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/microsoft/cll/android/SingletonCll;->log(Lcom/microsoft/telemetry/Base;Ljava/util/List;Ljava/util/Map;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 203
    return-void
.end method

.method public log(Lcom/microsoft/telemetry/Base;Ljava/util/Map;)V
    .locals 1
    .param p1, "event"    # Lcom/microsoft/telemetry/Base;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/telemetry/Base;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "tags":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 206
    invoke-virtual {p0, p1, v0, p2, v0}, Lcom/microsoft/cll/android/SingletonCll;->log(Lcom/microsoft/telemetry/Base;Ljava/util/List;Ljava/util/Map;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 207
    return-void
.end method

.method public varargs log(Lcom/microsoft/telemetry/Base;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 1
    .param p1, "event"    # Lcom/microsoft/telemetry/Base;
    .param p2, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;

    .prologue
    const/4 v0, 0x0

    .line 197
    invoke-virtual {p0, p1, v0, v0, p2}, Lcom/microsoft/cll/android/SingletonCll;->log(Lcom/microsoft/telemetry/Base;Ljava/util/List;Ljava/util/Map;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 198
    return-void
.end method

.method public pause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 130
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isChanging:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isPaused:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->eventHandler:Lcom/microsoft/cll/android/EventHandler;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/EventHandler;->pause()V

    .line 133
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->settingsSync:Lcom/microsoft/cll/android/SettingsSync;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/SettingsSync;->pause()V

    .line 134
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->snapshotScheduler:Lcom/microsoft/cll/android/SnapshotScheduler;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/SnapshotScheduler;->pause()V

    .line 135
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 137
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isPaused:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isChanging:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 142
    :cond_1
    return-void
.end method

.method public resume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 152
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isChanging:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isPaused:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    sget-object v0, Lcom/microsoft/cll/android/SettingsStore$Settings;->THREADSTOUSEWITHEXECUTOR:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v0}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 157
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->snapshotScheduler:Lcom/microsoft/cll/android/SnapshotScheduler;

    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {v0, v1}, Lcom/microsoft/cll/android/SnapshotScheduler;->resume(Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 158
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->eventHandler:Lcom/microsoft/cll/android/EventHandler;

    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {v0, v1}, Lcom/microsoft/cll/android/EventHandler;->resume(Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 159
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->settingsSync:Lcom/microsoft/cll/android/SettingsSync;

    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {v0, v1}, Lcom/microsoft/cll/android/SettingsSync;->resume(Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 161
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isPaused:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isChanging:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 166
    :cond_1
    return-void
.end method

.method public send()V
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->eventHandler:Lcom/microsoft/cll/android/EventHandler;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/EventHandler;->send()Z

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v1, "Cll"

    const-string v2, "Cannot send while the CLL is stopped."

    invoke-interface {v0, v1, v2}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAppUserId(Ljava/lang/String;)V
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->partA:Lcom/microsoft/cll/android/PartA;

    invoke-virtual {v0, p1}, Lcom/microsoft/cll/android/PartA;->setAppUserId(Ljava/lang/String;)V

    .line 284
    return-void
.end method

.method public setDebugVerbosity(Lcom/microsoft/cll/android/Verbosity;)V
    .locals 1
    .param p1, "verbosity"    # Lcom/microsoft/cll/android/Verbosity;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->logger:Lcom/microsoft/cll/android/ILogger;

    invoke-interface {v0, p1}, Lcom/microsoft/cll/android/ILogger;->setVerbosity(Lcom/microsoft/cll/android/Verbosity;)V

    .line 175
    return-void
.end method

.method public setEndpointUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 244
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->eventHandler:Lcom/microsoft/cll/android/EventHandler;

    invoke-virtual {v0, p1}, Lcom/microsoft/cll/android/EventHandler;->setEndpointUrl(Ljava/lang/String;)V

    .line 245
    return-void
.end method

.method protected setEventSender(Lcom/microsoft/cll/android/EventSender;)V
    .locals 1
    .param p1, "sender"    # Lcom/microsoft/cll/android/EventSender;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->eventHandler:Lcom/microsoft/cll/android/EventHandler;

    invoke-virtual {v0, p1}, Lcom/microsoft/cll/android/EventHandler;->setSender(Lcom/microsoft/cll/android/EventSender;)V

    .line 280
    return-void
.end method

.method public setExperimentId(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 261
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->partA:Lcom/microsoft/cll/android/PartA;

    invoke-virtual {v0, p1}, Lcom/microsoft/cll/android/PartA;->setExpId(Ljava/lang/String;)V

    .line 262
    return-void
.end method

.method public setXuidCallback(Lcom/microsoft/cll/android/ITicketCallback;)V
    .locals 3
    .param p1, "callback"    # Lcom/microsoft/cll/android/ITicketCallback;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/microsoft/cll/android/SingletonCll;->ticketCallback:Lcom/microsoft/cll/android/ITicketCallback;

    .line 292
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isPaused:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v1, "Cll"

    const-string v2, "Xuid callback must be set before start."

    invoke-interface {v0, v1, v2}, Lcom/microsoft/cll/android/ILogger;->warn(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :goto_0
    return-void

    .line 297
    :cond_1
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->eventHandler:Lcom/microsoft/cll/android/EventHandler;

    invoke-virtual {v0, p1}, Lcom/microsoft/cll/android/EventHandler;->setXuidCallback(Lcom/microsoft/cll/android/ITicketCallback;)V

    goto :goto_0
.end method

.method public start()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isChanging:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 85
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->snapshotScheduler:Lcom/microsoft/cll/android/SnapshotScheduler;

    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {v0, v1}, Lcom/microsoft/cll/android/SnapshotScheduler;->start(Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 86
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->eventHandler:Lcom/microsoft/cll/android/EventHandler;

    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {v0, v1}, Lcom/microsoft/cll/android/EventHandler;->start(Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->settingsSync:Lcom/microsoft/cll/android/SettingsSync;

    iget-object v1, p0, Lcom/microsoft/cll/android/SingletonCll;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {v0, v1}, Lcom/microsoft/cll/android/SettingsSync;->start(Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 89
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->isChanging:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 94
    :cond_1
    return-void
.end method

.method public stop()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 103
    iget-object v2, p0, Lcom/microsoft/cll/android/SingletonCll;->isChanging:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v4, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 105
    iget-object v2, p0, Lcom/microsoft/cll/android/SingletonCll;->isStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    iget-object v2, p0, Lcom/microsoft/cll/android/SingletonCll;->eventHandler:Lcom/microsoft/cll/android/EventHandler;

    invoke-virtual {v2}, Lcom/microsoft/cll/android/EventHandler;->stop()V

    .line 107
    iget-object v2, p0, Lcom/microsoft/cll/android/SingletonCll;->settingsSync:Lcom/microsoft/cll/android/SettingsSync;

    invoke-virtual {v2}, Lcom/microsoft/cll/android/SettingsSync;->stop()V

    .line 108
    iget-object v2, p0, Lcom/microsoft/cll/android/SingletonCll;->snapshotScheduler:Lcom/microsoft/cll/android/SnapshotScheduler;

    invoke-virtual {v2}, Lcom/microsoft/cll/android/SnapshotScheduler;->stop()V

    .line 109
    iget-object v2, p0, Lcom/microsoft/cll/android/SingletonCll;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v2}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 111
    iget-object v2, p0, Lcom/microsoft/cll/android/SingletonCll;->isStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 114
    :cond_0
    iget-object v2, p0, Lcom/microsoft/cll/android/SingletonCll;->cllEvents:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/cll/android/ICllEvents;

    .line 115
    .local v0, "event":Lcom/microsoft/cll/android/ICllEvents;
    invoke-interface {v0}, Lcom/microsoft/cll/android/ICllEvents;->stopped()V

    goto :goto_0

    .line 118
    .end local v0    # "event":Lcom/microsoft/cll/android/ICllEvents;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/cll/android/SingletonCll;->isChanging:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 120
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method public synchronize()V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->eventHandler:Lcom/microsoft/cll/android/EventHandler;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/EventHandler;->synchronize()V

    .line 266
    return-void
.end method

.method public useLegacyCS(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 252
    iget-object v0, p0, Lcom/microsoft/cll/android/SingletonCll;->partA:Lcom/microsoft/cll/android/PartA;

    invoke-virtual {v0, p1}, Lcom/microsoft/cll/android/PartA;->useLegacyCS(Z)V

    .line 253
    return-void
.end method
