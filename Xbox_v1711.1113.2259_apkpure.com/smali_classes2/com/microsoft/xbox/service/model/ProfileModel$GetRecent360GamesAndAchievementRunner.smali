.class Lcom/microsoft/xbox/service/model/ProfileModel$GetRecent360GamesAndAchievementRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetRecent360GamesAndAchievementRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "profileModel"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "xuid"    # Ljava/lang/String;

    .prologue
    .line 3955
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecent360GamesAndAchievementRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 3956
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecent360GamesAndAchievementRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 3957
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecent360GamesAndAchievementRunner;->xuid:Ljava/lang/String;

    .line 3959
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3963
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecent360GamesAndAchievementRunner;->xuid:Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getRecent360ProgressAndAchievementInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3950
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecent360GamesAndAchievementRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 3977
    const-wide/16 v0, 0xbd8

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3972
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecent360GamesAndAchievementRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$4900(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 3973
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 3968
    return-void
.end method
