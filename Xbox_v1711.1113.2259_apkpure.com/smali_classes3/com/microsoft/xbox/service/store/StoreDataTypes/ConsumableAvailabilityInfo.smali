.class public Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;
.super Ljava/lang/Object;
.source "ConsumableAvailabilityInfo.java"


# instance fields
.field public final availabilityId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final currencyCode:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final price:Ljava/math/BigDecimal;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final skuId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final skuTitle:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)V
    .locals 0
    .param p1, "skuId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "skuTitle"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "availabilityId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "price"    # Ljava/math/BigDecimal;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "currencyCode"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 31
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->skuId:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->skuTitle:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->availabilityId:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->price:Ljava/math/BigDecimal;

    .line 37
    iput-object p5, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->currencyCode:Ljava/lang/String;

    .line 38
    return-void
.end method
