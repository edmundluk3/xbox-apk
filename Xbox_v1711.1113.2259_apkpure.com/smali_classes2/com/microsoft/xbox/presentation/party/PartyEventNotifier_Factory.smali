.class public final Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;
.super Ljava/lang/Object;
.source "PartyEventNotifier_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final myXuidProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final navigationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationDisplayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;",
            ">;"
        }
    .end annotation
.end field

.field private final partyChatRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p1, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    .local p2, "myXuidProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;>;"
    .local p3, "navigationManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/ui/NavigationManager;>;"
    .local p4, "notificationDisplayProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;>;"
    .local p5, "partyChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    sget-boolean v0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 35
    sget-boolean v0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->myXuidProvider:Ljavax/inject/Provider;

    .line 37
    sget-boolean v0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->navigationManagerProvider:Ljavax/inject/Provider;

    .line 39
    sget-boolean v0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->notificationDisplayProvider:Ljavax/inject/Provider;

    .line 41
    sget-boolean v0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    .line 43
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    .local p1, "myXuidProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;>;"
    .local p2, "navigationManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/ui/NavigationManager;>;"
    .local p3, "notificationDisplayProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;>;"
    .local p4, "partyChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;
    .locals 6

    .prologue
    .line 47
    new-instance v0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 48
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->myXuidProvider:Ljavax/inject/Provider;

    .line 49
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->navigationManagerProvider:Ljavax/inject/Provider;

    .line 50
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->notificationDisplayProvider:Ljavax/inject/Provider;

    .line 51
    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    .line 52
    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;-><init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)V

    .line 47
    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->get()Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;

    move-result-object v0

    return-object v0
.end method
