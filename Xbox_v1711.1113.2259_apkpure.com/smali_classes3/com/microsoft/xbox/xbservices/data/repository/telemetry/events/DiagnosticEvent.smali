.class public Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;
.super Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;
.source "DiagnosticEvent.java"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V
    .locals 1
    .param p1, "diagnostics"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 10
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;-><init>(Ljava/lang/String;)V

    .line 11
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;-><init>(Ljava/lang/String;)V

    .line 14
    return-void
.end method
