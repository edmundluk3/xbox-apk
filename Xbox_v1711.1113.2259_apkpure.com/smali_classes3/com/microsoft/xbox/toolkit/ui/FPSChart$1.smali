.class Lcom/microsoft/xbox/toolkit/ui/FPSChart$1;
.super Ljava/lang/Object;
.source "FPSChart.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/FPSChart;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/FPSChart;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/FPSChart;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/FPSChart;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/FPSChart;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 62
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/FPSChart;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->access$000(Lcom/microsoft/xbox/toolkit/ui/FPSChart;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 63
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/FPSChart;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->access$100(Lcom/microsoft/xbox/toolkit/ui/FPSChart;)Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Surface;->isValid()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/FPSChart;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->access$100(Lcom/microsoft/xbox/toolkit/ui/FPSChart;)Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 65
    .local v0, "c":Landroid/graphics/Canvas;
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/FPSChart;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->access$200(Lcom/microsoft/xbox/toolkit/ui/FPSChart;Landroid/graphics/Canvas;)V

    .line 66
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/FPSChart;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->access$100(Lcom/microsoft/xbox/toolkit/ui/FPSChart;)Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 69
    const-wide/16 v2, 0x21

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 70
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 75
    .end local v0    # "c":Landroid/graphics/Canvas;
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_1
    return-void
.end method
