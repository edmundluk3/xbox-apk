.class public Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "ThirdPartyNoticeScreenAdapter.java"


# static fields
.field private static final NOTICE_FILENAME:Ljava/lang/String; = "ThirdPartyNotices.txt"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mCloseButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private mNotices:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ThirdPartyNoticeSceenViewModel;)V
    .locals 8
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ThirdPartyNoticeSceenViewModel;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 22
    const-string v4, "ThirdPartyNoticeScreenAdapter"

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter;->TAG:Ljava/lang/String;

    .line 28
    const v4, 0x7f0e0ac0

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter;->mCloseButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 29
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter;->mCloseButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter$1;

    invoke-direct {v5, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/ThirdPartyNoticeSceenViewModel;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    const v4, 0x7f0e0abf

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter;->mNotices:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 40
    const/4 v1, 0x0

    .line 42
    .local v1, "notices":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    const-string v5, "ThirdPartyNotices.txt"

    invoke-virtual {v4, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 45
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .local v2, "sb":Ljava/lang/StringBuilder;
    new-instance v4, Ljava/util/Scanner;

    invoke-direct {v4, v1}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;)V

    const-string v5, "\\Z"

    invoke-virtual {v4, v5}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object v3

    .line 47
    .local v3, "scanner":Ljava/util/Scanner;
    :goto_0
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 48
    invoke-virtual {v3}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 51
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    .end local v3    # "scanner":Ljava/util/Scanner;
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_1
    const-string v4, "ThirdPartyNoticeScreenAdapter"

    const-string v5, "Failed to read third party notices."

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57
    if-eqz v1, :cond_0

    .line 58
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 64
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    :goto_1
    return-void

    .line 50
    .restart local v2    # "sb":Ljava/lang/StringBuilder;
    .restart local v3    # "scanner":Ljava/util/Scanner;
    :cond_1
    :try_start_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter;->mNotices:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\r\n"

    sget-object v7, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    if-eqz v1, :cond_0

    .line 58
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 60
    :catch_1
    move-exception v0

    .line 61
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "ThirdPartyNoticeScreenAdapter"

    const-string v5, "Failed to close stream"

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 60
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    .end local v3    # "scanner":Ljava/util/Scanner;
    .local v0, "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v0

    .line 61
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "ThirdPartyNoticeScreenAdapter"

    const-string v5, "Failed to close stream"

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 53
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 54
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_5
    const-string v4, "ThirdPartyNoticeScreenAdapter"

    const-string v5, "Failed to open third party notices."

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 57
    if-eqz v1, :cond_0

    .line 58
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    .line 60
    :catch_4
    move-exception v0

    .line 61
    const-string v4, "ThirdPartyNoticeScreenAdapter"

    const-string v5, "Failed to close stream"

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 56
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 57
    if-eqz v1, :cond_2

    .line 58
    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 62
    :cond_2
    :goto_2
    throw v4

    .line 60
    :catch_5
    move-exception v0

    .line 61
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "ThirdPartyNoticeScreenAdapter"

    const-string v6, "Failed to close stream"

    invoke-static {v5, v6, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method
