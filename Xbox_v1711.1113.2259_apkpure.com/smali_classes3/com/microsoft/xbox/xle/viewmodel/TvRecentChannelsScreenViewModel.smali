.class public Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "TvRecentChannelsScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;


# instance fields
.field protected contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field protected diagTag:Ljava/lang/String;

.field protected isLoading:Z

.field protected listeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;",
            ">;"
        }
    .end annotation
.end field

.field protected modelChanged:Z

.field protected recentChannels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/epg/EPGChannel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    .line 26
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->recentChannels:Ljava/util/ArrayList;

    .line 35
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvRecentChannelsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 36
    const-string v0, "EPGTvRecentChannelsViewModel"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    .line 37
    return-void
.end method

.method private hasErrors()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 86
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v3

    if-nez v3, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v2

    .line 92
    :cond_1
    const/4 v0, 0x1

    .line 93
    .local v0, "factoryError":Z
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 94
    .local v1, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isError()Z

    move-result v4

    and-int/2addr v0, v4

    .line 95
    goto :goto_1

    .line 97
    .end local v1    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_2
    if-nez v0, :cond_3

    invoke-static {}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->isError()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private refresh()V
    .locals 3

    .prologue
    .line 73
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 76
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 78
    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->refresh()V

    goto :goto_0

    .line 82
    .end local v0    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->refresh()V

    .line 83
    return-void
.end method

.method private setLoading(Z)V
    .locals 3
    .param p1, "loading"    # Z

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->isLoading:Z

    if-ne v0, p1, :cond_0

    .line 115
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set loading "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->isLoading:Z

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->updateViewLoadingIndicator()V

    goto :goto_0

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v1, "Recent Channels adapter was null when it shouldn\'t have been."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V
    .locals 3
    .param p1, "state"    # Lcom/microsoft/xbox/toolkit/network/ListState;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, p1, :cond_1

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->isActive:Z

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->updateAdapter()V

    goto :goto_0
.end method

.method private updateLoadingStatus()V
    .locals 2

    .prologue
    .line 141
    const/4 v0, 0x0

    .line 142
    .local v0, "loading":Z
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->isLoading()Z

    move-result v1

    or-int/2addr v0, v1

    .line 143
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->setLoading(Z)V

    .line 144
    return-void
.end method

.method private updateRecentChannels()V
    .locals 1

    .prologue
    .line 180
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->recentChannels:Ljava/util/ArrayList;

    .line 181
    return-void
.end method

.method private updateViewModelState()V
    .locals 4

    .prologue
    .line 147
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->modelChanged:Z

    .line 150
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->updateLoadingStatus()V

    .line 152
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->isLoading:Z

    if-eqz v2, :cond_0

    .line 177
    :goto_0
    return-void

    .line 156
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->updateRecentChannels()V

    .line 157
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->modelChanged:Z

    .line 160
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->modelChanged:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->isActive:Z

    if-eqz v2, :cond_1

    .line 161
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;

    .line 162
    .local v0, "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;->modelChanged()V

    goto :goto_1

    .line 167
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->hasErrors()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 168
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 175
    .local v1, "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    :goto_2
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_0

    .line 169
    .end local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->getChannelCount()I

    move-result v2

    if-nez v2, :cond_3

    .line 170
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .restart local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    goto :goto_2

    .line 172
    .end local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .restart local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    goto :goto_2
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;

    .prologue
    .line 290
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 292
    :cond_0
    return-void
.end method

.method public getAdapter()Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;

    return-object v0
.end method

.method public bridge synthetic getAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->getAdapter()Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getChannelCount()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->recentChannels:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 69
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->isLoading:Z

    return v0
.end method

.method public load(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 239
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Load "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    if-eqz p1, :cond_0

    .line 241
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->isActive:Z

    if-nez v1, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->requestHeadend()Z

    .line 249
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 250
    .local v0, "currentUser":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_2

    .line 251
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadAsync(Z)V

    .line 254
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->stopLoading()V

    .line 255
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 256
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->refresh()V

    goto :goto_0
.end method

.method public onDataChanged()V
    .locals 5

    .prologue
    .line 271
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->updateViewModelState()V

    .line 273
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->modelChanged:Z

    if-eqz v1, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v1, v2, :cond_0

    .line 277
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;

    .line 278
    .local v0, "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;
    const/4 v2, 0x0

    const v3, 0x7fffffff

    const/4 v4, 0x1

    invoke-interface {v0, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;->dataArrived(IIZ)V

    goto :goto_0

    .line 282
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 286
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvRecentChannelsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 287
    return-void
.end method

.method public onSetActive()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v1, "VM onSetActive"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;->onSetActive()V

    .line 223
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->refresh()V

    .line 224
    return-void
.end method

.method public onSetInactive()V
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v1, "VM onSetInactive"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;->onSetInactive()V

    .line 231
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->stopLoading()V

    .line 234
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->setLoading(Z)V

    .line 235
    return-void
.end method

.method protected onStartOverride()V
    .locals 3

    .prologue
    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v2, "VM onstartOverride"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->start(Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;)V

    .line 196
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->updateLoadingStatus()V

    .line 198
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 199
    .local v0, "currentProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 203
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 204
    return-void
.end method

.method public onStateChanged()V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->updateViewModelState()V

    .line 267
    return-void
.end method

.method protected onStopOverride()V
    .locals 3

    .prologue
    .line 208
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v2, "VM onStopOverride"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->stop(Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;)V

    .line 211
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 212
    .local v0, "currentProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 213
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 216
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 217
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;

    .prologue
    .line 295
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 297
    :cond_0
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->ProfileData:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v0, v1, :cond_2

    .line 129
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->updateRecentChannels()V

    goto :goto_0

    .line 130
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->SessionState:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v0, v1, :cond_0

    .line 131
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;)V

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method
