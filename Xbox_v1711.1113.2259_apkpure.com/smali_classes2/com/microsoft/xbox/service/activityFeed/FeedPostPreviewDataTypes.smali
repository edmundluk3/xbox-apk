.class public final Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes;
.super Ljava/lang/Object;
.source "FeedPostPreviewDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;,
        Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;,
        Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewRequest;,
        Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type shouldn\'t be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
