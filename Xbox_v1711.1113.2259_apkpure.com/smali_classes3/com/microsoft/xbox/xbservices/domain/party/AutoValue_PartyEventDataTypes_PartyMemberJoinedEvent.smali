.class final Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyEventDataTypes_PartyMemberJoinedEvent;
.super Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;
.source "AutoValue_PartyEventDataTypes_PartyMemberJoinedEvent.java"


# instance fields
.field private final member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V
    .locals 2
    .param p1, "member"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;-><init>()V

    .line 14
    if-nez p1, :cond_0

    .line 15
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null member"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyEventDataTypes_PartyMemberJoinedEvent;->member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 35
    if-ne p1, p0, :cond_0

    .line 36
    const/4 v1, 0x1

    .line 42
    :goto_0
    return v1

    .line 38
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 39
    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;

    .line 40
    .local v0, "that":Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyEventDataTypes_PartyMemberJoinedEvent;->member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 42
    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 47
    const/4 v0, 0x1

    .line 48
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyEventDataTypes_PartyMemberJoinedEvent;->member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 50
    return v0
.end method

.method public member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyEventDataTypes_PartyMemberJoinedEvent;->member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PartyMemberJoinedEvent{member="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyEventDataTypes_PartyMemberJoinedEvent;->member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
