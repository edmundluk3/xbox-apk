.class final synthetic Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$44;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/domain/party/PartyInteractor;

.field private final arg$2:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$44;->arg$1:Lcom/microsoft/xbox/domain/party/PartyInteractor;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$44;->arg$2:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$44;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$44;-><init>(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$44;->arg$1:Lcom/microsoft/xbox/domain/party/PartyInteractor;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$44;->arg$2:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    check-cast p1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->lambda$null$4(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
