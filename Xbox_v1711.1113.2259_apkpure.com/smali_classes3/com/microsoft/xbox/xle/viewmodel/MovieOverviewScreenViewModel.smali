.class public Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "MovieOverviewScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;",
        ">;"
    }
.end annotation


# instance fields
.field private activitiesData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;"
        }
    .end annotation
.end field

.field private activitiesModelLoaded:Z

.field private activitiesTask:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->activitiesModelLoaded:Z

    .line 26
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getMovieOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 27
    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->activitiesModelLoaded:Z

    return p1
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->activitiesData:Ljava/util/ArrayList;

    return-object p1
.end method


# virtual methods
.method public getActivityData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->activitiesData:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f070683

    return v0
.end method

.method public getMovieReleaseData()Ljava/lang/String;
    .locals 5

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getReleaseYear()Ljava/lang/String;

    move-result-object v1

    .line 84
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getDurationInMinutes()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f071084

    .line 85
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    .line 83
    invoke-static {v1, v0, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 84
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->getDurationInMinutes()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07061b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getMoviewExtraData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getHasTvShowtimeInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    const-string v0, "TODO: Hookup to live data"

    .line 93
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRottenTomatoesReviewScore()F
    .locals 2

    .prologue
    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getRottenTomatoReviewSource()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;

    move-result-object v0

    .line 79
    .local v0, "reviewSource":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getRottenTomatoReviewSource()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;

    move-result-object v1

    iget v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;->ReviewScore:F

    :goto_0
    return v1

    :cond_0
    const/high16 v1, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method public getStudio()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getStudio()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public navigateToActivityDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->navigateToActivityDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 59
    return-void
.end method

.method public onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 43
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->activitiesModelLoaded:Z

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->activitiesTask:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->activitiesTask:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->load(Z)V

    .line 47
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getMovieOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 32
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->onStopOverride()V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->activitiesTask:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->activitiesTask:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->cancel()V

    .line 55
    :cond_0
    return-void
.end method
