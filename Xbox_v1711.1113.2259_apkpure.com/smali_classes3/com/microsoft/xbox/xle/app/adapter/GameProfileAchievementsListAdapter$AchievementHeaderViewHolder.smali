.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "GameProfileAchievementsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AchievementHeaderViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;",
        ">;"
    }
.end annotation


# instance fields
.field filterSpinner:Landroid/widget/Spinner;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0644
    .end annotation
.end field

.field private filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;Landroid/view/View;)V
    .locals 4
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .line 272
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 273
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 275
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x1090008

    .line 277
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->values()[Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 279
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v1, 0x7f03020a

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 280
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->filterSpinner:Landroid/widget/Spinner;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getAchievementsFilter()Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 281
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->filterSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->filterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder$1;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 298
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;

    .prologue
    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    return-object v0
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;)V
    .locals 0
    .param p1, "compositeItem"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 303
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 265
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;)V

    return-void
.end method
