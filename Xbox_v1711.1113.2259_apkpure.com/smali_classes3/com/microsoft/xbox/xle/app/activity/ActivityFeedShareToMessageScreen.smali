.class public Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ActivityFeedShareToMessageScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen$ComposeMessageWithAttachementAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

.field private oldInputMode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method

.method private restoreSoftInputAdjustMode()V
    .locals 3

    .prologue
    .line 63
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 64
    .local v1, "wnd":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 65
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->oldInputMode:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 67
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 68
    return-void
.end method

.method private saveAndAdjustSoftInputAdjustMode()V
    .locals 3

    .prologue
    .line 54
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 55
    .local v1, "wnd":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 56
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput v2, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->oldInputMode:I

    .line 57
    const/16 v2, 0x20

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 59
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 60
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string v0, "Compose Message"

    return-object v0
.end method

.method public getTrackPage()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public leaveScreen(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "closeHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->leaveViewModel(Ljava/lang/Runnable;)V

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 23
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 24
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->onCreateContentView()V

    .line 25
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen$ComposeMessageWithAttachementAdapterProvider;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen$ComposeMessageWithAttachementAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->onCreate()V

    .line 28
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 50
    const v0, 0x7f0300ab

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->setContentView(I)V

    .line 51
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 44
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedRecipients(Lcom/microsoft/xbox/toolkit/MultiSelection;)V

    .line 45
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 46
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 32
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStart()V

    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->saveAndAdjustSoftInputAdjustMode()V

    .line 34
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;->restoreSoftInputAdjustMode()V

    .line 39
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStop()V

    .line 40
    return-void
.end method
