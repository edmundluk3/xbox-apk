.class public Lcom/microsoft/xbox/xle/app/activity/ClubChatNotificationScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ClubChatNotificationScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "ClubChatNotification"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 11
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 13
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ClubChatNotificationScreen;->onCreateContentView()V

    .line 14
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ClubChatNotificationScreen;->setShowUtilityBar(Z)V

    .line 16
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ClubChatNotificationScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 17
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 21
    const v0, 0x7f03006e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ClubChatNotificationScreen;->setContentView(I)V

    .line 22
    return-void
.end method
