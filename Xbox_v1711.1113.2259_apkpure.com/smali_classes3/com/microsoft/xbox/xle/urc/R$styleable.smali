.class public final Lcom/microsoft/xbox/xle/urc/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final BranchButton:[I

.field public static final BranchButton_command:I = 0x0

.field public static final BranchImageButton:[I

.field public static final BranchImageButton_command:I = 0x0

.field public static final MaxWidthLinearLayout:[I

.field public static final MaxWidthLinearLayout_maxWidth:I = 0x0

.field public static final URCGroupView:[I

.field public static final URCGroupView_urc_group:I = 0x0

.field public static final URCPercentView:[I

.field public static final URCPercentView_Layout:[I

.field public static final URCPercentView_Layout_bottom:I = 0x3

.field public static final URCPercentView_Layout_left:I = 0x0

.field public static final URCPercentView_Layout_right:I = 0x1

.field public static final URCPercentView_Layout_top:I = 0x2

.field public static final URCPercentView_command:I = 0x0

.field public static final URCPowerLayout:[I

.field public static final URCPowerLayout_powerStripLayoutId:I = 0x0

.field public static final URCPowerLayout_willShowSystemOnOff:I = 0x1

.field public static final URCViewControl:[I

.field public static final URCViewControl_urcStatusDisconnectStringId:I = 0x1

.field public static final URCViewControl_urcStatusErrorStringId:I = 0x2

.field public static final URCViewControl_urcStatusFirstRunStringId:I = 0x4

.field public static final URCViewControl_urcStatusFirstRunSubTextStringId:I = 0x5

.field public static final URCViewControl_urcStatusLoadingStringId:I = 0x3

.field public static final URCViewControl_urcViewControlId:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const v4, 0x7f010002

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 276
    new-array v0, v3, [I

    aput v4, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/urc/R$styleable;->BranchButton:[I

    .line 278
    new-array v0, v3, [I

    aput v4, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/urc/R$styleable;->BranchImageButton:[I

    .line 280
    new-array v0, v3, [I

    const v1, 0x7f010039

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/urc/R$styleable;->MaxWidthLinearLayout:[I

    .line 282
    new-array v0, v3, [I

    const v1, 0x7f01023a

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCGroupView:[I

    .line 284
    new-array v0, v3, [I

    aput v4, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPercentView:[I

    .line 285
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPercentView_Layout:[I

    .line 291
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPowerLayout:[I

    .line 294
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCViewControl:[I

    return-void

    .line 285
    :array_0
    .array-data 4
        0x7f01023b
        0x7f01023c
        0x7f01023d
        0x7f01023e
    .end array-data

    .line 291
    :array_1
    .array-data 4
        0x7f01023f
        0x7f010240
    .end array-data

    .line 294
    :array_2
    .array-data 4
        0x7f010241
        0x7f010242
        0x7f010243
        0x7f010244
        0x7f010245
        0x7f010246
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
