.class public Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "EPGDrawableFactory.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SharedColorDrawable"
.end annotation


# instance fields
.field private mAlpha:I

.field private mColor:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;

.field private final mPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;)V
    .locals 1
    .param p1, "color"    # Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;

    .prologue
    .line 159
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 155
    const/16 v0, 0xff

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mAlpha:I

    .line 157
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mPaint:Landroid/graphics/Paint;

    .line 160
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mColor:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mColor:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->addListener(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;)V

    .line 162
    return-void
.end method

.method private applyAlpha(I)I
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 173
    shl-int/lit8 v0, p1, 0x8

    shr-int/lit8 v0, v0, 0x8

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mAlpha:I

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 166
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mAlpha:I

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mColor:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->getColor()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->applyAlpha(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 168
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 170
    :cond_0
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mAlpha:I

    sparse-switch v0, :sswitch_data_0

    .line 193
    const/4 v0, -0x3

    :goto_0
    return v0

    .line 189
    :sswitch_0
    const/4 v0, -0x1

    goto :goto_0

    .line 191
    :sswitch_1
    const/4 v0, -0x2

    goto :goto_0

    .line 187
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0xff -> :sswitch_0
    .end sparse-switch
.end method

.method public onSharedColorChanged(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;)V
    .locals 0
    .param p1, "color"    # Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->invalidateSelf()V

    .line 202
    return-void
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 178
    and-int/lit16 p1, p1, 0xff

    .line 179
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mAlpha:I

    if-ne v0, p1, :cond_0

    .line 184
    :goto_0
    return-void

    .line 182
    :cond_0
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->mAlpha:I

    .line 183
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;->invalidateSelf()V

    goto :goto_0
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1, "colorFilter"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 197
    return-void
.end method
