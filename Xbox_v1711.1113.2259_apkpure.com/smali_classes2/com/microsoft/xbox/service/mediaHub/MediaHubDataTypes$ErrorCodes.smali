.class public final Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ErrorCodes;
.super Ljava/lang/Object;
.source "MediaHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ErrorCodes"
.end annotation


# static fields
.field public static final ACCEPTED:I = 0x2261

.field public static final BODY_MISSING_REQUIRED_CONTENT:I = 0x2775

.field public static final CDC_REQUEST_FAILURE:I = 0x2af9

.field public static final CDC_REQUEST_TIMEOUT:I = 0x2afa

.field public static final CONTENT_ISO_CHECK_FAILED:I = 0x28a1

.field public static final CONTENT_SERVICE_REQUEST_FAILURE:I = 0x2afd

.field public static final CONTENT_SERVICE_REQUEST_TIMEOUT:I = 0x2afe

.field public static final CONTENT_SIZE_TOO_LARGE:I = 0x296d

.field public static final CONTENT_SIZE_ZERO:I = 0x2973

.field public static final CUSTOM_PICS_PERMISSIONS_CHECK_FAILED:I = 0x2978

.field public static final CUSTOM_PICS_PERMISSIONS_CHECK_REQUEST_FAILURE:I = 0x2aff

.field public static final CUSTOM_PICS_PERMISSIONS_CHECK_REQUEST_TIMEOUT:I = 0x2b00

.field public static final ENFORCEMENT_PERMISSIONS_REQUIRED:I = 0x2905

.field public static final EXPECTED_BLOCKS_ZERO:I = 0x2975

.field public static final INVALID_ACCOUNT_ID:I = 0x1870e

.field public static final INVALID_CONTENT_ID:I = 0x2777

.field public static final INVALID_CONTENT_TYPE:I = 0x2778

.field public static final INVALID_CONTINUATION_TOKEN:I = 0x1870d

.field public static final INVALID_CREATION_TYPE:I = 0x2972

.field public static final INVALID_CUSTOM_PIC_TYPE:I = 0x2977

.field public static final INVALID_DETAIL_LEVEL:I = 0x2776

.field public static final INVALID_ENFORCEMENT_ACTION:I = 0x2904

.field public static final INVALID_MAX_PAGE_SIZE_PARAMETER:I = 0x1870c

.field public static final INVALID_OWNER_ID:I = 0x2779

.field public static final INVALID_PRIVILEGES_CLAIM:I = 0x2719

.field public static final INVALID_SEGMENT_IDS:I = 0x2970

.field public static final INVALID_SERVICE_CLAIM:I = 0x2713

.field public static final INVALID_SORT_FIELD_NAME:I = 0x1870b

.field public static final INVALID_SORT_PARAMETERS:I = 0x277a

.field public static final INVALID_XUID_CLAIM:I = 0x2711

.field public static final MISSING_DEVICE_ID_CLAIM:I = 0x2715

.field public static final MISSING_DEVICE_TYPE_CLAIM:I = 0x2716

.field public static final MISSING_PRIVILEGES_CLAIM:I = 0x2718

.field public static final MISSING_REQUEST_LANGUAGE:I = 0x271a

.field public static final MISSING_SANDBOX_ID_CLAIM:I = 0x2714

.field public static final MISSING_SERVICE_CLAIM:I = 0x2712

.field public static final MISSING_STAT_IN_TITLE_CONFIGURATION:I = 0x27e6

.field public static final MISSING_TITLE_CONFIGURATION:I = 0x27e5

.field public static final MISSING_TITLE_ID_CLAIM:I = 0x2717

.field public static final MISSING_XUID_CLAIM:I = 0x2710

.field public static final NONE:I = 0x0

.field public static final NO_DATA_RETURNED:I = 0x2af8

.field public static final OK:I = 0x2260

.field public static final QUOTA_REACHED:I = 0x296b

.field public static final REQUESTING_USER_CANNOT_PERFORM_ACTION_ON_SPECIFIED_USER:I = 0x28a2

.field public static final REQUESTOR_DOES_NOT_OWN_CONTENT:I = 0x28a0

.field public static final REQUIRED_PRIVILEGE_NOT_PRESENT:I = 0x296a

.field public static final SEARCH_FD_REQUEST_FAILURE:I = 0x2afb

.field public static final SEARCH_FD_REQUEST_TIMEOUT:I = 0x2afc

.field public static final SEARCH_QUERY_CONTENT_ISO_CHECK_FAILED:I = 0x27df

.field public static final SEARCH_QUERY_INVALID_FIELD:I = 0x27d9

.field public static final SEARCH_QUERY_INVALID_QUERY:I = 0x27e2

.field public static final SEARCH_QUERY_INVALID_VALUE:I = 0x27db

.field public static final SEARCH_QUERY_INVALID_VALUE_TYPE:I = 0x27da

.field public static final SEARCH_QUERY_MATURITY_CHECK_FAILED:I = 0x27e0

.field public static final SEARCH_QUERY_PARSING_FAILURE:I = 0x27d8

.field public static final SEARCH_QUERY_PRIVACY_CHECK_FAILED:I = 0x27e1

.field public static final SEARCH_QUERY_STATS_FILTER_REQUIRES_STAT_NAME:I = 0x27dd

.field public static final SEARCH_QUERY_TITLE_BLOCKED:I = 0x27de

.field public static final SEARCH_QUERY_WITH_STATS_REQUIRES_SINGLE_TITLE_ID:I = 0x27dc

.field public static final SERIALIZATION_ERROR:I = 0x2774

.field public static final STAT_ACCESS_CHECK_FAILED:I = 0x27e4

.field public static final STAT_VISIBILITY_CHECK_FAILED:I = 0x27e3

.field public static final THUMBNAIL_BLOCKS_ZERO:I = 0x2976

.field public static final THUMBNAIL_SIZE_TOO_LARGE:I = 0x296e

.field public static final THUMBNAIL_SIZE_ZERO:I = 0x2974

.field public static final TITLE_BLOCKED_FROM_UPLOAD:I = 0x296c

.field public static final TITLE_DATA_SIZE_TOO_LARGE:I = 0x296f

.field public static final UNSUPPORTED_DEVICE_TYPE:I = 0x2969

.field public static final UNTRUSTED_UPLOAD_TITLE_ID:I = 0x2971


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 570
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
