.class Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter$1;
.super Ljava/lang/Object;
.source "GameProgressGameclipsScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-ltz p3, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getCount()I

    move-result v2

    if-ge p3, v2, :cond_0

    .line 85
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 88
    .local v0, "filter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 89
    .local v1, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v2, "Filter"

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->getTelemetryName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 90
    const-string v2, "Game Captures Filter"

    invoke-static {v2, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 92
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 93
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->setGameClipsFilter(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;Z)V

    .line 96
    .end local v0    # "filter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .end local v1    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
