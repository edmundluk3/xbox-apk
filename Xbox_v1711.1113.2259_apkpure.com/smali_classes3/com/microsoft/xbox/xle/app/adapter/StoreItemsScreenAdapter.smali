.class public Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "StoreItemsScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;


# instance fields
.field private currentSearchFilter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field private filterSpinner:Landroid/widget/Spinner;

.field private filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

.field private storeItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;)V
    .locals 6
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 30
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Undefined:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->currentSearchFilter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 34
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getSearchType()Lcom/microsoft/xbox/service/model/StoreBrowseType;

    move-result-object v3

    .line 35
    .local v3, "searchType":Lcom/microsoft/xbox/service/model/StoreBrowseType;
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$3;->$SwitchMap$com$microsoft$xbox$service$model$StoreBrowseType:[I

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/StoreBrowseType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 68
    :goto_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v4, 0x1090008

    .line 70
    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->getSpinnerArrayItemsByBrowseType(Lcom/microsoft/xbox/service/model/StoreBrowseType;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v0, v1, v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v1, 0x7f03020a

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 76
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getListRowLayoutId()I

    move-result v2

    .line 80
    .local v2, "listRowId":I
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/service/model/StoreBrowseType;Ljava/util/List;Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 82
    return-void

    .line 37
    .end local v2    # "listRowId":I
    :pswitch_0
    const v0, 0x7f0e0a6c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->screenBody:Landroid/view/View;

    .line 38
    const v0, 0x7f0e0a6d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 39
    const v0, 0x7f0e0a6e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 40
    const v0, 0x7f0e0a6f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    goto :goto_0

    .line 44
    :pswitch_1
    const v0, 0x7f0e0a60

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->screenBody:Landroid/view/View;

    .line 45
    const v0, 0x7f0e0a61

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 46
    const v0, 0x7f0e0a62

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 47
    const v0, 0x7f0e0a63

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    goto/16 :goto_0

    .line 51
    :pswitch_2
    const v0, 0x7f0e0a64

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->screenBody:Landroid/view/View;

    .line 52
    const v0, 0x7f0e0a65

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 53
    const v0, 0x7f0e0a66

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 54
    const v0, 0x7f0e0a67

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    goto/16 :goto_0

    .line 58
    :pswitch_3
    const v0, 0x7f0e0a68

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->screenBody:Landroid/view/View;

    .line 59
    const v0, 0x7f0e0a69

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 60
    const v0, 0x7f0e0a6a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 61
    const v0, 0x7f0e0a6b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    goto/16 :goto_0

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 74
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    goto/16 :goto_1

    .line 35
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->currentSearchFilter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->currentSearchFilter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->storeItems:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

    return-object v0
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 88
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 89
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_0

    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-virtual {v1, v0, p3}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->gotoDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;I)V

    .line 92
    :cond_0
    return-void
.end method


# virtual methods
.method public getCurrentSearchFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->currentSearchFilter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    return-object v0
.end method

.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStart()V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->setLoadMoreListener(Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;)V

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 124
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 128
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStop()V

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->setLoadMoreListener(Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;)V

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 134
    :cond_0
    return-void
.end method

.method public updateViewOverride()V
    .locals 6

    .prologue
    .line 143
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->isBusy()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 144
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v2

    .line 145
    .local v2, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->isBusy()Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v2, :cond_2

    .line 146
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getSearchType()Lcom/microsoft/xbox/service/model/StoreBrowseType;

    move-result-object v3

    sget-object v5, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Apps:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    if-ne v3, v5, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    .line 147
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v3

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->New:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    if-ne v3, v5, :cond_3

    const/4 v3, 0x1

    .line 146
    :goto_0
    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->setHideReleaseDate(Z)V

    .line 149
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 150
    .local v0, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->storeItems:Ljava/util/ArrayList;

    if-eq v3, v0, :cond_1

    .line 151
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->storeItems:Ljava/util/ArrayList;

    .line 152
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 153
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->clear()V

    .line 155
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->storeItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 157
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->onLoadMoreFinished()V

    .line 158
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->notifyDataSetChanged()V

    .line 161
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getlastSelectedItemIndex()I

    move-result v1

    .line 162
    .local v1, "lastSelected":I
    if-lez v1, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->getCount()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 163
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setSelection(I)V

    .line 167
    .end local v0    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .end local v1    # "lastSelected":I
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 168
    return-void

    .line 147
    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method
