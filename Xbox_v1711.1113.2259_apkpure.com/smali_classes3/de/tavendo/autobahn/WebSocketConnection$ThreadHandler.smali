.class Lde/tavendo/autobahn/WebSocketConnection$ThreadHandler;
.super Landroid/os/Handler;
.source "WebSocketConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lde/tavendo/autobahn/WebSocketConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ThreadHandler"
.end annotation


# instance fields
.field private final mWebSocketConnection:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lde/tavendo/autobahn/WebSocketConnection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lde/tavendo/autobahn/WebSocketConnection;)V
    .locals 1
    .param p1, "webSocketConnection"    # Lde/tavendo/autobahn/WebSocketConnection;

    .prologue
    .line 525
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 527
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection$ThreadHandler;->mWebSocketConnection:Ljava/lang/ref/WeakReference;

    .line 528
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 534
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection$ThreadHandler;->mWebSocketConnection:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/tavendo/autobahn/WebSocketConnection;

    .line 535
    .local v0, "webSocketConnection":Lde/tavendo/autobahn/WebSocketConnection;
    if-eqz v0, :cond_0

    .line 536
    invoke-static {v0, p1}, Lde/tavendo/autobahn/WebSocketConnection;->access$1(Lde/tavendo/autobahn/WebSocketConnection;Landroid/os/Message;)V

    .line 538
    :cond_0
    return-void
.end method
