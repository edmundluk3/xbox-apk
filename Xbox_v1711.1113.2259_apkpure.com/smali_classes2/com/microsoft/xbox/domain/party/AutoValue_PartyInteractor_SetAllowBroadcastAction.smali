.class final Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_SetAllowBroadcastAction;
.super Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;
.source "AutoValue_PartyInteractor_SetAllowBroadcastAction.java"


# instance fields
.field private final allow:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;-><init>()V

    .line 13
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_SetAllowBroadcastAction;->allow:Z

    .line 14
    return-void
.end method


# virtual methods
.method public allow()Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_SetAllowBroadcastAction;->allow:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30
    if-ne p1, p0, :cond_1

    .line 37
    :cond_0
    :goto_0
    return v1

    .line 33
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;

    if-eqz v3, :cond_2

    move-object v0, p1

    .line 34
    check-cast v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;

    .line 35
    .local v0, "that":Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;
    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_SetAllowBroadcastAction;->allow:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;->allow()Z

    move-result v4

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;
    :cond_2
    move v1, v2

    .line 37
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 42
    const/4 v0, 0x1

    .line 43
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 44
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_SetAllowBroadcastAction;->allow:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 45
    return v0

    .line 44
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SetAllowBroadcastAction{allow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_SetAllowBroadcastAction;->allow:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
