.class final enum Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;
.super Ljava/lang/Enum;
.source "EPGScrollAccelerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TweenInitiator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

.field public static final enum NOT_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

.field public static final enum OTHER_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

.field public static final enum SELF_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    const-string v1, "NOT_INITIATED"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->NOT_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    const-string v1, "SELF_INITIATED"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->SELF_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    const-string v1, "OTHER_INITIATED"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->OTHER_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->NOT_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->SELF_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->OTHER_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->$VALUES:[Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const-class v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->$VALUES:[Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    return-object v0
.end method
