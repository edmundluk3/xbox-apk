.class public Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "CustomizeProfileScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;
    }
.end annotation


# static fields
.field private static final CHANGE_GAMERTAG_URL:Ljava/lang/String; = "https://account.xbox.com/en-US/changegamertag"

.field private static final CUSTOM_GAMERPIC_DIMEN:I = 0x438

.field private static final GAMERTAG_SUGGESTION_COUNT:I = 0x8

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private bioString:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private changeGamertagAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;

.field private checkFreeGamertagChangeAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;

.field private checkGamertagAvailabilityAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;

.field private final colorIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final colorList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation
.end field

.field private currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private currentGamerpic:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final gamerpicList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private gamertagAvailabilityResultText:Ljava/lang/String;

.field private gamertagSuggestions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private getGamerpicCall:Lretrofit2/Call;

.field private getGamertagSuggestionsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;

.field private isChangingGamertag:Z

.field private isCheckingFreeGamertagChange:Z

.field private isCheckingGamertagAvailability:Z

.field private isColorListError:Z

.field private isGamerpicListError:Z

.field private isGamertagChangeAvailable:Z

.field private isLoadingGamerpicList:Z

.field private isLoadingProfileColorList:Z

.field private isLoadingUserProfile:Z

.field private isUpdatingUserProfile:Z

.field private loadProfileColorListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;

.field private loadUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;

.field private locationString:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field profileColorsRepository:Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private reservedGamertag:Ljava/lang/String;

.field private showGamertagAvailabilityResult:Z

.field private updateUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 74
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->colorList:Ljava/util/List;

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->colorIdMap:Ljava/util/Map;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamerpicList:Ljava/util/List;

    .line 121
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 122
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getCustomizeProfileScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getLocation()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->locationString:Ljava/lang/String;

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getBio()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->bioString:Ljava/lang/String;

    .line 129
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V

    .line 130
    return-void
.end method

.method static synthetic access$1002(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isColorListError:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->colorIdMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isCheckingFreeGamertagChange:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Z

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->onCheckFreeGamertagChangeCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V

    return-void
.end method

.method static synthetic access$1702(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isChangingGamertag:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->onChangeGamertagCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->reservedGamertag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isCheckingGamertagAvailability:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->onCheckGamertagAvailabilityCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$2200(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->onGetGamertagSuggestionsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$2302(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamertagSuggestions:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isLoadingUserProfile:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->onLoadUserProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isUpdatingUserProfile:Z

    return p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->onUpdateUserProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZZZ)V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->colorList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$802(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isLoadingProfileColorList:Z

    return p1
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->onGetProfileColorListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->onUploadPicSuccess(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->onLoadGamerpicListSuccess(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$2(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/lang/Void;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->onLoadGamerpicListFailure(Ljava/lang/Void;)V

    return-void
.end method

.method static synthetic lambda$showEditBioTextDialog$1(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/lang/String;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "updatedText"    # Ljava/lang/String;

    .prologue
    .line 351
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissEditDialog()V

    .line 353
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getBio()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 354
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->setBio(Ljava/lang/String;)V

    .line 356
    :cond_0
    return-void
.end method

.method static synthetic lambda$showEditLocationTextDialog$0(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/lang/String;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "updatedText"    # Ljava/lang/String;

    .prologue
    .line 335
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissEditDialog()V

    .line 337
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getLocation()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->setLocation(Ljava/lang/String;)V

    .line 340
    :cond_0
    return-void
.end method

.method private onChangeGamertagCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 883
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChangeGamertagCompleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->closeGamertagPickerDialog()V

    .line 886
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 887
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f0703a7

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    .line 889
    :cond_0
    return-void
.end method

.method private onCheckFreeGamertagChangeCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "isFreeChangeAvailable"    # Z

    .prologue
    .line 807
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCheckFreeGamertagChangeCompleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 810
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->closeGamertagPickerDialog()V

    .line 811
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f0703a5

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    .line 817
    :goto_0
    return-void

    .line 813
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Free gamertag change is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "available"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamertagChangeAvailable:Z

    .line 815
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateGamertagPickerDialog()V

    goto :goto_0

    .line 813
    :cond_1
    const-string v0, "not "

    goto :goto_1
.end method

.method private onCheckGamertagAvailabilityCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "gamertag"    # Ljava/lang/String;
    .param p3, "isAvailable"    # Z

    .prologue
    .line 945
    const-string/jumbo v0, "reservedGamertag should be reset when an availability check starts"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->reservedGamertag:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 946
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCheckGamertagAvailabilityCompleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 949
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f0703a5

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    .line 959
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateGamertagPickerDialog()V

    .line 960
    return-void

    .line 951
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gamertag "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p3, :cond_2

    const-string v0, " is available"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showGamertagAvailabilityResult:Z

    .line 953
    if-eqz p3, :cond_1

    .line 954
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->reservedGamertag:Ljava/lang/String;

    .line 956
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamertagReserved()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0703a3

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamertagAvailabilityResultText:Ljava/lang/String;

    goto :goto_0

    .line 951
    :cond_2
    const-string v0, " is not available"

    goto :goto_1

    .line 956
    :cond_3
    const v0, 0x7f0703aa

    goto :goto_2
.end method

.method private onGetGamertagSuggestionsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1031
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onGetGamertagSuggestionsCompleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateGamertagPickerDialog()V

    .line 1034
    return-void
.end method

.method private onGetProfileColorListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 732
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onGetProfileColorListCompleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 735
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isColorListError:Z

    .line 738
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateAdapter()V

    .line 739
    return-void
.end method

.method private onLoadGamerpicListFailure(Ljava/lang/Void;)V
    .locals 1
    .param p1, "v"    # Ljava/lang/Void;

    .prologue
    .line 527
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isLoadingGamerpicList:Z

    .line 528
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamerpicListError:Z

    .line 530
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateAdapter()V

    .line 531
    return-void
.end method

.method private onLoadGamerpicListSuccess(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "gamerpicList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 516
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 518
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isLoadingGamerpicList:Z

    .line 519
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamerpicListError:Z

    .line 520
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamerpicList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 521
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamerpicList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 523
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateAdapter()V

    .line 524
    return-void
.end method

.method private onLoadUserProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 545
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadUserProfileCompleted "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 566
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateAdapter()V

    .line 567
    return-void

    .line 551
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v0, :cond_0

    .line 552
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfilePreferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 555
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentGamerpic:Ljava/lang/String;

    .line 557
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 562
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 547
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onUpdateUserProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZZZ)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "partialFailure"    # Z
    .param p3, "updatedColor"    # Z
    .param p4, "updatedGamerpic"    # Z

    .prologue
    .line 625
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateUserProfileCompleted "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-eqz v0, :cond_0

    .line 629
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setMePreferredColor(I)V

    .line 630
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->setProfilePreferredColor(Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V

    .line 631
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->ProfileColorChange:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 634
    :cond_0
    if-eqz p4, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentGamerpic:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 636
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentGamerpic:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->setGamerPicImageUrl(Ljava/lang/String;)V

    .line 639
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 655
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateAdapter()V

    .line 656
    return-void

    .line 643
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->close()V

    goto :goto_0

    .line 647
    :pswitch_1
    if-eqz p2, :cond_2

    .line 648
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f070b1f

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0

    .line 650
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f070b20

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0

    .line 639
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onUploadPicSuccess(Landroid/net/Uri;)V
    .locals 2
    .param p1, "customPic"    # Landroid/net/Uri;

    .prologue
    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->setGamerPicImageUrl(Ljava/lang/String;)V

    .line 294
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 9

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getLocation()Ljava/lang/String;

    move-result-object v8

    .line 412
    .local v8, "location":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getBio()Ljava/lang/String;

    move-result-object v7

    .line 414
    .local v7, "bio":Ljava/lang/String;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getLocation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 415
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getBio()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 416
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfilePreferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentGamerpic:Ljava/lang/String;

    .line 417
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentGamerpic:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 418
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07074a

    .line 419
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070749

    .line 420
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070738

    .line 421
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070736

    .line 423
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    .line 418
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 428
    :goto_0
    return-void

    .line 426
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->close()V

    goto :goto_0
.end method

.method public changeGamertag()V
    .locals 2

    .prologue
    .line 872
    const-string v0, "A gamertag must be checked for availability before attempting to change to it"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->reservedGamertag:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertFalse(Ljava/lang/String;Z)V

    .line 874
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->changeGamertagAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;

    if-eqz v0, :cond_0

    .line 875
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->changeGamertagAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->cancel()V

    .line 878
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->changeGamertagAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;

    .line 879
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->changeGamertagAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->load(Z)V

    .line 880
    return-void
.end method

.method public checkFreeGamertagChange()V
    .locals 2

    .prologue
    .line 798
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkFreeGamertagChangeAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkFreeGamertagChangeAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->cancel()V

    .line 802
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkFreeGamertagChangeAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;

    .line 803
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkFreeGamertagChangeAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->load(Z)V

    .line 804
    return-void
.end method

.method public checkGamertagAvailablity(Ljava/lang/String;)V
    .locals 2
    .param p1, "gamertag"    # Ljava/lang/String;

    .prologue
    .line 934
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkGamertagAvailabilityAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;

    if-eqz v0, :cond_0

    .line 935
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkGamertagAvailabilityAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->cancel()V

    .line 938
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->resetReservedGamertag()V

    .line 940
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkGamertagAvailabilityAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;

    .line 941
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkGamertagAvailabilityAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->load(Z)V

    .line 942
    return-void
.end method

.method public close()V
    .locals 3

    .prologue
    .line 432
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GoBack()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 436
    :goto_0
    return-void

    .line 433
    :catch_0
    move-exception v0

    .line 434
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Failed to navigate back from customize profile page"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public closeColorPickerDialog()V
    .locals 1

    .prologue
    .line 369
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissChooseColorProfileDialog()V

    .line 370
    return-void
.end method

.method public closeGamerpicPickerDialog()V
    .locals 1

    .prologue
    .line 259
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissChooseGamerpicDialog()V

    .line 260
    return-void
.end method

.method public closeGamertagPickerDialog()V
    .locals 1

    .prologue
    .line 302
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissChangeGamertagDialog()V

    .line 303
    return-void
.end method

.method public commitChanges()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 373
    new-instance v2, Ljava/util/EnumMap;

    const-class v3, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-direct {v2, v3}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 375
    .local v2, "updateInfo":Ljava/util/EnumMap;, "Ljava/util/EnumMap<Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 376
    .local v1, "location":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getBio()Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "bio":Ljava/lang/String;
    invoke-static {p0, v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCCustomizeProfile;->trackSaveCustomizeProfile(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 381
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Location:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual {v2, v3, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getBio()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 384
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Bio:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual {v2, v3, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfilePreferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 388
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->PreferredColor:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getProfileColorUrl()Ljava/lang/String;

    move-result-object v5

    new-array v6, v10, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->colorIdMap:Ljava/util/Map;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentGamerpic:Ljava/lang/String;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 392
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->PublicGamerpic:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentGamerpic:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    :cond_3
    invoke-virtual {v2}, Ljava/util/EnumMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 397
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;

    if-eqz v3, :cond_4

    .line 398
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->cancel()V

    .line 401
    :cond_4
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Updating profile with "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/util/EnumMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " pieces of information"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;

    invoke-direct {v3, p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/util/Map;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;

    .line 404
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;

    invoke-virtual {v3, v10}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->load(Z)V

    .line 408
    :goto_0
    return-void

    .line 406
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->close()V

    goto :goto_0
.end method

.method public getBio()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 170
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->bioString:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getBio()Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "bio":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_1

    .end local v0    # "bio":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->bioString:Ljava/lang/String;

    goto :goto_0

    .line 171
    .restart local v0    # "bio":Ljava/lang/String;
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public getCurrentColor()I
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v0

    .line 150
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    goto :goto_0
.end method

.method public getCurrentColorObject()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    return-object v0
.end method

.method public getCurrentGamerpic()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentGamerpic:Ljava/lang/String;

    return-object v0
.end method

.method public getGamerpicList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamerpicList:Ljava/util/List;

    return-object v0
.end method

.method public getGamertagAvailabilityResultText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamertagAvailabilityResultText:Ljava/lang/String;

    return-object v0
.end method

.method public getGamertagSuggestions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamertagSuggestions:Ljava/util/List;

    return-object v0
.end method

.method public getIsGamertagChangeAvailable()Z
    .locals 1

    .prologue
    .line 201
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamertagChangeAvailable:Z

    return v0
.end method

.method public getIsLoadingGamerpicList()Z
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isLoadingGamerpicList:Z

    return v0
.end method

.method public getIsLoadingProfileColorList()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isLoadingProfileColorList:Z

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 161
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->locationString:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 162
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getLocation()Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, "location":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_1

    .end local v0    # "location":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->locationString:Ljava/lang/String;

    goto :goto_0

    .line 165
    .restart local v0    # "location":Ljava/lang/String;
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public getProfileColorList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->colorList:Ljava/util/List;

    return-object v0
.end method

.method public getProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public goToGamertagWebsite()V
    .locals 4

    .prologue
    .line 310
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 311
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "https://account.xbox.com/en-US/changegamertag"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 313
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    :goto_0
    return-void

    .line 314
    :catch_0
    move-exception v0

    .line 315
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Error navigating to change gamertag website"

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 483
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isLoadingUserProfile:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isUpdatingUserProfile:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isLoadingProfileColorList:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isLoadingGamerpicList:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCheckingGamertagAvailability()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isCheckingGamertagAvailability:Z

    return v0
.end method

.method public isGamertagDialogBusy()Z
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isCheckingFreeGamertagChange:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isChangingGamertag:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isCheckingGamertagAvailability:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGamertagReserved()Z
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->reservedGamertag:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v1, 0x0

    .line 488
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;->cancel()V

    .line 492
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;

    .line 493
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;->load(Z)V

    .line 495
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadProfileColorListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;

    if-eqz v0, :cond_1

    .line 496
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadProfileColorListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;->cancel()V

    .line 499
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadProfileColorListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;

    .line 500
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadProfileColorListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;->load(Z)V

    .line 502
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamerpicCall:Lretrofit2/Call;

    if-eqz v0, :cond_2

    .line 503
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamerpicCall:Lretrofit2/Call;

    invoke-interface {v0}, Lretrofit2/Call;->cancel()V

    .line 506
    :cond_2
    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamerpicList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 507
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isLoadingGamerpicList:Z

    .line 508
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamerpicListError:Z

    .line 509
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;->getGamerpicListAsync(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)Lretrofit2/Call;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamerpicCall:Lretrofit2/Call;

    .line 512
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateAdapter()V

    .line 513
    return-void
.end method

.method public loadGamertagSuggestions(Ljava/lang/String;)V
    .locals 2
    .param p1, "seed"    # Ljava/lang/String;

    .prologue
    .line 1018
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1019
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamertagSuggestionsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;

    if-eqz v0, :cond_0

    .line 1020
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamertagSuggestionsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;->cancel()V

    .line 1023
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamertagSuggestions:Ljava/util/List;

    .line 1025
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamertagSuggestionsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;

    .line 1026
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamertagSuggestionsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;->load(Z)V

    .line 1028
    :cond_1
    return-void
.end method

.method public navigateToUploadPicScreen()V
    .locals 8

    .prologue
    const/16 v7, 0x438

    .line 263
    invoke-static {}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->getPrivResult()Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;

    move-result-object v0

    .line 265
    .local v0, "customPicPrivResult":Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$uploadCustomPic$CustomPicPrivCheck$PrivResult:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected priv result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 290
    :goto_0
    return-void

    .line 267
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackUseCustomGamerPic()V

    .line 268
    const-class v1, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreen;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 272
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v4

    sget-object v3, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->Gamerpic:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 274
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v6

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v7

    .line 271
    invoke-static {v4, v5, v3, v6, v7}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->getOvalInstance(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Landroid/util/Pair;Lcom/microsoft/xbox/toolkit/generics/Action;)Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    move-result-object v3

    .line 268
    invoke-virtual {p0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 279
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->showPrivFailureDialog()V

    goto :goto_0

    .line 283
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->showChildFailureDialog()V

    goto :goto_0

    .line 265
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onBackButtonPressed()V
    .locals 0

    .prologue
    .line 535
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->cancel()V

    .line 536
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 447
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getCustomizeProfileScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 448
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 443
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadUserProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadUserProfileAsyncTask;->cancel()V

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadProfileColorListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadProfileColorListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$LoadProfileColorListAsyncTask;->cancel()V

    .line 460
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamerpicCall:Lretrofit2/Call;

    if-eqz v0, :cond_2

    .line 461
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamerpicCall:Lretrofit2/Call;

    invoke-interface {v0}, Lretrofit2/Call;->cancel()V

    .line 464
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkFreeGamertagChangeAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;

    if-eqz v0, :cond_3

    .line 465
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkFreeGamertagChangeAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckFreeGamertagChangeAsyncTask;->cancel()V

    .line 468
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->changeGamertagAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;

    if-eqz v0, :cond_4

    .line 469
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->changeGamertagAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->cancel()V

    .line 472
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkGamertagAvailabilityAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;

    if-eqz v0, :cond_5

    .line 473
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkGamertagAvailabilityAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->cancel()V

    .line 476
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamertagSuggestionsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;

    if-eqz v0, :cond_6

    .line 477
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamertagSuggestionsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;->cancel()V

    .line 479
    :cond_6
    return-void
.end method

.method public resetReservedGamertag()V
    .locals 1

    .prologue
    .line 241
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->reservedGamertag:Ljava/lang/String;

    .line 242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showGamertagAvailabilityResult:Z

    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamertagSuggestions:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->gamertagSuggestions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 246
    :cond_0
    return-void
.end method

.method public setBio(Ljava/lang/String;)V
    .locals 0
    .param p1, "bioString"    # Ljava/lang/String;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->bioString:Ljava/lang/String;

    .line 176
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateAdapter()V

    .line 177
    return-void
.end method

.method public setCurrentColor(Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V
    .locals 0
    .param p1, "color"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 231
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 232
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateAdapter()V

    .line 233
    return-void
.end method

.method public setCurrentGamerpic(Ljava/lang/String;)V
    .locals 0
    .param p1, "gamerpic"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 236
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->currentGamerpic:Ljava/lang/String;

    .line 237
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateAdapter()V

    .line 238
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "locationString"    # Ljava/lang/String;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->locationString:Ljava/lang/String;

    .line 181
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateAdapter()V

    .line 182
    return-void
.end method

.method public shouldShowGamertagAvailabilityResult()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showGamertagAvailabilityResult:Z

    return v0
.end method

.method public showColorPickerDialog()V
    .locals 2

    .prologue
    .line 320
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isColorListError:Z

    if-nez v0, :cond_0

    .line 321
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showChooseColorProfileDialog(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V

    .line 325
    :goto_0
    return-void

    .line 323
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f07039f

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0
.end method

.method public showEditBioTextDialog()V
    .locals 7

    .prologue
    .line 344
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070a4a

    .line 345
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->bioString:Ljava/lang/String;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0b0018

    .line 347
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070395

    .line 348
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 349
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getCurrentColor()I

    move-result v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;

    move-result-object v6

    .line 344
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showEditTextDialog(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;)V

    .line 357
    return-void
.end method

.method public showEditLocationTextDialog()V
    .locals 7

    .prologue
    .line 328
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070a4a

    .line 329
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->locationString:Ljava/lang/String;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0b0019

    .line 331
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070396

    .line 332
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 333
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getCurrentColor()I

    move-result v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;

    move-result-object v6

    .line 328
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showEditTextDialog(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;)V

    .line 341
    return-void
.end method

.method public showGamerpicPickerDialog()V
    .locals 2

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamerpicListError:Z

    if-nez v0, :cond_0

    .line 252
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showChooseGamerpicDialog(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V

    .line 256
    :goto_0
    return-void

    .line 254
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f0703a2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0
.end method

.method public showGamertagPickerDialog()V
    .locals 1

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->resetReservedGamertag()V

    .line 298
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showChangeGamertagDialog(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V

    .line 299
    return-void
.end method

.method public showKeyboard()V
    .locals 2

    .prologue
    .line 540
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 541
    return-void
.end method

.method public showPrivilegeError(Ljava/lang/String;)V
    .locals 5
    .param p1, "missingPrivilege"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 360
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 361
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07061d

    .line 362
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 363
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f0704a3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 364
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 361
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 366
    return-void
.end method

.method public updateGamertagPickerDialog()V
    .locals 1

    .prologue
    .line 306
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->updateChangeGamertagDialog()V

    .line 307
    return-void
.end method
