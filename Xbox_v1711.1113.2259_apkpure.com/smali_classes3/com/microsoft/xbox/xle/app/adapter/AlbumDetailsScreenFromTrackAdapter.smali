.class public Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "AlbumDetailsScreenFromTrackAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter$SongAdapter;
    }
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private albumDetailArtist:Landroid/widget/TextView;

.field private albumDetailImage:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private albumDetailTitle:Landroid/widget/TextView;

.field private albumDetailsButtonPrimary:Landroid/view/View;

.field private albumDetailsExplicitText:Landroid/widget/TextView;

.field private albumDetailsReleaseDate:Landroid/widget/TextView;

.field private albumDetailsReleaseDateAndStudio:Landroid/widget/TextView;

.field private final albumPlayClickListener:Landroid/view/View$OnClickListener;

.field private allSongListLayout:Landroid/widget/LinearLayout;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private trackList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final trackPlayListener:Landroid/view/View$OnClickListener;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)V
    .locals 1
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 151
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->trackPlayListener:Landroid/view/View$OnClickListener;

    .line 159
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumPlayClickListener:Landroid/view/View$OnClickListener;

    .line 48
    const v0, 0x7f0e01a4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->screenBody:Landroid/view/View;

    .line 49
    const v0, 0x7f0e01ae

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->content:Landroid/view/View;

    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    .line 53
    const v0, 0x7f0e01a8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailImage:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 54
    const v0, 0x7f0e01a7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailTitle:Landroid/widget/TextView;

    .line 55
    const v0, 0x7f0e01a9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailArtist:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f0e01aa

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailsReleaseDateAndStudio:Landroid/widget/TextView;

    .line 58
    const v0, 0x7f0e01af

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailsButtonPrimary:Landroid/view/View;

    .line 59
    const v0, 0x7f0e01ab

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailsExplicitText:Landroid/widget/TextView;

    .line 61
    const v0, 0x7f0e01b1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->allSongListLayout:Landroid/widget/LinearLayout;

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->content:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;)Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->trackList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;Landroid/view/View;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    .param p3, "x3"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->bindItemView(Landroid/view/View;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;I)V

    return-void
.end method

.method private bindItemView(Landroid/view/View;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;I)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    .param p3, "position"    # I

    .prologue
    .line 127
    if-eqz p2, :cond_2

    .line 128
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/view/View;->setClickable(Z)V

    .line 129
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->trackPlayListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 132
    const v3, 0x7f0e0a4f

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 133
    .local v1, "albumItemTitle":Landroid/widget/TextView;
    const v3, 0x7f0e0a50

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 135
    .local v0, "albumItemDuration":Landroid/widget/TextView;
    if-eqz v1, :cond_2

    .line 136
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getDuration()Ljava/lang/String;

    move-result-object v2

    .line 137
    .local v2, "duration":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getDuration()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getDuration()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 138
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f070632

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .end local v2    # "duration":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 141
    .restart local v2    # "duration":Ljava/lang/String;
    :cond_1
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    .end local v0    # "albumItemDuration":Landroid/widget/TextView;
    .end local v1    # "albumItemTitle":Landroid/widget/TextView;
    .end local v2    # "duration":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private createItemView(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 8
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "position"    # I

    .prologue
    const/4 v7, 0x0

    .line 116
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 117
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030209

    invoke-virtual {v0, v4, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 118
    .local v3, "view":Landroid/view/View;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 119
    .local v1, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v4, 0x1

    const/high16 v5, 0x40c00000    # 6.0f

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v2, v4

    .line 120
    .local v2, "marginBotttom":I
    invoke-virtual {v1, v7, v7, v7, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 121
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 122
    invoke-direct {p0, v3, p1, p3}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->bindItemView(Landroid/view/View;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;I)V

    .line 123
    return-object v3
.end method


# virtual methods
.method public getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 101
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailsButtonPrimary:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailsButtonPrimary:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumPlayClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailsButtonPrimary:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailsButtonPrimary:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 113
    return-void
.end method

.method public updateViewOverride()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 69
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->isBusy()Z

    move-result v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->updateLoadingIndicator(Z)V

    .line 70
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 72
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailImage:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getDefaultImageRid()I

    move-result v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getDefaultImageRid()I

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 74
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailTitle:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 75
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailsExplicitText:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getIsExplicit()Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    invoke-static {v4, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 77
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailArtist:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getArtist()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 79
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailsReleaseDate:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getReleaseYear()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 80
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->albumDetailsReleaseDateAndStudio:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getAlbumYearAndStudio()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 83
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->trackList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getTracks()Ljava/util/ArrayList;

    move-result-object v4

    if-eq v3, v4, :cond_1

    .line 84
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getTracks()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->trackList:Ljava/util/ArrayList;

    .line 85
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->allSongListLayout:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_1

    .line 87
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->trackList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 88
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->allSongListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 89
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getTracks()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 90
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getTracks()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .line 91
    .local v2, "track":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->allSongListLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v2, v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->createItemView(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    .line 92
    .local v1, "itemView":Landroid/view/View;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;->allSongListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 75
    .end local v0    # "i":I
    .end local v1    # "itemView":Landroid/view/View;
    .end local v2    # "track":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    :cond_0
    const/16 v3, 0x8

    goto :goto_0

    .line 97
    :cond_1
    return-void
.end method
