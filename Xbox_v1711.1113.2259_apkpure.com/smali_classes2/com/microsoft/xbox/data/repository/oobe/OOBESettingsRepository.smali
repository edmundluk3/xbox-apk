.class public interface abstract Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
.super Ljava/lang/Object;
.source "OOBESettingsRepository.java"


# virtual methods
.method public abstract getConsoleId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getSessionCode()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract setAutoDST(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract setAutoUpdateApps(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract setAutoUpdateSystem(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract setConsoleId(Ljava/lang/String;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
.end method

.method public abstract setInstantOn(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract setSessionCode(Ljava/lang/String;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
.end method

.method public abstract setTimeZone(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
