.class public Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;
.source "NowPlayingTrayViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$EnableButtonRunnable;,
        Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    }
.end annotation


# static fields
.field private static final RECORD_BUTTON_DISABLE_TIME:I = 0xbb8

.field private static final RECORD_LENGTH:I = 0x3c

.field private static profileBackgroundColorBitmapSetListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;


# instance fields
.field private contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

.field private isRunningOOBE:Z

.field private nowPlayingPosterUri:Ljava/lang/String;

.field private nowPlayingProviderOrArtistName:Ljava/lang/String;

.field private nowPlayingSubTitle:Ljava/lang/String;

.field private nowPlayingTitle:Ljava/lang/String;

.field private nowPlayingUri:Ljava/lang/String;

.field private final recordEnableHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$1;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->profileBackgroundColorBitmapSetListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
    .param p2, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;-><init>(Lcom/microsoft/xbox/toolkit/IViewUpdate;)V

    .line 82
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->None:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 83
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->recordEnableHandler:Landroid/os/Handler;

    .line 120
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 121
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->navigateToNowPlayingDetailsInternal()V

    return-void
.end method

.method private alreadyShowingDetailsScreenForMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Z
    .locals 2
    .param p1, "mediaItemToShow"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p2, "currentlyShowingItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 435
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    const-string v0, "NowPlayingTrayViewModel"

    const-string v1, "We\'re already showing the detail screen.  Not navigating."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const/4 v0, 0x1

    .line 439
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .locals 2

    .prologue
    .line 479
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getModel(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    return-object v0
.end method

.method private getPosterUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingPosterUri:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingPosterUri:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingUri:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getProfileBackgroundColorBitmapSetListener()Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->profileBackgroundColorBitmapSetListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    return-object v0
.end method

.method private navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 8
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 449
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getExpandedAppBar()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->dismiss()V

    .line 456
    const/4 v3, 0x0

    .line 457
    .local v3, "popExistingScreen":Z
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 458
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    instance-of v5, v0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    if-nez v5, :cond_0

    instance-of v5, v0, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;

    if-eqz v5, :cond_1

    .line 459
    :cond_0
    const/4 v3, 0x1

    .line 462
    :cond_1
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 464
    .local v2, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedMediaItemData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 465
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v5

    invoke-static {v5}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->getDetailScreenTypeFromMediaType(I)Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    move-result-object v4

    .line 466
    .local v4, "screenType":Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;
    invoke-static {v4, p1}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->getPaneConfigData(Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    move-result-object v1

    .line 467
    .local v1, "data":[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setDetailPivotData([Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;)V

    .line 469
    if-eqz v1, :cond_3

    .line 470
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v6

    const-class v7, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    if-nez v3, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-virtual {v6, v7, v5, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 471
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v5

    const-class v6, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 476
    :goto_1
    return-void

    .line 470
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 473
    :cond_3
    const-string v6, "NowPlayingTrayViewModel"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "the detail page for type "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_2
    invoke-static {v6, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v5

    const v6, 0x7f070683

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_1

    .line 473
    :cond_4
    const-string v5, "NULL does not exist"

    goto :goto_2
.end method

.method private navigateToNowPlayingDetailsInternal()V
    .locals 9

    .prologue
    .line 325
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v2

    .line 326
    .local v2, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v2, :cond_0

    .line 327
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingState()Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    move-result-object v5

    .line 329
    .local v5, "state":Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$3;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 365
    .end local v5    # "state":Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
    :cond_0
    :goto_0
    return-void

    .line 334
    .restart local v5    # "state":Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 336
    .local v1, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    instance-of v6, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    if-eqz v6, :cond_1

    .line 337
    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .end local v1    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getAlbum()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    move-result-object v0

    .line 338
    .local v0, "albumItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    if-eqz v0, :cond_0

    .line 342
    move-object v1, v0

    .line 347
    .end local v0    # "albumItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    .restart local v1    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v3

    .line 348
    .local v3, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v4

    .line 349
    .local v4, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_0

    .line 350
    invoke-direct {p0, v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->alreadyShowingDetailsScreenForMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 351
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getExpandedAppBar()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->dismiss()V

    goto :goto_0

    .line 353
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v6

    const-string v7, "View Details"

    iget-object v8, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 361
    .end local v1    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v3    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .end local v4    # "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :pswitch_1
    const-string v6, "The app is either connecting or disconnected, or it\'s trying to launch Dash"

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    goto :goto_0

    .line 329
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private recordThat()V
    .locals 4

    .prologue
    .line 404
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getGameDvr()Lcom/microsoft/xbox/smartglass/GameDvr;

    move-result-object v1

    const/16 v2, 0x3c

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/GameDvr;->recordPrevious(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    :goto_0
    return-void

    .line 405
    :catch_0
    move-exception v0

    .line 406
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "NowPlayingTrayViewModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "recordThat error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateContentType()V
    .locals 7

    .prologue
    .line 236
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingState()Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    move-result-object v0

    .line 238
    .local v0, "state":Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;
    const-string v1, "NowPlayingTrayVM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NowplayingState changed to  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->None:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 240
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$3;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 268
    const-string v1, "NowPlayingTrayVM"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Content type %s unhandled"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :goto_0
    :pswitch_0
    return-void

    .line 246
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Music:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    goto :goto_0

    .line 250
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Media:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    goto :goto_0

    .line 254
    :pswitch_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->isIeRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 255
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->InternetExplorer:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    goto :goto_0

    .line 256
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->isTvRunning()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 257
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Tv:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    goto :goto_0

    .line 259
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->App:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    goto :goto_0

    .line 264
    :pswitch_4
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Game:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    goto :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private updateNowPlayingInfo()V
    .locals 4

    .prologue
    .line 273
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v1

    .line 274
    .local v1, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getHeader()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingTitle:Ljava/lang/String;

    .line 275
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getSubHeader()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingSubTitle:Ljava/lang/String;

    .line 276
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getImageUri()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingUri:Ljava/lang/String;

    .line 277
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getPosterImageUri()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingPosterUri:Ljava/lang/String;

    .line 278
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v2

    const v3, 0x38b0b8c2

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->isRunningOOBE:Z

    .line 280
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingProviderOrArtistName:Ljava/lang/String;

    .line 281
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$3;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$NowPlayingTrayViewModel$ContentType:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 293
    :cond_0
    :goto_1
    return-void

    .line 278
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 283
    :pswitch_0
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getProviderName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingProviderOrArtistName:Ljava/lang/String;

    goto :goto_1

    .line 287
    :pswitch_1
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 288
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    if-eqz v2, :cond_0

    .line 289
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getArtistName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingProviderOrArtistName:Ljava/lang/String;

    goto :goto_1

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public canUnsnap()Z
    .locals 2

    .prologue
    .line 382
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    .line 383
    .local v0, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->canUnsnap()Z

    move-result v1

    goto :goto_0
.end method

.method public getBoxArtBackgroundColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getBoxArtBackgroundColor()I

    move-result v0

    goto :goto_0
.end method

.method public getContentType()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    return-object v0
.end method

.method public getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 1

    .prologue
    .line 198
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    goto :goto_0
.end method

.method protected getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 424
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getNowPlayingDefaultAspectRatio()[I
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getDefaultAspectRatio()[I

    move-result-object v0

    goto :goto_0

    :array_0
    .array-data 4
        0x1
        0x1
    .end array-data
.end method

.method public getNowPlayingDefaultImageRid()I
    .locals 1

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getDefaultResourceId()I

    move-result v0

    goto :goto_0
.end method

.method public getNowPlayingProviderOrArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingProviderOrArtistName:Ljava/lang/String;

    return-object v0
.end method

.method public getNowPlayingSubTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingSubTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getNowPlayingTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->isRunningOOBE:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070678

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method public getOOBEImageResourceId()I
    .locals 1

    .prologue
    .line 165
    const v0, 0x7f02019b

    return v0
.end method

.method public getShouldShowBackgroundColor()Z
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getShouldShowBackgroundColor()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShouldShowNowPlaying()Z
    .locals 2

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getContentType()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->None:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShouldShowOOBE()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->isRunningOOBE:Z

    return v0
.end method

.method public getShouldShowRecordThat()Z
    .locals 3

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    .line 182
    .local v0, "tileLocation":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getContentType()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Game:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    if-ne v1, v2, :cond_1

    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Full:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Fill:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTitlePanelImageUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingUri:Ljava/lang/String;

    return-object v0
.end method

.method public hasDefaultCompanion()Z
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public launchCompanion(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 3
    .param p1, "activityData"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 368
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Now Playing Companion"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchNativeOrHtmlCompanion(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 370
    return-void
.end method

.method public launchHelp()V
    .locals 4

    .prologue
    .line 387
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissAppBar()V

    .line 388
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v0

    int-to-long v0, v0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getAppCanonicalId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getHeader()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchHelp(JLjava/lang/String;Ljava/lang/String;)V

    .line 389
    return-void
.end method

.method public navigateToGameProfile()V
    .locals 3

    .prologue
    .line 314
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 315
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedMediaItemData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 318
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getExpandedAppBar()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->dismiss()V

    .line 319
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    invoke-virtual {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    :goto_0
    return-void

    .line 320
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public navigateToNowPlayingDetails()V
    .locals 6

    .prologue
    .line 300
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;

    if-eqz v0, :cond_0

    .line 301
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070344

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070343

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0707c7

    .line 302
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$2;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;)V

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07060d

    .line 307
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 301
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 311
    :goto_0
    return-void

    .line 309
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->navigateToNowPlayingDetailsInternal()V

    goto :goto_0
.end method

.method public nowPlayingBarFocusedImageUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingUri:Ljava/lang/String;

    return-object v0
.end method

.method public nowPlayingBarNonFocusedImageUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingUri:Ljava/lang/String;

    return-object v0
.end method

.method public nowPlayingTabImageUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingUri:Ljava/lang/String;

    return-object v0
.end method

.method public recordThat(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 392
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->recordThat()V

    .line 394
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    .line 395
    .local v0, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-nez v0, :cond_0

    move-object v1, v2

    .line 396
    .local v1, "titleId":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v3

    const-string v4, "Record That"

    invoke-virtual {v3, v4, v2, v1, v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 398
    invoke-virtual {p1, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 399
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->recordEnableHandler:Landroid/os/Handler;

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$EnableButtonRunnable;

    invoke-direct {v3, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$EnableButtonRunnable;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/view/View;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 400
    return-void

    .line 395
    .end local v1    # "titleId":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public setImageBackgroundColor(Landroid/widget/ImageView;)V
    .locals 2
    .param p1, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 105
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 107
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getShouldShowBackgroundColor()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getBoxArtBackgroundColor()I

    move-result v1

    if-eqz v1, :cond_1

    .line 109
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getBoxArtBackgroundColor()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    if-eqz v0, :cond_2

    .line 111
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 113
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected shouldObserveNowPlayingGlobalModel()Z
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x0

    return v0
.end method

.method public stopMediaTimer()V
    .locals 3

    .prologue
    .line 411
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getModel(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    .line 412
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v0, :cond_0

    .line 413
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->stopMediaTimer()V

    .line 415
    :cond_0
    return-void
.end method

.method public titleDetailsFailedToLoad()Z
    .locals 1

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->titleDetailsFailedToLoad()Z

    move-result v0

    goto :goto_0
.end method

.method public unSnap()V
    .locals 4

    .prologue
    .line 373
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingModel()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    .line 374
    .local v0, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v0, :cond_0

    .line 376
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Unsnap"

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->unSnap()V

    .line 379
    :cond_0
    return-void
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    if-nez v2, :cond_1

    .line 209
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getSender()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    if-eqz v2, :cond_1

    .line 210
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getSender()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 211
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 214
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v3

    if-ne v2, v3, :cond_1

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingQuickplay:Lcom/microsoft/xbox/service/model/UpdateType;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->InternetExplorerData:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v1, v2, :cond_1

    .line 217
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$3;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 229
    :goto_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 233
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_1
    return-void

    .line 221
    .restart local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .restart local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->updateContentType()V

    .line 222
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->updateNowPlayingInfo()V

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
