.class public final enum Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;
.super Ljava/lang/Enum;
.source "LinkedAccountHelpers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LinkedAccountState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

.field public static final enum Linked:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

.field public static final enum NeedsRepair:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

.field public static final enum Omitted:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

.field public static final enum Unlinked:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Unknown:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    const-string v1, "Linked"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Linked:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    const-string v1, "Unlinked"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Unlinked:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    const-string v1, "NeedsRepair"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->NeedsRepair:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    const-string v1, "Omitted"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Omitted:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    .line 14
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Unknown:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Linked:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Unlinked:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->NeedsRepair:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->Omitted:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->$VALUES:[Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->$VALUES:[Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountState;

    return-object v0
.end method
