.class public Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;
.super Landroid/widget/GridView;
.source "EPGRecentChannelsView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;,
        Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$IEPGAdapter;,
        Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$OnTapListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "EPGRecentChannelsView"

.field private static final UNMEASURED_ITEM_DIMENSION:I = -0x80000000


# instance fields
.field private mItemHeight:I

.field private mItemWidth:I

.field protected mListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

.field private final mRatioOfItemWidthToBecomeMargins:F

.field private final mWidthToHeightRatioOfCells:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v1, -0x80000000

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mItemHeight:I

    .line 39
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mItemWidth:I

    .line 54
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->getSharedInstance()Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    move-result-object v1

    if-nez v1, :cond_0

    .line 55
    new-instance v1, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;-><init>(Landroid/content/Context;)V

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->setSharedInstance(Lcom/microsoft/xbox/xle/ui/EPGViewConfig;)V

    .line 57
    :cond_0
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->EPGRecentChannelsView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 59
    .local v0, "array":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/high16 v2, 0x7fc00000    # NaNf

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mWidthToHeightRatioOfCells:F

    .line 60
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mRatioOfItemWidthToBecomeMargins:F

    .line 62
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 63
    return-void
.end method

.method private determineChildViewDimensionsAndSpacing(Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$IEPGAdapter;)V
    .locals 7
    .param p1, "adapter"    # Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$IEPGAdapter;

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 94
    if-eqz p1, :cond_0

    .line 95
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mWidthToHeightRatioOfCells:F

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_1

    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->getMeasuredWidth()I

    move-result v1

    .line 98
    .local v1, "mw":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->getNumColumns()I

    move-result v4

    int-to-float v4, v4

    div-float v2, v6, v4

    .line 100
    .local v2, "ratio":F
    int-to-float v4, v1

    mul-float/2addr v4, v2

    float-to-int v3, v4

    .line 101
    .local v3, "rawWidth":I
    int-to-float v4, v3

    iget v5, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mRatioOfItemWidthToBecomeMargins:F

    mul-float/2addr v4, v5

    float-to-int v0, v4

    .line 102
    .local v0, "margins":I
    sub-int v4, v3, v0

    iput v4, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mItemWidth:I

    .line 105
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mItemWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mWidthToHeightRatioOfCells:F

    div-float v5, v6, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mItemHeight:I

    .line 110
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->setVerticalSpacing(I)V

    .line 112
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mItemHeight:I

    iget v5, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mItemWidth:I

    invoke-interface {p1, v4, v5}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$IEPGAdapter;->setDimens(II)V

    .line 121
    .end local v0    # "margins":I
    .end local v1    # "mw":I
    .end local v2    # "ratio":F
    .end local v3    # "rawWidth":I
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0902e1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v0, v4

    .line 117
    .restart local v0    # "margins":I
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->setVerticalSpacing(I)V

    goto :goto_0
.end method

.method private ensureAdapterExists()V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 140
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    if-nez v2, :cond_1

    .line 141
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvRecentChannelsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;

    .line 142
    .local v1, "tvadapter":Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2, v1}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;-><init>(Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;Landroid/content/Context;Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;)V

    .line 143
    .local v0, "adapter":Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mItemHeight:I

    if-eq v2, v4, :cond_0

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mItemWidth:I

    if-eq v2, v4, :cond_0

    .line 144
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mItemHeight:I

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mItemWidth:I

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->setDimens(II)V

    .line 146
    :cond_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 148
    .end local v0    # "adapter":Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;
    .end local v1    # "tvadapter":Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;
    :cond_1
    return-void
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mListeners:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 153
    :cond_0
    return-void
.end method

.method public dataArrived(IIZ)V
    .locals 3
    .param p1, "startChannel"    # I
    .param p2, "endChannel"    # I
    .param p3, "refresh"    # Z

    .prologue
    .line 132
    const-string v0, "EPGRecentChannelsView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dataArrived("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " refresh:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    if-eqz p3, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->invalidateViews()V

    .line 137
    :cond_0
    return-void
.end method

.method public modelChanged()V
    .locals 2

    .prologue
    .line 126
    const-string v0, "EPGRecentChannelsView"

    const-string v1, "modelChanged();"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->ensureAdapterExists()V

    .line 128
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 86
    invoke-super {p0, p1, p2}, Landroid/widget/GridView;->onMeasure(II)V

    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->ensureAdapterExists()V

    .line 88
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->determineChildViewDimensionsAndSpacing(Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$IEPGAdapter;)V

    .line 91
    :cond_0
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mListeners:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 158
    :cond_0
    return-void
.end method

.method public setModel(Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;)V
    .locals 1
    .param p1, "model"    # Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    if-ne v0, p1, :cond_0

    .line 82
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V

    .line 75
    :cond_1
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    if-eqz v0, :cond_2

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V

    goto :goto_0

    .line 80
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method
