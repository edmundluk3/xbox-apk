.class public abstract Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
.super Ljava/lang/Object;
.source "ActivityHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TrendingResponseList"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    .locals 4
    .param p0, "queryType"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "contentCollection"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 138
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 139
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 141
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;

    .line 142
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;->items()Lcom/google/gson/JsonArray;

    move-result-object v1

    .line 144
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;->title()Ljava/lang/String;

    move-result-object v2

    .line 145
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;->description()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;-><init>(Lcom/google/gson/JsonArray;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-object v0
.end method

.method public static with(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    .locals 4
    .param p0, "queryType"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "flatResponse"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 151
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 152
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 154
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;

    .line 155
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;->items()Lcom/google/gson/JsonArray;

    move-result-object v1

    const-string v2, ""

    const-string v3, ""

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;-><init>(Lcom/google/gson/JsonArray;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    return-object v0
.end method

.method public static with(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    .locals 6
    .param p0, "queryType"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "categoriesResponse"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "categoryFilter"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 113
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 114
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 117
    const/4 v1, 0x0

    .line 119
    .local v1, "categoryResult":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;->categories()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;

    .line 120
    .local v0, "cat":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;->category()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    move-result-object v3

    if-ne v3, p2, :cond_0

    .line 121
    move-object v1, v0

    .line 126
    .end local v0    # "cat":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;
    :cond_1
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 128
    new-instance v3, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;

    if-nez v1, :cond_2

    new-instance v2, Lcom/google/gson/JsonArray;

    invoke-direct {v2}, Lcom/google/gson/JsonArray;-><init>()V

    .line 129
    :goto_0
    const-string v4, ""

    const-string v5, ""

    invoke-direct {v3, v2, p0, v4, v5}, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingResponseList;-><init>(Lcom/google/gson/JsonArray;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    return-object v3

    .line 129
    :cond_2
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;->results()Lcom/google/gson/JsonArray;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public abstract description()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public getItemsAsProfileRecentItems()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->items()Lcom/google/gson/JsonArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonArray;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 164
    .local v2, "profileRecentItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->items()Lcom/google/gson/JsonArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonArray;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/JsonElement;

    .line 165
    .local v0, "element":Lcom/google/gson/JsonElement;
    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJsonWithDateAdapter(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 167
    .local v1, "profileRecentItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 169
    if-eqz v1, :cond_0

    .line 171
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->updateSocialInfo()V

    .line 172
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 176
    .end local v0    # "element":Lcom/google/gson/JsonElement;
    .end local v1    # "profileRecentItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_1
    return-object v2
.end method

.method public getItemsAsTrendingTopics()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->items()Lcom/google/gson/JsonArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonArray;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 182
    .local v2, "trendingTopics":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->items()Lcom/google/gson/JsonArray;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonArray;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/JsonElement;

    .line 183
    .local v0, "element":Lcom/google/gson/JsonElement;
    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    .line 184
    .local v1, "trendingTopic":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 186
    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 191
    .end local v0    # "element":Lcom/google/gson/JsonElement;
    .end local v1    # "trendingTopic":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    :cond_1
    return-object v2
.end method

.method public abstract items()Lcom/google/gson/JsonArray;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract queryType()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract title()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
