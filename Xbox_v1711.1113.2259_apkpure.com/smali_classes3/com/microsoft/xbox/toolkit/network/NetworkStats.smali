.class public final enum Lcom/microsoft/xbox/toolkit/network/NetworkStats;
.super Ljava/lang/Enum;
.source "NetworkStats.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/toolkit/network/NetworkStats;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/toolkit/network/NetworkStats;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final connectionClassManager:Lcom/facebook/network/connectionclass/ConnectionClassManager;

.field private final deviceBandwidthSampler:Lcom/facebook/network/connectionclass/DeviceBandwidthSampler;

.field private mostRecentQuality:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    .line 14
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->$VALUES:[Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 62
    const-class v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->TAG:Ljava/lang/String;

    .line 63
    invoke-static {}, Lcom/facebook/network/connectionclass/DeviceBandwidthSampler;->getInstance()Lcom/facebook/network/connectionclass/DeviceBandwidthSampler;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->deviceBandwidthSampler:Lcom/facebook/network/connectionclass/DeviceBandwidthSampler;

    .line 64
    invoke-static {}, Lcom/facebook/network/connectionclass/ConnectionClassManager;->getInstance()Lcom/facebook/network/connectionclass/ConnectionClassManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->connectionClassManager:Lcom/facebook/network/connectionclass/ConnectionClassManager;

    .line 66
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->UNKNOWN:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->mostRecentQuality:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/NetworkStats;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/toolkit/network/NetworkStats;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->$VALUES:[Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/toolkit/network/NetworkStats;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    return-object v0
.end method


# virtual methods
.method public getBandwidthAverageKBits()D
    .locals 4

    .prologue
    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->connectionClassManager:Lcom/facebook/network/connectionclass/ConnectionClassManager;

    invoke-virtual {v1}, Lcom/facebook/network/connectionclass/ConnectionClassManager;->getDownloadKBitsPerSecond()D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 93
    :goto_0
    return-wide v2

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "ex":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->TAG:Ljava/lang/String;

    const-string v2, "getCurrentQuality: "

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 93
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getCurrentQuality()Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 71
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->connectionClassManager:Lcom/facebook/network/connectionclass/ConnectionClassManager;

    invoke-virtual {v2}, Lcom/facebook/network/connectionclass/ConnectionClassManager;->getCurrentBandwidthQuality()Lcom/facebook/network/connectionclass/ConnectionQuality;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->access$000(Lcom/facebook/network/connectionclass/ConnectionQuality;)Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    move-result-object v0

    .line 73
    .local v0, "currentQuality":Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->mostRecentQuality:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    if-eq v2, v0, :cond_0

    .line 74
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Network quality changed from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->mostRecentQuality:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->mostRecentQuality:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    .end local v0    # "currentQuality":Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
    :cond_0
    :goto_0
    return-object v0

    .line 79
    :catch_0
    move-exception v1

    .line 80
    .local v1, "ex":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->TAG:Ljava/lang/String;

    const-string v3, "getCurrentQuality: "

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 83
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->UNKNOWN:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    goto :goto_0
.end method

.method public startSampling()V
    .locals 3

    .prologue
    .line 98
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->deviceBandwidthSampler:Lcom/facebook/network/connectionclass/DeviceBandwidthSampler;

    invoke-virtual {v1}, Lcom/facebook/network/connectionclass/DeviceBandwidthSampler;->startSampling()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "ex":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->TAG:Ljava/lang/String;

    const-string v2, "startSampling: "

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public stopSampling()V
    .locals 3

    .prologue
    .line 106
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->deviceBandwidthSampler:Lcom/facebook/network/connectionclass/DeviceBandwidthSampler;

    invoke-virtual {v1}, Lcom/facebook/network/connectionclass/DeviceBandwidthSampler;->stopSampling()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "ex":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->TAG:Ljava/lang/String;

    const-string v2, "stopSampling: "

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
