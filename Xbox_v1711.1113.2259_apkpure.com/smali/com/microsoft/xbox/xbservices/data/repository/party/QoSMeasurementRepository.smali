.class public Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;
.super Ljava/lang/Object;
.source "QoSMeasurementRepository.java"


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final ADDITIONAL_PING_COUNT:I = 0x6

.field private static final DETAILED_PING_THRESHOLD_MS:I = 0x64

.field private static final INITIAL_PING_COUNT:I = 0x4

.field private static final PING_PACKET_SIZE:I = 0xfa

.field private static final PING_TIMEOUT_MS:I = 0x1f4

.field private static final QOS_SERVER_PORT:I = 0xc03

.field private static final SIX_HOURS_IN_MS:J = 0x1499700L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final gameServerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;

.field private lastPingRetrieval:J

.field private final logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

.field private final qosReport:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerLatency;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;)V
    .locals 1
    .param p1, "gameServerService"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;
    .param p2, "logger"    # Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->gameServerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;

    .line 46
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    .line 47
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->qosReport:Ljava/util/Map;

    .line 48
    return-void
.end method

.method private generateQoSReport()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerLatency;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 69
    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;)Lio/reactivex/SingleOnSubscribe;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private getQosReportFromCache()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerLatency;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->qosReport:Ljava/util/Map;

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$generateQoSReport$0(Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;Lio/reactivex/SingleEmitter;)V
    .locals 22
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;
    .param p1, "s"    # Lio/reactivex/SingleEmitter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->gameServerService:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;

    invoke-interface {v13}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;->getQosServerList()Lio/reactivex/Single;

    move-result-object v13

    invoke-virtual {v13}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QoSServerList;

    .line 71
    .local v9, "qosServers":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QoSServerList;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->qosReport:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    .line 73
    invoke-virtual {v9}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QoSServerList;->qosServers()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;

    .line 74
    .local v12, "server":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;
    const-wide/16 v14, 0x0

    .line 75
    .local v14, "totalPingTime":J
    const/4 v7, 0x0

    .line 76
    .local v7, "pingCount":I
    new-instance v8, Ljava/net/DatagramSocket;

    invoke-direct {v8}, Ljava/net/DatagramSocket;-><init>()V

    .line 77
    .local v8, "pingSocket":Ljava/net/DatagramSocket;
    invoke-virtual {v12}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;->serverFqdn()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/net/Inet4Address;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    .line 79
    .local v4, "addr":Ljava/net/InetAddress;
    const/16 v16, 0x1f4

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    .line 80
    const/16 v16, 0xc03

    move/from16 v0, v16

    invoke-virtual {v8, v4, v0}, Ljava/net/DatagramSocket;->connect(Ljava/net/InetAddress;I)V

    .line 83
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    const/16 v16, 0x4

    move/from16 v0, v16

    if-ge v6, v0, :cond_1

    .line 84
    :try_start_0
    invoke-virtual {v12}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;->targetLocation()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v8, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->pingQosServer(Ljava/net/DatagramSocket;Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v10

    .line 86
    .local v10, "pingTime":J
    if-lez v6, :cond_0

    .line 88
    add-long/2addr v14, v10

    .line 89
    add-int/lit8 v7, v7, 0x1

    .line 83
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 92
    .end local v10    # "pingTime":J
    :catch_0
    move-exception v5

    .line 93
    .local v5, "err":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-object/from16 v16, v0

    sget-object v17, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Caught IOException attempting to ping: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v12}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;->targetLocation()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    const-wide/16 v14, 0x1f4

    .line 95
    const/4 v7, 0x1

    .line 100
    .end local v5    # "err":Ljava/io/IOException;
    :cond_1
    int-to-long v0, v7

    move-wide/from16 v16, v0

    div-long v16, v14, v16

    const-wide/16 v18, 0x64

    cmp-long v16, v16, v18

    if-gez v16, :cond_3

    .line 101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-object/from16 v16, v0

    sget-object v17, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->TAG:Ljava/lang/String;

    const-string v18, "Average ping time is < 100ms, ping 6 more times"

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const/4 v6, 0x0

    :goto_2
    const/16 v16, 0x6

    move/from16 v0, v16

    if-ge v6, v0, :cond_2

    .line 103
    invoke-virtual {v12}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;->targetLocation()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v8, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->pingQosServer(Ljava/net/DatagramSocket;Ljava/lang/String;)J

    move-result-wide v16

    add-long v14, v14, v16

    .line 104
    add-int/lit8 v7, v7, 0x1

    .line 102
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 107
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-object/from16 v16, v0

    sget-object v17, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Average ping time for "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v12}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;->targetLocation()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " is "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    int-to-long v0, v7

    move-wide/from16 v20, v0

    div-long v20, v14, v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "ms"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->qosReport:Ljava/util/Map;

    move-object/from16 v16, v0

    invoke-virtual {v12}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;->targetLocation()Ljava/lang/String;

    move-result-object v17

    int-to-long v0, v7

    move-wide/from16 v18, v0

    div-long v18, v14, v18

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerLatency;->with(J)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerLatency;

    move-result-object v18

    invoke-interface/range {v16 .. v18}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 110
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-object/from16 v16, v0

    sget-object v17, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Average ping time for "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v12}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;->targetLocation()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " is "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    int-to-long v0, v7

    move-wide/from16 v20, v0

    div-long v20, v14, v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "ms"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v16 .. v18}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->qosReport:Ljava/util/Map;

    move-object/from16 v16, v0

    invoke-virtual {v12}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;->targetLocation()Ljava/lang/String;

    move-result-object v17

    int-to-long v0, v7

    move-wide/from16 v18, v0

    div-long v18, v14, v18

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerLatency;->with(J)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerLatency;

    move-result-object v18

    invoke-interface/range {v16 .. v18}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 115
    .end local v4    # "addr":Ljava/net/InetAddress;
    .end local v6    # "i":I
    .end local v7    # "pingCount":I
    .end local v8    # "pingSocket":Ljava/net/DatagramSocket;
    .end local v12    # "server":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;
    .end local v14    # "totalPingTime":J
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->lastPingRetrieval:J

    .line 116
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->qosReport:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    .line 117
    return-void
.end method

.method private pingQosServer(Ljava/net/DatagramSocket;Ljava/lang/String;)J
    .locals 11
    .param p1, "socket"    # Ljava/net/DatagramSocket;
    .param p2, "targetLocation"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0xfa

    const/4 v8, -0x1

    .line 121
    new-array v4, v7, [B

    .line 122
    .local v4, "packetContents":[B
    new-instance v7, Ljava/util/Random;

    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    invoke-virtual {v7, v4}, Ljava/util/Random;->nextBytes([B)V

    .line 123
    const/4 v7, 0x0

    aput-byte v8, v4, v7

    .line 124
    const/4 v7, 0x1

    aput-byte v8, v4, v7

    .line 126
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 127
    .local v0, "currentTimer":J
    new-instance v3, Ljava/net/DatagramPacket;

    array-length v7, v4

    invoke-direct {v3, v4, v7}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 128
    .local v3, "packet":Ljava/net/DatagramPacket;
    invoke-virtual {p1, v3}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    .line 131
    const/16 v7, 0xfa

    :try_start_0
    new-array v5, v7, [B

    .line 132
    .local v5, "recvData":[B
    new-instance v6, Ljava/net/DatagramPacket;

    array-length v7, v5

    invoke-direct {v6, v5, v7}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 133
    .local v6, "recvPacket":Ljava/net/DatagramPacket;
    invoke-virtual {p1, v6}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    sub-long/2addr v8, v0

    .line 137
    .end local v5    # "recvData":[B
    .end local v6    # "recvPacket":Ljava/net/DatagramPacket;
    :goto_0
    return-wide v8

    .line 135
    :catch_0
    move-exception v2

    .line 136
    .local v2, "e":Ljava/net/SocketTimeoutException;
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ping timed out for "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-wide/16 v8, 0x1f4

    goto :goto_0
.end method


# virtual methods
.method public getQoSReport()Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerLatency;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->lastPingRetrieval:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->lastPingRetrieval:J

    const-wide/32 v2, 0x1499700

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->TAG:Ljava/lang/String;

    const-string v2, "Generating qosReport"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->generateQoSReport()Lio/reactivex/Single;

    move-result-object v0

    .line 56
    :goto_0
    return-object v0

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->TAG:Ljava/lang/String;

    const-string v2, "Getting qosReport from cache"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;->getQosReportFromCache()Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0
.end method
