.class public interface abstract Lcom/microsoft/xbox/domain/auth/AuthStateManager;
.super Ljava/lang/Object;
.source "AuthStateManager.java"


# virtual methods
.method public abstract getAuthStates()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isBusy()Z
.end method

.method public abstract isNewUser()Z
.end method

.method public abstract isSignedIn()Z
.end method

.method public abstract isSignedOut()Z
.end method

.method public abstract isSigningIn()Z
.end method

.method public abstract isSigningOut()Z
.end method

.method public abstract signInSilentlyXsapi()V
.end method

.method public abstract signInWithUi()V
.end method

.method public abstract signOutWithUi()V
.end method

.method public abstract signOutXsapi()V
.end method
