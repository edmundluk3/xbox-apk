.class public abstract Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;
.super Ljava/lang/Object;
.source "PartyEventDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PartyMemberStartedBroadcastingEvent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;
    .locals 1
    .param p0, "member"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 42
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyEventDataTypes_PartyMemberStartedBroadcastingEvent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyEventDataTypes_PartyMemberStartedBroadcastingEvent;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V

    return-object v0
.end method


# virtual methods
.method public abstract member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
