.class public Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "EditTextDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private color:I

.field private doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private editCompletionHander:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;

.field private editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private editTextCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private headerText:Ljava/lang/String;

.field private headerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private hintText:Ljava/lang/String;

.field private initialText:Ljava/lang/String;

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private maxCount:I

.field private rootView:Landroid/widget/RelativeLayout;

.field private final textWatcher:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    const v0, 0x7f08021b

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 102
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->textWatcher:Landroid/text/TextWatcher;

    .line 71
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->layoutInflater:Landroid/view/LayoutInflater;

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->layoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0300e6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->rootView:Landroid/widget/RelativeLayout;

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->rootView:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->setContentView(Landroid/view/View;)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e0557

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->headerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e0559

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e0558

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setHorizontallyScrolling(Z)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setMaxLines(I)V

    .line 79
    const v0, 0x7f0e04ef

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editTextCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 80
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "headerText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "initialText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "maxCharacterCount"    # I
        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "hintText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "color"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p7, "editCompletionHander"    # Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;-><init>(Landroid/content/Context;)V

    .line 57
    const-wide/16 v0, 0x1

    int-to-long v2, p4

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 58
    invoke-static {p7}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 60
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->headerText:Ljava/lang/String;

    .line 61
    iput p4, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->maxCount:I

    .line 62
    iput-object p5, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->hintText:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->initialText:Ljava/lang/String;

    .line 64
    iput p6, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->color:I

    .line 65
    iput-object p7, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editCompletionHander:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v3, p4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editTextCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    .prologue
    .line 31
    iget v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->maxCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;Landroid/widget/TextView;Landroid/widget/TextView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # Landroid/widget/TextView;
    .param p3, "x3"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->updateEditTextCount(Landroid/widget/TextView;Landroid/widget/TextView;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;)Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editCompletionHander:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;

    return-object v0
.end method

.method private updateEditTextCount(Landroid/widget/TextView;Landroid/widget/TextView;I)V
    .locals 8
    .param p1, "editText"    # Landroid/widget/TextView;
    .param p2, "count"    # Landroid/widget/TextView;
    .param p3, "maxCount"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 118
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 119
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 120
    .local v0, "length":I
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "%d/%d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070d71

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/Object;

    .line 122
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    .line 121
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 124
    .end local v0    # "length":I
    :cond_0
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onBackPressed()V

    .line 163
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissEditDialog()V

    .line 164
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->rootView:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 137
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->rootView:Landroid/widget/RelativeLayout;

    iget v2, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->color:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->headerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->headerText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 141
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->initialText:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 142
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editTextCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->initialText:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setSelection(I)V

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editTextCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget v2, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->maxCount:I

    invoke-direct {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->updateEditTextCount(Landroid/widget/TextView;Landroid/widget/TextView;I)V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->textWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->hintText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 158
    :cond_1
    return-void

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->initialText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 168
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStop()V

    .line 169
    return-void
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 131
    iput p1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->color:I

    .line 132
    return-void
.end method

.method public setEditCompletionHander(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;)V
    .locals 0
    .param p1, "editCompletionHander"    # Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 98
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 99
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editCompletionHander:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;

    .line 100
    return-void
.end method

.method public setHeaderText(Ljava/lang/String;)V
    .locals 0
    .param p1, "headerText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 84
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->headerText:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setHintText(Ljava/lang/String;)V
    .locals 0
    .param p1, "hintText"    # Ljava/lang/String;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->hintText:Ljava/lang/String;

    .line 128
    return-void
.end method

.method public setInitialText(Ljava/lang/String;)V
    .locals 0
    .param p1, "initialText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 88
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->initialText:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public setMaxCharacterLength(I)V
    .locals 4
    .param p1, "maxCharacterLength"    # I
        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 92
    const-wide/16 v0, 0x1

    int-to-long v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 93
    iput p1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->maxCount:I

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v3, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 95
    return-void
.end method
