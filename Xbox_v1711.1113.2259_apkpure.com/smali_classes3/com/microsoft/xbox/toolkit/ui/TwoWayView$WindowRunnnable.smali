.class Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;
.super Ljava/lang/Object;
.source "TwoWayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WindowRunnnable"
.end annotation


# instance fields
.field private mOriginalAttachCount:I

.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 0

    .prologue
    .line 6012
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p2, "x1"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;

    .prologue
    .line 6012
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    return-void
.end method


# virtual methods
.method public rememberWindowAttachCount()V
    .locals 1

    .prologue
    .line 6016
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3000(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;->mOriginalAttachCount:I

    .line 6017
    return-void
.end method

.method public sameWindow()Z
    .locals 2

    .prologue
    .line 6020
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3100(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;->mOriginalAttachCount:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
