.class public Lcom/microsoft/xbox/xle/model/AutoConnectModel;
.super Lcom/microsoft/xbox/toolkit/XLEObservable;
.source "AutoConnectModel.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;,
        Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEObservable",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field private static AUTOCONNECT_SUCCESS_STATUS:Ljava/lang/String; = null

.field private static final AUTO_RETRY_COUNT:I = 0x3

.field private static final AUTO_RETRY_WAIT:I = 0x7d0

.field private static final KEEP_CONNECTION_TIME_SPAN:I = 0x493e0

.field private static instance:Lcom/microsoft/xbox/xle/model/AutoConnectModel;


# instance fields
.field private autoConnectTask:Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;

.field private final dismissHandler:Landroid/os/Handler;

.field private dismissTask:Ljava/lang/Runnable;

.field private isStarted:Z

.field private retryTask:Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;

.field private sessionReady:Lcom/microsoft/xbox/toolkit/Ready;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "AUTOCONNECT_SUCCESS_STATUS"

    sput-object v0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->AUTOCONNECT_SUCCESS_STATUS:Ljava/lang/String;

    .line 38
    new-instance v0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->instance:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEObservable;-><init>()V

    .line 44
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissHandler:Landroid/os/Handler;

    .line 48
    new-instance v0, Lcom/microsoft/xbox/toolkit/Ready;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/Ready;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->sessionReady:Lcom/microsoft/xbox/toolkit/Ready;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;-><init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/xle/model/AutoConnectModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->autoConnectTask:Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;-><init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/xle/model/AutoConnectModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->retryTask:Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;

    .line 52
    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissTask:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->onConnectToConsoleCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/model/AutoConnectModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->isStarted:Z

    return v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/xle/model/ConsoleData;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->connectToConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/model/AutoConnectModel;)Lcom/microsoft/xbox/toolkit/Ready;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->sessionReady:Lcom/microsoft/xbox/toolkit/Ready;

    return-object v0
.end method

.method private autoConnectAndLaunch()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 91
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 92
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getActiveConnection()I

    move-result v0

    .line 93
    .local v0, "connectionTye":I
    if-eq v0, v4, :cond_0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 94
    :cond_0
    const-string v1, "AutoConnectAndLaunch"

    const-string v2, "start called "

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->isStarted:Z

    if-nez v1, :cond_2

    .line 97
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->isStarted:Z

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->sessionReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->autoConnectTask:Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->execute()V

    .line 100
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->AutoConnectStarted:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v3, 0x0

    invoke-direct {v1, v2, p0, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 105
    :cond_1
    :goto_0
    return-void

    .line 102
    :cond_2
    const-string v1, "AutoConnectAndLaunch"

    const-string v2, "Already start, should not happen"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private connectToConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z
    .locals 7
    .param p1, "consoleData"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 379
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 380
    new-instance v0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$2;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$2;-><init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadSend(Ljava/lang/Runnable;)V

    .line 395
    const-string v0, "AutoConnect"

    const-string v3, "wait for session state change"

    invoke-static {v0, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->sessionReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->waitForReady()V

    .line 398
    const-string v0, "AutoConnect"

    const-string v3, "wake up"

    invoke-static {v0, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->isStarted:Z

    if-nez v0, :cond_0

    .line 401
    const-string v0, "AutoConnect"

    const-string v1, "already paused, let\'s fail"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const-string v0, "Auto Console Connect"

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getLastErrorCode()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :goto_0
    return v2

    .line 409
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    if-eq v0, v1, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 410
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v2, v1

    .line 411
    goto :goto_0

    :cond_1
    move v0, v2

    .line 409
    goto :goto_1

    .line 414
    :cond_2
    const-string v0, "Auto Console Connect"

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getLastErrorCode()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/model/AutoConnectModel;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->instance:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    return-object v0
.end method

.method public static getSuccessState(Landroid/os/Bundle;)Z
    .locals 1
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 161
    if-eqz p0, :cond_0

    .line 162
    sget-object v0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->AUTOCONNECT_SUCCESS_STATUS:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 164
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onConnectToConsoleCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v3, 0x1

    .line 199
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    .line 200
    .local v1, "failed":Z
    if-eqz v1, :cond_0

    .line 201
    const-string v2, "AutoConnect"

    const-string v4, "failed to connect to console"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 207
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v4, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->AUTOCONNECT_SUCCESS_STATUS:Ljava/lang/String;

    if-nez v1, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 208
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v4, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v5, Lcom/microsoft/xbox/service/model/UpdateType;->AutoConnectCompleted:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v4, v5, v3, v0}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;)V

    const/4 v3, 0x0

    invoke-direct {v2, v4, p0, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 210
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->safeDismiss()V

    .line 211
    return-void

    .line 203
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    const-string v2, "AutoConnect"

    const-string v4, "success"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 207
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private safeDismiss()V
    .locals 2

    .prologue
    .line 214
    const-string v0, "AutoConnectAndRety"

    const-string v1, "safe dismiss"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismiss()V

    .line 217
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SessionModel;->load(Z)V

    .line 218
    return-void
.end method

.method private setSessionReady()V
    .locals 1

    .prologue
    .line 194
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->sessionReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    .line 196
    return-void
.end method

.method private updateSessionState(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    .line 170
    .local v0, "newSessionState":I
    const-string v1, "AutoConnectAndLaunch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SessionState update   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-nez v1, :cond_0

    .line 173
    const-string v1, "AutoConnect"

    const-string v2, "update is not final, ignore"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :goto_0
    return-void

    .line 177
    :cond_0
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 179
    :pswitch_1
    const-string v1, "AutoConnect"

    const-string v2, "set ready because disconnected"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->setSessionReady()V

    goto :goto_0

    .line 183
    :pswitch_2
    const-string v1, "AutoConnect"

    const-string v2, "set ready because connect failed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->setSessionReady()V

    goto :goto_0

    .line 187
    :pswitch_3
    const-string v1, "AutoConnect"

    const-string v2, "set ready because connected"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->setSessionReady()V

    goto :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public autoRetryConnect()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 108
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 109
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getActiveConnection()I

    move-result v0

    .line 110
    .local v0, "connectionTye":I
    if-eq v0, v4, :cond_0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 111
    :cond_0
    const-string v1, "AutoConnectAndLaunch"

    const-string v2, "autoRetryConnect called"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->isStarted:Z

    if-nez v1, :cond_2

    .line 113
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->isStarted:Z

    .line 114
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/service/model/SessionModel;->setRetryConnectingStatus(Z)V

    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->sessionReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->retryTask:Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->execute()V

    .line 117
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->AutoConnectStarted:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v3, 0x0

    invoke-direct {v1, v2, p0, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 122
    :cond_1
    :goto_0
    return-void

    .line 119
    :cond_2
    const-string v1, "AutoConnectAndLaunch"

    const-string v2, "Already start, auto retry should not happen"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public dismiss()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    const-string v0, "AutoConnect"

    const-string v1, "dismiss called"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->isStarted:Z

    if-nez v0, :cond_0

    .line 127
    const-string v0, "AutoConnect"

    const-string v1, "not started, ignore"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 132
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->falseStart(Z)V

    .line 133
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->setRetryConnectingStatus(Z)V

    .line 135
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->isStarted:Z

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->sessionReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->autoConnectTask:Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->cancel()V

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->retryTask:Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->cancel()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 59
    const-string v0, "AutoConnectAndLaunch"

    const-string v1, "onPause is called"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissTask:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 63
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$1;-><init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissTask:Ljava/lang/Runnable;

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissTask:Ljava/lang/Runnable;

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 70
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissTask:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissTask:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 75
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->dismissTask:Ljava/lang/Runnable;

    .line 78
    :cond_0
    const-string v1, "AutoConnectAndLaunch"

    const-string v2, "onResume is called"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 80
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-nez v1, :cond_1

    .line 82
    const-string v1, "AutoConnectAndLaunch"

    const-string v2, "app started when phone is off, don\'t auto launch"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_1
    const-string v1, "AutoConnectAndLaunch"

    const-string v2, "screen is on, let\'s continue"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->autoConnectAndLaunch()V

    goto :goto_0
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    .line 148
    .local v0, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    const-string v1, "AutoConnectAndLaunch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received update: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    sget-object v1, Lcom/microsoft/xbox/xle/model/AutoConnectModel$3;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 155
    const-string v1, "AutoConnectAndLaunch"

    const-string v2, "update type known return and ignore"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :goto_0
    return-void

    .line 151
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->updateSessionState(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0

    .line 149
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
