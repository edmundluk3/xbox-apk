.class Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter$2;
.super Ljava/lang/Object;
.source "TVSeriesSeasonsScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->updateViewOverride()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->onSeasonChanged(I)V

    .line 77
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
