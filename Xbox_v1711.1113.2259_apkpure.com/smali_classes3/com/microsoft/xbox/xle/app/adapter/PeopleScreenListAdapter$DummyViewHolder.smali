.class Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;
.super Ljava/lang/Object;
.source "PeopleScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DummyViewHolder"
.end annotation


# instance fields
.field facebookImageMD:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08df
    .end annotation
.end field

.field friendsHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08e7
    .end annotation
.end field

.field linkToFacebookItem:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08de
    .end annotation
.end field

.field linkToPhoneContactItem:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08e2
    .end annotation
.end field

.field listStateText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08e8
    .end annotation
.end field

.field recommendationsHeader:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08db
    .end annotation
.end field

.field seeAllRecommendationsButton:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08dd
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 391
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 393
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 394
    return-void
.end method

.method static synthetic lambda$bindTo$0(Landroid/view/View;)V
    .locals 3
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 415
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends;->trackSeeAllClicked()V

    .line 416
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 417
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V
    .locals 7
    .param p1, "followerData"    # Lcom/microsoft/xbox/service/model/FollowersData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/4 v6, 0x0

    .line 397
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 398
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 400
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->recommendationsHeader:Landroid/view/View;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 401
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->friendsHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 402
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->linkToFacebookItem:Landroid/view/View;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 403
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->linkToPhoneContactItem:Landroid/view/View;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 404
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->listStateText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 406
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 408
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->friendsHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getOfflinePeopleCountLabel()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 411
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->recommendationsHeader:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 412
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->seeAllRecommendationsButton:Landroid/widget/RelativeLayout;

    invoke-static {v2, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 413
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->seeAllRecommendationsButton:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    .line 414
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->seeAllRecommendationsButton:Landroid/widget/RelativeLayout;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder$$Lambda$1;->lambdaFactory$()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 424
    :pswitch_2
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->FacebookFriend:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;->MEDIUM:Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->getIconBySize(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;)Ljava/lang/String;

    move-result-object v0

    .line 425
    .local v0, "facebookImageMd":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->facebookImageMD:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 426
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->facebookImageMD:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 428
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->linkToFacebookItem:Landroid/view/View;

    invoke-static {v2, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0

    .line 431
    .end local v0    # "facebookImageMd":Ljava/lang/String;
    :pswitch_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->linkToPhoneContactItem:Landroid/view/View;

    invoke-static {v2, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0

    .line 434
    :pswitch_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->friendsHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFilteredPeopleCountLabel()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 437
    :pswitch_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->friendsHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07032c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2, v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 440
    :pswitch_6
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070590

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getQueryText()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 441
    .local v1, "searchString":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->friendsHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v2, v1, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 444
    .end local v1    # "searchString":Ljava/lang/String;
    :pswitch_7
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->listStateText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFilteredNoContentText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto/16 :goto_0

    .line 447
    :pswitch_8
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->listStateText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070b6d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2, v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto/16 :goto_0

    .line 406
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
