.class Lcom/microsoft/xbox/xle/epg/TvStreamer$8;
.super Ljava/lang/Object;
.source "TvStreamer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamer;-><init>(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$8;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 266
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$8;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getQuality()Ljava/lang/String;

    move-result-object v2

    .line 272
    .local v2, "quality":Ljava/lang/String;
    const-string v3, "high"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 273
    const/4 v0, 0x0

    .line 281
    .local v0, "checkedItem":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$8;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$200(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/PopupMenu;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/Menu;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 282
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$8;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$200(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/PopupMenu;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-interface {v3, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-ne v1, v0, :cond_3

    const/4 v3, 0x1

    :goto_2
    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 281
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 274
    .end local v0    # "checkedItem":I
    .end local v1    # "i":I
    :cond_0
    const-string v3, "medium"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 275
    const/4 v0, 0x1

    .restart local v0    # "checkedItem":I
    goto :goto_0

    .line 276
    .end local v0    # "checkedItem":I
    :cond_1
    const-string v3, "low"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 277
    const/4 v0, 0x2

    .restart local v0    # "checkedItem":I
    goto :goto_0

    .line 279
    .end local v0    # "checkedItem":I
    :cond_2
    const/4 v0, 0x1

    .restart local v0    # "checkedItem":I
    goto :goto_0

    .line 282
    .restart local v1    # "i":I
    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    .line 286
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$8;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$200(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/PopupMenu;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->show()V

    .line 287
    return-void
.end method
