.class Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;
.super Ljava/lang/Object;
.source "AppendOnlyLinkedArrayList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList$NonThrowingPredicate;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final capacity:I

.field private final head:[Ljava/lang/Object;

.field private offset:I

.field private tail:[Ljava/lang/Object;


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 33
    .local p0, "this":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->capacity:I

    .line 35
    add-int/lit8 v0, p1, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->head:[Ljava/lang/Object;

    .line 36
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->head:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->tail:[Ljava/lang/Object;

    .line 37
    return-void
.end method


# virtual methods
.method accept(Lcom/jakewharton/rxrelay2/Relay;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/Relay",
            "<-TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 92
    .local p0, "this":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    .local p1, "observer":Lcom/jakewharton/rxrelay2/Relay;, "Lcom/jakewharton/rxrelay2/Relay<-TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->head:[Ljava/lang/Object;

    .line 93
    .local v0, "a":[Ljava/lang/Object;
    iget v1, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->capacity:I

    .line 94
    .local v1, "c":I
    :goto_0
    if-eqz v0, :cond_2

    .line 95
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_0

    .line 96
    aget-object v3, v0, v2

    .line 97
    .local v3, "o":Ljava/lang/Object;
    if-nez v3, :cond_1

    .line 103
    .end local v3    # "o":Ljava/lang/Object;
    :cond_0
    aget-object v4, v0, v1

    check-cast v4, [Ljava/lang/Object;

    move-object v0, v4

    check-cast v0, [Ljava/lang/Object;

    goto :goto_0

    .line 101
    .restart local v3    # "o":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p1, v3}, Lcom/jakewharton/rxrelay2/Relay;->accept(Ljava/lang/Object;)V

    .line 95
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 105
    .end local v2    # "i":I
    .end local v3    # "o":Ljava/lang/Object;
    :cond_2
    const/4 v4, 0x0

    return v4
.end method

.method add(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    iget v0, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->capacity:I

    .line 46
    .local v0, "c":I
    iget v2, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->offset:I

    .line 47
    .local v2, "o":I
    if-ne v2, v0, :cond_0

    .line 48
    add-int/lit8 v3, v0, 0x1

    new-array v1, v3, [Ljava/lang/Object;

    .line 49
    .local v1, "next":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->tail:[Ljava/lang/Object;

    aput-object v1, v3, v0

    .line 50
    iput-object v1, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->tail:[Ljava/lang/Object;

    .line 51
    const/4 v2, 0x0

    .line 53
    .end local v1    # "next":[Ljava/lang/Object;
    :cond_0
    iget-object v3, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->tail:[Ljava/lang/Object;

    aput-object p1, v3, v2

    .line 54
    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->offset:I

    .line 55
    return-void
.end method

.method forEachWhile(Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList$NonThrowingPredicate;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList$NonThrowingPredicate",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    .local p1, "consumer":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList$NonThrowingPredicate;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList$NonThrowingPredicate<-TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->head:[Ljava/lang/Object;

    .line 75
    .local v0, "a":[Ljava/lang/Object;
    iget v1, p0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->capacity:I

    .line 76
    .local v1, "c":I
    :goto_0
    if-eqz v0, :cond_2

    .line 77
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_0

    .line 78
    aget-object v3, v0, v2

    .line 79
    .local v3, "o":Ljava/lang/Object;
    if-nez v3, :cond_1

    .line 86
    .end local v3    # "o":Ljava/lang/Object;
    :cond_0
    aget-object v4, v0, v1

    check-cast v4, [Ljava/lang/Object;

    move-object v0, v4

    check-cast v0, [Ljava/lang/Object;

    goto :goto_0

    .line 82
    .restart local v3    # "o":Ljava/lang/Object;
    :cond_1
    invoke-interface {p1, v3}, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList$NonThrowingPredicate;->test(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 77
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 88
    .end local v2    # "i":I
    .end local v3    # "o":Ljava/lang/Object;
    :cond_2
    return-void
.end method
