.class public Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;
.super Ljava/lang/Object;
.source "BeamChannelDataMapper.java"

# interfaces
.implements Lcom/microsoft/xbox/data/repository/DataMapper;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/data/repository/DataMapper",
        "<",
        "Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;",
        "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method


# virtual methods
.method public apply(Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;)Lio/reactivex/Observable;
    .locals 9
    .param p1, "data"    # Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->id()Ljava/lang/Integer;

    move-result-object v0

    .line 27
    .local v0, "id":Ljava/lang/Integer;
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->name()Ljava/lang/String;

    move-result-object v1

    .line 28
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->token()Ljava/lang/String;

    move-result-object v3

    .line 29
    .local v3, "token":Ljava/lang/String;
    if-eqz v0, :cond_0

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "https://thumbs.beam.pro/channel/%s.small.jpg"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 30
    .local v2, "thumbnailUrl":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->viewersCurrent()Ljava/lang/Integer;

    move-result-object v4

    .line 32
    .local v4, "viewersCount":Ljava/lang/Integer;
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    if-eqz v4, :cond_1

    .line 37
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v0, v1, v3, v2, v5}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->with(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/microsoft/xbox/domain/beam/BeamChannel;

    move-result-object v5

    invoke-static {v5}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v5

    .line 40
    :goto_1
    return-object v5

    .line 29
    .end local v2    # "thumbnailUrl":Ljava/lang/String;
    .end local v4    # "viewersCount":Ljava/lang/Integer;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 39
    .restart local v2    # "thumbnailUrl":Ljava/lang/String;
    .restart local v4    # "viewersCount":Ljava/lang/Integer;
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to map channel: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 40
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v5

    goto :goto_1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 17
    check-cast p1, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;->apply(Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 17
    check-cast p1, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;->apply(Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
