.class public Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;
.super Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;
.source "GamerscoreLeaderboardInsertViewModel.java"


# static fields
.field private static final FOCUS_ITEM_COUNT:I = 0x1

.field private static final TOP_ITEM_COUNT:I = 0x3

.field private static final TOTAL_ITEM_COUNT:I = 0x4


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 0
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "parent"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 25
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 27
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->setParent(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 28
    return-void
.end method


# virtual methods
.method public goToDetailView()V
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreen;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 52
    return-void
.end method

.method public load(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->load(Z)V

    .line 39
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->hasData()Z

    move-result v0

    if-nez v0, :cond_1

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, p1, v2, v3}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->loadMoreAsync(ZJ)V

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_3

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->hasFocusData()Z

    move-result v0

    if-nez v0, :cond_3

    .line 44
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->loadFocusAsync(ZLjava/lang/String;J)V

    .line 47
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->updateLeaderboard()V

    .line 48
    return-void
.end method

.method protected updateLeaderboard()V
    .locals 15

    .prologue
    const/4 v2, 0x0

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->userDataMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->hasData()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->hasFocusData()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->leaderboardList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getData()Ljava/util/List;

    move-result-object v13

    .line 59
    .local v13, "topEntries":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getFocusData()Ljava/util/List;

    move-result-object v8

    .line 61
    .local v8, "focusEntries":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v12

    .line 62
    .local v12, "selfXuid":Ljava/lang/String;
    :goto_0
    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;

    iget-object v7, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    .line 63
    .local v7, "firstFocusXuid":Ljava/lang/String;
    :goto_1
    const/4 v10, 0x0

    .line 68
    .local v10, "mergeLists":Z
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    const/4 v0, 0x4

    if-ge v9, v0, :cond_7

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v0

    if-ge v9, v0, :cond_7

    .line 71
    if-nez v10, :cond_0

    const/4 v0, 0x3

    if-ge v9, v0, :cond_6

    .line 72
    :cond_0
    invoke-interface {v13, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;

    .line 73
    .local v11, "nextEntry":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;
    iget-object v0, v11, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    invoke-static {v0, v7}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    const/4 v10, 0x1

    .line 80
    :cond_1
    :goto_3
    iget-object v0, v11, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    invoke-static {v0, v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 81
    .local v6, "isSelf":Z
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->getGamerscore()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->topMonthlyGamerscore:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->topMonthlyGamerscore:J

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->userDataMap:Ljava/util/Map;

    iget-object v2, v11, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 83
    .local v1, "userSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v1, :cond_2

    .line 84
    iget-object v14, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->leaderboardList:Ljava/util/List;

    new-instance v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    iget-wide v2, v11, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->rank:J

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->getGamerscore()J

    move-result-wide v4

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;JJZ)V

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_2
    if-eqz v6, :cond_3

    .line 88
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->getGamerscore()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->selfMonthlyGamerscore:J

    .line 68
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 61
    .end local v1    # "userSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v6    # "isSelf":Z
    .end local v7    # "firstFocusXuid":Ljava/lang/String;
    .end local v9    # "i":I
    .end local v10    # "mergeLists":Z
    .end local v11    # "nextEntry":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;
    .end local v12    # "selfXuid":Ljava/lang/String;
    :cond_4
    const-string v12, ""

    goto :goto_0

    .line 62
    .restart local v12    # "selfXuid":Ljava/lang/String;
    :cond_5
    const-string v7, ""

    goto :goto_1

    .line 77
    .restart local v7    # "firstFocusXuid":Ljava/lang/String;
    .restart local v9    # "i":I
    .restart local v10    # "mergeLists":Z
    :cond_6
    add-int/lit8 v0, v9, -0x3

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;

    .restart local v11    # "nextEntry":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;
    goto :goto_3

    .line 92
    .end local v11    # "nextEntry":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;
    :cond_7
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->updateAdapter()V

    .line 96
    .end local v7    # "firstFocusXuid":Ljava/lang/String;
    .end local v8    # "focusEntries":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    .end local v9    # "i":I
    .end local v10    # "mergeLists":Z
    .end local v12    # "selfXuid":Ljava/lang/String;
    .end local v13    # "topEntries":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    :cond_8
    return-void
.end method

.method protected updateWithoutAdapter()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    return v0
.end method
