.class public Lcom/microsoft/xbox/service/model/recents/RecentItem;
.super Ljava/lang/Object;
.source "RecentItem.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/pins/LaunchableItem;


# instance fields
.field public AcquisitionContext:Ljava/lang/String;

.field public AltImageUrl:Ljava/lang/String;

.field public ChildItemCatalogId:Ljava/lang/String;

.field public ContentType:Ljava/lang/String;

.field public CurrentPositionMilliseconds:J

.field public DeviceId:Ljava/lang/String;

.field public DeviceType:Ljava/lang/String;

.field public EventDateTimeUtc:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;
    .end annotation
.end field

.field public ImageUrl:Ljava/lang/String;

.field public IsDefaultPin:Z

.field public ItemId:Ljava/lang/String;

.field public Locale:Ljava/lang/String;

.field public MediaLengthMs:J

.field public Provider:Ljava/lang/String;

.field public ProviderId:Ljava/lang/String;

.field public SubTitle:Ljava/lang/String;

.field public Title:Ljava/lang/String;

.field private transient boxArtBackgroundColor:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private transient hashCode:I

.field private launchUri:Ljava/lang/String;

.field private provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getMergedCompanionModel()Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    .locals 9

    .prologue
    .line 169
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/recents/RecentItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    .line 171
    .local v2, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v2, :cond_2

    .line 173
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 174
    .local v0, "mediaModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->hasValidData()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 175
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v3

    .line 176
    .local v3, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    if-nez v3, :cond_1

    const-wide/16 v4, -0x1

    .line 177
    .local v4, "titleId":J
    :goto_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v7

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 178
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v8

    .line 177
    invoke-static {v6, v7, v8, v4, v5}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getModel(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v1

    .line 179
    .local v1, "mergedModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    if-eqz v1, :cond_0

    .line 180
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->load(Z)V

    .line 186
    .end local v0    # "mediaModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .end local v1    # "mergedModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    .end local v3    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .end local v4    # "titleId":J
    :cond_0
    :goto_1
    return-object v1

    .line 176
    .restart local v0    # "mediaModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .restart local v3    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_1
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    goto :goto_0

    .line 186
    .end local v0    # "mediaModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .end local v3    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 191
    if-ne p1, p0, :cond_1

    .line 197
    :cond_0
    :goto_0
    return v1

    .line 193
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/model/recents/RecentItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 194
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 196
    check-cast v0, Lcom/microsoft/xbox/service/model/recents/RecentItem;

    .line 197
    .local v0, "that":Lcom/microsoft/xbox/service/model/recents/RecentItem;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ContentType:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ContentType:Ljava/lang/String;

    .line 198
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getBoxArtBackgroundColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 225
    iget v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->boxArtBackgroundColor:I

    return v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ContentType:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 2

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getMergedCompanionModel()Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v0

    .line 147
    .local v0, "mergedModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->hasValidData()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getHasActivities()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getFeaturedActivity()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v1

    .line 150
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getIsMusicPlayList()Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method public getIsTVChannel()Z
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public getIsWebLink()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public getItemId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    return-object v0
.end method

.method public getLaunchUri()Ljava/lang/String;
    .locals 6

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getProviderTitleId()J

    move-result-wide v0

    const-wide/32 v2, 0x2cb9057f

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 157
    const-string v0, "ms-xbox-dashboard://home?view=home"

    .line 160
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->launchUri:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->launchUri:Ljava/lang/String;

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ms-xbl-%08X://default/"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getProviderTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getProviderTitleId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setTitleId(J)V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setName(Ljava/lang/String;)V

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    return-object v0
.end method

.method public getProviderMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderTitleId()J
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Provider:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->parseHexLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getProviderTitleIdString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Provider:Ljava/lang/String;

    return-object v0
.end method

.method public getShouldShowBackgroundColor()Z
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ContentType:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "DApp"

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ContentType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    return-object v0
.end method

.method public hasCompanion()Z
    .locals 3

    .prologue
    .line 135
    const/4 v1, 0x0

    .line 136
    .local v1, "ret":Z
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getMergedCompanionModel()Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v0

    .line 137
    .local v0, "mergedModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->hasValidData()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getHasFeaturedActivity()Z

    move-result v1

    .line 141
    :cond_0
    return v1
.end method

.method public hasDetails()Z
    .locals 1

    .prologue
    .line 110
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->hasDetails(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    return v0
.end method

.method public hasProvider()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 204
    iget v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->hashCode:I

    if-nez v0, :cond_0

    .line 205
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->hashCode:I

    .line 206
    iget v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->hashCode:I

    .line 207
    iget v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ContentType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->hashCode:I

    .line 210
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->hashCode:I

    return v0
.end method

.method public isDLC()Z
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    return v0
.end method

.method public setBoxArtBackgroundColor(I)V
    .locals 0
    .param p1, "colorInt"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 219
    iput p1, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->boxArtBackgroundColor:I

    .line 220
    return-void
.end method

.method public setLaunchUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "launchUri"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 164
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 165
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->launchUri:Ljava/lang/String;

    .line 166
    return-void
.end method
