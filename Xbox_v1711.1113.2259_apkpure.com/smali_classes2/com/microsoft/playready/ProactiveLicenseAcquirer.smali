.class Lcom/microsoft/playready/ProactiveLicenseAcquirer;
.super Lcom/microsoft/playready/LicenseAcquirerBase;
.source "LicenseAcquirer.java"

# interfaces
.implements Lcom/microsoft/playready/ILicenseAcquirer;


# instance fields
.field private final mExecService:Ljava/util/concurrent/ExecutorService;

.field mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;


# direct methods
.method constructor <init>(Lcom/microsoft/playready/PlayReadyFactory;)V
    .locals 1
    .param p1, "prFactory"    # Lcom/microsoft/playready/PlayReadyFactory;

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/microsoft/playready/LicenseAcquirerBase;-><init>()V

    .line 135
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/ProactiveLicenseAcquirer;->mExecService:Ljava/util/concurrent/ExecutorService;

    .line 140
    iput-object p1, p0, Lcom/microsoft/playready/ProactiveLicenseAcquirer;->mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 141
    return-void
.end method


# virtual methods
.method public acquireLicense(Ljava/lang/String;)Lcom/microsoft/playready/ILicenseAcquisitionTask;
    .locals 5
    .param p1, "keyID"    # Ljava/lang/String;

    .prologue
    .line 173
    if-nez p1, :cond_0

    .line 175
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Invalid argument to acquireLicense, key id must not be null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 178
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/playready/ProactiveLicenseAcquirer;->getLicenseAcquisitionPlugin()Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    move-result-object v0

    .line 180
    .local v0, "lap":Lcom/microsoft/playready/ILicenseAcquisitionPlugin;
    if-nez v0, :cond_1

    .line 182
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "No ILicenseAquisionPlugin has been set for this LicenseAcquirer, please call setLicenseAcquisitionPlugin with a valid plugin before calling AcquireLicense"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 187
    :cond_1
    new-instance v2, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;

    .line 188
    iget-object v3, p0, Lcom/microsoft/playready/ProactiveLicenseAcquirer;->mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 187
    invoke-direct {v2, v3, v0, p1}, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;-><init>(Lcom/microsoft/playready/PlayReadyFactory;Lcom/microsoft/playready/ILicenseAcquisitionPlugin;Ljava/lang/String;)V

    .line 192
    .local v2, "worker":Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;
    new-instance v1, Lcom/microsoft/playready/LicenseAcquisitionTask;

    invoke-direct {v1, v2}, Lcom/microsoft/playready/LicenseAcquisitionTask;-><init>(Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;)V

    .line 193
    .local v1, "task":Lcom/microsoft/playready/LicenseAcquisitionTask;
    invoke-virtual {v1, p0}, Lcom/microsoft/playready/LicenseAcquisitionTask;->addLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V

    .line 194
    iget-object v3, p0, Lcom/microsoft/playready/ProactiveLicenseAcquirer;->mExecService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 196
    return-object v1
.end method

.method public deleteLicense(Ljava/lang/String;)V
    .locals 3
    .param p1, "base64EncodedKeyId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 159
    if-nez p1, :cond_0

    .line 161
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Invalid argument to deleteLicense, key id must not be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 164
    :cond_0
    new-instance v0, Lcom/microsoft/playready/DrmProxy;

    iget-object v1, p0, Lcom/microsoft/playready/ProactiveLicenseAcquirer;->mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;

    invoke-direct {v0, v1}, Lcom/microsoft/playready/DrmProxy;-><init>(Lcom/microsoft/playready/PlayReadyFactory;)V

    .line 166
    .local v0, "drmProxy":Lcom/microsoft/playready/DrmProxy;
    invoke-virtual {v0, p1}, Lcom/microsoft/playready/DrmProxy;->deleteLicense(Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 149
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/ProactiveLicenseAcquirer;->mExecService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 155
    return-void

    .line 152
    :catchall_0
    move-exception v0

    .line 153
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 154
    throw v0
.end method
