.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$NowPlaying;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NowPlaying"
.end annotation


# static fields
.field public static final Companion:Ljava/lang/String; = "Now Playing Companion"

.field public static final FastForward:Ljava/lang/String; = "Transport Control - FF"

.field public static final Pause:Ljava/lang/String; = "Transport Control - Pause"

.field public static final Play:Ljava/lang/String; = "Transport Control - Play"

.field public static final RecordThat:Ljava/lang/String; = "Record That"

.field public static final Rewind:Ljava/lang/String; = "Transport Control - Rewind"

.field public static final Seek:Ljava/lang/String; = "Transport Control - Seek"

.field public static final SkipBackward:Ljava/lang/String; = "Transport Control - SkipBack"

.field public static final SkipForward:Ljava/lang/String; = "Transport Control - SkipForward"

.field public static final ViewDetails:Ljava/lang/String; = "View Details"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
