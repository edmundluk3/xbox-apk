.class public abstract Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;
.super Ljava/lang/Object;
.source "PartyInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/party/PartyInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "KickMemberAction"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(J)Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;
    .locals 2
    .param p0, "xuid"    # J

    .prologue
    .line 474
    new-instance v0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_KickMemberAction;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_KickMemberAction;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public toLogString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KickMemberAction: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;->xuid()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract xuid()J
.end method
