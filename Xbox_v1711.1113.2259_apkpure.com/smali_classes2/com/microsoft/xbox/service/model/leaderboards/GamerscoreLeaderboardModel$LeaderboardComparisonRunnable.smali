.class Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "GamerscoreLeaderboardModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LeaderboardComparisonRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Landroid/util/Pair",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final meXuid:Ljava/lang/String;

.field private final youXuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "meXuid"    # Ljava/lang/String;
    .param p2, "youXuid"    # Ljava/lang/String;

    .prologue
    .line 371
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 372
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;->meXuid:Ljava/lang/String;

    .line 373
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;->youXuid:Ljava/lang/String;

    .line 374
    return-void
.end method


# virtual methods
.method public buildData()Landroid/util/Pair;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 382
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getCurrentLeaderboardMonth()Ljava/util/Calendar;

    move-result-object v5

    .line 387
    .local v5, "thisMonth":Ljava/util/Calendar;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$500()Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;->meXuid:Ljava/lang/String;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;->meXuid:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;->youXuid:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;->createGamerscoreLeaderboard(Ljava/lang/String;Ljava/util/Calendar;Ljava/util/Collection;)Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;

    move-result-object v2

    .line 388
    .local v2, "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    if-eqz v2, :cond_5

    iget-object v3, v2, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;->instanceId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 389
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$500()Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;->meXuid:Ljava/lang/String;

    iget-object v6, v2, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;->instanceId:Ljava/lang/String;

    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-wide/16 v8, 0x2

    invoke-interface/range {v3 .. v9}, Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;->getGamerscoreLeaderboard(Ljava/lang/String;Ljava/util/Calendar;Ljava/lang/String;Ljava/lang/Long;J)Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;

    move-result-object v13

    .line 390
    .local v13, "response":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;
    if-eqz v13, :cond_4

    .line 391
    const-wide/16 v14, -0x1

    .local v14, "meGamerscore":J
    const-wide/16 v16, -0x1

    .line 392
    .local v16, "youGamerscore":J
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->getLeaderboard()Ljava/util/List;

    move-result-object v12

    .line 393
    .local v12, "leaderboard":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;

    .line 394
    .local v10, "entry":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;
    if-eqz v10, :cond_0

    .line 395
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;->meXuid:Ljava/lang/String;

    iget-object v6, v10, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 396
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->getGamerscore()J

    move-result-wide v14

    goto :goto_0

    .line 397
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;->youXuid:Ljava/lang/String;

    iget-object v6, v10, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 398
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->getGamerscore()J

    move-result-wide v16

    goto :goto_0

    .line 403
    .end local v10    # "entry":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v3, v14, v6

    if-lez v3, :cond_3

    const-wide/16 v6, -0x1

    cmp-long v3, v16, v6

    if-lez v3, :cond_3

    .line 404
    new-instance v3, Landroid/util/Pair;

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v3, v4, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 418
    .end local v2    # "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    .end local v12    # "leaderboard":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    .end local v13    # "response":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;
    .end local v14    # "meGamerscore":J
    .end local v16    # "youGamerscore":J
    :goto_1
    return-object v3

    .line 406
    .restart local v2    # "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    .restart local v12    # "leaderboard":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    .restart local v13    # "response":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;
    .restart local v14    # "meGamerscore":J
    .restart local v16    # "youGamerscore":J
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$700()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failed to parse comparison leaderboard response"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    .end local v2    # "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    .end local v12    # "leaderboard":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    .end local v13    # "response":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;
    .end local v14    # "meGamerscore":J
    .end local v16    # "youGamerscore":J
    :goto_2
    const/4 v3, 0x0

    goto :goto_1

    .line 409
    .restart local v2    # "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    .restart local v13    # "response":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$700()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failed to get comparison leaderboard data"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 414
    .end local v2    # "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    .end local v13    # "response":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;
    :catch_0
    move-exception v11

    .line 415
    .local v11, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$700()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failed to get gamerscore comparison"

    invoke-static {v3, v4, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 412
    .end local v11    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    .restart local v2    # "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    :cond_5
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$700()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failed to create comparison leaderboard"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;->buildData()Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 427
    const-wide/16 v0, 0x2134

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 423
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 378
    return-void
.end method
