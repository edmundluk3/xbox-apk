.class public Lcom/microsoft/xbox/xle/epg/TvStreamerModel;
.super Ljava/lang/Object;
.source "TvStreamerModel.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;,
        Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;,
        Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    }
.end annotation


# static fields
.field private static final BUFFER_TIMEOUT:I = 0xea60

.field private static final TAG:Ljava/lang/String; = "TvStreamerModel"

.field private static instance:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

.field private static sCanLoadStreamingLibrary:Z


# instance fields
.field private loadTimeoutHandler:Landroid/os/Handler;

.field private final loadTimeoutRunnable:Ljava/lang/Runnable;

.field private mBufferTimeout:I

.field private mCanStreamHdmi:Z

.field private mCanStreamTuner:Z

.field private mCurrentChannel:Ljava/lang/String;

.field private mCurrentSource:Ljava/lang/String;

.field private mDeferredChannelChange:Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;

.field private mIpAddress:Ljava/lang/String;

.field private mIsCurrentShowBlocked:Z

.field private mIsLoading:Z

.field private mIsPausedByUser:Z

.field private mIsPreparing:Z

.field private mIsRadioChannel:Z

.field private final mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

.field private mQuality:Ljava/lang/String;

.field private mStartTime:J

.field private final mStreamerStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mStreamingPort:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    sput-boolean v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->sCanLoadStreamingLibrary:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/xle/epg/TvStreamerException;
        }
    .end annotation

    .prologue
    const v6, 0x7f070455

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-string v4, "medium"

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mQuality:Ljava/lang/String;

    .line 58
    const v4, 0xea60

    iput v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mBufferTimeout:I

    .line 59
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStartTime:J

    .line 70
    new-instance v4, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$1;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->loadTimeoutRunnable:Ljava/lang/Runnable;

    .line 97
    new-instance v4, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 98
    new-instance v4, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStreamerStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 135
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v3

    .line 136
    .local v3, "session":Lcom/microsoft/xbox/service/model/SessionModel;
    if-nez v3, :cond_0

    .line 137
    const-string v4, "TvStreamerModel"

    const-string v5, "SessionModel is null creating TvStreamerModel"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    new-instance v4, Lcom/microsoft/xbox/xle/epg/TvStreamerException;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/xle/epg/TvStreamerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 141
    :cond_0
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v2

    .line 142
    .local v2, "console":Lcom/microsoft/xbox/xle/model/ConsoleData;
    if-nez v2, :cond_1

    .line 143
    const-string v4, "TvStreamerModel"

    const-string v5, "ConsoleData is null creating TvStreamerModel"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    new-instance v4, Lcom/microsoft/xbox/xle/epg/TvStreamerException;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/xle/epg/TvStreamerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 147
    :cond_1
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddress()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIpAddress:Ljava/lang/String;

    .line 148
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIpAddress:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 149
    const-string v4, "TvStreamerModel"

    const-string v5, "Could not get local IP address while creating TvStreamerModel"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    new-instance v4, Lcom/microsoft/xbox/xle/epg/TvStreamerException;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/xle/epg/TvStreamerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 153
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 154
    .local v0, "branchSession":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_3

    .line 155
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 156
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;)V

    .line 157
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;)V

    .line 159
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getHeadend()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V

    .line 164
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v1

    .line 165
    .local v1, "companionSession":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;
    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addCompanionSessionStateListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;)V

    .line 167
    new-instance v4, Ljava/lang/Object;

    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mLock:Ljava/lang/Object;

    .line 169
    const-string v4, "TvStreamerModel"

    const-string v5, "Instance created"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    return-void

    .line 161
    .end local v1    # "companionSession":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;
    :cond_3
    const-string v4, "TvStreamerModel"

    const-string v5, "Branch session has not been initialized yet"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsLoading:Z

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Lcom/microsoft/playready/PRMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPausedByUser:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsCurrentShowBlocked:Z

    return v0
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->closeStreamHelper()V

    return-void
.end method

.method private closeStreamHelper()V
    .locals 1

    .prologue
    .line 319
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 321
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onCloseStream()V

    .line 322
    invoke-static {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 328
    return-void
.end method

.method public static destroy(Z)V
    .locals 1
    .param p0, "userExited"    # Z

    .prologue
    .line 275
    sget-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->instance:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    if-eqz v0, :cond_0

    .line 276
    sget-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->instance:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onDestroy(Z)V

    .line 278
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamerModel;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/xle/epg/TvStreamerException;
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->instance:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->instance:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    .line 126
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->instance:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    return-object v0
.end method

.method private handleChannelTypeChanged(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V
    .locals 6
    .param p1, "channelType"    # Ljava/lang/String;
    .param p2, "blockExplicitContentPerShowInfo"    # Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    .prologue
    const/4 v3, 0x0

    .line 437
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-nez v2, :cond_1

    .line 495
    :cond_0
    return-void

    .line 441
    :cond_1
    if-eqz p2, :cond_4

    iget-boolean v2, p2, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->blockExplicitContentPerShow:Z

    if-eqz v2, :cond_4

    const-string v2, "passed"

    iget-object v4, p2, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->userCanViewShow:Ljava/lang/String;

    .line 442
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsCurrentShowBlocked:Z

    .line 444
    if-eqz p2, :cond_5

    iget-boolean v2, p2, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->blockExplicitContentPerShow:Z

    if-eqz v2, :cond_5

    const-string v2, "failed"

    iget-object v4, p2, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->userCanViewShow:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 445
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 447
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->reset()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 452
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 453
    .local v1, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f070c7d

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_2

    .end local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_4
    move v2, v3

    .line 442
    goto :goto_0

    .line 448
    :catch_0
    move-exception v0

    .line 449
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v2, "TvStreamerModel"

    const-string v4, "Failed to reset media player"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 455
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_5
    if-eqz p2, :cond_8

    iget-boolean v2, p2, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->blockExplicitContentPerShow:Z

    if-eqz v2, :cond_8

    const-string/jumbo v2, "unknown"

    iget-object v4, p2, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->userCanViewShow:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 456
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 458
    :cond_6
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->reset()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 463
    :cond_7
    :goto_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 464
    .restart local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f070c85

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_4

    .line 459
    .end local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :catch_1
    move-exception v0

    .line 460
    .restart local v0    # "e":Ljava/lang/IllegalStateException;
    const-string v2, "TvStreamerModel"

    const-string v4, "Failed to reset media player"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 466
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_8
    const-string v2, "dataChannel"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 467
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 469
    :cond_9
    :try_start_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->reset()V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    .line 474
    :cond_a
    :goto_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 475
    .restart local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f070c71

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_6

    .line 470
    .end local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :catch_2
    move-exception v0

    .line 471
    .restart local v0    # "e":Ljava/lang/IllegalStateException;
    const-string v2, "TvStreamerModel"

    const-string v4, "Failed to reset media player"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 477
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_b
    const-string/jumbo v2, "televisionChannelNoContent"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 478
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 480
    :cond_c
    :try_start_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->reset()V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3

    .line 485
    :cond_d
    :goto_7
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 486
    .restart local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f070c79

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_8

    .line 481
    .end local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :catch_3
    move-exception v0

    .line 482
    .restart local v0    # "e":Ljava/lang/IllegalStateException;
    const-string v2, "TvStreamerModel"

    const-string v4, "Failed to reset media player"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 490
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_e
    const-string v2, "radioChannel"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsRadioChannel:Z

    .line 492
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 493
    .restart local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-interface {v1, p1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onChannelTypeChanged(Ljava/lang/String;)V

    goto :goto_9
.end method

.method static synthetic lambda$closeStreamHelper$0(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    .prologue
    .line 323
    const-string v1, "TvStreamerModel"

    const-string v2, "Stream closed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 325
    .local v0, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onTvStreamClosed()V

    goto :goto_0

    .line 327
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_0
    return-void
.end method

.method static synthetic lambda$setupMediaPlayer$1(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    .prologue
    .line 421
    :try_start_0
    const-string v1, "TvStreamerModel"

    const-string v2, "Preparing media player"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v1}, Lcom/microsoft/playready/PRMediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 427
    :goto_0
    return-void

    .line 423
    :catch_0
    move-exception v0

    .line 424
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    const-string v1, "TvStreamerModel"

    const-string v2, "Failed to prepare mediaplayer"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 425
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z

    goto :goto_0

    .line 423
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private onCloseStream()V
    .locals 3

    .prologue
    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-eqz v0, :cond_1

    .line 343
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v0}, Lcom/microsoft/playready/PRMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v0}, Lcom/microsoft/playready/PRMediaPlayer;->pauseForShutdown()V

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v0}, Lcom/microsoft/playready/PRMediaPlayer;->reset()V

    .line 351
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 352
    :try_start_0
    const-string v0, "TvStreamerModel"

    const-string v2, "Releasing media player"

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v0}, Lcom/microsoft/playready/PRMediaPlayer;->release()V

    .line 354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    .line 355
    monitor-exit v1

    .line 357
    :cond_1
    return-void

    .line 355
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 360
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getHeadend()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v1

    .line 361
    .local v1, "headend":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    .line 363
    .local v0, "currentConsole":Lcom/microsoft/xbox/xle/model/ConsoleData;
    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->streaming_port:Ljava/lang/String;

    :goto_0
    iput-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStreamingPort:Ljava/lang/String;

    .line 364
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddress()Ljava/lang/String;

    move-result-object v3

    :cond_0
    iput-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIpAddress:Ljava/lang/String;

    .line 365
    return-void

    :cond_1
    move-object v2, v3

    .line 363
    goto :goto_0
.end method

.method private resetLoadTimeout(Z)V
    .locals 4
    .param p1, "restartTimer"    # Z

    .prologue
    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->loadTimeoutHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->loadTimeoutHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->loadTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 246
    :cond_0
    if-eqz p1, :cond_2

    .line 247
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->loadTimeoutHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 248
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->loadTimeoutHandler:Landroid/os/Handler;

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->loadTimeoutHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->loadTimeoutRunnable:Ljava/lang/Runnable;

    iget v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mBufferTimeout:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 252
    :cond_2
    return-void
.end method

.method public static setCanLoadStreamingLibrary(Z)V
    .locals 0
    .param p0, "canLoad"    # Z

    .prologue
    .line 224
    sput-boolean p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->sCanLoadStreamingLibrary:Z

    .line 225
    return-void
.end method

.method private declared-synchronized setupMediaPlayer()V
    .locals 11

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 368
    monitor-enter p0

    :try_start_0
    const-string v9, "TvStreamerModel"

    const-string v10, "Preparing stream"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    iget-object v9, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-nez v9, :cond_0

    .line 370
    const-string v6, "TvStreamerModel"

    const-string/jumbo v7, "setupMediaPlayer called while media player object is null!"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 434
    :goto_0
    monitor-exit p0

    return-void

    .line 374
    :cond_0
    const/4 v9, 0x1

    :try_start_1
    iput-boolean v9, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376
    :try_start_2
    iget-object v9, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v9}, Lcom/microsoft/playready/PRMediaPlayer;->reset()V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 383
    :try_start_3
    iget-object v9, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/microsoft/playready/PRMediaPlayer;->setScreenOnWhilePlaying(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 386
    const/4 v5, 0x0

    .line 388
    .local v5, "xuid":Ljava/lang/String;
    :try_start_4
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    .line 389
    .local v3, "profile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v3, :cond_1

    .line 390
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v5

    .line 393
    :cond_1
    iget-object v9, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentSource:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_2
    move v6, v7

    :goto_1
    packed-switch v6, :pswitch_data_0

    .line 401
    const-string v6, "TvStreamerModel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Encountered unexpected source: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentSource:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ". Defaulting to tuner."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const-string/jumbo v4, "zurich"

    .line 407
    .local v4, "source":Ljava/lang/String;
    :goto_2
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 409
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "http://%s:%s/%s?xuid=%s&quality=%s"

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIpAddress:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStreamingPort:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    aput-object v4, v8, v9

    const/4 v9, 0x3

    aput-object v5, v8, v9

    const/4 v9, 0x4

    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mQuality:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 414
    .local v0, "dataSource":Ljava/lang/String;
    :goto_3
    const-string v6, "TvStreamerModel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Streaming data source: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    iget-object v6, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v6, v0}, Lcom/microsoft/playready/PRMediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 417
    iget-object v6, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/microsoft/playready/PRMediaPlayer;->setAudioStreamType(I)V

    .line 418
    iget-object v6, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/microsoft/playready/PRMediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 419
    new-instance v2, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$3;

    sget-object v6, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-direct {v2, p0, v6, v7}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$3;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 429
    .local v2, "preparePlayerTask":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 430
    .end local v0    # "dataSource":Ljava/lang/String;
    .end local v2    # "preparePlayerTask":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    .end local v3    # "profile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v4    # "source":Ljava/lang/String;
    :catch_0
    move-exception v6

    move-object v1, v6

    .line 431
    .local v1, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_5
    const-string v6, "TvStreamerModel"

    const-string v7, "Failed to set data source"

    invoke-static {v6, v7, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 432
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 368
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v5    # "xuid":Ljava/lang/String;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 377
    :catch_1
    move-exception v1

    .line 378
    .local v1, "e":Ljava/lang/IllegalStateException;
    :try_start_6
    const-string v6, "TvStreamerModel"

    const-string v7, "Failed to reset stream"

    invoke-static {v6, v7, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 379
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z

    .line 380
    iget-object v6, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    const/4 v7, -0x1

    const/4 v8, -0x1

    invoke-virtual {p0, v6, v7, v8}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onError(Landroid/media/MediaPlayer;II)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 393
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    .restart local v3    # "profile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .restart local v5    # "xuid":Ljava/lang/String;
    :sswitch_0
    :try_start_7
    const-string v8, "hdmi"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    goto/16 :goto_1

    :sswitch_1
    const-string/jumbo v6, "tuner"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v6, v8

    goto/16 :goto_1

    .line 395
    :pswitch_0
    const-string v4, "hdmi-in"

    .line 396
    .restart local v4    # "source":Ljava/lang/String;
    goto/16 :goto_2

    .line 398
    .end local v4    # "source":Ljava/lang/String;
    :pswitch_1
    const-string/jumbo v4, "zurich"

    .line 399
    .restart local v4    # "source":Ljava/lang/String;
    goto/16 :goto_2

    .line 411
    :cond_3
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "http://%s:%s/%s?quality=%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIpAddress:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStreamingPort:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    aput-object v4, v8, v9

    const/4 v9, 0x3

    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mQuality:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v0

    .restart local v0    # "dataSource":Ljava/lang/String;
    goto/16 :goto_3

    .line 430
    .end local v0    # "dataSource":Ljava/lang/String;
    .end local v3    # "profile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v4    # "source":Ljava/lang/String;
    :catch_2
    move-exception v6

    move-object v1, v6

    goto :goto_4

    :catch_3
    move-exception v6

    move-object v1, v6

    goto :goto_4

    .line 393
    :sswitch_data_0
    .sparse-switch
        0x30cb98 -> :sswitch_0
        0x6997f7a -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .prologue
    .line 259
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 260
    return-void
.end method

.method public addStreamerStateListener(Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStreamerStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 268
    return-void
.end method

.method public canStreamHdmi()Z
    .locals 1

    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCanStreamHdmi:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->sCanLoadStreamingLibrary:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canStreamTuner()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCanStreamTuner:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->sCanLoadStreamingLibrary:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public closeStream(Z)V
    .locals 8
    .param p1, "userExited"    # Z

    .prologue
    .line 291
    const-string v2, "TvStreamerModel"

    const-string v3, "Closing stream"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 294
    .local v1, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onTvStreamClosing()V

    goto :goto_0

    .line 297
    .end local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_0
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStartTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 298
    if-eqz p1, :cond_2

    .line 299
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "Stream Close"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStartTime:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_1
    :goto_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z

    .line 305
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStartTime:J

    .line 306
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mDeferredChannelChange:Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;

    .line 309
    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v2, v3, :cond_3

    .line 310
    new-instance v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$2;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$2;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 312
    .local v0, "closeStreamTask":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    .line 316
    .end local v0    # "closeStreamTask":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    :goto_2
    return-void

    .line 301
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "Stream ForceClose"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStartTime:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 314
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->closeStreamHelper()V

    goto :goto_2
.end method

.method public finishPreparing()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 199
    const-string v2, "TvStreamerModel"

    const-string v3, "Finished preparing stream"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z

    .line 203
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mDeferredChannelChange:Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;

    if-eqz v2, :cond_0

    .line 204
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mDeferredChannelChange:Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;->activate()V

    .line 205
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mDeferredChannelChange:Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;

    .line 207
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->resetLoadTimeout(Z)V

    .line 212
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public getCurrentSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentSource:Ljava/lang/String;

    return-object v0
.end method

.method public getIsLoading()Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsLoading:Z

    return v0
.end method

.method public getIsRadioChannel()Z
    .locals 1

    .prologue
    .line 255
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsRadioChannel:Z

    return v0
.end method

.method public getMediaPlayer()Lcom/microsoft/playready/PRMediaPlayer;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    return-object v0
.end method

.method public getMediaPlayerLock()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method public getQuality()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mQuality:Ljava/lang/String;

    return-object v0
.end method

.method public onChannelChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "channelId"    # Ljava/lang/String;
    .param p3, "userCanViewChannel"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 650
    const-string v2, "TvStreamerModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onChannelChanged (source: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", channelId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", userCanViewChannel: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCanStreamHdmi:Z

    if-nez v2, :cond_0

    const-string v2, "hdmi"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 706
    :goto_0
    return-void

    .line 657
    :cond_0
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z

    if-eqz v2, :cond_1

    .line 658
    const-string v2, "TvStreamerModel"

    const-string v3, "Deferring channel change"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    new-instance v2, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mDeferredChannelChange:Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;

    goto :goto_0

    .line 664
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-eqz v2, :cond_6

    .line 665
    const/4 v2, -0x1

    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 687
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentSource:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentSource:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentChannel:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentChannel:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 691
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 692
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->pauseForShutdown()V

    .line 696
    :cond_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 697
    .local v1, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onChannelChanged()V

    goto :goto_2

    .line 665
    .end local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :sswitch_0
    const-string v4, "failed"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v3

    goto :goto_1

    :sswitch_1
    const-string/jumbo v4, "unknown"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    .line 668
    :pswitch_0
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->reset()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 672
    :goto_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 673
    .restart local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f070c7d

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_4

    .line 669
    .end local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :catch_0
    move-exception v0

    .line 670
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v2, "TvStreamerModel"

    const-string v4, "Failed to reset media player"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 678
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :pswitch_1
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->reset()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 682
    :goto_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 683
    .restart local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f070c85

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_6

    .line 679
    .end local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :catch_1
    move-exception v0

    .line 680
    .restart local v0    # "e":Ljava/lang/IllegalStateException;
    const-string v2, "TvStreamerModel"

    const-string v4, "Failed to reset media player"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 703
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_6
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPausedByUser:Z

    .line 704
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentSource:Ljava/lang/String;

    .line 705
    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentChannel:Ljava/lang/String;

    goto/16 :goto_0

    .line 665
    :sswitch_data_0
    .sparse-switch
        -0x4c696bc3 -> :sswitch_0
        -0x10fa53b6 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onChannelTypeChanged(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V
    .locals 3
    .param p1, "channelType"    # Ljava/lang/String;
    .param p2, "blockExplicitContentPerShowInfo"    # Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    .prologue
    .line 710
    const-string v0, "TvStreamerModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChannelTypeChanged : channelType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->handleChannelTypeChanged(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V

    .line 712
    return-void
.end method

.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 0
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 590
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 4
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    .line 575
    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v1}, Lcom/microsoft/playready/PRMediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 576
    const-string v1, "TvStreamerModel"

    const-string v2, "Console disconnected"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-eqz v1, :cond_0

    .line 579
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->closeStream(Z)V

    .line 582
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 583
    .local v0, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f070c5f

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_0

    .line 586
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_1
    return-void
.end method

.method public onDestroy(Z)V
    .locals 1
    .param p1, "userExited"    # Z

    .prologue
    .line 281
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->closeStream(Z)V

    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStreamerStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 284
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 285
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;)V

    .line 286
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;)V

    .line 287
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->instance:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    .line 288
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 6
    .param p1, "arg0"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v5, 0x0

    .line 528
    const-string v2, "TvStreamerModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to play stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-eqz v2, :cond_0

    .line 531
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->reset()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 536
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 537
    .local v1, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f070c66

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_1

    .line 532
    .end local v1    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :catch_0
    move-exception v0

    .line 533
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v2, "TvStreamerModel"

    const-string v3, "Failed to reset media player"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 539
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_1
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->closeStream(Z)V

    .line 540
    return v5
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 6
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    const/4 v1, 0x0

    .line 594
    const-string v2, "TvStreamerModel"

    const-string v3, "Headend changed"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2}, Lcom/microsoft/playready/PRMediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 597
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->closeStream(Z)V

    .line 599
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->reset()V

    .line 602
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCanStreamTuner:Z

    .line 603
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCanStreamHdmi:Z

    .line 605
    if-nez p1, :cond_2

    .line 616
    :cond_1
    return-void

    .line 609
    :cond_2
    iget-object v2, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 610
    .local v0, "provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    iget-object v4, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_source:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v5, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v4, v5, :cond_4

    .line 611
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCanStreamTuner:Z

    iget-boolean v5, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->can_stream:Z

    or-int/2addr v4, v5

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCanStreamTuner:Z

    .line 609
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 612
    :cond_4
    iget-object v4, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_source:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v5, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v4, v5, :cond_3

    .line 613
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCanStreamHdmi:Z

    iget-boolean v5, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->can_stream:Z

    or-int/2addr v4, v5

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCanStreamHdmi:Z

    goto :goto_1
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 621
    return-void
.end method

.method public onLiveTvInfoReceived(Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;

    .prologue
    .line 721
    const-string v1, "TvStreamerModel"

    const-string v2, "onLiveTvInfoReceived"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    if-eqz p1, :cond_0

    .line 723
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->currentTunerChannelId:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentChannel:Ljava/lang/String;

    .line 724
    iget-boolean v1, p1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->isInHdmiMode:Z

    if-eqz v1, :cond_1

    const-string v1, "hdmi"

    :goto_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentSource:Ljava/lang/String;

    .line 725
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->tunerChannelType:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->handleChannelTypeChanged(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V

    .line 727
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 728
    .local v0, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onLiveTvInfoReceived(Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;)V

    goto :goto_1

    .line 724
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_1
    const-string/jumbo v1, "tuner"

    goto :goto_0

    .line 730
    :cond_2
    return-void
.end method

.method public onSessionStateChanged(ILcom/microsoft/xbox/toolkit/XLEException;)V
    .locals 4
    .param p1, "newSessionState"    # I
    .param p2, "exception"    # Lcom/microsoft/xbox/toolkit/XLEException;

    .prologue
    .line 625
    if-nez p1, :cond_1

    .line 626
    const-string v1, "TvStreamerModel"

    const-string v2, "Session disconnected"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 629
    .local v0, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f070c65

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_0

    .line 631
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-eqz v1, :cond_1

    .line 632
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->closeStream(Z)V

    .line 635
    :cond_1
    return-void
.end method

.method public onStreamError(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorType"    # Ljava/lang/String;

    .prologue
    .line 717
    return-void
.end method

.method public onStreamFormatChanged()V
    .locals 3

    .prologue
    .line 639
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z

    if-nez v1, :cond_0

    .line 640
    const-string v1, "TvStreamerModel"

    const-string v2, "onStreamFormatChanged"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setupMediaPlayer()V

    .line 642
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 643
    .local v0, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamFormatChanged()V

    goto :goto_0

    .line 646
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_0
    return-void
.end method

.method public onStreamStarted(Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;)V
    .locals 7
    .param p1, "info"    # Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;

    .prologue
    const v6, 0x7f070c85

    const v5, 0x7f070c7d

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 734
    if-nez p1, :cond_1

    .line 789
    :cond_0
    return-void

    .line 737
    :cond_1
    const-string v1, "TvStreamerModel"

    const-string v4, "onStreamStarted"

    invoke-static {v1, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->source:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentSource:Ljava/lang/String;

    .line 740
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->currentChannelId:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mCurrentChannel:Ljava/lang/String;

    .line 741
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->streamingPort:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStreamingPort:Ljava/lang/String;

    .line 742
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->blockExplicitContentPerShow:Z

    if-eqz v1, :cond_2

    const-string v1, "passed"

    iget-object v4, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->userCanViewShow:Ljava/lang/String;

    .line 743
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsCurrentShowBlocked:Z

    .line 745
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStreamingPort:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 746
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 747
    .local v0, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v4, 0x7f07048d

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_1

    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_2
    move v1, v3

    .line 743
    goto :goto_0

    .line 749
    :cond_3
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->userCanViewChannel:Ljava/lang/String;

    const-string v4, "failed"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 750
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 751
    .restart local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_2

    .line 753
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_4
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    if-eqz v1, :cond_5

    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->blockExplicitContentPerShow:Z

    if-eqz v1, :cond_5

    const-string v1, "failed"

    iget-object v4, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->userCanViewShow:Ljava/lang/String;

    .line 754
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 755
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 756
    .restart local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_3

    .line 758
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_5
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->userCanViewChannel:Ljava/lang/String;

    const-string/jumbo v4, "unknown"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 759
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 760
    .restart local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_4

    .line 762
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_6
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    if-eqz v1, :cond_7

    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->blockExplicitContentPerShow:Z

    if-eqz v1, :cond_7

    const-string/jumbo v1, "unknown"

    iget-object v4, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->userCanViewShow:Ljava/lang/String;

    .line 763
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 764
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 765
    .restart local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_5

    .line 767
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_7
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->currentChannelType:Ljava/lang/String;

    const-string v4, "dataChannel"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 768
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 769
    .restart local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v4, 0x7f070c71

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_6

    .line 771
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_8
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->currentChannelType:Ljava/lang/String;

    const-string/jumbo v4, "televisionChannelNoContent"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 772
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 773
    .restart local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v4, 0x7f070c79

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_7

    .line 776
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStartTime:J

    .line 777
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPreparing:Z

    if-nez v1, :cond_a

    .line 778
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setupMediaPlayer()V

    .line 780
    :cond_a
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setIsLoading(Z)V

    .line 783
    :cond_b
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->currentChannelType:Ljava/lang/String;

    const-string v2, "radioChannel"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsRadioChannel:Z

    .line 786
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 787
    .restart local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    iget-object v2, p1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->currentChannelType:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onChannelTypeChanged(Ljava/lang/String;)V

    goto :goto_8
.end method

.method protected onStreamerStateChanged(Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;)V
    .locals 4
    .param p1, "state"    # Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    .prologue
    .line 518
    const-string v1, "TvStreamerModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Streamer state changed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setStreamerState(Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;)V

    .line 521
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStreamerStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;

    .line 522
    .local v0, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;->onStreamerStateChanged(Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;)V

    goto :goto_0

    .line 524
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;
    :cond_0
    return-void
.end method

.method public onTunerStateChange(Ljava/lang/String;)V
    .locals 5
    .param p1, "playbackState"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 793
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-nez v0, :cond_1

    .line 814
    :cond_0
    :goto_0
    return-void

    .line 797
    :cond_1
    const-string v0, "TvStreamerModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Tuner state changed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 811
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected tuner state change: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 799
    :sswitch_0
    const-string v3, "paused"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string v3, "playing"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v2

    goto :goto_1

    .line 801
    :pswitch_0
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPausedByUser:Z

    .line 802
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->playbackPause()V

    goto :goto_0

    .line 805
    :pswitch_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPausedByUser:Z

    if-eqz v0, :cond_0

    .line 806
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsPausedByUser:Z

    .line 807
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->playbackPlay()V

    goto :goto_0

    .line 799
    nop

    :sswitch_data_0
    .sparse-switch
        -0x3b5366d2 -> :sswitch_0
        -0x1d6b2fd2 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public playbackPause()V
    .locals 3

    .prologue
    .line 498
    const-string v1, "TvStreamerModel"

    const-string v2, "Pausing playback"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v1}, Lcom/microsoft/playready/PRMediaPlayer;->pause()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 505
    :goto_0
    return-void

    .line 502
    :catch_0
    move-exception v0

    .line 503
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "TvStreamerModel"

    const-string v2, "Could not pause stream"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public playbackPlay()V
    .locals 3

    .prologue
    .line 508
    const-string v1, "TvStreamerModel"

    const-string v2, "Resuming playback"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v1}, Lcom/microsoft/playready/PRMediaPlayer;->unpause()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 515
    :goto_0
    return-void

    .line 512
    :catch_0
    move-exception v0

    .line 513
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "TvStreamerModel"

    const-string v2, "Could not pause stream"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .prologue
    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeStreamerStateListener(Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;

    .prologue
    .line 271
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mStreamerStateListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setIsLoading(Z)V
    .locals 4
    .param p1, "loading"    # Z

    .prologue
    .line 228
    const-string v1, "TvStreamerModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setIsLoading "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsLoading:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "->"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsLoading:Z

    if-ne v1, p1, :cond_1

    .line 239
    :cond_0
    return-void

    .line 232
    :cond_1
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsLoading:Z

    .line 234
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->resetLoadTimeout(Z)V

    .line 236
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 237
    .local v0, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mIsLoading:Z

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onLoadingChanged(Z)V

    goto :goto_0
.end method

.method public setMediaPlayer(Lcom/microsoft/playready/PRMediaPlayer;)V
    .locals 1
    .param p1, "mediaPlayer"    # Lcom/microsoft/playready/PRMediaPlayer;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v0, p0}, Lcom/microsoft/playready/PRMediaPlayer;->addOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 179
    return-void
.end method

.method public setQuality(Ljava/lang/String;)V
    .locals 0
    .param p1, "quality"    # Ljava/lang/String;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mQuality:Ljava/lang/String;

    .line 332
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 563
    const-string v0, "TvStreamerModel"

    const-string v1, "Video surface changed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 5
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 545
    const-string v1, "TvStreamerModel"

    const-string v2, "Video surface created"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 547
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 549
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v1, p1}, Lcom/microsoft/playready/PRMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 558
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v2

    .line 559
    return-void

    .line 550
    :catch_0
    move-exception v0

    .line 551
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "TvStreamerModel"

    const-string v3, "IllegalStateException during setDisplay"

    invoke-static {v1, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 552
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-virtual {p0, v1, v3, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onError(Landroid/media/MediaPlayer;II)Z

    goto :goto_0

    .line 558
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 553
    :catch_1
    move-exception v0

    .line 554
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    const-string v1, "TvStreamerModel"

    const-string v3, "IllegalArgumentException during setDisplay"

    invoke-static {v1, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 555
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-virtual {p0, v1, v3, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onError(Landroid/media/MediaPlayer;II)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 569
    const-string v0, "TvStreamerModel"

    const-string v1, "Video surface destroyed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    return-void
.end method
