.class public Lcom/microsoft/xbox/service/model/StoreSearchModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "StoreSearchModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_SEARCH_ENCODED_TEXT_LENGTH:I = 0xfa

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private currentSearchTag:Ljava/lang/String;

.field private hasMoreToLoad:Z

.field private pageCount:I

.field private searchResult:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

.field private storeSearchResultRunner:Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/model/StoreSearchModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/StoreSearchModel;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->currentSearchTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/service/model/StoreSearchModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/StoreSearchModel;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->hasMoreToLoad:Z

    return p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/StoreSearchModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/StoreSearchModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->onGetSearchDataFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private onGetSearchDataFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;>;"
    sget-object v2, Lcom/microsoft/xbox/service/model/StoreSearchModel;->TAG:Ljava/lang/String;

    const-string v3, "onGetSearchDataFeedCompleted"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 80
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 81
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 83
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->SearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 85
    .local v1, "updateType":Lcom/microsoft/xbox/service/model/UpdateType;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 86
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .line 88
    .local v0, "searchResultData":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    iget v2, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->pageCount:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->pageCount:I

    if-lez v2, :cond_0

    .line 89
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->MoreSearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 92
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->searchResult:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-nez v2, :cond_2

    .line 93
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->searchResult:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .line 100
    .end local v0    # "searchResultData":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_1
    :goto_0
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    const/4 v4, 0x1

    invoke-direct {v3, v1, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v4

    invoke-direct {v2, v3, p0, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 101
    return-void

    .line 95
    .restart local v0    # "searchResultData":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->searchResult:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 96
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->searchResult:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->searchResult:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->setTotalCount(I)V

    goto :goto_0
.end method


# virtual methods
.method public doSearch(ZLjava/lang/String;)V
    .locals 5
    .param p1, "forceRefresh"    # Z
    .param p2, "searchTag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 43
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 44
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 46
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->currentSearchTag:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->SearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v0, v1, p0, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 62
    :goto_0
    return-void

    .line 51
    :cond_0
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->currentSearchTag:Ljava/lang/String;

    .line 52
    iput-object v4, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->searchResult:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .line 53
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->hasMoreToLoad:Z

    .line 54
    iput v1, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->pageCount:I

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->storeSearchResultRunner:Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->storeSearchResultRunner:Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->cancel()V

    .line 60
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->currentSearchTag:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->pageCount:I

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;-><init>(Lcom/microsoft/xbox/service/model/StoreSearchModel;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->storeSearchResultRunner:Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;

    .line 61
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->SearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->storeSearchResultRunner:Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;

    invoke-virtual {p0, v3, v0, v1}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    goto :goto_0
.end method

.method public getHasMoreToLoad()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->hasMoreToLoad:Z

    return v0
.end method

.method public getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->searchResult:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    return-object v0
.end method

.method public loadMoreSearchResults()V
    .locals 3

    .prologue
    .line 65
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->searchResult:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 67
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->hasMoreToLoad:Z

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->storeSearchResultRunner:Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->storeSearchResultRunner:Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->cancel()V

    .line 73
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->currentSearchTag:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->pageCount:I

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;-><init>(Lcom/microsoft/xbox/service/model/StoreSearchModel;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->storeSearchResultRunner:Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;

    .line 74
    const/4 v0, 0x1

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->MoreSearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel;->storeSearchResultRunner:Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;

    invoke-virtual {p0, v0, v1, v2}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 75
    return-void
.end method
