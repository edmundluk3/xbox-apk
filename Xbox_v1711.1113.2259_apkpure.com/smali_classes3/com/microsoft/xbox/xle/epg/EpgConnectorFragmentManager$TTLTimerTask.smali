.class Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;
.super Ljava/util/TimerTask;
.source "EpgConnectorFragmentManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TTLTimerTask"
.end annotation


# instance fields
.field private datagramId:I

.field private fragmentManagerWeak:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;Ljava/lang/ref/WeakReference;I)V
    .locals 0
    .param p3, "datagramId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p2, "fragmentManagerWeak":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;->this$0:Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 73
    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;->fragmentManagerWeak:Ljava/lang/ref/WeakReference;

    .line 74
    iput p3, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;->datagramId:I

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;->fragmentManagerWeak:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;

    .prologue
    .line 68
    iget v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;->datagramId:I

    return v0
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask$1;-><init>(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 89
    return-void
.end method
