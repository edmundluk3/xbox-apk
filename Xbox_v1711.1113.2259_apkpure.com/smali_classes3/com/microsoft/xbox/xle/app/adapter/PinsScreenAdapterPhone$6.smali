.class Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;
.super Landroid/widget/BaseAdapter;
.source "PinsScreenAdapterPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/microsoft/xbox/service/model/pins/PinItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/pins/PinItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;->getItem(I)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 215
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 220
    move-object v0, p2

    .line 222
    .local v0, "cell":Landroid/view/View;
    if-nez v0, :cond_0

    .line 223
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$400(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03013d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$200(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 226
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$500(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;->getItem(I)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;->bindPin(Landroid/view/View;Lcom/microsoft/xbox/service/model/pins/PinItem;I)V

    .line 227
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$200(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->onViewBound(Landroid/view/View;I)V

    .line 228
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->access$600(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method
