.class Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;
.super Ljava/lang/Object;
.source "ChatHeadManagerEventObservable.java"

# interfaces
.implements Lcom/flipkart/chatheads/ui/ChatHeadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;-><init>(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;Lcom/flipkart/chatheads/ui/ChatHeadManager;Lio/reactivex/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/flipkart/chatheads/ui/ChatHeadListener",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;

.field final synthetic val$observer:Lio/reactivex/Observer;

.field final synthetic val$this$0:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;Lio/reactivex/Observer;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;

    .prologue
    .line 49
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;"
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->this$1:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->val$this$0:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;

    iput-object p3, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->val$observer:Lio/reactivex/Observer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAllChatHeadsRemoved(Z)V
    .locals 2
    .param p1, "userTriggered"    # Z

    .prologue
    .line 66
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->this$1:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->val$observer:Lio/reactivex/Observer;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;->with(Z)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 69
    :cond_0
    return-void
.end method

.method public onChatHeadAdded(Ljava/io/Serializable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;"
    .local p1, "key":Ljava/io/Serializable;, "TT;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->this$1:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->val$observer:Lio/reactivex/Observer;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAddedEvent;->with(Ljava/io/Serializable;)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAddedEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 55
    :cond_0
    return-void
.end method

.method public onChatHeadAnimateEnd(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;"
    .local p1, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->this$1:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->val$observer:Lio/reactivex/Observer;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAnimateEndEvent;->with(Lcom/flipkart/chatheads/ui/ChatHead;)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAnimateEndEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 83
    :cond_0
    return-void
.end method

.method public onChatHeadAnimateStart(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;"
    .local p1, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->this$1:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->val$observer:Lio/reactivex/Observer;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAnimateStartEvent;->with(Lcom/flipkart/chatheads/ui/ChatHead;)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAnimateStartEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 90
    :cond_0
    return-void
.end method

.method public onChatHeadArrangementChanged(Lcom/flipkart/chatheads/ui/ChatHeadArrangement;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;)V
    .locals 2
    .param p1, "oldArrangement"    # Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    .param p2, "newArrangement"    # Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    .prologue
    .line 73
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->this$1:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->val$observer:Lio/reactivex/Observer;

    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;->with(Lcom/flipkart/chatheads/ui/ChatHeadArrangement;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 76
    :cond_0
    return-void
.end method

.method public onChatHeadRemoved(Ljava/io/Serializable;Z)V
    .locals 2
    .param p2, "userTriggered"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;"
    .local p1, "key":Ljava/io/Serializable;, "TT;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->this$1:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;->val$observer:Lio/reactivex/Observer;

    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;->with(Ljava/io/Serializable;Z)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 62
    :cond_0
    return-void
.end method
