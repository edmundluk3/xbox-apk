.class public Lcom/microsoft/playready/networkdevice/PRNDMediaPlayer;
.super Lcom/microsoft/playready/MediaPlayer;
.source "PRNDMediaPlayer.java"


# instance fields
.field private mReceiverSession:Lcom/microsoft/playready/networkdevice/IReceiverSession;


# direct methods
.method constructor <init>(Lcom/microsoft/playready/networkdevice/IReceiverSession;)V
    .locals 0
    .param p1, "prndSession"    # Lcom/microsoft/playready/networkdevice/IReceiverSession;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/microsoft/playready/MediaPlayer;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/PRNDMediaPlayer;->mReceiverSession:Lcom/microsoft/playready/networkdevice/IReceiverSession;

    .line 39
    check-cast p1, Lcom/microsoft/playready/networkdevice/ReceiverSession;

    .end local p1    # "prndSession":Lcom/microsoft/playready/networkdevice/IReceiverSession;
    invoke-virtual {p1, p0}, Lcom/microsoft/playready/networkdevice/ReceiverSession;->setupDrmProxy(Lcom/microsoft/playready/MediaPlayer;)V

    .line 40
    return-void
.end method

.method private buildContentUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 62
    move-object v4, p1

    .line 63
    .local v4, "result":Ljava/lang/String;
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 65
    .local v6, "uri":Landroid/net/Uri;
    if-eqz v6, :cond_3

    .line 69
    invoke-virtual {v6}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v2

    .line 70
    .local v2, "names":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {v6}, Landroid/net/Uri;->getPort()I

    move-result v3

    .line 76
    .local v3, "port":I
    if-lez v3, :cond_1

    .line 78
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Landroid/net/Uri;->getPort()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 85
    .local v5, "strFinal":Ljava/lang/String;
    :goto_0
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 87
    .local v8, "uriFinal":Landroid/net/Uri;
    if-eqz v8, :cond_3

    .line 89
    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v10, "PRNDSESSION"

    iget-object v11, p0, Lcom/microsoft/playready/networkdevice/PRNDMediaPlayer;->mReceiverSession:Lcom/microsoft/playready/networkdevice/IReceiverSession;

    invoke-interface {v11}, Lcom/microsoft/playready/networkdevice/IReceiverSession;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    .line 94
    .local v7, "uriBuilder":Landroid/net/Uri$Builder;
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 96
    .local v1, "name":Ljava/lang/String;
    const-string v9, "CONTENT_PROTECTION_TYPE"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "PRND_TCP_HOST"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "PRND_TCP_PORT"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "PRND_CID"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 101
    invoke-virtual {v6, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v1, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    goto :goto_1

    .line 82
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "name":Ljava/lang/String;
    .end local v5    # "strFinal":Ljava/lang/String;
    .end local v7    # "uriBuilder":Landroid/net/Uri$Builder;
    .end local v8    # "uriFinal":Landroid/net/Uri;
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "strFinal":Ljava/lang/String;
    goto :goto_0

    .line 105
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v7    # "uriBuilder":Landroid/net/Uri$Builder;
    .restart local v8    # "uriFinal":Landroid/net/Uri;
    :cond_2
    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 109
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "names":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v3    # "port":I
    .end local v5    # "strFinal":Ljava/lang/String;
    .end local v7    # "uriBuilder":Landroid/net/Uri$Builder;
    .end local v8    # "uriFinal":Landroid/net/Uri;
    :cond_3
    const-string v9, "PRNDMediaPlayer"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "DataSource: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    return-object v4
.end method

.method static createPRNDMediaPlayer(Lcom/microsoft/playready/networkdevice/IReceiverSession;)Lcom/microsoft/playready/networkdevice/PRNDMediaPlayer;
    .locals 1
    .param p0, "prndSession"    # Lcom/microsoft/playready/networkdevice/IReceiverSession;

    .prologue
    .line 32
    new-instance v0, Lcom/microsoft/playready/networkdevice/PRNDMediaPlayer;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/networkdevice/PRNDMediaPlayer;-><init>(Lcom/microsoft/playready/networkdevice/IReceiverSession;)V

    return-object v0
.end method


# virtual methods
.method public setDataSource(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/microsoft/playready/networkdevice/PRNDMediaPlayer;->buildContentUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 57
    invoke-super {p0, p1}, Lcom/microsoft/playready/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method public setDataSource(Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 46
    .local p2, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/playready/networkdevice/PRNDMediaPlayer;->buildContentUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 48
    invoke-super {p0, p1, p2}, Lcom/microsoft/playready/MediaPlayer;->setDataSource(Ljava/lang/String;Ljava/util/Map;)V

    .line 49
    return-void
.end method
