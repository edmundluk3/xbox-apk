.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$8;
.super Ljava/lang/Object;
.source "UTCPeopleHub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackAchievementsFilterAction(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$filter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$8;->val$filter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 250
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$8;->val$filter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 252
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 253
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "Filter"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$8;->val$filter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;->getTelemetryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 254
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$000()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 255
    const-string v1, "ProfileType"

    const-string v2, "YouProfile"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 260
    :goto_0
    const-string v1, "People Hub - Achievements Filter"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 261
    return-void

    .line 257
    :cond_0
    const-string v1, "ProfileType"

    const-string v2, "MeProfile"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
