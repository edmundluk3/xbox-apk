.class public Lcom/microsoft/xbox/xle/app/XLEHockeyIntegration;
.super Ljava/lang/Object;
.source "XLEHockeyIntegration.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;


# static fields
.field private static instance:Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static getInstance(Landroid/app/Activity;)Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 22
    sget-object v0, Lcom/microsoft/xbox/xle/app/XLEHockeyIntegration;->instance:Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xle/app/XLEHockeyIntegration;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/XLEHockeyIntegration;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/XLEHockeyIntegration;->instance:Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;

    .line 25
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/XLEHockeyIntegration;->instance:Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;

    return-object v0
.end method


# virtual methods
.method public checkForCrashes()V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public checkForUpdates()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public setLogging()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public shutdown()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method
