.class Lcom/microsoft/playready/HttpLocalServer$WorkerThread;
.super Ljava/lang/Thread;
.source "HttpLocalServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/HttpLocalServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WorkerThread"
.end annotation


# instance fields
.field private final conn:Lorg/apache/http/HttpServerConnection;

.field private final httpservice:Lorg/apache/http/protocol/HttpService;


# direct methods
.method public constructor <init>(Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;)V
    .locals 0
    .param p1, "httpservice"    # Lorg/apache/http/protocol/HttpService;
    .param p2, "conn"    # Lorg/apache/http/HttpServerConnection;

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 198
    iput-object p1, p0, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;->httpservice:Lorg/apache/http/protocol/HttpService;

    .line 199
    iput-object p2, p0, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    .line 200
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 203
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lorg/apache/http/protocol/BasicHttpContext;-><init>(Lorg/apache/http/protocol/HttpContext;)V

    .line 206
    .local v0, "context":Lorg/apache/http/protocol/HttpContext;
    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v2}, Lorg/apache/http/HttpServerConnection;->isOpen()Z
    :try_end_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/apache/http/HttpException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 218
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v2}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    .line 222
    :goto_1
    return-void

    .line 207
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;->httpservice:Lorg/apache/http/protocol/HttpService;

    iget-object v3, p0, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-virtual {v2, v3, v0}, Lorg/apache/http/protocol/HttpService;->handleRequest(Lorg/apache/http/HttpServerConnection;Lorg/apache/http/protocol/HttpContext;)V
    :try_end_2
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/apache/http/HttpException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 209
    :catch_0
    move-exception v1

    .line 210
    .local v1, "ex":Lorg/apache/http/ConnectionClosedException;
    :try_start_3
    const-string v2, "HttpLocalServer"

    const-string v3, "Closed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 218
    :try_start_4
    iget-object v2, p0, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v2}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 219
    :catch_1
    move-exception v2

    goto :goto_1

    .line 211
    .end local v1    # "ex":Lorg/apache/http/ConnectionClosedException;
    :catch_2
    move-exception v1

    .line 212
    .local v1, "ex":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 213
    const-string v2, "HttpLocalServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "I/O error: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 218
    :try_start_6
    iget-object v2, p0, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v2}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 219
    :catch_3
    move-exception v2

    goto :goto_1

    .line 214
    .end local v1    # "ex":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 215
    .local v1, "ex":Lorg/apache/http/HttpException;
    :try_start_7
    const-string v2, "HttpLocalServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unrecoverable: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/http/HttpException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 218
    :try_start_8
    iget-object v2, p0, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v2}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_1

    .line 219
    :catch_5
    move-exception v2

    goto :goto_1

    .line 216
    .end local v1    # "ex":Lorg/apache/http/HttpException;
    :catchall_0
    move-exception v2

    .line 218
    :try_start_9
    iget-object v3, p0, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;->conn:Lorg/apache/http/HttpServerConnection;

    invoke-interface {v3}, Lorg/apache/http/HttpServerConnection;->shutdown()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 221
    :goto_2
    throw v2

    .line 219
    :catch_6
    move-exception v2

    goto :goto_1

    :catch_7
    move-exception v3

    goto :goto_2
.end method
