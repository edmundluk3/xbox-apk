.class public abstract Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;
.super Ljava/lang/Object;
.source "MediaHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ScreenshotBatchResponse"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ScreenshotBatchResponse$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_ScreenshotBatchResponse$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract values()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;",
            ">;"
        }
    .end annotation
.end method
