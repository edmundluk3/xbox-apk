.class final synthetic Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$14;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# static fields
.field private static final instance:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$14;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$14;

    invoke-direct {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$14;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$14;->instance:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$14;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/Function;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$14;->instance:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$14;

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyMuteIntent;

    invoke-static {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->lambda$null$5(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyMuteIntent;)Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    move-result-object v0

    return-object v0
.end method
