.class public final enum Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;
.super Ljava/lang/Enum;
.source "BiEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/BiEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BiPoint"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

.field public static final enum ITEM_ACHIEVEMENT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

.field public static final enum ITEM_BROADCAST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

.field public static final enum ITEM_FRIEND_ADD:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

.field public static final enum ITEM_GAME_CLIP:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

.field public static final enum ITEM_GENERIC:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

.field public static final enum ITEM_SCREENSHOT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

.field public static final enum ITEM_SCREENSHOT_GLOBAL:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

.field public static final enum ITEM_STATUS_POST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

.field public static final enum PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

.field public static final enum TARGET_PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 88
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    const-string v1, "PROFILE"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .line 89
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    const-string v1, "TARGET_PROFILE"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->TARGET_PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .line 90
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    const-string v1, "ITEM_ACHIEVEMENT"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_ACHIEVEMENT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .line 91
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    const-string v1, "ITEM_BROADCAST"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_BROADCAST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .line 92
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    const-string v1, "ITEM_GAME_CLIP"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GAME_CLIP:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .line 93
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    const-string v1, "ITEM_GENERIC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GENERIC:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .line 94
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    const-string v1, "ITEM_FRIEND_ADD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_FRIEND_ADD:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .line 95
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    const-string v1, "ITEM_STATUS_POST"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_STATUS_POST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .line 96
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    const-string v1, "ITEM_SCREENSHOT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .line 97
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    const-string v1, "ITEM_SCREENSHOT_GLOBAL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT_GLOBAL:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .line 87
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->TARGET_PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_ACHIEVEMENT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_BROADCAST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GAME_CLIP:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GENERIC:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_FRIEND_ADD:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_STATUS_POST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT_GLOBAL:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 87
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    return-object v0
.end method
