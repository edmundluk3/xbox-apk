.class Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "GameProfileFriendsListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FollowersDataViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private favoriteIconView:Landroid/widget/TextView;

.field private gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private gamertagTextView:Landroid/widget/TextView;

.field private listHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private presenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private presenceTextView:Landroid/widget/TextView;

.field private realNameTextView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

.field private validItemImageView:Landroid/view/View;

.field private validItemView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;Landroid/view/View;)V
    .locals 1
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 263
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    .line 264
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 265
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->inflateListItemView()V

    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 267
    return-void
.end method

.method private inflateListItemView()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0658

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 271
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0659

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 272
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e065e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->validItemView:Landroid/view/View;

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e065a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->validItemImageView:Landroid/view/View;

    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e065b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0660

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->gamertagTextView:Landroid/widget/TextView;

    .line 277
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0662

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->presenceTextView:Landroid/widget/TextView;

    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e065c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->favoriteIconView:Landroid/widget/TextView;

    .line 279
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0661

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->realNameTextView:Landroid/widget/TextView;

    .line 280
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e065d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->presenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 281
    return-void
.end method

.method private updateUI(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 5
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 324
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 325
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 326
    if-eqz p1, :cond_1

    .line 327
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->gamertagTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->presenceTextView:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 333
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->realNameTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 335
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->favoriteIconView:Landroid/widget/TextView;

    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 336
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->presenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iget-object v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v4, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v0, v4, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 338
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v3, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v0, v3, :cond_1

    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->presenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->favoriteIconView:Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0c000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 343
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->validItemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->validItemImageView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 345
    return-void

    :cond_2
    move v0, v2

    .line 335
    goto :goto_0

    :cond_3
    move v0, v2

    .line 336
    goto :goto_1
.end method

.method private updateUIwithDummyData(Lcom/microsoft/xbox/service/model/FollowersData;I)V
    .locals 6
    .param p1, "dummyItem"    # Lcom/microsoft/xbox/service/model/FollowersData;
    .param p2, "friendsCount"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 284
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->validItemView:Landroid/view/View;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 285
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->validItemImageView:Landroid/view/View;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 286
    if-eqz p1, :cond_0

    .line 287
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 289
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 290
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0705c3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 291
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0

    .line 295
    .end local v0    # "text":Ljava/lang/String;
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 296
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0705c8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->areBothListEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    .line 300
    :pswitch_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 301
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07013a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0

    .line 305
    :pswitch_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 306
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0705c5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0

    .line 310
    :pswitch_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 311
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0705c4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->areBothListEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_2
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_2
    move v1, v2

    goto :goto_2

    .line 315
    :pswitch_5
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 316
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0704c4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->listStateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 287
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;)V
    .locals 2
    .param p1, "dataObject"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 359
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 361
    iget-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;->followersData:Lcom/microsoft/xbox/service/model/FollowersData;

    .line 362
    .local v0, "followersData":Lcom/microsoft/xbox/service/model/FollowersData;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getIsDummy()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 363
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->getFriendsWhoPlayCount()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->updateUIwithDummyData(Lcom/microsoft/xbox/service/model/FollowersData;I)V

    .line 367
    :goto_0
    return-void

    .line 365
    :cond_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->updateUI(Lcom/microsoft/xbox/service/model/FollowersData;)V

    goto :goto_0
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 249
    check-cast p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->onBind(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 349
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->getAdapterPosition()I

    move-result v1

    .line 350
    .local v1, "position":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;

    iget-object v0, v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;->followersData:Lcom/microsoft/xbox/service/model/FollowersData;

    .line 352
    .local v0, "item":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->setLastSelectedPosition(I)V

    .line 353
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->setFollowersData(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 354
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->gotoGamerProfile(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 355
    return-void
.end method
