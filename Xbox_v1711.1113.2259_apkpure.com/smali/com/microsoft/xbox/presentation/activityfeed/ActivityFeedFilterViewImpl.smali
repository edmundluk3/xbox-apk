.class public Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;
.super Landroid/support/constraint/ConstraintLayout;
.source "ActivityFeedFilterViewImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterView;


# instance fields
.field applyButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0154
    .end annotation
.end field

.field clubs:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e014b
    .end annotation
.end field

.field favorites:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0148
    .end annotation
.end field

.field private firstRenderComplete:Z

.field friends:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0145
    .end annotation
.end field

.field games:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e014e
    .end annotation
.end field

.field popular:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0151
    .end annotation
.end field

.field presenter:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private viewIntents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/support/constraint/ConstraintLayout;-><init>(Landroid/content/Context;)V

    .line 59
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->init(Landroid/content/Context;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/support/constraint/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->init(Landroid/content/Context;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/support/constraint/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->init(Landroid/content/Context;)V

    .line 70
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002a

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 74
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/view/View;)Lbutterknife/Unbinder;

    .line 75
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->applyButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v6

    .line 88
    .local v6, "apply":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$ApplyIntent;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->friends:Landroid/widget/CheckBox;

    .line 90
    invoke-static {v0}, Lcom/jakewharton/rxbinding2/widget/RxCompoundButton;->checkedChanges(Landroid/widget/CompoundButton;)Lcom/jakewharton/rxbinding2/InitialValueObservable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->favorites:Landroid/widget/CheckBox;

    .line 91
    invoke-static {v1}, Lcom/jakewharton/rxbinding2/widget/RxCompoundButton;->checkedChanges(Landroid/widget/CompoundButton;)Lcom/jakewharton/rxbinding2/InitialValueObservable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->clubs:Landroid/widget/CheckBox;

    .line 92
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/widget/RxCompoundButton;->checkedChanges(Landroid/widget/CompoundButton;)Lcom/jakewharton/rxbinding2/InitialValueObservable;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->games:Landroid/widget/CheckBox;

    .line 93
    invoke-static {v3}, Lcom/jakewharton/rxbinding2/widget/RxCompoundButton;->checkedChanges(Landroid/widget/CompoundButton;)Lcom/jakewharton/rxbinding2/InitialValueObservable;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->popular:Landroid/widget/CheckBox;

    .line 94
    invoke-static {v4}, Lcom/jakewharton/rxbinding2/widget/RxCompoundButton;->checkedChanges(Landroid/widget/CompoundButton;)Lcom/jakewharton/rxbinding2/InitialValueObservable;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function5;

    move-result-object v5

    .line 89
    invoke-static/range {v0 .. v5}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;)Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 97
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v7

    .line 100
    .local v7, "prefsChanged":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;>;"
    invoke-static {v6, v7}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->viewIntents:Lio/reactivex/Observable;

    .line 102
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getCheckboxMePreferedColorStateList()Landroid/content/res/ColorStateList;

    move-result-object v8

    .line 104
    .local v8, "stateList":Landroid/content/res/ColorStateList;
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->friends:Landroid/widget/CheckBox;

    invoke-static {v0, v8}, Landroid/support/v4/widget/CompoundButtonCompat;->setButtonTintList(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->favorites:Landroid/widget/CheckBox;

    invoke-static {v0, v8}, Landroid/support/v4/widget/CompoundButtonCompat;->setButtonTintList(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->clubs:Landroid/widget/CheckBox;

    invoke-static {v0, v8}, Landroid/support/v4/widget/CompoundButtonCompat;->setButtonTintList(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->games:Landroid/widget/CheckBox;

    invoke-static {v0, v8}, Landroid/support/v4/widget/CompoundButtonCompat;->setButtonTintList(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->popular:Landroid/widget/CheckBox;

    invoke-static {v0, v8}, Landroid/support/v4/widget/CompoundButtonCompat;->setButtonTintList(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V

    .line 109
    return-void
.end method

.method static synthetic lambda$init$0(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$ApplyIntent;
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;
    .param p1, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->friends:Landroid/widget/CheckBox;

    .line 81
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->favorites:Landroid/widget/CheckBox;

    .line 82
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->clubs:Landroid/widget/CheckBox;

    .line 83
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->games:Landroid/widget/CheckBox;

    .line 84
    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->popular:Landroid/widget/CheckBox;

    .line 85
    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    .line 80
    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->with(ZZZZZ)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    .line 79
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$ApplyIntent;->with(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$ApplyIntent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$init$1(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->firstRenderComplete:Z

    return v0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Landroid/support/constraint/ConstraintLayout;->onAttachedToWindow()V

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->presenter:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->onStart(Ljava/lang/Object;)V

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->firstRenderComplete:Z

    .line 116
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Landroid/support/constraint/ConstraintLayout;->onDetachedFromWindow()V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->presenter:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->onDestroy(Ljava/lang/Object;)V

    .line 122
    return-void
.end method

.method public render(Lcom/microsoft/xbox/presentation/lce/LceViewState;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "state":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;>;"
    const/4 v2, 0x1

    .line 131
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->content()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    .line 133
    .local v0, "prefs":Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isContent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->friends:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFriends()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->favorites:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFavorites()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->clubs:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showClubs()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 137
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->games:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showGames()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->popular:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showPopular()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 140
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->favorites:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFriends()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 142
    iput-boolean v2, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->firstRenderComplete:Z

    .line 146
    :goto_1
    return-void

    .line 140
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 144
    :cond_1
    const-string v1, "Only content state is currently implemented"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public bridge synthetic render(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lcom/microsoft/xbox/presentation/lce/LceViewState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->render(Lcom/microsoft/xbox/presentation/lce/LceViewState;)V

    return-void
.end method

.method public viewIntents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;->viewIntents:Lio/reactivex/Observable;

    return-object v0
.end method
