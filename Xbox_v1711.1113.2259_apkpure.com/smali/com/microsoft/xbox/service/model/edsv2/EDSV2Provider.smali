.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
.super Ljava/lang/Object;
.source "EDSV2Provider.java"


# static fields
.field private static XboxMusic:Ljava/lang/String; = null

.field private static XboxVideo:Ljava/lang/String; = null

.field public static final Xbox_One_FirstPartyTemplate:Ljava/lang/String; = "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

.field public static final Xbox_One_LaunchMediaTemplate:Ljava/lang/String; = "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s&DeepLinkInfo=%s"

.field public static final Xbox_One_LaunchTitleTemplate:Ljava/lang/String; = "ms-xbl-%08X://default/"


# instance fields
.field private ContentId:Ljava/lang/String;

.field public DeepLinkInfo:Ljava/lang/String;

.field public ProductId:Ljava/lang/String;

.field public ProviderMediaId:Ljava/lang/String;

.field private canonicalId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "CanonicalId"
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ID"
    .end annotation
.end field

.field private imageUrl:Ljava/lang/String;

.field private images:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Images"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation
.end field

.field private isXboxMusic:Z

.field private isXboxVideo:Z

.field private launchParams:Ljava/lang/String;

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Name"
    .end annotation
.end field

.field private partnerMediaId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PartnerMediaId"
    .end annotation
.end field

.field private titleId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "XboxVideo"

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->XboxVideo:Ljava/lang/String;

    .line 20
    const-string v0, "XboxMusic"

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->XboxMusic:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->titleId:J

    .line 48
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->isXboxMusic:Z

    .line 49
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->isXboxVideo:Z

    return-void
.end method

.method public static getMusicProviders(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .param p0, "bingId"    # Ljava/lang/String;
    .param p1, "zuneId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 196
    .local v0, "generatedProvider":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;>;"
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;-><init>()V

    .line 197
    .local v1, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    const-wide/32 v2, 0x18ffc9f4

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setTitleId(J)V

    .line 198
    const-string v2, "Xbox Music"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setName(Ljava/lang/String;)V

    .line 199
    const-string v2, "6D96DEDC-F3C9-43F8-89E3-0C95BF76AD2A"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setCanonicalId(Ljava/lang/String;)V

    .line 200
    iput-object p1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->ProductId:Ljava/lang/String;

    .line 201
    iput-object p0, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->ProviderMediaId:Ljava/lang/String;

    .line 202
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setIsXboxMusic(Z)V

    .line 203
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    return-object v0
.end method


# virtual methods
.method public getAlbumLaunchUri()Ljava/lang/String;
    .locals 5

    .prologue
    .line 182
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0x18ffc9f4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getEncodedProviderMediaId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "Album"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppLaunchUri()Ljava/lang/String;
    .locals 6

    .prologue
    .line 177
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ms-xbl-%08X://default/"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getArtistLaunchUri()Ljava/lang/String;
    .locals 5

    .prologue
    .line 190
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0x18ffc9f4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getEncodedProviderMediaId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "MusicArtist"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->canonicalId:Ljava/lang/String;

    return-object v0
.end method

.method public getEncodedDeepLinkInfo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->DeepLinkInfo:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/toolkit/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->ensureUrlEncoding(Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEncodedProviderMediaId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->ProviderMediaId:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/toolkit/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->ensureUrlEncoding(Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->imageUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->imageUrl:Ljava/lang/String;

    .line 120
    :goto_0
    return-object v0

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->images:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getBoxArtImageURI(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->imageUrl:Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->imageUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getImages()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->images:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getIsXboxMusic()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->isXboxMusic:Z

    return v0
.end method

.method public getIsXboxVideo()Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->isXboxVideo:Z

    return v0
.end method

.method public getMediaLaunchUri(Ljava/lang/String;I)Ljava/lang/String;
    .locals 10
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "mediaType"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 167
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "mediaTypeString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v2

    const-wide/32 v4, 0x3d705025

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 170
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object p1, v3, v7

    aput-object v0, v3, v8

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 172
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s&DeepLinkInfo=%s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getEncodedProviderMediaId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    aput-object v0, v3, v8

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getEncodedDeepLinkInfo()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPartnerMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->partnerMediaId:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleId()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    .line 60
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->titleId:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->id:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 63
    iput-wide v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->titleId:J

    .line 74
    :cond_0
    :goto_0
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->titleId:J

    return-wide v0

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->id:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->XboxVideo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    const-wide/32 v0, 0x3d705025

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->titleId:J

    .line 66
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->isXboxVideo:Z

    goto :goto_0

    .line 67
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->id:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->XboxMusic:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 68
    const-wide/32 v0, 0x18ffc9f4

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->titleId:J

    .line 69
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->isXboxMusic:Z

    goto :goto_0

    .line 71
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->id:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->parseHexLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->titleId:J

    goto :goto_0
.end method

.method public getTrackLaunchUri()Ljava/lang/String;
    .locals 5

    .prologue
    .line 186
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0x18ffc9f4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getEncodedProviderMediaId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "Track"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasLaunchInfo()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 152
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->isXboxMusic:Z

    if-eqz v1, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v0

    .line 155
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v2

    const-wide/32 v4, 0x18ffc9f4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v2

    const-wide/32 v4, 0x3d705025

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 163
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->DeepLinkInfo:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCanonicalId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->canonicalId:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->id:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public setImageUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->imageUrl:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public setImages(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "images":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->images:Ljava/util/ArrayList;

    .line 133
    return-void
.end method

.method public setIsXboxMusic(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 140
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->isXboxMusic:Z

    .line 141
    return-void
.end method

.method public setIsXboxVideo(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->isXboxVideo:Z

    .line 149
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->name:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setPartnerMediaId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->partnerMediaId:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setTitleId(J)V
    .locals 1
    .param p1, "titleId"    # J

    .prologue
    .line 83
    iput-wide p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->titleId:J

    .line 84
    return-void
.end method
