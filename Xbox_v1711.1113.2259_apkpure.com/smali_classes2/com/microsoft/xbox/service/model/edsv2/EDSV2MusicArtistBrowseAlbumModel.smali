.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;
.source "EDSV2MusicArtistBrowseAlbumModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_TOP_TRACKS:I = 0x14


# instance fields
.field private lastRefreshedTopTrackTime:Ljava/util/Date;

.field private loadTracksRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;

.field private loadingTrackStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private tracks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 2
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 21
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->lastRefreshedTopTrackTime:Ljava/util/Date;

    .line 22
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->loadingTrackStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 23
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->tracks:Ljava/util/ArrayList;

    .line 24
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->loadTracksRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->loadingTrackStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->loadTracksRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;

    .line 30
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->onGetTopTracksCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private onGetTopTracksCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;>;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 88
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->lastRefreshedTopTrackTime:Ljava/util/Date;

    .line 89
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->tracks:Ljava/util/ArrayList;

    .line 91
    :cond_0
    return-void
.end method


# virtual methods
.method protected bridge synthetic createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    move-result-object v0

    return-object v0
.end method

.method protected createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;
    .locals 1
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 47
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-object v0
.end method

.method public getAlbums()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->browseListData:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->getArtistName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getDesiredMediaItemType()I
    .locals 1

    .prologue
    .line 38
    const/16 v0, 0x3ee

    return v0
.end method

.method public getIsLoadingTracks()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->loadingTrackStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public getMediaGroup()I
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x6

    return v0
.end method

.method protected getOrderBy()Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_RELEASEDATE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    return-object v0
.end method

.method public getTopTracks()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->tracks:Ljava/util/ArrayList;

    return-object v0
.end method

.method public loadTopTracks(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 69
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 70
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 72
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 74
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_1

    .line 75
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->lastRefreshedTopTrackTime:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->loadingTrackStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->loadTracksRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel$LoadTopTracksRunner;

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    .line 77
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0xfaf

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v1, v3, v3, v2, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0
.end method

.method public shouldRefreshTrack()Z
    .locals 4

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->lastRefreshedTopTrackTime:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method
