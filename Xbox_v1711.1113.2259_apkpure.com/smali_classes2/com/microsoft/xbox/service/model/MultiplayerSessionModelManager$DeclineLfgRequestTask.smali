.class Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "MultiplayerSessionModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeclineLfgRequestTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final handleId:Ljava/lang/String;

.field private final memberIndex:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

.field private updatedSession:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "handleId"    # Ljava/lang/String;
    .param p3, "memberIndex"    # Ljava/lang/String;

    .prologue
    .line 457
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 458
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->handleId:Ljava/lang/String;

    .line 459
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->memberIndex:Ljava/lang/String;

    .line 460
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 464
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 479
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->handleId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->memberIndex:Ljava/lang/String;

    .line 481
    invoke-static {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RemoveMultiplayerMemberRequest;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RemoveMultiplayerMemberRequest;

    move-result-object v3

    .line 479
    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;->removeUserFromSession(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RemoveMultiplayerMemberRequest;)Lio/reactivex/Single;

    move-result-object v1

    .line 481
    invoke-virtual {v1}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->updatedSession:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 487
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v1

    .line 482
    :catch_0
    move-exception v0

    .line 483
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error removing user from LFG session"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 484
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 452
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 473
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 452
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 469
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 496
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->updatedSession:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->LeaveLFGSession:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->access$100(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;Lcom/microsoft/xbox/service/model/UpdateType;)V

    .line 497
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 452
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 492
    return-void
.end method
