.class final Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult;
.super Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult;
.source "AutoValue_CommonActionsAndResults_ItemClickedResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final item:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 13
    .local p0, "this":Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult;, "Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult<TT;>;"
    .local p1, "item":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult;-><init>()V

    .line 14
    if-nez p1, :cond_0

    .line 15
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null item"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult;->item:Ljava/lang/Object;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 35
    .local p0, "this":Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult;, "Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult<TT;>;"
    if-ne p1, p0, :cond_0

    .line 36
    const/4 v1, 0x1

    .line 42
    :goto_0
    return v1

    .line 38
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 39
    check-cast v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult;

    .line 40
    .local v0, "that":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult;->item:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult;->item()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 42
    .end local v0    # "that":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult<*>;"
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 47
    .local p0, "this":Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult;, "Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult<TT;>;"
    const/4 v0, 0x1

    .line 48
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult;->item:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 50
    return v0
.end method

.method public item()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult;, "Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult;->item:Ljava/lang/Object;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28
    .local p0, "this":Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult;, "Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ItemClickedResult{item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_ItemClickedResult;->item:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
