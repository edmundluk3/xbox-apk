.class public Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "TVSeriesSeasonsScreenAdapter.java"


# instance fields
.field private episodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private filterSpinner:Landroid/widget/Spinner;

.field private filterSpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;)V
    .locals 3
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 36
    const v0, 0x7f0e0b89

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->screenBody:Landroid/view/View;

    .line 37
    const v0, 0x7f0e0b8b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    .line 40
    const v0, 0x7f0e0b8c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 42
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030254

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 52
    const v0, 0x7f0e0b8a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 4

    .prologue
    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->getEpisodesListState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->isBusy()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->filterSpinnerAdapter:Landroid/widget/ArrayAdapter;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->getSeasons()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 62
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f030207

    invoke-direct {v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->filterSpinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->filterSpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v2, 0x7f03020a

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->getSeasons()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 65
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    instance-of v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    if-eqz v2, :cond_0

    .line 66
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->filterSpinnerAdapter:Landroid/widget/ArrayAdapter;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->getSeasonName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 69
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->filterSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 87
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->episodes:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->getEpisodesOfSelectedSeason()Ljava/util/ArrayList;

    move-result-object v2

    if-eq v1, v2, :cond_4

    .line 88
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;->clear()V

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->getEpisodesOfSelectedSeason()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->episodes:Ljava/util/ArrayList;

    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->getEpisodesOfSelectedSeason()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->episodes:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 93
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 96
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->finishedLoadingEpisodes()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->episodes:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/TVSeriesSeasonsScreen;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 99
    :cond_5
    return-void
.end method
