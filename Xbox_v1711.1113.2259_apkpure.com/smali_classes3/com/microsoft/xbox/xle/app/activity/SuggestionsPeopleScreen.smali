.class public Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "SuggestionsPeopleScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen$SuggestionsPeopleAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen$SuggestionsPeopleAdapterProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen$SuggestionsPeopleAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen$SuggestionsPeopleAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen$SuggestionsPeopleAdapterProvider;

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen$SuggestionsPeopleAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen$SuggestionsPeopleAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen$SuggestionsPeopleAdapterProvider;

    .line 28
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const-string v0, "Suggestions People"

    return-object v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070c94

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 32
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 34
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;->onCreateContentView()V

    .line 35
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen$SuggestionsPeopleAdapterProvider;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 36
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->setAsPivotPane()V

    .line 38
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 39
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 55
    const v0, 0x7f030220

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;->setContentView(I)V

    .line 56
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method
