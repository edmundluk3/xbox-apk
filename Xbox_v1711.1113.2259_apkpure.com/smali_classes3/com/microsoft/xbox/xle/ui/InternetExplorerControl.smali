.class public Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;
.super Landroid/widget/RelativeLayout;
.source "InternetExplorerControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;
    }
.end annotation


# static fields
.field private static final BROWSER_COMMAND_FORMAT:Ljava/lang/String; = "{\"action\" : \"command\", \"id\": \"{%s}\", \"parameters\" : { \"type\" : %d }}"

.field private static final IE_CHANNEL_WAIT_TIME:I = 0x2710

.field private static final REQUEST_URL_REQUEST_FORMAT:Ljava/lang/String; = "{\"action\" : \"request_url\", \"id\": \"{%s}\"}"

.field private static final SEND_URL_REQUEST_FORMAT:Ljava/lang/String; = "{\"action\" : \"send_url\", \"id\": \"{%s}\", \"parameters\": { \"url\": { \"id\": \"{%s}\", \"size\": 1, \"index\": 0, \"data\": \"%s\" }}}"


# instance fields
.field private editBoxHasFocus:Z

.field private ieClearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private ieControlsContainer:Landroid/widget/RelativeLayout;

.field private ieEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private ieRefreshButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private ieStopButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private nowPlayingMediaPause:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private nowPlayingMediaPlay:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private nowPlayingPullDown:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private originalSoftInputMode:I

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 86
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f030195

    const/4 v4, 0x1

    invoke-virtual {v2, v3, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 88
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 89
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    const v3, 0x7f0e07f7

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieControlsContainer:Landroid/widget/RelativeLayout;

    .line 92
    const v3, 0x7f0e07f8

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 93
    const v3, 0x7f0e078e

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingMediaPlay:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 94
    const v3, 0x7f0e078d

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingMediaPause:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 95
    const v3, 0x7f0e07fc

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingPullDown:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 96
    const v3, 0x7f0e07f9

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieRefreshButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 97
    const v3, 0x7f0e07fa

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieStopButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 98
    const v3, 0x7f0e07fb

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieClearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 100
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingMediaPlay:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v4, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$1;-><init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingMediaPause:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v4, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$2;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$2;-><init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingPullDown:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v4, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$3;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$3;-><init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieRefreshButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v4, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$4;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$4;-><init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieStopButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v4, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$5;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$5;-><init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieClearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v4, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$6;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$6;-><init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v4, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$7;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$7;-><init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 164
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v4, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$8;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$8;-><init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 173
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v4, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$9;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$9;-><init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setKeyStrokeListener(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$KeyStrokeListener;)V

    .line 185
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 186
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->originalSoftInputMode:I

    .line 187
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->pullDown()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->refreshBrowser()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->stopBrowsing()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->clearTextbox()V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieControlsContainer:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->navigateBrowser(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->editBoxHasFocus:Z

    return p1
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .prologue
    .line 43
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->originalSoftInputMode:I

    return v0
.end method

.method private clearTextbox()V
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->requestFocus()Z

    .line 292
    return-void
.end method

.method private navigateBrowser(Ljava/lang/String;)V
    .locals 5
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 262
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "id":Ljava/lang/String;
    const-string v2, "{\"action\" : \"send_url\", \"id\": \"{%s}\", \"parameters\": { \"url\": { \"id\": \"{%s}\", \"size\": 1, \"index\": 0, \"data\": \"%s\" }}}"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 265
    .local v1, "json":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->sendIeTitleMessage(Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method private pullDown()V
    .locals 5

    .prologue
    .line 269
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->getLastKnownIeUrl()Ljava/lang/String;

    move-result-object v2

    .line 270
    .local v2, "url":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 271
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 272
    .local v0, "browserIntent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 274
    :try_start_0
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/XLEApplication;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    .end local v0    # "browserIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 275
    .restart local v0    # "browserIntent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 276
    .local v1, "ex":Landroid/content/ActivityNotFoundException;
    const-string v3, "InternetExplorerControl"

    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private refreshBrowser()V
    .locals 1

    .prologue
    .line 282
    sget-object v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Refresh:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->sendBrowserCommand(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;)V

    .line 283
    return-void
.end method

.method private sendBrowserCommand(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;)V
    .locals 5
    .param p1, "type"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    .prologue
    .line 239
    const-string v1, "{\"action\" : \"command\", \"id\": \"{%s}\", \"parameters\" : { \"type\" : %d }}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/SessionModel;->getDeviceId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->getValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 240
    .local v0, "json":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->sendIeTitleMessage(Ljava/lang/String;)V

    .line 241
    return-void
.end method

.method private sendIeTitleMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "json"    # Ljava/lang/String;

    .prologue
    .line 244
    new-instance v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$11;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$10;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$10;-><init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$11;-><init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 258
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    .line 259
    return-void
.end method

.method private sendRequestUrlCommand()V
    .locals 5

    .prologue
    .line 234
    const-string v1, "{\"action\" : \"request_url\", \"id\": \"{%s}\"}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/SessionModel;->getDeviceId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "json":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->sendIeTitleMessage(Ljava/lang/String;)V

    .line 236
    return-void
.end method

.method private stopBrowsing()V
    .locals 1

    .prologue
    .line 286
    sget-object v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Stop:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->sendBrowserCommand(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;)V

    .line 287
    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingMediaPlay:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingMediaPause:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingPullDown:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieRefreshButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieStopButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieClearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setKeyStrokeListener(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$KeyStrokeListener;)V

    .line 231
    return-void
.end method

.method public initialize(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;Z)V
    .locals 0
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;
    .param p2, "requestUrl"    # Z

    .prologue
    .line 190
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    .line 191
    if-eqz p2, :cond_0

    .line 192
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->sendRequestUrlCommand()V

    .line 194
    :cond_0
    return-void
.end method

.method public update()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x4

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->shouldShowMediaProgress()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingMediaPlay:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->isMediaPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 199
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingMediaPause:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->isMediaPaused()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 205
    :goto_2
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->editBoxHasFocus:Z

    if-eqz v0, :cond_3

    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieClearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieStopButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieRefreshButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 221
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 198
    goto :goto_0

    :cond_1
    move v0, v1

    .line 199
    goto :goto_1

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingMediaPlay:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->nowPlayingMediaPause:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_2

    .line 210
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->getLastKnownIeUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieClearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->shouldShowIeStopButton()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieStopButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieRefreshButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_3

    .line 217
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieStopButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->ieRefreshButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_3
.end method
