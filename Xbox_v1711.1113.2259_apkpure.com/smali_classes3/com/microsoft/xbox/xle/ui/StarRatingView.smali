.class public Lcom/microsoft/xbox/xle/ui/StarRatingView;
.super Landroid/view/View;
.source "StarRatingView.java"


# static fields
.field private static final DefaultPaddingLeft:I = 0x3

.field private static final DefaultTotalStar:I = 0x5

.field protected static final MASK_COLOR_GRAY:I = 0x2


# instance fields
.field private averageUserRating:F

.field private emptyBitmap:Landroid/graphics/Bitmap;

.field private fullStarBitmap:Landroid/graphics/Bitmap;

.field private halfStarBitmap:Landroid/graphics/Bitmap;

.field private normalizedUserRating:F

.field private paint:Landroid/graphics/Paint;

.field private quarterBitmap:Landroid/graphics/Bitmap;

.field private threeQuartersBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 50
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->paint:Landroid/graphics/Paint;

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201e5

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->fullStarBitmap:Landroid/graphics/Bitmap;

    .line 58
    sget-object v2, Lcom/microsoft/xboxone/smartglass/R$styleable;->StarRatingView:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 59
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 60
    .local v1, "maskColor":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201e2

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->threeQuartersBitmap:Landroid/graphics/Bitmap;

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201e7

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->halfStarBitmap:Landroid/graphics/Bitmap;

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201e9

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->quarterBitmap:Landroid/graphics/Bitmap;

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201e4

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->emptyBitmap:Landroid/graphics/Bitmap;

    .line 71
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 75
    return-void

    .line 66
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201e1

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->threeQuartersBitmap:Landroid/graphics/Bitmap;

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201e6

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->halfStarBitmap:Landroid/graphics/Bitmap;

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201e8

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->quarterBitmap:Landroid/graphics/Bitmap;

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201e3

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->emptyBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private setNarratorContent(F)V
    .locals 4
    .param p1, "averageUserRating"    # F

    .prologue
    .line 152
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070d82

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 153
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 112
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 114
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->getPaddingLeft()I

    move-result v3

    .line 115
    .local v3, "left":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->getPaddingTop()I

    move-result v6

    .line 116
    .local v6, "top":I
    move v4, v3

    .line 118
    .local v4, "leftNow":I
    iget v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->averageUserRating:F

    float-to-int v1, v7

    .line 119
    .local v1, "fullStarNum":I
    rsub-int/lit8 v7, v1, 0x5

    add-int/lit8 v0, v7, -0x1

    .line 120
    .local v0, "emptyStarNum":I
    iget v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->averageUserRating:F

    int-to-float v8, v1

    sub-float v5, v7, v8

    .line 122
    .local v5, "quarterStar":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 123
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->fullStarBitmap:Landroid/graphics/Bitmap;

    int-to-float v8, v4

    int-to-float v9, v6

    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 124
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->fullStarBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, 0x3

    add-int/2addr v4, v7

    .line 122
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 127
    :cond_0
    const/4 v7, 0x0

    cmpg-float v7, v5, v7

    if-gtz v7, :cond_1

    .line 128
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->emptyBitmap:Landroid/graphics/Bitmap;

    int-to-float v8, v4

    int-to-float v9, v6

    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 129
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->emptyBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, 0x3

    add-int/2addr v4, v7

    .line 144
    :goto_1
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v0, :cond_5

    .line 145
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->emptyBitmap:Landroid/graphics/Bitmap;

    int-to-float v8, v4

    int-to-float v9, v6

    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 146
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->emptyBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, 0x3

    add-int/2addr v4, v7

    .line 144
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 130
    :cond_1
    float-to-double v8, v5

    const-wide/high16 v10, 0x3fd0000000000000L    # 0.25

    cmpg-double v7, v8, v10

    if-gtz v7, :cond_2

    .line 131
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->quarterBitmap:Landroid/graphics/Bitmap;

    int-to-float v8, v4

    int-to-float v9, v6

    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 132
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->quarterBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, 0x3

    add-int/2addr v4, v7

    goto :goto_1

    .line 133
    :cond_2
    float-to-double v8, v5

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    cmpg-double v7, v8, v10

    if-gtz v7, :cond_3

    .line 134
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->halfStarBitmap:Landroid/graphics/Bitmap;

    int-to-float v8, v4

    int-to-float v9, v6

    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 135
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->halfStarBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, 0x3

    add-int/2addr v4, v7

    goto :goto_1

    .line 136
    :cond_3
    float-to-double v8, v5

    const-wide/high16 v10, 0x3fe8000000000000L    # 0.75

    cmpg-double v7, v8, v10

    if-gtz v7, :cond_4

    .line 137
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->threeQuartersBitmap:Landroid/graphics/Bitmap;

    int-to-float v8, v4

    int-to-float v9, v6

    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 138
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->threeQuartersBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, 0x3

    add-int/2addr v4, v7

    goto :goto_1

    .line 140
    :cond_4
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->fullStarBitmap:Landroid/graphics/Bitmap;

    int-to-float v8, v4

    int-to-float v9, v6

    iget-object v10, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 141
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->fullStarBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, 0x3

    add-int/2addr v4, v7

    goto :goto_1

    .line 148
    :cond_5
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 105
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->fullStarBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    mul-int/lit8 v2, v2, 0x5

    add-int/lit8 v2, v2, 0xc

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->getPaddingLeft()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->getPaddingRight()I

    move-result v3

    add-int v1, v2, v3

    .line 106
    .local v1, "width":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->fullStarBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->getPaddingBottom()I

    move-result v3

    add-int v0, v2, v3

    .line 107
    .local v0, "height":I
    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setMeasuredDimension(II)V

    .line 108
    return-void
.end method

.method public setAverageUserRating(F)V
    .locals 2
    .param p1, "averageUserRating"    # F

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 80
    cmpl-float v0, p1, v1

    if-lez v0, :cond_0

    .line 81
    const/high16 p1, 0x3f800000    # 1.0f

    .line 84
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->normalizedUserRating:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_1

    .line 85
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_2

    cmpg-float v0, p1, v1

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 86
    const/high16 v0, 0x40a00000    # 5.0f

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->averageUserRating:F

    .line 87
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->normalizedUserRating:F

    .line 88
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->postInvalidate()V

    .line 91
    :cond_1
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->averageUserRating:F

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setNarratorContent(F)V

    .line 92
    return-void

    .line 85
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFiveStarRating(F)V
    .locals 1
    .param p1, "fiveStarRating"    # F

    .prologue
    .line 95
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->normalizedUserRating:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 96
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    const/high16 v0, 0x40a00000    # 5.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 97
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->averageUserRating:F

    .line 98
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/StarRatingView;->normalizedUserRating:F

    .line 99
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->postInvalidate()V

    .line 101
    :cond_0
    return-void

    .line 96
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
