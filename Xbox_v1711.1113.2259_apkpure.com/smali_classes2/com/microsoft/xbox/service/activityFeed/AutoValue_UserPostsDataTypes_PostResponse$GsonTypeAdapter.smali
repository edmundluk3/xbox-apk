.class public final Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_UserPostsDataTypes_PostResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultPostAuthor:Ljava/lang/String;

.field private defaultPostDate:Ljava/util/Date;

.field private defaultPostId:Ljava/lang/String;

.field private defaultPostText:Ljava/lang/String;

.field private defaultPostType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

.field private defaultPostTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

.field private defaultPostUri:Ljava/lang/String;

.field private defaultTimelines:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;",
            ">;"
        }
    .end annotation
.end field

.field private final postAuthorAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final postDateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final postIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final postTextAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final postTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;",
            ">;"
        }
    .end annotation
.end field

.field private final postTypeDataAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;",
            ">;"
        }
    .end annotation
.end field

.field private final postUriAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final timelinesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostAuthor:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostDate:Ljava/util/Date;

    .line 34
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostId:Ljava/lang/String;

    .line 35
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostText:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 37
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .line 38
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostUri:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultTimelines:Lcom/google/common/collect/ImmutableList;

    .line 41
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postAuthorAdapter:Lcom/google/gson/TypeAdapter;

    .line 42
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postDateAdapter:Lcom/google/gson/TypeAdapter;

    .line 43
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 44
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postTextAdapter:Lcom/google/gson/TypeAdapter;

    .line 45
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 46
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postTypeDataAdapter:Lcom/google/gson/TypeAdapter;

    .line 47
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postUriAdapter:Lcom/google/gson/TypeAdapter;

    .line 48
    const-class v0, Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    const-class v3, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->timelinesAdapter:Lcom/google/gson/TypeAdapter;

    .line 49
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 11
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v10, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v10, :cond_0

    .line 111
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 112
    const/4 v0, 0x0

    .line 168
    :goto_0
    return-object v0

    .line 114
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostAuthor:Ljava/lang/String;

    .line 116
    .local v1, "postAuthor":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostDate:Ljava/util/Date;

    .line 117
    .local v2, "postDate":Ljava/util/Date;
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostId:Ljava/lang/String;

    .line 118
    .local v3, "postId":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostText:Ljava/lang/String;

    .line 119
    .local v4, "postText":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 120
    .local v5, "postType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
    iget-object v6, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .line 121
    .local v6, "postTypeData":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    iget-object v7, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostUri:Ljava/lang/String;

    .line 122
    .local v7, "postUri":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultTimelines:Lcom/google/common/collect/ImmutableList;

    .line 123
    .local v8, "timelines":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;>;"
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 124
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v9

    .line 125
    .local v9, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v10, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v10, :cond_1

    .line 126
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 129
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 163
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 129
    :sswitch_0
    const-string v10, "postAuthor"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v10, "postDate"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v10, "postId"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v10, "postText"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v10, "postType"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v10, "postTypeData"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v10, "postUri"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x6

    goto :goto_2

    :sswitch_7
    const-string v10, "timelines"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x7

    goto :goto_2

    .line 131
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postAuthorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "postAuthor":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 132
    .restart local v1    # "postAuthor":Ljava/lang/String;
    goto :goto_1

    .line 135
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postDateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "postDate":Ljava/util/Date;
    check-cast v2, Ljava/util/Date;

    .line 136
    .restart local v2    # "postDate":Ljava/util/Date;
    goto/16 :goto_1

    .line 139
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "postId":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 140
    .restart local v3    # "postId":Ljava/lang/String;
    goto/16 :goto_1

    .line 143
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postTextAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "postText":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 144
    .restart local v4    # "postText":Ljava/lang/String;
    goto/16 :goto_1

    .line 147
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "postType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
    check-cast v5, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 148
    .restart local v5    # "postType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
    goto/16 :goto_1

    .line 151
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postTypeDataAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "postTypeData":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    check-cast v6, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .line 152
    .restart local v6    # "postTypeData":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    goto/16 :goto_1

    .line 155
    :pswitch_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postUriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "postUri":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 156
    .restart local v7    # "postUri":Ljava/lang/String;
    goto/16 :goto_1

    .line 159
    :pswitch_7
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->timelinesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "timelines":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;>;"
    check-cast v8, Lcom/google/common/collect/ImmutableList;

    .line 160
    .restart local v8    # "timelines":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;>;"
    goto/16 :goto_1

    .line 167
    .end local v9    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 168
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse;

    invoke-direct/range {v0 .. v8}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V

    goto/16 :goto_0

    .line 129
    :sswitch_data_0
    .sparse-switch
        -0x4b9add3c -> :sswitch_5
        -0x3a8f0625 -> :sswitch_2
        -0x17518f54 -> :sswitch_6
        0x2e1b452 -> :sswitch_7
        0x2d17ac6e -> :sswitch_1
        0x2d1f01ed -> :sswitch_3
        0x2d1f4bfa -> :sswitch_4
        0x41d9ae4b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultPostAuthor(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPostAuthor"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostAuthor:Ljava/lang/String;

    .line 52
    return-object p0
.end method

.method public setDefaultPostDate(Ljava/util/Date;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPostDate"    # Ljava/util/Date;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostDate:Ljava/util/Date;

    .line 56
    return-object p0
.end method

.method public setDefaultPostId(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPostId"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostId:Ljava/lang/String;

    .line 60
    return-object p0
.end method

.method public setDefaultPostText(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPostText"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostText:Ljava/lang/String;

    .line 64
    return-object p0
.end method

.method public setDefaultPostType(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPostType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 68
    return-object p0
.end method

.method public setDefaultPostTypeData(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPostTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .line 72
    return-object p0
.end method

.method public setDefaultPostUri(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPostUri"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultPostUri:Ljava/lang/String;

    .line 76
    return-object p0
.end method

.method public setDefaultTimelines(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "defaultTimelines":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->defaultTimelines:Lcom/google/common/collect/ImmutableList;

    .line 80
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    if-nez p2, :cond_0

    .line 86
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 107
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 90
    const-string v0, "postAuthor"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postAuthorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 92
    const-string v0, "postDate"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postDateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 94
    const-string v0, "postId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 96
    const-string v0, "postText"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postTextAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 98
    const-string v0, "postType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 100
    const-string v0, "postTypeData"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postTypeDataAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postTypeData()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 102
    const-string v0, "postUri"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->postUriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 104
    const-string v0, "timelines"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->timelinesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->timelines()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 106
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    check-cast p2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)V

    return-void
.end method
