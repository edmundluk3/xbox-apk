.class Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$7;
.super Ljava/lang/Object;
.source "PivotWithTabs.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/functions/GenericFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
        "<",
        "Ljava/lang/Boolean;",
        "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .prologue
    .line 354
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$7;->this$0:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public eval(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "arg"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 357
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getIsStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    .line 360
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic eval(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 354
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$7;->eval(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
