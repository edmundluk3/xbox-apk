.class public abstract Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;
.super Ljava/lang/Object;
.source "OOBEViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TimeZoneChangedIntent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/String;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;
    .locals 1
    .param p0, "timeZone"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 36
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_TimeZoneChangedIntent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_TimeZoneChangedIntent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract timeZone()Ljava/lang/String;
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
