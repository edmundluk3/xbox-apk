.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;
.super Ljava/lang/Object;
.source "UTCFeedback.java"


# static fields
.field private static PART_C_VERSION:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x1

    sput v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;->PART_C_VERSION:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 15
    sget v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;->PART_C_VERSION:I

    return v0
.end method

.method public static trackCancel()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$4;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$4;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 64
    return-void
.end method

.method public static trackDontAsk()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$3;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$3;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 52
    return-void
.end method

.method public static trackFeedback(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "rating"    # I
    .param p1, "ratingType"    # I
    .param p2, "pageName"    # Ljava/lang/String;
    .param p3, "logData"    # Ljava/lang/String;
    .param p4, "comment"    # Ljava/lang/String;

    .prologue
    .line 82
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;

    move v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$6;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 103
    return-void
.end method

.method public static trackRateApp()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$1;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 28
    return-void
.end method

.method public static trackRateAppPageView()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$5;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$5;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 76
    return-void
.end method

.method public static trackRemindMeLater()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback$2;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 40
    return-void
.end method
