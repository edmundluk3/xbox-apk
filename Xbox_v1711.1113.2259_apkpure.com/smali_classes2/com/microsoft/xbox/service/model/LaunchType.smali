.class public final enum Lcom/microsoft/xbox/service/model/LaunchType;
.super Ljava/lang/Enum;
.source "LaunchType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/LaunchType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/LaunchType;

.field public static final enum AppLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

.field public static final enum GameContentLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

.field public static final enum GameLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

.field public static final enum UnknownLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lcom/microsoft/xbox/service/model/LaunchType;

    const-string v1, "GameLaunchType"

    invoke-direct {v0, v1, v2, v2}, Lcom/microsoft/xbox/service/model/LaunchType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->GameLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    new-instance v0, Lcom/microsoft/xbox/service/model/LaunchType;

    const-string v1, "GameContentLaunchType"

    invoke-direct {v0, v1, v3, v3}, Lcom/microsoft/xbox/service/model/LaunchType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->GameContentLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    new-instance v0, Lcom/microsoft/xbox/service/model/LaunchType;

    const-string v1, "AppLaunchType"

    invoke-direct {v0, v1, v4, v4}, Lcom/microsoft/xbox/service/model/LaunchType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->AppLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    new-instance v0, Lcom/microsoft/xbox/service/model/LaunchType;

    const-string v1, "UnknownLaunchType"

    invoke-direct {v0, v1, v5, v5}, Lcom/microsoft/xbox/service/model/LaunchType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->UnknownLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    .line 4
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/LaunchType;

    sget-object v1, Lcom/microsoft/xbox/service/model/LaunchType;->GameLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/LaunchType;->GameContentLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/LaunchType;->AppLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/LaunchType;->UnknownLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->$VALUES:[Lcom/microsoft/xbox/service/model/LaunchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput p3, p0, Lcom/microsoft/xbox/service/model/LaunchType;->value:I

    .line 11
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/LaunchType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4
    const-class v0, Lcom/microsoft/xbox/service/model/LaunchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/LaunchType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/LaunchType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->$VALUES:[Lcom/microsoft/xbox/service/model/LaunchType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/LaunchType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/LaunchType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/microsoft/xbox/service/model/LaunchType;->value:I

    return v0
.end method
