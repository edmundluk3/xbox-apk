.class public final Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;
.super Ljava/lang/Object;
.source "ClubProfileDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClubProfileUpdateContract"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract$Builder;
    }
.end annotation


# instance fields
.field private final activityFeedEnabled:Z

.field private final backgroundImageUrl:Ljava/lang/String;

.field private final chatEnabled:Z

.field private final creationDateUtc:Ljava/util/Date;

.field private final description:Ljava/lang/String;

.field private final displayImageUrl:Ljava/lang/String;

.field private final founder:Ljava/lang/String;

.field private final isPromotedClub:Z

.field private final lfgEnabled:Z

.field private matureContentEnabled:Z

.field private final name:Ljava/lang/String;

.field private final preferredColor:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;

.field private final preferredLocale:Ljava/lang/String;

.field private final requestToJoinEnabled:Z

.field private final shortName:Ljava/lang/String;

.field private final tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final type:Ljava/lang/String;

.field private watchClubTitlesOnly:Z

.field private final whoCanChat:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private final whoCanCreateLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private final whoCanInvite:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private final whoCanJoinLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private final whoCanPostToFeed:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private final whoCanSetChatTopic:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private final whoCanViewChat:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private final whoCanViewFeed:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private final whoCanViewLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field private final whoCanViewRoster:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;ZZZZZLjava/lang/String;ZZLjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "founder"    # Ljava/lang/String;
    .param p5, "creationDateUtc"    # Ljava/util/Date;
    .param p6, "displayImageUrl"    # Ljava/lang/String;
    .param p7, "backgroundImageUrl"    # Ljava/lang/String;
    .param p8, "preferredColor"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;
    .param p9, "activityFeedEnabled"    # Z
    .param p10, "chatEnabled"    # Z
    .param p11, "lfgEnabled"    # Z
    .param p12, "matureContentEnabled"    # Z
    .param p13, "watchClubTitlesOnly"    # Z
    .param p14, "preferredLocale"    # Ljava/lang/String;
    .param p15, "requestToJoinEnabled"    # Z
    .param p16, "isPromotedClub"    # Z
    .param p17, "shortName"    # Ljava/lang/String;
    .param p20, "whoCanPostToFeed"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p21, "whoCanInvite"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p22, "whoCanChat"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p23, "whoCanCreateLfg"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p24, "whoCanJoinLfg"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p25, "whoCanViewRoster"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p26, "whoCanViewFeed"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p27, "whoCanViewLfg"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p28, "whoCanViewChat"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p29, "whoCanSetChatTopic"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;",
            "ZZZZZ",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;",
            ")V"
        }
    .end annotation

    .prologue
    .line 162
    .local p18, "titles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local p19, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->name:Ljava/lang/String;

    .line 164
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->type:Ljava/lang/String;

    .line 165
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->description:Ljava/lang/String;

    .line 166
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->founder:Ljava/lang/String;

    .line 167
    iput-object p5, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->creationDateUtc:Ljava/util/Date;

    .line 168
    iput-object p6, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->displayImageUrl:Ljava/lang/String;

    .line 169
    iput-object p7, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->backgroundImageUrl:Ljava/lang/String;

    .line 170
    iput-object p8, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->preferredColor:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;

    .line 171
    iput-boolean p9, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->activityFeedEnabled:Z

    .line 172
    iput-boolean p10, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->chatEnabled:Z

    .line 173
    iput-boolean p11, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->lfgEnabled:Z

    .line 174
    iput-boolean p12, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->matureContentEnabled:Z

    .line 175
    iput-boolean p13, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->watchClubTitlesOnly:Z

    .line 176
    iput-object p14, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->preferredLocale:Ljava/lang/String;

    .line 177
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->requestToJoinEnabled:Z

    .line 178
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->isPromotedClub:Z

    .line 179
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->shortName:Ljava/lang/String;

    .line 180
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->titles:Ljava/util/List;

    .line 181
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->tags:Ljava/util/List;

    .line 182
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->whoCanPostToFeed:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 183
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->whoCanInvite:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 184
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->whoCanChat:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 185
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->whoCanCreateLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 186
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->whoCanJoinLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 187
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->whoCanViewRoster:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 188
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->whoCanViewFeed:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 189
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->whoCanViewLfg:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 190
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->whoCanViewChat:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 191
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;->whoCanSetChatTopic:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 192
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;ZZZZZLjava/lang/String;ZZLjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/util/Date;
    .param p6, "x5"    # Ljava/lang/String;
    .param p7, "x6"    # Ljava/lang/String;
    .param p8, "x7"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;
    .param p9, "x8"    # Z
    .param p10, "x9"    # Z
    .param p11, "x10"    # Z
    .param p12, "x11"    # Z
    .param p13, "x12"    # Z
    .param p14, "x13"    # Ljava/lang/String;
    .param p15, "x14"    # Z
    .param p16, "x15"    # Z
    .param p17, "x16"    # Ljava/lang/String;
    .param p18, "x17"    # Ljava/util/List;
    .param p19, "x18"    # Ljava/util/List;
    .param p20, "x19"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p21, "x20"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p22, "x21"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p23, "x22"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p24, "x23"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p25, "x24"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p26, "x25"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p27, "x26"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p28, "x27"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p29, "x28"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .param p30, "x29"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$1;

    .prologue
    .line 102
    invoke-direct/range {p0 .. p29}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;ZZZZZLjava/lang/String;ZZLjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;)V

    return-void
.end method
