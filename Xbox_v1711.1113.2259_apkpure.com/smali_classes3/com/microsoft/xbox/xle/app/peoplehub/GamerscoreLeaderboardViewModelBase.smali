.class public abstract Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "GamerscoreLeaderboardViewModelBase.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final dateFilters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;",
            ">;"
        }
    .end annotation
.end field

.field private getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private final getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field protected final leaderboardList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;",
            ">;"
        }
    .end annotation
.end field

.field protected meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

.field protected model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

.field private selectedFilterIndex:I

.field protected selfMonthlyGamerscore:J

.field protected topMonthlyGamerscore:J

.field protected final userDataMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 57
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 70
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->dateFilters:Ljava/util/List;

    .line 72
    iput v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->selectedFilterIndex:I

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->userDataMap:Ljava/util/Map;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->leaderboardList:Ljava/util/List;

    .line 76
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->initializeDateFilters()V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->dateFilters:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;->getMonth()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getGamerscoreLeaderboardModel(Ljava/util/Calendar;)Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    .line 78
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->onUserDataLoadCompleted(Ljava/util/List;)V

    return-void
.end method

.method private cancelTasks()V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 237
    :cond_0
    return-void
.end method

.method private initializeDateFilters()V
    .locals 4

    .prologue
    .line 250
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getCurrentLeaderboardMonth()Ljava/util/Calendar;

    move-result-object v1

    .line 251
    .local v1, "thisMonth":Ljava/util/Calendar;
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getCurrentLeaderboardMonth()Ljava/util/Calendar;

    move-result-object v0

    .line 252
    .local v0, "lastMonth":Ljava/util/Calendar;
    const/4 v2, 0x2

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 254
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->dateFilters:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 255
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->dateFilters:Ljava/util/List;

    new-instance v3, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;

    invoke-direct {v3, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;-><init>(Ljava/util/Calendar;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->dateFilters:Ljava/util/List;

    new-instance v3, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;

    invoke-direct {v3, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;-><init>(Ljava/util/Calendar;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    return-void
.end method

.method private onUserDataLoadCompleted(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 260
    .local p1, "userData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onUserDataLoadCompleted "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p1, :cond_0

    const-string v1, "NULL"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    if-eqz p1, :cond_1

    .line 263
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 264
    .local v0, "user":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->userDataMap:Ljava/util/Map;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 260
    .end local v0    # "user":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 268
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->updateLeaderboard()V

    .line 269
    return-void
.end method

.method private setModel(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)V
    .locals 1
    .param p1, "newModel"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 244
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    .line 245
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 246
    return-void
.end method


# virtual methods
.method public getFilterItems()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->dateFilters:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLeaderboardList()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->leaderboardList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSelfMonthlyGamerscore()J
    .locals 2

    .prologue
    .line 143
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->selfMonthlyGamerscore:J

    return-wide v0
.end method

.method public getTopMonthlyGamerscore()J
    .locals 2

    .prologue
    .line 139
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->topMonthlyGamerscore:J

    return-wide v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public goToProfile(Ljava/lang/String;)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 167
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 169
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getIsLoading()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_1

    .line 104
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 107
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->cancelTasks()V

    .line 109
    if-eqz p1, :cond_3

    .line 110
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->userDataMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 112
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->leaderboardList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 113
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->topMonthlyGamerscore:J

    .line 114
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->selfMonthlyGamerscore:J

    .line 115
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->initializeDateFilters()V

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->dateFilters:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;

    .line 117
    .local v0, "month":Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;->getMonth()Ljava/util/Calendar;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getGamerscoreLeaderboardModel(Ljava/util/Calendar;)Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->refreshServerInstance()V

    goto :goto_0

    .line 119
    .end local v0    # "month":Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->resetXuidList()V

    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->dateFilters:Ljava/util/List;

    iget v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->selectedFilterIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;->getMonth()Ljava/util/Calendar;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getGamerscoreLeaderboardModel(Ljava/util/Calendar;)Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->setModel(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)V

    .line 123
    :cond_3
    if-nez p1, :cond_4

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->userDataMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 124
    :cond_4
    new-instance v1, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getLeaderboardXuids()Ljava/util/Collection;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 125
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    .line 127
    :cond_5
    return-void
.end method

.method public navigateToSendMessage(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 5
    .param p1, "friend"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 191
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 192
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/MultiSelection;->reset()V

    .line 193
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    new-instance v4, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-direct {v4, p1}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;-><init>(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 194
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/MultiSelection;->add(Ljava/lang/Object;)V

    .line 195
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 196
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 197
    .local v0, "currentScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMessagesOriginatingScreen(Ljava/lang/Class;)V

    .line 200
    :cond_0
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 205
    .end local v0    # "currentScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_0
    return-void

    .line 202
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->TAG:Ljava/lang/String;

    const-string v3, "User has no privilege to send message"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const v2, 0x7f07061c

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->showError(I)V

    goto :goto_0
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 83
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->cancelTasks()V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 89
    return-void
.end method

.method public setFilterIndex(I)V
    .locals 6
    .param p1, "index"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
            to = 0x1L
        .end annotation
    .end param

    .prologue
    const-wide/16 v0, 0x0

    .line 147
    const-wide/16 v2, 0x1

    int-to-long v4, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 149
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setFilterIndex: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iput p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->selectedFilterIndex:I

    .line 151
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->dateFilters:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;->getMonth()Ljava/util/Calendar;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getGamerscoreLeaderboardModel(Ljava/util/Calendar;)Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->setModel(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)V

    .line 152
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->leaderboardList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 153
    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->topMonthlyGamerscore:J

    .line 154
    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->selfMonthlyGamerscore:J

    .line 156
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->load(Z)V

    .line 158
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->updateAdapter()V

    .line 159
    return-void
.end method

.method public shareLeaderboardCompare(Ljava/lang/String;)V
    .locals 2
    .param p1, "friendXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 182
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 183
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->loadLeaderboardShareUriAsync(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :goto_0
    return-void

    .line 186
    :cond_0
    const v0, 0x7f070110

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->showError(I)V

    goto :goto_0
.end method

.method public shareMyLeaderboard()V
    .locals 3

    .prologue
    .line 173
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->model:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->loadLeaderboardShareUriAsync(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    const v0, 0x7f070110

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->showError(I)V

    goto :goto_0
.end method

.method protected abstract updateLeaderboard()V
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v3, 0x0

    .line 209
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    move-object v0, v2

    .line 210
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 211
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 230
    :cond_0
    :goto_1
    return-void

    .end local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :cond_1
    move-object v0, v3

    .line 209
    goto :goto_0

    .line 213
    .restart local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 214
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 215
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->updateAdapter()V

    goto :goto_1

    .line 217
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->updateLeaderboard()V

    goto :goto_1

    .line 221
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getExtra()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "leaderboardUri"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "uri":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 223
    :cond_3
    const v2, 0x7f070b6f

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->showError(I)V

    goto :goto_1

    .line 225
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Leaderboard:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-virtual {v2, p0, v1, v3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    goto :goto_1

    .line 211
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
