.class Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
.super Ljava/lang/Object;
.source "ContactsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/rt2zz/reactnativecontacts/ContactsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Contact"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;,
        Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;
    }
.end annotation


# instance fields
.field private company:Ljava/lang/String;

.field private contactId:Ljava/lang/String;

.field private department:Ljava/lang/String;

.field private displayName:Ljava/lang/String;

.field private emails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;",
            ">;"
        }
    .end annotation
.end field

.field private familyName:Ljava/lang/String;

.field private givenName:Ljava/lang/String;

.field private hasPhoto:Z

.field private jobTitle:Ljava/lang/String;

.field private middleName:Ljava/lang/String;

.field private phones:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;",
            ">;"
        }
    .end annotation
.end field

.field private photoUri:Ljava/lang/String;

.field private postalAddresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;",
            ">;"
        }
    .end annotation
.end field

.field private prefix:Ljava/lang/String;

.field private suffix:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "contactId"    # Ljava/lang/String;

    .prologue
    .line 297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    const-string v0, ""

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->givenName:Ljava/lang/String;

    .line 284
    const-string v0, ""

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->middleName:Ljava/lang/String;

    .line 285
    const-string v0, ""

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->familyName:Ljava/lang/String;

    .line 286
    const-string v0, ""

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->prefix:Ljava/lang/String;

    .line 287
    const-string v0, ""

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->suffix:Ljava/lang/String;

    .line 288
    const-string v0, ""

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->company:Ljava/lang/String;

    .line 289
    const-string v0, ""

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->jobTitle:Ljava/lang/String;

    .line 290
    const-string v0, ""

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->department:Ljava/lang/String;

    .line 291
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->hasPhoto:Z

    .line 293
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->emails:Ljava/util/List;

    .line 294
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->phones:Ljava/util/List;

    .line 295
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->postalAddresses:Ljava/util/List;

    .line 298
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->contactId:Ljava/lang/String;

    .line 299
    return-void
.end method

.method static synthetic access$100(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->emails:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$102(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->displayName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->company:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->jobTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->department:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->postalAddresses:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->photoUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->photoUri:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .param p1, "x1"    # Z

    .prologue
    .line 280
    iput-boolean p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->hasPhoto:Z

    return p1
.end method

.method static synthetic access$402(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->givenName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->middleName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->familyName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->prefix:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->suffix:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->phones:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public toMap()Lcom/facebook/react/bridge/WritableMap;
    .locals 9

    .prologue
    .line 302
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    .line 303
    .local v0, "contact":Lcom/facebook/react/bridge/WritableMap;
    const-string v6, "recordID"

    iget-object v7, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->contactId:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const-string v7, "givenName"

    iget-object v6, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->givenName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->displayName:Ljava/lang/String;

    :goto_0
    invoke-interface {v0, v7, v6}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const-string v6, "middleName"

    iget-object v7, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->middleName:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const-string v6, "familyName"

    iget-object v7, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->familyName:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string v6, "prefix"

    iget-object v7, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->prefix:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const-string v6, "suffix"

    iget-object v7, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->suffix:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const-string v6, "company"

    iget-object v7, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->company:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    const-string v6, "jobTitle"

    iget-object v7, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->jobTitle:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const-string v6, "department"

    iget-object v7, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->department:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string v6, "hasThumbnail"

    iget-boolean v7, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->hasPhoto:Z

    invoke-interface {v0, v6, v7}, Lcom/facebook/react/bridge/WritableMap;->putBoolean(Ljava/lang/String;Z)V

    .line 313
    const-string v7, "thumbnailPath"

    iget-object v6, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->photoUri:Ljava/lang/String;

    if-nez v6, :cond_1

    const-string v6, ""

    :goto_1
    invoke-interface {v0, v7, v6}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v4

    .line 316
    .local v4, "phoneNumbers":Lcom/facebook/react/bridge/WritableArray;
    iget-object v6, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->phones:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;

    .line 317
    .local v2, "item":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v3

    .line 318
    .local v3, "map":Lcom/facebook/react/bridge/WritableMap;
    const-string v7, "number"

    iget-object v8, v2, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;->value:Ljava/lang/String;

    invoke-interface {v3, v7, v8}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const-string v7, "label"

    iget-object v8, v2, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;->label:Ljava/lang/String;

    invoke-interface {v3, v7, v8}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    invoke-interface {v4, v3}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_2

    .line 304
    .end local v2    # "item":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;
    .end local v3    # "map":Lcom/facebook/react/bridge/WritableMap;
    .end local v4    # "phoneNumbers":Lcom/facebook/react/bridge/WritableArray;
    :cond_0
    iget-object v6, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->givenName:Ljava/lang/String;

    goto :goto_0

    .line 313
    :cond_1
    iget-object v6, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->photoUri:Ljava/lang/String;

    goto :goto_1

    .line 322
    .restart local v4    # "phoneNumbers":Lcom/facebook/react/bridge/WritableArray;
    :cond_2
    const-string v6, "phoneNumbers"

    invoke-interface {v0, v6, v4}, Lcom/facebook/react/bridge/WritableMap;->putArray(Ljava/lang/String;Lcom/facebook/react/bridge/WritableArray;)V

    .line 324
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v1

    .line 325
    .local v1, "emailAddresses":Lcom/facebook/react/bridge/WritableArray;
    iget-object v6, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->emails:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;

    .line 326
    .restart local v2    # "item":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v3

    .line 327
    .restart local v3    # "map":Lcom/facebook/react/bridge/WritableMap;
    const-string v7, "email"

    iget-object v8, v2, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;->value:Ljava/lang/String;

    invoke-interface {v3, v7, v8}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const-string v7, "label"

    iget-object v8, v2, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;->label:Ljava/lang/String;

    invoke-interface {v3, v7, v8}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    invoke-interface {v1, v3}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_3

    .line 331
    .end local v2    # "item":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;
    .end local v3    # "map":Lcom/facebook/react/bridge/WritableMap;
    :cond_3
    const-string v6, "emailAddresses"

    invoke-interface {v0, v6, v1}, Lcom/facebook/react/bridge/WritableMap;->putArray(Ljava/lang/String;Lcom/facebook/react/bridge/WritableArray;)V

    .line 333
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v5

    .line 334
    .local v5, "postalAddresses":Lcom/facebook/react/bridge/WritableArray;
    iget-object v6, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->postalAddresses:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;

    .line 335
    .local v2, "item":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;
    iget-object v7, v2, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->map:Lcom/facebook/react/bridge/WritableMap;

    invoke-interface {v5, v7}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_4

    .line 337
    .end local v2    # "item":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;
    :cond_4
    const-string v6, "postalAddresses"

    invoke-interface {v0, v6, v5}, Lcom/facebook/react/bridge/WritableMap;->putArray(Ljava/lang/String;Lcom/facebook/react/bridge/WritableArray;)V

    .line 339
    return-object v0
.end method
