.class public final Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;
.super Ljava/lang/Object;
.source "RepositoryModule_ProvideOOBESettingsRepositoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final dataMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

.field private final prefsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/repository/RepositoryModule;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p2, "prefsProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/SharedPreferences;>;"
    .local p3, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    .line 29
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->prefsProvider:Ljavax/inject/Provider;

    .line 31
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->dataMapperProvider:Ljavax/inject/Provider;

    .line 33
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/repository/RepositoryModule;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "prefsProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/SharedPreferences;>;"
    .local p2, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;>;"
    new-instance v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;-><init>(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideOOBESettingsRepository(Lcom/microsoft/xbox/data/repository/RepositoryModule;Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;)Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "dataMapper"    # Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;

    .prologue
    .line 56
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/data/repository/RepositoryModule;->provideOOBESettingsRepository(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;)Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .locals 3

    .prologue
    .line 37
    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->prefsProvider:Ljavax/inject/Provider;

    .line 38
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->dataMapperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;

    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/data/repository/RepositoryModule;->provideOOBESettingsRepository(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;)Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 37
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->get()Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    move-result-object v0

    return-object v0
.end method
