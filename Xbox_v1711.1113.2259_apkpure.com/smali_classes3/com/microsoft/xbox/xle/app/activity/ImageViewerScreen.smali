.class public Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ImageViewerScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(I)V

    .line 12
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    const-string v0, "Screenshot Gallery"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 16
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 17
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;->onCreateContentView()V

    .line 18
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 19
    return-void
.end method

.method public onCreateContentView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    const v0, 0x7f030153

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;->setContentView(I)V

    .line 29
    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;->setAppBarLayout(IZZ)V

    .line 30
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;->setShowUtilityBar(Z)V

    .line 31
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;->setShowRightPane(Z)V

    .line 32
    return-void
.end method
