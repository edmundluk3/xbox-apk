.class final Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent;
.super Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;
.source "AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final userTriggered:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0
    .param p1, "userTriggered"    # Z

    .prologue
    .line 13
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;-><init>()V

    .line 14
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent;->userTriggered:Z

    .line 15
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    if-ne p1, p0, :cond_1

    .line 38
    :cond_0
    :goto_0
    return v1

    .line 34
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;

    if-eqz v3, :cond_2

    move-object v0, p1

    .line 35
    check-cast v0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;

    .line 36
    .local v0, "that":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent<*>;"
    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent;->userTriggered:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;->userTriggered()Z

    move-result v4

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent<*>;"
    :cond_2
    move v1, v2

    .line 38
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 43
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent<TT;>;"
    const/4 v0, 0x1

    .line 44
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 45
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent;->userTriggered:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 46
    return v0

    .line 45
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AllChatHeadsRemovedEvent{userTriggered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent;->userTriggered:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userTriggered()Z
    .locals 1

    .prologue
    .line 19
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_AllChatHeadsRemovedEvent;->userTriggered:Z

    return v0
.end method
