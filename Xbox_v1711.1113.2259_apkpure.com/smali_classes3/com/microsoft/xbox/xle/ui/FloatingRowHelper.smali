.class public Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;
.super Ljava/lang/Object;
.source "FloatingRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;
    }
.end annotation


# instance fields
.field private firstVisibleItem:I

.field private firstVisibleItemHeight:F

.field private floatingHeaderVisible:Z

.field private final headerInfo:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;

.field private scrollY:F

.field private translation:F


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItem:I

    .line 17
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItemHeight:F

    .line 18
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->scrollY:F

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->headerInfo:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;

    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->headerInfo:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;->getHeader()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->floatingHeaderVisible:Z

    .line 24
    return-void
.end method

.method private computeDelta(Landroid/view/View;Landroid/view/View;IF)F
    .locals 5
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "firstView"    # Landroid/view/View;
    .param p3, "firstVisibleItem"    # I
    .param p4, "scrollY"    # F

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 103
    .local v0, "delta":F
    const/4 v1, 0x0

    .line 104
    .local v1, "dividerHeight":I
    instance-of v3, p1, Landroid/widget/AbsListView;

    if-eqz v3, :cond_0

    move-object v2, p1

    .line 105
    check-cast v2, Landroid/widget/ListView;

    .line 106
    .local v2, "lv":Landroid/widget/ListView;
    invoke-virtual {v2}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v1

    .line 110
    .end local v2    # "lv":Landroid/widget/ListView;
    :cond_0
    iget v3, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItem:I

    if-ge p3, v3, :cond_2

    .line 111
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v3, v1

    int-to-float v3, v3

    add-float/2addr v0, v3

    .line 115
    :cond_1
    :goto_0
    iget v3, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->scrollY:F

    sub-float v3, p4, v3

    add-float/2addr v0, v3

    .line 116
    return v0

    .line 112
    :cond_2
    iget v3, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItem:I

    if-le p3, v3, :cond_1

    .line 113
    iget v3, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItemHeight:F

    int-to-float v4, v1

    add-float/2addr v3, v4

    sub-float/2addr v0, v3

    goto :goto_0
.end method

.method private isHeaderVisible(ILandroid/view/View;)Z
    .locals 5
    .param p1, "firstVisibleItem"    # I
    .param p2, "firstChild"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 126
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->headerInfo:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;->getPosition()I

    move-result v0

    .line 127
    .local v0, "headerPosition":I
    if-ge p1, v0, :cond_1

    .line 133
    :cond_0
    :goto_0
    return v1

    .line 130
    :cond_1
    if-ne p1, v0, :cond_2

    if-eqz p2, :cond_2

    .line 131
    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->headerInfo:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;

    invoke-interface {v4}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;->getHeader()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    if-gt v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    move v1, v2

    .line 133
    goto :goto_0
.end method

.method private isScrollingDown(IF)Z
    .locals 1
    .param p1, "firstVisibleItem"    # I
    .param p2, "scrollY"    # F

    .prologue
    .line 98
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItem:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItem:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->scrollY:F

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isScrollingUp(IF)Z
    .locals 1
    .param p1, "firstVisibleItem"    # I
    .param p2, "scrollY"    # F

    .prologue
    .line 120
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItem:I

    if-gt p1, v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItem:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->scrollY:F

    cmpg-float v0, p2, v0

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onScrollHandler(Landroid/view/View;Landroid/view/View;I)V
    .locals 9
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "firstVisibleItem"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 53
    if-eqz p2, :cond_2

    .line 54
    invoke-virtual {p2}, Landroid/view/View;->getY()F

    move-result v3

    .line 55
    .local v3, "scrollY":F
    cmpl-float v5, v3, v6

    if-eqz v5, :cond_2

    .line 56
    invoke-direct {p0, p1, p2, p3, v3}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->computeDelta(Landroid/view/View;Landroid/view/View;IF)F

    move-result v0

    .line 57
    .local v0, "delta":F
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->headerInfo:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;

    invoke-interface {v5}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;->getHeader()Landroid/view/View;

    move-result-object v1

    .line 58
    .local v1, "floatingHeader":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v2, v5

    .line 59
    .local v2, "height":F
    invoke-direct {p0, p3, v3}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->isScrollingDown(IF)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 60
    invoke-direct {p0, p3, p2}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->isHeaderVisible(ILandroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 61
    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->showFloatingHeader(Z)V

    .line 62
    iput v6, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->translation:F

    .line 88
    :cond_0
    :goto_0
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItem:I

    if-eq p3, v5, :cond_1

    .line 89
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    iput v5, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItemHeight:F

    .line 90
    iput p3, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->firstVisibleItem:I

    .line 92
    :cond_1
    iput v3, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->scrollY:F

    .line 95
    .end local v0    # "delta":F
    .end local v1    # "floatingHeader":Landroid/view/View;
    .end local v2    # "height":F
    .end local v3    # "scrollY":F
    :cond_2
    return-void

    .line 64
    .restart local v0    # "delta":F
    .restart local v1    # "floatingHeader":Landroid/view/View;
    .restart local v2    # "height":F
    .restart local v3    # "scrollY":F
    :cond_3
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->translation:F

    sub-float/2addr v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 65
    .local v4, "translation":F
    cmpg-float v5, v6, v4

    if-gtz v5, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->isFloatingHeaderVisible()Z

    move-result v5

    if-nez v5, :cond_4

    .line 67
    invoke-direct {p0, v8}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->showFloatingHeader(Z)V

    .line 69
    :cond_4
    neg-float v5, v4

    invoke-virtual {v1, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 70
    iput v4, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->translation:F

    goto :goto_0

    .line 73
    .end local v4    # "translation":F
    :cond_5
    invoke-direct {p0, p3, v3}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->isScrollingUp(IF)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 74
    invoke-direct {p0, p3, p2}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->isHeaderVisible(ILandroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 75
    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->showFloatingHeader(Z)V

    goto :goto_0

    .line 77
    :cond_6
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->translation:F

    sub-float/2addr v5, v0

    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 78
    .restart local v4    # "translation":F
    cmpg-float v5, v4, v2

    if-gez v5, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->isFloatingHeaderVisible()Z

    move-result v5

    if-nez v5, :cond_7

    .line 80
    invoke-direct {p0, v8}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->showFloatingHeader(Z)V

    .line 82
    :cond_7
    neg-float v5, v4

    invoke-virtual {v1, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 83
    iput v4, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->translation:F

    goto :goto_0
.end method

.method private showFloatingHeader(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->headerInfo:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;->getHeader()Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->headerInfo:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 138
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->floatingHeaderVisible:Z

    .line 139
    return-void

    .line 137
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public getHeaderPosition()I
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->headerInfo:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;->getPosition()I

    move-result v0

    return v0
.end method

.method public isFloatingHeaderVisible()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->floatingHeaderVisible:Z

    return v0
.end method

.method public onScroll(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 5
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 32
    if-eqz p1, :cond_0

    .line 33
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v2

    .line 34
    .local v2, "lm":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    instance-of v4, v2, Landroid/support/v7/widget/LinearLayoutManager;

    if-eqz v4, :cond_0

    move-object v1, v2

    .line 35
    check-cast v1, Landroid/support/v7/widget/LinearLayoutManager;

    .line 36
    .local v1, "llm":Landroid/support/v7/widget/LinearLayoutManager;
    if-eqz v1, :cond_0

    .line 37
    invoke-virtual {v1}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v0

    .line 38
    .local v0, "firstVisibleItem":I
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v3

    .line 39
    .local v3, "v":Landroid/view/View;
    invoke-direct {p0, p1, v3, v0}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->onScrollHandler(Landroid/view/View;Landroid/view/View;I)V

    .line 43
    .end local v0    # "firstVisibleItem":I
    .end local v1    # "llm":Landroid/support/v7/widget/LinearLayoutManager;
    .end local v2    # "lm":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    .end local v3    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2
    .param p1, "absListView"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 47
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 48
    .local v0, "v":Landroid/view/View;
    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->onScrollHandler(Landroid/view/View;Landroid/view/View;I)V

    .line 49
    return-void
.end method
