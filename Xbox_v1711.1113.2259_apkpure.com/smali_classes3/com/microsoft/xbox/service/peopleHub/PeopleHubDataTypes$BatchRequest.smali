.class public abstract Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;
.super Ljava/lang/Object;
.source "PeopleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BatchRequest"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_BatchRequest$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_BatchRequest$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/util/Collection;)Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;
    .locals 2
    .param p0    # Ljava/util/Collection;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "xuids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_BatchRequest;

    invoke-static {p0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_BatchRequest;-><init>(Lcom/google/common/collect/ImmutableList;)V

    return-object v0
.end method


# virtual methods
.method public abstract xuids()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end method
