.class Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "MovieOverviewScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadActivitiesTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$1;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;)V

    return-void
.end method

.method private onLoadActivitiesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;Z)Z

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getActivitiesList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->isLoadingDetail:Z

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->updateAdapter()V

    .line 157
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    if-nez v0, :cond_0

    .line 105
    :goto_0
    return v1

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->shouldRefresh()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->shouldRefresh()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 6

    .prologue
    .line 132
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->forceLoad:Z

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    .line 134
    .local v3, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 135
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 145
    :goto_0
    return-object v4

    .line 138
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, "mediaId":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget-object v2, v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->MediaItemType:Ljava/lang/String;

    .line 140
    .local v2, "mediaType":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getMediaGroup()I

    move-result v0

    .line 141
    .local v0, "mediaGroup":I
    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getModel(Ljava/lang/String;Ljava/lang/String;I)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .line 142
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    if-eqz v4, :cond_1

    .line 143
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->forceLoad:Z

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v4

    goto :goto_0

    .line 145
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->onLoadActivitiesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 112
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->onLoadActivitiesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 123
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 97
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->isLoadingDetail:Z

    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;->updateAdapter()V

    .line 118
    return-void
.end method
