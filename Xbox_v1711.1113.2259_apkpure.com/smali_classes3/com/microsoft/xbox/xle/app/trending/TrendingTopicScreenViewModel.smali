.class public Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
.source "TrendingTopicScreenViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final TrendingFeedMaxItems:I = 0x14


# instance fields
.field private trendingModel:Lcom/microsoft/xbox/service/model/TrendingModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 32
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 33
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTrendingTopic()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    move-result-object v1

    .line 34
    .local v1, "trendingTopic":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 36
    const/16 v2, 0x14

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->sourceId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/TrendingModel;->newTopicInstance(ILjava/lang/String;)Lcom/microsoft/xbox/service/model/TrendingModel;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->trendingModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    .line 37
    return-void
.end method


# virtual methods
.method protected getActivityFeedData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->trendingModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v0

    .line 42
    .local v0, "responseList":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->getItemsAsProfileRecentItems()Ljava/util/ArrayList;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method protected getContinuationToken()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->trendingModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->trendingModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    .line 99
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->description()Ljava/lang/String;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    .line 99
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getNoContentText()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 83
    const-string v0, ""

    return-object v0
.end method

.method protected getScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 72
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTrendingTopicScreenAdapter(Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->trendingModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->trendingModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    .line 93
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->title()Ljava/lang/String;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    .line 93
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public isStatusPostEnabled()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method protected loadActivityFeedData(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3
    .param p1, "forceLoad"    # Z
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 60
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->trendingModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/TrendingModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 62
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    sget-object v1, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Unable to get trending topic data"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_0
    return-object v0
.end method

.method protected shouldReloadActivityFeedData()Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->trendingModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->trendingModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->shouldRefresh()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showActivityAlertNotificationBar()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method
