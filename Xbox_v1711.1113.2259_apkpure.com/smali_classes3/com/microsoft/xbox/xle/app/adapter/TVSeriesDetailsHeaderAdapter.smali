.class public Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "TVSeriesDetailsHeaderAdapter.java"


# instance fields
.field private tvSeriesBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private tvSeriesExtraTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private tvSeriesMetacriticRatingView:Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;

.field private tvSeriesRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

.field private tvSeriesReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private tvSeriesTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private tvSeriesTitleSmallTextView:Landroid/widget/TextView;

.field private tvSeriesTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    .line 32
    const v0, 0x7f0e0b40

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->screenBody:Landroid/view/View;

    .line 33
    const v0, 0x7f0e04fe

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 34
    const v0, 0x7f0e0b41

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 35
    const v0, 0x7f0e0b45

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 36
    const v0, 0x7f0e0b42

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    .line 37
    const v0, 0x7f0e0b43

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesMetacriticRatingView:Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;

    .line 38
    const v0, 0x7f0e0b44

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 39
    const v0, 0x7f0e0b46

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesExtraTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 40
    const v0, 0x7f0e0b47

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesTitleSmallTextView:Landroid/widget/TextView;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;)Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    return-object v0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    :cond_1
    return-void
.end method

.method public updateViewOverride()V
    .locals 5

    .prologue
    const v2, 0x7f0201f7

    const/4 v4, 0x0

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesTitleSmallTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v3, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getAverageUserRating()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setAverageUserRating(F)V

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setVisibility(I)V

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesMetacriticRatingView:Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;

    if-eqz v0, :cond_2

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getMetacriticReviewScore()I

    move-result v0

    if-lez v0, :cond_3

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesMetacriticRatingView:Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getMetacriticReviewScore()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;->setRating(I)V

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesMetacriticRatingView:Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;->setVisibility(I)V

    .line 96
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getTVSeriesReleaseData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesExtraTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getTVSeriesExtraData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 98
    return-void

    .line 92
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;->tvSeriesMetacriticRatingView:Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;->setVisibility(I)V

    goto :goto_0
.end method
