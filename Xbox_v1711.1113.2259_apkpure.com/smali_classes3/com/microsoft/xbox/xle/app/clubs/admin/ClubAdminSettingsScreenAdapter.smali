.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
.super Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;
.source "ClubAdminSettingsScreenAdapter.java"


# instance fields
.field private final broadcastMatureContentEnabledCheckbox:Landroid/widget/CheckBox;

.field private final broadcastMatureContentEnabledLoading:Landroid/view/View;

.field private final broadcastWatchClubTitlesOnlyCheckbox:Landroid/widget/CheckBox;

.field private final broadcastWatchClubTitlesOnlyLoading:Landroid/view/View;

.field private final chatLoading:Landroid/view/View;

.field private final chatSpinner:Landroid/widget/Spinner;

.field private final deleteButton:Landroid/view/View;

.field private final feedLoading:Landroid/view/View;

.field private final feedSpinner:Landroid/widget/Spinner;

.field private final inviteLoading:Landroid/view/View;

.field private final inviteSpinner:Landroid/widget/Spinner;

.field private final lfgCheckbox:Landroid/widget/CheckBox;

.field private final lfgJoinDescription:Landroid/widget/TextView;

.field private final lfgJoinLoading:Landroid/view/View;

.field private final lfgLoading:Landroid/view/View;

.field private final lfgSpinner:Landroid/widget/Spinner;

.field private final membershipCheckbox:Landroid/widget/CheckBox;

.field private final membershipLoading:Landroid/view/View;

.field private final privacyDescription:Landroid/widget/TextView;

.field private final privacyHeader:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private final setToDefaultButton:Landroid/view/View;

.field private final transferButton:Landroid/view/View;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V
    .locals 3
    .param p1, "clubViewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;)V

    .line 48
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    .line 51
    const v0, 0x7f0e029c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->setToDefaultButton:Landroid/view/View;

    .line 52
    const v0, 0x7f0e029a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->privacyHeader:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->privacyHeader:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconFontSize(I)V

    .line 54
    const v0, 0x7f0e029b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->privacyDescription:Landroid/widget/TextView;

    .line 55
    const v0, 0x7f0e029e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->membershipCheckbox:Landroid/widget/CheckBox;

    .line 56
    const v0, 0x7f0e029f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->membershipLoading:Landroid/view/View;

    .line 57
    const v0, 0x7f0e02a0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->inviteSpinner:Landroid/widget/Spinner;

    .line 58
    const v0, 0x7f0e02a1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->inviteLoading:Landroid/view/View;

    .line 59
    const v0, 0x7f0e02a2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->feedSpinner:Landroid/widget/Spinner;

    .line 60
    const v0, 0x7f0e02a3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->feedLoading:Landroid/view/View;

    .line 61
    const v0, 0x7f0e02a7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgCheckbox:Landroid/widget/CheckBox;

    .line 62
    const v0, 0x7f0e02a8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgJoinLoading:Landroid/view/View;

    .line 63
    const v0, 0x7f0e02a5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgSpinner:Landroid/widget/Spinner;

    .line 64
    const v0, 0x7f0e02a6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgLoading:Landroid/view/View;

    .line 65
    const v0, 0x7f0e02a9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgJoinDescription:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0e02aa

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->chatSpinner:Landroid/widget/Spinner;

    .line 67
    const v0, 0x7f0e02ab

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->chatLoading:Landroid/view/View;

    .line 68
    const v0, 0x7f0e02ac

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastMatureContentEnabledCheckbox:Landroid/widget/CheckBox;

    .line 69
    const v0, 0x7f0e02ad

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastMatureContentEnabledLoading:Landroid/view/View;

    .line 70
    const v0, 0x7f0e02ae

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastWatchClubTitlesOnlyCheckbox:Landroid/widget/CheckBox;

    .line 71
    const v0, 0x7f0e02af

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastWatchClubTitlesOnlyLoading:Landroid/view/View;

    .line 72
    const v0, 0x7f0e02b0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->transferButton:Landroid/view/View;

    .line 73
    const v0, 0x7f0e02b1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->deleteButton:Landroid/view/View;

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->setToDefaultButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->transferButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->deleteButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->transferButton:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->canTransferClub()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->deleteButton:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->canDeleteClub()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->membershipCheckbox:Landroid/widget/CheckBox;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0702b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->membershipCheckbox:Landroid/widget/CheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgCheckbox:Landroid/widget/CheckBox;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0702b8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgCheckbox:Landroid/widget/CheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->inviteSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->feedSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->chatSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastMatureContentEnabledCheckbox:Landroid/widget/CheckBox;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0702b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastMatureContentEnabledCheckbox:Landroid/widget/CheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastWatchClubTitlesOnlyCheckbox:Landroid/widget/CheckBox;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0702b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastWatchClubTitlesOnlyCheckbox:Landroid/widget/CheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    return-void
.end method

.method private enableControls(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->setToDefaultButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->inviteSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->feedSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->chatSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->membershipCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastMatureContentEnabledCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastWatchClubTitlesOnlyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 212
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->transferButton:Landroid/view/View;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->enableTransfer()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->deleteButton:Landroid/view/View;

    if-eqz p1, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->enableDelete()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 214
    return-void

    :cond_0
    move v0, v2

    .line 212
    goto :goto_0

    :cond_1
    move v1, v2

    .line 213
    goto :goto_1
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 76
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->enableControls(Z)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->setToDefault()V

    .line 79
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->transferClub()V

    return-void
.end method

.method static synthetic lambda$new$10(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastWatchClubTitlesOnlyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->setWatchClubTitlesOnly(Z)Z

    move-result v0

    .line 139
    .local v0, "changed":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastWatchClubTitlesOnlyCheckbox:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastWatchClubTitlesOnlyLoading:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 141
    return-void

    .line 139
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->deleteClub()V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->membershipCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->membershipEnableJoin(Z)Z

    move-result v0

    .line 90
    .local v0, "changed":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->membershipCheckbox:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->membershipLoading:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 92
    return-void

    .line 90
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 96
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgEnableJoin(Z)Z

    move-result v0

    .line 97
    .local v0, "changed":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgCheckbox:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgJoinLoading:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 99
    return-void

    .line 97
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->inviteOptionChanged(I)Z

    move-result v0

    .line 103
    .local v0, "optionChanged":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->inviteLoading:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 104
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->inviteSpinner:Landroid/widget/Spinner;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 105
    invoke-virtual {p1, v2}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 106
    return-void

    :cond_0
    move v1, v2

    .line 104
    goto :goto_0
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 109
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->feedOptionChanged(I)Z

    move-result v0

    .line 110
    .local v0, "optionChanged":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->feedLoading:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 111
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->feedSpinner:Landroid/widget/Spinner;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 112
    invoke-virtual {p1, v2}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 113
    return-void

    :cond_0
    move v1, v2

    .line 111
    goto :goto_0
.end method

.method static synthetic lambda$new$7(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgOptionChanged(I)Z

    move-result v0

    .line 117
    .local v0, "optionChanged":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgLoading:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 118
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgSpinner:Landroid/widget/Spinner;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 119
    invoke-virtual {p1, v2}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 120
    return-void

    :cond_0
    move v1, v2

    .line 118
    goto :goto_0
.end method

.method static synthetic lambda$new$8(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 123
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->chatOptionChanged(I)Z

    move-result v0

    .line 124
    .local v0, "optionChanged":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->chatLoading:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 125
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->chatSpinner:Landroid/widget/Spinner;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 126
    invoke-virtual {p1, v2}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 127
    return-void

    :cond_0
    move v1, v2

    .line 125
    goto :goto_0
.end method

.method static synthetic lambda$new$9(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 131
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastMatureContentEnabledCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->setMatureContentEnabled(Z)Z

    move-result v0

    .line 132
    .local v0, "changed":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastMatureContentEnabledCheckbox:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastMatureContentEnabledLoading:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 134
    return-void

    .line 132
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onStart()V
    .locals 8

    .prologue
    const v7, 0x7f03020a

    .line 146
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->onStart()V

    .line 148
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 149
    .local v0, "c":Landroid/content/Context;
    const v5, 0x1090008

    .line 151
    .local v5, "resId":I
    new-instance v3, Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getInviteOptions()Ljava/util/List;

    move-result-object v6

    invoke-direct {v3, v0, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 152
    .local v3, "inviteSpinnerAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v3, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 153
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->inviteSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 155
    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getFeedOptions()Ljava/util/List;

    move-result-object v6

    invoke-direct {v2, v0, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 156
    .local v2, "feedSpinnerAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v2, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 157
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->feedSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 159
    new-instance v4, Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getLfgOptions()Ljava/util/List;

    move-result-object v6

    invoke-direct {v4, v0, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 160
    .local v4, "lfgSpinnerAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v4, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 161
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 163
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getChatOptions()Ljava/util/List;

    move-result-object v6

    invoke-direct {v1, v0, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 164
    .local v1, "chatSpinnerAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v1, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 165
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->chatSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 166
    return-void
.end method

.method protected updateViewOverride()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 170
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->updateViewOverride()V

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->privacyHeader:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getPrivacyHeader()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->privacyHeader:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getPrivacyHeaderIcon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->privacyDescription:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getPrivacyDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgJoinDescription:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getLfgJoinDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->membershipCheckbox:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isMembershipEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgCheckbox:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isLfgEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->inviteSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getSelectedInviteOption()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->feedSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getSelectedFeedOption()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getSelectedLfgOption()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->chatSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getSelectedChatOption()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastMatureContentEnabledCheckbox:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isMatureContentEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastWatchClubTitlesOnlyCheckbox:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isWatchClubTitlesOnly()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 189
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->enableControls(Z)V

    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->inviteLoading:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->feedLoading:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgLoading:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastMatureContentEnabledLoading:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->broadcastWatchClubTitlesOnlyLoading:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->chatLoading:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->membershipLoading:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;->lfgJoinLoading:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 200
    :cond_0
    return-void
.end method
