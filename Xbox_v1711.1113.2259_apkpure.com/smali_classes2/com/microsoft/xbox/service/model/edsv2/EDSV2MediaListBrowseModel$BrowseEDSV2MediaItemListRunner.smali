.class Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "EDSV2MediaListBrowseModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BrowseEDSV2MediaItemListRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<TChildT;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;)V
    .locals 0

    .prologue
    .line 164
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>.BrowseEDSV2MediaItemListRunner;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$1;

    .prologue
    .line 164
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>.BrowseEDSV2MediaItemListRunner;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 164
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>.BrowseEDSV2MediaItemListRunner;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<TChildT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>.BrowseEDSV2MediaItemListRunner;"
    const/4 v4, 0x0

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 173
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getEDSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    .line 174
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getDesiredMediaItemTypeString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getParentMediaType()I

    move-result v3

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getOrderBy()Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    move-result-object v5

    const/4 v7, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getMaxCount()I

    move-result v8

    move-object v6, v4

    invoke-interface/range {v0 .. v8}, Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;->browseMediaItemList(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v0

    .line 173
    return-object v0

    .line 177
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0xfaf

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 187
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>.BrowseEDSV2MediaItemListRunner;"
    const-wide/16 v0, 0xfaf

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TChildT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 182
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>.BrowseEDSV2MediaItemListRunner;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<TChildT;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->onBrowseMediaItemListCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 183
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 167
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>.BrowseEDSV2MediaItemListRunner;"
    return-void
.end method
