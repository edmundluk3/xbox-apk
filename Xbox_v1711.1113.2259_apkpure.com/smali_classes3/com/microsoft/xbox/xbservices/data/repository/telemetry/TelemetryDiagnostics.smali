.class public final enum Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;
.super Ljava/lang/Enum;
.source "TelemetryDiagnostics.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatAudioDeviceChange:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatBroadcasting:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatDataChannelConnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatJoined:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatKick:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatLeft:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatMessageReceived:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatPing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatRemoved:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatSendDataChannelMessage:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatSendInvite:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatStarted:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatWebRTCConnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatWebRTCDisconnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatWebRTCFailed:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatWebSocketClose:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

.field public static final enum PartyChatWebSocketOpen:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;


# instance fields
.field private description:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 11
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatStarted"

    const-string v2, "Party - Started party"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatStarted:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 12
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatSendInvite"

    const-string v2, "Party - Send invite to user"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatSendInvite:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 13
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatJoined"

    const-string v2, "Party - User joined Party"

    invoke-direct {v0, v1, v6, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatJoined:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatLeft"

    const-string v2, "Party - User left party"

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatLeft:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatRemoved"

    const-string v2, "Party - User was removed from party"

    invoke-direct {v0, v1, v8, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatRemoved:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatBroadcasting"

    const/4 v2, 0x5

    const-string v3, "Party - User is broadcasting audio"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatBroadcasting:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatPing"

    const/4 v2, 0x6

    const-string v3, "Party - Ping"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatPing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatKick"

    const/4 v2, 0x7

    const-string v3, "Party - Kick user"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatKick:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatDataChannelConnected"

    const/16 v2, 0x8

    const-string v3, "Party - DataChannelConnected"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatDataChannelConnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatMessageReceived"

    const/16 v2, 0x9

    const-string v3, "Party - Chat message received"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatMessageReceived:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatWebRTCConnected"

    const/16 v2, 0xa

    const-string v3, "Party - WebRtc connected"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebRTCConnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatWebRTCDisconnected"

    const/16 v2, 0xb

    const-string v3, "Party - WebRtc disconnected"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebRTCDisconnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatWebRTCFailed"

    const/16 v2, 0xc

    const-string v3, "Party - WebRtc failed"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebRTCFailed:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatSendDataChannelMessage"

    const/16 v2, 0xd

    const-string v3, "Party - Send data channel message"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatSendDataChannelMessage:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatWebSocketOpen"

    const/16 v2, 0xe

    const-string v3, "Party - WebSocket open"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebSocketOpen:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatWebSocketClose"

    const/16 v2, 0xf

    const-string v3, "Party - WebSocket close"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebSocketClose:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 27
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    const-string v1, "PartyChatAudioDeviceChange"

    const/16 v2, 0x10

    const-string v3, "Party - Audio device changed"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatAudioDeviceChange:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    .line 10
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatStarted:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatSendInvite:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatJoined:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatLeft:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatRemoved:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatBroadcasting:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatPing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatKick:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatDataChannelConnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatMessageReceived:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebRTCConnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebRTCDisconnected:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebRTCFailed:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatSendDataChannelMessage:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebSocketOpen:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebSocketClose:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatAudioDeviceChange:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->$VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "description"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->description:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->description:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->$VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    return-object v0
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->description:Ljava/lang/String;

    return-object v0
.end method
