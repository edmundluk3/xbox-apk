.class public Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PhoneContactsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ShortCircuitProfileAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PhoneContactsManager"


# instance fields
.field private task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

.field final synthetic this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;
    .param p2, "task"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    .prologue
    .line 414
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 415
    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    .line 416
    return-void
.end method

.method private addProfile()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .locals 9

    .prologue
    .line 499
    const/4 v3, 0x0

    .line 501
    .local v3, "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getRegion()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getCountryCode()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 502
    .local v0, "country":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 504
    .local v2, "name":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    sget-object v7, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->AddProfileViaVoice:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    if-ne v6, v7, :cond_0

    const/4 v5, 0x1

    .line 505
    .local v5, "viaVoice":Z
    :goto_0
    new-instance v4, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;

    sget-object v6, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$MsgType;->Add:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$MsgType;

    invoke-direct {v4, v6, v2, v0, v5}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;-><init>(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$MsgType;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 507
    .end local v3    # "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    .local v4, "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v6

    invoke-interface {v6, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->sendShortCircuitProfile(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    move-object v3, v4

    .line 512
    .end local v0    # "country":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v4    # "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    .end local v5    # "viaVoice":Z
    .restart local v3    # "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    :goto_1
    return-object v6

    .line 504
    .restart local v0    # "country":Ljava/lang/String;
    .restart local v2    # "name":Ljava/lang/String;
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 508
    .end local v0    # "country":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 509
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :goto_2
    const-string v6, "PhoneContactsManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to add phone profile - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    const/4 v6, 0x0

    goto :goto_1

    .line 508
    .end local v1    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v3    # "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    .restart local v0    # "country":Ljava/lang/String;
    .restart local v2    # "name":Ljava/lang/String;
    .restart local v4    # "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    .restart local v5    # "viaVoice":Z
    :catch_1
    move-exception v1

    move-object v3, v4

    .end local v4    # "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    .restart local v3    # "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    goto :goto_2
.end method

.method private batchUploadContacts(Ljava/util/ArrayList;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 569
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 570
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v6, 0x64

    if-le v3, v6, :cond_1

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 572
    const/4 v2, 0x0

    .line 574
    .local v2, "startUploadIndex":I
    const/4 v1, 0x1

    .line 576
    .local v1, "moreContactsToUpload":Z
    :goto_1
    if-eqz v1, :cond_3

    .line 577
    add-int/lit8 v0, v2, 0x64

    .line 579
    .local v0, "endUploadIndex":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 580
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 581
    const/4 v1, 0x0

    .line 584
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p1, v2, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->uploadContacts(Ljava/util/ArrayList;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 591
    .end local v0    # "endUploadIndex":I
    :goto_2
    return v5

    .end local v1    # "moreContactsToUpload":Z
    .end local v2    # "startUploadIndex":I
    :cond_1
    move v3, v5

    .line 570
    goto :goto_0

    .line 588
    .restart local v0    # "endUploadIndex":I
    .restart local v1    # "moreContactsToUpload":Z
    .restart local v2    # "startUploadIndex":I
    :cond_2
    move v2, v0

    goto :goto_1

    .end local v0    # "endUploadIndex":I
    :cond_3
    move v5, v4

    .line 591
    goto :goto_2
.end method

.method private deleteProfile()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .locals 8

    .prologue
    .line 517
    :try_start_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getPhoneState()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;

    move-result-object v3

    .line 518
    .local v3, "phoneState":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;
    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    sget-object v6, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->DeleteProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    if-eq v5, v6, :cond_0

    if-eqz v3, :cond_1

    iget-boolean v5, v3, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;->isVerified:Z

    if-nez v5, :cond_1

    .line 519
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getRegion()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getCountryCode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 520
    .local v0, "country":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 521
    .local v2, "name":Ljava/lang/String;
    new-instance v4, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;

    sget-object v5, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$MsgType;->Delete:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$MsgType;

    invoke-direct {v4, v5, v2, v0}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;-><init>(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$MsgType;Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    .local v4, "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    invoke-interface {v5, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->sendShortCircuitProfile(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 529
    .end local v0    # "country":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "phoneState":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;
    .end local v4    # "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    :goto_0
    return-object v5

    .line 525
    :catch_0
    move-exception v1

    .line 526
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v5, "PhoneContactsManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to remove phone profile - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    .end local v1    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private reloadProfile()V
    .locals 5

    .prologue
    .line 615
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$002(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    .line 617
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getMyShortCircuitProfile()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    move-result-object v1

    .line 618
    .local v1, "profile":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    if-eqz v1, :cond_0

    .line 619
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-static {v2, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$002(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 624
    .end local v1    # "profile":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :cond_0
    :goto_0
    return-void

    .line 621
    :catch_0
    move-exception v0

    .line 622
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v2, "PhoneContactsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to load phone profile - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private uploadContacts(Ljava/util/ArrayList;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 595
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 596
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v6, 0x64

    if-gt v3, v6, :cond_0

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 598
    new-instance v1, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;

    .line 600
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getFullPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p1, v3}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 602
    .local v1, "uploadContactsRequest":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->updatePhoneContacts(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;

    move-result-object v2

    .line 604
    .local v2, "uploadContactsResponse":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;
    if-eqz v2, :cond_1

    iget-boolean v3, v2, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;->isErrorResponse:Z

    if-nez v3, :cond_1

    .line 605
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;->getXboxPhoneContacts()Ljava/util/Set;

    move-result-object v0

    .line 606
    .local v0, "aliases":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->updateXboxContacts(Ljava/util/Set;)V

    .line 607
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setPhoneContactsUploadTimestamp()V

    .line 611
    .end local v0    # "aliases":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_1
    return v4

    .end local v1    # "uploadContactsRequest":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;
    .end local v2    # "uploadContactsResponse":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;
    :cond_0
    move v3, v5

    .line 596
    goto :goto_0

    .restart local v1    # "uploadContactsRequest":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;
    .restart local v2    # "uploadContactsResponse":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;
    :cond_1
    move v4, v5

    .line 611
    goto :goto_1
.end method

.method private uploadContactsSucceeded()Z
    .locals 5

    .prologue
    .line 549
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getContacts()Ljava/util/ArrayList;

    move-result-object v0

    .line 552
    .local v0, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;>;"
    if-eqz v0, :cond_2

    .line 553
    :try_start_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 554
    const/4 v2, 0x1

    .line 565
    :goto_0
    return v2

    .line 555
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x64

    if-le v2, v3, :cond_1

    .line 556
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->batchUploadContacts(Ljava/util/ArrayList;)Z

    move-result v2

    goto :goto_0

    .line 558
    :cond_1
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->uploadContacts(Ljava/util/ArrayList;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 561
    :catch_0
    move-exception v1

    .line 562
    .local v1, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v2, "PhoneContactsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Upload contacts failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    .end local v1    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private verifyPhone()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .locals 7

    .prologue
    .line 534
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getRegion()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getCountryCode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 535
    .local v0, "country":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 537
    .local v2, "name":Ljava/lang/String;
    new-instance v3, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;

    sget-object v4, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$MsgType;->PhoneVerification:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$MsgType;

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$100(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v2, v0, v5}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;-><init>(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$MsgType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    .local v3, "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->sendShortCircuitProfile(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 545
    .end local v0    # "country":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "request":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    :goto_0
    return-object v4

    .line 541
    :catch_0
    move-exception v1

    .line 542
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v4, "PhoneContactsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to verify phone number - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 7

    .prologue
    .line 435
    :try_start_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-static {v4}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$000(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    sget-object v5, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->GetProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    if-ne v4, v5, :cond_2

    .line 436
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getMyShortCircuitProfile()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    move-result-object v1

    .line 437
    .local v1, "profile":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    if-eqz v1, :cond_1

    .line 438
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$002(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    .line 440
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    sget-object v5, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->GetProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    if-ne v4, v5, :cond_2

    .line 441
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 486
    .end local v1    # "profile":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :goto_0
    return-object v4

    .line 444
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    sget-object v5, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->AddProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    if-eq v4, v5, :cond_3

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    sget-object v5, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->AddProfileViaVoice:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    if-ne v4, v5, :cond_7

    .line 446
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->addProfile()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    move-result-object v3

    .line 447
    .local v3, "response":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    if-eqz v3, :cond_5

    iget-object v4, v3, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;->error:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ErrorReturn;

    if-eqz v4, :cond_5

    .line 448
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->deleteProfile()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    move-result-object v2

    .line 449
    .local v2, "res":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    if-eqz v2, :cond_4

    iget-object v4, v2, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;->error:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ErrorReturn;

    if-eqz v4, :cond_4

    .line 450
    const-string v4, "PhoneContactsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to delete profile: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 453
    :cond_4
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->reloadProfile()V

    .line 454
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->addProfile()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    move-result-object v2

    .line 455
    if-eqz v2, :cond_6

    iget-object v4, v2, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;->error:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ErrorReturn;

    if-eqz v4, :cond_6

    .line 456
    const-string v4, "PhoneContactsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to add profile: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 460
    .end local v2    # "res":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :cond_5
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->reloadProfile()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 486
    .end local v3    # "response":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :cond_6
    :goto_1
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 462
    :cond_7
    :try_start_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    sget-object v5, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->DeleteProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    if-ne v4, v5, :cond_8

    .line 463
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->deleteProfile()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 483
    :catch_0
    move-exception v0

    .line 484
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 464
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_8
    :try_start_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    sget-object v5, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UpdateProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    if-ne v4, v5, :cond_b

    .line 465
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->refreshContacts()V

    .line 466
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->verifyPhone()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    move-result-object v3

    .line 467
    .restart local v3    # "response":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    if-eqz v3, :cond_9

    iget-object v4, v3, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;->error:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ErrorReturn;

    if-eqz v4, :cond_9

    .line 468
    const-string v4, "PhoneContactsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to verify profile: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto/16 :goto_0

    .line 472
    :cond_9
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->reloadProfile()V

    .line 474
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->uploadContactsSucceeded()Z

    move-result v4

    if-nez v4, :cond_a

    .line 475
    const-string v4, "PhoneContactsManager"

    const-string v5, "Contacts upload after verification failed. Reporting success in UX"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :cond_a
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UploadContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    iput-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    goto :goto_1

    .line 480
    .end local v3    # "response":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :cond_b
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    sget-object v5, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UploadContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    if-ne v4, v5, :cond_6

    .line 481
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->uploadContactsSucceeded()Z
    :try_end_2
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 409
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 409
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 425
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 495
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->task:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->onShortCircuitProfileAsyncTaskCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    .line 496
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 409
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 491
    return-void
.end method
