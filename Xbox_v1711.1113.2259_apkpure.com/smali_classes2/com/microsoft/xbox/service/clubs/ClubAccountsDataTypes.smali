.class public final Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes;
.super Ljava/lang/Object;
.source "ClubAccountsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$TransferOwnershipRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ChangeNameRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$BaseClubChangeRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;,
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;,
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;,
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;,
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;,
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$CreateClubRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ReserveClubNameRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;,
        Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
