.class Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;
.super Landroid/support/v4/view/AccessibilityDelegateCompat;
.source "TwoWayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListItemAccessibilityDelegate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 0

    .prologue
    .line 6173
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-direct {p0}, Landroid/support/v4/view/AccessibilityDelegateCompat;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p2, "x1"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;

    .prologue
    .line 6173
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    return-void
.end method


# virtual methods
.method public onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 4
    .param p1, "host"    # Landroid/view/View;
    .param p2, "info"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v3, 0x1

    .line 6176
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/AccessibilityDelegateCompat;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 6178
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    .line 6179
    .local v1, "position":I
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 6182
    .local v0, "adapter":Landroid/widget/ListAdapter;
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-nez v0, :cond_1

    .line 6207
    :cond_0
    :goto_0
    return-void

    .line 6187
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6191
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedItemPosition()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 6192
    invoke-virtual {p2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setSelected(Z)V

    .line 6193
    const/16 v2, 0x8

    invoke-virtual {p2, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 6198
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isClickable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 6199
    const/16 v2, 0x10

    invoke-virtual {p2, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 6200
    invoke-virtual {p2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setClickable(Z)V

    .line 6203
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isLongClickable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6204
    const/16 v2, 0x20

    invoke-virtual {p2, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 6205
    invoke-virtual {p2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setLongClickable(Z)V

    goto :goto_0

    .line 6195
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {p2, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    goto :goto_1
.end method

.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 8
    .param p1, "host"    # Landroid/view/View;
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 6211
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/AccessibilityDelegateCompat;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 6252
    :cond_0
    :goto_0
    return v4

    .line 6215
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v6, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    .line 6216
    .local v1, "position":I
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 6219
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eq v1, v7, :cond_2

    if-nez v0, :cond_3

    :cond_2
    move v4, v5

    .line 6220
    goto :goto_0

    .line 6224
    :cond_3
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v6

    if-nez v6, :cond_5

    :cond_4
    move v4, v5

    .line 6225
    goto :goto_0

    .line 6228
    :cond_5
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v6, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getItemIdAtPosition(I)J

    move-result-wide v2

    .line 6230
    .local v2, "id":J
    sparse-switch p2, :sswitch_data_0

    move v4, v5

    .line 6252
    goto :goto_0

    .line 6232
    :sswitch_0
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedItemPosition()I

    move-result v6

    if-ne v6, v1, :cond_6

    .line 6233
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelection(I)V

    goto :goto_0

    :cond_6
    move v4, v5

    .line 6236
    goto :goto_0

    .line 6239
    :sswitch_1
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedItemPosition()I

    move-result v6

    if-eq v6, v1, :cond_7

    .line 6240
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelection(I)V

    goto :goto_0

    :cond_7
    move v4, v5

    .line 6243
    goto :goto_0

    .line 6246
    :sswitch_2
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isClickable()Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v6, p1, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v6

    if-nez v6, :cond_0

    :cond_8
    move v4, v5

    goto :goto_0

    .line 6249
    :sswitch_3
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isLongClickable()Z

    move-result v6

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v6, p1, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$4000(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;IJ)Z

    move-result v6

    if-nez v6, :cond_0

    :cond_9
    move v4, v5

    goto :goto_0

    .line 6230
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method
