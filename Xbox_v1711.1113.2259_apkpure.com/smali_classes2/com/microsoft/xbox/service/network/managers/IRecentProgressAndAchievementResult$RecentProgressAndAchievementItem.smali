.class public Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
.super Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
.source "IRecentProgressAndAchievementResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecentProgressAndAchievementItem"
.end annotation


# instance fields
.field private mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field public name:Ljava/lang/String;

.field public platforms:Ljava/lang/String;

.field public rareUnlocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RareUnlock;",
            ">;"
        }
    .end annotation
.end field

.field public serviceConfigId:Ljava/lang/String;

.field public titleId:J

.field private titleType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;-><init>()V

    .line 36
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/SLSResponseType;->XboxOne:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    .line 37
    return-void
.end method


# virtual methods
.method public getCompletionPercentage()I
    .locals 4

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getCurrentGamerscore()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getMaxGamerscore()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-double v0, v2

    .line 77
    .local v0, "percent":D
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    :goto_0
    return v2

    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    goto :goto_0
.end method

.method public getEDSV2MediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v0
.end method

.method public getResponseType()Lcom/microsoft/xbox/service/network/managers/SLSResponseType;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    return-object v0
.end method

.method public getSuperHeroArtUri()Ljava/lang/String;
    .locals 5

    .prologue
    .line 60
    const/4 v1, 0x0

    .line 61
    .local v1, "ret":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 63
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    .line 64
    .local v0, "img":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getPurpose()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v3, "SuperHeroArt"

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getPurpose()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 65
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 70
    .end local v0    # "img":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    :cond_1
    return-object v1
.end method

.method public getTitleType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->titleType:Ljava/lang/String;

    return-object v0
.end method

.method public setEDSV2MediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 45
    return-void
.end method

.method public setTitleType(Ljava/lang/String;)V
    .locals 0
    .param p1, "titleType"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->titleType:Ljava/lang/String;

    .line 53
    return-void
.end method
