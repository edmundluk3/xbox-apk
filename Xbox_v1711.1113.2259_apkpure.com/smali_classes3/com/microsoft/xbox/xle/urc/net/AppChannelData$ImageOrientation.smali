.class public final enum Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;
.super Ljava/lang/Enum;
.source "AppChannelData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/AppChannelData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ImageOrientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

.field public static final enum Square:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

.field public static final enum Standard:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

.field public static final enum Tall:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

.field public static final enum Widescreen:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    const-string v1, "Tall"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->Tall:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    const-string v1, "Square"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->Square:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    const-string v1, "Widescreen"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->Widescreen:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    const-string v1, "Standard"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->Standard:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    .line 11
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->Tall:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->Square:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->Widescreen:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->Standard:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->$VALUES:[Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->$VALUES:[Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    return-object v0
.end method
