.class abstract Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;
.super Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;
.source "$AutoValue_TitleHubDataTypes_AchievementNotification.java"


# instance fields
.field private final achievementIconUrl:Ljava/lang/String;

.field private final achievementName:Ljava/lang/String;

.field private final gamerscore:J

.field private final rarityCategory:Ljava/lang/String;

.field private final rarityPercentage:F

.field private final titleId:J

.field private final titleName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;JJLjava/lang/String;)V
    .locals 1
    .param p1, "titleName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "achievementIconUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "rarityPercentage"    # F
    .param p4, "rarityCategory"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "titleId"    # J
    .param p7, "gamerscore"    # J
    .param p9, "achievementName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleName:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementIconUrl:Ljava/lang/String;

    .line 28
    iput p3, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityPercentage:F

    .line 29
    iput-object p4, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityCategory:Ljava/lang/String;

    .line 30
    iput-wide p5, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleId:J

    .line 31
    iput-wide p7, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->gamerscore:J

    .line 32
    iput-object p9, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementName:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public achievementIconUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementIconUrl:Ljava/lang/String;

    return-object v0
.end method

.method public achievementName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementName:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    if-ne p1, p0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v1

    .line 92
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;

    if-eqz v3, :cond_7

    move-object v0, p1

    .line 93
    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;

    .line 94
    .local v0, "that":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleName:Ljava/lang/String;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->titleName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementIconUrl:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 95
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->achievementIconUrl()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget v3, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityPercentage:F

    .line 96
    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->rarityPercentage()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityCategory:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 97
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->rarityCategory()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-wide v4, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleId:J

    .line 98
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->titleId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->gamerscore:J

    .line 99
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->gamerscore()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementName:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 100
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->achievementName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 94
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->titleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 95
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementIconUrl:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->achievementIconUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 97
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityCategory:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->rarityCategory()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 100
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->achievementName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;
    :cond_7
    move v1, v2

    .line 102
    goto/16 :goto_0
.end method

.method public gamerscore()J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->gamerscore:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const/16 v10, 0x20

    const/4 v2, 0x0

    const v3, 0xf4243

    .line 107
    const/4 v0, 0x1

    .line 108
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 109
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleName:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 110
    mul-int/2addr v0, v3

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementIconUrl:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 112
    mul-int/2addr v0, v3

    .line 113
    iget v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityPercentage:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    .line 114
    mul-int/2addr v0, v3

    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityCategory:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 116
    mul-int/2addr v0, v3

    .line 117
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleId:J

    ushr-long/2addr v6, v10

    iget-wide v8, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleId:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 118
    mul-int/2addr v0, v3

    .line 119
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->gamerscore:J

    ushr-long/2addr v6, v10

    iget-wide v8, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->gamerscore:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 120
    mul-int/2addr v0, v3

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementName:Ljava/lang/String;

    if-nez v1, :cond_3

    :goto_3
    xor-int/2addr v0, v2

    .line 122
    return v0

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 111
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementIconUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 115
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityCategory:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 121
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public rarityCategory()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityCategory:Ljava/lang/String;

    return-object v0
.end method

.method public rarityPercentage()F
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityPercentage:F

    return v0
.end method

.method public titleId()J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleId:J

    return-wide v0
.end method

.method public titleName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AchievementNotification{titleName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", achievementIconUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementIconUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rarityPercentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityPercentage:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rarityCategory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->rarityCategory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->titleId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gamerscore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->gamerscore:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", achievementName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/$AutoValue_TitleHubDataTypes_AchievementNotification;->achievementName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
