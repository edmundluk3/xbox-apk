.class public Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;
.super Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.source "TvHubPhoneScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "TvHubPhoneScreen"


# instance fields
.field private atBottomOfBackStack:Z

.field private currentVisiblePane:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

.field private streamerControlBar:Landroid/widget/RelativeLayout;

.field private tvStreamerModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

.field private tvStreamerResumeBtn:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->atBottomOfBackStack:Z

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->atBottomOfBackStack:Z

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->currentVisiblePane:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    return-object v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->currentVisiblePane:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    return-object p1
.end method

.method private adjustControlBarBottomMargin()V
    .locals 5

    .prologue
    .line 196
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    .line 197
    .local v0, "appBarManager":Lcom/microsoft/xbox/xle/app/ApplicationBarManager;
    const/4 v2, 0x0

    .line 198
    .local v2, "margin":I
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getIsApplicationBarShown()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 199
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getCollapsedAppBarView()Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    .line 202
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 203
    .local v1, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f090054

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v3, v2

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 204
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 205
    return-void
.end method

.method private adjustPivotHeight()V
    .locals 6

    .prologue
    .line 183
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 184
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v4, v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 185
    .local v0, "childView":Landroid/view/View;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 186
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 187
    .local v3, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getStreamerState()Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->BACKGROUND:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v4, v5, :cond_1

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f090579

    .line 188
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 189
    .local v1, "controlBarHeight":I
    :goto_1
    iput v1, v3, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 190
    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 183
    .end local v1    # "controlBarHeight":I
    .end local v3    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 188
    .restart local v3    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 193
    .end local v0    # "childView":Landroid/view/View;
    .end local v3    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    return-void
.end method


# virtual methods
.method public adjustBottomMargin(I)V
    .locals 0
    .param p1, "bottomMargin"    # I

    .prologue
    .line 250
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->adjustBottomMargin(I)V

    .line 251
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->adjustControlBarBottomMargin()V

    .line 252
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->adjustPivotHeight()V

    .line 253
    return-void
.end method

.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v0

    .line 96
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "OneGuide"

    goto :goto_0
.end method

.method public onBackButtonPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 223
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getStreamerState()Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    move-result-object v0

    .line 224
    .local v0, "currentState":Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->atBottomOfBackStack:Z

    if-nez v1, :cond_1

    sget-object v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->BACKGROUND:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->PIP:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v0, v1, :cond_1

    .line 226
    :cond_0
    invoke-static {v3, v3}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->streamChannel(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->atBottomOfBackStack:Z

    .line 239
    :goto_0
    return-void

    .line 228
    :cond_1
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->atBottomOfBackStack:Z

    if-nez v1, :cond_3

    sget-object v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->FULLSCREEN:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v0, v1, :cond_3

    .line 229
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 230
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupPipUI()V

    .line 234
    :goto_1
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->atBottomOfBackStack:Z

    goto :goto_0

    .line 232
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->backgroundStreamerUI()V

    goto :goto_1

    .line 236
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->dismiss(Z)V

    .line 237
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onBackButtonPressed()V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 53
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onCreate()V

    .line 54
    const v3, 0x7f030250

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->setContentView(I)V

    .line 56
    const v3, 0x7f0e0b63

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    .line 57
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 59
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 60
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03024a

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    .line 61
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f090579

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 62
    .local v2, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 63
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->addView(Landroid/view/View;)V

    .line 67
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    const v4, 0x7f0e0b49

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->tvStreamerResumeBtn:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    .line 68
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->tvStreamerResumeBtn:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    if-eqz v3, :cond_0

    .line 69
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->tvStreamerResumeBtn:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    new-instance v4, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen$1;-><init>(Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->tvStreamerModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    .line 80
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->tvStreamerModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v3, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->addStreamerStateListener(Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/xle/epg/TvStreamerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Lcom/microsoft/xbox/xle/epg/TvStreamerException;
    const-string v3, "TvHubPhoneScreen"

    const-string v4, "Failed to get TvStreamerModel"

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onCreateContentView()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->tvStreamerModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->tvStreamerModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->removeStreamerStateListener(Lcom/microsoft/xbox/xle/epg/TvStreamerModel$IStreamerStateListener;)Z

    .line 218
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onDestroy()V

    .line 219
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 209
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->stopTrackingSession()V

    .line 210
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onPause()V

    .line 211
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 170
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->startTrackingSession()V

    .line 171
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->getSharedInstance()Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->refreshTimeAndDateFormatting()V

    .line 173
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onResume()V

    .line 175
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getStreamerState()Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->BACKGROUND:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v0, v1, :cond_0

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 180
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 109
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getLastScreen()I

    move-result v1

    .line 110
    .local v1, "startScreen":I
    if-ltz v1, :cond_0

    sget-object v2, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->LAST:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->ordinal()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 111
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->TV_LISTINGS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->ordinal()I

    move-result v1

    .line 113
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->TV_LISTINGS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->ordinal()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 115
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "EPGLoad"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->beginTrackPerformance(Ljava/lang/String;)V

    .line 118
    :cond_2
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onStart()V

    .line 120
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->startTrackingSession()V

    .line 123
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    .line 124
    .local v0, "dialogManager":Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;
    if-eqz v0, :cond_3

    .line 125
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->setShouldLaunchUrcRemote(Z)V

    .line 129
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setCurrentPaneIndex(I)V

    .line 131
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    new-instance v3, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen$2;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen$2;-><init>(Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setOnCurrentPaneChangedRunnable(Ljava/lang/Runnable;)V

    .line 149
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 153
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onStop()V

    .line 156
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    .line 157
    .local v0, "dialogManager":Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;
    if-eqz v0, :cond_0

    .line 158
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->setShouldLaunchUrcRemote(Z)V

    .line 162
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "EPGLoad"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->cancelTrackPerformance(Ljava/lang/String;)V

    .line 165
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->purgeOldSchedules()V

    .line 166
    return-void
.end method

.method public onStreamerStateChanged(Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;)V
    .locals 3
    .param p1, "state"    # Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    .prologue
    const/4 v2, 0x0

    .line 257
    sget-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->BACKGROUND:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne p1, v0, :cond_2

    .line 258
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->addView(Landroid/view/View;)V

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 268
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->adjustPivotHeight()V

    .line 269
    return-void

    .line 263
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->streamerControlBar:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 264
    sget-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->FULLSCREEN:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne p1, v0, :cond_1

    .line 265
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->atBottomOfBackStack:Z

    goto :goto_0
.end method

.method public resetBottomMargin()V
    .locals 0

    .prologue
    .line 243
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->resetBottomMargin()V

    .line 244
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->adjustControlBarBottomMargin()V

    .line 245
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;->adjustPivotHeight()V

    .line 246
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method
