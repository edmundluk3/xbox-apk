.class public Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ActivityFeedStatusPostScreenAdapter.java"


# static fields
.field private static final MAX_TEXT_LENGTH:I = 0x15e


# instance fields
.field private addLinkButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private addToPostButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private cancelPreviewButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private closeButton:Landroid/widget/Button;

.field private counter:Landroid/widget/TextView;

.field private getLinkPreviewButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private imageIcon:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private linkImageForPost:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private linkPreviewImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private linkPreviewLayout:Landroid/view/View;

.field private linkTextEntry:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private postButton:Landroid/widget/Button;

.field private postError:Landroid/widget/Button;

.field private previewError:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private textEntry:Landroid/widget/EditText;

.field private textEntryLayout:Landroid/view/View;

.field private final textWatcher:Landroid/text/TextWatcher;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 221
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textWatcher:Landroid/text/TextWatcher;

    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    .line 56
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->initPostLayout()V

    .line 57
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->initPreviewLayout()V

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->updateGetPreviewButton()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->updateCountAndPostButton()V

    return-void
.end method

.method private initPostLayout()V
    .locals 2

    .prologue
    .line 61
    const v0, 0x7f0e0164

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textEntryLayout:Landroid/view/View;

    .line 62
    const v0, 0x7f0e0166

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->closeButton:Landroid/widget/Button;

    .line 63
    const v0, 0x7f0e0168

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textEntry:Landroid/widget/EditText;

    .line 64
    const v0, 0x7f0e0169

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postButton:Landroid/widget/Button;

    .line 65
    const v0, 0x7f0e016c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->counter:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0e016d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->linkImageForPost:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 67
    const v0, 0x7f0e016e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postError:Landroid/widget/Button;

    .line 68
    const v0, 0x7f0e016a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 70
    const v0, 0x7f0e016b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->addLinkButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->addLinkButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    return-void
.end method

.method private initPreviewLayout()V
    .locals 2

    .prologue
    .line 75
    const v0, 0x7f0e0165

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->linkPreviewLayout:Landroid/view/View;

    .line 77
    const v0, 0x7f0e013a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->linkTextEntry:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->linkTextEntry:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 96
    const v0, 0x7f0e0139

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->getLinkPreviewButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->getLinkPreviewButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    const v0, 0x7f0e013d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->linkPreviewImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 100
    const v0, 0x7f0e013e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->imageIcon:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 101
    const v0, 0x7f0e013f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->previewError:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 103
    const v0, 0x7f0e0140

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->addToPostButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->addToPostButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    const v0, 0x7f0e0141

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->cancelPreviewButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->cancelPreviewButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    return-void
.end method

.method static synthetic lambda$initPostLayout$0(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->updateMainLayout(Z)V

    return-void
.end method

.method static synthetic lambda$initPreviewLayout$1(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->loadPreview()V

    return-void
.end method

.method static synthetic lambda$initPreviewLayout$2(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->addLinkToPost()V

    return-void
.end method

.method static synthetic lambda$initPreviewLayout$3(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->cancelPreview()V

    return-void
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->closeDialog()V

    return-void
.end method

.method static synthetic lambda$onStart$5(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postStatus()V

    return-void
.end method

.method static synthetic lambda$onStart$6(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postStatus()V

    return-void
.end method

.method private postStatus()V
    .locals 2

    .prologue
    .line 239
    const/4 v0, 0x0

    .line 240
    .local v0, "pin":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->isChecked()Z

    move-result v0

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->checkPinAndPostStatus(Z)V

    .line 245
    return-void
.end method

.method private swapTextEntryAndPreviewLayout(Z)V
    .locals 3
    .param p1, "showPreviewLayout"    # Z

    .prologue
    const/4 v1, 0x0

    .line 208
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textEntryLayout:Landroid/view/View;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->linkPreviewLayout:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 210
    if-eqz p1, :cond_0

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->linkTextEntry:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->requestFocus()Z

    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textEntry:Landroid/widget/EditText;

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->showKeyboard(Landroid/view/View;I)V

    .line 214
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 208
    goto :goto_0
.end method

.method private updateCountAndPostButton()V
    .locals 10

    .prologue
    const/16 v9, 0x15e

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 253
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 254
    .local v0, "msg":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->counter:Landroid/widget/TextView;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%d/%d"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->counter:Landroid/widget/TextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070d71

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    .line 256
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    .line 255
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postButton:Landroid/widget/Button;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->hasValidLinkPreview()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->isBusy()Z

    move-result v4

    if-nez v4, :cond_1

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 258
    return-void

    :cond_1
    move v1, v2

    .line 257
    goto :goto_0
.end method

.method private updateGetPreviewButton()V
    .locals 3

    .prologue
    .line 248
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->getLinkPreviewButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getPreviewLinkText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    .line 249
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getLoadPreviewDataState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    .line 248
    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 250
    return-void

    .line 249
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateImageForPost(Z)V
    .locals 3
    .param p1, "hasImage"    # Z

    .prologue
    const v2, 0x7f020127

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->linkImageForPost:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->linkImageForPost:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getPreviewImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2, v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V

    .line 219
    return-void
.end method

.method private updatePreviewLayout()V
    .locals 3

    .prologue
    const v2, 0x7f020127

    .line 170
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->updateGetPreviewButton()V

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->linkTextEntry:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getPreviewLinkText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->addToPostButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->hasValidLinkPreview()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->linkPreviewImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getPreviewImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2, v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setImageURI2(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Ljava/lang/String;II)V

    .line 174
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->imageIcon:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->hasPreviewImage()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->previewError:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->shouldShowPreviewError()Z

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getPreviewErrorText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 176
    return-void

    .line 174
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateTextEntryLayout()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v4, 0x0

    .line 137
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->addLinkButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->showAddLinkButton()Z

    move-result v5

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 138
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getErrorStringResId()I

    move-result v0

    .line 139
    .local v0, "errorResId":I
    if-nez v0, :cond_1

    const/4 v1, 0x0

    .line 140
    .local v1, "strError":Ljava/lang/String;
    :goto_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postError:Landroid/widget/Button;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-static {v5, v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 141
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->updateCountAndPostButton()V

    .line 142
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->hasPreviewImage()Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->updateImageForPost(Z)V

    .line 144
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getLaunchContextTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v2

    .line 146
    .local v2, "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getLaunchContextTimelineId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes;->canUserPinPost(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 147
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$3;->$SwitchMap$com$microsoft$xbox$service$activityFeed$UserPostsDataTypes$TimelineType:[I

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getLaunchContextTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 161
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 167
    :cond_0
    :goto_2
    return-void

    .line 139
    .end local v1    # "strError":Ljava/lang/String;
    .end local v2    # "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postError:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .restart local v1    # "strError":Ljava/lang/String;
    :cond_2
    move v3, v4

    .line 140
    goto :goto_1

    .line 149
    .restart local v2    # "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-eqz v3, :cond_0

    .line 150
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0700e2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setVisibility(I)V

    goto :goto_2

    .line 155
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-eqz v3, :cond_0

    .line 156
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0700e5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setVisibility(I)V

    goto :goto_2

    .line 165
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_2

    .line 147
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onAnimateInCompleted()V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onAnimateInCompleted()V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textEntry:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textEntry:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->showKeyboard(Landroid/view/View;I)V

    .line 121
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onPause()V

    .line 113
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->dismissKeyboard()V

    .line 114
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 180
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->closeButton:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->closeButton:Landroid/widget/Button;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textEntry:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textEntry:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postButton:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postButton:Landroid/widget/Button;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postError:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->updateViewOverride()V

    .line 196
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 200
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->closeButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textEntry:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->textWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->postError:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    return-void
.end method

.method protected updateViewOverride()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->updateLoadingIndicator(Z)V

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getShowPreviewLayout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->updatePreviewLayout()V

    .line 133
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getShowPreviewLayout()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->swapTextEntryAndPreviewLayout(Z)V

    .line 134
    return-void

    .line 130
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;->updateTextEntryLayout()V

    goto :goto_0
.end method
