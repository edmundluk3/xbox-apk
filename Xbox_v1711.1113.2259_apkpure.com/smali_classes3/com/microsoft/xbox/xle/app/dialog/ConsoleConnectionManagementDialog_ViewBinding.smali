.class public Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog_ViewBinding;
.super Ljava/lang/Object;
.source "ConsoleConnectionManagementDialog_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;)V
    .locals 1
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog_ViewBinding;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;Landroid/view/View;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;

    .line 29
    const v0, 0x7f0e044f

    const-string v1, "field \'closeButton\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 30
    const v0, 0x7f0e0450

    const-string v1, "field \'toggleConnectionButton\'"

    const-class v2, Landroid/widget/LinearLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButton:Landroid/widget/LinearLayout;

    .line 31
    const v0, 0x7f0e0451

    const-string v1, "field \'toggleConnectionButtonIcon\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 32
    const v0, 0x7f0e0452

    const-string v1, "field \'toggleConnectionButtonText\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButtonText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 33
    const v0, 0x7f0e0453

    const-string v1, "field \'navigateToOOBESettingsButton\'"

    const-class v2, Landroid/widget/LinearLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->navigateToOOBESettingsButton:Landroid/widget/LinearLayout;

    .line 34
    const v0, 0x7f0e0454

    const-string v1, "field \'navigateToOOBESettingsButtonText\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->navigateToOOBESettingsButtonText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 35
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;

    .line 41
    .local v0, "target":Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 42
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;

    .line 44
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 45
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButton:Landroid/widget/LinearLayout;

    .line 46
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 47
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->toggleConnectionButtonText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 48
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->navigateToOOBESettingsButton:Landroid/widget/LinearLayout;

    .line 49
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->navigateToOOBESettingsButtonText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 50
    return-void
.end method
