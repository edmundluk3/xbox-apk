.class public Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "LfgListViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;,
        Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;",
        "Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isGameProfile:Z

.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private final onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field private final rowResId:I

.field private tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;

.field private userMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/toolkit/generics/Action;Z)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowResId"    # I
    .param p4, "isGameProfile"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p3, "onItemClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/toolkit/generics/Action;ZLcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/toolkit/generics/Action;ZLcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowResId"    # I
    .param p4, "isGameProfile"    # Z
    .param p5, "listener"    # Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;Z",
            "Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    .local p3, "onItemClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 64
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 65
    iput p2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->rowResId:I

    .line 66
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 67
    iput-boolean p4, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->isGameProfile:Z

    .line 68
    iput-object p5, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;

    .line 69
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->isGameProfile:Z

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->data:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->data:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 52
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;I)V
    .locals 4
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->data:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 87
    .local v0, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->data:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->userMap:Ljava/util/Map;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->userMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    :goto_0
    invoke-static {p1, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->access$000(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 88
    return-void

    .line 87
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 77
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    iget v3, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->rowResId:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 79
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;Landroid/view/View;)V

    .line 80
    .local v1, "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 81
    return-object v1
.end method

.method public setUserMap(Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "userMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 93
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->userMap:Ljava/util/Map;

    .line 94
    return-void
.end method
