.class public Lcom/microsoft/xbox/xle/ui/MediaProgressBar;
.super Landroid/widget/RelativeLayout;
.source "MediaProgressBar.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;
    }
.end annotation


# static fields
.field private static final MAX_PROGRESS:I = 0x64


# instance fields
.field private currentPostionSeconds:J

.field private durationSeconds:J

.field private isSeekable:Z

.field private lastUserInitiatedProgressData:Lcom/microsoft/xbox/toolkit/MediaProgressData;

.field private mediaProgressBar:Landroid/widget/ProgressBar;

.field private mediaProgressBarLayout:Landroid/view/View;

.field private mediaSeekBar:Landroid/widget/SeekBar;

.field private progressText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private trackingStarted:Z

.field private userInitiatedProgressListener:Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 34
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->trackingStarted:Z

    .line 35
    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->userInitiatedProgressListener:Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;

    .line 36
    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->lastUserInitiatedProgressData:Lcom/microsoft/xbox/toolkit/MediaProgressData;

    .line 37
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->isSeekable:Z

    .line 42
    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->trackingStarted:Z

    .line 35
    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->userInitiatedProgressListener:Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;

    .line 36
    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->lastUserInitiatedProgressData:Lcom/microsoft/xbox/toolkit/MediaProgressData;

    .line 37
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->isSeekable:Z

    .line 48
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->trackingStarted:Z

    .line 35
    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->userInitiatedProgressListener:Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;

    .line 36
    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->lastUserInitiatedProgressData:Lcom/microsoft/xbox/toolkit/MediaProgressData;

    .line 37
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->isSeekable:Z

    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method private getTimeString(J)Ljava/lang/String;
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 144
    invoke-static {p1, p2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getTimeStringMMSS(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setCurrentPostion(J)V
    .locals 9
    .param p1, "currentPostion"    # J

    .prologue
    const/4 v3, 0x0

    .line 123
    const-string v4, "Trying to set progressbar position while the thumb is being dragged"

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->trackingStarted:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v4, v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 124
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->currentPostionSeconds:J

    .line 125
    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->currentPostionSeconds:J

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->durationSeconds:J

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->updateProgressText(JJ)V

    .line 127
    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->durationSeconds:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-nez v2, :cond_1

    .line 128
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->getProgessBar()Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 133
    :goto_1
    return-void

    :cond_0
    move v2, v3

    .line 123
    goto :goto_0

    .line 130
    :cond_1
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->currentPostionSeconds:J

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->durationSeconds:J

    div-long/2addr v2, v4

    long-to-double v0, v2

    .line 131
    .local v0, "progressDouble":D
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->getProgessBar()Landroid/widget/ProgressBar;

    move-result-object v2

    double-to-int v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_1
.end method

.method private updateProgressText(JJ)V
    .locals 7
    .param p1, "position"    # J
    .param p3, "duration"    # J

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->progressText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s / %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->getTimeString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-direct {p0, p3, p4}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->getTimeString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    return-void
.end method


# virtual methods
.method protected getProgessBar()Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->isSeekable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaSeekBar:Landroid/widget/SeekBar;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaProgressBar:Landroid/widget/ProgressBar;

    goto :goto_0
.end method

.method public init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const-wide/16 v8, 0x0

    const/16 v6, 0x64

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 58
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 59
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f03016d

    invoke-virtual {v2, v3, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 61
    const v3, 0x7f0e07a3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaProgressBarLayout:Landroid/view/View;

    .line 62
    const v3, 0x7f0e07a6

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaProgressBar:Landroid/widget/ProgressBar;

    .line 63
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v6}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 64
    const v3, 0x7f0e07a5

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaSeekBar:Landroid/widget/SeekBar;

    .line 65
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v6}, Landroid/widget/SeekBar;->setMax(I)V

    .line 66
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 67
    const v3, 0x7f0e07a4

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->progressText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 69
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->setSeekable(Z)V

    .line 71
    invoke-direct {p0, v8, v9, v8, v9}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->updateProgressText(JJ)V

    .line 73
    if-eqz p2, :cond_0

    .line 74
    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->MediaProgressBar:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 76
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 77
    .local v1, "showProgressText":Z
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 79
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->progressText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v1, :cond_1

    move v3, v4

    :goto_0
    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 81
    .end local v0    # "a":Landroid/content/res/TypedArray;
    .end local v1    # "showProgressText":Z
    :cond_0
    return-void

    .line 79
    .restart local v0    # "a":Landroid/content/res/TypedArray;
    .restart local v1    # "showProgressText":Z
    :cond_1
    const/16 v3, 0x8

    goto :goto_0
.end method

.method public initialize(JJ)V
    .locals 3
    .param p1, "currentPostionInSeconds"    # J
    .param p3, "durationInSeconds"    # J

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->trackingStarted:Z

    if-nez v0, :cond_0

    .line 100
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gtz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaProgressBarLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaProgressBarLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 107
    iput-wide p3, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->durationSeconds:J

    .line 109
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->setCurrentPostion(J)V

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 153
    if-eqz p3, :cond_0

    .line 154
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->durationSeconds:J

    long-to-double v2, v2

    int-to-double v4, p2

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v4

    double-to-long v0, v2

    .line 155
    .local v0, "progressInSeconds":J
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->durationSeconds:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->updateProgressText(JJ)V

    .line 156
    new-instance v2, Lcom/microsoft/xbox/toolkit/MediaProgressData;

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->durationSeconds:J

    invoke-direct {v2, v0, v1, v4, v5}, Lcom/microsoft/xbox/toolkit/MediaProgressData;-><init>(JJ)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->lastUserInitiatedProgressData:Lcom/microsoft/xbox/toolkit/MediaProgressData;

    .line 158
    .end local v0    # "progressInSeconds":J
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->trackingStarted:Z

    .line 163
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 167
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->trackingStarted:Z

    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->lastUserInitiatedProgressData:Lcom/microsoft/xbox/toolkit/MediaProgressData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->userInitiatedProgressListener:Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->userInitiatedProgressListener:Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->lastUserInitiatedProgressData:Lcom/microsoft/xbox/toolkit/MediaProgressData;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;->onProgressChanged(Lcom/microsoft/xbox/toolkit/MediaProgressData;)V

    .line 171
    :cond_0
    return-void
.end method

.method public setPositionInSeconds(J)V
    .locals 5
    .param p1, "currentPostionInSeconds"    # J

    .prologue
    .line 115
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->durationSeconds:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->currentPostionSeconds:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->trackingStarted:Z

    if-eqz v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->setCurrentPostion(J)V

    goto :goto_0
.end method

.method public setSeekable(Z)V
    .locals 3
    .param p1, "isSeekable"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 84
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->isSeekable:Z

    .line 85
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->isSeekable:Z

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 92
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->mediaProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public setShowProgressText(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 95
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->progressText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 96
    return-void

    .line 95
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setUserInitiatedProgressListener(Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->userInitiatedProgressListener:Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;

    .line 149
    return-void
.end method
