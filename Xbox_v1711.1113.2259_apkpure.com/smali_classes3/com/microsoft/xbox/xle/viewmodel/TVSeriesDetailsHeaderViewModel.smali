.class public Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "TVSeriesDetailsHeaderViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;",
        ">;"
    }
.end annotation


# instance fields
.field private seriesImageUri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 23
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVSeriesDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 24
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 25
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 26
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    .line 28
    .local v1, "url":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 30
    :try_start_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->seriesImageUri:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    .end local v1    # "url":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 31
    .restart local v1    # "url":Ljava/lang/String;
    :catch_0
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method public getBackgroundImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 104
    const v0, 0x7f070683

    return v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "uri":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->seriesImageUri:Ljava/lang/String;

    .line 62
    :cond_0
    return-object v0
.end method

.method public getMetacriticReviewScore()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getMetacriticReviewScore()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getParentalRating()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSeasonCount()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getSeasonCount()I

    move-result v0

    return v0
.end method

.method public getTVSeriesExtraData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getHasTvShowtimeInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    const-string v0, "TODO: Hookup to live data"

    .line 98
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTVSeriesReleaseData()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 38
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getReleaseYear()Ljava/lang/String;

    move-result-object v0

    .line 39
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0703d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getSeasonCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f071084

    .line 40
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 38
    invoke-static {v0, v1, v2, v3, v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->isLoadingDetail:Z

    return v0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVSeriesDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 54
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->cancel()V

    .line 70
    :cond_0
    return-void
.end method

.method public shouldShowBackground()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method
