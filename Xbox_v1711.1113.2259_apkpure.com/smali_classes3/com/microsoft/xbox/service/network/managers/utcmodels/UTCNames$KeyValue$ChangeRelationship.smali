.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue$ChangeRelationship;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChangeRelationship"
.end annotation


# static fields
.field public static final ExistingFavorite:Ljava/lang/String; = "ExistingFavorite"

.field public static final ExistingFriend:Ljava/lang/String; = "ExistingFriend"

.field public static final ExistingNotFavorite:Ljava/lang/String; = "ExistingNotFavorite"

.field public static final ExistingSharingOff:Ljava/lang/String; = "ExisitingSharingOff"

.field public static final ExistingSharingOn:Ljava/lang/String; = "ExisitingSharingOn"

.field public static final FacebookAdd:Ljava/lang/String; = "FacebookAdd"

.field public static final Favorited:Ljava/lang/String; = "Favorited"

.field public static final FriendAdd:Ljava/lang/String; = "FriendAdd"

.field public static final FriendRemove:Ljava/lang/String; = "FriendRemove"

.field public static final NotFavorited:Ljava/lang/String; = "NotFavorited"

.field public static final SharingOff:Ljava/lang/String; = "SharingOff"

.field public static final SharingOn:Ljava/lang/String; = "SharingOn"

.field public static final SuggestionAdd:Ljava/lang/String; = "SuggestionAdd"

.field public static final Unfavorited:Ljava/lang/String; = "Unfavorited"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
