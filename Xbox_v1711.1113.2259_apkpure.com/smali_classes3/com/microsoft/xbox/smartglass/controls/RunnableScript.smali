.class Lcom/microsoft/xbox/smartglass/controls/RunnableScript;
.super Ljava/lang/Object;
.source "RunnableScript.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final _script:Ljava/lang/String;

.field private final _view:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "script"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;->_view:Landroid/webkit/WebView;

    .line 15
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;->_script:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public getScript()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;->_script:Ljava/lang/String;

    return-object v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;->_view:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;->_script:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 20
    return-void
.end method
