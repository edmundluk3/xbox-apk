.class public final Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;
.super Ljava/lang/Object;
.source "ServiceModule_ProvideXTokenOkHttpClientFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lokhttp3/OkHttpClient;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final localeHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private final loggingInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/microsoft/xbox/data/service/ServiceModule;

.field private final xTokenAuthenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private final xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/ServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/service/ServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/ServiceModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p2, "loggingInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lokhttp3/logging/HttpLoggingInterceptor;>;"
    .local p3, "localeHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;>;"
    .local p4, "xTokenAuthenticatorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;>;"
    .local p5, "xTokenHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;>;"
    .local p6, "xUserAgentHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    sget-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->module:Lcom/microsoft/xbox/data/service/ServiceModule;

    .line 40
    sget-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->loggingInterceptorProvider:Ljavax/inject/Provider;

    .line 42
    sget-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 44
    sget-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 45
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    .line 46
    sget-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 47
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 48
    sget-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 49
    :cond_5
    iput-object p6, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 50
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/service/ServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 7
    .param p0, "module"    # Lcom/microsoft/xbox/data/service/ServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/ServiceModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "loggingInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lokhttp3/logging/HttpLoggingInterceptor;>;"
    .local p2, "localeHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;>;"
    .local p3, "xTokenAuthenticatorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;>;"
    .local p4, "xTokenHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;>;"
    .local p5, "xUserAgentHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;>;"
    new-instance v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;-><init>(Lcom/microsoft/xbox/data/service/ServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideXTokenOkHttpClient(Lcom/microsoft/xbox/data/service/ServiceModule;Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;)Lokhttp3/OkHttpClient;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/ServiceModule;
    .param p1, "loggingInterceptor"    # Lokhttp3/logging/HttpLoggingInterceptor;
    .param p2, "localeHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;
    .param p3, "xTokenAuthenticator"    # Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;
    .param p4, "xTokenHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;
    .param p5, "xUserAgentHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;

    .prologue
    .line 92
    invoke-virtual/range {p0 .. p5}, Lcom/microsoft/xbox/data/service/ServiceModule;->provideXTokenOkHttpClient(Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;)Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->get()Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method

.method public get()Lokhttp3/OkHttpClient;
    .locals 6

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->module:Lcom/microsoft/xbox/data/service/ServiceModule;

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->loggingInterceptorProvider:Ljavax/inject/Provider;

    .line 56
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lokhttp3/logging/HttpLoggingInterceptor;

    iget-object v2, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 57
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;

    iget-object v3, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    .line 58
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;

    iget-object v4, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 59
    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;

    iget-object v5, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 60
    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;

    .line 55
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/data/service/ServiceModule;->provideXTokenOkHttpClient(Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;)Lokhttp3/OkHttpClient;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 54
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/OkHttpClient;

    return-object v0
.end method
