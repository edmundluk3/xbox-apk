.class public abstract Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;
.super Ljava/lang/Object;
.source "PeopleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PeopleHubPerson"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    new-instance v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract addedDateTimeUtc()Ljava/util/Date;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract displayName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract displayPicRaw()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract gamerScore()J
.end method

.method public abstract gamertag()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract isBroadcasting()Z
.end method

.method public abstract isFavorite()Z
.end method

.method public abstract isFollowedByCaller()Z
.end method

.method public abstract isFollowingCaller()Z
.end method

.method public abstract isIdentityShared()Z
.end method

.method public abstract multiplayerSummary()Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract presenceState()Lcom/microsoft/xbox/service/model/UserStatus;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract presenceText()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract realName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract xuid()J
.end method
