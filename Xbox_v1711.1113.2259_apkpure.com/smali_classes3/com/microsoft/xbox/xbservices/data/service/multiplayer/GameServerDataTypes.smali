.class public Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes;
.super Ljava/lang/Object;
.source "GameServerDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QosServer;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerDataTypes$QoSServerList;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type cannot be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
