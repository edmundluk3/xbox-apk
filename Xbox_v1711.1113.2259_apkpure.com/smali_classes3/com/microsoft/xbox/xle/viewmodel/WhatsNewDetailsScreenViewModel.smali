.class public Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "WhatsNewDetailsScreenViewModel.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 8
    return-void
.end method


# virtual methods
.method public isBusy()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 37
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 23
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 17
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 19
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method public onTapCloseButton()V
    .locals 0

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;->goBack()V

    .line 12
    return-void
.end method
