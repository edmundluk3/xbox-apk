.class Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ViewClient;
.super Landroid/webkit/WebViewClient;
.source "BridgedWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewClient"
.end annotation


# static fields
.field private static final OnLoadScript:Ljava/lang/String; = "javascript:setTimeout(function() { if (typeof(Xbc) !== \'undefined\') { smartglass.onLoad(); } else { setTimeout(function () { smartglass.onLoad(); }, 200); } }, 0)"


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    .prologue
    .line 565
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ViewClient;->this$0:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 572
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ViewClient;->this$0:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->access$300(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;)V

    .line 575
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ViewClient;->this$0:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    const-string v2, "javascript:setTimeout(function() { if (typeof(Xbc) !== \'undefined\') { smartglass.onLoad(); } else { setTimeout(function () { smartglass.onLoad(); }, 200); } }, 0)"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;-><init>(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 576
    .local v0, "loadScript":Lcom/microsoft/xbox/smartglass/controls/RunnableScript;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ViewClient;->this$0:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->access$400(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;)Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->run(Lcom/microsoft/xbox/smartglass/controls/RunnableScript;)V

    .line 577
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 581
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ViewClient;->this$0:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-static {v0, p2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->access$500(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;Ljava/lang/String;)V

    .line 582
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 586
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ViewClient;->this$0:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-static {v0, p4, p2, p3}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->access$600(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;Ljava/lang/String;ILjava/lang/String;)V

    .line 587
    return-void
.end method
