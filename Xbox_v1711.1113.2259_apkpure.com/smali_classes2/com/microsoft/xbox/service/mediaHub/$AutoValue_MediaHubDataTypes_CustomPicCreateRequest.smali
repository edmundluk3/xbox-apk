.class abstract Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;
.super Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;
.source "$AutoValue_MediaHubDataTypes_CustomPicCreateRequest.java"


# instance fields
.field private final expectedBlocks:J

.field private final fileSize:J

.field private final initialMetaData:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;


# direct methods
.method constructor <init>(JJLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;)V
    .locals 3
    .param p1, "expectedBlocks"    # J
    .param p3, "fileSize"    # J
    .param p5, "initialMetaData"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;-><init>()V

    .line 18
    iput-wide p1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->expectedBlocks:J

    .line 19
    iput-wide p3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->fileSize:J

    .line 20
    if-nez p5, :cond_0

    .line 21
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null initialMetaData"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_0
    iput-object p5, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->initialMetaData:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    if-ne p1, p0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v1

    .line 56
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 57
    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;

    .line 58
    .local v0, "that":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->expectedBlocks:J

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;->expectedBlocks()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->fileSize:J

    .line 59
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;->fileSize()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->initialMetaData:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    .line 60
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;->initialMetaData()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;
    :cond_3
    move v1, v2

    .line 62
    goto :goto_0
.end method

.method public expectedBlocks()J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->expectedBlocks:J

    return-wide v0
.end method

.method public fileSize()J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->fileSize:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const v1, 0xf4243

    .line 67
    const/4 v0, 0x1

    .line 68
    .local v0, "h":I
    mul-int/2addr v0, v1

    .line 69
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->expectedBlocks:J

    ushr-long/2addr v4, v8

    iget-wide v6, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->expectedBlocks:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 70
    mul-int/2addr v0, v1

    .line 71
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->fileSize:J

    ushr-long/2addr v4, v8

    iget-wide v6, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->fileSize:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 72
    mul-int/2addr v0, v1

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->initialMetaData:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 74
    return v0
.end method

.method public initialMetaData()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->initialMetaData:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CustomPicCreateRequest{expectedBlocks="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->expectedBlocks:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fileSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->fileSize:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", initialMetaData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_CustomPicCreateRequest;->initialMetaData:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
