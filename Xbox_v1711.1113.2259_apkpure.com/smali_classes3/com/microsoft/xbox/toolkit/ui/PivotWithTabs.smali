.class public Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;
.super Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;
.source "PivotWithTabs.java"


# instance fields
.field private final body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

.field private tabNames:[Ljava/lang/String;

.field private telemetrySetActiveTabCallback:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;-><init>(Landroid/content/Context;)V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .line 52
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->captureHeaderParams(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setPageTitleFunction(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)V

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    .prologue
    .line 34
    invoke-static {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    move-result v0

    return v0
.end method

.method private captureHeaderParams(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 203
    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->PivotWithTabs:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 205
    .local v0, "a":Landroid/content/res/TypedArray;
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

    const/4 v3, 0x0

    .line 206
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;-><init>(Ljava/lang/String;)V

    .line 207
    .local v1, "headerParams":Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

    const/4 v3, 0x1

    .line 208
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;-><init>(Ljava/lang/String;)V

    .line 209
    .local v2, "subHeaderParams":Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    invoke-direct {v3, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;-><init>(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 209
    return-object v3

    .line 211
    .end local v1    # "headerParams":Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;
    .end local v2    # "subHeaderParams":Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;
    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v3
.end method

.method private getInitialPanesCollection()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 198
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getInitialPanes()[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 199
    return-object v0
.end method

.method private iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Boolean;",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 566
    .local p1, "f":Lcom/microsoft/xbox/toolkit/functions/GenericFunction;, "Lcom/microsoft/xbox/toolkit/functions/GenericFunction<Ljava/lang/Boolean;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    move-result v0

    return v0
.end method

.method private static iterateScreens(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I
    .locals 4
    .param p0, "body"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;",
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Boolean;",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 570
    .local p1, "f":Lcom/microsoft/xbox/toolkit/functions/GenericFunction;, "Lcom/microsoft/xbox/toolkit/functions/GenericFunction<Ljava/lang/Boolean;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getPageCount()I

    move-result v1

    .line 571
    .local v1, "paneCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 572
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    .line 573
    .local v2, "v":Landroid/view/View;
    instance-of v3, v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v3, :cond_0

    .line 574
    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .end local v2    # "v":Landroid/view/View;
    invoke-interface {p1, v2}, Lcom/microsoft/xbox/toolkit/functions/GenericFunction;->eval(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 579
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 571
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 579
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public addPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "index"    # I

    .prologue
    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p2, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->addPageAt(ILandroid/view/View;)Z

    .line 219
    return-void
.end method

.method public adjustBottomMargin(I)V
    .locals 1
    .param p1, "bottomMargin"    # I

    .prologue
    .line 420
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$13;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$13;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;I)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    .line 427
    return-void
.end method

.method public adjustPaneSize(Ljava/lang/Class;I)V
    .locals 0
    .param p2, "percent"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 527
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    return-void
.end method

.method public animateToCurrentPaneIndex(I)V
    .locals 1
    .param p1, "newPane"    # I

    .prologue
    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setActivePageIndex(I)Z

    .line 275
    return-void
.end method

.method public appendPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->addPage(Landroid/view/View;)Z

    .line 223
    return-void
.end method

.method public forceRefresh()V
    .locals 1

    .prologue
    .line 531
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 532
    .local v0, "s":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 533
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceRefresh()V

    .line 535
    :cond_0
    return-void
.end method

.method public getAnimateIn(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 1
    .param p1, "goingBack"    # Z

    .prologue
    .line 493
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getAnimateIn(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    move-result-object v0

    return-object v0
.end method

.method public getAnimateOut(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 1
    .param p1, "goingBack"    # Z

    .prologue
    .line 498
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getAnimateOut(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getActivePage()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    return-object v0
.end method

.method public getCurrentPaneIndex()I
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getActivePageIndex()I

    move-result v0

    return v0
.end method

.method public getHeaderText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getHeaderText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getIndexOfScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)I
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 410
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$12;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$12;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    move-result v0

    return v0
.end method

.method public getIndexOfScreen(Ljava/lang/Class;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 400
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$11;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$11;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    move-result v0

    return v0
.end method

.method protected getInitialPanes()[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 4

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getChildCount()I

    move-result v3

    new-array v2, v3, [Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 189
    .local v2, "panes":[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 190
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 191
    .local v1, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    aput-object v1, v2, v0

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    .end local v1    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-object v2
.end method

.method public getIsScrolling()Z
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method

.method public getPaneAtIndex(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1
    .param p1, "index"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    return-object v0
.end method

.method public getSubHeaderText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getSubHeaderText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTotalPaneCount()I
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getPageCount()I

    move-result v0

    return v0
.end method

.method protected initialize()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 154
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getInitialPanesCollection()Ljava/util/Collection;

    move-result-object v1

    .line 157
    .local v1, "panes":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->removeAllViews()V

    .line 160
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->ensureInitialized()V

    .line 161
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->addPages(Ljava/util/Collection;)Z

    .line 164
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 165
    .local v0, "bodyParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {p0, v2, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 168
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getPageContainer()Landroid/view/ViewGroup;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$2;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$2;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 184
    return-void
.end method

.method public isPageIndicatorVisible()Z
    .locals 1

    .prologue
    .line 589
    const/4 v0, 0x0

    return v0
.end method

.method public onAnimateInCompleted()V
    .locals 3

    .prologue
    .line 450
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 451
    .local v0, "bodyWeakPtr":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$15;

    invoke-direct {v2, p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$15;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->postRunnableAfterReady(Ljava/lang/Runnable;)V

    .line 466
    return-void
.end method

.method public onAnimateInStarted()V
    .locals 1

    .prologue
    .line 442
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 443
    .local v0, "page":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 444
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceUpdateViewImmediately()V

    .line 446
    :cond_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 508
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 509
    .local v0, "s":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 510
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    .line 512
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 306
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$3;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    .line 313
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 517
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 518
    .local v0, "s":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 519
    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 521
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 389
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$10;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$10;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    .line 396
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 341
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$6;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$6;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    .line 350
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 378
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$9;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$9;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    .line 385
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 354
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$7;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$7;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    .line 363
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 479
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getActivePageIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setCurrentPaneIndex(I)V

    .line 482
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->telemetrySetActiveTabCallback:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->telemetrySetActiveTabCallback:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->tabNames:[Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getActivePageIndex()I

    move-result v2

    aget-object v1, v1, v2

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/functions/GenericFunction;->eval(Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    :cond_0
    return-void
.end method

.method public onSetActive(I)V
    .locals 2
    .param p1, "pivotPaneIndex"    # I

    .prologue
    .line 470
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setCurrentPaneIndex(I)V

    .line 473
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->telemetrySetActiveTabCallback:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->telemetrySetActiveTabCallback:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->tabNames:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/functions/GenericFunction;->eval(Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    :cond_0
    return-void
.end method

.method public onSetInactive()V
    .locals 0

    .prologue
    .line 489
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 317
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$4;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    .line 324
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 328
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$5;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$5;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    .line 337
    return-void
.end method

.method public onTombstone()V
    .locals 1

    .prologue
    .line 367
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$8;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$8;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    .line 374
    return-void
.end method

.method public removePane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->removePageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    return-object v0
.end method

.method public resetBottomMargin()V
    .locals 1

    .prologue
    .line 431
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$14;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$14;-><init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->iterateScreens(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)I

    .line 438
    return-void
.end method

.method public setActiveOnAdd(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 0
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 548
    return-void
.end method

.method public setCurrentPaneIndex(I)V
    .locals 4
    .param p1, "newPane"    # I

    .prologue
    .line 247
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getPageCount()I

    move-result v1

    .line 248
    .local v1, "paneCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 249
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    .line 250
    .local v2, "v":Landroid/view/View;
    if-eq v0, p1, :cond_0

    instance-of v3, v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v3, :cond_0

    .line 251
    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .end local v2    # "v":Landroid/view/View;
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetInactive()V

    .line 248
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 255
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setActivePageIndex(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 256
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    .line 257
    .restart local v2    # "v":Landroid/view/View;
    instance-of v3, v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v3, :cond_2

    .line 258
    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .end local v2    # "v":Landroid/view/View;
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetActive()V

    .line 261
    :cond_2
    return-void
.end method

.method public setHeaderBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setHeaderBackgroundColor(I)V

    .line 110
    return-void
.end method

.method public setHeaderStyle(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setHeaderStyle(I)V

    .line 102
    return-void
.end method

.method public setHeaderText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 98
    return-void
.end method

.method public setHeaderTypefaceSource(Ljava/lang/String;)V
    .locals 1
    .param p1, "typefaceSource"    # Ljava/lang/String;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setHeaderTypefaceSource(Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public setHeaderVisibility(Z)V
    .locals 2
    .param p1, "b"    # Z

    .prologue
    .line 593
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setHeaderVisibility(Ljava/lang/Boolean;)V

    .line 594
    return-void
.end method

.method public setOnCurrentPaneChangedRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 302
    return-void
.end method

.method public setOnScrollingChangedRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runner"    # Ljava/lang/Runnable;

    .prologue
    .line 237
    return-void
.end method

.method public setScreenState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 539
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 540
    .local v0, "s":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 541
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setScreenState(I)V

    .line 543
    :cond_0
    return-void
.end method

.method public setSelectedTabIndicatorColor(I)V
    .locals 1
    .param p1, "color"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setSelectedTabIndicatorColor(I)V

    .line 126
    return-void
.end method

.method public setSelectedTabIndicatorColorToProfileColor()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setSelectedTabIndicatorColorToProfileColor()V

    .line 122
    return-void
.end method

.method public setStartPaneIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 270
    return-void
.end method

.method public setSubHeaderStyle(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setSubHeaderStyle(I)V

    .line 138
    return-void
.end method

.method public setSubHeaderText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setSubHeaderText(Ljava/lang/CharSequence;)V

    .line 134
    return-void
.end method

.method public setSubHeaderTypefaceSource(Ljava/lang/String;)V
    .locals 1
    .param p1, "typefaceSource"    # Ljava/lang/String;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setSubHeaderTypefaceSource(Ljava/lang/String;)V

    .line 142
    return-void
.end method

.method public setTabLayoutBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setTabLayoutBackgroundColor(I)V

    .line 114
    return-void
.end method

.method public setTabLayoutBackgroundResource(I)V
    .locals 1
    .param p1, "res"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setTabLayoutBackgroundResource(I)V

    .line 118
    return-void
.end method

.method public setTabLayoutBehaviorEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setTabLayoutBehaviorEnabled(Z)V

    .line 146
    return-void
.end method

.method public setTelemetrySetActiveTabCallback(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;)V
    .locals 0
    .param p2, "pageNames"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "telemetryCallbackFunction":Lcom/microsoft/xbox/toolkit/functions/GenericFunction;, "Lcom/microsoft/xbox/toolkit/functions/GenericFunction<Ljava/lang/Void;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->telemetrySetActiveTabCallback:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    .line 89
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->tabNames:[Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setTelemetryTabSelectCallback(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p2, "pageNames"    # [Ljava/lang/String;
    .param p3, "pageActions"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "telemetryCallbackFunction":Lcom/microsoft/xbox/toolkit/functions/GenericFunction;, "Lcom/microsoft/xbox/toolkit/functions/GenericFunction<Ljava/lang/Void;Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setTelemetryFunction(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public switchToPane(I)V
    .locals 0
    .param p1, "newIndex"    # I

    .prologue
    .line 503
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setCurrentPaneIndex(I)V

    .line 504
    return-void
.end method

.method public xleFindViewId(I)Landroid/view/View;
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 552
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getPageCount()I

    move-result v1

    .line 553
    .local v1, "paneCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 554
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->body:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    .line 555
    .local v2, "v":Landroid/view/View;
    instance-of v4, v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v4, :cond_0

    .line 556
    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .end local v2    # "v":Landroid/view/View;
    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->xleFindViewId(I)Landroid/view/View;

    move-result-object v3

    .line 557
    .local v3, "view":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 562
    .end local v3    # "view":Landroid/view/View;
    :goto_1
    return-object v3

    .line 553
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 562
    :cond_1
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->xleFindViewId(I)Landroid/view/View;

    move-result-object v3

    goto :goto_1
.end method
