.class Lcom/microsoft/xbox/service/model/ProfileModel$HeroStatComparator;
.super Ljava/lang/Object;
.source "ProfileModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HeroStatComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;)V
    .locals 0

    .prologue
    .line 4138
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$HeroStatComparator;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/ProfileModel$1;

    .prologue
    .line 4138
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel$HeroStatComparator;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)I
    .locals 5
    .param p1, "object1"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .param p2, "object2"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 4142
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 4163
    :cond_0
    :goto_0
    return v0

    .line 4144
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 4145
    goto :goto_0

    .line 4146
    :cond_2
    if-nez p2, :cond_3

    move v0, v2

    .line 4147
    goto :goto_0

    .line 4150
    :cond_3
    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    if-nez v3, :cond_4

    iget-object v3, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    if-eqz v3, :cond_0

    .line 4152
    :cond_4
    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    if-nez v3, :cond_5

    move v0, v1

    .line 4153
    goto :goto_0

    .line 4154
    :cond_5
    iget-object v3, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    if-nez v3, :cond_6

    move v0, v2

    .line 4155
    goto :goto_0

    .line 4158
    :cond_6
    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget v3, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->Ordinal:I

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget v4, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->Ordinal:I

    if-ge v3, v4, :cond_7

    move v0, v2

    .line 4159
    goto :goto_0

    .line 4160
    :cond_7
    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->Ordinal:I

    iget-object v3, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget v3, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->Ordinal:I

    if-le v2, v3, :cond_0

    move v0, v1

    .line 4161
    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 4138
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    check-cast p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/model/ProfileModel$HeroStatComparator;->compare(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)I

    move-result v0

    return v0
.end method
