.class Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;
.super Ljava/lang/Object;
.source "HomeScreenPopupScreenViewModelFullSnap.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/pins/LaunchableItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBoxArtBackgroundColor()I
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 2

    .prologue
    .line 93
    const/4 v0, 0x0

    .line 95
    .local v0, "ret":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->hasValidData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->hasValidData(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getHasActivities()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getFeaturedActivity()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    .line 100
    :cond_0
    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getIsMusicPlayList()Z
    .locals 1

    .prologue
    .line 141
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    return v0
.end method

.method public getIsTVChannel()Z
    .locals 1

    .prologue
    .line 136
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isTVChannel(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    return v0
.end method

.method public getIsWebLink()Z
    .locals 1

    .prologue
    .line 131
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isWebLink(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    return v0
.end method

.method public getItemId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLaunchUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    return-object v0
.end method

.method public getProviderMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->ProviderMediaId:Ljava/lang/String;

    goto :goto_0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getProviderTitleId()J
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getProviderTitleIdString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getShouldShowBackgroundColor()Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v0

    .line 106
    .local v0, "mediaModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->hasValidData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasCompanion()Z
    .locals 2

    .prologue
    .line 81
    const/4 v0, 0x0

    .line 83
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->hasValidData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->hasValidData(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getHasFeaturedActivity()Z

    move-result v0

    .line 88
    :cond_0
    return v0
.end method

.method public hasDetails()Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProvider()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDLC()Z
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return v0
.end method
