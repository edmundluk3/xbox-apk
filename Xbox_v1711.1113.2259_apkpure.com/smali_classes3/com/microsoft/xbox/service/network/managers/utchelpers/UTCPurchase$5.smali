.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;
.super Ljava/lang/Object;
.source "UTCPurchase.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->trackWebBlend(Ljava/lang/String;JLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$actionName:Ljava/lang/String;

.field final synthetic val$elapsedSeconds:J

.field final synthetic val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    iput-wide p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;->val$elapsedSeconds:J

    iput-object p4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;->val$actionName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 10

    .prologue
    .line 222
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 224
    .local v4, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v5, "NormalPurchase"

    .line 225
    .local v5, "purchaseType":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->isCodeRedeemable()Ljava/lang/Boolean;

    move-result-object v3

    .line 227
    .local v3, "isCodeRedemption":Ljava/lang/Boolean;
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 228
    const-string v5, "RedeemCode"

    .line 231
    :cond_0
    const-string v7, "PurchaseType"

    invoke-virtual {v4, v7, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 233
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseProductId()Ljava/lang/String;

    move-result-object v1

    .line 234
    .local v1, "catId":Ljava/lang/String;
    const-string v7, "BigCatId"

    invoke-virtual {v4, v7, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 236
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseSkuId()Ljava/lang/String;

    move-result-object v6

    .line 237
    .local v6, "skuId":Ljava/lang/String;
    const-string v7, "SkuId"

    invoke-virtual {v4, v7, v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 239
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseAvailabilityId()Ljava/lang/String;

    move-result-object v0

    .line 240
    .local v0, "availabilityId":Ljava/lang/String;
    const-string v7, "AvailabilityId"

    invoke-virtual {v4, v7, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 242
    const-string v7, "ElapsedTime"

    iget-wide v8, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;->val$elapsedSeconds:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 244
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseOriginatingPage()Ljava/lang/String;

    move-result-object v2

    .line 246
    .local v2, "fromPage":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;->val$actionName:Ljava/lang/String;

    invoke-static {v7, v2, v4}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 247
    return-void
.end method
