.class public final Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamEndpointFactory;
.super Ljava/lang/Object;
.source "BeamServiceModule_ProvideBeamEndpointFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final module:Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamEndpointFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamEndpointFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    sget-boolean v0, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamEndpointFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 16
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamEndpointFactory;->module:Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    .line 17
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;",
            ")",
            "Ldagger/internal/Factory",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamEndpointFactory;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamEndpointFactory;-><init>(Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;)V

    return-object v0
.end method

.method public static proxyProvideBeamEndpoint(Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;)Ljava/lang/String;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;->provideBeamEndpoint()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamEndpointFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamEndpointFactory;->module:Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    .line 22
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;->provideBeamEndpoint()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 21
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
