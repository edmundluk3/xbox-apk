.class Lcom/microsoft/xbox/xle/urc/net/BranchSession$17;
.super Ljava/lang/Object;
.source "BranchSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/net/BranchSession;->setState(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

.field final synthetic val$needToNotify:Z


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 633
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$17;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$17;->val$needToNotify:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 636
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$17;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$002(Lcom/microsoft/xbox/xle/urc/net/BranchSession;[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .line 637
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$17;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$102(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Z)Z

    .line 639
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$17;->val$needToNotify:Z

    if-eqz v1, :cond_0

    .line 640
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$17;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$200(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;

    .line 641
    .local v0, "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$17;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$000(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;->onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V

    goto :goto_0

    .line 644
    .end local v0    # "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
    :cond_0
    return-void
.end method
