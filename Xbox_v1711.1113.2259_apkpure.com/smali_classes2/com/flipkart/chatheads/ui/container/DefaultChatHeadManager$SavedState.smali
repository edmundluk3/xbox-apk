.class Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "DefaultChatHeadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private activeArrangement:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;"
        }
    .end annotation
.end field

.field private activeArrangementBundle:Landroid/os/Bundle;

.field private chatHeads:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<+",
            "Ljava/io/Serializable;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588
    new-instance v0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState$1;

    invoke-direct {v0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState$1;-><init>()V

    sput-object v0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 605
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 606
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->activeArrangement:Ljava/lang/Class;

    .line 607
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->activeArrangementBundle:Landroid/os/Bundle;

    .line 608
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashMap;

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->chatHeads:Ljava/util/LinkedHashMap;

    .line 609
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;

    .prologue
    .line 612
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 613
    return-void
.end method


# virtual methods
.method public getActiveArrangement()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 616
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->activeArrangement:Ljava/lang/Class;

    return-object v0
.end method

.method public getActiveArrangementBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->activeArrangementBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public getChatHeads()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<+",
            "Ljava/io/Serializable;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 640
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->chatHeads:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method public setActiveArrangement(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 620
    .local p1, "activeArrangement":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/flipkart/chatheads/ui/ChatHeadArrangement;>;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->activeArrangement:Ljava/lang/Class;

    .line 621
    return-void
.end method

.method public setActiveArrangementBundle(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activeArrangementBundle"    # Landroid/os/Bundle;

    .prologue
    .line 628
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->activeArrangementBundle:Landroid/os/Bundle;

    .line 629
    return-void
.end method

.method public setChatHeads(Ljava/util/LinkedHashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<+",
            "Ljava/io/Serializable;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 644
    .local p1, "chatHeads":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<+Ljava/io/Serializable;Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->chatHeads:Ljava/util/LinkedHashMap;

    .line 645
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 633
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 634
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->activeArrangement:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 635
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->activeArrangementBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 636
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->chatHeads:Ljava/util/LinkedHashMap;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 637
    return-void
.end method
