.class Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;
.super Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;
.source "ServiceProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->sendRequest(ILcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

.field final synthetic val$currentRequestId:I


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

    iput p2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->val$currentRequestId:I

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V
    .locals 6
    .param p1, "request"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    .prologue
    .line 92
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->access$000(Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->remove(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V

    .line 93
    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->getResponse()Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;

    move-result-object v1

    .line 95
    .local v1, "response":Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonServiceResponse;

    iget v2, v1, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;->statusCode:I

    iget-object v3, v1, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;->response:Ljava/lang/String;

    iget-object v4, v1, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;->headers:Ljava/util/Map;

    invoke-direct {v0, v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonServiceResponse;-><init>(ILjava/lang/String;Ljava/util/Map;)V

    .line 96
    .local v0, "jsonResponse":Lcom/microsoft/xbox/smartglass/controls/JsonServiceResponse;
    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;->isSuccess()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 97
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    iget v3, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->val$currentRequestId:I

    invoke-interface {v2, v3, v0}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(ILorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    .end local v0    # "jsonResponse":Lcom/microsoft/xbox/smartglass/controls/JsonServiceResponse;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

    const-string v4, "SendRequest"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->val$currentRequestId:I

    sget-object v5, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(Ljava/lang/String;ILjava/lang/String;)V

    .line 105
    return-void

    .line 99
    .restart local v0    # "jsonResponse":Lcom/microsoft/xbox/smartglass/controls/JsonServiceResponse;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    iget v3, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->val$currentRequestId:I

    invoke-interface {v2, v3, v0}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 101
    .end local v0    # "jsonResponse":Lcom/microsoft/xbox/smartglass/controls/JsonServiceResponse;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public onError(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;Ljava/lang/String;)V
    .locals 5
    .param p1, "request"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->access$000(Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->remove(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to send request. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    iget v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->val$currentRequestId:I

    invoke-interface {v1, v2, v0}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 112
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

    const-string v3, "SendRequest"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;->val$currentRequestId:I

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 113
    return-void
.end method
