.class Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "PartyDetailsHeaderViewHolder.java"


# instance fields
.field broadcastAlertCheckbox:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0851
    .end annotation
.end field

.field broadcastAlertContainer:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e084e
    .end annotation
.end field

.field broadcastAlertDescription:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0850
    .end annotation
.end field

.field description:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0844
    .end annotation
.end field

.field descriptionExpandIcon:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0841
    .end annotation
.end field

.field gamePic:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e083f
    .end annotation
.end field

.field headerView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e083e
    .end annotation
.end field

.field inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0846
    .end annotation
.end field

.field joinableToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e084a
    .end annotation
.end field

.field leaveButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e084d
    .end annotation
.end field

.field lfgButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0845
    .end annotation
.end field

.field mutePartyToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e084b
    .end annotation
.end field

.field name:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0840
    .end annotation
.end field

.field optionsButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0847
    .end annotation
.end field

.field optionsContainer:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0849
    .end annotation
.end field

.field optionsExpandIcon:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0848
    .end annotation
.end field

.field private optionsVisible:Z

.field private partyDetailsVisible:Z

.field private rxDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field status:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0842
    .end annotation
.end field

.field tags:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0843
    .end annotation
.end field

.field textChatButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e084c
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/microsoft/xbox/toolkit/generics/Action;Lio/reactivex/Observable;)V
    .locals 5
    .param p1, "itemView"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "intent":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;>;"
    .local p3, "viewStateStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;>;"
    const/4 v4, 0x0

    .line 106
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 100
    iput-boolean v4, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->partyDetailsVisible:Z

    .line 101
    iput-boolean v4, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsVisible:Z

    .line 103
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->rxDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 107
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->rxDisposable:Lio/reactivex/disposables/CompositeDisposable;

    const/16 v1, 0x8

    new-array v1, v1, [Lio/reactivex/disposables/Disposable;

    .line 110
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 111
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->lfgButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 115
    invoke-static {v3}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/generics/Action;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 116
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 117
    invoke-static {v3}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/generics/Action;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 118
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->joinableToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 119
    invoke-static {v3}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/generics/Action;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 120
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->mutePartyToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 121
    invoke-static {v3}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/generics/Action;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 122
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->textChatButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 123
    invoke-static {v3}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/generics/Action;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 124
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->leaveButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 125
    invoke-static {v3}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/generics/Action;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 126
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertCheckbox:Landroid/widget/CheckBox;

    .line 127
    invoke-static {v3}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p0, p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;Lcom/microsoft/xbox/toolkit/generics/Action;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 128
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    .line 109
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->addAll([Lio/reactivex/disposables/Disposable;)Z

    .line 130
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->renderHeader(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V

    .line 113
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->renderOptions(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V

    .line 114
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/toolkit/generics/Action;Ljava/lang/Object;)V
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/toolkit/generics/Action;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 116
    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$NavigateToLfgIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$NavigateToLfgIntent;

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/toolkit/generics/Action;Ljava/lang/Object;)V
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/toolkit/generics/Action;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 118
    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$InviteIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$InviteIntent;

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/toolkit/generics/Action;Ljava/lang/Object;)V
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/toolkit/generics/Action;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/toolkit/generics/Action;Ljava/lang/Object;)V
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/toolkit/generics/Action;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 122
    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyMuteIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyMuteIntent;

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/toolkit/generics/Action;Ljava/lang/Object;)V
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/toolkit/generics/Action;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TextChatIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TextChatIntent;

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/toolkit/generics/Action;Ljava/lang/Object;)V
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/toolkit/generics/Action;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 126
    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$LeavePartyIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$LeavePartyIntent;

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$new$7(Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;Lcom/microsoft/xbox/toolkit/generics/Action;Ljava/lang/Object;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;
    .param p1, "intent"    # Lcom/microsoft/xbox/toolkit/generics/Action;
    .param p2, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;->with(Z)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method private renderHeader(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V
    .locals 13
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    .prologue
    const/16 v7, 0x8

    const/4 v12, 0x1

    const/4 v6, 0x0

    .line 151
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->renderHeaderVisibility()V

    .line 152
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->renderLfgDetails(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V

    .line 154
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v4

    .line 155
    .local v4, "partySession":Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->getRoster()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    .line 156
    .local v3, "partyMembers":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 157
    iget-object v8, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->name:Landroid/widget/TextView;

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070a3f

    new-array v11, v12, [Ljava/lang/Object;

    invoke-virtual {v3, v6}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->gamertag()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v11, v6

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v5

    if-ne v5, v12, :cond_3

    .line 159
    iget-object v8, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->status:Landroid/widget/TextView;

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->isJoinable()Z

    move-result v5

    if-eqz v5, :cond_2

    const v5, 0x7f070a33

    :goto_1
    invoke-virtual {v9, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    :goto_2
    const/4 v0, 0x0

    .line 165
    .local v0, "broadcastCount":I
    const/4 v1, 0x0

    .line 166
    .local v1, "broadcaster":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v5

    :cond_0
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 167
    .local v2, "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isBroadcasting()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 168
    add-int/lit8 v0, v0, 0x1

    .line 169
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->gamertag()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 155
    .end local v0    # "broadcastCount":I
    .end local v1    # "broadcaster":Ljava/lang/String;
    .end local v2    # "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .end local v3    # "partyMembers":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 159
    .restart local v3    # "partyMembers":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :cond_2
    const v5, 0x7f070a2f

    goto :goto_1

    .line 161
    :cond_3
    iget-object v8, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->status:Landroid/widget/TextView;

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->isJoinable()Z

    move-result v5

    if-eqz v5, :cond_4

    const v5, 0x7f070a32

    :goto_4
    new-array v10, v12, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v6

    invoke-virtual {v9, v5, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    const v5, 0x7f070a2e

    goto :goto_4

    .line 173
    .restart local v0    # "broadcastCount":I
    .restart local v1    # "broadcaster":Ljava/lang/String;
    :cond_5
    if-nez v0, :cond_6

    .line 174
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertContainer:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 192
    .end local v0    # "broadcastCount":I
    .end local v1    # "broadcaster":Ljava/lang/String;
    :goto_5
    iget-object v8, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    if-eqz v4, :cond_9

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->meIsHost(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    move v5, v6

    :goto_6
    invoke-virtual {v8, v5}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 193
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->joinableToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    if-eqz v4, :cond_a

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->meIsHost(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    :goto_7
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 194
    return-void

    .line 176
    .restart local v0    # "broadcastCount":I
    .restart local v1    # "broadcaster":Ljava/lang/String;
    :cond_6
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertContainer:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 177
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->userAllowsBroadcasting()Z

    move-result v8

    invoke-virtual {v5, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 179
    if-ne v0, v12, :cond_7

    .line 180
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 181
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertDescription:Landroid/widget/TextView;

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f070a39

    new-array v10, v12, [Ljava/lang/Object;

    aput-object v1, v10, v6

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 183
    :cond_7
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertDescription:Landroid/widget/TextView;

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f070a13

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 187
    .end local v0    # "broadcastCount":I
    .end local v1    # "broadcaster":Ljava/lang/String;
    :cond_8
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->name:Landroid/widget/TextView;

    const-string v8, ""

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->status:Landroid/widget/TextView;

    const-string v8, ""

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertContainer:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    :cond_9
    move v5, v7

    .line 192
    goto :goto_6

    :cond_a
    move v6, v7

    .line 193
    goto :goto_7
.end method

.method private renderHeaderVisibility()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 197
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->tags:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->partyDetailsVisible:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->description:Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->partyDetailsVisible:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 199
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->descriptionExpandIcon:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->partyDetailsVisible:Z

    if-eqz v0, :cond_2

    const v0, 0x7f070ef6

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 200
    return-void

    :cond_0
    move v0, v2

    .line 197
    goto :goto_0

    :cond_1
    move v1, v2

    .line 198
    goto :goto_1

    .line 199
    :cond_2
    const v0, 0x7f070ef3

    goto :goto_2
.end method

.method private renderLfgDetails(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V
    .locals 4
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 203
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->gamePic:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->hasLFG()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->descriptionExpandIcon:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->hasLFG()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 205
    return-void

    :cond_0
    move v0, v2

    .line 203
    goto :goto_0

    :cond_1
    move v1, v2

    .line 204
    goto :goto_1
.end method

.method private renderOptions(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V
    .locals 4
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->renderOptionsVisibility()V

    .line 210
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v0

    .line 211
    .local v0, "partySession":Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    if-eqz v0, :cond_0

    .line 212
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->joinableToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->isJoinable()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070f79

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->joinableToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->isJoinable()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f070a1e

    :goto_1
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->mutePartyToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->isMuted()Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f071052

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->mutePartyToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->isMuted()Z

    move-result v1

    if-nez v1, :cond_4

    const v1, 0x7f070a37

    :goto_3
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 217
    :cond_0
    return-void

    .line 212
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f071039

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 213
    :cond_2
    const v1, 0x7f070a1f

    goto :goto_1

    .line 214
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07104f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 215
    :cond_4
    const v1, 0x7f070a46

    goto :goto_3
.end method

.method private renderOptionsVisibility()V
    .locals 2

    .prologue
    .line 220
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsContainer:Landroid/view/View;

    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsVisible:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 221
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsExpandIcon:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsVisible:Z

    if-eqz v0, :cond_1

    const v0, 0x7f070ef6

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 222
    return-void

    .line 220
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 221
    :cond_1
    const v0, 0x7f070ef3

    goto :goto_1
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->rxDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 135
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 136
    return-void
.end method

.method public toggleDetails()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0e083e
        }
    .end annotation

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->partyDetailsVisible:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->descriptionExpandIcon:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->partyDetailsVisible:Z

    .line 141
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->renderHeaderVisibility()V

    .line 142
    return-void

    .line 140
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggleOptions()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0e0847
        }
    .end annotation

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsVisible:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsExpandIcon:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsVisible:Z

    .line 147
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->renderOptionsVisibility()V

    .line 148
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
