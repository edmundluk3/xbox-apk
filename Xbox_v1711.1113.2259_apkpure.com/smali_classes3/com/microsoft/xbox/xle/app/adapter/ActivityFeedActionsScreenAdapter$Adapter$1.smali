.class Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter$1;
.super Ljava/lang/Object;
.source "ActivityFeedActionsScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->createSpinner()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;

    .prologue
    .line 498
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 501
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-static {}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->values()[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v1

    .line 502
    .local v1, "values":[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    if-ltz p3, :cond_1

    array-length v2, v1

    if-ge p3, v2, :cond_1

    .line 503
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1200()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 504
    invoke-static {p3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackDetailsFilter(I)V

    .line 505
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1202(Z)Z

    .line 507
    :cond_0
    aget-object v0, v1, p3

    .line 508
    .local v0, "listType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->displayActionList(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 510
    .end local v0    # "listType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    :cond_1
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 514
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1202(Z)Z

    .line 515
    return-void
.end method
