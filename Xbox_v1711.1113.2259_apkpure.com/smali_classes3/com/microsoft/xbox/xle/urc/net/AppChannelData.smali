.class public Lcom/microsoft/xbox/xle/urc/net/AppChannelData;
.super Ljava/lang/Object;
.source "AppChannelData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;,
        Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;,
        Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public imageOrientation:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

.field public shows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 6
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v3, "channelId"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData;->id:Ljava/lang/String;

    .line 22
    const-string v3, "imageOrientation"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData;->imageOrientation:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    .line 24
    const-string v3, "shows"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 25
    .local v2, "shows":Lorg/json/JSONArray;
    if-eqz v2, :cond_0

    .line 26
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData;->shows:Ljava/util/ArrayList;

    .line 28
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 29
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 31
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData;->shows:Ljava/util/ArrayList;

    new-instance v4, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 37
    .end local v0    # "count":I
    .end local v1    # "index":I
    :cond_0
    return-void

    .line 32
    .restart local v0    # "count":I
    .restart local v1    # "index":I
    :catch_0
    move-exception v3

    goto :goto_1
.end method
