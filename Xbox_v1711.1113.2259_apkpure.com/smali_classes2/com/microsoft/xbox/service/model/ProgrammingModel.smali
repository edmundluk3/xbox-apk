.class public Lcom/microsoft/xbox/service/model/ProgrammingModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "ProgrammingModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;,
        Lcom/microsoft/xbox/service/model/ProgrammingModel$DiscoverModelHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field private featuredItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private getDiscoverListRunner:Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel;->featuredItems:Ljava/util/ArrayList;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel;->isLoading:Z

    .line 46
    new-instance v0, Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;

    invoke-direct {v0, p0, p0}, Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;-><init>(Lcom/microsoft/xbox/service/model/ProgrammingModel;Lcom/microsoft/xbox/service/model/ProgrammingModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getDiscoverListRunner:Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;

    .line 47
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/ProgrammingModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/ProgrammingModel$1;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/ProgrammingModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/ProgrammingModel;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->clearObservers()V

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel$DiscoverModelHolder;->access$200()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    return-object v0
.end method

.method public static reset()V
    .locals 0

    .prologue
    .line 64
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel$DiscoverModelHolder;->access$300()V

    .line 65
    return-void
.end method


# virtual methods
.method public getFeaturedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel;->featuredItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getIsLoading()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel;->isLoading:Z

    return v0
.end method

.method public loadDiscoverList(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 80
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 81
    const-string v0, "ProgrammingModel"

    const-string v1, "load discover list"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->DiscoverData:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getDiscoverListRunner:Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 83
    return-void

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadFeaturedListSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getDiscoverListRunner:Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 87
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v4, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 89
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel;->isLoading:Z

    .line 90
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v3, :cond_0

    .line 91
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel;->lastRefreshTime:Ljava/util/Date;

    .line 92
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 94
    .local v0, "featuredItemsResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel;->featuredItems:Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 95
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel;->featuredItems:Ljava/util/ArrayList;

    .line 100
    .end local v0    # "featuredItemsResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_0
    :goto_1
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->DiscoverData:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v3, v4, v2}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    invoke-direct {v1, v3, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 101
    return-void

    :cond_1
    move v1, v3

    .line 87
    goto :goto_0

    .line 97
    .restart local v0    # "featuredItemsResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_2
    const-string v1, "ProgrammingModel"

    const-string v3, "Programming content didn\'t change, ignore"

    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
