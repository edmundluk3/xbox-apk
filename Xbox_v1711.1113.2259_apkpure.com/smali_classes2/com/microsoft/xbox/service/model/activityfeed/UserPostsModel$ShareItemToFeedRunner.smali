.class Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "UserPostsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShareItemToFeedRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final caption:Ljava/lang/String;

.field private final clubId:J

.field private final locator:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p2, "locator"    # Ljava/lang/String;
    .param p3, "caption"    # Ljava/lang/String;

    .prologue
    .line 123
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;-><init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Ljava/lang/String;Ljava/lang/String;J)V

    .line 124
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p2, "locator"    # Ljava/lang/String;
    .param p3, "caption"    # Ljava/lang/String;
    .param p4, "clubId"    # J

    .prologue
    .line 126
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 127
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->locator:Ljava/lang/String;

    .line 128
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->caption:Ljava/lang/String;

    .line 129
    iput-wide p4, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->clubId:J

    .line 130
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 138
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->clubId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 139
    invoke-static {}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->access$000()Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->locator:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->caption:Ljava/lang/String;

    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->clubId:J

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;->shareItemToClub(Ljava/lang/String;Ljava/lang/String;J)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    .line 141
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->access$000()Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->locator:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->caption:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;->shareItem(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->buildData()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 154
    const-wide/16 v0, 0x232a

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->ItemSharedToFeed:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;->this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 150
    :cond_0
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 134
    return-void
.end method
