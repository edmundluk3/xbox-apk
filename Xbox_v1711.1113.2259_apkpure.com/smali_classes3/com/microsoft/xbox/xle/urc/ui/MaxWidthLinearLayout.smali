.class public Lcom/microsoft/xbox/xle/urc/ui/MaxWidthLinearLayout;
.super Landroid/widget/LinearLayout;
.source "MaxWidthLinearLayout.java"


# instance fields
.field private final mMaxWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/MaxWidthLinearLayout;->mMaxWidth:I

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/MaxWidthLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/xle/urc/R$styleable;->MaxWidthLinearLayout:[I

    invoke-virtual {v5, p2, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 23
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->MaxWidthLinearLayout_maxWidth:I

    const v6, 0x7fffffff

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 24
    .local v2, "requestedMaxWidth":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 26
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 27
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    const-string v5, "window"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    .line 28
    .local v4, "windowManager":Landroid/view/WindowManager;
    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 29
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 32
    .local v3, "screenWidth":I
    if-ge v3, v2, :cond_0

    .line 33
    iput v3, p0, Lcom/microsoft/xbox/xle/urc/ui/MaxWidthLinearLayout;->mMaxWidth:I

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    iput v2, p0, Lcom/microsoft/xbox/xle/urc/ui/MaxWidthLinearLayout;->mMaxWidth:I

    goto :goto_0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 41
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 42
    .local v1, "measuredWidth":I
    iget v2, p0, Lcom/microsoft/xbox/xle/urc/ui/MaxWidthLinearLayout;->mMaxWidth:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/microsoft/xbox/xle/urc/ui/MaxWidthLinearLayout;->mMaxWidth:I

    if-ge v2, v1, :cond_0

    .line 43
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 44
    .local v0, "measureMode":I
    iget v2, p0, Lcom/microsoft/xbox/xle/urc/ui/MaxWidthLinearLayout;->mMaxWidth:I

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 46
    .end local v0    # "measureMode":I
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 47
    return-void
.end method
