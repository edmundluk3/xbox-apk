.class public Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;
.super Ljava/lang/Object;
.source "DetailPivotHeaderData.java"


# instance fields
.field private final pivotHeaderLayout:I

.field private final pivotHeaderViewModelClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 0
    .param p2, "headerLayoutID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 8
    .local p1, "pivotHeaderClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput p2, p0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;->pivotHeaderLayout:I

    .line 10
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;->pivotHeaderViewModelClass:Ljava/lang/Class;

    .line 11
    return-void
.end method


# virtual methods
.method public getPivotHeaderLayout()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;->pivotHeaderLayout:I

    return v0
.end method

.method public getViewModelClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;->pivotHeaderViewModelClass:Ljava/lang/Class;

    return-object v0
.end method
