.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Friends;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Friends"
.end annotation


# static fields
.field public static final FindFacebookFriends:Ljava/lang/String; = "Friends - Find Facebook Friends"

.field public static final FindPhoneContacts:Ljava/lang/String; = "Friends - Find Phone Contacts"

.field public static final FriendsFilter:Ljava/lang/String; = "Friends - Filter"

.field public static final FriendsSeeAll:Ljava/lang/String; = "Friends - See All"

.field public static final SelectClub:Ljava/lang/String; = "Friends - Select Club"

.field public static final SelectFriend:Ljava/lang/String; = "Friends - Select Friend"

.field public static final SuggestionsFilter:Ljava/lang/String; = "Friends - Suggestions Filter"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
