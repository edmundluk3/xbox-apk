.class Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ClubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdatePresenceRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final clubPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field private final service:Lcom/microsoft/xbox/service/clubs/IClubPresenceService;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

.field private final userId:J


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V
    .locals 2
    .param p2, "userId"    # J
    .param p4, "clubPresenceState"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .prologue
    .line 1406
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1401
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubPresenceService()Lcom/microsoft/xbox/service/clubs/IClubPresenceService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;->service:Lcom/microsoft/xbox/service/clubs/IClubPresenceService;

    .line 1407
    iput-wide p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;->userId:J

    .line 1408
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;->clubPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 1409
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .param p2, "x1"    # J
    .param p4, "x2"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    .param p5, "x3"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;

    .prologue
    .line 1400
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1417
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;->service:Lcom/microsoft/xbox/service/clubs/IClubPresenceService;

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$400(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)J

    move-result-wide v2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;->userId:J

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;->clubPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-interface/range {v1 .. v6}, Lcom/microsoft/xbox/service/clubs/IClubPresenceService;->updateClubPresence(JJLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1400
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$UpdatePresenceRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1426
    const-wide/16 v0, 0x2581

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1422
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1413
    return-void
.end method
