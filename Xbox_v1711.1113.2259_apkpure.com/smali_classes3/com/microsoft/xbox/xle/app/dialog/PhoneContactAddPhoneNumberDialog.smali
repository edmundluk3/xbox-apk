.class public Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "PhoneContactAddPhoneNumberDialog.java"


# instance fields
.field private changeRegionButton:Landroid/widget/RelativeLayout;

.field private closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private countryName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private phoneNumberEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private region:Ljava/lang/String;

.field private regionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 31
    const v0, 0x7f0301cb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->setContentView(I)V

    .line 33
    const v0, 0x7f0e0946

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 34
    const v0, 0x7f0e093f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 35
    const v0, 0x7f0e0944

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->regionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 36
    const v0, 0x7f0e0941

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->countryName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 37
    const v0, 0x7f0e0945

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->phoneNumberEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 38
    const v0, 0x7f0e0942

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->changeRegionButton:Landroid/widget/RelativeLayout;

    .line 39
    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;Landroid/view/View;)V
    .locals 10
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 46
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsNext()V

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getRegion()Ljava/lang/String;

    move-result-object v4

    .line 48
    .local v4, "oldRegion":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v8

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->region:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getContryCodeFromRegion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "countryCode":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->phoneNumberEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 50
    .local v5, "phoneNumber":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    move v1, v6

    .line 51
    .local v1, "isValidRegionAndPhoneNumber":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 52
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->normalizePhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 53
    .local v2, "normalized":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    move v1, v6

    .line 55
    .end local v2    # "normalized":Ljava/lang/String;
    :cond_0
    :goto_1
    if-nez v1, :cond_3

    .line 56
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v6

    const v8, 0x7f07053c

    invoke-virtual {v6, v8, v7}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V

    .line 64
    :goto_2
    return-void

    .end local v1    # "isValidRegionAndPhoneNumber":Z
    :cond_1
    move v1, v7

    .line 50
    goto :goto_0

    .restart local v1    # "isValidRegionAndPhoneNumber":Z
    .restart local v2    # "normalized":Ljava/lang/String;
    :cond_2
    move v1, v7

    .line 53
    goto :goto_1

    .line 59
    .end local v2    # "normalized":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 60
    .local v3, "oldNumber":Ljava/lang/String;
    if-eqz v4, :cond_4

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->region:Ljava/lang/String;

    invoke-static {v8, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    if-eqz v3, :cond_4

    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 61
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v8

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->region:Ljava/lang/String;

    invoke-virtual {v8, v9, v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->updateRegionAndPhoneNumber(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_5
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v8

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v8, p0, v6, v7}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->returnFromDialog(Lcom/microsoft/xbox/toolkit/XLEManagedDialog;Z[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 69
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsClose()V

    .line 70
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->dismiss()V

    .line 71
    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 76
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsChangeRegion()V

    .line 77
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;)V

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->show()V

    .line 78
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 0

    .prologue
    .line 131
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->dismiss()V

    .line 132
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v2, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->returnFromDialog(Lcom/microsoft/xbox/toolkit/XLEManagedDialog;Z[Ljava/lang/Object;)V

    .line 126
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->dismiss()V

    .line 127
    return-void
.end method

.method protected onStart()V
    .locals 5

    .prologue
    .line 43
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsAddPhoneView()V

    .line 44
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_0

    .line 45
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_1

    .line 68
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->changeRegionButton:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_2

    .line 75
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->changeRegionButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getRegion()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->region:Ljava/lang/String;

    .line 82
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->region:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 83
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "regionText":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->regionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    .end local v1    # "regionText":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->countryName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->region:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getCountryNameFromRegion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "number":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 91
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->phoneNumberEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    .line 93
    :cond_4
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->changeRegionButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->changeRegionButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    :cond_2
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 121
    return-void
.end method

.method public setRegionAndCountryName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "region"    # Ljava/lang/String;
    .param p2, "countryName"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 110
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->region:Ljava/lang/String;

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getContryCodeFromRegion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "regionText":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->regionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    .end local v0    # "regionText":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;->countryName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1, p2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 116
    return-void
.end method
