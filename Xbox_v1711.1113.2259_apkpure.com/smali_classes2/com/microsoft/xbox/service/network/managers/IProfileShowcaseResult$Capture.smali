.class public Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
.super Ljava/lang/Object;
.source "IProfileShowcaseResult.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/ICapture;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Capture"
.end annotation


# static fields
.field private static transient FALLBACK_DATE:Ljava/util/Date;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public capturerRealName:Ljava/lang/String;

.field protected gamertag:Ljava/lang/String;

.field public rawDate:Ljava/util/Date;

.field public scid:Ljava/lang/String;

.field public socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

.field public state:Ljava/lang/String;

.field public thumbnails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;",
            ">;"
        }
    .end annotation
.end field

.field public titleName:Ljava/lang/String;

.field public userCaption:Ljava/lang/String;

.field public views:I

.field public xuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->TAG:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->FALLBACK_DATE:Ljava/util/Date;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getClipName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->titleName:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->rawDate:Ljava/util/Date;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->rawDate:Ljava/util/Date;

    .line 108
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->FALLBACK_DATE:Ljava/util/Date;

    goto :goto_0
.end method

.method public getDurationInSeconds()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const-string v0, ""

    return-object v0
.end method

.method public getGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->gamertag:Ljava/lang/String;

    return-object v0
.end method

.method public getIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string v0, ""

    return-object v0
.end method

.method public getIsScreenshot()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getItemRoot()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const-string v0, ""

    return-object v0
.end method

.method public getSocialInfo()Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    return-object v0
.end method

.method public getUserCaption()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->userCaption:Ljava/lang/String;

    return-object v0
.end method

.method public getViews()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->views:I

    return v0
.end method

.method public getXuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->xuid:Ljava/lang/String;

    return-object v0
.end method
