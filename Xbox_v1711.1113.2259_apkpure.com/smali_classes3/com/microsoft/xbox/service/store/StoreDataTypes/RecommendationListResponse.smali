.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse;
.super Ljava/lang/Object;
.source "RecommendationListResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type shouldn\'t be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
