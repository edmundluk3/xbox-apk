.class public Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "MovieDetailsHeaderViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;",
        ">;"
    }
.end annotation


# instance fields
.field private detailTask:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;

.field private isLoadingDetail:Z

.field protected mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 24
    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    .line 30
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getMovieDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 31
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 32
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 33
    .local v1, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    .line 34
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$1;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;

    .line 35
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 36
    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->isLoadingDetail:Z

    return p1
.end method


# virtual methods
.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 132
    const v0, 0x7f070683

    return v0
.end method

.method public getMovieReleaseData()Ljava/lang/String;
    .locals 5

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getReleaseYear()Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getDurationInMinutes()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f071084

    .line 41
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    .line 39
    invoke-static {v1, v0, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 40
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getDurationInMinutes()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07061b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getMoviewExtraData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getHasTvShowtimeInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const-string v0, "TODO: Hookup to live data"

    .line 92
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRottenTomatoesReviewScore()F
    .locals 2

    .prologue
    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getRottenTomatoReviewSource()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;

    move-result-object v0

    .line 46
    .local v0, "reviewSource":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;
    if-eqz v0, :cond_0

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getRottenTomatoReviewSource()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;

    move-result-object v1

    iget v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;->ReviewScore:F

    .line 49
    :goto_0
    return v1

    :cond_0
    const/high16 v1, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->isLoadingDetail:Z

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->load(Z)V

    .line 77
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getMovieDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 58
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->cancel()V

    .line 67
    return-void
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    .line 82
    .local v0, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->MediaItemDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->updateAdapter()V

    .line 85
    :cond_0
    return-void
.end method
