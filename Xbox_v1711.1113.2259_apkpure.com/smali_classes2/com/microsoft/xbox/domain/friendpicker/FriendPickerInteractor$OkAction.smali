.class public final enum Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;
.super Ljava/lang/Enum;
.source "FriendPickerInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OkAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;",
        ">;",
        "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27
    new-instance v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;->INSTANCE:Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

    sget-object v1, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;->INSTANCE:Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;->$VALUES:[Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

    .line 29
    const-class v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;->$VALUES:[Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;

    return-object v0
.end method


# virtual methods
.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;->TAG:Ljava/lang/String;

    return-object v0
.end method
