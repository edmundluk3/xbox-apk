.class public abstract Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;
.super Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;
.source "HomeScreenPopupScreenViewModelContent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase",
        "<TT;>;"
    }
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private final excludeCommands:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;",
            ">;"
        }
    .end annotation
.end field

.field private launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

.field private loadModelTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent",
            "<TT;>.",
            "LoadMergedModelTask;"
        }
    .end annotation
.end field

.field private loadingModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field protected mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

.field private provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

.field protected final viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPins;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    return-void
.end method

.method public varargs constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;[Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .param p2, "excludeCommands"    # [Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .prologue
    .line 62
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;-><init>()V

    .line 57
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->loadModelTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;

    .line 58
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->loadingModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 63
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .line 64
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->excludeCommands:Ljava/util/Set;

    .line 72
    :cond_0
    return-void

    .line 67
    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->excludeCommands:Ljava/util/Set;

    .line 68
    array-length v2, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p2, v1

    .line 69
    .local v0, "cmd":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->excludeCommands:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 68
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->onModelLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    return-object v0
.end method

.method private addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V
    .locals 1
    .param p1, "cmd"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .prologue
    .line 617
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->excludeCommands:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->canAddIfRemote(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 618
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->commands:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 620
    :cond_0
    return-void
.end method

.method private addCommandsForActiveProvider()Z
    .locals 15

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    const/4 v14, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 467
    const/4 v4, 0x0

    .line 468
    .local v4, "ret":Z
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v8}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->hasProvider()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 469
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v8}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getContentType()Ljava/lang/String;

    move-result-object v0

    .line 470
    .local v0, "contentType":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v1

    .line 471
    .local v1, "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getModels()[Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v3

    .line 472
    .local v3, "npms":[Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v3, :cond_0

    .line 473
    array-length v9, v3

    move v8, v7

    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v2, v3, v8

    .line 474
    .local v2, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v10}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v10

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getProviderTitleId()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-nez v10, :cond_8

    .line 475
    sget-object v8, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$6;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingState()Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 553
    .end local v0    # "contentType":Ljava/lang/String;
    .end local v1    # "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    .end local v2    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .end local v3    # "npms":[Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .end local v4    # "ret":Z
    :cond_0
    :goto_1
    return v4

    .line 478
    .restart local v0    # "contentType":Ljava/lang/String;
    .restart local v1    # "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    .restart local v2    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .restart local v3    # "npms":[Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .restart local v4    # "ret":Z
    :pswitch_0
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getProviderTitleId()J

    move-result-wide v8

    const-wide/32 v10, 0x3d8b930f

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    .line 479
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isWebLink(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 480
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v8}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getItemId()Ljava/lang/String;

    move-result-object v5

    .line 481
    .local v5, "url":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/SessionModel;->getLastKnownIeUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 482
    const-string v7, "Popover"

    const-string v8, "IE is playing the exact url, only show play button to switch snap/fill"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addPlayButton(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .line 484
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->REMOTE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 485
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addUnSnapButton(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    move v4, v6

    .line 487
    goto :goto_1

    .line 492
    .end local v5    # "url":Ljava/lang/String;
    :cond_1
    sget-object v8, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-eq v1, v8, :cond_2

    .line 493
    const-string v6, "Popover"

    const-string v8, "now playing is app, launchable is not dapp, add play button later"

    invoke-static {v6, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v7

    .line 494
    goto :goto_1

    .line 496
    :cond_2
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getProviderTitleId()J

    move-result-wide v8

    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v10}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-nez v8, :cond_3

    .line 497
    const-string v7, "Popover"

    const-string v8, "now playing is app, launchable is same app, don\'t app play button"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addPlayButton(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .line 499
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->REMOTE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 500
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addUnSnapButton(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    move v4, v6

    .line 501
    goto :goto_1

    .line 504
    :cond_3
    const-string v6, "Popover"

    const-string v8, "now playing is app, launchable is different, add play button later"

    invoke-static {v6, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v7

    .line 505
    goto :goto_1

    .line 508
    :pswitch_1
    sget-object v8, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-eq v1, v8, :cond_4

    sget-object v8, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGameDemo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-eq v1, v8, :cond_4

    .line 509
    const-string v6, "Popover"

    const-string v8, "now playing is game, launchable is not game, add play button later"

    invoke-static {v6, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v7

    .line 510
    goto/16 :goto_1

    .line 512
    :cond_4
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getProviderTitleId()J

    move-result-wide v8

    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v10}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-nez v8, :cond_5

    .line 513
    const-string v7, "Popover"

    const-string v8, "now playing is game, launchable is same game, don\'t app play button"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addPlayButton(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .line 515
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addUnSnapButton(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    move v4, v6

    .line 516
    goto/16 :goto_1

    .line 519
    :cond_5
    const-string v6, "Popover"

    const-string v8, "now playing is game, launchable is different game, add play button later"

    invoke-static {v6, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v7

    .line 520
    goto/16 :goto_1

    .line 524
    :pswitch_2
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v8}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v8

    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v10}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderMediaId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v14, v10}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->isTitlePlayingSameContent(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 525
    const-string v7, "Popover"

    const-string v8, "now playing is same music, don\'t add any button"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->REMOTE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 527
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addUnSnapButton(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    move v4, v6

    .line 528
    goto/16 :goto_1

    :cond_6
    move v4, v7

    .line 530
    goto/16 :goto_1

    .line 534
    :pswitch_3
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v8}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v8

    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v10}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderMediaId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v14, v10}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->isTitlePlayingSameContent(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 536
    const-string v7, "Popover"

    const-string v8, "now playing is video, launch is the same video, don\'t add play button"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addPlayButton(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .line 539
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addUnSnapButton(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    move v4, v6

    .line 540
    goto/16 :goto_1

    :cond_7
    move v4, v7

    .line 542
    goto/16 :goto_1

    .line 473
    :cond_8
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 475
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private addOptionalPlayToSnap()V
    .locals 1

    .prologue
    .line 567
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isSnappable(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->isVideoRelated(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 568
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_SNAP:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 570
    :cond_0
    return-void
.end method

.method private addOptionalPlayToXboxOne()V
    .locals 2

    .prologue
    .line 557
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 558
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_XBOX_ONE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 564
    :cond_1
    :goto_0
    return-void

    .line 559
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getLaunchUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 560
    const-string v0, "HomeScreenPopup"

    const-string v1, "use launch uri"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_XBOX_ONE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    goto :goto_0
.end method

.method private addPlayButton(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V
    .locals 0
    .param p1, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 458
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    return-void
.end method

.method private addUnSnapButton(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V
    .locals 1
    .param p1, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 461
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->canUnsnap()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->UNSNAP:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 464
    :cond_0
    return-void
.end method

.method private static canAddIfRemote(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)Z
    .locals 1
    .param p0, "cmd"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .prologue
    .line 629
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->REMOTE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    if-ne p0, v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getItemIdForPinLookup()Ljava/lang/String;
    .locals 2

    .prologue
    .line 428
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->ensureMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 429
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_1

    .line 430
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    .line 432
    :goto_0
    return-object v1

    .line 430
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPackageFamilyName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 432
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getItemId()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getLaunchPostActionRunnable(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)Ljava/lang/Runnable;
    .locals 8
    .param p1, "launchableItem"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .param p2, "cmd"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .prologue
    .line 312
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    const/4 v0, 0x0

    .line 313
    .local v0, "action":Ljava/lang/Runnable;
    invoke-interface {p1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v4

    const/4 v6, 0x0

    invoke-interface {p1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderMediaId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->isTitlePlayingSameContent(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 314
    .local v2, "contentPlaying":Z
    if-nez v2, :cond_1

    invoke-static {p1}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isGame(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v1, 0x1

    .line 316
    .local v1, "canLaunchRemote":Z
    :goto_0
    invoke-interface {p1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->hasCompanion()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 318
    const-wide/32 v4, 0x3d705025

    invoke-interface {p1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 319
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$2;

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$1;

    invoke-direct {v5, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)V

    invoke-direct {v3, p0, v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 332
    .local v3, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$3;

    .end local v0    # "action":Ljava/lang/Runnable;
    invoke-direct {v0, p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$3;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;)V

    .line 366
    .end local v3    # "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    .restart local v0    # "action":Ljava/lang/Runnable;
    :cond_0
    :goto_1
    return-object v0

    .line 314
    .end local v1    # "canLaunchRemote":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 339
    .restart local v1    # "canLaunchRemote":Z
    :cond_2
    invoke-interface {p1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v4

    const-wide/32 v6, 0x162615ad

    cmp-long v4, v4, v6

    if-nez v4, :cond_4

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_XBOX_ONE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    if-eq p2, v4, :cond_3

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_FILL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    if-eq p2, v4, :cond_3

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_FULL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    if-eq p2, v4, :cond_3

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_SNAP:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    if-ne p2, v4, :cond_4

    .line 341
    :cond_3
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$4;

    .end local v0    # "action":Ljava/lang/Runnable;
    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$4;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;)V

    .restart local v0    # "action":Ljava/lang/Runnable;
    goto :goto_1

    .line 355
    :cond_4
    if-eqz v1, :cond_0

    .line 356
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$5;

    .end local v0    # "action":Ljava/lang/Runnable;
    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$5;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;)V

    .restart local v0    # "action":Ljava/lang/Runnable;
    goto :goto_1
.end method

.method protected static getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .locals 1
    .param p0, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ")",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
            "<+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            "+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 374
    const/4 v0, 0x0

    .line 375
    .local v0, "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    if-eqz p0, :cond_0

    .line 376
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 378
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_0
    return-object v0
.end method

.method private getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .locals 1

    .prologue
    .line 590
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    if-nez v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    return-object v0
.end method

.method protected static hasValidData(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)Z
    .locals 1
    .param p0, "model"    # Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .prologue
    .line 441
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->hasValidData()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static hasValidData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
            "<+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            "+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 437
    .local p0, "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->hasValidData()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isConnected()Z
    .locals 2

    .prologue
    .line 633
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isVideoRelated(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Z
    .locals 2
    .param p0, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 573
    const/4 v0, 0x0

    .line 574
    .local v0, "ret":Z
    if-eqz p0, :cond_0

    .line 575
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 585
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 581
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 575
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static launchPinWithoutProvider(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V
    .locals 5
    .param p0, "launchableItem"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .param p1, "location"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    .param p2, "postAction"    # Ljava/lang/Runnable;

    .prologue
    .line 598
    const/4 v0, 0x0

    .line 599
    .local v0, "url":Ljava/lang/String;
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getIsMusicPlayList()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 601
    const-string v1, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0x18ffc9f4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderMediaId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "MusicPlaylist"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 608
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 609
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "launch url "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    invoke-static {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    .line 614
    :goto_1
    return-void

    .line 603
    :cond_1
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getIsWebLink()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 605
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getItemId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 612
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "don\'t know how to launch "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getContentType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private onModelLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 637
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$6;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 650
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->loadModelTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;

    .line 651
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->updateView()V

    .line 652
    return-void

    .line 641
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->hasValidData(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->loadingModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 645
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->loadingModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 646
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->loadingModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 637
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private repopulateCommands()V
    .locals 8

    .prologue
    .line 384
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->commands:Ljava/util/ArrayList;

    .line 385
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->computeHasState()Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v0

    .line 386
    .local v0, "hasState":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    if-eqz v3, :cond_7

    .line 387
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommandsForActiveProvider()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 388
    sget-object v3, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_COMPANION:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    if-ne v0, v3, :cond_0

    .line 389
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->SHOW_COMPANION:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 398
    :cond_0
    :goto_0
    sget-object v3, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_ONEGUIDE:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    if-ne v0, v3, :cond_1

    .line 399
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->ONE_GUIDE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 400
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3}, Lcom/microsoft/xbox/XLEApplication;->shouldShowStreamingButton()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 401
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->STREAM_TV:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 404
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v3}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->isDLC()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 405
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->GET_ON_XBOX_ONE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 408
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v3}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->hasDetails()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isOneGuide(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 409
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->SHOW_DETAILS_PAGE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 412
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 413
    .local v1, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_5

    .line 414
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v2

    .line 415
    .local v2, "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-eq v2, v3, :cond_4

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGameDemo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-ne v2, v3, :cond_5

    .line 416
    :cond_4
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->LAUNCH_GAMEPROFILE_PAGE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 419
    .end local v2    # "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getItemIdForPinLookup()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->hasPin(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v3}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v4

    const-wide/32 v6, 0x162615ad

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    .line 420
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getItemIdForPinLookup()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->hasPin(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->REMOVE_PIN:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    :goto_1
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 423
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_7
    return-void

    .line 392
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isOneGuide(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 394
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addOptionalPlayToXboxOne()V

    .line 396
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->addOptionalPlayToSnap()V

    goto/16 :goto_0

    .line 420
    .restart local v1    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_9
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->ADD_PIN:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    goto :goto_1
.end method

.method private updateView()V
    .locals 0

    .prologue
    .line 370
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->updateAdapter()V

    .line 371
    return-void
.end method


# virtual methods
.method protected final computeHasState()Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    .locals 1

    .prologue
    .line 308
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->computeHasState(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getLaunchableItem()Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
.end method

.method protected abstract getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.end method

.method protected abstract getMediaItemForDetails()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.end method

.method public handleCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V
    .locals 14
    .param p1, "cmd"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .prologue
    .line 124
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v7

    .line 125
    .local v7, "popup":Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    const/4 v2, 0x0

    .line 126
    .local v2, "location":Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$6;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$HomeScreenPopupScreenViewModelBase$Command:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 284
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 285
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 286
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v9}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getLaunchUri()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_a

    .line 287
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v9}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getLaunchUri()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-direct {p0, v10, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getLaunchPostActionRunnable(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)Ljava/lang/Runnable;

    move-result-object v10

    invoke-static {v9, v2, v10}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    .line 305
    :cond_1
    :goto_1
    return-void

    .line 128
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .line 129
    goto :goto_0

    .line 133
    :pswitch_1
    iget v9, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->titleResId:I

    int-to-long v10, v9

    sget-object v9, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Fill:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v10, v11, v9}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackPlayOnXboxAction(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V

    .line 137
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Fill:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .line 138
    goto :goto_0

    .line 142
    :pswitch_2
    iget v9, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->titleResId:I

    int-to-long v10, v9

    sget-object v9, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v10, v11, v9}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackPlayOnXboxAction(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V

    .line 144
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .line 145
    goto :goto_0

    .line 149
    :pswitch_3
    iget v9, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->titleResId:I

    int-to-long v10, v9

    sget-object v9, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Fill:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v10, v11, v9}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackPlayOnXboxAction(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V

    .line 151
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Fill:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .line 152
    goto :goto_0

    .line 156
    :pswitch_4
    iget v9, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->titleResId:I

    int-to-long v10, v9

    sget-object v9, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Snapped:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v10, v11, v9}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackPlayOnXboxAction(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V

    .line 158
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Snapped:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .line 159
    goto :goto_0

    .line 161
    :pswitch_5
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 162
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;

    .line 164
    .local v0, "destScreen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v9

    const-string v10, "OneGuide Pin Launch"

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;)V

    .line 167
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v0, v10}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 168
    :catch_0
    move-exception v1

    .line 169
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Failed to navigate to screen \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 173
    .end local v0    # "destScreen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    .end local v1    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :pswitch_6
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 174
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 175
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->trackStreamLaunchAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->streamChannel(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 178
    :cond_2
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    const-string v10, "Attempted to stream TV without any providers!"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 182
    :pswitch_7
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 183
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v9}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 185
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v9

    const-string v10, "Now Playing Companion"

    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v11}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v11

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ID:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v9}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v9

    invoke-static {v9}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchNativeOrHtmlCompanion(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    goto/16 :goto_0

    .line 188
    :cond_3
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    const-string v10, "No companion to launch"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 194
    :pswitch_8
    iget v9, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->titleResId:I

    int-to-long v10, v9

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackShowDetailsAction(J)V

    .line 196
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 197
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v3

    .line 198
    .local v3, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v3, :cond_5

    .line 199
    instance-of v9, v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    if-eqz v9, :cond_4

    .line 200
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getMediaItemForDetails()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v3

    .line 203
    :cond_4
    if-eqz v3, :cond_0

    .line 204
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto/16 :goto_0

    .line 207
    :cond_5
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    const-string v10, "No media item"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 213
    .end local v3    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :pswitch_9
    iget v9, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->titleResId:I

    int-to-long v10, v9

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackShowGameProfileAction(J)V

    .line 215
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 216
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v3

    .line 217
    .restart local v3    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v3, :cond_7

    .line 218
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-gtz v9, :cond_6

    .line 219
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 220
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v10

    invoke-virtual {v3, v10, v11}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setNowPlayingTitleId(J)V

    .line 223
    :cond_6
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-lez v9, :cond_0

    .line 224
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->navigateToGameProfile(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto/16 :goto_0

    .line 227
    :cond_7
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    const-string v10, "No media item"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 233
    .end local v3    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :pswitch_a
    iget v9, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->titleResId:I

    int-to-long v10, v9

    const/4 v9, 0x0

    invoke-static {v10, v11, v9}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackPinToggleAction(JZ)V

    .line 235
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 236
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    if-eqz v9, :cond_0

    .line 237
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getItemIdForPinLookup()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->getPin(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v6

    .line 238
    .local v6, "pin":Lcom/microsoft/xbox/service/model/pins/PinItem;
    if-eqz v6, :cond_8

    .line 239
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v9

    const-string v10, "Unpin"

    iget-object v11, v6, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    invoke-virtual {v9, v6}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->removePin(Lcom/microsoft/xbox/service/model/pins/PinItem;)V

    goto/16 :goto_0

    .line 242
    :cond_8
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    const-string v10, "No item to unpin"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 249
    .end local v6    # "pin":Lcom/microsoft/xbox/service/model/pins/PinItem;
    :pswitch_b
    iget v9, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->titleResId:I

    int-to-long v10, v9

    const/4 v9, 0x0

    invoke-static {v10, v11, v9}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackPinToggleAction(JZ)V

    .line 251
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 252
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    if-eqz v9, :cond_0

    .line 253
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v5

    .line 254
    .local v5, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v5, :cond_9

    .line 255
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v9

    const-string v10, "Pin"

    iget-object v11, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->addPin(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto/16 :goto_0

    .line 258
    :cond_9
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    const-string v10, "No media item to pin"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 263
    .end local v5    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :pswitch_c
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 264
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showRemoteControl()V

    goto/16 :goto_0

    .line 268
    :pswitch_d
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 269
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->unSnap()V

    .line 271
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v9

    const-string v10, "Unsnap"

    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v11}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getItemId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 288
    :cond_a
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v9}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->hasDetails()Z

    move-result v9

    if-nez v9, :cond_b

    .line 289
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-direct {p0, v10, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getLaunchPostActionRunnable(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)Ljava/lang/Runnable;

    move-result-object v10

    invoke-static {v9, v2, v10}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchPinWithoutProvider(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    goto/16 :goto_1

    .line 291
    :cond_b
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v8

    .line 292
    .local v8, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    if-eqz v8, :cond_d

    .line 293
    const-string v9, "HomeScreenPopup"

    const-string v10, "no launch uri, construct one"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v9

    invoke-static {v9}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v4

    .line 295
    .local v4, "mediaModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    if-eqz v4, :cond_c

    .line 296
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v10

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v11

    invoke-static {v8, v9, v10, v11, v2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Ljava/lang/String;IILcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V

    goto/16 :goto_1

    .line 298
    :cond_c
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    const-string v10, "No media model found"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 301
    .end local v4    # "mediaModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_d
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    const-string v10, "No provider found"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 126
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 113
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->load(Z)V

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->loadModelTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->loadModelTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->cancel()V

    .line 117
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->loadModelTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->loadModelTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->load(Z)V

    .line 119
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->loadingModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 120
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 76
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->onStartOverride()V

    .line 80
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getLaunchableItem()Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->convertToActiveIfNowPlaying(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .line 82
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->repopulateCommands()V

    .line 84
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->createScreenAdapter()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 85
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->updateView()V

    .line 87
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 88
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 89
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 99
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 100
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 101
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->onStopOverride()V

    .line 102
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>;"
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 108
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->updateView()V

    .line 109
    return-void
.end method
