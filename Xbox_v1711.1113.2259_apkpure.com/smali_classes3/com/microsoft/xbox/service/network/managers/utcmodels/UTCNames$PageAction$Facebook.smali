.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Facebook;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Facebook"
.end annotation


# static fields
.field public static final AddFacebookFriend:Ljava/lang/String; = "Facebook - Add Facebook Friend"

.field public static final Login:Ljava/lang/String; = "Facebook - Login"

.field public static final Logout:Ljava/lang/String; = "Facebook - Logout"

.field public static final RepairFacebookFriend:Ljava/lang/String; = "Facebook - Repair"

.field public static final ShareCancel:Ljava/lang/String; = "Facebook - Upsell Cancel"

.field public static final ShareSuccess:Ljava/lang/String; = "Facebook - Upsell Success"

.field public static final ViewSuggestedFriends:Ljava/lang/String; = "Facebook - View Suggested Friends"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
