.class Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;
.super Ljava/lang/Object;
.source "TitlePickerScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;

.field final synthetic val$viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;->val$viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClear()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->access$002(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;Z)Z

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;->val$viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->clearSearchResults()V

    .line 99
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "newText"    # Ljava/lang/CharSequence;

    .prologue
    .line 76
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;->val$viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->clearSearchResults()V

    .line 82
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->access$002(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;Z)Z

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;->val$viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->loadAutoSuggestAsync(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/CharSequence;

    .prologue
    .line 86
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackDiscoverExecuteGamesSearch()V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;->val$viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->loadSearchResultsAsync(Ljava/lang/String;)V

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->access$002(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;Z)Z

    .line 92
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 93
    return-void
.end method
