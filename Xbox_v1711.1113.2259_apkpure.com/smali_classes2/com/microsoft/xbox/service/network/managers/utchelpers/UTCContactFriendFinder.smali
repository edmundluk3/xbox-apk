.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;
.super Ljava/lang/Object;
.source "UTCContactFriendFinder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackContactsAddFriends(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "selectedXUIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$8;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$8;-><init>(Ljava/util/ArrayList;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 88
    return-void
.end method

.method public static trackContactsAddPhoneView()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$1;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 22
    return-void
.end method

.method public static trackContactsCallme()V
    .locals 1

    .prologue
    .line 227
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$24;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$24;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 233
    return-void
.end method

.method public static trackContactsCancel()V
    .locals 1

    .prologue
    .line 200
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$21;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$21;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 206
    return-void
.end method

.method public static trackContactsChangeRegion()V
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$14;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$14;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 143
    return-void
.end method

.method public static trackContactsChangeRegionView()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$2;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 31
    return-void
.end method

.method public static trackContactsClose()V
    .locals 1

    .prologue
    .line 209
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$22;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$22;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 215
    return-void
.end method

.method public static trackContactsDontAsk()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$9;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$9;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 97
    return-void
.end method

.method public static trackContactsErrorView()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$3;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$3;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 40
    return-void
.end method

.method public static trackContactsFindFriendsView()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$4;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$4;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 49
    return-void
.end method

.method public static trackContactsInviteFriendsView()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$5;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$5;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 58
    return-void
.end method

.method public static trackContactsLinkAction()V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$13;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$13;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 134
    return-void
.end method

.method public static trackContactsNext()V
    .locals 1

    .prologue
    .line 164
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$17;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$17;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 170
    return-void
.end method

.method public static trackContactsOK()V
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$10;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$10;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 106
    return-void
.end method

.method public static trackContactsOptInView()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$6;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$6;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 67
    return-void
.end method

.method public static trackContactsResendCode()V
    .locals 1

    .prologue
    .line 218
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$23;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$23;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 224
    return-void
.end method

.method public static trackContactsSendInvitation()V
    .locals 1

    .prologue
    .line 191
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$20;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$20;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 197
    return-void
.end method

.method public static trackContactsShowInvitation()V
    .locals 1

    .prologue
    .line 182
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$19;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$19;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 188
    return-void
.end method

.method public static trackContactsSkip()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$11;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$11;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 115
    return-void
.end method

.method public static trackContactsUnlink()V
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$15;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$15;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 152
    return-void
.end method

.method public static trackContactsUnlinkConfirmed()V
    .locals 1

    .prologue
    .line 155
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$16;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$16;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 161
    return-void
.end method

.method public static trackContactsVerifyPhone()V
    .locals 1

    .prologue
    .line 118
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$12;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$12;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 124
    return-void
.end method

.method public static trackContactsVerifyPhoneView()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$7;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$7;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 76
    return-void
.end method

.method public static trackOptOut()V
    .locals 1

    .prologue
    .line 173
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$18;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder$18;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 179
    return-void
.end method
