.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "GameProfileCapturesScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;
.implements Lcom/microsoft/xbox/xle/viewmodel/ICaptureScreenControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;
    }
.end annotation


# static fields
.field private static final GAME_PROFILE_CAPTURES_FILTER:Ljava/lang/String; = "GameProfileCapturesFilter"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private capture:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation
.end field

.field private isFilterChanged:Z

.field private isLoadingCaptures:Z

.field private lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

.field private likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;

.field private loadingCapturesAsyncTask:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;",
            ">;"
        }
    .end annotation
.end field

.field private model:Lcom/microsoft/xbox/service/model/TitleModel;

.field private realnames:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private titleId:J

.field private updateLikeControl:Z

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 8
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    const/4 v4, 0x0

    .line 74
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 63
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 64
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->loadingCapturesAsyncTask:Ljava/util/Hashtable;

    .line 65
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->capture:Ljava/util/ArrayList;

    .line 68
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->isFilterChanged:Z

    .line 69
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 82
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->realnames:Ljava/util/Hashtable;

    .line 75
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 76
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 77
    .local v0, "gameItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    :goto_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->titleId:J

    .line 78
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->titleId:J

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 79
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 80
    return-void

    .line 77
    :cond_0
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v2

    goto :goto_0

    :cond_1
    move v2, v4

    .line 78
    goto :goto_1
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->onLoadCapturesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;)V

    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->isLoadingCaptures:Z

    return p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Ljava/util/Hashtable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->realnames:Ljava/util/Hashtable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->onLikeCapturesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V

    return-void
.end method

.method static synthetic lambda$navigateToScreenshot$0(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)V
    .locals 0
    .param p0, "screenshot1"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 149
    return-void
.end method

.method static synthetic lambda$navigateToScreenshot$1(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 150
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to increment screenshot count"

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static navigateToScreenshot(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Z
    .locals 4
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "screenshot"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .prologue
    .line 131
    const/4 v0, 0x0

    .line 132
    .local v0, "ret":Z
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->getScreenshotUriForNavigation(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;

    move-result-object v1

    .line 133
    .local v1, "screenshotUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    if-eqz v1, :cond_0

    .line 134
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;->uri:Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedImages(Ljava/util/List;)V

    .line 135
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;)V

    .line 137
    const/4 v0, 0x1

    .line 139
    :cond_0
    return v0
.end method

.method private onLikeCapturesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V
    .locals 7
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "newLikeState"    # Z
    .param p3, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 524
    const-string v0, "GameProfileCapturesViewModel"

    const-string v1, "onLikeCapturesCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne p1, v0, :cond_0

    .line 526
    if-eqz p2, :cond_1

    .line 527
    const-string v0, "LikeClickAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Like XUID:%s GameClipID: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->xuid:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p3}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->updateAdapter()V

    .line 534
    return-void

    .line 529
    :cond_1
    const-string v0, "LikeClickAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Unlike XUID:%s GameClipID: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->xuid:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p3}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onLoadCapturesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;)V
    .locals 7
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    .prologue
    .line 230
    const-string v3, "GameProfileCaptures ViewModel"

    const-string v4, "onLoadCapturesCompleted"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 299
    :cond_0
    :goto_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->isLoadingCaptures:Z

    .line 300
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->updateViewModelState()V

    .line 301
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->updateAdapter()V

    .line 302
    return-void

    .line 236
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->capture:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 237
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$GameProfileCapturesFilter:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 276
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->capture:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 277
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)V

    .line 289
    .local v1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->capture:Ljava/util/ArrayList;

    invoke-static {v3, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    .line 239
    .end local v1    # "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/TitleModel;->getCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    .line 240
    .local v0, "captures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    if-eqz v0, :cond_2

    .line 241
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ICapture;

    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/ICapture;
    move-object v3, v2

    .line 242
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->realnames:Ljava/util/Hashtable;

    invoke-interface {v2}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getXuid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->capturerRealName:Ljava/lang/String;

    .line 243
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->capture:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 246
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ICapture;
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/TitleModel;->getCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    .line 247
    if-eqz v0, :cond_1

    .line 248
    if-eqz v0, :cond_1

    .line 249
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ICapture;

    .restart local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ICapture;
    move-object v3, v2

    .line 250
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->realnames:Ljava/util/Hashtable;

    invoke-interface {v2}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getXuid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->capturerRealName:Ljava/lang/String;

    .line 251
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->capture:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 257
    .end local v0    # "captures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ICapture;
    :pswitch_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/TitleModel;->getCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    .line 258
    .restart local v0    # "captures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    if-eqz v0, :cond_1

    .line 259
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ICapture;

    .restart local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ICapture;
    move-object v3, v2

    .line 260
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->realnames:Ljava/util/Hashtable;

    invoke-interface {v2}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getXuid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->capturerRealName:Ljava/lang/String;

    .line 261
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->capture:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 266
    .end local v0    # "captures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ICapture;
    :pswitch_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/TitleModel;->getCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    .line 267
    .restart local v0    # "captures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    if-eqz v0, :cond_1

    .line 268
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ICapture;

    .restart local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ICapture;
    move-object v3, v2

    .line 269
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->realnames:Ljava/util/Hashtable;

    invoke-interface {v2}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getXuid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->capturerRealName:Ljava/lang/String;

    .line 270
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->capture:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 295
    .end local v0    # "captures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ICapture;
    :pswitch_4
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 296
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->capture:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 232
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 237
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->capture:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->isLoadingCaptures:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 227
    :goto_1
    return-void

    .line 223
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 225
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1
.end method


# virtual methods
.method public deleteActivityFeedItem(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "feedItemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 199
    const-string v0, "Cannot delete item from here"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 200
    return-void
.end method

.method public getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;
    .locals 2

    .prologue
    .line 100
    const-string v0, "GameProfileCapturesFilter"

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->Everything:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/FilterPreferences;->get(Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    return-object v0
.end method

.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->capture:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLastSelectedCapture()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 95
    .local v0, "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 96
    return-object v0
.end method

.method public getNoContentText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0705ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->isLoadingCaptures:Z

    return v0
.end method

.method public likeClick(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 5
    .param p1, "uncastItem"    # Ljava/lang/Object;
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 457
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 458
    .local v0, "gameClip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v2, :cond_1

    .line 461
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    iput-boolean v2, v4, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    .line 462
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-eqz v2, :cond_3

    .line 463
    const/4 v1, 0x1

    .line 464
    .local v1, "newLikeState":Z
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 469
    :goto_1
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->updateLikeControl:Z

    .line 470
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->updateAdapter()V

    .line 472
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;

    if-eqz v2, :cond_0

    .line 473
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->cancel()V

    .line 476
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;

    invoke-direct {v2, p0, v0, v1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;ZLjava/lang/String;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;

    .line 477
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->load(Z)V

    .line 479
    .end local v1    # "newLikeState":Z
    :cond_1
    return-void

    .line 461
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 466
    :cond_3
    const/4 v1, 0x0

    .line 467
    .restart local v1    # "newLikeState":Z
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    goto :goto_1
.end method

.method public load(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 306
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->loadingCapturesAsyncTask:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;

    .line 307
    .local v1, "task":Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->cancel()V

    goto :goto_0

    .line 310
    .end local v1    # "task":Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    move-result-object v0

    .line 312
    .local v0, "filter":Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->loadingCapturesAsyncTask:Ljava/util/Hashtable;

    invoke-virtual {v2, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;

    .line 313
    .restart local v1    # "task":Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;
    if-nez v1, :cond_1

    .line 314
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;

    .end local v1    # "task":Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;
    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;)V

    .line 315
    .restart local v1    # "task":Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->loadingCapturesAsyncTask:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    :cond_1
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->load(Z)V

    .line 318
    return-void
.end method

.method public navigateToActionsScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 1
    .param p1, "itemRoot"    # Ljava/lang/String;
    .param p2, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 162
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    .line 163
    return-void
.end method

.method public navigateToGameclip(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V
    .locals 5
    .param p1, "clip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .prologue
    .line 119
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->getGameClipUriForNavigation(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;

    move-result-object v0

    .line 120
    .local v0, "clipUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    if-eqz v0, :cond_0

    .line 121
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedDataSource(Ljava/lang/String;)V

    .line 122
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 123
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setGameClip(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    .line 124
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "Play Game DVR"

    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->titleName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 126
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 128
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method public navigateToPostCommentScreen(Ljava/lang/String;)V
    .locals 7
    .param p1, "itemLocator"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 167
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    invoke-static {p0, p1, v4}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToPostCommentScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Z)V

    .line 179
    :goto_0
    return-void

    .line 170
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "User has no communication privileges"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a4

    .line 172
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0704a3

    .line 174
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0704ac

    .line 175
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 173
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 176
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 171
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public navigateToScreenshot(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)V
    .locals 4
    .param p1, "screenshot"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .prologue
    .line 143
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->navigateToScreenshot(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 145
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/IncrementScreenshotViewCount;->execute(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Lio/reactivex/Single;

    move-result-object v1

    .line 146
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    .line 147
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 148
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 144
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 154
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Screenshot View Count"

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->titleName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Screenshot Display"

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->titleName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 158
    :cond_0
    return-void
.end method

.method public navigateToShareScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V
    .locals 7
    .param p1, "itemLocator"    # Ljava/lang/String;
    .param p2, "itemType"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .prologue
    .line 183
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    .line 194
    :goto_0
    return-void

    .line 186
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a4

    .line 187
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0704a3

    .line 189
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0704ac

    .line 190
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 188
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 191
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 186
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 210
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->titleId:J

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 211
    return-void
.end method

.method protected onStopOverride()V
    .locals 2

    .prologue
    .line 215
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->loadingCapturesAsyncTask:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 216
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->cancel()V

    goto :goto_0

    .line 219
    :cond_0
    return-void
.end method

.method public resetLikeControlUpdate()V
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->updateLikeControl:Z

    .line 453
    return-void
.end method

.method public setGameProfileCapturesFilter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;Z)V
    .locals 1
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;
    .param p2, "manually"    # Z

    .prologue
    .line 104
    const-string v0, "GameProfileCapturesFilter"

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/FilterPreferences;->set(Ljava/lang/String;Ljava/lang/Enum;)Z

    .line 105
    if-eqz p2, :cond_0

    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->isFilterChanged:Z

    .line 109
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->load(Z)V

    .line 110
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->updateViewModelState()V

    .line 111
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->updateAdapter()V

    .line 112
    return-void
.end method

.method public shouldUpdateLikeControl()Z
    .locals 1

    .prologue
    .line 447
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->updateLikeControl:Z

    return v0
.end method
