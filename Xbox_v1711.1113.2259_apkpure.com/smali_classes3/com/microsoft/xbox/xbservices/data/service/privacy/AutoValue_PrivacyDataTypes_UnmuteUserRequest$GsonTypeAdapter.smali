.class public final Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_PrivacyDataTypes_UnmuteUserRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final operationAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final usersAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 29
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest$GsonTypeAdapter;->operationAdapter:Lcom/google/gson/TypeAdapter;

    .line 30
    const-class v0, Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    const-class v3, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest$GsonTypeAdapter;->usersAdapter:Lcom/google/gson/TypeAdapter;

    .line 31
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;
    .locals 5
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_0

    .line 49
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 50
    const/4 v3, 0x0

    .line 76
    :goto_0
    return-object v3

    .line 52
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 53
    const/4 v1, 0x0

    .line 54
    .local v1, "operation":Ljava/lang/String;
    const/4 v2, 0x0

    .line 55
    .local v2, "users":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;>;"
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 56
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_1

    .line 58
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 61
    :cond_1
    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 71
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 61
    :sswitch_0
    const-string v4, "operation"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :sswitch_1
    const-string v4, "users"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    .line 63
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest$GsonTypeAdapter;->operationAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "operation":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 64
    .restart local v1    # "operation":Ljava/lang/String;
    goto :goto_1

    .line 67
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest$GsonTypeAdapter;->usersAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "users":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;>;"
    check-cast v2, Lcom/google/common/collect/ImmutableList;

    .line 68
    .restart local v2    # "users":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;>;"
    goto :goto_1

    .line 75
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 76
    new-instance v3, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest;

    invoke-direct {v3, v1, v2}, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V

    goto :goto_0

    .line 61
    :sswitch_data_0
    .sparse-switch
        0x6a68e08 -> :sswitch_1
        0x631ad567 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;

    move-result-object v0

    return-object v0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    if-nez p2, :cond_0

    .line 36
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 45
    :goto_0
    return-void

    .line 39
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 40
    const-string v0, "operation"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest$GsonTypeAdapter;->operationAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;->operation()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 42
    const-string v0, "users"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest$GsonTypeAdapter;->usersAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;->users()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 44
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    check-cast p2, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UnmuteUserRequest$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;)V

    return-void
.end method
