.class public final enum Lcom/microsoft/xbox/service/chat/ChatServiceStub;
.super Ljava/lang/Enum;
.source "ChatServiceStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/chat/IChatService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/chat/ChatServiceStub;",
        ">;",
        "Lcom/microsoft/xbox/service/chat/IChatService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/chat/ChatServiceStub;

.field private static final BATCH_STUB_FILIE:Ljava/lang/String; = "stubdata/ClubBatchMessages.json"

.field private static final CHAT_HISTORY_MESSAGES_STUB_FILE:Ljava/lang/String; = "stubdata/ChatHistoryMessages.json"

.field private static final CHAT_MESSAGE_OF_THE_DAY_STUB_FILIE:Ljava/lang/String; = "stubdata/ChatMessageOfTheDay.json"

.field private static final CHAT_NOTIFICATION_REGISTRATION_RESPONSE_STUB_FILE:Ljava/lang/String; = "stubdata/ChatNotificationRegistrationResponse"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/chat/ChatServiceStub;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatServiceStub;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/chat/ChatServiceStub;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/chat/ChatServiceStub;

    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/chat/ChatServiceStub;

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/chat/ChatServiceStub;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/chat/ChatServiceStub;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/chat/ChatServiceStub;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/service/chat/ChatServiceStub;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatServiceStub;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/chat/ChatServiceStub;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/chat/ChatServiceStub;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/chat/ChatServiceStub;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/chat/ChatServiceStub;

    return-object v0
.end method


# virtual methods
.method public deleteMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;J)Z
    .locals 3
    .param p1, "type"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "messageId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 66
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 67
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p3, p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 68
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 70
    const/4 v0, 0x1

    return v0
.end method

.method public getBackwardHistoryMessages(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;JI)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    .locals 7
    .param p1, "type"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "messageId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p5, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x1

    .line 50
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 51
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 52
    invoke-static {v4, v5, p3, p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 53
    int-to-long v2, p5

    invoke-static {v4, v5, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 54
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 57
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/ChatHistoryMessages.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    return-object v1

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getForwardHistoryMessages(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;JI)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    .locals 7
    .param p1, "type"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "messageId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p5, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x1

    .line 34
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 35
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 36
    invoke-static {v4, v5, p3, p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 37
    int-to-long v2, p5

    invoke-static {v4, v5, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 38
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 41
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/ChatHistoryMessages.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    return-object v1

    .line 42
    :catch_0
    move-exception v0

    .line 43
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMessageOfTheDay(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;
    .locals 3
    .param p1, "type"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 99
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 100
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 101
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 104
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/ChatMessageOfTheDay.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_0
    return-object v1

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMessages(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;
    .locals 3
    .param p1, "type"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 113
    .local p3, "messageIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 114
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 115
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 116
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 119
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/ClubBatchMessages.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-object v1

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "ex":Ljava/io/IOException;
    const-string v1, "Failed to parse stub data"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 122
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public registerForNotifications(JLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;
    .locals 5
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "systemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "postBody"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 76
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 77
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 78
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 82
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/ChatNotificationRegistrationResponse"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-object v1

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public unregisterForNotifications(JLjava/lang/String;)Z
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "systemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 90
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 91
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 92
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 93
    const/4 v0, 0x1

    return v0
.end method
