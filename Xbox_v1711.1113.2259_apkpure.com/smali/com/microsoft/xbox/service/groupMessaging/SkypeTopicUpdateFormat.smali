.class public Lcom/microsoft/xbox/service/groupMessaging/SkypeTopicUpdateFormat;
.super Ljava/lang/Object;
.source "SkypeTopicUpdateFormat.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    name = "topicupdate"
.end annotation


# instance fields
.field public eventtime:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "eventtime"
        required = false
    .end annotation
.end field

.field public initiator:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "initiator"
        required = false
    .end annotation
.end field

.field public value:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "value"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
