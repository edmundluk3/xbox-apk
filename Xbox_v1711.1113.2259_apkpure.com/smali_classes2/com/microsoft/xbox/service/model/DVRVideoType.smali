.class public final enum Lcom/microsoft/xbox/service/model/DVRVideoType;
.super Ljava/lang/Enum;
.source "DVRVideoType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/DVRVideoType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/DVRVideoType;

.field public static final enum Ahls:Lcom/microsoft/xbox/service/model/DVRVideoType;

.field public static final enum Download:Lcom/microsoft/xbox/service/model/DVRVideoType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/xbox/service/model/DVRVideoType;

    const-string v1, "Download"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/DVRVideoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/DVRVideoType;->Download:Lcom/microsoft/xbox/service/model/DVRVideoType;

    new-instance v0, Lcom/microsoft/xbox/service/model/DVRVideoType;

    const-string v1, "Ahls"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/DVRVideoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/DVRVideoType;->Ahls:Lcom/microsoft/xbox/service/model/DVRVideoType;

    .line 3
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/DVRVideoType;

    sget-object v1, Lcom/microsoft/xbox/service/model/DVRVideoType;->Download:Lcom/microsoft/xbox/service/model/DVRVideoType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/DVRVideoType;->Ahls:Lcom/microsoft/xbox/service/model/DVRVideoType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/service/model/DVRVideoType;->$VALUES:[Lcom/microsoft/xbox/service/model/DVRVideoType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/DVRVideoType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/service/model/DVRVideoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/DVRVideoType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/DVRVideoType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/service/model/DVRVideoType;->$VALUES:[Lcom/microsoft/xbox/service/model/DVRVideoType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/DVRVideoType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/DVRVideoType;

    return-object v0
.end method
