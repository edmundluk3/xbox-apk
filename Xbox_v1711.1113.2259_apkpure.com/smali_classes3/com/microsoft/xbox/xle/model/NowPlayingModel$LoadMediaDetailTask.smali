.class Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "NowPlayingModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/model/NowPlayingModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadMediaDetailTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private companion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

.field private detailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

.field private partnerMediaId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

.field private titleId:I


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/model/NowPlayingModel;ILjava/lang/String;)V
    .locals 3
    .param p2, "titleId"    # I
    .param p3, "mediaId"    # Ljava/lang/String;

    .prologue
    .line 918
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 919
    iput p2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->titleId:I

    .line 920
    iput-object p3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->partnerMediaId:Ljava/lang/String;

    .line 921
    int-to-long v0, p2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->partnerMediaId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getModel(JLjava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->detailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 922
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 926
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->detailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 11

    .prologue
    .line 939
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->detailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->forceLoad:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v9

    .line 940
    .local v9, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 941
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->detailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaType()I

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/ActivityUtil;->isValidMediaTypeForActivity(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 942
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$602(Lcom/microsoft/xbox/xle/model/NowPlayingModel;Z)Z

    .line 945
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->detailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    .line 946
    .local v1, "id":Ljava/lang/String;
    iget v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->titleId:I

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$700(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 947
    .local v8, "lookupKey":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$800()Ljava/util/Hashtable;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 949
    .local v10, "temp":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    if-nez v10, :cond_1

    .line 950
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getActivitiesServiceManager()Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;

    move-result-object v0

    .line 952
    .local v0, "serviceManager":Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->detailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->detailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getMediaGroup()I

    move-result v3

    iget v4, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->titleId:I

    int-to-long v4, v4

    invoke-interface/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;->getCompanions(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    move-result-object v6

    .line 953
    .local v6, "companionList":Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    if-eqz v6, :cond_0

    .line 954
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->getDefault()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v10

    .line 957
    :cond_0
    if-nez v10, :cond_3

    .line 958
    sget-object v9, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 966
    .end local v0    # "serviceManager":Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;
    .end local v6    # "companionList":Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    :cond_1
    :goto_0
    iput-object v10, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->companion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 970
    .end local v1    # "id":Ljava/lang/String;
    .end local v8    # "lookupKey":Ljava/lang/String;
    .end local v10    # "temp":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :cond_2
    return-object v9

    .line 960
    .restart local v0    # "serviceManager":Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;
    .restart local v1    # "id":Ljava/lang/String;
    .restart local v6    # "companionList":Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    .restart local v8    # "lookupKey":Ljava/lang/String;
    .restart local v10    # "temp":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :cond_3
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$800()Ljava/util/Hashtable;

    move-result-object v2

    invoke-virtual {v2, v8, v10}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 962
    .end local v6    # "companionList":Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    :catch_0
    move-exception v7

    .line 963
    .local v7, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$000(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "failed to load app companion info"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 911
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 980
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 911
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 931
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 8
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 975
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    iget v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->titleId:I

    int-to-long v2, v0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->partnerMediaId:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->detailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->companion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-object v7, p1

    invoke-static/range {v1 .. v7}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$900(Lcom/microsoft/xbox/xle/model/NowPlayingModel;JLjava/lang/String;Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 976
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 911
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$LoadMediaDetailTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 935
    return-void
.end method
