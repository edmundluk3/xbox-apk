.class public Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ConversationsViewAllMembersListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "rowViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p3, "members":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 29
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;->notifyDataSetChanged()V

    .line 30
    return-void
.end method

.method private getMemberView(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;Landroid/view/View;)V
    .locals 10
    .param p1, "member"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "v"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v9, 0x7f020125

    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 77
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 78
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 80
    const v5, 0x7f0e04dc

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 81
    .local v0, "gamertagView":Landroid/widget/TextView;
    const v5, 0x7f0e04dd

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 83
    .local v4, "realnameView":Landroid/widget/TextView;
    const v5, 0x7f0e04d7

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 84
    .local v1, "listTileView":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    const v5, 0x7f0e04d9

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 86
    .local v2, "presenceIcon":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    if-eqz v0, :cond_0

    .line 87
    iget-object v5, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->senderGamerTag:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    :cond_0
    iget-object v3, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->realName:Ljava/lang/String;

    .line 91
    .local v3, "realName":Ljava/lang/String;
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v5, v6

    :goto_0
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    iget-object v5, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v8, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v5, v8, :cond_3

    :goto_1
    invoke-static {v2, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 97
    if-eqz v1, :cond_1

    .line 98
    iget-object v5, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->gamerPicUrl:Ljava/lang/String;

    invoke-virtual {v1, v5, v9, v9}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 100
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0145

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {p2, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 101
    return-void

    :cond_2
    move v5, v7

    .line 92
    goto :goto_0

    :cond_3
    move v7, v6

    .line 95
    goto :goto_1
.end method

.method private getSectionView(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;Landroid/view/View;)V
    .locals 4
    .param p1, "member"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "v"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 64
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 65
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 67
    const v1, 0x7f0e04d5

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 68
    .local v0, "sectionView":Landroid/widget/TextView;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->getItemDummyType()Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;->DUMMY_HEADER_OFFLINE:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;

    if-ne v1, v2, :cond_0

    .line 69
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070582

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 74
    :goto_0
    return-void

    .line 71
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070660

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 34
    move-object v4, p2

    .line 36
    .local v4, "v":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    if-eqz v6, :cond_2

    .line 37
    if-eqz v4, :cond_0

    instance-of v6, v4, Landroid/widget/LinearLayout;

    if-nez v6, :cond_1

    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v9, "layout_inflater"

    invoke-virtual {v6, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 39
    .local v5, "vi":Landroid/view/LayoutInflater;
    const v6, 0x7f0300c9

    invoke-virtual {v5, v6, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 42
    .end local v5    # "vi":Landroid/view/LayoutInflater;
    :cond_1
    const v6, 0x7f0e04d5

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 43
    .local v3, "sectionView":Landroid/view/View;
    const v6, 0x7f0e04da

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 44
    .local v0, "dataView":Landroid/view/View;
    const v6, 0x7f0e04d6

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 46
    .local v1, "imageView":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 47
    .local v2, "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 49
    invoke-virtual {v4, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 50
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->isDummy()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 51
    invoke-direct {p0, v2, v4}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;->getSectionView(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;Landroid/view/View;)V

    .line 56
    :goto_0
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->isDummy()Z

    move-result v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 57
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->isDummy()Z

    move-result v6

    if-nez v6, :cond_4

    move v6, v7

    :goto_1
    invoke-static {v0, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 58
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->isDummy()Z

    move-result v6

    if-nez v6, :cond_5

    :goto_2
    invoke-static {v1, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 60
    .end local v0    # "dataView":Landroid/view/View;
    .end local v1    # "imageView":Landroid/view/View;
    .end local v2    # "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v3    # "sectionView":Landroid/view/View;
    :cond_2
    return-object v4

    .line 53
    .restart local v0    # "dataView":Landroid/view/View;
    .restart local v1    # "imageView":Landroid/view/View;
    .restart local v2    # "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .restart local v3    # "sectionView":Landroid/view/View;
    :cond_3
    invoke-direct {p0, v2, v4}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;->getMemberView(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;Landroid/view/View;)V

    goto :goto_0

    :cond_4
    move v6, v8

    .line 57
    goto :goto_1

    :cond_5
    move v7, v8

    .line 58
    goto :goto_2
.end method
