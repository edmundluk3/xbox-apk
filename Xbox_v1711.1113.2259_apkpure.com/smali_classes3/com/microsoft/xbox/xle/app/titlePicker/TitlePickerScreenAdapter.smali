.class public Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "TitlePickerScreenAdapter.java"


# instance fields
.field private isTypingSearch:Z

.field private final recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

.field private final recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

.field private final searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

.field private final searchResultsAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

.field private final searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

.field private final searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 41
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    const v2, 0x7f0e0ae2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    const v2, 0x7f0e0ade

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/SearchBarView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    const v2, 0x7f0e0ae3

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    const v2, 0x7f0e0adf

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    const v2, 0x7f0e0ae1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 51
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PopularNowListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f0301d5

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/PopularNowListAdapter;-><init>(Landroid/app/Activity;I)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 54
    new-instance v1, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;)Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$SelectionListener;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$SelectionListener;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    .line 55
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 57
    new-instance v1, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;)Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$SelectionListener;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$SelectionListener;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchResultsAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchResultsAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    new-instance v2, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnSearchBarListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;)V

    .line 102
    const v1, 0x7f0e0add

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    const v1, 0x7f0e0ae0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    const v2, 0x7f0e09c9

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 106
    .local v0, "searchInputEditView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 107
    return-void
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->isTypingSearch:Z

    return p1
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 64
    .local v0, "searchSuggestion":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setSearchTag(Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->loadSearchResultsAsync(Ljava/lang/String;)V

    .line 69
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->isTypingSearch:Z

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 71
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->onTapCloseButton()V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->onDoneButton()V

    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 111
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->getSearchResults()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 112
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 113
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 115
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchResultsAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;->clear()V

    .line 116
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchResultsAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->getSearchResultTitleInfoList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;->addAll(Ljava/util/Collection;)V

    .line 117
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchResultsAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;->notifyDataSetChanged()V

    .line 129
    :goto_0
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->isTypingSearch:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    .line 130
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->getSearchSuggestions()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    .line 131
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->getSearchSuggestions()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;->getResults()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 132
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 133
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 134
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 135
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->clear()V

    .line 137
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->getSearchSuggestions()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;->getResults()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;

    .line 138
    .local v1, "result":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;->getProducts()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 139
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;->getProducts()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;

    .line 140
    .local v0, "product":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 120
    .end local v0    # "product":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;
    .end local v1    # "result":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 121
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 123
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 124
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;->clear()V

    .line 125
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->getTitleInfoList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;->addAll(Ljava/util/Collection;)V

    .line 126
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 145
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;->searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 147
    :cond_3
    return-void
.end method
