.class public Lcom/microsoft/xbox/xle/ui/WelcomeCard_ViewBinding;
.super Ljava/lang/Object;
.source "WelcomeCard_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/ui/WelcomeCard;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/WelcomeCard;)V
    .locals 0
    .param p1, "target"    # Lcom/microsoft/xbox/xle/ui/WelcomeCard;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p1}, Lcom/microsoft/xbox/xle/ui/WelcomeCard_ViewBinding;-><init>(Lcom/microsoft/xbox/xle/ui/WelcomeCard;Landroid/view/View;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/WelcomeCard;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/ui/WelcomeCard;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard_ViewBinding;->target:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 27
    const v0, 0x7f0e0bd4

    const-string v1, "field \'completionIndicator\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->completionIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 28
    const v0, 0x7f0e0bd5

    const-string v1, "field \'icon\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->icon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 29
    const v0, 0x7f0e0bd6

    const-string v1, "field \'title\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 30
    const v0, 0x7f0e0bd7

    const-string v1, "field \'description\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 31
    const v0, 0x7f0e0bd8

    const-string v1, "field \'button\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->button:Landroid/widget/Button;

    .line 32
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard_ViewBinding;->target:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 38
    .local v0, "target":Lcom/microsoft/xbox/xle/ui/WelcomeCard;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 39
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard_ViewBinding;->target:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 41
    iput-object v1, v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->completionIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 42
    iput-object v1, v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->icon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 43
    iput-object v1, v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 44
    iput-object v1, v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 45
    iput-object v1, v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->button:Landroid/widget/Button;

    .line 46
    return-void
.end method
