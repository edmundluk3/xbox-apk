.class public Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;
.super Landroid/widget/LinearLayout;
.source "EPGHeaderDayView.java"


# instance fields
.field private mDayTextView:Landroid/widget/TextView;

.field private mNowView:Landroid/view/View;

.field private mShowingDay:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->mShowingDay:I

    .line 47
    return-void
.end method

.method public static getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;
    .locals 3
    .param p0, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 51
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 52
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const v1, 0x7f0300ee

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

    return-object v1
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 59
    const v0, 0x7f0e0576

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->mDayTextView:Landroid/widget/TextView;

    .line 60
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->mDayTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 61
    return-void
.end method

.method public setDate(Ljava/util/Date;)V
    .locals 4
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 68
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->getSharedInstance()Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    move-result-object v0

    .line 71
    .local v0, "cfg":Lcom/microsoft/xbox/xle/ui/EPGViewConfig;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->calendar()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 72
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->calendar()Ljava/util/Calendar;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 73
    .local v1, "dayOfYear":I
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->mShowingDay:I

    if-eq v1, v2, :cond_0

    .line 74
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->mDayTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->formatDay(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->mShowingDay:I

    .line 77
    :cond_0
    return-void
.end method

.method public showNow(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->mNowView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 84
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 85
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->mNowView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->mNowView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 88
    .end local v0    # "visibility":I
    :cond_0
    return-void

    .line 84
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method
