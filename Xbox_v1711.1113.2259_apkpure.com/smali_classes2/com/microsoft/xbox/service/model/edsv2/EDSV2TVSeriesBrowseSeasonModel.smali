.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;
.source "EDSV2TVSeriesBrowseSeasonModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 9
    return-void
.end method


# virtual methods
.method protected bridge synthetic createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    move-result-object v0

    return-object v0
.end method

.method protected createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;
    .locals 1
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 18
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-object v0
.end method

.method protected getDesiredMediaItemType()I
    .locals 1

    .prologue
    .line 13
    const/16 v0, 0x3ed

    return v0
.end method

.method public getLatestEpisodeItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->getLatestEpisode()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    move-result-object v0

    goto :goto_0
.end method

.method public getMediaGroup()I
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x4

    return v0
.end method

.method public getMetacriticReviewScore()F
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->getMetaCriticReviewScore()F

    move-result v0

    return v0
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->getNetwork()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getOrderBy()Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_NUMBERDESCENDING:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    return-object v0
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ParentalRating:Ljava/lang/String;

    return-object v0
.end method

.method protected getRelatedMediaType()I
    .locals 1

    .prologue
    .line 27
    const/16 v0, 0x3ec

    return v0
.end method

.method public getSeasonCount()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->SeasonCount:I

    return v0
.end method
