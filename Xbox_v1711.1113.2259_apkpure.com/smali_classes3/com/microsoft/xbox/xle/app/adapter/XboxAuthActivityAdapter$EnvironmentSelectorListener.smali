.class final Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;
.super Ljava/lang/Object;
.source "XboxAuthActivityAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "EnvironmentSelectorListener"
.end annotation


# instance fields
.field enviromentNames:[Ljava/lang/CharSequence;

.field enviroments:[Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;)V
    .locals 4

    .prologue
    .line 105
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->this$0:Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v1, 0x3

    new-array v1, v1, [Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;->STUB:Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;->DNET:Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;->PROD:Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->enviroments:[Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->enviroments:[Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    array-length v1, v1

    new-array v1, v1, [Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->enviromentNames:[Ljava/lang/CharSequence;

    .line 112
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->enviroments:[Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->enviromentNames:[Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->enviroments:[Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 115
    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 120
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->getPlatformReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/Ready;->getIsReady()Z

    move-result v4

    if-nez v4, :cond_0

    .line 121
    const-string v4, "XboxAuthActivityAdapter"

    const-string v5, "platform not ready yet, ignore"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :goto_0
    return-void

    .line 125
    :cond_0
    const/4 v2, -0x1

    .line 126
    .local v2, "currentSelection":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->enviroments:[Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    array-length v4, v4

    if-ge v3, v4, :cond_2

    .line 127
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->enviroments:[Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    aget-object v4, v4, v3

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEnvironment()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    move-result-object v5

    if-ne v4, v5, :cond_1

    .line 128
    move v2, v3

    .line 126
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 132
    :cond_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 133
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const-string v4, "Select an enviroment"

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 134
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;->enviromentNames:[Ljava/lang/CharSequence;

    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;)V

    invoke-virtual {v1, v4, v2, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 150
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 151
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
