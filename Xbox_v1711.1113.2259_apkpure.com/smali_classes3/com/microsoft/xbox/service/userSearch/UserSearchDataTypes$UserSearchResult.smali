.class public final Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
.super Ljava/lang/Object;
.source "UserSearchDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserSearchResult"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field public final result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final text:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "result"    # Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 56
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->text:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    .line 60
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    if-ne p1, p0, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 66
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;

    if-nez v3, :cond_2

    move v1, v2

    .line 67
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 69
    check-cast v0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;

    .line 70
    .local v0, "other":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
    iget-object v3, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->text:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->text:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    iget-object v4, v0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 76
    iget v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->hashCode:I

    if-nez v0, :cond_0

    .line 77
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->hashCode:I

    .line 78
    iget v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->text:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->hashCode:I

    .line 79
    iget v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->hashCode:I

    .line 82
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
