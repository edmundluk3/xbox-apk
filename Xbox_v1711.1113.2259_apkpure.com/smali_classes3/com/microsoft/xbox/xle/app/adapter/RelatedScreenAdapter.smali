.class public Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "RelatedScreenAdapter.java"


# instance fields
.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;)V
    .locals 2
    .param p1, "viewMode"    # Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    .line 17
    const v0, 0x7f0e0503

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->screenBody:Landroid/view/View;

    .line 18
    const v0, 0x7f0e0504

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->content:Landroid/view/View;

    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->content:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 21
    const v0, 0x7f0e0505

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->findAndInitializeModuleById(ILcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->updateLoadingIndicator(Z)V

    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->hasRelated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 31
    :cond_0
    return-void
.end method
