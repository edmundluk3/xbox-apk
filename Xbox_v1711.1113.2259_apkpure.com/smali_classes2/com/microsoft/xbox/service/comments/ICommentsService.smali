.class public interface abstract Lcom/microsoft/xbox/service/comments/ICommentsService;
.super Ljava/lang/Object;
.source "ICommentsService.java"


# static fields
.field public static final MAX_NUM_PER_ACTION_BATCH:I = 0x64


# virtual methods
.method public abstract getActionBatchResponse(Ljava/util/List;)Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            max = 0x64L
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method
