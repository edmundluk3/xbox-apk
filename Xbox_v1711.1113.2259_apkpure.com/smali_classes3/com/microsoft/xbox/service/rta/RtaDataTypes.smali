.class public final Lcom/microsoft/xbox/service/rta/RtaDataTypes;
.super Ljava/lang/Object;
.source "RtaDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/rta/RtaDataTypes$ShoulderTap;,
        Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdEvent;,
        Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;,
        Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;,
        Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaEventResponse;,
        Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;,
        Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;,
        Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;,
        Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;,
        Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No Instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
