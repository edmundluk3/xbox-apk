.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue$Settings;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Settings"
.end annotation


# static fields
.field public static final DefaultHomeAchievements:Ljava/lang/String; = "Achievements"

.field public static final DefaultHomeActivityFeed:Ljava/lang/String; = "ActivityFeed"

.field public static final DefaultHomeClubs:Ljava/lang/String; = "Clubs"

.field public static final DefaultHomeFriends:Ljava/lang/String; = "Friends"

.field public static final DefaultHomeMessages:Ljava/lang/String; = "Messages"

.field public static final DefaultHomeTrending:Ljava/lang/String; = "Trending"

.field public static final DefaultHomeTutorial:Ljava/lang/String; = "Tutorial"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
