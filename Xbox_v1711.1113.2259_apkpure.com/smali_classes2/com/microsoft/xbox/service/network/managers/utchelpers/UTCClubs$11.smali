.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$11;
.super Ljava/lang/Object;
.source "UTCClubs.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackDiscoverSeeAll(Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$item:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$11;->val$item:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 294
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$11;->val$item:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 296
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 297
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TitleIds"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$11;->val$item:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->associatedTitles:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 299
    const-string v1, "Clubs - Discover See All Game Suggested Clubs"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 300
    return-void
.end method
