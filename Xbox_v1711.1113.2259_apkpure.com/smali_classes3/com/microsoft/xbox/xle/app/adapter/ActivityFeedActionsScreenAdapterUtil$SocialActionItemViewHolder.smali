.class public Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ActivityFeedActionsScreenAdapterUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SocialActionItemViewHolder"
.end annotation


# instance fields
.field public commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

.field public moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field public shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public socialBar:Landroid/widget/LinearLayout;

.field public socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

.field public socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

.field public socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

.field public socialBarValuesContainer:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 557
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 558
    return-void
.end method


# virtual methods
.method public init()V
    .locals 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e012e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBar:Landroid/widget/LinearLayout;

    .line 563
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0193

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 564
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0195

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 565
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0197

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 566
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e019b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBarValuesContainer:Landroid/view/View;

    .line 567
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0192

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/LikeControl;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    .line 568
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0194

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 569
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0196

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 570
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0199

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 571
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e019a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 572
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0198

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 573
    return-void
.end method
