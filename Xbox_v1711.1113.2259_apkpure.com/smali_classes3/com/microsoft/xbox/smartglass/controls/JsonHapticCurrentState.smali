.class Lcom/microsoft/xbox/smartglass/controls/JsonHapticCurrentState;
.super Lorg/json/JSONObject;
.source "JsonHapticCurrentState.java"


# direct methods
.method public constructor <init>(ZZ)V
    .locals 1
    .param p1, "hasHaptic"    # Z
    .param p2, "isActive"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 12
    const-string v0, "hasHaptic"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/JsonHapticCurrentState;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 13
    const-string v0, "isActive"

    invoke-virtual {p0, v0, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonHapticCurrentState;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 14
    return-void
.end method
