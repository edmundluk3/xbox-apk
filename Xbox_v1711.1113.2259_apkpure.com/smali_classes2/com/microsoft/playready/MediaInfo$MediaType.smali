.class public final enum Lcom/microsoft/playready/MediaInfo$MediaType;
.super Ljava/lang/Enum;
.source "MediaInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/MediaInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/playready/MediaInfo$MediaType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AUDIO:Lcom/microsoft/playready/MediaInfo$MediaType;

.field private static final synthetic ENUM$VALUES:[Lcom/microsoft/playready/MediaInfo$MediaType;

.field public static final enum EVENT:Lcom/microsoft/playready/MediaInfo$MediaType;

.field public static final enum METADATA:Lcom/microsoft/playready/MediaInfo$MediaType;

.field public static final enum TEXT:Lcom/microsoft/playready/MediaInfo$MediaType;

.field public static final enum UNKNOWN:Lcom/microsoft/playready/MediaInfo$MediaType;

.field public static final enum VIDEO:Lcom/microsoft/playready/MediaInfo$MediaType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/microsoft/playready/MediaInfo$MediaType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/playready/MediaInfo$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/MediaInfo$MediaType;->UNKNOWN:Lcom/microsoft/playready/MediaInfo$MediaType;

    new-instance v0, Lcom/microsoft/playready/MediaInfo$MediaType;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/playready/MediaInfo$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/MediaInfo$MediaType;->AUDIO:Lcom/microsoft/playready/MediaInfo$MediaType;

    new-instance v0, Lcom/microsoft/playready/MediaInfo$MediaType;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/playready/MediaInfo$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/MediaInfo$MediaType;->VIDEO:Lcom/microsoft/playready/MediaInfo$MediaType;

    new-instance v0, Lcom/microsoft/playready/MediaInfo$MediaType;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/playready/MediaInfo$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/MediaInfo$MediaType;->TEXT:Lcom/microsoft/playready/MediaInfo$MediaType;

    new-instance v0, Lcom/microsoft/playready/MediaInfo$MediaType;

    const-string v1, "EVENT"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/playready/MediaInfo$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/MediaInfo$MediaType;->EVENT:Lcom/microsoft/playready/MediaInfo$MediaType;

    new-instance v0, Lcom/microsoft/playready/MediaInfo$MediaType;

    const-string v1, "METADATA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/playready/MediaInfo$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/MediaInfo$MediaType;->METADATA:Lcom/microsoft/playready/MediaInfo$MediaType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/playready/MediaInfo$MediaType;

    sget-object v1, Lcom/microsoft/playready/MediaInfo$MediaType;->UNKNOWN:Lcom/microsoft/playready/MediaInfo$MediaType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/playready/MediaInfo$MediaType;->AUDIO:Lcom/microsoft/playready/MediaInfo$MediaType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/playready/MediaInfo$MediaType;->VIDEO:Lcom/microsoft/playready/MediaInfo$MediaType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/playready/MediaInfo$MediaType;->TEXT:Lcom/microsoft/playready/MediaInfo$MediaType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/playready/MediaInfo$MediaType;->EVENT:Lcom/microsoft/playready/MediaInfo$MediaType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/playready/MediaInfo$MediaType;->METADATA:Lcom/microsoft/playready/MediaInfo$MediaType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/playready/MediaInfo$MediaType;->ENUM$VALUES:[Lcom/microsoft/playready/MediaInfo$MediaType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/playready/MediaInfo$MediaType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/microsoft/playready/MediaInfo$MediaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/MediaInfo$MediaType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/playready/MediaInfo$MediaType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/microsoft/playready/MediaInfo$MediaType;->ENUM$VALUES:[Lcom/microsoft/playready/MediaInfo$MediaType;

    array-length v1, v0

    new-array v2, v1, [Lcom/microsoft/playready/MediaInfo$MediaType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
