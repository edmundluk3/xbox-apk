.class public Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "GamerscoreLeaderboardListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 41
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 43
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 44
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;

    return-object v0
.end method


# virtual methods
.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    .line 56
    .local v0, "item":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    if-eqz v0, :cond_0

    .line 57
    check-cast p1, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;

    .end local p1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->bindTo(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;)V

    .line 59
    :cond_0
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 49
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03012f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;Landroid/view/View;)V

    return-object v0
.end method
