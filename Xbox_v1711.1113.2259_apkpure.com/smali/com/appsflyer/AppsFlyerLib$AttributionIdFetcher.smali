.class abstract Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;
.super Ljava/lang/Object;
.source "AppsFlyerLib.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/appsflyer/AppsFlyerLib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "AttributionIdFetcher"
.end annotation


# instance fields
.field private appsFlyerDevKey:Ljava/lang/String;

.field protected ctxReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private currentRequestsCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private executorService:Ljava/util/concurrent/ScheduledExecutorService;

.field final synthetic this$0:Lcom/appsflyer/AppsFlyerLib;


# direct methods
.method public constructor <init>(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "appsFlyerDevKey"    # Ljava/lang/String;
    .param p4, "executorService"    # Ljava/util/concurrent/ScheduledExecutorService;

    .prologue
    .line 2114
    iput-object p1, p0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->this$0:Lcom/appsflyer/AppsFlyerLib;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->ctxReference:Ljava/lang/ref/WeakReference;

    .line 2112
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->currentRequestsCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2115
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->ctxReference:Ljava/lang/ref/WeakReference;

    .line 2116
    iput-object p3, p0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->appsFlyerDevKey:Ljava/lang/String;

    .line 2117
    iput-object p4, p0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->executorService:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2118
    return-void
.end method


# virtual methods
.method protected abstract attributionCallback(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method protected abstract attributionCallbackFailure(Ljava/lang/String;I)V
.end method

.method public abstract getUrl()Ljava/lang/String;
.end method

.method public run()V
    .locals 32

    .prologue
    .line 2121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->appsFlyerDevKey:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->appsFlyerDevKey:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    if-nez v25, :cond_1

    .line 2235
    :cond_0
    :goto_0
    return-void

    .line 2124
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->currentRequestsCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 2125
    const/4 v7, 0x0

    .line 2127
    .local v7, "connection":Ljava/net/HttpURLConnection;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->ctxReference:Ljava/lang/ref/WeakReference;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2128
    .local v8, "context":Landroid/content/Context;
    if-nez v8, :cond_2

    .line 2229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->currentRequestsCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 2230
    if-eqz v7, :cond_0

    .line 2231
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 2132
    :cond_2
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 2133
    .local v16, "now":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->this$0:Lcom/appsflyer/AppsFlyerLib;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->this$0:Lcom/appsflyer/AppsFlyerLib;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v0, v8}, Lcom/appsflyer/AppsFlyerLib;->access$1000(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v8, v1}, Lcom/appsflyer/AppsFlyerLib;->access$1100(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2134
    .local v5, "channel":Ljava/lang/String;
    const-string v6, ""

    .line 2135
    .local v6, "channelPostfix":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 2136
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "-"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2138
    :cond_3
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    .line 2139
    invoke-virtual/range {p0 .. p0}, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->getUrl()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    .line 2140
    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    .line 2141
    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "?devkey="

    .line 2142
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->appsFlyerDevKey:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "&device_id="

    .line 2143
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->this$0:Lcom/appsflyer/AppsFlyerLib;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Lcom/appsflyer/AppsFlyerLib;->getAppsFlyerUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    .line 2145
    .local v24, "urlString":Ljava/lang/StringBuilder;
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Calling server for attribution url: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/appsflyer/LogMessages;->logMessageMaskKey(Ljava/lang/String;)V

    .line 2147
    new-instance v25, Ljava/net/URL;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v25}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v25

    move-object/from16 v0, v25

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v7, v0

    .line 2149
    const-string v25, "GET"

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 2150
    const/16 v25, 0x2710

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 2151
    const-string v25, "Connection"

    const-string v26, "close"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v7, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2152
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->connect()V

    .line 2154
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v25

    const/16 v26, 0xc8

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_f

    .line 2156
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 2158
    .local v20, "responseTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->this$0:Lcom/appsflyer/AppsFlyerLib;

    move-object/from16 v25, v0

    const-string v26, "appsflyerGetConversionDataTiming"

    sub-long v28, v20, v16

    const-wide/16 v30, 0x3e8

    div-long v28, v28, v30

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-wide/from16 v2, v28

    invoke-static {v0, v8, v1, v2, v3}, Lcom/appsflyer/AppsFlyerLib;->access$1200(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;J)V

    .line 2161
    const/16 v18, 0x0

    .line 2162
    .local v18, "reader":Ljava/io/BufferedReader;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2163
    .local v22, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v12, 0x0

    .line 2165
    .local v12, "inputStreamReader":Ljava/io/InputStreamReader;
    :try_start_2
    new-instance v13, Ljava/io/InputStreamReader;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-direct {v13, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2166
    .end local v12    # "inputStreamReader":Ljava/io/InputStreamReader;
    .local v13, "inputStreamReader":Ljava/io/InputStreamReader;
    :try_start_3
    new-instance v19, Ljava/io/BufferedReader;

    move-object/from16 v0, v19

    invoke-direct {v0, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2168
    .end local v18    # "reader":Ljava/io/BufferedReader;
    .local v19, "reader":Ljava/io/BufferedReader;
    const/4 v15, 0x0

    .line 2169
    .local v15, "line":Ljava/lang/String;
    :goto_1
    :try_start_4
    invoke-virtual/range {v19 .. v19}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_8

    .line 2170
    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const/16 v26, 0xa

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 2173
    :catchall_0
    move-exception v25

    move-object v12, v13

    .end local v13    # "inputStreamReader":Ljava/io/InputStreamReader;
    .restart local v12    # "inputStreamReader":Ljava/io/InputStreamReader;
    move-object/from16 v18, v19

    .end local v15    # "line":Ljava/lang/String;
    .end local v19    # "reader":Ljava/io/BufferedReader;
    .restart local v18    # "reader":Ljava/io/BufferedReader;
    :goto_2
    if-eqz v18, :cond_4

    .line 2174
    :try_start_5
    invoke-virtual/range {v18 .. v18}, Ljava/io/BufferedReader;->close()V

    .line 2176
    :cond_4
    if-eqz v12, :cond_5

    .line 2177
    invoke-virtual {v12}, Ljava/io/InputStreamReader;->close()V

    :cond_5
    throw v25
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2223
    .end local v5    # "channel":Ljava/lang/String;
    .end local v6    # "channelPostfix":Ljava/lang/String;
    .end local v8    # "context":Landroid/content/Context;
    .end local v12    # "inputStreamReader":Ljava/io/InputStreamReader;
    .end local v16    # "now":J
    .end local v18    # "reader":Ljava/io/BufferedReader;
    .end local v20    # "responseTime":J
    .end local v22    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v24    # "urlString":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v23

    .line 2224
    .local v23, "t":Ljava/lang/Throwable;
    :try_start_6
    invoke-static {}, Lcom/appsflyer/AppsFlyerLib;->access$700()Lcom/appsflyer/AppsFlyerConversionListener;

    move-result-object v25

    if-eqz v25, :cond_6

    .line 2225
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v25

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->attributionCallbackFailure(Ljava/lang/String;I)V

    .line 2227
    :cond_6
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/appsflyer/AFLogger;->afLogE(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->currentRequestsCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 2230
    if-eqz v7, :cond_7

    .line 2231
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 2234
    .end local v23    # "t":Ljava/lang/Throwable;
    :cond_7
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->executorService:Ljava/util/concurrent/ScheduledExecutorService;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    goto/16 :goto_0

    .line 2173
    .restart local v5    # "channel":Ljava/lang/String;
    .restart local v6    # "channelPostfix":Ljava/lang/String;
    .restart local v8    # "context":Landroid/content/Context;
    .restart local v13    # "inputStreamReader":Ljava/io/InputStreamReader;
    .restart local v15    # "line":Ljava/lang/String;
    .restart local v16    # "now":J
    .restart local v19    # "reader":Ljava/io/BufferedReader;
    .restart local v20    # "responseTime":J
    .restart local v22    # "stringBuilder":Ljava/lang/StringBuilder;
    .restart local v24    # "urlString":Ljava/lang/StringBuilder;
    :cond_8
    if-eqz v19, :cond_9

    .line 2174
    :try_start_7
    invoke-virtual/range {v19 .. v19}, Ljava/io/BufferedReader;->close()V

    .line 2176
    :cond_9
    if-eqz v13, :cond_a

    .line 2177
    invoke-virtual {v13}, Ljava/io/InputStreamReader;->close()V

    .line 2181
    :cond_a
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Attribution data: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/appsflyer/LogMessages;->logMessageMaskKey(Ljava/lang/String;)V

    .line 2183
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->length()I

    move-result v25

    if-lez v25, :cond_c

    if-eqz v8, :cond_c

    .line 2184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->this$0:Lcom/appsflyer/AppsFlyerLib;

    move-object/from16 v25, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/appsflyer/AppsFlyerLib;->access$1300(Lcom/appsflyer/AppsFlyerLib;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v10

    .line 2185
    .local v10, "conversionDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v25, "iscache"

    move-object/from16 v0, v25

    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 2188
    .local v14, "isCache":Ljava/lang/String;
    if-eqz v14, :cond_b

    const-string v25, "false"

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 2190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->this$0:Lcom/appsflyer/AppsFlyerLib;

    move-object/from16 v25, v0

    const-string v26, "appsflyerConversionDataCacheExpiration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-wide/from16 v2, v28

    invoke-static {v0, v8, v1, v2, v3}, Lcom/appsflyer/AppsFlyerLib;->access$1200(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;J)V

    .line 2193
    :cond_b
    new-instance v25, Lorg/json/JSONObject;

    move-object/from16 v0, v25

    invoke-direct {v0, v10}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual/range {v25 .. v25}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v11

    .line 2194
    .local v11, "conversionJsonString":Ljava/lang/String;
    if-eqz v11, :cond_d

    .line 2195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->this$0:Lcom/appsflyer/AppsFlyerLib;

    move-object/from16 v25, v0

    const-string v26, "attributionId"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v8, v1, v11}, Lcom/appsflyer/AppsFlyerLib;->access$1400(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2201
    :goto_4
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "iscache="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " caching conversion data"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 2203
    invoke-static {}, Lcom/appsflyer/AppsFlyerLib;->access$700()Lcom/appsflyer/AppsFlyerConversionListener;

    move-result-object v25

    if-eqz v25, :cond_c

    .line 2204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->currentRequestsCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-gt v0, v1, :cond_c

    .line 2207
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->this$0:Lcom/appsflyer/AppsFlyerLib;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-static {v0, v8}, Lcom/appsflyer/AppsFlyerLib;->access$1500(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;)Ljava/util/Map;
    :try_end_8
    .catch Lcom/appsflyer/AttributionIDNotReady; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result-object v9

    .line 2211
    .local v9, "conversionData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_5
    :try_start_9
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->attributionCallback(Ljava/util/Map;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 2229
    .end local v9    # "conversionData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v10    # "conversionDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v11    # "conversionJsonString":Ljava/lang/String;
    .end local v13    # "inputStreamReader":Ljava/io/InputStreamReader;
    .end local v14    # "isCache":Ljava/lang/String;
    .end local v15    # "line":Ljava/lang/String;
    .end local v19    # "reader":Ljava/io/BufferedReader;
    .end local v20    # "responseTime":J
    .end local v22    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_c
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->currentRequestsCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 2230
    if-eqz v7, :cond_7

    .line 2231
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_3

    .line 2198
    .restart local v10    # "conversionDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v11    # "conversionJsonString":Ljava/lang/String;
    .restart local v13    # "inputStreamReader":Ljava/io/InputStreamReader;
    .restart local v14    # "isCache":Ljava/lang/String;
    .restart local v15    # "line":Ljava/lang/String;
    .restart local v19    # "reader":Ljava/io/BufferedReader;
    .restart local v20    # "responseTime":J
    .restart local v22    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_d
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->this$0:Lcom/appsflyer/AppsFlyerLib;

    move-object/from16 v25, v0

    const-string v26, "attributionId"

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-static {v0, v8, v1, v2}, Lcom/appsflyer/AppsFlyerLib;->access$1400(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_4

    .line 2229
    .end local v5    # "channel":Ljava/lang/String;
    .end local v6    # "channelPostfix":Ljava/lang/String;
    .end local v8    # "context":Landroid/content/Context;
    .end local v10    # "conversionDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v11    # "conversionJsonString":Ljava/lang/String;
    .end local v13    # "inputStreamReader":Ljava/io/InputStreamReader;
    .end local v14    # "isCache":Ljava/lang/String;
    .end local v15    # "line":Ljava/lang/String;
    .end local v16    # "now":J
    .end local v19    # "reader":Ljava/io/BufferedReader;
    .end local v20    # "responseTime":J
    .end local v22    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v24    # "urlString":Ljava/lang/StringBuilder;
    :catchall_1
    move-exception v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->currentRequestsCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 2230
    if-eqz v7, :cond_e

    .line 2231
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_e
    throw v25

    .line 2208
    .restart local v5    # "channel":Ljava/lang/String;
    .restart local v6    # "channelPostfix":Ljava/lang/String;
    .restart local v8    # "context":Landroid/content/Context;
    .restart local v10    # "conversionDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v11    # "conversionJsonString":Ljava/lang/String;
    .restart local v13    # "inputStreamReader":Ljava/io/InputStreamReader;
    .restart local v14    # "isCache":Ljava/lang/String;
    .restart local v15    # "line":Ljava/lang/String;
    .restart local v16    # "now":J
    .restart local v19    # "reader":Ljava/io/BufferedReader;
    .restart local v20    # "responseTime":J
    .restart local v22    # "stringBuilder":Ljava/lang/StringBuilder;
    .restart local v24    # "urlString":Ljava/lang/StringBuilder;
    :catch_1
    move-exception v4

    .line 2209
    .local v4, "ae":Lcom/appsflyer/AttributionIDNotReady;
    move-object v9, v10

    .restart local v9    # "conversionData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_5

    .line 2217
    .end local v4    # "ae":Lcom/appsflyer/AttributionIDNotReady;
    .end local v9    # "conversionData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v10    # "conversionDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v11    # "conversionJsonString":Ljava/lang/String;
    .end local v13    # "inputStreamReader":Ljava/io/InputStreamReader;
    .end local v14    # "isCache":Ljava/lang/String;
    .end local v15    # "line":Ljava/lang/String;
    .end local v19    # "reader":Ljava/io/BufferedReader;
    .end local v20    # "responseTime":J
    .end local v22    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_f
    :try_start_b
    invoke-static {}, Lcom/appsflyer/AppsFlyerLib;->access$700()Lcom/appsflyer/AppsFlyerConversionListener;

    move-result-object v25

    if-eqz v25, :cond_10

    .line 2218
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Error connection to server: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v26

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/appsflyer/AppsFlyerLib$AttributionIdFetcher;->attributionCallbackFailure(Ljava/lang/String;I)V

    .line 2220
    :cond_10
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "AttributionIdFetcher response code: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "  url: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/appsflyer/LogMessages;->logMessageMaskKey(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_6

    .line 2173
    .restart local v12    # "inputStreamReader":Ljava/io/InputStreamReader;
    .restart local v18    # "reader":Ljava/io/BufferedReader;
    .restart local v20    # "responseTime":J
    .restart local v22    # "stringBuilder":Ljava/lang/StringBuilder;
    :catchall_2
    move-exception v25

    goto/16 :goto_2

    .end local v12    # "inputStreamReader":Ljava/io/InputStreamReader;
    .restart local v13    # "inputStreamReader":Ljava/io/InputStreamReader;
    :catchall_3
    move-exception v25

    move-object v12, v13

    .end local v13    # "inputStreamReader":Ljava/io/InputStreamReader;
    .restart local v12    # "inputStreamReader":Ljava/io/InputStreamReader;
    goto/16 :goto_2
.end method
