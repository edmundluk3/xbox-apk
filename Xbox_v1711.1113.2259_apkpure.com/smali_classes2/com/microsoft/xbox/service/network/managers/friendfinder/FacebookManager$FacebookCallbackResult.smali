.class public final enum Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;
.super Ljava/lang/Enum;
.source "FacebookManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FacebookCallbackResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

.field public static final enum LoginCancel:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

.field public static final enum LoginError:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

.field public static final enum LoginSuccess:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

.field public static final enum Logout:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

.field public static final enum ShareCancel:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

.field public static final enum ShareError:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

.field public static final enum ShareSuccess:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    const-string v1, "ShareSuccess"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->ShareSuccess:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    .line 65
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    const-string v1, "ShareCancel"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->ShareCancel:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    .line 66
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    const-string v1, "ShareError"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->ShareError:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    .line 68
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    const-string v1, "LoginSuccess"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->LoginSuccess:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    .line 69
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    const-string v1, "LoginCancel"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->LoginCancel:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    .line 70
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    const-string v1, "LoginError"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->LoginError:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    .line 72
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    const-string v1, "Logout"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->Logout:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    .line 63
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->ShareSuccess:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->ShareCancel:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->ShareError:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->LoginSuccess:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->LoginCancel:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->LoginError:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->Logout:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    const-class v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    return-object v0
.end method
