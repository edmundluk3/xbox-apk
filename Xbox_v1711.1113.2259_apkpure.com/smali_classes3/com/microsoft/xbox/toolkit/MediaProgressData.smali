.class public Lcom/microsoft/xbox/toolkit/MediaProgressData;
.super Ljava/lang/Object;
.source "MediaProgressData.java"


# static fields
.field public static final KEY_DURATION_IN_SECONDS:Ljava/lang/String; = "DurationInSeconds"

.field public static final KEY_LOCATION:Ljava/lang/String; = "Location"

.field public static final KEY_POSITION_IN_SECONDS:Ljava/lang/String; = "PositionInSeconds"


# instance fields
.field public durationInSeconds:J

.field public location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

.field public positionInSeconds:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 7
    .param p1, "positionInSeconds"    # J
    .param p3, "durationInSeconds"    # J

    .prologue
    .line 26
    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Default:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/MediaProgressData;-><init>(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;JJ)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-wide v2, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->positionInSeconds:J

    .line 16
    iput-wide v2, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->durationInSeconds:J

    .line 36
    if-nez p1, :cond_0

    .line 43
    :goto_0
    return-void

    .line 40
    :cond_0
    const-string v0, "Location"

    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->getValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->fromInt(I)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 41
    const-string v0, "PositionInSeconds"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->positionInSeconds:J

    .line 42
    const-string v0, "DurationInSeconds"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->durationInSeconds:J

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;JJ)V
    .locals 2
    .param p1, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .param p2, "positionInSeconds"    # J
    .param p4, "durationInSeconds"    # J

    .prologue
    const-wide/16 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->positionInSeconds:J

    .line 16
    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->durationInSeconds:J

    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 31
    iput-wide p2, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->positionInSeconds:J

    .line 32
    iput-wide p4, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->durationInSeconds:J

    .line 33
    return-void
.end method


# virtual methods
.method public getBundle()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 46
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 47
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "Location"

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 48
    const-string v1, "PositionInSeconds"

    iget-wide v2, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->positionInSeconds:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 49
    const-string v1, "DurationInSeconds"

    iget-wide v2, p0, Lcom/microsoft/xbox/toolkit/MediaProgressData;->durationInSeconds:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 50
    return-object v0
.end method
