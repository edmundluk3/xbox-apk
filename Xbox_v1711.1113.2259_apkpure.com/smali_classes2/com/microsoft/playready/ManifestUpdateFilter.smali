.class public Lcom/microsoft/playready/ManifestUpdateFilter;
.super Ljava/lang/Object;
.source "ManifestUpdateFilter.java"

# interfaces
.implements Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;
    }
.end annotation


# instance fields
.field private mMediaProxy:Lcom/microsoft/playready/MediaProxy;

.field private mNotificationHandler:Landroid/os/Handler;

.field private streamFilterMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/MediaProxy;Landroid/os/Looper;)V
    .locals 1
    .param p1, "mediaProxy"    # Lcom/microsoft/playready/MediaProxy;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    const/4 v0, 0x0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    .line 155
    iput-object v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->mNotificationHandler:Landroid/os/Handler;

    .line 157
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->streamFilterMap:Landroid/util/SparseArray;

    .line 161
    iput-object p1, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    .line 162
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->mNotificationHandler:Landroid/os/Handler;

    .line 163
    iget-object v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    invoke-virtual {v0, p0}, Lcom/microsoft/playready/MediaProxy;->addManifestUpdateListener(Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;)V

    .line 164
    return-void
.end method


# virtual methods
.method public addStreamUpdateListener(Lcom/microsoft/playready/IStreamUpdateListener;Lcom/microsoft/playready/MediaStream;)V
    .locals 3
    .param p1, "listener"    # Lcom/microsoft/playready/IStreamUpdateListener;
    .param p2, "stream"    # Lcom/microsoft/playready/MediaStream;

    .prologue
    .line 168
    monitor-enter p0

    .line 170
    :try_start_0
    invoke-virtual {p2}, Lcom/microsoft/playready/MediaStream;->getStreamIndex()I

    move-result v1

    .line 171
    .local v1, "streamIndex":I
    iget-object v2, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->streamFilterMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;

    .line 172
    .local v0, "filter":Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;
    if-nez v0, :cond_0

    .line 174
    new-instance v0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;

    .end local v0    # "filter":Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;
    iget-object v2, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    invoke-direct {v0, p0, p2, v2}, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;-><init>(Lcom/microsoft/playready/ManifestUpdateFilter;Lcom/microsoft/playready/MediaStream;Lcom/microsoft/playready/MediaProxy;)V

    .line 175
    .restart local v0    # "filter":Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;
    iget-object v2, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->streamFilterMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 178
    :cond_0
    invoke-virtual {v0, p1}, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->addStreamUpdateListener(Lcom/microsoft/playready/IStreamUpdateListener;)V

    .line 168
    monitor-exit p0

    .line 180
    return-void

    .line 168
    .end local v0    # "filter":Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;
    .end local v1    # "streamIndex":I
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public onManifestUpdate()V
    .locals 3

    .prologue
    .line 213
    monitor-enter p0

    .line 215
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->streamFilterMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clone()Landroid/util/SparseArray;

    move-result-object v1

    .line 217
    .local v1, "streamFilterMapCopy":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;>;"
    new-instance v0, Lcom/microsoft/playready/ManifestUpdateFilter$1;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/ManifestUpdateFilter$1;-><init>(Lcom/microsoft/playready/ManifestUpdateFilter;)V

    .line 245
    .local v0, "runMe":Ljava/lang/Runnable;
    iget-object v2, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 213
    monitor-exit p0

    .line 248
    return-void

    .line 213
    .end local v0    # "runMe":Ljava/lang/Runnable;
    .end local v1    # "streamFilterMapCopy":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method removeStreamUpdateListener(Lcom/microsoft/playready/IStreamUpdateListener;I)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/playready/IStreamUpdateListener;
    .param p2, "streamIndex"    # I

    .prologue
    .line 190
    monitor-enter p0

    .line 192
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->streamFilterMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;

    .line 193
    .local v0, "filter":Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;
    if-eqz v0, :cond_0

    .line 195
    invoke-virtual {v0, p1}, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->removeStreamUpdateListener(Lcom/microsoft/playready/IStreamUpdateListener;)V

    .line 201
    invoke-virtual {v0}, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->hasListeners()Z

    move-result v1

    if-nez v1, :cond_0

    .line 203
    iget-object v1, p0, Lcom/microsoft/playready/ManifestUpdateFilter;->streamFilterMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 190
    :cond_0
    monitor-exit p0

    .line 207
    return-void

    .line 190
    .end local v0    # "filter":Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeStreamUpdateListener(Lcom/microsoft/playready/IStreamUpdateListener;Lcom/microsoft/playready/MediaStream;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/IStreamUpdateListener;
    .param p2, "stream"    # Lcom/microsoft/playready/MediaStream;

    .prologue
    .line 184
    invoke-virtual {p2}, Lcom/microsoft/playready/MediaStream;->getStreamIndex()I

    move-result v0

    .line 185
    .local v0, "streamIndex":I
    invoke-virtual {p0, p1, v0}, Lcom/microsoft/playready/ManifestUpdateFilter;->removeStreamUpdateListener(Lcom/microsoft/playready/IStreamUpdateListener;I)V

    .line 186
    return-void
.end method
