.class public final enum Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;
.super Ljava/lang/Enum;
.source "TournamentNotificationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TeamState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

.field public static final enum CheckedIn:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

.field public static final enum Eliminated:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

.field public static final enum MissedCheckIn:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

.field public static final enum Playing:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

.field public static final enum Registered:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

.field public static final enum Standby:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

.field public static final enum Unregistered:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

.field public static final enum Waitlisted:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    const-string v1, "Registered"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Registered:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    const-string v1, "Waitlisted"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Waitlisted:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    const-string v1, "Standby"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Standby:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    .line 27
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    const-string v1, "CheckedIn"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->CheckedIn:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    const-string v1, "MissedCheckIn"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->MissedCheckIn:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    const-string v1, "Playing"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Playing:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    .line 30
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    const-string v1, "Eliminated"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Eliminated:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    const-string v1, "Unregistered"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Unregistered:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    .line 23
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    sget-object v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Registered:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Waitlisted:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Standby:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->CheckedIn:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->MissedCheckIn:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Playing:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Eliminated:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->Unregistered:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->$VALUES:[Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->$VALUES:[Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;

    return-object v0
.end method
