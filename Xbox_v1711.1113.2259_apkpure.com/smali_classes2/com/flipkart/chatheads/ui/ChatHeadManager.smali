.class public interface abstract Lcom/flipkart/chatheads/ui/ChatHeadManager;
.super Ljava/lang/Object;
.source "ChatHeadManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract addChatHead(Ljava/io/Serializable;ZZ)Lcom/flipkart/chatheads/ui/ChatHead;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;ZZ)",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract attachView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract bringToFront(Lcom/flipkart/chatheads/ui/ChatHead;)V
.end method

.method public abstract captureChatHeads(Lcom/flipkart/chatheads/ui/ChatHead;)V
.end method

.method public abstract detachView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation
.end method

.method public abstract findChatHeadByKey(Ljava/io/Serializable;)Lcom/flipkart/chatheads/ui/ChatHead;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getActiveArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
.end method

.method public abstract getArrangement(Ljava/lang/Class;)Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;)",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;"
        }
    .end annotation
.end method

.method public abstract getArrangementType()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;
.end method

.method public abstract getChatHeadContainer()Lcom/flipkart/chatheads/ui/ChatHeadContainer;
.end method

.method public abstract getChatHeadCoordsForCloseButton(Lcom/flipkart/chatheads/ui/ChatHead;)[I
.end method

.method public abstract getChatHeads()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;>;"
        }
    .end annotation
.end method

.method public abstract getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;
.end method

.method public abstract getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getDisplayMetrics()Landroid/util/DisplayMetrics;
.end method

.method public abstract getDistanceCloseButtonFromHead(FF)D
.end method

.method public abstract getListener()Lcom/flipkart/chatheads/ui/ChatHeadListener;
.end method

.method public abstract getMaxHeight()I
.end method

.method public abstract getMaxWidth()I
.end method

.method public abstract getOverlayView()Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;
.end method

.method public abstract getSpringSystem()Lcom/facebook/rebound/SpringSystem;
.end method

.method public abstract getViewAdapter()Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;
.end method

.method public abstract hideOverlayView(Z)V
.end method

.method public abstract onCloseButtonAppear()V
.end method

.method public abstract onCloseButtonDisappear()V
.end method

.method public abstract onItemRollOut(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public abstract onItemRollOver(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public abstract onItemSelected(Lcom/flipkart/chatheads/ui/ChatHead;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)Z"
        }
    .end annotation
.end method

.method public abstract onMeasure(II)V
.end method

.method public abstract onRestoreInstanceState(Landroid/os/Parcelable;)V
.end method

.method public abstract onSaveInstanceState(Landroid/os/Parcelable;)Landroid/os/Parcelable;
.end method

.method public abstract onSizeChanged(IIII)V
.end method

.method public abstract recreateView(Ljava/io/Serializable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public abstract reloadDrawable(Ljava/io/Serializable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public abstract removeAllChatHeads(Z)V
.end method

.method public abstract removeChatHead(Ljava/io/Serializable;Z)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)Z"
        }
    .end annotation
.end method

.method public abstract removeView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation
.end method

.method public abstract selectChatHead(Lcom/flipkart/chatheads/ui/ChatHead;)V
.end method

.method public abstract selectChatHead(Ljava/io/Serializable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public abstract setArrangement(Ljava/lang/Class;Landroid/os/Bundle;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation
.end method

.method public abstract setArrangement(Ljava/lang/Class;Landroid/os/Bundle;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;",
            "Landroid/os/Bundle;",
            "Z)V"
        }
    .end annotation
.end method

.method public abstract setConfig(Lcom/flipkart/chatheads/ui/ChatHeadConfig;)V
.end method

.method public abstract setListener(Lcom/flipkart/chatheads/ui/ChatHeadListener;)V
.end method

.method public abstract setOnItemSelectedListener(Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public abstract setViewAdapter(Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;)V
.end method

.method public abstract showOverlayView(Z)V
.end method
