.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView$Clubs;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Clubs"
.end annotation


# static fields
.field public static final ActivityFeed:Ljava/lang/String; = "Clubs - Club Activity Feed view"

.field public static final Admin:Ljava/lang/String; = "Clubs - Club Admin view"

.field public static final AdminBanned:Ljava/lang/String; = "Clubs - Club Admin Banned view"

.field public static final AdminMembers:Ljava/lang/String; = "Clubs - Club Admin Members view"

.field public static final AdminReports:Ljava/lang/String; = "Clubs - Club Admin Reports view"

.field public static final AdminRequestsRecommendations:Ljava/lang/String; = "Clubs - Club Admin Requests Recommendations view"

.field public static final AdminSettings:Ljava/lang/String; = "Clubs - Club Admin Settings view"

.field public static final Chat:Ljava/lang/String; = "Clubs - Club Chat view"

.field public static final ClubsEditTopicHomeView:Ljava/lang/String; = "Clubs Edit Topic Home view"

.field public static final ClubsEditTopicView:Ljava/lang/String; = "Clubs Edit Topic view"

.field public static final ClubsView:Ljava/lang/String; = "Clubs"

.field public static final CreateClub:Ljava/lang/String; = "Clubs - Create Club view"

.field public static final CreateClubChooseName:Ljava/lang/String; = "Clubs - Create Club Choose Name view"

.field public static final CreateClubChooseType:Ljava/lang/String; = "Clubs - Create Club Choose Type view"

.field public static final CreateClubConfirmation:Ljava/lang/String; = "Clubs - Create Club Confirmation view"

.field public static final CustomizeClub:Ljava/lang/String; = "Clubs - Customize Club view"

.field public static final DiscoverClubs:Ljava/lang/String; = "Clubs - Discover Clubs view"

.field public static final GameHubSuggestedClubs:Ljava/lang/String; = "Clubs - Game Hub Suggested Clubs view"

.field public static final Home:Ljava/lang/String; = "Clubs - Club Home view"

.field public static final Play:Ljava/lang/String; = "Clubs - Club Play view"

.field public static final Roster:Ljava/lang/String; = "Clubs - Club Roster view"

.field public static final SearchClub:Ljava/lang/String; = "Clubs - Search Clubs view"

.field public static final SearchResults:Ljava/lang/String; = "Clubs - Search Results view"

.field public static final TitlePicker:Ljava/lang/String; = "Clubs - Title Picker view"

.field public static final Watch:Ljava/lang/String; = "Clubs - Watch View"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
