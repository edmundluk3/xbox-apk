.class public abstract Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
.super Ljava/lang/Object;
.source "EditorialDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SystemTagGroup"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    new-instance v0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract groupName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "systemTagGroup.name"
    .end annotation
.end method

.method public abstract tags()Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "systemTagGroup.Tags"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation
.end method
