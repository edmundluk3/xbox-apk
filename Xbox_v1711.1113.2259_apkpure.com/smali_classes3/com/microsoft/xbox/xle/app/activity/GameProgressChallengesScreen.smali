.class public Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "GameProgressChallengesScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen$GameProgressChallengesScreenAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen$GameProgressChallengesScreenAdapterProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 20
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen$GameProgressChallengesScreenAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen$GameProgressChallengesScreenAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen$GameProgressChallengesScreenAdapterProvider;

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attr"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen$GameProgressChallengesScreenAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen$GameProgressChallengesScreenAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen$GameProgressChallengesScreenAdapterProvider;

    .line 26
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "GameProgressChallenges"

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen;->onCreateContentView()V

    .line 34
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen$GameProgressChallengesScreenAdapterProvider;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 35
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->onCreate()V

    .line 37
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 38
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 42
    const v0, 0x7f03012c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameProgressChallengesScreen;->setContentView(I)V

    .line 43
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method
