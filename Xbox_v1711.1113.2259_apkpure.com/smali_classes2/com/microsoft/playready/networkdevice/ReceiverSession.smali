.class Lcom/microsoft/playready/networkdevice/ReceiverSession;
.super Ljava/lang/Object;
.source "ReceiverSession.java"

# interfaces
.implements Lcom/microsoft/playready/networkdevice/IReceiverSession;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/networkdevice/ReceiverSession$1;,
        Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;,
        Lcom/microsoft/playready/networkdevice/ReceiverSession$CloseRunnable;
    }
.end annotation


# static fields
.field private static final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private static s_syncObject:Ljava/lang/Object;


# instance fields
.field private final cExpirationThresholdInMs:J

.field private final cMillisecondsBeforeExpiration:I

.field private mDrmProxy:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

.field private final mExpirationTimers:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;",
            ">;"
        }
    .end annotation
.end field

.field private final mReceiver:Lcom/microsoft/playready/networkdevice/Receiver;

.field private mSessionID:[B

.field private mSessionStart:Ljava/util/Date;

.field private mTransmitter:Lcom/microsoft/playready/networkdevice/TransmitterInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->s_syncObject:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Lcom/microsoft/playready/networkdevice/IPRNDFactory;Lcom/microsoft/playready/networkdevice/TransmitterInfo;Lcom/microsoft/playready/networkdevice/Receiver;)V
    .locals 3
    .param p1, "prFactory"    # Lcom/microsoft/playready/networkdevice/IPRNDFactory;
    .param p2, "transmitter"    # Lcom/microsoft/playready/networkdevice/TransmitterInfo;
    .param p3, "receiver"    # Lcom/microsoft/playready/networkdevice/Receiver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object v2, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mSessionStart:Ljava/util/Date;

    .line 48
    const v0, 0x927c0

    iput v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->cMillisecondsBeforeExpiration:I

    .line 62
    iput-object p2, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mTransmitter:Lcom/microsoft/playready/networkdevice/TransmitterInfo;

    .line 63
    iput-object p3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mReceiver:Lcom/microsoft/playready/networkdevice/Receiver;

    .line 64
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExpirationTimers:Ljava/util/Hashtable;

    .line 66
    const-wide/32 v0, 0xa439040

    iput-wide v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->cExpirationThresholdInMs:J

    .line 69
    new-instance v0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-direct {v0, p1, p0}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;-><init>(Lcom/microsoft/playready/networkdevice/IPRNDFactory;Lcom/microsoft/playready/networkdevice/IReceiverSession;)V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mDrmProxy:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    .line 71
    invoke-direct {p0, v2}, Lcom/microsoft/playready/networkdevice/ReceiverSession;->startSession([B)V

    .line 72
    return-void
.end method

.method static synthetic access$000(Ljava/util/Hashtable;Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)V
    .locals 0
    .param p0, "x0"    # Ljava/util/Hashtable;
    .param p1, "x1"    # Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/microsoft/playready/networkdevice/ReceiverSession;->close(Ljava/util/Hashtable;Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)V

    return-void
.end method

.method private static close(Ljava/util/Hashtable;Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)V
    .locals 5
    .param p1, "drmProxy"    # Lcom/microsoft/playready/networkdevice/PrndDrmProxy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;",
            ">;",
            "Lcom/microsoft/playready/networkdevice/PrndDrmProxy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 315
    .local p0, "expirationTimers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;>;"
    sget-object v4, Lcom/microsoft/playready/networkdevice/ReceiverSession;->s_syncObject:Ljava/lang/Object;

    monitor-enter v4

    .line 317
    :try_start_0
    invoke-virtual {p0}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 319
    .local v1, "key":Ljava/lang/Long;
    invoke-virtual {p0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;

    .line 321
    .local v2, "task":Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
    invoke-virtual {v2}, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->cancel()Z

    goto :goto_0

    .line 325
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/Long;
    .end local v2    # "task":Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 324
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->receiverStop()V

    .line 325
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 326
    return-void
.end method

.method private startSession([B)V
    .locals 18
    .param p1, "previousSessionID"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 377
    const/4 v5, 0x0

    .line 378
    .local v5, "customDataTypeID":[B
    const/4 v6, 0x0

    .line 380
    .local v6, "customData":[B
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mReceiver:Lcom/microsoft/playready/networkdevice/Receiver;

    invoke-virtual {v2}, Lcom/microsoft/playready/networkdevice/Receiver;->getReceiverPlugin()Lcom/microsoft/playready/networkdevice/IReceiverPlugin;

    move-result-object v12

    .line 382
    .local v12, "plugin":Lcom/microsoft/playready/networkdevice/IReceiverPlugin;
    if-eqz v12, :cond_0

    .line 384
    invoke-interface {v12}, Lcom/microsoft/playready/networkdevice/IReceiverPlugin;->getDeviceRegistrationCustomData()Lcom/microsoft/playready/networkdevice/CustomData;

    move-result-object v8

    .line 386
    .local v8, "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    if-eqz v8, :cond_0

    .line 388
    iget-object v5, v8, Lcom/microsoft/playready/networkdevice/CustomData;->CustomDataTypeID:[B

    .line 389
    iget-object v6, v8, Lcom/microsoft/playready/networkdevice/CustomData;->CustomData:[B

    .line 393
    .end local v8    # "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    :cond_0
    new-instance v14, Ljava/util/Date;

    invoke-direct {v14}, Ljava/util/Date;-><init>()V

    .line 396
    .local v14, "start":Ljava/util/Date;
    new-instance v9, Ljava/util/Date;

    invoke-virtual {v14}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/32 v16, 0x1d4c0

    add-long v2, v2, v16

    invoke-direct {v9, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 398
    .local v9, "end":Ljava/util/Date;
    sget-object v16, Lcom/microsoft/playready/networkdevice/ReceiverSession;->s_syncObject:Ljava/lang/Object;

    monitor-enter v16

    .line 400
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mTransmitter:Lcom/microsoft/playready/networkdevice/TransmitterInfo;

    invoke-virtual {v2}, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->getProtocolAddress()Ljava/net/InetSocketAddress;

    move-result-object v13

    .line 402
    .local v13, "protAddress":Ljava/net/InetSocketAddress;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mDrmProxy:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-virtual {v13}, Ljava/net/InetSocketAddress;->getHostString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v4

    move-object/from16 v7, p1

    invoke-virtual/range {v2 .. v7}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->receiverStart(Ljava/lang/String;I[B[B[B)[B

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mSessionID:[B

    .line 408
    monitor-exit v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    const-string v2, "ReceiverSession"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sessionID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/playready/networkdevice/ReceiverSession;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :goto_0
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v9}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 416
    new-instance v2, Ljava/util/concurrent/TimeoutException;

    const-string v3, "PRND Proximity detection failed."

    invoke-direct {v2, v3}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 408
    .end local v13    # "protAddress":Ljava/net/InetSocketAddress;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v16
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 419
    .restart local v13    # "protAddress":Ljava/net/InetSocketAddress;
    :cond_1
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 421
    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2}, Ljava/lang/InterruptedException;-><init>()V

    throw v2

    .line 424
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mDrmProxy:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-virtual {v2}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->receiverProximityCheck()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 434
    sget-object v3, Lcom/microsoft/playready/networkdevice/ReceiverSession;->s_syncObject:Ljava/lang/Object;

    monitor-enter v3

    .line 436
    :try_start_2
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mSessionStart:Ljava/util/Date;

    .line 438
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExpirationTimers:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    .line 440
    .local v11, "key":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExpirationTimers:Ljava/util/Hashtable;

    invoke-virtual {v2, v11}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;

    .line 442
    .local v15, "task":Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->schedule(J)V

    goto :goto_1

    .line 444
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "key":Ljava/lang/Long;
    .end local v15    # "task":Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 431
    :cond_3
    const-wide/16 v2, 0x32

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0

    .line 444
    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_4
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 445
    return-void
.end method


# virtual methods
.method public addSessionExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;

    .prologue
    .line 125
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->cExpirationThresholdInMs:J

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/microsoft/playready/networkdevice/ReceiverSession;->addSessionExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;JLjava/util/concurrent/TimeUnit;)V

    .line 126
    return-void
.end method

.method public addSessionExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;JLjava/util/concurrent/TimeUnit;)V
    .locals 20
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;
    .param p2, "expirationNotificationTime"    # J
    .param p4, "timeUnit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 144
    move-wide/from16 v12, p2

    .line 145
    .local v12, "timeoutMs":J
    const-wide/16 v8, 0x1

    .line 146
    .local v8, "multiplier":J
    const-wide v14, 0x7fffffffffffffffL

    div-long v4, v14, v12

    .line 148
    .local v4, "maxMultiplier":J
    sget-object v11, Lcom/microsoft/playready/networkdevice/ReceiverSession$1;->$SwitchMap$java$util$concurrent$TimeUnit:[I

    invoke-virtual/range {p4 .. p4}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v14

    aget v11, v11, v14

    packed-switch v11, :pswitch_data_0

    .line 170
    new-instance v11, Ljava/lang/IllegalArgumentException;

    const-string v14, "expirationNotificationTime"

    invoke-direct {v11, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 154
    :pswitch_0
    const-wide/16 v8, 0x3e8

    .line 174
    :goto_0
    :pswitch_1
    cmp-long v11, v4, v8

    if-gez v11, :cond_0

    .line 176
    new-instance v11, Ljava/lang/RuntimeException;

    const-string v14, "integer overflow"

    invoke-direct {v11, v14}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 158
    :pswitch_2
    const-wide/32 v8, 0xea60

    .line 159
    goto :goto_0

    .line 162
    :pswitch_3
    const-wide/32 v8, 0x36ee80

    .line 163
    goto :goto_0

    .line 166
    :pswitch_4
    const-wide/32 v8, 0x5265c00

    .line 167
    goto :goto_0

    .line 179
    :cond_0
    mul-long/2addr v12, v8

    .line 181
    sget-object v14, Lcom/microsoft/playready/networkdevice/ReceiverSession;->s_syncObject:Ljava/lang/Object;

    monitor-enter v14

    .line 183
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExpirationTimers:Ljava/util/Hashtable;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v11, v15}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 185
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExpirationTimers:Ljava/util/Hashtable;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v11, v15}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;

    .line 187
    .local v10, "task":Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->addReceiverNearExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;)V

    .line 207
    :goto_1
    monitor-exit v14

    .line 208
    return-void

    .line 191
    .end local v10    # "task":Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
    :cond_1
    new-instance v10, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;-><init>(Lcom/microsoft/playready/networkdevice/ReceiverSession;)V

    .line 193
    .restart local v10    # "task":Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
    new-instance v11, Ljava/util/Date;

    invoke-direct {v11}, Ljava/util/Date;-><init>()V

    invoke-virtual {v11}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mSessionStart:Ljava/util/Date;

    invoke-virtual {v11}, Ljava/util/Date;->getTime()J

    move-result-wide v18

    sub-long v6, v16, v18

    .line 194
    .local v6, "msFromStart":J
    sub-long v2, v12, v6

    .line 196
    .local v2, "diff":J
    const-wide/16 v16, 0x0

    cmp-long v11, v2, v16

    if-lez v11, :cond_2

    .line 198
    invoke-virtual {v10, v2, v3}, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->schedule(J)V

    .line 205
    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->addReceiverNearExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;)V

    goto :goto_1

    .line 207
    .end local v2    # "diff":J
    .end local v6    # "msFromStart":J
    .end local v10    # "task":Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
    :catchall_0
    move-exception v11

    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v11

    .line 202
    .restart local v2    # "diff":J
    .restart local v6    # "msFromStart":J
    .restart local v10    # "task":Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
    :cond_2
    :try_start_1
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;->onExpirationThreshold(Lcom/microsoft/playready/networkdevice/IReceiverSession;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public close()V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExpirationTimers:Ljava/util/Hashtable;

    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mDrmProxy:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-static {v0, v1}, Lcom/microsoft/playready/networkdevice/ReceiverSession;->close(Ljava/util/Hashtable;Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)V

    .line 311
    return-void
.end method

.method public fetchLicense(Lcom/microsoft/playready/networkdevice/ContentIDType;[B)Z
    .locals 6
    .param p1, "contentIdentifierType"    # Lcom/microsoft/playready/networkdevice/ContentIDType;
    .param p2, "contentIdentifier"    # [B

    .prologue
    .line 275
    const/4 v2, 0x0

    .line 276
    .local v2, "customDataType":[B
    const/4 v1, 0x0

    .line 278
    .local v1, "customData":[B
    const-string v4, "ReceiverSession"

    const-string v5, "license fetch"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mReceiver:Lcom/microsoft/playready/networkdevice/Receiver;

    invoke-virtual {v4}, Lcom/microsoft/playready/networkdevice/Receiver;->getReceiverPlugin()Lcom/microsoft/playready/networkdevice/IReceiverPlugin;

    move-result-object v3

    .line 282
    .local v3, "plugin":Lcom/microsoft/playready/networkdevice/IReceiverPlugin;
    if-eqz v3, :cond_0

    .line 284
    invoke-interface {v3, p1, p2}, Lcom/microsoft/playready/networkdevice/IReceiverPlugin;->getLicenseFetchCustomData(Lcom/microsoft/playready/networkdevice/ContentIDType;[B)Lcom/microsoft/playready/networkdevice/CustomData;

    move-result-object v0

    .line 288
    .local v0, "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    if-eqz v0, :cond_0

    .line 290
    iget-object v2, v0, Lcom/microsoft/playready/networkdevice/CustomData;->CustomDataTypeID:[B

    .line 291
    iget-object v1, v0, Lcom/microsoft/playready/networkdevice/CustomData;->CustomData:[B

    .line 295
    .end local v0    # "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    :cond_0
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mDrmProxy:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-virtual {p1}, Lcom/microsoft/playready/networkdevice/ContentIDType;->ordinal()I

    move-result v5

    invoke-virtual {v4, v5, p2, v2, v1}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->receiverFetchLicense(I[B[B[B)Z

    move-result v4

    return v4
.end method

.method protected finalize()V
    .locals 4

    .prologue
    .line 372
    sget-object v0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/microsoft/playready/networkdevice/ReceiverSession$CloseRunnable;

    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExpirationTimers:Ljava/util/Hashtable;

    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mDrmProxy:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-direct {v1, p0, v2, v3}, Lcom/microsoft/playready/networkdevice/ReceiverSession$CloseRunnable;-><init>(Lcom/microsoft/playready/networkdevice/ReceiverSession;Ljava/util/Hashtable;Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 373
    return-void
.end method

.method public getID()[B
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mSessionID:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public getReceiverPlugin()Lcom/microsoft/playready/networkdevice/IReceiverPlugin;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mReceiver:Lcom/microsoft/playready/networkdevice/Receiver;

    invoke-virtual {v0}, Lcom/microsoft/playready/networkdevice/Receiver;->getReceiverPlugin()Lcom/microsoft/playready/networkdevice/IReceiverPlugin;

    move-result-object v0

    return-object v0
.end method

.method public getSessionStartTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mSessionStart:Ljava/util/Date;

    return-object v0
.end method

.method public getTransmitter()Lcom/microsoft/playready/networkdevice/TransmitterInfo;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mTransmitter:Lcom/microsoft/playready/networkdevice/TransmitterInfo;

    return-object v0
.end method

.method public removeSessionExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;

    .prologue
    .line 219
    sget-object v4, Lcom/microsoft/playready/networkdevice/ReceiverSession;->s_syncObject:Ljava/lang/Object;

    monitor-enter v4

    .line 221
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExpirationTimers:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 223
    .local v1, "key":Ljava/lang/Long;
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExpirationTimers:Ljava/util/Hashtable;

    invoke-virtual {v3, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;

    .line 225
    .local v2, "task":Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
    invoke-virtual {v2, p1}, Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;->removeReceiverNearExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;)V

    goto :goto_0

    .line 227
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/Long;
    .end local v2    # "task":Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228
    return-void
.end method

.method restart()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 338
    invoke-virtual {p0}, Lcom/microsoft/playready/networkdevice/ReceiverSession;->getID()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/playready/networkdevice/ReceiverSession;->startSession([B)V

    .line 339
    return-void
.end method

.method public restartSession()Lcom/microsoft/playready/networkdevice/IReceiverSessionTask;
    .locals 4

    .prologue
    .line 87
    const-string v2, "Receiver"

    const-string v3, "restarting session"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-instance v1, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;

    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mReceiver:Lcom/microsoft/playready/networkdevice/Receiver;

    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mTransmitter:Lcom/microsoft/playready/networkdevice/TransmitterInfo;

    invoke-direct {v1, v2, v3, p0}, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;-><init>(Lcom/microsoft/playready/networkdevice/Receiver;Lcom/microsoft/playready/networkdevice/TransmitterInfo;Lcom/microsoft/playready/networkdevice/IReceiverSession;)V

    .line 90
    .local v1, "worker":Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;
    new-instance v0, Lcom/microsoft/playready/networkdevice/ReceiverSessionTask;

    invoke-direct {v0, v1}, Lcom/microsoft/playready/networkdevice/ReceiverSessionTask;-><init>(Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;)V

    .line 92
    .local v0, "task":Lcom/microsoft/playready/networkdevice/ReceiverSessionTask;
    sget-object v2, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v2, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 94
    return-object v0
.end method

.method setupDrmProxy(Lcom/microsoft/playready/MediaPlayer;)V
    .locals 1
    .param p1, "mediaPlayer"    # Lcom/microsoft/playready/MediaPlayer;

    .prologue
    .line 333
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mDrmProxy:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->initializeNativeDrmProxyPointer(Lcom/microsoft/playready/MediaPlayer;)V

    .line 334
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 104
    const-string v2, ""

    .line 105
    .local v2, "id":Ljava/lang/String;
    const-string v0, "%02x"

    .line 106
    .local v0, "fmt":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mSessionID:[B

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 108
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession;->mSessionID:[B

    aget-byte v6, v6, v1

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 106
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 111
    :cond_0
    return-object v2
.end method
