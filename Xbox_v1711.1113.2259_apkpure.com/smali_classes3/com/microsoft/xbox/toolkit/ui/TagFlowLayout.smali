.class public Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;
.super Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;
.source "TagFlowLayout.java"


# instance fields
.field private maxRows:I

.field private tagCountView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method private getTagAdapter()Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->adapter:Landroid/widget/Adapter;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    return-object v0
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 22
    .param p1, "changed"    # Z
    .param p2, "leftPosition"    # I
    .param p3, "topPosition"    # I
    .param p4, "rightPosition"    # I
    .param p5, "bottomPosition"    # I

    .prologue
    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getMeasuredWidth()I

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getPaddingRight()I

    move-result v21

    sub-int v11, v20, v21

    .line 108
    .local v11, "maxInternalWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getPaddingLeft()I

    move-result v18

    .line 109
    .local v18, "x":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getPaddingTop()I

    move-result v19

    .line 111
    .local v19, "y":I
    const/16 v16, 0x0

    .line 112
    .local v16, "rowIndex":I
    const/4 v8, 0x0

    .line 114
    .local v8, "displayedChildren":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getChildCount()I

    move-result v12

    .line 115
    .local v12, "numChildren":I
    add-int/lit8 v13, v12, -0x1

    .line 117
    .local v13, "numTags":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v12, :cond_6

    .line 118
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 119
    .local v4, "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    instance-of v10, v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$PlaceholderTag;

    .line 121
    .local v10, "isPlaceholder":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->maxRows:I

    move/from16 v20, v0

    if-lez v20, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->maxRows:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    if-eqz v10, :cond_0

    if-ne v8, v13, :cond_1

    .line 117
    :cond_0
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 125
    :cond_1
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v20

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_0

    .line 129
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    instance-of v0, v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$PlaceholderTag;

    move/from16 v20, v0

    if-eqz v20, :cond_2

    .line 130
    add-int/lit8 v20, v12, -0x1

    sub-int v20, v20, v8

    if-eqz v20, :cond_0

    .line 134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->tagCountView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f0e0aa2

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 135
    .local v17, "tagText":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    sub-int v20, v13, v8

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 138
    .end local v17    # "tagText":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    :cond_2
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 139
    .local v7, "childWidth":I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 140
    .local v5, "childHeight":I
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    .line 142
    .local v6, "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    add-int v20, v18, v7

    iget v0, v6, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->leftMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move/from16 v0, v20

    if-le v0, v11, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->rowHeights:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-ge v0, v1, :cond_4

    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getPaddingLeft()I

    move-result v18

    .line 145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->rowHeights:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v20

    add-int v19, v19, v20

    .line 146
    add-int/lit8 v16, v16, 0x1

    .line 164
    :cond_3
    iget-boolean v0, v6, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->centerVertical:Z

    move/from16 v20, v0

    if-eqz v20, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->rowHeights:Ljava/util/List;

    move-object/from16 v20, v0

    .line 165
    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v20

    sub-int v20, v20, v5

    div-int/lit8 v20, v20, 0x2

    add-int v3, v19, v20

    .line 168
    .local v3, "_y":I
    :goto_2
    iget v0, v6, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->leftMargin:I

    move/from16 v20, v0

    add-int v18, v18, v20

    .line 169
    iget v0, v6, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->topMargin:I

    move/from16 v20, v0

    add-int v3, v3, v20

    .line 171
    add-int v20, v18, v7

    add-int v21, v3, v5

    move/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v4, v0, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 172
    add-int/lit8 v8, v8, 0x1

    .line 174
    iget v0, v6, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->rightMargin:I

    move/from16 v20, v0

    add-int v20, v20, v7

    add-int v18, v18, v20

    goto/16 :goto_1

    .line 147
    .end local v3    # "_y":I
    :cond_4
    if-nez v10, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->tagCountView:Landroid/view/View;

    move-object/from16 v20, v0

    if-eqz v20, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->maxRows:I

    move/from16 v20, v0

    if-lez v20, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->maxRows:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->tagCountView:Landroid/view/View;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    .line 151
    .local v15, "placeholderWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->tagCountView:Landroid/view/View;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    .line 153
    .local v14, "placeholderLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    add-int v20, v18, v7

    iget v0, v6, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->leftMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    add-int v20, v20, v15

    iget v0, v14, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->leftMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move/from16 v0, v20

    if-le v0, v11, :cond_3

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->tagCountView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f0e0aa2

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 157
    .restart local v17    # "tagText":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    add-int/lit8 v20, v12, -0x1

    sub-int v20, v20, v8

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 159
    add-int/lit8 v16, v16, 0x1

    .line 160
    goto/16 :goto_1

    .end local v14    # "placeholderLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    .end local v15    # "placeholderWidth":I
    .end local v17    # "tagText":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    :cond_5
    move/from16 v3, v19

    .line 165
    goto/16 :goto_2

    .line 176
    .end local v4    # "child":Landroid/view/View;
    .end local v5    # "childHeight":I
    .end local v6    # "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    .end local v7    # "childWidth":I
    .end local v10    # "isPlaceholder":Z
    :cond_6
    return-void
.end method

.method protected onMeasure(II)V
    .locals 18
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 40
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->rowHeights:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 42
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getHorizontalPadding()I

    move-result v16

    sub-int v10, v15, v16

    .line 44
    .local v10, "maxInternalWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getChildCount()I

    move-result v11

    .line 46
    .local v11, "numChildren":I
    const/4 v8, 0x0

    .line 47
    .local v8, "currentLineWidth":I
    const/4 v7, 0x0

    .line 48
    .local v7, "currentLineHeight":I
    const/4 v13, 0x0

    .line 49
    .local v13, "requiredWidth":I
    const/4 v12, 0x0

    .line 50
    .local v12, "requiredHeight":I
    const/4 v14, 0x0

    .line 52
    .local v14, "rowIndex":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v11, :cond_7

    .line 53
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 55
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v15

    const/16 v16, 0x8

    move/from16 v0, v16

    if-eq v15, v0, :cond_2

    .line 56
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v3, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->measureChild(Landroid/view/View;II)V

    .line 58
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    .line 60
    .local v5, "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    iget v0, v5, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->leftMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    iget v0, v5, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->rightMargin:I

    move/from16 v16, v0

    add-int v6, v15, v16

    .line 61
    .local v6, "childWidth":I
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    iget v0, v5, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->topMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    iget v0, v5, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->bottomMargin:I

    move/from16 v16, v0

    add-int v4, v15, v16

    .line 63
    .local v4, "childHeight":I
    add-int v15, v8, v6

    if-le v15, v10, :cond_6

    .line 64
    invoke-static {v8, v13}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 66
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->rowHeights:Ljava/util/List;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v14, v14, 0x1

    .line 69
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->maxRows:I

    if-eqz v15, :cond_0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->maxRows:I

    if-lt v14, v15, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v15

    instance-of v15, v15, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$PlaceholderTag;

    if-eqz v15, :cond_1

    .line 72
    :cond_0
    add-int/2addr v12, v7

    .line 75
    :cond_1
    move v8, v6

    .line 76
    move v7, v4

    .line 83
    .end local v4    # "childHeight":I
    .end local v5    # "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    .end local v6    # "childWidth":I
    :cond_2
    :goto_1
    add-int/lit8 v15, v11, -0x1

    if-ne v9, v15, :cond_5

    const/4 v15, 0x1

    if-le v11, v15, :cond_5

    .line 84
    invoke-static {v8, v13}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 85
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->maxRows:I

    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->maxRows:I

    if-lt v14, v15, :cond_3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v15

    instance-of v15, v15, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$PlaceholderTag;

    if-eqz v15, :cond_4

    .line 88
    :cond_3
    add-int/2addr v12, v7

    .line 91
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->rowHeights:Ljava/util/List;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 78
    .restart local v4    # "childHeight":I
    .restart local v5    # "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    .restart local v6    # "childWidth":I
    :cond_6
    add-int/2addr v8, v6

    .line 79
    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v7

    goto :goto_1

    .line 96
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "childHeight":I
    .end local v5    # "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    .end local v6    # "childWidth":I
    :cond_7
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v15

    const/high16 v16, 0x40000000    # 2.0f

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 97
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    .line 99
    :goto_2
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v16

    const/high16 v17, 0x40000000    # 2.0f

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_9

    .line 100
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v16

    .line 95
    :goto_3
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setMeasuredDimension(II)V

    .line 102
    return-void

    .line 98
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getHorizontalPadding()I

    move-result v15

    add-int/2addr v15, v13

    goto :goto_2

    .line 101
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getVerticalPadding()I

    move-result v16

    add-int v16, v16, v12

    goto :goto_3
.end method

.method protected populateFromAdapter()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 180
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->removeAllViewsInLayout()V

    .line 182
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->adapter:Landroid/widget/Adapter;

    if-eqz v3, :cond_1

    .line 183
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getTagAdapter()Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 184
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getTagAdapter()Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-result-object v3

    invoke-virtual {v3, v0, v5, p0}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 185
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 186
    .local v1, "lays":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    .end local v1    # "lays":Landroid/view/ViewGroup$LayoutParams;
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->getTagAdapter()Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4, v5, p0}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 190
    .restart local v2    # "v":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 191
    .restart local v1    # "lays":Landroid/view/ViewGroup$LayoutParams;
    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->tagCountView:Landroid/view/View;

    .line 192
    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 195
    .end local v0    # "i":I
    .end local v1    # "lays":Landroid/view/ViewGroup$LayoutParams;
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->requestLayout()V

    .line 196
    return-void
.end method

.method public setMaxRows(I)V
    .locals 4
    .param p1, "maxRows"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 30
    const-wide/16 v0, 0x1

    int-to-long v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 31
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->maxRows:I

    .line 32
    return-void
.end method
