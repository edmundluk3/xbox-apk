.class Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProgrammingModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProgrammingModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetDiscoverListRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ProgrammingModel;

.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProgrammingModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProgrammingModel;Lcom/microsoft/xbox/service/model/ProgrammingModel;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProgrammingModel;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;->this$0:Lcom/microsoft/xbox/service/model/ProgrammingModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 109
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;->caller:Lcom/microsoft/xbox/service/model/ProgrammingModel;

    .line 110
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 114
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getEDSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    move-result-object v4

    .line 115
    .local v4, "edsmanager":Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getProgrammingServiceManager()Lcom/microsoft/xbox/service/network/managers/ISGFeaturedServiceManager;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/microsoft/xbox/service/network/managers/ISGFeaturedServiceManager;->getProgrammingContentManifest()Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;

    move-result-object v9

    .line 116
    .local v9, "result":Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v8, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    if-eqz v9, :cond_d

    iget-object v0, v9, Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;->Content:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    if-eqz v17, :cond_d

    .line 118
    iget-object v0, v9, Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;->Content:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_d

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlotGroup;

    .line 119
    .local v14, "slotgroup":Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlotGroup;
    iget-object v15, v14, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlotGroup;->SlotList:Ljava/util/List;

    .line 120
    .local v15, "slots":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;>;"
    if-eqz v15, :cond_0

    .line 121
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_1
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_0

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;

    .line 122
    .local v13, "slot":Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;
    const/4 v6, 0x0

    .line 123
    .local v6, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const/4 v3, 0x0

    .line 124
    .local v3, "donotuseID":Z
    if-eqz v13, :cond_1

    iget-object v0, v13, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;->Action:Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;

    move-object/from16 v19, v0

    if-eqz v19, :cond_1

    .line 125
    iget-object v1, v13, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;->Action:Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;

    .line 126
    .local v1, "action":Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;
    sget-object v19, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Game:Ljava/lang/String;

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 127
    new-instance v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    .end local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-direct {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;-><init>()V

    .line 128
    .restart local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const-string v19, "DGame"

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 174
    :cond_2
    :goto_1
    if-eqz v6, :cond_1

    .line 175
    if-nez v3, :cond_3

    .line 176
    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Target:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 178
    :cond_3
    iget-object v0, v13, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;->Title:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    .line 179
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    .line 180
    new-instance v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;-><init>()V

    .line 181
    .local v5, "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    const-string v19, "BoxArt"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->setPurpose(Ljava/lang/String;)V

    .line 183
    iget-object v0, v13, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;->ImageUrl:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_4

    .line 184
    iget-object v0, v13, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;->ImageUrl:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v16

    .line 185
    .local v16, "uri":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_4

    .line 186
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->setUrl(Ljava/lang/String;)V

    .line 189
    .end local v16    # "uri":Ljava/lang/String;
    :cond_4
    iget-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    .end local v5    # "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    :cond_5
    sget-object v19, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Movie:Ljava/lang/String;

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 131
    new-instance v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    .end local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-direct {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;-><init>()V

    .line 132
    .restart local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const-string v19, "Movie"

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    goto :goto_1

    .line 133
    :cond_6
    sget-object v19, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->App:Ljava/lang/String;

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 134
    new-instance v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    .end local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-direct {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;-><init>()V

    .line 135
    .restart local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const-string v19, "DApp"

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    goto :goto_1

    .line 136
    :cond_7
    sget-object v19, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Album:Ljava/lang/String;

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 137
    new-instance v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    .end local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-direct {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;-><init>()V

    .line 138
    .restart local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const-string v19, "Album"

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    goto/16 :goto_1

    .line 139
    :cond_8
    sget-object v19, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Aritst:Ljava/lang/String;

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 140
    new-instance v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    .end local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-direct {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;-><init>()V

    .line 141
    .restart local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const-string v19, "MusicArtist"

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    goto/16 :goto_1

    .line 142
    :cond_9
    sget-object v19, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->TV:Ljava/lang/String;

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 143
    new-instance v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    .end local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-direct {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;-><init>()V

    .line 144
    .restart local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const-string v19, "TVSeries"

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    goto/16 :goto_1

    .line 145
    :cond_a
    sget-object v19, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Season:Ljava/lang/String;

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 146
    new-instance v12, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    invoke-direct {v12}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;-><init>()V

    .line 147
    .local v12, "seasonItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Target:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iput-object v0, v12, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->ID:Ljava/lang/String;

    .line 150
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v2, "bingIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Target:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    :try_start_0
    invoke-interface {v4, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;->getSeriesFromSeaonId(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v11

    .line 154
    .local v11, "seasonDetails":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    if-eqz v11, :cond_2

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_2

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v0, v19

    instance-of v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    move/from16 v19, v0

    if-eqz v19, :cond_2

    .line 155
    new-instance v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    invoke-direct {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    .end local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .local v7, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :try_start_1
    const-string v19, "TVSeries"

    move-object/from16 v0, v19

    iput-object v0, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 157
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .line 158
    .local v10, "season":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->getParentSeries()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ID:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iput-object v0, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 159
    const/4 v3, 0x1

    move-object v6, v7

    .end local v7    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .restart local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto/16 :goto_1

    .line 166
    .end local v2    # "bingIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v10    # "season":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    .end local v11    # "seasonDetails":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .end local v12    # "seasonItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    :cond_b
    sget-object v19, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Episode:Ljava/lang/String;

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 167
    new-instance v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    .end local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-direct {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;-><init>()V

    .line 168
    .restart local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const-string v19, "TVEpisode"

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    goto/16 :goto_1

    .line 169
    :cond_c
    sget-object v19, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Activity:Ljava/lang/String;

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 170
    new-instance v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .end local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-direct {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;-><init>()V

    .line 171
    .restart local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const-string v19, "DActivity"

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    goto/16 :goto_1

    .line 199
    .end local v1    # "action":Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;
    .end local v3    # "donotuseID":Z
    .end local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v13    # "slot":Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;
    .end local v14    # "slotgroup":Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlotGroup;
    .end local v15    # "slots":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;>;"
    :cond_d
    return-object v8

    .line 162
    .restart local v1    # "action":Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;
    .restart local v2    # "bingIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v3    # "donotuseID":Z
    .restart local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .restart local v12    # "seasonItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    .restart local v13    # "slot":Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;
    .restart local v14    # "slotgroup":Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlotGroup;
    .restart local v15    # "slots":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;>;"
    :catch_0
    move-exception v19

    goto/16 :goto_1

    .end local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .restart local v7    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .restart local v11    # "seasonDetails":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :catch_1
    move-exception v19

    move-object v6, v7

    .end local v7    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .restart local v6    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto/16 :goto_1
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 213
    const-wide/16 v0, 0xfa1

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 208
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProgrammingModel$GetDiscoverListRunner;->caller:Lcom/microsoft/xbox/service/model/ProgrammingModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 209
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 204
    return-void
.end method
