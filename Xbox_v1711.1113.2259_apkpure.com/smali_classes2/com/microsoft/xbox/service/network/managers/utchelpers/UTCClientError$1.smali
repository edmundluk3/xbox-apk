.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;
.super Ljava/lang/Object;
.source "UTCClientError.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackPartyChatError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$errorCode:Ljava/lang/String;

.field final synthetic val$errorName:Ljava/lang/String;

.field final synthetic val$exception:Ljava/lang/Exception;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$errorName:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$errorCode:Ljava/lang/String;

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$exception:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 78
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$errorName:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$errorCode:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 81
    new-instance v1, Lxbox/smartglass/ClientError;

    invoke-direct {v1}, Lxbox/smartglass/ClientError;-><init>()V

    .line 82
    .local v1, "error":Lxbox/smartglass/ClientError;
    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lxbox/smartglass/ClientError;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 83
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$errorName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lxbox/smartglass/ClientError;->setErrorText(Ljava/lang/String;)V

    .line 84
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$errorCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lxbox/smartglass/ClientError;->setErrorCode(Ljava/lang/String;)V

    .line 85
    const-string v0, "unknown"

    .line 86
    .local v0, "callStack":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$exception:Ljava/lang/Exception;

    if-eqz v2, :cond_0

    .line 87
    const-string v2, "%s: %s"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$exception:Ljava/lang/Exception;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$exception:Ljava/lang/Exception;

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-virtual {v1, v0}, Lxbox/smartglass/ClientError;->setCallStack(Ljava/lang/String;)V

    .line 91
    :cond_0
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 92
    const-string v2, "ClientError:ErrorName %s, errorCode %s, \nCALLSTACK:\n%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$errorName:Ljava/lang/String;

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;->val$errorCode:Ljava/lang/String;

    aput-object v4, v3, v5

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    return-void
.end method
