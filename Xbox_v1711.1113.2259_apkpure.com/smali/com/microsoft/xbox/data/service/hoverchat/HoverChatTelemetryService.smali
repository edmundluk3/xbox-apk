.class public Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
.super Ljava/lang/Object;
.source "HoverChatTelemetryService.java"


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final MENU_TAB_ACTIONS:[Ljava/lang/String;

.field public static final MENU_TAB_NAMES:[Ljava/lang/String;

.field private static final TAB_SELECTED_PATTERN:Ljava/lang/String; = "02"


# instance fields
.field private currentTab:Ljava/lang/String;

.field private previousTab:Ljava/lang/String;

.field public final trackInboxTabPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public final trackInboxTabResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "ChatBubble - Conversations View"

    aput-object v1, v0, v2

    const-string v1, "ChatBubble - Clubs View"

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->MENU_TAB_NAMES:[Ljava/lang/String;

    .line 44
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "ChatBubble - Show Message Tab"

    aput-object v1, v0, v2

    const-string v1, "ChatBubble - Show Club Tab"

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->MENU_TAB_ACTIONS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;)Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->trackInboxTabPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    .line 74
    invoke-static {p0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;)Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->trackInboxTabResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    .line 92
    const-string v0, "ChatBubble - Conversations View"

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->currentTab:Ljava/lang/String;

    .line 93
    const-string v0, "ChatBubble - Conversations View"

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->previousTab:Ljava/lang/String;

    .line 94
    return-void
.end method

.method static synthetic lambda$addPeopleSelected$17(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 269
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 271
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 272
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 274
    const-string v1, "ChatBubble - Add People"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 275
    return-void
.end method

.method static synthetic lambda$clubChatOpenFromContextMenu$4(J)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 120
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 122
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 123
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 125
    const-string v1, "ChatBubble - Show Club as ChatBubble"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 126
    return-void
.end method

.method static synthetic lambda$clubChatViewed$33(J)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 438
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 440
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 441
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 443
    const-string v1, "ChatBubble - Club View"

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;)V

    .line 444
    return-void
.end method

.method static synthetic lambda$clubMessageDeleted$28(J)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 390
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 392
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 393
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 395
    const-string v1, "ChatBubble - Club Delete Message"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 396
    return-void
.end method

.method static synthetic lambda$clubMessageReportedToAdmins$30(J)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 412
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 414
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 415
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 417
    const-string v1, "ChatBubble - Club Report Message to Admin"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 418
    return-void
.end method

.method static synthetic lambda$clubMessageReportedToXbox$29(J)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 401
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 403
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 404
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 406
    const-string v1, "ChatBubble - Club Report Message to Xbox"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 407
    return-void
.end method

.method static synthetic lambda$clubMessageSent$27(J)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 379
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 381
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 382
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 384
    const-string v1, "ChatBubble - Club Send Message"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 385
    return-void
.end method

.method static synthetic lambda$clubNotificationTriggered$35(J)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 460
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 462
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 463
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 465
    const-string v1, "ChatBubble - Notification View"

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;)V

    .line 466
    return-void
.end method

.method static synthetic lambda$clubSettingChanged$37(JZ)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "newValue"    # Z

    .prologue
    .line 480
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 482
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 483
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ClubId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 484
    const-string v1, "Enabled"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 486
    const-string v1, "Settings - Show Club Notifications in ChatBubble"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 487
    return-void
.end method

.method static synthetic lambda$clubSettingsSelected$26(J)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 368
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 370
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 371
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 373
    const-string v1, "ChatBubble - Club Settings"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 374
    return-void
.end method

.method static synthetic lambda$collapsed$7(I)V
    .locals 6
    .param p0, "instanceCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 166
    const-wide/16 v2, 0x1

    int-to-long v4, p0

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 168
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 169
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleCount"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 171
    const-string v1, "ChatBubble - Hide Bubbles"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 172
    return-void
.end method

.method static synthetic lambda$conversationOpenedFromContextMenu$2(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 98
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 100
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 101
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 103
    const-string v1, "ChatBubble - Show Conversation as ChatBubble"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 104
    return-void
.end method

.method static synthetic lambda$conversationOpenedFromDropDown$3(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 109
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 111
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 112
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 114
    const-string v1, "ChatBubble - Open as ChatBubble"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 115
    return-void
.end method

.method static synthetic lambda$createGroupConvoSelected$13(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 225
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 227
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 228
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 230
    const-string v1, "ChatBubble - Create Group Conversation"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 231
    return-void
.end method

.method static synthetic lambda$expanded$6(I)V
    .locals 6
    .param p0, "instanceCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 155
    const-wide/16 v2, 0x1

    int-to-long v4, p0

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 157
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 158
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleCount"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 160
    const-string v1, "ChatBubble - Expand Bubbles"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 161
    return-void
.end method

.method static synthetic lambda$inboxClubChatSelected$10(J)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 192
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 194
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 195
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 197
    const-string v1, "ChatBubble - Show Club"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 198
    return-void
.end method

.method static synthetic lambda$inboxConversationSelected$9(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 181
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 183
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 184
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 186
    const-string v1, "ChatBubble - Show Conversation"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 187
    return-void
.end method

.method static synthetic lambda$inboxSettingsSelected$8()V
    .locals 1

    .prologue
    .line 176
    const-string v0, "ChatBubble - Settings"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$inboxViewed$31()V
    .locals 1

    .prologue
    .line 422
    const-string v0, "ChatBubble - Summary View"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$instanceClosed$5(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .locals 4
    .param p0, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 131
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 135
    instance-of v2, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    if-eqz v2, :cond_1

    .line 136
    check-cast p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    .end local p0    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->conversationId()Ljava/lang/String;

    move-result-object v0

    .line 146
    :goto_0
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 147
    .local v1, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v2, "BubbleId"

    invoke-virtual {v1, v2, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 149
    const-string v2, "ChatBubble - Delete Bubble"

    invoke-static {v2, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 150
    .end local v1    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_0
    :goto_1
    return-void

    .line 137
    .restart local p0    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :cond_1
    instance-of v2, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v2, :cond_2

    .line 138
    check-cast p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    .end local p0    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->clubId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .local v0, "id":Ljava/lang/Long;
    goto :goto_0

    .line 139
    .end local v0    # "id":Ljava/lang/Long;
    .restart local p0    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :cond_2
    instance-of v2, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MenuKey;

    if-nez v2, :cond_0

    .line 142
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic lambda$leaveGroupConvoSelected$19(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 291
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 293
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 294
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296
    const-string v1, "ChatBubble - Leave Group Conversation"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 297
    return-void
.end method

.method static synthetic lambda$messageDeleted$21(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 313
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 315
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 316
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 318
    const-string v1, "ChatBubble - Delete Message"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 319
    return-void
.end method

.method static synthetic lambda$messageNotificationTriggered$34(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 449
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 451
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 452
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 454
    const-string v1, "ChatBubble - Notification View"

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;)V

    .line 455
    return-void
.end method

.method static synthetic lambda$messageOfTheDaySelected$25(J)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 357
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 359
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 360
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 362
    const-string v1, "ChatBubble - Message of the Day"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 363
    return-void
.end method

.method static synthetic lambda$messageReported$23(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 335
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 337
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 338
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 340
    const-string v1, "ChatBubble - Report Message"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 341
    return-void
.end method

.method static synthetic lambda$messageSent$20(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 302
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 304
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 305
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 307
    const-string v1, "ChatBubble - Send Message"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 308
    return-void
.end method

.method static synthetic lambda$messageSettingChanged$36(Z)V
    .locals 3
    .param p0, "newValue"    # Z

    .prologue
    .line 471
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 472
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "Enabled"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474
    const-string v1, "Settings - Show Message Notifications in ChatBubble"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 475
    return-void
.end method

.method static synthetic lambda$messageTextCopied$22(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 324
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 326
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 327
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 329
    const-string v1, "ChatBubble - Copy Text"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 330
    return-void
.end method

.method static synthetic lambda$messageViewed$32(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 427
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 429
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 430
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 432
    const-string v1, "ChatBubble - Conversation View"

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;)V

    .line 433
    return-void
.end method

.method static synthetic lambda$muteConversationSelected$14(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 236
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 238
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 239
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 241
    const-string v1, "ChatBubble - Mute"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 242
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;Ljava/util/HashMap;)Ljava/lang/Void;
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
    .param p1, "args"    # Ljava/util/HashMap;

    .prologue
    .line 50
    const-string v4, "pagename"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "pageaction"

    .line 51
    invoke-virtual {p1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "pagetransitionorder"

    .line 52
    invoke-virtual {p1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 53
    const-string v4, "pagetransitionorder"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 54
    .local v3, "pagetransitionorder":Ljava/lang/String;
    const-string v4, "pagename"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 55
    .local v2, "pageName":Ljava/lang/String;
    const-string v4, "pageaction"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 57
    .local v1, "pageAction":Ljava/lang/String;
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 59
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    iget-object v4, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->currentTab:Ljava/lang/String;

    iput-object v4, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->previousTab:Ljava/lang/String;

    .line 60
    iput-object v2, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->currentTab:Ljava/lang/String;

    .line 62
    const-string v4, "02"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 63
    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 66
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->currentTab:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->previousTab:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 67
    iget-object v4, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->currentTab:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->previousTab:Ljava/lang/String;

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 71
    .end local v0    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .end local v1    # "pageAction":Ljava/lang/String;
    .end local v2    # "pageName":Ljava/lang/String;
    .end local v3    # "pagetransitionorder":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    return-object v4
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;Ljava/lang/String;)Ljava/lang/Void;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
    .param p1, "pageName"    # Ljava/lang/String;

    .prologue
    .line 75
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 77
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->currentTab:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->previousTab:Ljava/lang/String;

    .line 78
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->currentTab:Ljava/lang/String;

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->currentTab:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->previousTab:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->currentTab:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->previousTab:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 84
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method static synthetic lambda$renameGroupConvoSelected$18(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 280
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 282
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 283
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 285
    const-string v1, "ChatBubble - Rename Conversation"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 286
    return-void
.end method

.method static synthetic lambda$shareSelected$24(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 346
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 348
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 349
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 351
    const-string v1, "ChatBubble - Share"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 352
    return-void
.end method

.method static synthetic lambda$unmuteConversationSelected$15(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 247
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 249
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 250
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 252
    const-string v1, "ChatBubble - Unmute"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 253
    return-void
.end method

.method static synthetic lambda$viewPeopleSelected$16(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 258
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 260
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 261
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 263
    const-string v1, "ChatBubble - View People"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 264
    return-void
.end method

.method static synthetic lambda$viewProfileFromClubSelected$12(J)V
    .locals 4
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 214
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 216
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 217
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 219
    const-string v1, "ChatBubble - View People"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 220
    return-void
.end method

.method static synthetic lambda$viewProfileFromConvoSelected$11(Ljava/lang/String;)V
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 203
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 205
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 206
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "BubbleId"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 208
    const-string v1, "ChatBubble - View People"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 209
    return-void
.end method


# virtual methods
.method public addPeopleSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 268
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$18;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 276
    return-void
.end method

.method public clubChatOpenFromContextMenu(J)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 119
    invoke-static {p1, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$5;->lambdaFactory$(J)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 127
    return-void
.end method

.method public clubChatViewed(J)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 437
    invoke-static {p1, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$34;->lambdaFactory$(J)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 445
    return-void
.end method

.method public clubMessageDeleted(J)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 389
    invoke-static {p1, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$29;->lambdaFactory$(J)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 397
    return-void
.end method

.method public clubMessageReportedToAdmins(J)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 411
    invoke-static {p1, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$31;->lambdaFactory$(J)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 419
    return-void
.end method

.method public clubMessageReportedToXbox(J)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 400
    invoke-static {p1, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$30;->lambdaFactory$(J)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 408
    return-void
.end method

.method public clubMessageSent(J)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 378
    invoke-static {p1, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$28;->lambdaFactory$(J)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 386
    return-void
.end method

.method public clubNotificationTriggered(J)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 459
    invoke-static {p1, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$36;->lambdaFactory$(J)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 467
    return-void
.end method

.method public clubSettingChanged(JZ)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "newValue"    # Z

    .prologue
    .line 479
    invoke-static {p1, p2, p3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$38;->lambdaFactory$(JZ)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 488
    return-void
.end method

.method public clubSettingsSelected(J)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 367
    invoke-static {p1, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$27;->lambdaFactory$(J)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 375
    return-void
.end method

.method public collapsed(I)V
    .locals 1
    .param p1, "instanceCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 165
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$8;->lambdaFactory$(I)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 173
    return-void
.end method

.method public conversationOpenedFromContextMenu(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 97
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$3;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 105
    return-void
.end method

.method public conversationOpenedFromDropDown(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 108
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$4;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 116
    return-void
.end method

.method public createGroupConvoSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 224
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$14;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 232
    return-void
.end method

.method public expanded(I)V
    .locals 1
    .param p1, "instanceCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 154
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$7;->lambdaFactory$(I)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 162
    return-void
.end method

.method public inboxClubChatSelected(J)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 191
    invoke-static {p1, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$11;->lambdaFactory$(J)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 199
    return-void
.end method

.method public inboxConversationSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 180
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$10;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 188
    return-void
.end method

.method public inboxSettingsSelected()V
    .locals 1

    .prologue
    .line 176
    invoke-static {}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$9;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 177
    return-void
.end method

.method public inboxViewed()V
    .locals 1

    .prologue
    .line 422
    invoke-static {}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$32;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 423
    return-void
.end method

.method public instanceClosed(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .locals 1
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 130
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 151
    return-void
.end method

.method public leaveGroupConvoSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 290
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$20;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 298
    return-void
.end method

.method public messageDeleted(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 312
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$22;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 320
    return-void
.end method

.method public messageNotificationTriggered(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 448
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$35;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 456
    return-void
.end method

.method public messageOfTheDaySelected(J)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 356
    invoke-static {p1, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$26;->lambdaFactory$(J)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 364
    return-void
.end method

.method public messageReported(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 334
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$24;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 342
    return-void
.end method

.method public messageSent(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 301
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$21;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 309
    return-void
.end method

.method public messageSettingChanged(Z)V
    .locals 1
    .param p1, "newValue"    # Z

    .prologue
    .line 470
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$37;->lambdaFactory$(Z)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 476
    return-void
.end method

.method public messageTextCopied(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 323
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$23;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 331
    return-void
.end method

.method public messageViewed(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 426
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$33;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 434
    return-void
.end method

.method public muteConversationSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 235
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$15;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 243
    return-void
.end method

.method public renameGroupConvoSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 279
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$19;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 287
    return-void
.end method

.method public shareSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 345
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$25;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 353
    return-void
.end method

.method public unmuteConversationSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 246
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$16;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 254
    return-void
.end method

.method public viewPeopleSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 257
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$17;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 265
    return-void
.end method

.method public viewProfileFromClubSelected(J)V
    .locals 1
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 213
    invoke-static {p1, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$13;->lambdaFactory$(J)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 221
    return-void
.end method

.method public viewProfileFromConvoSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 202
    invoke-static {p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService$$Lambda$12;->lambdaFactory$(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 210
    return-void
.end method
