.class public interface abstract Lcom/microsoft/playready/IMeteringReporter;
.super Ljava/lang/Object;
.source "IMeteringReporter.java"


# virtual methods
.method public abstract getMeteringReportingPlugin()Lcom/microsoft/playready/IMeteringReportingPlugin;
.end method

.method public abstract reportMetering(Ljava/lang/String;)Lcom/microsoft/playready/IMeteringReportingTask;
.end method

.method public abstract reportMetering([B)Lcom/microsoft/playready/IMeteringReportingTask;
.end method

.method public abstract setMeteringReportingPlugin(Lcom/microsoft/playready/IMeteringReportingPlugin;)V
.end method
