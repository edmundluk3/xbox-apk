.class public Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "PeopleActivityFeedScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$BlockingIndicatorContainer;


# static fields
.field private static final REFRESH_TRIGGER_DISTANCE:I = 0xc8

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private currentVmState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private floatingHeader:Landroid/view/View;

.field private isUserDataReported:Z

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final listAdapter:Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final onRefreshListener:Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;

.field private postFeedButton:Landroid/view/View;

.field private final recyclerView:Landroid/support/v7/widget/RecyclerView;

.field private shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private statusPostEnabled:Z

.field private final swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V
    .locals 8
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    .prologue
    const/4 v7, 0x0

    .line 67
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 64
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->isUserDataReported:Z

    .line 258
    new-instance v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter$2;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->onRefreshListener:Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;

    .line 68
    const v4, 0x7f0e035a

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->screenBody:Landroid/view/View;

    .line 69
    const v4, 0x7f0e035b

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 70
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v4, :cond_0

    .line 71
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 74
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    const/high16 v6, 0x43480000    # 200.0f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->setDistanceToTriggerSync(I)V

    .line 77
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    .line 78
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setPositionSettingEnabled(Z)V

    .line 79
    const v4, 0x7f0e035d

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/RecyclerView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 80
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 82
    const/4 v1, 0x0

    .line 83
    .local v1, "floatingRowHelper":Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isPostButtonSticky()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 84
    const v4, 0x7f0e035c

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 85
    .local v3, "listContainer":Landroid/view/ViewGroup;
    if-eqz v3, :cond_1

    .line 86
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 87
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f0301ad

    invoke-virtual {v2, v4, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->floatingHeader:Landroid/view/View;

    .line 88
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->floatingHeader:Landroid/view/View;

    const v5, 0x7f0e08ac

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->postFeedButton:Landroid/view/View;

    .line 89
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->floatingHeader:Landroid/view/View;

    const v5, 0x7f0e08ad

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 90
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMePreferredColor()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setBackgroundColor(I)V

    .line 91
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->postFeedButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->updateFilterButton()V

    .line 96
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->floatingHeader:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 97
    new-instance v1, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

    .end local v1    # "floatingRowHelper":Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;
    new-instance v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;)V

    invoke-direct {v1, v4}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;-><init>(Lcom/microsoft/xbox/xle/ui/FloatingRowHelper$HeaderInfo;)V

    .line 115
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    .end local v3    # "listContainer":Landroid/view/ViewGroup;
    .restart local v1    # "floatingRowHelper":Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-direct {v0, v4, v5, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;)V

    .line 116
    .local v0, "adapter":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->getHeader()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->setHeader(Landroid/view/View;)V

    .line 117
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;

    .line 118
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 119
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v5, Lcom/microsoft/xbox/toolkit/ui/DividerItemDecoration;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v6

    const v7, 0x7f020163

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/ui/DividerItemDecoration;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 120
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isStatusPostEnabled()Z

    move-result v4

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->statusPostEnabled:Z

    .line 121
    return-void

    .line 71
    :array_0
    .array-data 4
        0x7f0c0108
        0x7f0c0109
        0x7f0c010a
        0x7f0c010b
    .end array-data
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->floatingHeader:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    return-object v0
.end method

.method private getAverageTimeFeedItems(Ljava/util/List;I)F
    .locals 10
    .param p2, "avgItemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;I)F"
        }
    .end annotation

    .prologue
    .line 317
    .local p1, "profileData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    const-wide/16 v4, 0x0

    .line 318
    .local v4, "runningTotal":J
    move v0, p2

    .line 320
    .local v0, "actualItemCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 321
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 323
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->date:Ljava/util/Date;

    if-eqz v3, :cond_0

    .line 325
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->date:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    add-long/2addr v4, v6

    .line 320
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 327
    :cond_0
    const-string v3, "HomeScreeViewAdapter"

    const-string v6, "Invalid element found in activity feed list"

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 332
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_1
    if-lez v0, :cond_2

    int-to-long v6, v0

    div-long v6, v4, v6

    long-to-float v3, v6

    :goto_2
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_2
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->launchStatusPost()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->launchUnsharedFeed()V

    return-void
.end method

.method static synthetic lambda$showWhatsNewIfNeeded$3()V
    .locals 3

    .prologue
    .line 271
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/WhatsNewDetailsScreen;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    return-void
.end method

.method static synthetic lambda$updateFilterButton$2(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->launchFilterSelection()V

    return-void
.end method

.method private showWhatsNewIfNeeded()V
    .locals 3

    .prologue
    .line 268
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    .line 269
    .local v0, "settings":Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionCode()I

    move-result v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getLastWhatNewDetailsVersion()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 270
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPostOOBEStatus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 271
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter$$Lambda$4;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 273
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setLastWhatsNewDetailsVersion(I)V

    .line 275
    :cond_1
    return-void
.end method

.method private trackUserInfoOnNewLaunch()V
    .locals 12

    .prologue
    const/4 v9, 0x1

    .line 279
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 280
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isNewLaunch()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 281
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v5

    .line 282
    .local v5, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getData()Ljava/util/List;

    move-result-object v7

    .line 283
    .local v7, "profileData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    if-eqz v5, :cond_1

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 285
    :try_start_0
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x5

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    invoke-direct {p0, v7, v10}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->getAverageTimeFeedItems(Ljava/util/List;I)F

    move-result v1

    .line 286
    .local v1, "averageTimeForLastFiveFeedItems":F
    new-instance v6, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 287
    .local v6, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v10, "Followers"

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfFollowers()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v6, v10, v11}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 288
    const-string v10, "Following"

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfFollowing()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v6, v10, v11}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 289
    const-string v10, "HasRealName"

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRealName()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    :goto_0
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v10, v9}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 290
    const-string v9, "GamerScore"

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerScore()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 291
    const-string v9, "SubLevel"

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAccountTier()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 292
    const-string v9, "ActivityFeedItems"

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 293
    const-string v9, "AFLast5Age"

    float-to-int v10, v1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 294
    const-string v10, "FacebookLinkedStatus"

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->isFacebookFriendFinderOptedIn()Z

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "OptIn"

    :goto_1
    invoke-virtual {v6, v10, v9}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296
    const-string v4, "UNKNOWN"

    .line 297
    .local v4, "linkedStatus":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getResult()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 298
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getResult()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v8

    .line 299
    .local v8, "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    if-eqz v8, :cond_0

    .line 300
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getPhoneAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v9

    sget-object v10, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v9, v10, :cond_4

    const-string v4, "OptIn"

    .line 304
    .end local v8    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :cond_0
    :goto_2
    const-string v9, "ContactsLinkedStatus"

    invoke-virtual {v6, v9, v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 305
    const-string v9, "User Info"

    invoke-static {v9, v6}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 307
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->isUserDataReported:Z

    .line 314
    .end local v1    # "averageTimeForLastFiveFeedItems":F
    .end local v4    # "linkedStatus":Ljava/lang/String;
    .end local v5    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v6    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .end local v7    # "profileData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    :cond_1
    :goto_3
    return-void

    .line 289
    .restart local v1    # "averageTimeForLastFiveFeedItems":F
    .restart local v5    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .restart local v6    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .restart local v7    # "profileData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    :cond_2
    const/4 v9, 0x0

    goto :goto_0

    .line 294
    :cond_3
    const-string v9, "OptOut"

    goto :goto_1

    .line 300
    .restart local v4    # "linkedStatus":Ljava/lang/String;
    .restart local v8    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :cond_4
    const-string v4, "OptOut"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 308
    .end local v1    # "averageTimeForLastFiveFeedItems":F
    .end local v4    # "linkedStatus":Ljava/lang/String;
    .end local v6    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .end local v8    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :catch_0
    move-exception v3

    .line 309
    .local v3, "ex":Ljava/lang/Exception;
    const-string v2, "trackUserInfoOnNewLaunch exception for bugs 8698041;9032437"

    .line 310
    .local v2, "error":Ljava/lang/String;
    const-string v9, "trackUserInfoOnNewLaunch exception for bugs 8698041;9032437"

    invoke-static {v9, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3
.end method

.method private updateFilterButton()V
    .locals 3

    .prologue
    .line 128
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->floatingHeader:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->floatingHeader:Landroid/view/View;

    const v2, 0x7f0e08ae

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 130
    .local v0, "filterButton":Landroid/widget/Button;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isFilterEnabled()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->filtersAreActive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMePreferredColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 136
    const v1, 0x7f070ff6

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 142
    .end local v0    # "filterButton":Landroid/widget/Button;
    :cond_0
    :goto_0
    return-void

    .line 138
    .restart local v0    # "filterButton":Landroid/widget/Button;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0142

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 139
    const v1, 0x7f070f34

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method public getHeader()Landroid/view/View;
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    return-object v0
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 146
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStart()V

    .line 147
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->onRefreshListener:Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 150
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getOnScrollListener()Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    move-result-object v0

    .line 151
    .local v0, "onScrollListener":Landroid/support/v7/widget/RecyclerView$OnScrollListener;
    if-eqz v0, :cond_1

    .line 152
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->addOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_2

    .line 158
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getRecycledViewPool()Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->clear()V

    .line 160
    :cond_2
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 164
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStop()V

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getOnScrollListener()Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->removeOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 169
    return-void
.end method

.method public updateBlockingIndicator()V
    .locals 2

    .prologue
    .line 244
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->TAG:Ljava/lang/String;

    const-string v1, "updating blocking indicator"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getIsDeletingFeedItem()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getIsHidingUnhidingFeedItem()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getIsPinningUnpinningFeedItem()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->setBlocking(ZLjava/lang/String;)V

    .line 246
    return-void

    .line 245
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateLoadingIndicator(Z)V
    .locals 2
    .param p1, "isLoading"    # Z

    .prologue
    .line 234
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-nez v0, :cond_0

    .line 235
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->updateLoadingIndicator(Z)V

    .line 240
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 238
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public updateViewOverride()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 174
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isBusy()Z

    move-result v5

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->updateLoadingIndicator(Z)V

    .line 176
    const/4 v4, 0x0

    .line 178
    .local v4, "notifyAdapter":Z
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getData()Ljava/util/List;

    move-result-object v1

    .line 179
    .local v1, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->items:Ljava/util/List;

    if-ne v5, v1, :cond_0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->drainFeedDataChangedInternally()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 180
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;->clear()V

    .line 181
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;->addAll(Ljava/util/Collection;)V

    .line 182
    const/4 v4, 0x1

    .line 184
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->items:Ljava/util/List;

    .line 187
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getLastSelectedPosition()Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;

    move-result-object v0

    .line 188
    .local v0, "listPosition":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;
    if-eqz v0, :cond_2

    .line 189
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget v6, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;->position:I

    iget v7, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;->offset:I

    invoke-static {v5, v6, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPositionWithOffset(Landroid/support/v7/widget/RecyclerView;II)V

    .line 192
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->shouldUpdateLikeControl()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 193
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->resetLikeControlUpdate()V

    .line 194
    const/4 v4, 0x1

    .line 197
    :cond_3
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    .line 198
    .local v3, "newVmState":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->currentVmState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v5, v3, :cond_4

    .line 199
    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->currentVmState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 200
    const/4 v4, 0x1

    .line 203
    :cond_4
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isStatusPostEnabled()Z

    move-result v2

    .line 204
    .local v2, "newStatusPostEnabled":Z
    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->statusPostEnabled:Z

    if-eq v2, v5, :cond_5

    .line 205
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->statusPostEnabled:Z

    .line 206
    const/4 v4, 0x1

    .line 209
    :cond_5
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getUpdateSocialRecommendations()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 210
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setUpdateSocialRecommendations(Z)V

    .line 211
    const/4 v4, 0x1

    .line 214
    :cond_6
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getShouldUpdateHiddenState()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 215
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setShouldUpdateHiddenState(Z)V

    .line 216
    const/4 v4, 0x1

    .line 219
    :cond_7
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;

    if-eqz v5, :cond_8

    if-eqz v4, :cond_8

    .line 220
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;->notifyDataSetChanged()V

    .line 223
    :cond_8
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->showWhatsNewIfNeeded()V

    .line 225
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->updateFilterButton()V

    .line 227
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    instance-of v5, v5, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModel;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isBusy()Z

    move-result v5

    if-nez v5, :cond_9

    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->isUserDataReported:Z

    if-nez v5, :cond_9

    .line 228
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->trackUserInfoOnNewLaunch()V

    .line 230
    :cond_9
    return-void
.end method
