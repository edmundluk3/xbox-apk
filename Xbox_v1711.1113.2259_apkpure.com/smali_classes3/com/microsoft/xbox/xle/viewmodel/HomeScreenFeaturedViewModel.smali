.class public Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "HomeScreenFeaturedViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithPosition;


# instance fields
.field private adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

.field private final containerResourceId:I

.field private dataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private position:I

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(II)V
    .locals 2
    .param p1, "containerResourceId"    # I
    .param p2, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 26
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    invoke-direct {v0, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;-><init>(II)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    .line 29
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->containerResourceId:I

    .line 30
    iput p2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->position:I

    .line 31
    return-void
.end method

.method private createScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;)V

    return-object v0
.end method

.method private onLoadFeaturedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 141
    const-string v0, "HomeScreenFeatured"

    const-string v1, "onLoad featured completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getFeaturedItems()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->dataList:Ljava/util/List;

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->dataList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->dataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 146
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 150
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->updateAdapter()V

    .line 151
    return-void

    .line 148
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getAdpViewScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    return-object v0
.end method

.method public getContainerResourceId()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->containerResourceId:I

    return v0
.end method

.method public getDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->dataList:Ljava/util/List;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->position:I

    return v0
.end method

.method public getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 0

    .prologue
    .line 98
    return-object p0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public gotoDetails(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 120
    const/16 v0, 0x14

    .line 121
    .local v0, "featuredStoreIndex":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->dataList:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->dataList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, p1, :cond_0

    .line 122
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->dataList:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 123
    .local v2, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v3

    .line 125
    .local v3, "pageUri":Ljava/lang/String;
    new-instance v4, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 126
    .local v4, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;-><init>()V

    .line 127
    .local v1, "hm":Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;
    const/16 v5, 0x14

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->putStoreFilterPosition(I)V

    .line 130
    const-string v5, "Store"

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->putPurchaseOriginatingSource(Ljava/lang/String;)V

    .line 131
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 136
    invoke-virtual {p0, v2, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 138
    .end local v1    # "hm":Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;
    .end local v2    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v3    # "pageUri":Ljava/lang/String;
    .end local v4    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 88
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->loadDiscoverList(Z)V

    .line 90
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->createScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 58
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->createScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 46
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getFeaturedItems()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getFeaturedItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 47
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 52
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 53
    return-void

    .line 49
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 63
    return-void
.end method

.method public setAdpVewScrollState(Lcom/microsoft/xbox/toolkit/ui/ScrollState;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    .line 39
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->position:I

    .line 109
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    .line 68
    .local v0, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->DiscoverData:Lcom/microsoft/xbox/service/model/UpdateType;

    if-eq v0, v1, :cond_1

    .line 69
    const-string v1, "HomeScreenFeatured"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown update type ignore "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->onLoadFeaturedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0
.end method
