.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;
.source "$AutoValue_ClubHubDataTypes_Deeplink.java"


# instance fields
.field private final Uri:Ljava/lang/String;

.field private final titleId:J


# direct methods
.method constructor <init>(JLjava/lang/String;)V
    .locals 1
    .param p1, "titleId"    # J
    .param p3, "Uri"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->titleId:J

    .line 17
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->Uri:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public Uri()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->Uri:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p1, p0, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v1

    .line 44
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 45
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;

    .line 46
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->titleId:J

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;->titleId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->Uri:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 47
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;->Uri()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->Uri:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;->Uri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;
    :cond_4
    move v1, v2

    .line 49
    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const v8, 0xf4243

    .line 54
    const/4 v0, 0x1

    .line 55
    .local v0, "h":I
    mul-int/2addr v0, v8

    .line 56
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->titleId:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->titleId:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 57
    mul-int/2addr v0, v8

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->Uri:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 59
    return v0

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->Uri:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public titleId()J
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->titleId:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Deeplink{titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->titleId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Deeplink;->Uri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
