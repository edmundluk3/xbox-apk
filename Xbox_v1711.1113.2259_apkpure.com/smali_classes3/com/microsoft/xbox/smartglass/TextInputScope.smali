.class public final enum Lcom/microsoft/xbox/smartglass/TextInputScope;
.super Ljava/lang/Enum;
.source "TextInputScope.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/TextInputScope;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum AddressCity:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum AddressCountryName:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum AddressCountryShortName:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum AddressStateOrProvince:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum AddressStreet:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum AlphanumericFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum AlphanumericHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Bopomofo:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum ChineseFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum ChineseHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum CurrencyAmount:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum CurrencyAmountAndSymbol:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum CurrencyChinese:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Date:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum DateDay:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum DateDayName:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum DateMonth:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum DateMonthName:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum DateYear:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Default:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Digits:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum EmailSmtpAddress:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum EmailUserName:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum EnumString:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum FileName:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum FullFilePath:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum HangulFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum HangulHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Hanja:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Hiragana:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum KatakanaFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum KatakanaHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum LogOnName:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum NativeScript:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Number:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum NumberFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum OneChar:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Password:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum PersonalFullName:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum PersonalGivenName:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum PersonalMiddleName:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum PersonalNamePrefix:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum PersonalNameSuffix:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum PersonalSurname:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum PhraseList:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum PostalAddress:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum PostalCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum RegularExpression:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Search:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum SearchIncremental:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum SearchTitleText:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Srgs:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum TelephoneAreaCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum TelephoneCountryCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum TelephoneLocalNumber:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum TelephoneNumber:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Time:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum TimeHour:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum TimeMinorSec:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Url:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public static final enum Xml:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/TextInputScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Default"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Default:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Url"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Url:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "FullFilePath"

    invoke-direct {v2, v3, v7, v7}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->FullFilePath:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "FileName"

    invoke-direct {v2, v3, v8, v8}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->FileName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "EmailUserName"

    invoke-direct {v2, v3, v9, v9}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->EmailUserName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 22
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "EmailSmtpAddress"

    const/4 v4, 0x5

    const/4 v5, 0x5

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->EmailSmtpAddress:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 24
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "LogOnName"

    const/4 v4, 0x6

    const/4 v5, 0x6

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->LogOnName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 26
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "PersonalFullName"

    const/4 v4, 0x7

    const/4 v5, 0x7

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalFullName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 28
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "PersonalNamePrefix"

    const/16 v4, 0x8

    const/16 v5, 0x8

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalNamePrefix:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 30
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "PersonalGivenName"

    const/16 v4, 0x9

    const/16 v5, 0x9

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalGivenName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 32
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "PersonalMiddleName"

    const/16 v4, 0xa

    const/16 v5, 0xa

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalMiddleName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 34
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "PersonalSurname"

    const/16 v4, 0xb

    const/16 v5, 0xb

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalSurname:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 36
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "PersonalNameSuffix"

    const/16 v4, 0xc

    const/16 v5, 0xc

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalNameSuffix:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 38
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "PostalAddress"

    const/16 v4, 0xd

    const/16 v5, 0xd

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->PostalAddress:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 40
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "PostalCode"

    const/16 v4, 0xe

    const/16 v5, 0xe

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->PostalCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 42
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "AddressStreet"

    const/16 v4, 0xf

    const/16 v5, 0xf

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressStreet:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 44
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "AddressStateOrProvince"

    const/16 v4, 0x10

    const/16 v5, 0x10

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressStateOrProvince:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 46
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "AddressCity"

    const/16 v4, 0x11

    const/16 v5, 0x11

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressCity:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 48
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "AddressCountryName"

    const/16 v4, 0x12

    const/16 v5, 0x12

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressCountryName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 50
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "AddressCountryShortName"

    const/16 v4, 0x13

    const/16 v5, 0x13

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressCountryShortName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 52
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "CurrencyAmountAndSymbol"

    const/16 v4, 0x14

    const/16 v5, 0x14

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->CurrencyAmountAndSymbol:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 54
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "CurrencyAmount"

    const/16 v4, 0x15

    const/16 v5, 0x15

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->CurrencyAmount:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 56
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Date"

    const/16 v4, 0x16

    const/16 v5, 0x16

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Date:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 58
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "DateMonth"

    const/16 v4, 0x17

    const/16 v5, 0x17

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateMonth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 60
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "DateDay"

    const/16 v4, 0x18

    const/16 v5, 0x18

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateDay:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 62
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "DateYear"

    const/16 v4, 0x19

    const/16 v5, 0x19

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateYear:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 64
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "DateMonthName"

    const/16 v4, 0x1a

    const/16 v5, 0x1a

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateMonthName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 66
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "DateDayName"

    const/16 v4, 0x1b

    const/16 v5, 0x1b

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateDayName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 68
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Digits"

    const/16 v4, 0x1c

    const/16 v5, 0x1c

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Digits:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 70
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Number"

    const/16 v4, 0x1d

    const/16 v5, 0x1d

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Number:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 72
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "OneChar"

    const/16 v4, 0x1e

    const/16 v5, 0x1e

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->OneChar:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 74
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Password"

    const/16 v4, 0x1f

    const/16 v5, 0x1f

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Password:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 76
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "TelephoneNumber"

    const/16 v4, 0x20

    const/16 v5, 0x20

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneNumber:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 78
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "TelephoneCountryCode"

    const/16 v4, 0x21

    const/16 v5, 0x21

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneCountryCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 80
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "TelephoneAreaCode"

    const/16 v4, 0x22

    const/16 v5, 0x22

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneAreaCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 82
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "TelephoneLocalNumber"

    const/16 v4, 0x23

    const/16 v5, 0x23

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneLocalNumber:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 84
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Time"

    const/16 v4, 0x24

    const/16 v5, 0x24

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Time:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 86
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "TimeHour"

    const/16 v4, 0x25

    const/16 v5, 0x25

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->TimeHour:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 88
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "TimeMinorSec"

    const/16 v4, 0x26

    const/16 v5, 0x26

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->TimeMinorSec:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 90
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "NumberFullWidth"

    const/16 v4, 0x27

    const/16 v5, 0x27

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->NumberFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 92
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "AlphanumericHalfWidth"

    const/16 v4, 0x28

    const/16 v5, 0x28

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->AlphanumericHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 94
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "AlphanumericFullWidth"

    const/16 v4, 0x29

    const/16 v5, 0x29

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->AlphanumericFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 96
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "CurrencyChinese"

    const/16 v4, 0x2a

    const/16 v5, 0x2a

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->CurrencyChinese:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 98
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Bopomofo"

    const/16 v4, 0x2b

    const/16 v5, 0x2b

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Bopomofo:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 100
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Hiragana"

    const/16 v4, 0x2c

    const/16 v5, 0x2c

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Hiragana:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 102
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "KatakanaHalfWidth"

    const/16 v4, 0x2d

    const/16 v5, 0x2d

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->KatakanaHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 104
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "KatakanaFullWidth"

    const/16 v4, 0x2e

    const/16 v5, 0x2e

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->KatakanaFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 106
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Hanja"

    const/16 v4, 0x2f

    const/16 v5, 0x2f

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Hanja:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 108
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "HangulHalfWidth"

    const/16 v4, 0x30

    const/16 v5, 0x30

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->HangulHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 110
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "HangulFullWidth"

    const/16 v4, 0x31

    const/16 v5, 0x31

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->HangulFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 112
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Search"

    const/16 v4, 0x32

    const/16 v5, 0x32

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Search:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 114
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "SearchTitleText"

    const/16 v4, 0x33

    const/16 v5, 0x33

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->SearchTitleText:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 116
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "SearchIncremental"

    const/16 v4, 0x34

    const/16 v5, 0x34

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->SearchIncremental:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 118
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "ChineseHalfWidth"

    const/16 v4, 0x35

    const/16 v5, 0x35

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->ChineseHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 120
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "ChineseFullWidth"

    const/16 v4, 0x36

    const/16 v5, 0x36

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->ChineseFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 122
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "NativeScript"

    const/16 v4, 0x37

    const/16 v5, 0x37

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->NativeScript:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 124
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "PhraseList"

    const/16 v4, 0x38

    const/4 v5, -0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->PhraseList:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 126
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "RegularExpression"

    const/16 v4, 0x39

    const/4 v5, -0x2

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->RegularExpression:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 128
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Srgs"

    const/16 v4, 0x3a

    const/4 v5, -0x3

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Srgs:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 130
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "Xml"

    const/16 v4, 0x3b

    const/4 v5, -0x4

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->Xml:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 132
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextInputScope;

    const-string v3, "EnumString"

    const/16 v4, 0x3c

    const/4 v5, -0x5

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextInputScope;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->EnumString:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 10
    const/16 v2, 0x3d

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/TextInputScope;

    sget-object v3, Lcom/microsoft/xbox/smartglass/TextInputScope;->Default:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/TextInputScope;->Url:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/smartglass/TextInputScope;->FullFilePath:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v3, v2, v7

    sget-object v3, Lcom/microsoft/xbox/smartglass/TextInputScope;->FileName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/smartglass/TextInputScope;->EmailUserName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->EmailSmtpAddress:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->LogOnName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalFullName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalNamePrefix:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalGivenName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalMiddleName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalSurname:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalNameSuffix:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0xd

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->PostalAddress:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0xe

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->PostalCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0xf

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressStreet:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x10

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressStateOrProvince:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x11

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressCity:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x12

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressCountryName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x13

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressCountryShortName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x14

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->CurrencyAmountAndSymbol:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x15

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->CurrencyAmount:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x16

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->Date:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x17

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateMonth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x18

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateDay:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x19

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateYear:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateMonthName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateDayName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->Digits:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->Number:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->OneChar:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->Password:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x20

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneNumber:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x21

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneCountryCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x22

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneAreaCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x23

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneLocalNumber:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x24

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->Time:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x25

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->TimeHour:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x26

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->TimeMinorSec:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x27

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->NumberFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x28

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->AlphanumericHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x29

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->AlphanumericFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x2a

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->CurrencyChinese:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x2b

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->Bopomofo:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x2c

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->Hiragana:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x2d

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->KatakanaHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x2e

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->KatakanaFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x2f

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->Hanja:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x30

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->HangulHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x31

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->HangulFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x32

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->Search:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x33

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->SearchTitleText:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x34

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->SearchIncremental:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x35

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->ChineseHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x36

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->ChineseFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x37

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->NativeScript:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x38

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->PhraseList:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x39

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->RegularExpression:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x3a

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->Srgs:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x3b

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->Xml:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    const/16 v3, 0x3c

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->EnumString:Lcom/microsoft/xbox/smartglass/TextInputScope;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->$VALUES:[Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 135
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextInputScope;->_lookup:Landroid/util/SparseArray;

    .line 138
    invoke-static {}, Lcom/microsoft/xbox/smartglass/TextInputScope;->values()[Lcom/microsoft/xbox/smartglass/TextInputScope;

    move-result-object v2

    array-length v3, v2

    .local v0, "scope":Lcom/microsoft/xbox/smartglass/TextInputScope;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 139
    sget-object v4, Lcom/microsoft/xbox/smartglass/TextInputScope;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/TextInputScope;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 141
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 144
    iput p3, p0, Lcom/microsoft/xbox/smartglass/TextInputScope;->_value:I

    .line 145
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/smartglass/TextInputScope;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 161
    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 162
    .local v0, "scope":Lcom/microsoft/xbox/smartglass/TextInputScope;
    if-nez v0, :cond_0

    .line 163
    sget-object v0, Lcom/microsoft/xbox/smartglass/TextInputScope;->Default:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 166
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/TextInputScope;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/TextInputScope;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/TextInputScope;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/smartglass/TextInputScope;->$VALUES:[Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/TextInputScope;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/TextInputScope;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/microsoft/xbox/smartglass/TextInputScope;->_value:I

    return v0
.end method
