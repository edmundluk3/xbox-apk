.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;
.super Ljava/lang/Object;
.source "GameProfileFriendsWhoPlayScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GameProfileSpotlightDataListItem"
.end annotation


# instance fields
.field public final broadcastProvider:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final broadcasterImageUrl:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final broadcasterName:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "broadcasterName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "broadcasterImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "broadcastProvider"    # Ljava/lang/String;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcasterName:Ljava/lang/String;

    .line 203
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcasterImageUrl:Ljava/lang/String;

    .line 204
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcastProvider:Ljava/lang/String;

    .line 205
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 209
    if-ne p1, p0, :cond_1

    .line 215
    :cond_0
    :goto_0
    return v1

    .line 211
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 212
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 214
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;

    .line 215
    .local v0, "other":Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcasterName:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcasterName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcasterImageUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcasterImageUrl:Ljava/lang/String;

    .line 216
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcastProvider:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcastProvider:Ljava/lang/String;

    .line 217
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 223
    const/16 v0, 0x11

    .line 224
    .local v0, "result":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcasterName:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 225
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcasterImageUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v2

    add-int v0, v1, v2

    .line 226
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcastProvider:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v2

    add-int v0, v1, v2

    .line 227
    return v0
.end method
