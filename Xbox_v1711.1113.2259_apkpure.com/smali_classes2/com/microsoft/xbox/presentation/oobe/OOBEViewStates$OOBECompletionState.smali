.class public final enum Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;
.super Ljava/lang/Enum;
.source "OOBEViewStates.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OOBECompletionState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;",
        ">;",
        "Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 127
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    .line 126
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    sget-object v1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;->$VALUES:[Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 126
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 126
    const-class v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;->$VALUES:[Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    return-object v0
.end method


# virtual methods
.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    const-string v0, "OOBECompletionState"

    return-object v0
.end method
