.class public abstract Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "AdapterBaseNormal.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "WrongViewCast"
    }
.end annotation


# static fields
.field protected static final AVATAR_VIEW_ANIMATION_NAME:Ljava/lang/String; = "AvatarView"

.field protected static final CONTENT_ANIMATION_NAME:Ljava/lang/String; = "Content"

.field protected static final SCREEN_BODY_ANIMATION_NAME:Ljava/lang/String; = "Screen"

.field protected static final TITLE_BAR_ANIMATION_NAME:Ljava/lang/String; = "TitleBar"


# instance fields
.field protected content:Landroid/view/View;

.field protected screenBody:Landroid/view/View;

.field protected titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 35
    return-void
.end method

.method protected constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 28
    const v0, 0x7f0e0ac4

    .line 29
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/TitleBarView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    .line 39
    return-void
.end method


# virtual methods
.method protected getTitleBarAnimation(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    .locals 1
    .param p1, "animationType"    # Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;
    .param p2, "goingBack"    # Z

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method public onSetInactive()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onSetInactive()V

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/TitleBarView;->updateIsLoading(Z)V

    .line 59
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 64
    const v0, 0x7f0e0ac4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/TitleBarView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    .line 66
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v0

    const-string v1, "Screen"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    .line 67
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v0

    const-string v1, "TitleBar"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    .line 68
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v0

    const-string v1, "Content"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    .line 69
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onStart()V

    .line 70
    return-void
.end method

.method protected updateLoadingIndicator(Z)V
    .locals 1
    .param p1, "isLoading"    # Z

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->isActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/TitleBarView;->updateIsLoading(Z)V

    .line 50
    :cond_0
    return-void
.end method
