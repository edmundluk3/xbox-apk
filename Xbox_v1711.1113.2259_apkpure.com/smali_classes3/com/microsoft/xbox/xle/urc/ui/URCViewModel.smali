.class public Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;
.super Ljava/lang/Object;
.source "URCViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    }
.end annotation


# static fields
.field public static final GROUP_ACTION:I = 0x0

.field public static final GROUP_COLOR:I = 0x2

.field public static final GROUP_CUSTOM:I = 0x6

.field public static final GROUP_CUSTOM_FULL:I = 0x7

.field public static final GROUP_NAVIGATION:I = 0x1

.field public static final GROUP_NUMPAD:I = 0x5

.field public static final GROUP_PLAYBACK:I = 0x3

.field public static final GROUP_UNKNOWN:I = -0x1

.field public static final GROUP_VOLUME:I = 0x4

.field public static final TAG:Ljava/lang/String; = "URCViewModel"

.field private static final groupActionInit:[Ljava/lang/Object;

.field private static final groupColorInit:[Ljava/lang/Object;

.field private static final groupNavVolumeInit:[Ljava/lang/String;

.field private static final groupNumpadInit:[Ljava/lang/Object;

.field private static final groupPlaybackInit:[Ljava/lang/Object;

.field private static instance:Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

.field private static settingsFileName:Ljava/lang/String;


# instance fields
.field private alternativeButtons:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private alwaysAvaliable:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private doNotUseLabel:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public groupAction:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field public groupColor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field public groupMoreFull:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field public groupMoreSmall:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field public groupNumpad:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field public groupPlayback:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private lastUsedButtons:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNarratorContentMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mStringMap:Landroid/util/SparseIntArray;

.field private final views:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->instance:Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    .line 48
    const-string v0, "xleurcsettings"

    sput-object v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->settingsFileName:Ljava/lang/String;

    .line 191
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "btn.menu"

    aput-object v1, v0, v3

    sget v1, Lcom/microsoft/xbox/xle/urc/R$string;->btn_menu:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "btn.guide"

    aput-object v1, v0, v5

    sget v1, Lcom/microsoft/xbox/xle/urc/R$string;->btn_guide:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "btn.dvr"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_dvr:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupActionInit:[Ljava/lang/Object;

    .line 193
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "btn.yellow"

    aput-object v1, v0, v3

    sget v1, Lcom/microsoft/xbox/xle/urc/R$drawable;->dot_yellow:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "btn.blue"

    aput-object v1, v0, v5

    sget v1, Lcom/microsoft/xbox/xle/urc/R$drawable;->dot_blue:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "btn.red"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget v2, Lcom/microsoft/xbox/xle/urc/R$drawable;->dot_red:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "btn.green"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lcom/microsoft/xbox/xle/urc/R$drawable;->dot_green:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupColorInit:[Ljava/lang/Object;

    .line 195
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "btn.rewind"

    aput-object v1, v0, v3

    sget v1, Lcom/microsoft/xbox/xle/urc/R$drawable;->ico_rwd:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "btn.pause"

    aput-object v1, v0, v5

    sget v1, Lcom/microsoft/xbox/xle/urc/R$drawable;->ico_pause:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "btn.fast_fwd"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget v2, Lcom/microsoft/xbox/xle/urc/R$drawable;->ico_fwd:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "btn.stop"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lcom/microsoft/xbox/xle/urc/R$drawable;->ico_stop:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "btn.play"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget v2, Lcom/microsoft/xbox/xle/urc/R$drawable;->ico_play:I

    .line 196
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "btn.record"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget v2, Lcom/microsoft/xbox/xle/urc/R$drawable;->ico_record:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "btn.replay"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget v2, Lcom/microsoft/xbox/xle/urc/R$drawable;->ico_replay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "btn.live"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "btn.skip_fwd"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget v2, Lcom/microsoft/xbox/xle/urc/R$drawable;->ico_next:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlaybackInit:[Ljava/lang/Object;

    .line 198
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "btn.digit_1"

    aput-object v1, v0, v3

    sget v1, Lcom/microsoft/xbox/xle/urc/R$string;->btn_1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "btn.digit_2"

    aput-object v1, v0, v5

    sget v1, Lcom/microsoft/xbox/xle/urc/R$string;->btn_2:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "btn.digit_3"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_3:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "btn.digit_4"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_4:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "btn.digit_5"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_5:I

    .line 199
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "btn.digit_6"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_6:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "btn.digit_7"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_7:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "btn.digit_8"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_8:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "btn.digit_9"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_9:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "btn.delimiter"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_delimiter:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "btn.digit_0"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_0:I

    .line 200
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "btn.ch_enter"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_ch_enter:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNumpadInit:[Ljava/lang/Object;

    .line 202
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "btn.vol_up"

    aput-object v1, v0, v3

    const-string v1, "btn.vol_down"

    aput-object v1, v0, v4

    const-string v1, "btn.vol_mute"

    aput-object v1, v0, v5

    const-string v1, "btn.last"

    aput-object v1, v0, v6

    const-string v1, "btn.ch_down"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "btn.ch_up"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "btn.up"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "btn.down"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "btn.left"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "btn.right"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "btn.select"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "btn.page_up"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "btn.page_down"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "btn.info"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "btn.exit"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "btn.power"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "btn.power_on"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "btn.power_off"

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNavVolumeInit:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    .line 51
    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alwaysAvaliable:Ljava/util/Set;

    .line 52
    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->doNotUseLabel:Ljava/util/Set;

    .line 68
    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupColor:Ljava/util/ArrayList;

    .line 69
    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNumpad:Ljava/util/ArrayList;

    .line 70
    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupAction:Ljava/util/ArrayList;

    .line 71
    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlayback:Ljava/util/ArrayList;

    .line 72
    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreFull:Ljava/util/ArrayList;

    .line 73
    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    .line 75
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->views:Ljava/util/ArrayList;

    .line 79
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mStringMap:Landroid/util/SparseIntArray;

    .line 81
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    .line 95
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->context:Landroid/content/Context;

    .line 97
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    const-string v2, "btn.yellow"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "btn.func_a"

    aput-object v4, v3, v5

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    const-string v2, "btn.blue"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "btn.func_b"

    aput-object v4, v3, v5

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    const-string v2, "btn.red"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "btn.func_c"

    aput-object v4, v3, v5

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    const-string v2, "btn.green"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "btn.func_d"

    aput-object v4, v3, v5

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    const-string v2, "btn.menu"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "btn.home"

    aput-object v4, v3, v5

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    const-string v2, "btn.exit"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "btn.back"

    aput-object v4, v3, v5

    const-string v4, "btn.live"

    aput-object v4, v3, v6

    const-string v4, "btn.last"

    aput-object v4, v3, v7

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    const-string v2, "btn.last"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "btn.back"

    aput-object v4, v3, v5

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    const-string v2, "btn.live"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "btn.exit"

    aput-object v4, v3, v5

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    const-string v2, "btn.delimiter"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "btn.plus_100"

    aput-object v4, v3, v5

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    const-string v2, "btn.ch_enter"

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "btn.format"

    aput-object v4, v3, v5

    const-string v4, "btn.last"

    aput-object v4, v3, v6

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alwaysAvaliable:Ljava/util/Set;

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alwaysAvaliable:Ljava/util/Set;

    const-string v2, "btn.input"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 113
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->doNotUseLabel:Ljava/util/Set;

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->doNotUseLabel:Ljava/util/Set;

    const-string v2, "btn.vol_up"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->doNotUseLabel:Ljava/util/Set;

    const-string v2, "btn.vol_down"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->doNotUseLabel:Ljava/util/Set;

    const-string v2, "btn.ch_down"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->doNotUseLabel:Ljava/util/Set;

    const-string v2, "btn.ch_up"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->doNotUseLabel:Ljava/util/Set;

    const-string v2, "btn.page_down"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->doNotUseLabel:Ljava/util/Set;

    const-string v2, "btn.page_up"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mStringMap:Landroid/util/SparseIntArray;

    sget v2, Lcom/microsoft/xbox/xle/urc/R$id;->text_select:I

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->btn_select:I

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 122
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mStringMap:Landroid/util/SparseIntArray;

    sget v2, Lcom/microsoft/xbox/xle/urc/R$id;->text_page:I

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->btn_page:I

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 123
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mStringMap:Landroid/util/SparseIntArray;

    sget v2, Lcom/microsoft/xbox/xle/urc/R$id;->text_info:I

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->btn_info:I

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 124
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mStringMap:Landroid/util/SparseIntArray;

    sget v2, Lcom/microsoft/xbox/xle/urc/R$id;->text_exit:I

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->btn_exit:I

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 125
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mStringMap:Landroid/util/SparseIntArray;

    sget v2, Lcom/microsoft/xbox/xle/urc/R$id;->text_vol:I

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->btn_vol:I

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 126
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mStringMap:Landroid/util/SparseIntArray;

    sget v2, Lcom/microsoft/xbox/xle/urc/R$id;->text_ch:I

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->btn_ch:I

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 129
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.input"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_input:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.page_up"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_page_up:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.up"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_arrow_up:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.left"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_arrow_left:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.right"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_arrow_right:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.page_down"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_page_down:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.down"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_arrow_down:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.vol_up"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_volume_up:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.vol_mute"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_mute:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.ch_up"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_channel_up:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.vol_down"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_volume_down:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.last"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_previous_channel:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.ch_down"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_channel_down:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.yellow"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_yellow:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.blue"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_blue:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.red"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_red:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.green"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_green:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.rewind"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_rewind:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.pause"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_pause:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.fast_fwd"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_fast_forward:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.stop"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_stop:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.play"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_play:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.record"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_record:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.replay"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_back:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    const-string v2, "btn.skip_fwd"

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_skip_forward:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->loadLastUsedButtons()V

    .line 157
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 158
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 159
    new-instance v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$1;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->generateCollections()V

    .line 189
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->notifyViews()V

    return-void
.end method

.method private fillGroup(Ljava/util/ArrayList;Ljava/util/Map;[Ljava/lang/Object;Ljava/util/Set;ZZ)V
    .locals 6
    .param p3, "data"    # [Ljava/lang/Object;
    .param p5, "labels"    # Z
    .param p6, "alwaysKeep"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/Object;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 332
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;>;"
    .local p2, "buttons":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p4, "usedButtons":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, p3

    if-ge v1, v5, :cond_3

    .line 333
    aget-object v2, p3, v1

    check-cast v2, Ljava/lang/String;

    .line 334
    .local v2, "id":Ljava/lang/String;
    add-int/lit8 v5, v1, 0x1

    aget-object v5, p3, v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 336
    .local v4, "src":I
    new-instance v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;-><init>()V

    .line 338
    .local v0, "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    invoke-virtual {p0, v2, p2}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getRealID(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v3

    .line 339
    .local v3, "realID":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 340
    if-nez p6, :cond_1

    .line 332
    :goto_1
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 344
    :cond_0
    invoke-interface {p4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 346
    iput-object v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    .line 347
    if-eqz p5, :cond_2

    .line 349
    iput v4, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->label:I

    .line 355
    :cond_1
    :goto_2
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 350
    :cond_2
    if-eqz v4, :cond_1

    .line 351
    iput v4, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->image:I

    goto :goto_2

    .line 357
    .end local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "realID":Ljava/lang/String;
    .end local v4    # "src":I
    :cond_3
    return-void
.end method

.method private fixGroup(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;>;"
    const/4 v1, 0x0

    .line 319
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 329
    :goto_0
    return-void

    .line 323
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 326
    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    const-class v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->instance:Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->instance:Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    .line 44
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->instance:Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private loadLastUsedButtons()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 450
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    .line 452
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->settingsFileName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 453
    .local v0, "prefs":Landroid/content/SharedPreferences;
    if-nez v0, :cond_1

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 457
    :cond_1
    const-string v2, "lastUsed1"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 458
    .local v1, "tmp":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 459
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    :cond_2
    const-string v2, "lastUsed2"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 462
    if-eqz v1, :cond_3

    .line 463
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465
    :cond_3
    const-string v2, "lastUsed3"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 466
    if-eqz v1, :cond_0

    .line 467
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private notifyViews()V
    .locals 3

    .prologue
    .line 491
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->views:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;

    .line 492
    .local v0, "view":Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->updateButtons()V

    .line 493
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->requestLayout()V

    goto :goto_0

    .line 495
    .end local v0    # "view":Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;
    :cond_0
    return-void
.end method

.method private saveLastUsedButtons()V
    .locals 7

    .prologue
    .line 472
    iget-object v4, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    if-nez v4, :cond_0

    .line 488
    :goto_0
    return-void

    .line 475
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->settingsFileName:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 476
    .local v3, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 478
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v4, 0x3

    if-ge v1, v4, :cond_2

    .line 479
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "lastUsed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v1, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 480
    .local v2, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v1, :cond_1

    .line 481
    iget-object v4, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 478
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 483
    :cond_1
    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    .line 487
    .end local v2    # "key":Ljava/lang/String;
    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method


# virtual methods
.method public addView(Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;

    .prologue
    .line 498
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 499
    return-void
.end method

.method public generateCollections()V
    .locals 21

    .prologue
    .line 206
    const/4 v4, 0x0

    .line 208
    .local v4, "buttons":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v18

    .line 209
    .local v18, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v18, :cond_4

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getDevices()[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    move-result-object v11

    .line 210
    .local v11, "devices":[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    :goto_0
    if-eqz v11, :cond_0

    .line 211
    array-length v3, v11

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v10, v11, v2

    .line 212
    .local v10, "d":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    if-eqz v10, :cond_5

    iget-object v5, v10, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    if-eqz v5, :cond_5

    iget-object v5, v10, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    const-string v7, "stb"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 213
    const-string v2, "URCViewModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Buttons received from stb: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v10, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v4, v10, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    .line 220
    .end local v10    # "d":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    :cond_0
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 223
    .local v6, "usedButtons":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupAction:Ljava/util/ArrayList;

    .line 224
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupAction:Ljava/util/ArrayList;

    sget-object v5, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupActionInit:[Ljava/lang/Object;

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->fillGroup(Ljava/util/ArrayList;Ljava/util/Map;[Ljava/lang/Object;Ljava/util/Set;ZZ)V

    .line 225
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupAction:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->fixGroup(Ljava/util/ArrayList;)V

    .line 228
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupColor:Ljava/util/ArrayList;

    .line 229
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupColor:Ljava/util/ArrayList;

    sget-object v5, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupColorInit:[Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->fillGroup(Ljava/util/ArrayList;Ljava/util/Map;[Ljava/lang/Object;Ljava/util/Set;ZZ)V

    .line 230
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupColor:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->fixGroup(Ljava/util/ArrayList;)V

    .line 234
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlayback:Ljava/util/ArrayList;

    .line 235
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlayback:Ljava/util/ArrayList;

    sget-object v5, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlaybackInit:[Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->fillGroup(Ljava/util/ArrayList;Ljava/util/Map;[Ljava/lang/Object;Ljava/util/Set;ZZ)V

    .line 236
    const/4 v13, 0x0

    .line 237
    .local v13, "i":I
    const/4 v12, 0x0

    .line 238
    .local v12, "emptyCount":I
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlayback:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v13, v2, :cond_7

    .line 239
    rem-int/lit8 v2, v13, 0x3

    if-nez v2, :cond_2

    .line 240
    const/4 v12, 0x0

    .line 243
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlayback:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;

    .line 244
    .local v9, "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    iget-object v2, v9, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 245
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlayback:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v13, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 246
    add-int/lit8 v12, v12, 0x1

    .line 251
    :cond_3
    :goto_3
    add-int/lit8 v13, v13, 0x1

    .line 253
    rem-int/lit8 v2, v13, 0x3

    if-nez v2, :cond_1

    const/4 v2, 0x3

    if-ne v12, v2, :cond_1

    .line 254
    add-int/lit8 v13, v13, -0x3

    .line 255
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlayback:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 256
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlayback:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 257
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupPlayback:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 209
    .end local v6    # "usedButtons":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v9    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    .end local v11    # "devices":[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    .end local v12    # "emptyCount":I
    .end local v13    # "i":I
    :cond_4
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 211
    .restart local v10    # "d":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    .restart local v11    # "devices":[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 247
    .end local v10    # "d":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    .restart local v6    # "usedButtons":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v9    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    .restart local v12    # "emptyCount":I
    .restart local v13    # "i":I
    :cond_6
    iget-object v2, v9, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    const-string v3, "btn.live"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    .line 249
    sget v2, Lcom/microsoft/xbox/xle/urc/R$string;->btn_live:I

    iput v2, v9, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->label:I

    goto :goto_3

    .line 263
    .end local v9    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    :cond_7
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNumpad:Ljava/util/ArrayList;

    .line 264
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNumpad:Ljava/util/ArrayList;

    sget-object v5, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNumpadInit:[Ljava/lang/Object;

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->fillGroup(Ljava/util/ArrayList;Ljava/util/Map;[Ljava/lang/Object;Ljava/util/Set;ZZ)V

    .line 265
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNumpad:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0xc

    if-ne v2, v3, :cond_9

    .line 266
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNumpad:Ljava/util/ArrayList;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 267
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNumpad:Ljava/util/ArrayList;

    const/16 v3, 0x9

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 269
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNumpad:Ljava/util/ArrayList;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 270
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNumpad:Ljava/util/ArrayList;

    const/16 v3, 0xb

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 275
    :cond_9
    sget-object v3, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupNavVolumeInit:[Ljava/lang/String;

    array-length v5, v3

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v5, :cond_b

    aget-object v19, v3, v2

    .line 276
    .local v19, "skipID":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getRealID(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v17

    .line 277
    .local v17, "realID":Ljava/lang/String;
    if-eqz v17, :cond_a

    .line 278
    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 275
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 282
    .end local v17    # "realID":Ljava/lang/String;
    .end local v19    # "skipID":Ljava/lang/String;
    :cond_b
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    .line 284
    .local v20, "tempMoreMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreFull:Ljava/util/ArrayList;

    .line 285
    if-eqz v4, :cond_d

    .line 286
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 287
    .local v15, "key":Ljava/lang/String;
    invoke-interface {v6, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 291
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v4}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getRealLabel(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v16

    .line 292
    .local v16, "label":Ljava/lang/String;
    if-eqz v16, :cond_c

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_c

    .line 296
    new-instance v9, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;

    invoke-direct {v9}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;-><init>()V

    .line 297
    .restart local v9    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    iput-object v15, v9, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    .line 298
    move-object/from16 v0, v16

    iput-object v0, v9, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->labelStr:Ljava/lang/String;

    .line 300
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreFull:Ljava/util/ArrayList;

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    iget-object v3, v9, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 306
    .end local v9    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    .end local v15    # "key":Ljava/lang/String;
    .end local v16    # "label":Ljava/lang/String;
    :cond_d
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    .line 307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    if-eqz v2, :cond_f

    .line 308
    const/4 v14, 0x0

    .local v14, "ind":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v14, v2, :cond_f

    .line 309
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;

    .line 310
    .restart local v9    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    if-eqz v9, :cond_e

    iget-object v2, v9, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 311
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    :cond_e
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 315
    .end local v9    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    .end local v14    # "ind":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->fixGroup(Ljava/util/ArrayList;)V

    .line 316
    return-void
.end method

.method public getNarratorContentIdFromButtonId(Ljava/lang/String;)I
    .locals 1
    .param p1, "buttonId"    # Ljava/lang/String;

    .prologue
    .line 88
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    :cond_0
    const/4 v0, 0x0

    .line 91
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mNarratorContentMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public getRealID(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p2, "buttons":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 361
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alwaysAvaliable:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 384
    .end local p1    # "id":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 366
    .restart local p1    # "id":Ljava/lang/String;
    :cond_1
    if-nez p2, :cond_2

    move-object p1, v2

    .line 367
    goto :goto_0

    .line 370
    :cond_2
    invoke-interface {p2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 374
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->alternativeButtons:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 375
    .local v1, "alts":[Ljava/lang/String;
    if-nez v1, :cond_3

    move-object p1, v2

    .line 376
    goto :goto_0

    .line 379
    :cond_3
    array-length v4, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_5

    aget-object v0, v1, v3

    .line 380
    .local v0, "alt":Ljava/lang/String;
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move-object p1, v0

    .line 381
    goto :goto_0

    .line 379
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v0    # "alt":Ljava/lang/String;
    :cond_5
    move-object p1, v2

    .line 384
    goto :goto_0
.end method

.method public getRealLabel(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p2, "buttons":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 388
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 396
    :cond_0
    :goto_0
    return-object v0

    .line 392
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->doNotUseLabel:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 396
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getStringIdFromTextId(I)I
    .locals 1
    .param p1, "textId"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->mStringMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    return v0
.end method

.method public removeView(Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;

    .prologue
    .line 502
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 503
    return-void
.end method

.method public declared-synchronized useButton(Ljava/lang/String;)V
    .locals 8
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x3

    .line 401
    monitor-enter p0

    const/4 v1, 0x0

    .line 402
    .local v1, "foundInfo":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreFull:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 403
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreFull:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;

    .line 404
    .local v0, "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    if-eqz v0, :cond_2

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_2

    .line 405
    move-object v1, v0

    .line 410
    .end local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    :cond_0
    if-nez v1, :cond_3

    .line 447
    :cond_1
    monitor-exit p0

    return-void

    .line 402
    .restart local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 415
    .end local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    :cond_3
    const/4 v3, 0x0

    .line 416
    :goto_1
    :try_start_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_6

    .line 417
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;

    .line 418
    .restart local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    if-eqz v0, :cond_4

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 419
    :cond_4
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 401
    .end local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 421
    .restart local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 425
    .end local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    :cond_6
    :try_start_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 426
    :goto_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, v7, :cond_7

    .line 427
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 430
    :cond_7
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    .line 431
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_9

    .line 432
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;

    .line 433
    .restart local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    if-eqz v0, :cond_8

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    if-eqz v5, :cond_8

    .line 434
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->lastUsedButtons:Ljava/util/ArrayList;

    iget-object v6, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 431
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 437
    .end local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$ButtonInfo;
    :cond_9
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->saveLastUsedButtons()V

    .line 439
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->groupMoreSmall:Ljava/util/ArrayList;

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->fixGroup(Ljava/util/ArrayList;)V

    .line 440
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->views:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_a
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;

    .line 441
    .local v4, "view":Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->getGroupType()I

    move-result v6

    const/4 v7, 0x6

    if-ne v6, v7, :cond_a

    .line 444
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->updateButtons()V

    .line 445
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->requestLayout()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method
