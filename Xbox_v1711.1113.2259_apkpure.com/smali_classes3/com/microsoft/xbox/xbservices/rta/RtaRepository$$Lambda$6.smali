.class final synthetic Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$6;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Predicate;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/xbservices/rta/RtaRepository;

.field private final arg$2:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$6;->arg$1:Lcom/microsoft/xbox/xbservices/rta/RtaRepository;

    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$6;->arg$2:Ljava/lang/String;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;Ljava/lang/String;)Lio/reactivex/functions/Predicate;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$6;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$6;-><init>(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public test(Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$6;->arg$1:Lcom/microsoft/xbox/xbservices/rta/RtaRepository;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$6;->arg$2:Ljava/lang/String;

    check-cast p1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->lambda$subscribeToRtaEvents$4(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;)Z

    move-result v0

    return v0
.end method
