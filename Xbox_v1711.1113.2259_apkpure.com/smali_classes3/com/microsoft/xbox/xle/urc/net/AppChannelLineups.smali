.class public Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups;
.super Ljava/lang/Object;
.source "AppChannelLineups.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;,
        Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseJSON(Lorg/json/JSONArray;)Ljava/util/List;
    .locals 5
    .param p0, "providers"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 82
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;>;"
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 85
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 86
    new-instance v3, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;-><init>(Lorg/json/JSONObject;)V

    .line 87
    .local v3, "provider":Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;
    iget-object v4, v3, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->channels:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 88
    iget-object v4, v3, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->channels:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 85
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 91
    .end local v3    # "provider":Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;
    :cond_1
    return-object v2
.end method

.method public static testChannelInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;
    .locals 3
    .param p0, "providerName"    # Ljava/lang/String;
    .param p1, "channelName"    # Ljava/lang/String;

    .prologue
    .line 96
    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;-><init>()V

    .line 97
    .local v1, "provider":Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;
    iput-object p0, v1, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->name:Ljava/lang/String;

    iput-object p0, v1, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->id:Ljava/lang/String;

    .line 98
    const/16 v2, -0x2800

    iput v2, v1, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->primaryColor:I

    .line 99
    const v2, -0x80cd00

    iput v2, v1, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->secondaryColor:I

    .line 101
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;-><init>()V

    .line 102
    .local v0, "channel":Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;
    iput-object p1, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->name:Ljava/lang/String;

    iput-object p1, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->id:Ljava/lang/String;

    .line 103
    iput-object v1, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    .line 105
    return-object v0
.end method
