.class public Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreen;
.super Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;
.source "ClubInviteScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getContentScreenId()I
    .locals 1

    .prologue
    .line 21
    const v0, 0x7f03008c

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 12
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->onCreate()V

    .line 13
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreen;->setShowUtilityBar(Z)V

    .line 14
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreen;->setDesiredDrawerLockState(I)V

    .line 16
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 17
    return-void
.end method
