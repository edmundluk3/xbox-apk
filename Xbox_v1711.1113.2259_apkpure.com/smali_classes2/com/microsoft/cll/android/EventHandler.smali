.class public Lcom/microsoft/cll/android/EventHandler;
.super Lcom/microsoft/cll/android/ScheduledWorker;
.source "EventHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/cll/android/EventHandler$1;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

.field private final cllEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/ICllEvents;",
            ">;"
        }
    .end annotation
.end field

.field final criticalHandler:Lcom/microsoft/cll/android/AbstractHandler;

.field private endpoint:Ljava/net/URL;

.field private final logger:Lcom/microsoft/cll/android/ILogger;

.field final normalHandler:Lcom/microsoft/cll/android/AbstractHandler;

.field private sampleId:I

.field private sender:Lcom/microsoft/cll/android/EventSender;

.field private ticketCallback:Lcom/microsoft/cll/android/ITicketCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/cll/android/EventHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/cll/android/EventHandler;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lcom/microsoft/cll/android/ClientTelemetry;Ljava/util/List;Lcom/microsoft/cll/android/ILogger;Lcom/microsoft/cll/android/AbstractHandler;Lcom/microsoft/cll/android/AbstractHandler;)V
    .locals 2
    .param p1, "clientTelemetry"    # Lcom/microsoft/cll/android/ClientTelemetry;
    .param p3, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p4, "normalEventHandler"    # Lcom/microsoft/cll/android/AbstractHandler;
    .param p5, "criticalEventAbstractHandler"    # Lcom/microsoft/cll/android/AbstractHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/cll/android/ClientTelemetry;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/ICllEvents;",
            ">;",
            "Lcom/microsoft/cll/android/ILogger;",
            "Lcom/microsoft/cll/android/AbstractHandler;",
            "Lcom/microsoft/cll/android/AbstractHandler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, "cllEvents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/cll/android/ICllEvents;>;"
    sget-object v0, Lcom/microsoft/cll/android/SettingsStore$Settings;->QUEUEDRAININTERVAL:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v0}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v0

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/cll/android/ScheduledWorker;-><init>(J)V

    .line 15
    const-string v0, "EventHandler"

    iput-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->TAG:Ljava/lang/String;

    .line 31
    iput-object p1, p0, Lcom/microsoft/cll/android/EventHandler;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    .line 32
    iput-object p2, p0, Lcom/microsoft/cll/android/EventHandler;->cllEvents:Ljava/util/List;

    .line 33
    iput-object p3, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    .line 34
    iput-object p4, p0, Lcom/microsoft/cll/android/EventHandler;->normalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    .line 35
    iput-object p5, p0, Lcom/microsoft/cll/android/EventHandler;->criticalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/cll/android/EventHandler;->sampleId:I

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/cll/android/ClientTelemetry;Ljava/util/List;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;)V
    .locals 2
    .param p1, "clientTelemetry"    # Lcom/microsoft/cll/android/ClientTelemetry;
    .param p3, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p4, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/cll/android/ClientTelemetry;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/ICllEvents;",
            ">;",
            "Lcom/microsoft/cll/android/ILogger;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    .local p2, "cllEvents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/cll/android/ICllEvents;>;"
    sget-object v0, Lcom/microsoft/cll/android/SettingsStore$Settings;->QUEUEDRAININTERVAL:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v0}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v0

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/cll/android/ScheduledWorker;-><init>(J)V

    .line 15
    const-string v0, "EventHandler"

    iput-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->TAG:Ljava/lang/String;

    .line 46
    iput-object p1, p0, Lcom/microsoft/cll/android/EventHandler;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    .line 47
    iput-object p2, p0, Lcom/microsoft/cll/android/EventHandler;->cllEvents:Ljava/util/List;

    .line 48
    iput-object p3, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    .line 49
    new-instance v0, Lcom/microsoft/cll/android/CriticalEventHandler;

    invoke-direct {v0, p3, p4, p1}, Lcom/microsoft/cll/android/CriticalEventHandler;-><init>(Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/ClientTelemetry;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->criticalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    .line 50
    new-instance v0, Lcom/microsoft/cll/android/NormalEventHandler;

    invoke-direct {v0, p3, p4, p1}, Lcom/microsoft/cll/android/NormalEventHandler;-><init>(Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/ClientTelemetry;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->normalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/cll/android/EventHandler;->sampleId:I

    .line 52
    return-void
.end method

.method private Filter(Lcom/microsoft/cll/android/SerializedEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/microsoft/cll/android/SerializedEvent;

    .prologue
    const/4 v0, 0x1

    .line 157
    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getSerializedData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sget-object v2, Lcom/microsoft/cll/android/SettingsStore$Settings;->MAXEVENTSIZEINBYTES:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v2}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v2

    if-le v1, v2, :cond_0

    .line 158
    iget-object v1, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "EventHandler"

    const-string v3, "Event is too large"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :goto_0
    return v0

    .line 162
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/cll/android/EventHandler;->IsUploadEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/microsoft/cll/android/EventHandler;->IsInSample(Lcom/microsoft/cll/android/SerializedEvent;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "EventHandler"

    const-string v3, "Filtered event"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private IsInSample(Lcom/microsoft/cll/android/SerializedEvent;)Z
    .locals 6
    .param p1, "event"    # Lcom/microsoft/cll/android/SerializedEvent;

    .prologue
    const/4 v1, 0x0

    .line 176
    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 189
    :cond_0
    :goto_0
    return v1

    .line 180
    :cond_1
    iget v2, p0, Lcom/microsoft/cll/android/EventHandler;->sampleId:I

    if-gez v2, :cond_2

    .line 181
    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x7

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 182
    .local v0, "lastDigits":Ljava/lang/String;
    const/16 v2, 0x10

    invoke-static {v0, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    rem-int/lit8 v2, v2, 0x64

    iput v2, p0, Lcom/microsoft/cll/android/EventHandler;->sampleId:I

    .line 185
    .end local v0    # "lastDigits":Ljava/lang/String;
    :cond_2
    iget v2, p0, Lcom/microsoft/cll/android/EventHandler;->sampleId:I

    int-to-double v2, v2

    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getSampleRate()D

    move-result-wide v4

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 186
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private IsUploadEnabled()Z
    .locals 1

    .prologue
    .line 197
    sget-object v0, Lcom/microsoft/cll/android/SettingsStore$Settings;->UPLOADENABLED:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v0}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsBoolean(Lcom/microsoft/cll/android/SettingsStore$Settings;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    const/4 v0, 0x0

    .line 201
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startEventQueueWriter(Ljava/lang/Runnable;)Z
    .locals 6
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    const/4 v2, 0x0

    .line 295
    iget-object v3, p0, Lcom/microsoft/cll/android/EventHandler;->endpoint:Ljava/net/URL;

    if-nez v3, :cond_0

    .line 296
    iget-object v3, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v4, "EventHandler"

    const-string v5, "No endpoint set"

    invoke-interface {v3, v4, v5}, Lcom/microsoft/cll/android/ILogger;->warn(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :goto_0
    return v2

    :cond_0
    move-object v1, p1

    .line 301
    check-cast v1, Lcom/microsoft/cll/android/EventQueueWriter;

    .line 302
    .local v1, "eqw":Lcom/microsoft/cll/android/EventQueueWriter;
    iget-object v3, p0, Lcom/microsoft/cll/android/EventHandler;->sender:Lcom/microsoft/cll/android/EventSender;

    if-eqz v3, :cond_1

    .line 303
    iget-object v3, p0, Lcom/microsoft/cll/android/EventHandler;->sender:Lcom/microsoft/cll/android/EventSender;

    invoke-virtual {v1, v3}, Lcom/microsoft/cll/android/EventQueueWriter;->setSender(Lcom/microsoft/cll/android/EventSender;)V

    .line 307
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/cll/android/EventHandler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v3, p1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 315
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 308
    :catch_0
    move-exception v0

    .line 309
    .local v0, "e":Ljava/util/concurrent/RejectedExecutionException;
    iget-object v3, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v4, "EventHandler"

    const-string v5, "Could not start new thread for EventQueueWriter"

    invoke-interface {v3, v4, v5}, Lcom/microsoft/cll/android/ILogger;->warn(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 311
    .end local v0    # "e":Ljava/util/concurrent/RejectedExecutionException;
    :catch_1
    move-exception v0

    .line 312
    .local v0, "e":Ljava/lang/NullPointerException;
    iget-object v2, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v3, "EventHandler"

    const-string v4, "Executor is null. Is the cll paused or stopped?"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method protected addToStorage(Lcom/microsoft/cll/android/SerializedEvent;Ljava/util/List;)Z
    .locals 5
    .param p1, "event"    # Lcom/microsoft/cll/android/SerializedEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/cll/android/SerializedEvent;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 115
    sget-object v2, Lcom/microsoft/cll/android/EventHandler$1;->$SwitchMap$com$microsoft$cll$android$Cll$EventPersistence:[I

    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getPersistence()Lcom/microsoft/cll/android/Cll$EventPersistence;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/cll/android/Cll$EventPersistence;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 141
    iget-object v1, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "EventHandler"

    const-string v3, "Unknown persistence"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    sget-boolean v1, Lcom/microsoft/cll/android/EventHandler;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 118
    :pswitch_0
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/cll/android/EventHandler;->normalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getSerializedData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/microsoft/cll/android/AbstractHandler;->add(Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/microsoft/cll/android/FileStorage$FileFullException; {:try_start_0 .. :try_end_0} :catch_1

    .line 146
    :cond_0
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v3, "EventHandler"

    const-string v4, "Could not add event to normal storage"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 122
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 123
    .local v0, "e":Lcom/microsoft/cll/android/FileStorage$FileFullException;
    iget-object v2, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v3, "EventHandler"

    const-string v4, "No space on disk to store events"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/cll/android/ILogger;->warn(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 130
    .end local v0    # "e":Lcom/microsoft/cll/android/FileStorage$FileFullException;
    :pswitch_1
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/cll/android/EventHandler;->criticalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getSerializedData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/microsoft/cll/android/AbstractHandler;->add(Ljava/lang/String;Ljava/util/List;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/microsoft/cll/android/FileStorage$FileFullException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 131
    :catch_2
    move-exception v0

    .line 132
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v3, "EventHandler"

    const-string v4, "Could not add event to normal storage"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 134
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 135
    .local v0, "e":Lcom/microsoft/cll/android/FileStorage$FileFullException;
    iget-object v2, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v3, "EventHandler"

    const-string v4, "No space on disk to store events"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/cll/android/ILogger;->warn(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 115
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected log(Lcom/microsoft/cll/android/SerializedEvent;Ljava/util/List;)Z
    .locals 12
    .param p1, "event"    # Lcom/microsoft/cll/android/SerializedEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/cll/android/SerializedEvent;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v11, 0x1

    .line 92
    invoke-direct {p0, p1}, Lcom/microsoft/cll/android/EventHandler;->Filter(Lcom/microsoft/cll/android/SerializedEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    const/4 v0, 0x0

    .line 104
    :goto_0
    return v0

    .line 97
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getLatency()Lcom/microsoft/cll/android/Cll$EventLatency;

    move-result-object v0

    sget-object v1, Lcom/microsoft/cll/android/Cll$EventLatency;->REALTIME:Lcom/microsoft/cll/android/Cll$EventLatency;

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/microsoft/cll/android/EventHandler;->isPaused:Z

    if-nez v0, :cond_1

    .line 98
    new-instance v0, Lcom/microsoft/cll/android/EventQueueWriter;

    iget-object v1, p0, Lcom/microsoft/cll/android/EventHandler;->endpoint:Ljava/net/URL;

    iget-object v4, p0, Lcom/microsoft/cll/android/EventHandler;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    iget-object v5, p0, Lcom/microsoft/cll/android/EventHandler;->cllEvents:Ljava/util/List;

    iget-object v6, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    iget-object v7, p0, Lcom/microsoft/cll/android/EventHandler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v9, p0, Lcom/microsoft/cll/android/EventHandler;->ticketCallback:Lcom/microsoft/cll/android/ITicketCallback;

    move-object v2, p1

    move-object v3, p2

    move-object v8, p0

    invoke-direct/range {v0 .. v9}, Lcom/microsoft/cll/android/EventQueueWriter;-><init>(Ljava/net/URL;Lcom/microsoft/cll/android/SerializedEvent;Ljava/util/List;Lcom/microsoft/cll/android/ClientTelemetry;Ljava/util/List;Lcom/microsoft/cll/android/ILogger;Ljava/util/concurrent/ScheduledExecutorService;Lcom/microsoft/cll/android/EventHandler;Lcom/microsoft/cll/android/ITicketCallback;)V

    invoke-direct {p0, v0}, Lcom/microsoft/cll/android/EventHandler;->startEventQueueWriter(Ljava/lang/Runnable;)Z

    move-result v10

    .line 99
    .local v10, "result":Z
    if-ne v10, v11, :cond_1

    move v0, v11

    .line 100
    goto :goto_0

    .line 104
    .end local v10    # "result":Z
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/cll/android/EventHandler;->addToStorage(Lcom/microsoft/cll/android/SerializedEvent;Ljava/util/List;)Z

    move-result v0

    goto :goto_0
.end method

.method public run()V
    .locals 7

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/microsoft/cll/android/EventHandler;->interval:J

    sget-object v2, Lcom/microsoft/cll/android/SettingsStore$Settings;->QUEUEDRAININTERVAL:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v2}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->nextExecution:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 63
    sget-object v0, Lcom/microsoft/cll/android/SettingsStore$Settings;->QUEUEDRAININTERVAL:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v0}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/microsoft/cll/android/EventHandler;->interval:J

    .line 64
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    iget-wide v2, p0, Lcom/microsoft/cll/android/EventHandler;->interval:J

    iget-wide v4, p0, Lcom/microsoft/cll/android/EventHandler;->interval:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->nextExecution:Ljava/util/concurrent/ScheduledFuture;

    .line 68
    :cond_0
    sget-object v0, Lcom/microsoft/cll/android/EventQueueWriter;->future:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v1, "EventHandler"

    const-string v2, "Retry logic in progress, skipping normal send"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :goto_0
    return-void

    .line 73
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/cll/android/EventHandler;->send()Z

    goto :goto_0
.end method

.method protected send()Z
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/cll/android/EventHandler;->send(Lcom/microsoft/cll/android/Cll$EventPersistence;)Z

    move-result v0

    return v0
.end method

.method protected send(Lcom/microsoft/cll/android/Cll$EventPersistence;)Z
    .locals 8
    .param p1, "persistence"    # Lcom/microsoft/cll/android/Cll$EventPersistence;

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/microsoft/cll/android/EventHandler;->isPaused:Z

    if-eqz v0, :cond_0

    .line 222
    const/4 v0, 0x0

    .line 252
    :goto_0
    return v0

    .line 225
    :cond_0
    const/4 v2, 0x0

    .line 227
    .local v2, "storages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/cll/android/IStorage;>;"
    if-nez p1, :cond_2

    .line 228
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v1, "EventHandler"

    const-string v3, "Draining All events"

    invoke-interface {v0, v1, v3}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->normalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/AbstractHandler;->getFilesForDraining()Ljava/util/List;

    move-result-object v2

    .line 230
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->criticalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/AbstractHandler;->getFilesForDraining()Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 248
    :cond_1
    :goto_1
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_3

    .line 249
    new-instance v0, Lcom/microsoft/cll/android/EventQueueWriter;

    iget-object v1, p0, Lcom/microsoft/cll/android/EventHandler;->endpoint:Ljava/net/URL;

    iget-object v3, p0, Lcom/microsoft/cll/android/EventHandler;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    iget-object v4, p0, Lcom/microsoft/cll/android/EventHandler;->cllEvents:Ljava/util/List;

    iget-object v5, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    iget-object v6, p0, Lcom/microsoft/cll/android/EventHandler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v7, p0, Lcom/microsoft/cll/android/EventHandler;->ticketCallback:Lcom/microsoft/cll/android/ITicketCallback;

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/cll/android/EventQueueWriter;-><init>(Ljava/net/URL;Ljava/util/List;Lcom/microsoft/cll/android/ClientTelemetry;Ljava/util/List;Lcom/microsoft/cll/android/ILogger;Ljava/util/concurrent/ScheduledExecutorService;Lcom/microsoft/cll/android/ITicketCallback;)V

    invoke-direct {p0, v0}, Lcom/microsoft/cll/android/EventHandler;->startEventQueueWriter(Ljava/lang/Runnable;)Z

    move-result v0

    goto :goto_0

    .line 232
    :cond_2
    sget-object v0, Lcom/microsoft/cll/android/EventHandler$1;->$SwitchMap$com$microsoft$cll$android$Cll$EventPersistence:[I

    invoke-virtual {p1}, Lcom/microsoft/cll/android/Cll$EventPersistence;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 242
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v1, "EventHandler"

    const-string v3, "Unknown persistence"

    invoke-interface {v0, v1, v3}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    sget-boolean v0, Lcom/microsoft/cll/android/EventHandler;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 234
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v1, "EventHandler"

    const-string v3, "Draining normal events"

    invoke-interface {v0, v1, v3}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->normalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/AbstractHandler;->getFilesForDraining()Ljava/util/List;

    move-result-object v2

    .line 236
    goto :goto_1

    .line 238
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v1, "EventHandler"

    const-string v3, "Draining Critical events"

    invoke-interface {v0, v1, v3}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->criticalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/AbstractHandler;->getFilesForDraining()Ljava/util/List;

    move-result-object v2

    .line 240
    goto :goto_1

    .line 252
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected setEndpointUrl(Ljava/lang/String;)V
    .locals 4
    .param p1, "endpointUrl"    # Ljava/lang/String;

    .prologue
    .line 262
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/microsoft/cll/android/EventHandler;->endpoint:Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    :goto_0
    return-void

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/net/MalformedURLException;
    iget-object v1, p0, Lcom/microsoft/cll/android/EventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "EventHandler"

    const-string v3, "Bad Endpoint URL Form"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method setSender(Lcom/microsoft/cll/android/EventSender;)V
    .locals 0
    .param p1, "sender"    # Lcom/microsoft/cll/android/EventSender;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/microsoft/cll/android/EventHandler;->sender:Lcom/microsoft/cll/android/EventSender;

    .line 275
    return-void
.end method

.method setXuidCallback(Lcom/microsoft/cll/android/ITicketCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/microsoft/cll/android/ITicketCallback;

    .prologue
    .line 285
    iput-object p1, p0, Lcom/microsoft/cll/android/EventHandler;->ticketCallback:Lcom/microsoft/cll/android/ITicketCallback;

    .line 286
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Lcom/microsoft/cll/android/ScheduledWorker;->stop()V

    .line 80
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->normalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/AbstractHandler;->close()V

    .line 81
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->criticalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/AbstractHandler;->close()V

    .line 82
    return-void
.end method

.method synchronize()V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/microsoft/cll/android/EventHandler;->normalHandler:Lcom/microsoft/cll/android/AbstractHandler;

    check-cast v0, Lcom/microsoft/cll/android/NormalEventHandler;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/NormalEventHandler;->writeQueueToDisk()V

    .line 282
    return-void
.end method
