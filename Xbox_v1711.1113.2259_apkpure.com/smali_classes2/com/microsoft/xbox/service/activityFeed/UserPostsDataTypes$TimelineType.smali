.class public final enum Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
.super Ljava/lang/Enum;
.source "UserPostsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TimelineType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

.field public static final enum Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

.field public static final enum Page:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

.field public static final enum Title:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

.field public static final enum User:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    const-string v1, "User"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->User:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 40
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    const-string v1, "Club"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    const-string v1, "Title"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Title:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 42
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    const-string v1, "Page"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Page:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Unknown:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 38
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->User:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Title:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Page:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Unknown:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    return-object v0
.end method
