.class public Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ClubRecommendationScreenAdapter.java"


# instance fields
.field private final adapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

.field private final clubRecommendationViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;

.field private final header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;)V
    .locals 5
    .param p1, "clubRecommendationViewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->clubRecommendationViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;

    .line 27
    const v2, 0x7f0e0a95

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 28
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 29
    const v2, 0x7f0e0358

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 30
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->getHeaderText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 32
    const v2, 0x7f0e06bd

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 33
    .local v1, "noContent":Landroid/widget/TextView;
    const v2, 0x7f070b62

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 35
    const v2, 0x7f0e03b9

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    const v2, 0x7f0e03ba

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 39
    .local v0, "clubSearchRecyclerView":Landroid/support/v7/widget/RecyclerView;
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->adapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    .line 45
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->adapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 46
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->clubRecommendationViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->navigateToSearch()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;)V
    .locals 4
    .param p0, "clubRecommendationViewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;
    .param p1, "clubModel"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    .prologue
    .line 40
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 41
    .local v0, "infoModel":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ClubId"

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->clubId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 42
    const-string v1, "Clubs - Navigate To Suggested Club"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 43
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->clubId()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;J)V

    .line 44
    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->clubRecommendationViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->getState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->adapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->clear()V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->adapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->clubRecommendationViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->getClubCardModels()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->addAll(Ljava/util/Collection;)V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;->adapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->notifyDataSetChanged()V

    .line 54
    return-void
.end method
