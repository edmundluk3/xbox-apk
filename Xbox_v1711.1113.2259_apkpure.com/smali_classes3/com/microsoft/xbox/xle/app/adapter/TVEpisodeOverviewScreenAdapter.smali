.class public Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "TVEpisodeOverviewScreenAdapter.java"


# instance fields
.field private airDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private allSeasonsTextView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private descriptionView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private durationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private languagesTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private networkTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected playOnXboxButton:Landroid/widget/RelativeLayout;

.field private ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private resolutionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private staringListTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field protected viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 34
    const v0, 0x7f0e0b0b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->screenBody:Landroid/view/View;

    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    .line 37
    const v0, 0x7f0e0b0c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 38
    const v0, 0x7f0e0b11

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->descriptionView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->content:Landroid/view/View;

    .line 41
    const v0, 0x7f0e0b14

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->staringListTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 42
    const v0, 0x7f0e0b15

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->airDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 43
    const v0, 0x7f0e0b16

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->networkTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 44
    const v0, 0x7f0e0b17

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->durationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 45
    const v0, 0x7f0e0b18

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 46
    const v0, 0x7f0e0b19

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 47
    const v0, 0x7f0e0b1a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->resolutionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 48
    const v0, 0x7f0e0b1b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->languagesTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 50
    const v0, 0x7f0e0b13

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->allSeasonsTextView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->allSeasonsTextView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->allSeasonsTextView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->allSeasonsTextView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setPaintFlags(I)V

    .line 55
    :cond_0
    const v0, 0x7f0e0b0d

    .line 56
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->playOnXboxButton:Landroid/widget/RelativeLayout;

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->playOnXboxButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->playOnXboxButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->allSeasonsTextView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 58
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getCurrentScreenData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showProviderPickerDialogScreen(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->navigateToParentSeries()V

    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->updateLoadingIndicator(Z)V

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->descriptionView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getHasParentSeasons()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->allSeasonsTextView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->allSeasonsTextView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getHasParentSeasons()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 80
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->staringListTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getStars()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0703d5

    .line 81
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getStars()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 80
    invoke-static {v3, v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 82
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->airDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getReleaseData()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070c9d

    .line 83
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getReleaseDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 82
    invoke-static {v3, v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 84
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->networkTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getNetwork()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0703cb

    .line 85
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getNetwork()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 84
    invoke-static {v3, v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 86
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->durationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    .line 88
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getDurationInMinutes()I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0703c2

    .line 89
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getDurationInMinutes()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f07061b

    .line 90
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 86
    invoke-static {v3, v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 92
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0703cd

    .line 93
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 92
    invoke-static {v3, v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    :goto_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0703c8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->resolutionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->languagesTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 98
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;->allSeasonsTextView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 80
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 82
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 84
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 88
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 92
    goto :goto_5

    :cond_6
    move v1, v2

    .line 95
    goto :goto_6
.end method
