.class Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$AcceptInviteCallback;
.super Ljava/lang/Object;
.source "ClubInvitationsScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AcceptInviteCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

.field private final xuid:J


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;J)V
    .locals 0
    .param p2, "xuid"    # J

    .prologue
    .line 458
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$AcceptInviteCallback;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459
    iput-wide p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$AcceptInviteCallback;->xuid:J

    .line 460
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;JLcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
    .param p2, "x1"    # J
    .param p4, "x2"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$1;

    .prologue
    .line 454
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$AcceptInviteCallback;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;J)V

    return-void
.end method


# virtual methods
.method public onPartialRosterChangeSuccess(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 477
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .local p2, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    const-string v0, "This shouldn\'t be possible, since all calls are single action."

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 478
    return-void
.end method

.method public onRosterChangeFailure(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 469
    .local p1, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 470
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$AcceptInviteCallback;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$AcceptInviteCallback;->xuid:J

    invoke-static {v0, p1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->access$700(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;Ljava/util/List;J)V

    .line 471
    return-void
.end method

.method public onRosterChangeSuccess(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 464
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$AcceptInviteCallback;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$AcceptInviteCallback;->xuid:J

    invoke-static {v0, v2, v3, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;JLjava/util/List;)V

    .line 465
    return-void
.end method
