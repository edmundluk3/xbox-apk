.class public Lcom/microsoft/xbox/xle/app/activity/HomeNowPlayingScreenPhone;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "HomeNowPlayingScreenPhone.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, "HomeNowPlayingScreenPhone"

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 18
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 19
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/HomeNowPlayingScreenPhone;->onCreateContentView()V

    .line 20
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->findChildPosition(Landroid/view/View;)I

    move-result v0

    .line 21
    .local v0, "position":I
    if-ltz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 22
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    const v2, 0x7f0e035d

    invoke-direct {v1, v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;-><init>(II)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/HomeNowPlayingScreenPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 23
    return-void

    .line 21
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 27
    const v0, 0x7f03014a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/HomeNowPlayingScreenPhone;->setContentView(I)V

    .line 28
    return-void
.end method
