.class public abstract Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
.super Ljava/lang/Object;
.source "HoverChatHeadKey.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubChatKey"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(J)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 29
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 30
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatHeadKey_ClubChatKey;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatHeadKey_ClubChatKey;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public abstract clubId()J
.end method
