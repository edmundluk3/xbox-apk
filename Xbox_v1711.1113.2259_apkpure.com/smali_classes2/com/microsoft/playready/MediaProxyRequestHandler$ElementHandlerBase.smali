.class Lcom/microsoft/playready/MediaProxyRequestHandler$ElementHandlerBase;
.super Ljava/lang/Object;
.source "MediaProxyRequestHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/MediaProxyRequestHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ElementHandlerBase"
.end annotation


# instance fields
.field private final mWPRootHandler:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/microsoft/playready/MediaProxyRequestHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/MediaProxyRequestHandler;)V
    .locals 1
    .param p1, "proxyHandler"    # Lcom/microsoft/playready/MediaProxyRequestHandler;

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$ElementHandlerBase;->mWPRootHandler:Ljava/lang/ref/WeakReference;

    .line 186
    return-void
.end method


# virtual methods
.method public getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$ElementHandlerBase;->mWPRootHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/MediaProxyRequestHandler;

    return-object v0
.end method
