.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Home;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Home"
.end annotation


# static fields
.field public static final LaunchHelp:Ljava/lang/String; = "Launch Help"

.field public static final OneGuidePin:Ljava/lang/String; = "OneGuide Pin Launch"

.field public static final Pin:Ljava/lang/String; = "Pin"

.field public static final PinReorder:Ljava/lang/String; = "Pin Reorder"

.field public static final Pins:Ljava/lang/String; = "Home Pins"

.field public static final Refresh:Ljava/lang/String; = "Pull Down Refresh"

.field public static final Unpin:Ljava/lang/String; = "Unpin"

.field public static final Unsnap:Ljava/lang/String; = "Unsnap"

.field public static final UserInfo:Ljava/lang/String; = "User Info"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
