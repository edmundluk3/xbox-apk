.class Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementHighestProgressCompare;
.super Ljava/lang/Object;
.source "GameAchievementComparisonViewModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AchievementHighestProgressCompare"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V
    .locals 0

    .prologue
    .line 1019
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementHighestProgressCompare;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;

    .prologue
    .line 1019
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementHighestProgressCompare;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)I
    .locals 5
    .param p1, "first"    # Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .param p2, "second"    # Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 1023
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 1039
    :cond_0
    :goto_0
    return v2

    .line 1025
    :cond_1
    if-nez p1, :cond_2

    move v2, v3

    .line 1026
    goto :goto_0

    .line 1027
    :cond_2
    if-nez p2, :cond_3

    move v2, v4

    .line 1028
    goto :goto_0

    .line 1031
    :cond_3
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v0

    .line 1032
    .local v0, "firstVal":I
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v1

    .line 1034
    .local v1, "secondVal":I
    if-ge v0, v1, :cond_4

    move v2, v4

    .line 1035
    goto :goto_0

    .line 1036
    :cond_4
    if-le v0, v1, :cond_0

    move v2, v3

    .line 1037
    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1019
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    check-cast p2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementHighestProgressCompare;->compare(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)I

    move-result v0

    return v0
.end method
