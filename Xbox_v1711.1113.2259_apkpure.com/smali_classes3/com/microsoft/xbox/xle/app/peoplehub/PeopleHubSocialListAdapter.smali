.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "PeopleHubSocialListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$HeaderViewHolder;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;",
        "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final CLUB_VIEW_TYPE:I = 0x7f0301b3

.field private static final GAME_VIEW_TYPE:I = 0x7f0301c8

.field private static final HEADER_VIEW_TYPE:I = 0x7f030135

.field private static final PERSON_VIEW_TYPE:I = 0x7f0301c9


# instance fields
.field private final itemAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "itemAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 44
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->itemAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 46
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;

    .line 84
    .local v0, "item":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;
    if-eqz v0, :cond_4

    .line 85
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;

    if-eqz v1, :cond_0

    .line 86
    const v1, 0x7f030135

    .line 98
    :goto_0
    return v1

    .line 87
    :cond_0
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;

    if-eqz v1, :cond_1

    .line 88
    const v1, 0x7f0301b3

    goto :goto_0

    .line 89
    :cond_1
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;

    if-eqz v1, :cond_2

    .line 90
    const v1, 0x7f0301c9

    goto :goto_0

    .line 91
    :cond_2
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;

    if-eqz v1, :cond_3

    .line 92
    const v1, 0x7f0301c8

    goto :goto_0

    .line 94
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown ISocialListItem type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 98
    :cond_4
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;I)V
    .locals 2
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 69
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;

    .line 70
    .local v0, "item":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;
    if-eqz v0, :cond_0

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->itemAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-virtual {p1, v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;->bindTo(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 73
    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v3, 0x0

    .line 50
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 52
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sparse-switch p2, :sswitch_data_0

    .line 63
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown viewType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 64
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 54
    :sswitch_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$HeaderViewHolder;

    const v2, 0x7f030135

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$HeaderViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 56
    :sswitch_1
    new-instance v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;

    const v2, 0x7f0301b3

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 58
    :sswitch_2
    new-instance v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;

    const v2, 0x7f0301c9

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 60
    :sswitch_3
    new-instance v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;

    const v2, 0x7f0301c8

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 52
    :sswitch_data_0
    .sparse-switch
        0x7f030135 -> :sswitch_0
        0x7f0301b3 -> :sswitch_1
        0x7f0301c8 -> :sswitch_3
        0x7f0301c9 -> :sswitch_2
    .end sparse-switch
.end method
