.class public Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;
.super Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
.source "CustomTypefaceEllipsizeTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$EllipsizeListener;
    }
.end annotation


# instance fields
.field private final DEFAULT_LINE_COUNT:I

.field private final ellipsisChanger:Ljava/lang/Runnable;

.field private ellipsizeListener:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$EllipsizeListener;

.field private isNeedResetLayout:Z

.field private lineCount:I

.field private textExpandedHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0b001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->DEFAULT_LINE_COUNT:I

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->isNeedResetLayout:Z

    .line 43
    new-instance v0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$1;-><init>(Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->ellipsisChanger:Ljava/lang/Runnable;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0b001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->DEFAULT_LINE_COUNT:I

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->isNeedResetLayout:Z

    .line 43
    new-instance v0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$1;-><init>(Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->ellipsisChanger:Ljava/lang/Runnable;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "typeface"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 18
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0b001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->DEFAULT_LINE_COUNT:I

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->isNeedResetLayout:Z

    .line 43
    new-instance v0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$1;-><init>(Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->ellipsisChanger:Ljava/lang/Runnable;

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    .prologue
    .line 13
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->lineCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    .prologue
    .line 13
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->DEFAULT_LINE_COUNT:I

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$EllipsizeListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->ellipsizeListener:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$EllipsizeListener;

    return-object v0
.end method


# virtual methods
.method public getCollapsedLineCount()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->DEFAULT_LINE_COUNT:I

    return v0
.end method

.method public getLineCount()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->lineCount:I

    return v0
.end method

.method public getTextExpandedHeight()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->textExpandedHeight:I

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v7, 0x0

    .line 52
    invoke-super/range {p0 .. p5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->onLayout(ZIIII)V

    .line 54
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->isNeedResetLayout:Z

    if-eqz v1, :cond_1

    .line 55
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getWidth()I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 56
    .local v0, "layout":Landroid/text/StaticLayout;
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->textExpandedHeight:I

    .line 57
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->lineCount:I

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->ellipsizeListener:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$EllipsizeListener;

    if-eqz v1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->ellipsisChanger:Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 62
    :cond_0
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->isNeedResetLayout:Z

    .line 64
    .end local v0    # "layout":Landroid/text/StaticLayout;
    :cond_1
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "after"    # I

    .prologue
    .line 38
    invoke-super {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->isNeedResetLayout:Z

    .line 41
    return-void
.end method

.method public setAlwaysShowText(Z)V
    .locals 1
    .param p1, "showAllText"    # Z

    .prologue
    .line 67
    if-nez p1, :cond_0

    .line 68
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->DEFAULT_LINE_COUNT:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setLines(I)V

    .line 70
    :cond_0
    return-void
.end method

.method public setEllipsizeListener(Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$EllipsizeListener;)V
    .locals 0
    .param p1, "ellipsizeListener"    # Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$EllipsizeListener;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->ellipsizeListener:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$EllipsizeListener;

    .line 86
    return-void
.end method
