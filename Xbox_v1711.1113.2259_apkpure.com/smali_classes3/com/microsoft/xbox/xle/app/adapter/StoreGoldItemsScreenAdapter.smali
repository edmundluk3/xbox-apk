.class public Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "StoreGoldItemsScreenAdapter.java"


# instance fields
.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;

.field private storeItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;"
        }
    .end annotation
.end field

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;)V
    .locals 4
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 28
    const v0, 0x7f0e0a70

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->screenBody:Landroid/view/View;

    .line 29
    const v0, 0x7f0e0a71

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    .line 33
    const v0, 0x7f0e0a72

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030219

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStart()V

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 50
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStop()V

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 56
    return-void
.end method

.method public updateViewOverride()V
    .locals 3

    .prologue
    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->getGoldLoungeData()Ljava/util/List;

    move-result-object v0

    .line 67
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->isBusy()Z

    move-result v1

    if-nez v1, :cond_1

    .line 69
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->storeItems:Ljava/util/List;

    if-eq v1, v0, :cond_1

    .line 71
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->storeItems:Ljava/util/List;

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->clear()V

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->storeItems:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;->addAll(Ljava/util/Collection;)V

    .line 76
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->restoreListPosition()V

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 81
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 82
    return-void
.end method
