.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$2;
.super Ljava/lang/Object;
.source "UTCClubs.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/functions/GenericFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
        "<",
        "Ljava/lang/Void;",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic eval(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 104
    check-cast p1, Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$2;->eval(Ljava/util/HashMap;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public eval(Ljava/util/HashMap;)Ljava/lang/Void;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "args":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v4, "pagename"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "pageaction"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "pagetransitionorder"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 109
    const-string v4, "pagetransitionorder"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 110
    .local v3, "pagetransitionorder":Ljava/lang/String;
    const-string v4, "pagename"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 111
    .local v2, "pageName":Ljava/lang/String;
    const-string v4, "pageaction"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 113
    .local v1, "pageAction":Ljava/lang/String;
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 114
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v4, "ClubId"

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->access$000()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 116
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->access$102(Ljava/lang/String;)Ljava/lang/String;

    .line 117
    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->access$202(Ljava/lang/String;)Ljava/lang/String;

    .line 118
    const-string v4, "02"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 119
    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 122
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->access$100()Ljava/lang/String;

    move-result-object v5

    if-eq v4, v5, :cond_1

    .line 123
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->access$100()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 128
    .end local v0    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .end local v1    # "pageAction":Ljava/lang/String;
    .end local v2    # "pageName":Ljava/lang/String;
    .end local v3    # "pagetransitionorder":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    return-object v4
.end method
