.class public final Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;
.super Ljava/lang/Object;
.source "PeopleHubSocialListItems.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HeaderListItem"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field public final headerCount:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final headerText:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "headerText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "headerCount"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 42
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->headerText:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->headerCount:Ljava/lang/String;

    .line 45
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;)I
    .locals 3
    .param p1, "another"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;

    .prologue
    .line 81
    instance-of v1, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;

    if-nez v1, :cond_0

    .line 82
    const/4 v1, -0x1

    .line 85
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 84
    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;

    .line 85
    .local v0, "other":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->headerText:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->headerText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringCompareUserLocale(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 31
    check-cast p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->compareTo(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    if-ne p1, p0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v1

    .line 55
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 56
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 58
    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;

    .line 59
    .local v0, "other":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->headerText:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->headerText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->headerCount:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->headerCount:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 65
    iget v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->hashCode:I

    if-nez v0, :cond_0

    .line 66
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->hashCode:I

    .line 67
    iget v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->headerText:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->hashCode:I

    .line 68
    iget v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->headerCount:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->hashCode:I

    .line 71
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;->hashCode:I

    return v0
.end method

.method public onClick(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 0
    .param p1, "viewModelBase"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 49
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
