.class public Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;
.super Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
.source "EPGAppChannelRow.java"


# instance fields
.field private mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

.field private mNameView:Lcom/microsoft/xbox/xle/ui/EPGProgramView;

.field private mShowsView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V
    .locals 4
    .param p1, "grid"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V

    .line 61
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mTopFrame:Landroid/view/ViewGroup;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->newInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mNameView:Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    .line 62
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mNameView:Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->disableStateColors()V

    .line 63
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mNameView:Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->setTopView(Landroid/view/View;)V

    .line 69
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mNameView:Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 70
    .local v1, "lpTopView":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    if-lez v2, :cond_0

    .line 72
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 73
    .local v0, "lpBottomFrame":Landroid/view/ViewGroup$MarginLayoutParams;
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f090290

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 76
    .end local v0    # "lpBottomFrame":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mShowsView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;
    .param p1, "x1"    # Landroid/animation/AnimatorSet;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrameAnimation:Landroid/animation/AnimatorSet;

    return-object p1
.end method


# virtual methods
.method public bind(ILjava/lang/Object;)V
    .locals 4
    .param p1, "row"    # I
    .param p2, "model"    # Ljava/lang/Object;

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->bind(I)V

    .line 86
    check-cast p2, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .end local p2    # "model":Ljava/lang/Object;
    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 89
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getAdapter()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    move-result-object v2

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mRow:I

    invoke-interface {v2, v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;->getHeightSpec(I)I

    move-result v0

    .line 90
    .local v0, "height":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mNameView:Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 91
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v2, v0, :cond_0

    .line 92
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 93
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mNameView:Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->requestLayout()V

    .line 96
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mNameView:Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getChannelName()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setTitle(Ljava/lang/String;)V

    .line 97
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mNameView:Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->displayTitle(Z)V

    .line 98
    return-void

    .line 96
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public refresh(Z)V
    .locals 0
    .param p1, "clear"    # Z

    .prologue
    .line 105
    return-void
.end method

.method protected startAnimateBottomFrame(Landroid/view/View;)Z
    .locals 20
    .param p1, "removeView"    # Landroid/view/View;

    .prologue
    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->minimumLayoutParamDimen(I)F

    move-result v15

    .line 117
    .local v15, "startHeight":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomView:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, Landroid/view/ViewGroup;->measure(II)V

    .line 120
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomView:Landroid/view/View;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->minimumLayoutParamDimen(I)F

    move-result v7

    .line 124
    .local v7, "finishHeight":F
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    .line 125
    .local v9, "animatorSet":Landroid/animation/AnimatorSet;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 127
    .local v10, "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    .line 128
    .local v14, "sourceView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mShowsView:Landroid/view/View;

    if-nez v2, :cond_1

    .line 129
    const v2, 0x7f0e0571

    invoke-virtual {v14, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mShowsView:Landroid/view/View;

    .line 132
    :cond_1
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v17, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const/16 v13, 0x64

    .line 135
    .local v13, "sequenceDelay":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mShowsView:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mShowsView:Landroid/view/View;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    :cond_2
    cmpl-float v2, v15, v7

    if-eqz v2, :cond_3

    .line 141
    new-instance v8, Landroid/animation/ValueAnimator;

    invoke-direct {v8}, Landroid/animation/ValueAnimator;-><init>()V

    .line 142
    .local v8, "animator":Landroid/animation/ValueAnimator;
    sget v2, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->BOTTOM_FRAME_ANIMATION_DURATION_MS:I

    int-to-long v2, v2

    invoke-virtual {v8, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 143
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v8, v2}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 144
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    float-to-int v0, v15

    move/from16 v18, v0

    aput v18, v2, v3

    const/4 v3, 0x1

    float-to-int v0, v7

    move/from16 v18, v0

    aput v18, v2, v3

    invoke-virtual {v8, v2}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 145
    new-instance v2, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$1;-><init>(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)V

    invoke-virtual {v8, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 153
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    .end local v8    # "animator":Landroid/animation/ValueAnimator;
    :cond_3
    float-to-double v2, v15

    const-wide v18, 0x3f947ae147ae147bL    # 0.02

    cmpg-double v2, v2, v18

    if-gez v2, :cond_5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mXTweenPosition:I

    int-to-float v4, v2

    .line 159
    .local v4, "startX":F
    :goto_1
    const/4 v11, 0x0

    .line 160
    .local v11, "finishX":F
    const v2, 0x3ca3d70a    # 0.02f

    cmpl-float v2, v7, v2

    if-lez v2, :cond_6

    const v2, 0x3ca3d70a    # 0.02f

    cmpg-float v2, v15, v2

    if-gez v2, :cond_6

    .line 161
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v12, v2, :cond_6

    .line 162
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/view/View;

    .line 163
    .local v16, "targetView":Landroid/view/View;
    new-instance v8, Landroid/animation/ValueAnimator;

    invoke-direct {v8}, Landroid/animation/ValueAnimator;-><init>()V

    .line 164
    .restart local v8    # "animator":Landroid/animation/ValueAnimator;
    sget v2, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->BOTTOM_FRAME_ANIMATION_DURATION_MS:I

    int-to-long v2, v2

    invoke-virtual {v8, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 165
    mul-int/lit8 v2, v12, 0x64

    int-to-long v2, v2

    invoke-virtual {v8, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 166
    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 167
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v4, v2, v3

    const/4 v3, 0x1

    const/16 v18, 0x0

    aput v18, v2, v3

    invoke-virtual {v8, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 168
    new-instance v2, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$2;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v2, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$2;-><init>(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;Landroid/view/View;)V

    invoke-virtual {v8, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 176
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 120
    .end local v4    # "startX":F
    .end local v7    # "finishHeight":F
    .end local v8    # "animator":Landroid/animation/ValueAnimator;
    .end local v9    # "animatorSet":Landroid/animation/AnimatorSet;
    .end local v10    # "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    .end local v11    # "finishX":F
    .end local v12    # "i":I
    .end local v13    # "sequenceDelay":I
    .end local v14    # "sourceView":Landroid/view/View;
    .end local v16    # "targetView":Landroid/view/View;
    .end local v17    # "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 158
    .restart local v7    # "finishHeight":F
    .restart local v9    # "animatorSet":Landroid/animation/AnimatorSet;
    .restart local v10    # "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    .restart local v13    # "sequenceDelay":I
    .restart local v14    # "sourceView":Landroid/view/View;
    .restart local v17    # "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    :cond_5
    const/4 v4, 0x0

    goto :goto_1

    .line 184
    .restart local v4    # "startX":F
    .restart local v11    # "finishX":F
    :cond_6
    float-to-double v2, v7

    const-wide v18, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v2, v2, v18

    if-lez v2, :cond_7

    const/4 v5, 0x0

    .line 185
    .local v5, "startAlpha":F
    :goto_3
    float-to-double v2, v7

    const-wide v18, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v2, v2, v18

    if-lez v2, :cond_8

    const/high16 v6, 0x3f800000    # 1.0f

    .line 186
    .local v6, "finishAlpha":F
    :goto_4
    const v2, 0x3ca3d70a    # 0.02f

    cmpl-float v2, v7, v2

    if-lez v2, :cond_9

    const v2, 0x3ca3d70a    # 0.02f

    cmpg-float v2, v15, v2

    if-gez v2, :cond_9

    .line 187
    const/4 v12, 0x0

    .restart local v12    # "i":I
    :goto_5
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v12, v2, :cond_9

    .line 188
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/view/View;

    .line 189
    .restart local v16    # "targetView":Landroid/view/View;
    new-instance v8, Landroid/animation/ValueAnimator;

    invoke-direct {v8}, Landroid/animation/ValueAnimator;-><init>()V

    .line 190
    .restart local v8    # "animator":Landroid/animation/ValueAnimator;
    sget v2, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->BOTTOM_FRAME_ANIMATION_DURATION_MS:I

    int-to-long v2, v2

    invoke-virtual {v8, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 191
    mul-int/lit8 v2, v12, 0x64

    int-to-long v2, v2

    invoke-virtual {v8, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 192
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v5, v2, v3

    const/4 v3, 0x1

    aput v6, v2, v3

    invoke-virtual {v8, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 193
    new-instance v2, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$3;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v2, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$3;-><init>(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;Landroid/view/View;)V

    invoke-virtual {v8, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 200
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    .line 184
    .end local v5    # "startAlpha":F
    .end local v6    # "finishAlpha":F
    .end local v8    # "animator":Landroid/animation/ValueAnimator;
    .end local v12    # "i":I
    .end local v16    # "targetView":Landroid/view/View;
    :cond_7
    const/high16 v5, 0x3f800000    # 1.0f

    goto :goto_3

    .line 185
    .restart local v5    # "startAlpha":F
    :cond_8
    const/4 v6, 0x0

    goto :goto_4

    .line 203
    .restart local v6    # "finishAlpha":F
    :cond_9
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_a

    .line 205
    new-instance v2, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;-><init>(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;FFFF)V

    invoke-virtual {v9, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 236
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrameAnimation:Landroid/animation/AnimatorSet;

    .line 237
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    float-to-int v3, v15

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->requestLayout()V

    .line 240
    invoke-virtual {v9, v10}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 241
    new-instance v2, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->mEasingExponent:F

    sget-object v18, Lcom/microsoft/xbox/toolkit/anim/EasingMode;->EaseOut:Lcom/microsoft/xbox/toolkit/anim/EasingMode;

    move-object/from16 v0, v18

    invoke-direct {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;-><init>(FLcom/microsoft/xbox/toolkit/anim/EasingMode;)V

    invoke-virtual {v9, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 242
    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->start()V

    .line 246
    :cond_a
    const/4 v2, 0x1

    return v2
.end method
