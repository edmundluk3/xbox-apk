.class abstract Lcom/microsoft/xbox/service/rta/$AutoValue_RtaDataTypes_RtaMpsdSubscription;
.super Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;
.source "$AutoValue_RtaDataTypes_RtaMpsdSubscription.java"


# instance fields
.field private final connectionId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "connectionId"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;-><init>()V

    .line 15
    if-nez p1, :cond_0

    .line 16
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null connectionId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/rta/$AutoValue_RtaDataTypes_RtaMpsdSubscription;->connectionId:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public connectionId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConnectionId"
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/$AutoValue_RtaDataTypes_RtaMpsdSubscription;->connectionId:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 37
    if-ne p1, p0, :cond_0

    .line 38
    const/4 v1, 0x1

    .line 44
    :goto_0
    return v1

    .line 40
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 41
    check-cast v0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;

    .line 42
    .local v0, "that":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/$AutoValue_RtaDataTypes_RtaMpsdSubscription;->connectionId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;->connectionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 44
    .end local v0    # "that":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 49
    const/4 v0, 0x1

    .line 50
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/$AutoValue_RtaDataTypes_RtaMpsdSubscription;->connectionId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 52
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RtaMpsdSubscription{connectionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/$AutoValue_RtaDataTypes_RtaMpsdSubscription;->connectionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
