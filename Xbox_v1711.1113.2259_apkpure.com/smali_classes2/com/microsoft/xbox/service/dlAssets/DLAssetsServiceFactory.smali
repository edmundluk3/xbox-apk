.class public final Lcom/microsoft/xbox/service/dlAssets/DLAssetsServiceFactory;
.super Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory;
.source "DLAssetsServiceFactory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory",
        "<",
        "Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;",
        ">;"
    }
.end annotation


# static fields
.field private static final BASE_URL:Ljava/lang/String; = "https://dlassets-ssl.xboxlive.com/public/content/ppl/"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory;-><init>()V

    return-void
.end method

.method private getClient()Lokhttp3/OkHttpClient;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 33
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory;->getDefaultLoggingInterceptor()Lokhttp3/Interceptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 32
    return-object v0
.end method


# virtual methods
.method public getRetrofit()Lretrofit2/Retrofit;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 20
    const-string v0, "https://dlassets-ssl.xboxlive.com/public/content/ppl/"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsServiceFactory;->getDefaultRetrofitBuilder(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsServiceFactory;->getClient()Lokhttp3/OkHttpClient;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v0

    .line 20
    return-object v0
.end method

.method public getStubInstance(Lretrofit2/mock/BehaviorDelegate;)Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/mock/BehaviorDelegate",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;",
            ">;)",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "delegate":Lretrofit2/mock/BehaviorDelegate;, "Lretrofit2/mock/BehaviorDelegate<Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic getStubInstance(Lretrofit2/mock/BehaviorDelegate;)Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsServiceFactory;->getStubInstance(Lretrofit2/mock/BehaviorDelegate;)Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;

    move-result-object v0

    return-object v0
.end method
