.class public Lxbox/smartglass/ConsoleDisconnect;
.super LMicrosoft/Telemetry/Data;
.source "ConsoleDisconnect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxbox/smartglass/ConsoleDisconnect$Schema;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LMicrosoft/Telemetry/Data",
        "<",
        "Lxbox/smartglass/CommonData;",
        ">;"
    }
.end annotation


# instance fields
.field private duration:I

.field private mode:I

.field private sessionId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0}, LMicrosoft/Telemetry/Data;-><init>()V

    .line 233
    return-void
.end method

.method public static getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 226
    sget-object v0, Lxbox/smartglass/ConsoleDisconnect$Schema;->schemaDef:Lcom/microsoft/bond/SchemaDef;

    return-object v0
.end method


# virtual methods
.method public clone()Lcom/microsoft/bond/BondSerializable;
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lxbox/smartglass/ConsoleDisconnect;->clone()Lcom/microsoft/bond/BondSerializable;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Lcom/microsoft/bond/StructDef;)Lcom/microsoft/bond/BondMirror;
    .locals 1
    .param p1, "structDef"    # Lcom/microsoft/bond/StructDef;

    .prologue
    .line 210
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getDuration()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lxbox/smartglass/ConsoleDisconnect;->duration:I

    return v0
.end method

.method public getField(Lcom/microsoft/bond/FieldDef;)Ljava/lang/Object;
    .locals 1
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;

    .prologue
    .line 175
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 183
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 177
    :sswitch_0
    iget-object v0, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    goto :goto_0

    .line 179
    :sswitch_1
    iget v0, p0, Lxbox/smartglass/ConsoleDisconnect;->duration:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 181
    :sswitch_2
    iget v0, p0, Lxbox/smartglass/ConsoleDisconnect;->mode:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 175
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
    .end sparse-switch
.end method

.method public final getMode()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lxbox/smartglass/ConsoleDisconnect;->mode:I

    return v0
.end method

.method public getSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 218
    invoke-static {}, Lxbox/smartglass/ConsoleDisconnect;->getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;

    move-result-object v0

    return-object v0
.end method

.method public final getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    return-object v0
.end method

.method public marshal(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 0
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 374
    invoke-static {p0, p1}, Lcom/microsoft/bond/internal/Marshaler;->marshal(Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/ProtocolWriter;)V

    .line 375
    return-void
.end method

.method public memberwiseCompare(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 417
    if-nez p1, :cond_1

    .line 423
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 421
    check-cast v0, Lxbox/smartglass/ConsoleDisconnect;

    .line 423
    .local v0, "that":Lxbox/smartglass/ConsoleDisconnect;
    invoke-virtual {p0, v0}, Lxbox/smartglass/ConsoleDisconnect;->memberwiseCompareQuick(Lxbox/smartglass/ConsoleDisconnect;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lxbox/smartglass/ConsoleDisconnect;->memberwiseCompareDeep(Lxbox/smartglass/ConsoleDisconnect;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected memberwiseCompareDeep(Lxbox/smartglass/ConsoleDisconnect;)Z
    .locals 5
    .param p1, "that"    # Lxbox/smartglass/ConsoleDisconnect;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 437
    const/4 v0, 0x1

    .line 438
    .local v0, "equals":Z
    if-eqz v0, :cond_1

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareDeep(LMicrosoft/Telemetry/Data;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 439
    :goto_0
    if-eqz v0, :cond_3

    iget-object v3, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    if-nez v3, :cond_2

    :cond_0
    move v0, v1

    .line 440
    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 438
    goto :goto_0

    .line 439
    :cond_2
    iget-object v3, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method protected memberwiseCompareQuick(Lxbox/smartglass/ConsoleDisconnect;)Z
    .locals 5
    .param p1, "that"    # Lxbox/smartglass/ConsoleDisconnect;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 427
    const/4 v0, 0x1

    .line 428
    .local v0, "equals":Z
    if-eqz v0, :cond_1

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareQuick(LMicrosoft/Telemetry/Data;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v0, v2

    .line 429
    :goto_0
    if-eqz v0, :cond_4

    iget-object v1, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    iget-object v4, p1, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    if-nez v4, :cond_3

    move v4, v2

    :goto_2
    if-ne v1, v4, :cond_4

    move v0, v2

    .line 430
    :goto_3
    if-eqz v0, :cond_6

    iget-object v1, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    if-nez v1, :cond_5

    :cond_0
    move v0, v2

    .line 431
    :goto_4
    if-eqz v0, :cond_7

    iget v1, p0, Lxbox/smartglass/ConsoleDisconnect;->duration:I

    iget v4, p1, Lxbox/smartglass/ConsoleDisconnect;->duration:I

    if-ne v1, v4, :cond_7

    move v0, v2

    .line 432
    :goto_5
    if-eqz v0, :cond_8

    iget v1, p0, Lxbox/smartglass/ConsoleDisconnect;->mode:I

    iget v4, p1, Lxbox/smartglass/ConsoleDisconnect;->mode:I

    if-ne v1, v4, :cond_8

    move v0, v2

    .line 433
    :goto_6
    return v0

    :cond_1
    move v0, v3

    .line 428
    goto :goto_0

    :cond_2
    move v1, v3

    .line 429
    goto :goto_1

    :cond_3
    move v4, v3

    goto :goto_2

    :cond_4
    move v0, v3

    goto :goto_3

    .line 430
    :cond_5
    iget-object v1, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_0

    :cond_6
    move v0, v3

    goto :goto_4

    :cond_7
    move v0, v3

    .line 431
    goto :goto_5

    :cond_8
    move v0, v3

    .line 432
    goto :goto_6
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 267
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readBegin()V

    .line 268
    invoke-virtual {p0, p1}, Lxbox/smartglass/ConsoleDisconnect;->readNested(Lcom/microsoft/bond/ProtocolReader;)V

    .line 269
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readEnd()V

    .line 270
    return-void
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    return-void
.end method

.method public readNested(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 276
    sget-object v0, Lcom/microsoft/bond/ProtocolCapability;->TAGGED:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v0}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 277
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/ConsoleDisconnect;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/ConsoleDisconnect;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    invoke-static {p1}, Lcom/microsoft/bond/internal/ReadHelper;->skipPartialStruct(Lcom/microsoft/bond/ProtocolReader;)V

    goto :goto_0
.end method

.method protected readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z
    .locals 5
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 325
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 327
    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 366
    :goto_0
    return v2

    .line 344
    .local v0, "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :sswitch_0
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    .line 360
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldEnd()V

    .line 334
    .end local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldBegin()Lcom/microsoft/bond/ProtocolReader$FieldTag;

    move-result-object v0

    .line 336
    .restart local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP:Lcom/microsoft/bond/BondDataType;

    if-eq v3, v4, :cond_1

    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_3

    .line 338
    :cond_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_2

    .line 363
    .local v1, "isPartial":Z
    :goto_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    move v2, v1

    .line 366
    goto :goto_0

    .end local v1    # "isPartial":Z
    :cond_2
    move v1, v2

    .line 338
    goto :goto_2

    .line 342
    :cond_3
    iget v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->id:I

    sparse-switch v3, :sswitch_data_0

    .line 356
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-virtual {p1, v3}, Lcom/microsoft/bond/ProtocolReader;->skip(Lcom/microsoft/bond/BondDataType;)V

    goto :goto_1

    .line 348
    :sswitch_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/ConsoleDisconnect;->duration:I

    goto :goto_1

    .line 352
    :sswitch_2
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/ConsoleDisconnect;->mode:I

    goto :goto_1

    .line 342
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
    .end sparse-switch
.end method

.method protected readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 291
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 293
    .local v0, "canOmitFields":Z
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 294
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 296
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 297
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    .line 304
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_3

    .line 305
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/ConsoleDisconnect;->duration:I

    .line 312
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_5

    .line 313
    :cond_4
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/ConsoleDisconnect;->mode:I

    .line 319
    :cond_5
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    .line 320
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 239
    const-string v0, "ConsoleDisconnect"

    const-string v1, "xbox.smartglass.ConsoleDisconnect"

    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/ConsoleDisconnect;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    return-void
.end method

.method protected reset(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "qualifiedName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 243
    invoke-super {p0, p1, p2}, LMicrosoft/Telemetry/Data;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    .line 245
    iput v1, p0, Lxbox/smartglass/ConsoleDisconnect;->duration:I

    .line 246
    iput v1, p0, Lxbox/smartglass/ConsoleDisconnect;->mode:I

    .line 247
    return-void
.end method

.method public final setDuration(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 63
    iput p1, p0, Lxbox/smartglass/ConsoleDisconnect;->duration:I

    .line 64
    return-void
.end method

.method public setField(Lcom/microsoft/bond/FieldDef;Ljava/lang/Object;)V
    .locals 1
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 192
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 203
    .end local p2    # "value":Ljava/lang/Object;
    :goto_0
    return-void

    .line 194
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_0
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    goto :goto_0

    .line 197
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_1
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/ConsoleDisconnect;->duration:I

    goto :goto_0

    .line 200
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_2
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/ConsoleDisconnect;->mode:I

    goto :goto_0

    .line 192
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
    .end sparse-switch
.end method

.method public final setMode(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 77
    iput p1, p0, Lxbox/smartglass/ConsoleDisconnect;->mode:I

    .line 78
    return-void
.end method

.method public final setSessionId(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 253
    invoke-static {p1, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V

    .line 254
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 260
    check-cast p2, Lcom/microsoft/bond/SchemaDef;

    .end local p2    # "schema":Lcom/microsoft/bond/BondSerializable;
    invoke-static {p1, p2, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/BondSerializable;)V

    .line 261
    return-void
.end method

.method public write(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 2
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 381
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeBegin()V

    .line 383
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->getFirstPassWriter()Lcom/microsoft/bond/ProtocolWriter;

    move-result-object v0

    .local v0, "firstPassWriter":Lcom/microsoft/bond/ProtocolWriter;
    if-eqz v0, :cond_0

    .line 385
    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/ConsoleDisconnect;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 386
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/ConsoleDisconnect;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 392
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeEnd()V

    .line 393
    return-void

    .line 390
    :cond_0
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/ConsoleDisconnect;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    goto :goto_0
.end method

.method public writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V
    .locals 4
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 396
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 397
    .local v0, "canOmitFields":Z
    sget-object v1, Lxbox/smartglass/ConsoleDisconnect$Schema;->metadata:Lcom/microsoft/bond/Metadata;

    invoke-virtual {p1, v1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructBegin(Lcom/microsoft/bond/BondSerializable;Z)V

    .line 398
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 400
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0xa

    invoke-static {}, Lxbox/smartglass/ConsoleDisconnect$Schema;->access$000()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 401
    iget-object v1, p0, Lxbox/smartglass/ConsoleDisconnect;->sessionId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 402
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 404
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x14

    invoke-static {}, Lxbox/smartglass/ConsoleDisconnect$Schema;->access$100()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 405
    iget v1, p0, Lxbox/smartglass/ConsoleDisconnect;->duration:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 406
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 408
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x1e

    invoke-static {}, Lxbox/smartglass/ConsoleDisconnect$Schema;->access$200()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 409
    iget v1, p0, Lxbox/smartglass/ConsoleDisconnect;->mode:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 410
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 412
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructEnd(Z)V

    .line 413
    return-void
.end method
