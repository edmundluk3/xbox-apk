.class Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "SettingsGeneralPageViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditFirstNameAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private firstName:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "firstName"    # Ljava/lang/String;

    .prologue
    .line 552
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 553
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->firstName:Ljava/lang/String;

    .line 554
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 558
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .line 559
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFirstName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .line 560
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFirstName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->firstName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 558
    :goto_0
    return v0

    .line 560
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 586
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 587
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 588
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->firstName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->editFirstName(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 589
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_1

    .line 595
    :cond_0
    :goto_0
    return-object v0

    .line 592
    :cond_1
    const-string v1, "SettingsActivityViewModel"

    const-string v2, "Failed to edit first name."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 576
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$600(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 566
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 581
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$600(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 582
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 549
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 570
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 571
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$702(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Z)Z

    .line 572
    return-void
.end method
