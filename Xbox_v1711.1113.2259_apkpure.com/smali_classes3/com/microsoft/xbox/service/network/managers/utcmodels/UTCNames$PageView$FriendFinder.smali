.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView$FriendFinder;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FriendFinder"
.end annotation


# static fields
.field public static final ContactsAddPhoneView:Ljava/lang/String; = "Friend Finder - Contacts Add Phone View"

.field public static final ContactsChangeRegionView:Ljava/lang/String; = "Friend Finder - Contacts Change Region View"

.field public static final ContactsErrorView:Ljava/lang/String; = "Friend Finder - Contacts Error View"

.field public static final ContactsFindFriendsView:Ljava/lang/String; = "Friend Finder - Contacts Find Friends View"

.field public static final ContactsInviteFriendsView:Ljava/lang/String; = "Friend Finder - Contacts Invite Friends View"

.field public static final ContactsOptInView:Ljava/lang/String; = "Friend Finder - Contacts Opt In View"

.field public static final ContactsVerifyPhoneView:Ljava/lang/String; = "Friend Finder - Contacts Verify Phone View"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
