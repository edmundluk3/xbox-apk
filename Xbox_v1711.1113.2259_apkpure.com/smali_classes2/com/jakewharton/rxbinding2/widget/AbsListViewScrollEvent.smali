.class public abstract Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;
.super Ljava/lang/Object;
.source "AbsListViewScrollEvent.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static create(Landroid/widget/AbsListView;IIII)Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;
    .locals 6
    .param p0, "listView"    # Landroid/widget/AbsListView;
    .param p1, "scrollState"    # I
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 13
    new-instance v0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AbsListViewScrollEvent;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jakewharton/rxbinding2/widget/AutoValue_AbsListViewScrollEvent;-><init>(Landroid/widget/AbsListView;IIII)V

    return-object v0
.end method


# virtual methods
.method public abstract firstVisibleItem()I
.end method

.method public abstract scrollState()I
.end method

.method public abstract totalItemCount()I
.end method

.method public abstract view()Landroid/widget/AbsListView;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract visibleItemCount()I
.end method
