.class public Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "ClubWhosHereScreenAdapter.java"


# static fields
.field private static final SERVICE_SEARCH_DELAY_MS:J = 0x1f4L


# instance fields
.field private final filterSpinner:Landroid/widget/Spinner;

.field private final filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final invalidScreenReason:Landroid/widget/TextView;

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

.field private final searchClearButton:Landroid/widget/Button;

.field private final searchInput:Landroid/widget/EditText;

.field private final searchSection:Landroid/view/View;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final tooManyWarningText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;)V
    .locals 5
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 47
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 48
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    .line 50
    const v1, 0x7f0e03da

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->screenBody:Landroid/view/View;

    .line 51
    const v1, 0x7f0e03db

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->searchSection:Landroid/view/View;

    .line 52
    const v1, 0x7f0e0a95

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 53
    const v1, 0x7f0e03dd

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->searchInput:Landroid/widget/EditText;

    .line 54
    const v1, 0x7f0e03de

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->searchClearButton:Landroid/widget/Button;

    .line 55
    const v1, 0x7f0e03df

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 56
    const v1, 0x7f0e03e0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->tooManyWarningText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 57
    const v1, 0x7f0e02b5

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->invalidScreenReason:Landroid/widget/TextView;

    .line 58
    const v1, 0x7f0e03e1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 60
    const v1, 0x7f0e06bd

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 61
    .local v0, "noContent":Landroid/widget/TextView;
    const v1, 0x7f070b62

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->searchClearButton:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    .line 66
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f03009e

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 75
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 76
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x1090008

    .line 78
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getSpinnerArrayItems()Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v2, 0x7f03020a

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->searchInput:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;
    .param p1, "listItem"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .prologue
    .line 69
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->navigateToProfile(Ljava/lang/String;)V

    .line 72
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;
    .param p1, "changeEvent"    # Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p1}, Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;->text()Ljava/lang/CharSequence;

    move-result-object v0

    .line 106
    .local v0, "text":Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 107
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->searchClearButton:Landroid/widget/Button;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 108
    return-void

    .line 107
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;
    .param p1, "changeEvent"    # Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    invoke-virtual {p1}, Lcom/jakewharton/rxbinding2/widget/TextViewTextChangeEvent;->text()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->setSearchTerm(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 85
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStart()V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->searchInput:Landroid/widget/EditText;

    .line 102
    invoke-static {v1}, Lcom/jakewharton/rxbinding2/widget/RxTextView;->textChangeEvents(Landroid/widget/TextView;)Lcom/jakewharton/rxbinding2/InitialValueObservable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 104
    invoke-virtual {v1, v2}, Lcom/jakewharton/rxbinding2/InitialValueObservable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 113
    invoke-virtual {v1, v2, v3, v4}, Lio/reactivex/Observable;->debounce(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v1

    .line 115
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 116
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 101
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 118
    return-void
.end method

.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->updateLoadingIndicator(Z)V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->searchSection:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->showSearchSection()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->searchInput:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getSearchHint()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->clear()V

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getUserData()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->addAll(Ljava/util/List;)V

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->notifyDataSetChanged()V

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->tooManyWarningText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->showTooManyWarning()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->invalidScreenReason:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getInvalidReasonText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    return-void
.end method
