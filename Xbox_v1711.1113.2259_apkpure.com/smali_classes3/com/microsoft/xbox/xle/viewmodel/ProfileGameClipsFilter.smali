.class public final enum Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;
.super Ljava/lang/Enum;
.source "ProfileGameClipsFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

.field public static final enum GameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

.field public static final enum GameClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    const-string v1, "GameClipsRecent"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;->GameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    const-string v1, "GameClipsSaved"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;->GameClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    .line 6
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;->GameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;->GameClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getTitles(Z)[Ljava/lang/String;
    .locals 5
    .param p0, "isMyProfile"    # Z

    .prologue
    const/4 v0, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10
    if-eqz p0, :cond_0

    .line 11
    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07059f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705a0

    .line 12
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 14
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070abe

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070abf

    .line 15
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/ProfileGameClipsFilter;

    return-object v0
.end method
