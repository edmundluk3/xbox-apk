.class public abstract Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.super Ljava/lang/Object;
.source "XLEAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Result:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected cancelled:Z

.field private chainedTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

.field private doBackgroundAndPostExecuteRunnable:Ljava/lang/Runnable;

.field private executorService:Ljava/util/concurrent/ExecutorService;

.field protected isBusy:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 2
    .param p1, "executorService"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<TResult;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->doBackgroundAndPostExecuteRunnable:Ljava/lang/Runnable;

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 28
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->cancelled:Z

    .line 29
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->isBusy:Z

    .line 30
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->chainedTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 36
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/XLEAsyncTask;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->doBackgroundAndPostExecuteRunnable:Ljava/lang/Runnable;

    .line 54
    return-void
.end method

.method public static varargs executeAll([Lcom/microsoft/xbox/toolkit/XLEAsyncTask;)V
    .locals 3
    .param p0, "tasks"    # [Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    .prologue
    .line 81
    array-length v1, p0

    if-lez v1, :cond_1

    .line 83
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 84
    aget-object v1, p0, v0

    add-int/lit8 v2, v0, 0x1

    aget-object v2, p0, v2

    iput-object v2, v1, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->chainedTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_0
    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->execute()V

    .line 89
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/toolkit/XLEAsyncTask;)V
    .locals 3

    .prologue
    .line 38
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<TResult;>;"
    const/4 v1, 0x0

    .line 39
    .local v1, "result":Ljava/lang/Object;, "TResult;"
    iget-boolean v2, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->cancelled:Z

    if-nez v2, :cond_0

    .line 40
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->doInBackground()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    .line 44
    .end local v1    # "result":Ljava/lang/Object;, "TResult;"
    .local v0, "r":Ljava/lang/Object;, "TResult;"
    .local v0, "result":Ljava/lang/Object;, "TResult;"
    :goto_0
    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/XLEAsyncTask;Ljava/lang/Object;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 53
    return-void

    .end local v0    # "result":Ljava/lang/Object;, "TResult;"
    .restart local v1    # "result":Ljava/lang/Object;, "TResult;"
    :cond_0
    move-object v0, v1

    .end local v1    # "result":Ljava/lang/Object;, "TResult;"
    .local v0, "result":Ljava/lang/Object;, "TResult;"
    goto :goto_0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/toolkit/XLEAsyncTask;Ljava/lang/Object;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/Object;

    .prologue
    .line 45
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<TResult;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->isBusy:Z

    .line 46
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->cancelled:Z

    if-nez v0, :cond_0

    .line 47
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->chainedTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->chainedTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->execute()V

    .line 52
    :cond_0
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<TResult;>;"
    const/4 v1, 0x1

    .line 63
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 64
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->cancelled:Z

    .line 65
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract doInBackground()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResult;"
        }
    .end annotation
.end method

.method public execute()V
    .locals 4

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<TResult;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 68
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 69
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->cancelled:Z

    .line 70
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->isBusy:Z

    .line 71
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->onPreExecute()V

    .line 72
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->executeBackground()V

    .line 73
    return-void

    :cond_0
    move v0, v2

    .line 68
    goto :goto_0
.end method

.method protected executeBackground()V
    .locals 2

    .prologue
    .line 92
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<TResult;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->cancelled:Z

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->executorService:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->doBackgroundAndPostExecuteRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 94
    return-void
.end method

.method public getIsBusy()Z
    .locals 1

    .prologue
    .line 77
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<TResult;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->isBusy:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->cancelled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract onPostExecute(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation
.end method

.method protected abstract onPreExecute()V
.end method
