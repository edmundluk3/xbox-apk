.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam;
.super Ljava/lang/Object;
.source "UTCBeam.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackLaunchBeamStoreListing()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam$1;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 23
    return-void
.end method

.method public static trackLaunchBeamStreamInApp(I)V
    .locals 1
    .param p0, "streamId"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 30
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam$2;-><init>(I)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 41
    return-void
.end method

.method public static trackLaunchBeamStreamInWeb(I)V
    .locals 1
    .param p0, "streamId"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 48
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam$3;-><init>(I)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 59
    return-void
.end method
