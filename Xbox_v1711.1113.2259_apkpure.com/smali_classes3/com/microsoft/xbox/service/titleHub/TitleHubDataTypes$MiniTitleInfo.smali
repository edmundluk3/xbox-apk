.class public final Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;
.super Ljava/lang/Object;
.source "TitleHubDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MiniTitleInfo"
.end annotation


# instance fields
.field public final displayImage:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private volatile transient hashCode:I

.field public final name:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final serviceConfigId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final titleId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "displayImage"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "serviceConfigId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "titleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 645
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 646
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 647
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 648
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 650
    iput-object p1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->displayImage:Ljava/lang/String;

    .line 651
    iput-object p2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->name:Ljava/lang/String;

    .line 652
    iput-object p3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->serviceConfigId:Ljava/lang/String;

    .line 653
    iput-object p4, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->titleId:Ljava/lang/String;

    .line 654
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 658
    if-ne p1, p0, :cond_1

    .line 664
    :cond_0
    :goto_0
    return v1

    .line 660
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;

    if-nez v3, :cond_2

    move v1, v2

    .line 661
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 663
    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;

    .line 664
    .local v0, "that":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->displayImage:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->displayImage:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->name:Ljava/lang/String;

    .line 665
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->serviceConfigId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->serviceConfigId:Ljava/lang/String;

    .line 666
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->titleId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->titleId:Ljava/lang/String;

    .line 667
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getBoxArt()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 709
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->displayImage:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 703
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getScid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 697
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->serviceConfigId:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 691
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->titleId:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 673
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->hashCode:I

    if-nez v0, :cond_0

    .line 674
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->hashCode:I

    .line 675
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->displayImage:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->hashCode:I

    .line 676
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->hashCode:I

    .line 677
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->serviceConfigId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->hashCode:I

    .line 678
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->titleId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->hashCode:I

    .line 681
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 686
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
