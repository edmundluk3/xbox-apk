.class Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM$1;
.super Ljava/lang/Object;
.source "HomeScreenPopupScreenViewModelNPM.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
        "<",
        "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;

    .prologue
    .line 14
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHasState()Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;->computeHasState()Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderData()Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;->access$000(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;)Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getHeaderData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM$1;->getHeaderData()Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    move-result-object v0

    return-object v0
.end method

.method public getPlaceholderRid()I
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->getPlaceholderRid(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)I

    move-result v0

    return v0
.end method
