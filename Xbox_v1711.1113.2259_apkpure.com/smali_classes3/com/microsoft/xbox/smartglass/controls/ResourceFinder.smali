.class public Lcom/microsoft/xbox/smartglass/controls/ResourceFinder;
.super Ljava/lang/Object;
.source "ResourceFinder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static find(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 12
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    .line 14
    const/4 v6, 0x0

    .line 15
    .local v6, "resourceClass":Ljava/lang/Class;
    const/4 v5, -0x1

    .line 18
    .local v5, "id":I
    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".R"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    .line 20
    invoke-virtual {v6}, Ljava/lang/Class;->getClasses()[Ljava/lang/Class;

    move-result-object v2

    .line 21
    .local v2, "classes":[Ljava/lang/Class;
    const/4 v3, 0x0

    .line 23
    .local v3, "desiredClass":Ljava/lang/Class;
    array-length v8, v2

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v8, :cond_0

    aget-object v0, v2, v7

    .line 24
    .local v0, "c":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "\\$"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 25
    .local v1, "classNameComponents":[Ljava/lang/String;
    array-length v9, v1

    if-le v9, v11, :cond_2

    const/4 v9, 0x1

    aget-object v9, v1, v9

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 26
    move-object v3, v0

    .line 31
    .end local v0    # "c":Ljava/lang/Class;
    .end local v1    # "classNameComponents":[Ljava/lang/String;
    :cond_0
    if-eqz v3, :cond_1

    .line 32
    invoke-virtual {v3, p2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 38
    .end local v2    # "classes":[Ljava/lang/Class;
    .end local v3    # "desiredClass":Ljava/lang/Class;
    :cond_1
    :goto_1
    return v5

    .line 23
    .restart local v0    # "c":Ljava/lang/Class;
    .restart local v1    # "classNameComponents":[Ljava/lang/String;
    .restart local v2    # "classes":[Ljava/lang/Class;
    .restart local v3    # "desiredClass":Ljava/lang/Class;
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 34
    .end local v0    # "c":Ljava/lang/Class;
    .end local v1    # "classNameComponents":[Ljava/lang/String;
    .end local v2    # "classes":[Ljava/lang/Class;
    .end local v3    # "desiredClass":Ljava/lang/Class;
    :catch_0
    move-exception v4

    .line 35
    .local v4, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to find UI resource: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Error:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v7, v8, v9}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    goto :goto_1
.end method
