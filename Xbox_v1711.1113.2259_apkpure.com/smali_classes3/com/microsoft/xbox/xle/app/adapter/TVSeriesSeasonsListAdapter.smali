.class public Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "TVSeriesSeasonsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 20
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 25
    move-object v2, p2

    .line 26
    .local v2, "v":Landroid/view/View;
    if-nez v2, :cond_3

    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 28
    .local v3, "vi":Landroid/view/LayoutInflater;
    const v5, 0x7f030254

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 29
    new-instance v4, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;

    invoke-direct {v4, v2}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;-><init>(Landroid/view/View;)V

    .line 30
    .local v4, "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;
    invoke-virtual {v2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 35
    .end local v3    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    .line 36
    .local v1, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
    if-eqz v1, :cond_2

    .line 37
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->getEpisodeTitleTextView()Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 38
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->getEpisodeTitleTextView()Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v5

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getDisplayTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    :cond_0
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->getEpisodeDescriptionTextView()Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 42
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->getEpisodeDescriptionTextView()Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v5

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    :cond_1
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->getEpisodeAirDateTextView()Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 46
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v0

    .line 47
    .local v0, "date":Ljava/util/Date;
    if-eqz v0, :cond_2

    .line 48
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->getEpisodeAirDateTextView()Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070c9d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getDateStringAsMonthDateYear(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    .end local v0    # "date":Ljava/util/Date;
    :cond_2
    return-object v2

    .line 32
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
    .end local v4    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;
    :cond_3
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;

    .restart local v4    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;
    goto :goto_0
.end method
