.class public Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "DLCDetailHeaderAdapter.java"


# instance fields
.field private gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

.field private gameTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private gameTitleSmallTextView:Landroid/widget/TextView;

.field private gameTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;

    .line 25
    const v0, 0x7f0e050b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->screenBody:Landroid/view/View;

    .line 27
    const v0, 0x7f0e050c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 28
    const v0, 0x7f0e050e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 29
    const v0, 0x7f0e050d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    .line 30
    const v0, 0x7f0e050f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameTitleSmallTextView:Landroid/widget/TextView;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;)Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;

    return-object v0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    :cond_0
    return-void
.end method

.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameTitleSmallTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->getAverageUserRating()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setAverageUserRating(F)V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setVisibility(I)V

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->gameTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 71
    :cond_1
    return-void
.end method
