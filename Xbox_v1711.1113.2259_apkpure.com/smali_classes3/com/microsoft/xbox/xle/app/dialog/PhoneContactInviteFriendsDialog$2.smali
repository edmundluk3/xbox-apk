.class Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$2;
.super Ljava/lang/Object;
.source "PhoneContactInviteFriendsDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$2;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsSendInvitation()V

    .line 98
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 99
    .local v1, "phoneNumbers":Ljava/lang/StringBuffer;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$2;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;

    .line 100
    .local v0, "contact":Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;
    iget-boolean v4, v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;->isSelected:Z

    if-eqz v4, :cond_0

    .line 101
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 102
    const/16 v4, 0x2c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 104
    :cond_1
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;->phoneNumbers:Ljava/util/ArrayList;

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 108
    .end local v0    # "contact":Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lez v4, :cond_4

    .line 109
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.SENDTO"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 110
    .local v2, "smsIntent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 111
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "smsto:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 112
    const-string v4, "sms_body"

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070e09

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const-string v4, "address"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$2;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v2, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 116
    .local v3, "smsResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 117
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$2;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 120
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$2;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->dismiss()V

    .line 124
    .end local v2    # "smsIntent":Landroid/content/Intent;
    .end local v3    # "smsResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :goto_1
    return-void

    .line 122
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v4

    const v5, 0x7f070e0b

    invoke-virtual {v4, v5, v7}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V

    goto :goto_1
.end method
