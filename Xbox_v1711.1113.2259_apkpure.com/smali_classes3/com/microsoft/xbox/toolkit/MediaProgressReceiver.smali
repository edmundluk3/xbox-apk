.class public Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaProgressReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/MediaProgressReceiver$IUpdate;
    }
.end annotation


# static fields
.field private static instance:Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;

.field private static updaters:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/toolkit/MediaProgressReceiver$IUpdate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;->instance:Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;

    .line 16
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;->updaters:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;->instance:Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 33
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 34
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "action":Ljava/lang/String;
    const-string v3, "com.microsoft.xbox.media.PROGRESS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 36
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 37
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 38
    sget-object v3, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;->updaters:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver$IUpdate;

    .line 39
    .local v2, "updater":Lcom/microsoft/xbox/toolkit/MediaProgressReceiver$IUpdate;
    new-instance v4, Lcom/microsoft/xbox/toolkit/MediaProgressData;

    invoke-direct {v4, v1}, Lcom/microsoft/xbox/toolkit/MediaProgressData;-><init>(Landroid/os/Bundle;)V

    invoke-interface {v2, v4}, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver$IUpdate;->update(Lcom/microsoft/xbox/toolkit/MediaProgressData;)V

    goto :goto_0

    .line 43
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "updater":Lcom/microsoft/xbox/toolkit/MediaProgressReceiver$IUpdate;
    :cond_0
    return-void
.end method

.method public register(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/MediaProgressReceiver$IUpdate;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "updater"    # Lcom/microsoft/xbox/toolkit/MediaProgressReceiver$IUpdate;

    .prologue
    .line 23
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 24
    sget-object v0, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;->updaters:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    return-void
.end method

.method public unRegister(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 29
    sget-object v0, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;->updaters:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method
