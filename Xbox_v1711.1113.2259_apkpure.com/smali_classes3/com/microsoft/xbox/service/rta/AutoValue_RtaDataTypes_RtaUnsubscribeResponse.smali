.class final Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;
.super Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;
.source "AutoValue_RtaDataTypes_RtaUnsubscribeResponse.java"


# instance fields
.field private final errorCode:I

.field private final payload:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

.field private final sequenceNumber:I

.field private final subscriptionId:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Ljava/lang/Integer;Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;II)V
    .locals 0
    .param p1, "subscriptionId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "payload"    # Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "sequenceNumber"    # I
    .param p4, "errorCode"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->subscriptionId:Ljava/lang/Integer;

    .line 21
    iput-object p2, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->payload:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    .line 22
    iput p3, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->sequenceNumber:I

    .line 23
    iput p4, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->errorCode:I

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    if-ne p1, p0, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 63
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 64
    check-cast v0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;

    .line 65
    .local v0, "that":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;
    iget-object v3, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->subscriptionId:Ljava/lang/Integer;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;->subscriptionId()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->payload:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    if-nez v3, :cond_4

    .line 66
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;->payload()Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget v3, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->sequenceNumber:I

    .line 67
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;->sequenceNumber()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->errorCode:I

    .line 68
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;->errorCode()I

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 65
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->subscriptionId:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;->subscriptionId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 66
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->payload:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;->payload()Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .end local v0    # "that":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;
    :cond_5
    move v1, v2

    .line 70
    goto :goto_0
.end method

.method public errorCode()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->errorCode:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 75
    const/4 v0, 0x1

    .line 76
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->subscriptionId:Ljava/lang/Integer;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 78
    mul-int/2addr v0, v3

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->payload:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    if-nez v1, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    .line 80
    mul-int/2addr v0, v3

    .line 81
    iget v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->sequenceNumber:I

    xor-int/2addr v0, v1

    .line 82
    mul-int/2addr v0, v3

    .line 83
    iget v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->errorCode:I

    xor-int/2addr v0, v1

    .line 84
    return v0

    .line 77
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->subscriptionId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    .line 79
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->payload:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public payload()Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->payload:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    return-object v0
.end method

.method public sequenceNumber()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->sequenceNumber:I

    return v0
.end method

.method public subscriptionId()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->subscriptionId:Ljava/lang/Integer;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RtaUnsubscribeResponse{subscriptionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->subscriptionId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", payload="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->payload:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sequenceNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->sequenceNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;->errorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
