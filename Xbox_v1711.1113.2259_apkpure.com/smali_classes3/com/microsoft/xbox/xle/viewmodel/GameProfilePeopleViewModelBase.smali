.class public abstract Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "GameProfilePeopleViewModelBase.java"


# instance fields
.field protected gameTitleId:J

.field protected peopleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 16
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 18
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->gameTitleId:J

    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->createAdapter()V

    .line 24
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 25
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 26
    .local v1, "selectedItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    :goto_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->gameTitleId:J

    .line 27
    return-void

    .line 26
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v2

    goto :goto_0
.end method

.method private createAdapter()V
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 71
    :cond_0
    return-void
.end method


# virtual methods
.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->peopleList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGameTitleId()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->gameTitleId:J

    return-wide v0
.end method

.method public getPeopleCount()I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->peopleList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->peopleList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public hidePeopleList()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    return v0
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->createAdapter()V

    .line 48
    return-void
.end method

.method protected updateViewModelState()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->peopleList:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 65
    :goto_0
    return-void

    .line 59
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 60
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->hidePeopleList()Z

    goto :goto_0

    .line 63
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method
