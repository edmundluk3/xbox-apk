.class Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;
.super Lcom/facebook/rebound/SimpleSpringListener;
.source "MinimizedArrangement.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipkart/chatheads/ui/MinimizedArrangement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;


# direct methods
.method constructor <init>(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)V
    .locals 0
    .param p1, "this$0"    # Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    .prologue
    .line 53
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-direct {p0}, Lcom/facebook/rebound/SimpleSpringListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onSpringAtRest(Lcom/facebook/rebound/Spring;)V
    .locals 4
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;"
    const-wide/16 v2, 0x0

    .line 62
    invoke-super {p0, p1}, Lcom/facebook/rebound/SimpleSpringListener;->onSpringAtRest(Lcom/facebook/rebound/Spring;)V

    .line 63
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$400(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$402(Lcom/flipkart/chatheads/ui/MinimizedArrangement;Z)Z

    .line 67
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 68
    invoke-virtual {p1, v2, v3}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 70
    :cond_1
    return-void
.end method

.method public onSpringUpdate(Lcom/facebook/rebound/Spring;)V
    .locals 4
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 56
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$500(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)Lcom/facebook/rebound/SpringChain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$500(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)Lcom/facebook/rebound/SpringChain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/rebound/SpringChain;->getControlSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 58
    :cond_0
    return-void
.end method
