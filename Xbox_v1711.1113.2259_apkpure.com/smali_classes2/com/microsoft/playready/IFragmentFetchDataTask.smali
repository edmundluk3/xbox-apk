.class public interface abstract Lcom/microsoft/playready/IFragmentFetchDataTask;
.super Ljava/lang/Object;
.source "IFragmentFetchDataTask.java"

# interfaces
.implements Ljava/util/concurrent/Future;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Future",
        "<[B>;"
    }
.end annotation


# virtual methods
.method public abstract addFragmentFetchDataListener(Lcom/microsoft/playready/IFragmentFetchDataListener;)V
.end method

.method public abstract fragmentData()[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract fragmentInfo()Lcom/microsoft/playready/FragmentIterator$FragmentInfo;
.end method

.method public abstract fragmentStreamRepresentation()Lcom/microsoft/playready/MediaRepresentation;
.end method

.method public abstract removeFragmentFetchDataListener(Lcom/microsoft/playready/IFragmentFetchDataListener;)V
.end method
