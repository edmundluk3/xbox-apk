.class public abstract Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;
.super Ljava/lang/Object;
.source "PartyListItems.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/party/PartyListItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PartyMemberListItem"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;Z)Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;
    .locals 1
    .param p0, "partyMember"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "userSummary"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .param p2, "canBeKicked"    # Z

    .prologue
    .line 36
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 37
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;Z)V

    return-object v0
.end method


# virtual methods
.method public abstract canBeKicked()Z
.end method

.method public abstract partyMember()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract userSummary()Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
