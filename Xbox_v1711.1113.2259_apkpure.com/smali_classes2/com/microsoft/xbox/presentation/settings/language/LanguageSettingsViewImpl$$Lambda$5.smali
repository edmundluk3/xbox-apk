.class final synthetic Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$5;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Predicate;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$5;->arg$1:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)Lio/reactivex/functions/Predicate;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$5;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$5;-><init>(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)V

    return-object v0
.end method


# virtual methods
.method public test(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl$$Lambda$5;->arg$1:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;

    check-cast p1, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->lambda$onCreateContentView$3(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)Z

    move-result v0

    return v0
.end method
