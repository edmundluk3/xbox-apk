.class final Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;
.super Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
.source "$AutoValue_OOBESettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private autoDST:Ljava/lang/Boolean;

.field private autoUpdateApps:Ljava/lang/Boolean;

.field private autoUpdateSystem:Ljava/lang/Boolean;

.field private instantOn:Ljava/lang/Boolean;

.field private timeZone:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;-><init>()V

    .line 119
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;-><init>()V

    .line 121
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoUpdateApps()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoUpdateApps:Ljava/lang/Boolean;

    .line 122
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoUpdateSystem()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoUpdateSystem:Ljava/lang/Boolean;

    .line 123
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->instantOn()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->instantOn:Ljava/lang/Boolean;

    .line 124
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoDST()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoDST:Ljava/lang/Boolean;

    .line 125
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->timeZone()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->timeZone:Ljava/lang/String;

    .line 126
    return-void
.end method


# virtual methods
.method public autoDST(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
    .locals 1
    .param p1, "autoDST"    # Z

    .prologue
    .line 144
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoDST:Ljava/lang/Boolean;

    .line 145
    return-object p0
.end method

.method public autoUpdateApps(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
    .locals 1
    .param p1, "autoUpdateApps"    # Z

    .prologue
    .line 129
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoUpdateApps:Ljava/lang/Boolean;

    .line 130
    return-object p0
.end method

.method public autoUpdateSystem(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
    .locals 1
    .param p1, "autoUpdateSystem"    # Z

    .prologue
    .line 134
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoUpdateSystem:Ljava/lang/Boolean;

    .line 135
    return-object p0
.end method

.method public build()Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 7

    .prologue
    .line 154
    const-string v6, ""

    .line 155
    .local v6, "missing":Ljava/lang/String;
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoUpdateApps:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " autoUpdateApps"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoUpdateSystem:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " autoUpdateSystem"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoDST:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " autoDST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->timeZone:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timeZone"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 167
    :cond_3
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 168
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing required properties:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_4
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoUpdateApps:Ljava/lang/Boolean;

    .line 171
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoUpdateSystem:Ljava/lang/Boolean;

    .line 172
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->instantOn:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->autoDST:Ljava/lang/Boolean;

    .line 174
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->timeZone:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings;-><init>(ZZLjava/lang/Boolean;ZLjava/lang/String;)V

    .line 170
    return-object v0
.end method

.method public instantOn(Ljava/lang/Boolean;)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
    .locals 0
    .param p1, "instantOn"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 139
    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->instantOn:Ljava/lang/Boolean;

    .line 140
    return-object p0
.end method

.method public timeZone(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
    .locals 0
    .param p1, "timeZone"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;->timeZone:Ljava/lang/String;

    .line 150
    return-object p0
.end method
