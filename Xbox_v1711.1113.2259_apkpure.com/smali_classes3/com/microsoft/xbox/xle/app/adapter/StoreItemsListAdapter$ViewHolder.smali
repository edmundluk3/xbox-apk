.class Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "StoreItemsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private goldScreenFullPrice:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private goldScreenReducedPrice:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private ratingWithUserCountView:Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;

.field private releaseTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private titleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$1;

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->titleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->titleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->releaseTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->releaseTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->ratingWithUserCountView:Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;)Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->ratingWithUserCountView:Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->goldScreenFullPrice:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->goldScreenFullPrice:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->goldScreenReducedPrice:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method
