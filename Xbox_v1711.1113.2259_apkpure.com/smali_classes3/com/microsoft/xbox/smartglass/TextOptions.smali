.class public final enum Lcom/microsoft/xbox/smartglass/TextOptions;
.super Ljava/lang/Enum;
.source "TextOptions.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/TextOptions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/TextOptions;

.field public static final enum AcceptsReturn:Lcom/microsoft/xbox/smartglass/TextOptions;

.field public static final enum Default:Lcom/microsoft/xbox/smartglass/TextOptions;

.field public static final enum Dismiss:Lcom/microsoft/xbox/smartglass/TextOptions;

.field public static final enum MultiLine:Lcom/microsoft/xbox/smartglass/TextOptions;

.field public static final enum Password:Lcom/microsoft/xbox/smartglass/TextOptions;

.field public static final enum PredictionEnabled:Lcom/microsoft/xbox/smartglass/TextOptions;

.field public static final enum RTL:Lcom/microsoft/xbox/smartglass/TextOptions;

.field public static final enum SpellCheckEnabled:Lcom/microsoft/xbox/smartglass/TextOptions;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/TextOptions;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextOptions;

    const-string v3, "Default"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/TextOptions;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->Default:Lcom/microsoft/xbox/smartglass/TextOptions;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextOptions;

    const-string v3, "AcceptsReturn"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/smartglass/TextOptions;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->AcceptsReturn:Lcom/microsoft/xbox/smartglass/TextOptions;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextOptions;

    const-string v3, "Password"

    invoke-direct {v2, v3, v7, v7}, Lcom/microsoft/xbox/smartglass/TextOptions;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->Password:Lcom/microsoft/xbox/smartglass/TextOptions;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextOptions;

    const-string v3, "MultiLine"

    invoke-direct {v2, v3, v9, v8}, Lcom/microsoft/xbox/smartglass/TextOptions;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->MultiLine:Lcom/microsoft/xbox/smartglass/TextOptions;

    .line 22
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextOptions;

    const-string v3, "SpellCheckEnabled"

    const/16 v4, 0x8

    invoke-direct {v2, v3, v8, v4}, Lcom/microsoft/xbox/smartglass/TextOptions;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->SpellCheckEnabled:Lcom/microsoft/xbox/smartglass/TextOptions;

    .line 24
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextOptions;

    const-string v3, "PredictionEnabled"

    const/4 v4, 0x5

    const/16 v5, 0x10

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextOptions;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->PredictionEnabled:Lcom/microsoft/xbox/smartglass/TextOptions;

    .line 26
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextOptions;

    const-string v3, "RTL"

    const/4 v4, 0x6

    const/16 v5, 0x20

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextOptions;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->RTL:Lcom/microsoft/xbox/smartglass/TextOptions;

    .line 28
    new-instance v2, Lcom/microsoft/xbox/smartglass/TextOptions;

    const-string v3, "Dismiss"

    const/4 v4, 0x7

    const/16 v5, 0x4000

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TextOptions;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->Dismiss:Lcom/microsoft/xbox/smartglass/TextOptions;

    .line 12
    const/16 v2, 0x8

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/TextOptions;

    sget-object v3, Lcom/microsoft/xbox/smartglass/TextOptions;->Default:Lcom/microsoft/xbox/smartglass/TextOptions;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/TextOptions;->AcceptsReturn:Lcom/microsoft/xbox/smartglass/TextOptions;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/smartglass/TextOptions;->Password:Lcom/microsoft/xbox/smartglass/TextOptions;

    aput-object v3, v2, v7

    sget-object v3, Lcom/microsoft/xbox/smartglass/TextOptions;->MultiLine:Lcom/microsoft/xbox/smartglass/TextOptions;

    aput-object v3, v2, v9

    sget-object v3, Lcom/microsoft/xbox/smartglass/TextOptions;->SpellCheckEnabled:Lcom/microsoft/xbox/smartglass/TextOptions;

    aput-object v3, v2, v8

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextOptions;->PredictionEnabled:Lcom/microsoft/xbox/smartglass/TextOptions;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextOptions;->RTL:Lcom/microsoft/xbox/smartglass/TextOptions;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/smartglass/TextOptions;->Dismiss:Lcom/microsoft/xbox/smartglass/TextOptions;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->$VALUES:[Lcom/microsoft/xbox/smartglass/TextOptions;

    .line 31
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->_lookup:Landroid/util/SparseArray;

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/smartglass/TextOptions;->values()[Lcom/microsoft/xbox/smartglass/TextOptions;

    move-result-object v2

    array-length v3, v2

    .local v0, "option":Lcom/microsoft/xbox/smartglass/TextOptions;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 35
    sget-object v4, Lcom/microsoft/xbox/smartglass/TextOptions;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/TextOptions;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 34
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 37
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput p3, p0, Lcom/microsoft/xbox/smartglass/TextOptions;->_value:I

    .line 41
    return-void
.end method

.method public static fromInt(I)Ljava/util/EnumSet;
    .locals 6
    .param p0, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/TextOptions;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    const-class v2, Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 58
    .local v1, "set":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/TextOptions;>;"
    invoke-static {}, Lcom/microsoft/xbox/smartglass/TextOptions;->values()[Lcom/microsoft/xbox/smartglass/TextOptions;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 59
    .local v0, "option":Lcom/microsoft/xbox/smartglass/TextOptions;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/TextOptions;->getValue()I

    move-result v5

    and-int/2addr v5, p0

    if-eqz v5, :cond_0

    .line 60
    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 58
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 64
    .end local v0    # "option":Lcom/microsoft/xbox/smartglass/TextOptions;
    :cond_1
    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/TextOptions;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/TextOptions;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/TextOptions;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/smartglass/TextOptions;->$VALUES:[Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/TextOptions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/TextOptions;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/microsoft/xbox/smartglass/TextOptions;->_value:I

    return v0
.end method
