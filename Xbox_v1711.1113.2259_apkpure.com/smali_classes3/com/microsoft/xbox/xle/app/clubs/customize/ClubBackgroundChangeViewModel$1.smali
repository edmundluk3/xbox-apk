.class Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$1;
.super Ljava/lang/Object;
.source "ClubBackgroundChangeViewModel.java"

# interfaces
.implements Lretrofit2/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->load(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit2/Callback",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lretrofit2/Call;Ljava/lang/Throwable;)V
    .locals 2
    .param p2, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 198
    .local p1, "call":Lretrofit2/Call;, "Lretrofit2/Call<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Load screenshots failed"

    invoke-static {v0, v1, p2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->access$400(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;Ljava/util/List;)V

    .line 200
    return-void
.end method

.method public onResponse(Lretrofit2/Call;Lretrofit2/Response;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;",
            ">;",
            "Lretrofit2/Response",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    .local p1, "call":Lretrofit2/Call;, "Lretrofit2/Call<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;>;"
    .local p2, "response":Lretrofit2/Response;, "Lretrofit2/Response<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {p2}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;->values()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->access$400(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;Ljava/util/List;)V

    .line 194
    return-void
.end method
