.class public Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "IncludedContentScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
        "<+",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        "+",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;>",
        "Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;"
    }
.end annotation


# instance fields
.field private biAlreadyLogged:Z

.field private isLoadingIncludedContent:Z

.field private loadIncludedContentTask:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel",
            "<TT;>.",
            "LoadIncludedContentAsyncTask;"
        }
    .end annotation
.end field

.field protected mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 32
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>()V

    .line 23
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getIncludedContentAdapter(Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 36
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 37
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 38
    .local v1, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 39
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 40
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 41
    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->isLoadingIncludedContent:Z

    return p1
.end method

.method private hasIncludedContent()Z
    .locals 1

    .prologue
    .line 105
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIncludedContent()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIncludedContent()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logBI()V
    .locals 8

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    const/4 v2, 0x0

    .line 113
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->biAlreadyLogged:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsBundle()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 114
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v7

    .line 115
    .local v7, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const/4 v0, 0x0

    .line 116
    .local v0, "from":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 117
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getFromScreen()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v6

    .line 118
    .local v6, "fromScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v6, :cond_0

    .line 119
    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v0

    .line 122
    .end local v6    # "fromScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    if-nez v0, :cond_1

    .line 123
    const-string v0, ""

    .line 125
    :cond_1
    const-string v1, "Details Bundle"

    const-string v4, "productID"

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getProductId()Ljava/lang/String;

    move-result-object v5

    move-object v3, v2

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->trackLegacy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->biAlreadyLogged:Z

    .line 128
    .end local v0    # "from":Ljava/lang/String;
    .end local v7    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_2
    return-void
.end method


# virtual methods
.method public getIncludedContent()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIncludedContent()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 45
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 50
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->isLoadingIncludedContent:Z

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 56
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->loadIncludedContentTask:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->load(Z)V

    .line 57
    return-void
.end method

.method public navigateToDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 109
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 110
    return-void
.end method

.method public onLoadIncludedContentCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 83
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    const-string v0, "IncludedContentScreenViewModel"

    const-string v1, "onLoadIncludedContent Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->isLoadingIncludedContent:Z

    .line 86
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 100
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->logBI()V

    .line 101
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->updateAdapter()V

    .line 102
    return-void

    .line 90
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->hasIncludedContent()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 94
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 95
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 66
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getIncludedContentAdapter(Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 67
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 61
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->loadIncludedContentTask:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;

    .line 62
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 72
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->loadIncludedContentTask:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->loadIncludedContentTask:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->cancel()V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->loadIncludedContentTask:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;

    .line 76
    :cond_0
    return-void
.end method
