.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ClubAdminHomeScreenAdapter.java"


# instance fields
.field private final bannedBase:Ljava/lang/String;

.field private final bannedButton:Landroid/widget/Button;

.field private final countAppearance:Landroid/text/style/TextAppearanceSpan;

.field private final membersBase:Ljava/lang/String;

.field private final membersButton:Landroid/widget/Button;

.field private final reportsBase:Ljava/lang/String;

.field private final reportsButton:Landroid/widget/Button;

.field private final requestBase:Ljava/lang/String;

.field private final requestsButton:Landroid/widget/Button;

.field private final settingsButton:Landroid/view/View;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 39
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    .line 42
    const v1, 0x7f0e0a95

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 43
    const v1, 0x7f0e0265

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->requestsButton:Landroid/widget/Button;

    .line 44
    const v1, 0x7f0e0266

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->reportsButton:Landroid/widget/Button;

    .line 45
    const v1, 0x7f0e0267

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->membersButton:Landroid/widget/Button;

    .line 46
    const v1, 0x7f0e0268

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->bannedButton:Landroid/widget/Button;

    .line 47
    const v1, 0x7f0e0269

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->settingsButton:Landroid/view/View;

    .line 49
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07020f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->requestBase:Ljava/lang/String;

    .line 50
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070215

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->reportsBase:Ljava/lang/String;

    .line 51
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070214

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->membersBase:Ljava/lang/String;

    .line 52
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070167

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->bannedBase:Ljava/lang/String;

    .line 54
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f08021e

    invoke-direct {v1, v2, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->countAppearance:Landroid/text/style/TextAppearanceSpan;

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->requestsButton:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->reportsButton:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->membersButton:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->bannedButton:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->settingsButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    const v1, 0x7f0e02b5

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 63
    .local v0, "suspendedTextView":Landroid/widget/TextView;
    const v1, 0x7f0702da

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 64
    return-void
.end method

.method private static getCount(J)Ljava/lang/String;
    .locals 2
    .param p0, "count"    # J

    .prologue
    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->navigateToRequests()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->navigateToReports()V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->navigateToMembers()V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->navigateToBanned()V

    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->navigateToSettings()V

    return-void
.end method

.method private setStyledText(Landroid/widget/Button;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "button"    # Landroid/widget/Button;
    .param p2, "base"    # Ljava/lang/String;
    .param p3, "count"    # Ljava/lang/String;

    .prologue
    .line 89
    new-instance v0, Landroid/text/SpannableString;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 90
    .local v0, "styledTitle":Landroid/text/SpannableString;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->countAppearance:Landroid/text/style/TextAppearanceSpan;

    .line 92
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    .line 93
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    const/16 v4, 0x21

    .line 90
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 96
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 97
    return-void
.end method

.method private updateCountIfPositive(Landroid/widget/Button;Ljava/lang/String;J)V
    .locals 3
    .param p1, "button"    # Landroid/widget/Button;
    .param p2, "base"    # Ljava/lang/String;
    .param p3, "count"    # J

    .prologue
    .line 81
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    .line 82
    invoke-static {p3, p4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->getCount(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->setStyledText(Landroid/widget/Button;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :goto_0
    return-void

    .line 84
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->requestsButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->requestBase:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->getRequestsCount()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->updateCountIfPositive(Landroid/widget/Button;Ljava/lang/String;J)V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->reportsButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->reportsBase:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->getReportsCount()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->updateCountIfPositive(Landroid/widget/Button;Ljava/lang/String;J)V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->membersButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->membersBase:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->getMembersCount()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->updateCountIfPositive(Landroid/widget/Button;Ljava/lang/String;J)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->bannedButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->bannedBase:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->getBannedCount()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->updateCountIfPositive(Landroid/widget/Button;Ljava/lang/String;J)V

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->settingsButton:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;->isSettingsVisible()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 78
    :cond_0
    return-void
.end method
