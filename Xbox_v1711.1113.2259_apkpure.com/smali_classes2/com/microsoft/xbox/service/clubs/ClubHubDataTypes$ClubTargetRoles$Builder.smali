.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles$Builder;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;
.end method

.method public abstract localizedRole(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles$Builder;
.end method

.method public abstract roles(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles$Builder;"
        }
    .end annotation
.end method
