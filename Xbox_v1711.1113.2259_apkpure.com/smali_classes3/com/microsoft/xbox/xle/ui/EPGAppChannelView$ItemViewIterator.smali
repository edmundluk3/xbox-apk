.class Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;
.super Ljava/lang/Object;
.source "EPGAppChannelView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemViewIterator"
.end annotation


# instance fields
.field private mIncrement:I

.field private mIndex:I

.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$1;

    .prologue
    .line 195
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;-><init>(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)V

    return-void
.end method


# virtual methods
.method public getNext(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 215
    iget v6, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIndex:I

    if-gez v6, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-object v5

    .line 219
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->access$200(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    move-result-object v6

    iget v9, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIndex:I

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getItem(I)Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    move-result-object v0

    .line 221
    .local v0, "item":Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->access$200(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getItemCount()I

    move-result v1

    .line 223
    .local v1, "itemCount":I
    if-lez v1, :cond_3

    iget v6, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIndex:I

    add-int/lit8 v9, v1, 0x1

    if-le v6, v9, :cond_3

    move v3, v7

    .line 224
    .local v3, "needsPlaceholder":Z
    :goto_1
    if-nez v0, :cond_4

    move v2, v7

    .line 225
    .local v2, "itemIsEmpty":Z
    :goto_2
    move v3, v2

    .line 226
    if-lez v1, :cond_5

    move v3, v7

    .line 227
    :goto_3
    if-eqz v3, :cond_6

    iget v6, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIndex:I

    if-le v6, v1, :cond_6

    move v3, v7

    .line 229
    :goto_4
    if-eqz v2, :cond_2

    if-eqz v3, :cond_0

    .line 233
    :cond_2
    invoke-static {p1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    move-result-object v5

    .line 234
    .local v5, "view":Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->access$200(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getImageAspectRatio()F

    move-result v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->setAspectRatio(F)V

    .line 236
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->access$300(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getScrollState()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    move-result-object v4

    .line 237
    .local v4, "scrollState":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;
    if-nez v2, :cond_8

    .line 238
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->scrolling()Z

    move-result v6

    if-nez v6, :cond_7

    move v6, v7

    :goto_5
    invoke-virtual {v5, v0, v7, v6}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->setData(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;ZZ)V

    .line 239
    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->setVisibility(I)V

    .line 240
    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->setClickable(Z)V

    .line 246
    :goto_6
    iget v6, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIndex:I

    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->access$100(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)I

    move-result v7

    mul-int/2addr v6, v7

    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->access$100(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)I

    move-result v7

    invoke-static {v5, v6, v7, v8}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setChildViewLayoutParams(Landroid/view/View;IIZ)Landroid/view/View;

    .line 249
    iget v6, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIndex:I

    iget v7, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIncrement:I

    add-int/2addr v6, v7

    iput v6, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIndex:I

    goto :goto_0

    .end local v2    # "itemIsEmpty":Z
    .end local v3    # "needsPlaceholder":Z
    .end local v4    # "scrollState":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;
    .end local v5    # "view":Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
    :cond_3
    move v3, v8

    .line 223
    goto :goto_1

    .restart local v3    # "needsPlaceholder":Z
    :cond_4
    move v2, v8

    .line 224
    goto :goto_2

    .restart local v2    # "itemIsEmpty":Z
    :cond_5
    move v3, v8

    .line 226
    goto :goto_3

    :cond_6
    move v3, v8

    .line 227
    goto :goto_4

    .restart local v4    # "scrollState":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;
    .restart local v5    # "view":Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
    :cond_7
    move v6, v8

    .line 238
    goto :goto_5

    .line 243
    :cond_8
    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->setClickable(Z)V

    goto :goto_6
.end method

.method public reset(IZ)Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;
    .locals 2
    .param p1, "offset"    # I
    .param p2, "forward"    # Z

    .prologue
    const/4 v1, -0x1

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->access$100(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->access$100(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)I

    move-result v0

    div-int v0, p1, v0

    :goto_0
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIndex:I

    .line 203
    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIncrement:I

    .line 205
    if-nez p2, :cond_1

    .line 206
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIndex:I

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIncrement:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->mIndex:I

    .line 209
    :cond_1
    return-object p0

    :cond_2
    move v0, v1

    .line 202
    goto :goto_0
.end method
