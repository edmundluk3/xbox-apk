.class public Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "PurchaseWebViewActivityAdapter.java"


# instance fields
.field private errorCloseButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private loadingCancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

.field private webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)V
    .locals 3
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    const v2, 0x7f0e0995

    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .line 26
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->getIsCodeRedemption()Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    const v0, 0x7f0e0996

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->webView:Landroid/webkit/WebView;

    .line 31
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->webView:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocus()Z

    .line 33
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->screenBody:Landroid/view/View;

    .line 34
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 35
    const v0, 0x7f0e099a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->loadingCancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 36
    const v0, 0x7f0e0998

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->errorCloseButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 37
    return-void

    .line 29
    :cond_0
    const v0, 0x7f0e0997

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->webView:Landroid/webkit/WebView;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    return-object v0
.end method


# virtual methods
.method public getAnimateIn(Z)Ljava/util/ArrayList;
    .locals 3
    .param p1, "goingBack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v0, "animations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;>;"
    sget-object v2, Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;->ANIMATE_IN:Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;

    invoke-virtual {p0, v2, p1}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->getTitleBarAnimation(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v1

    .line 103
    .local v1, "titleBarAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    if-eqz v1, :cond_0

    .line 104
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    :cond_0
    return-object v0
.end method

.method public getAnimateOut(Z)Ljava/util/ArrayList;
    .locals 3
    .param p1, "goingBack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v0, "animations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;>;"
    sget-object v2, Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;->ANIMATE_OUT:Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;

    invoke-virtual {p0, v2, p1}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->getTitleBarAnimation(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v1

    .line 114
    .local v1, "titleBarAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_0
    return-object v0
.end method

.method public getWebView()Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->webView:Landroid/webkit/WebView;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onDestroy()V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->webView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->removeAllViews()V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->webView:Landroid/webkit/WebView;

    .line 96
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->loadingCancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->loadingCancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->errorCloseButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->errorCloseButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->loadingCancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->loadingCancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->errorCloseButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->errorCloseButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    :cond_1
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 85
    return-void
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 43
    :cond_0
    return-void
.end method
